package com.sinosoft.version;
/**
 * 【系统版本文件】
 * @Description: TODO
 * @author chenxin@sinosoft.com.cn
 * @date 2015年7月31日 上午10:58:25 
 * @version V1.0
 */
public class Version {

	public static final String VERSION_NO = "1.0";
	
	public static final String APP_NAME = "中科软税优健康险中间平台系统";
	
	public static final String COMPANY_NAME = "中科软科技股份有限公司";
	
	public static final String WEB_SITE = "www.sinosoft.com.cn@Copyright 2015 Sinosoft Co. Ld.";
}
