package com.sinosoft.platform.common.logger;

import org.apache.log4j.Logger;

/**
 * 
  *【日志类】
  *
  * @author chenxin
  * @date  May 24, 2011 
  * @version 1.0
  *
  *【修改描述】
  *
  * @author Administrator
  * @date  May 24, 2011
 */
public class DevLogger {

    /**
     * 跟踪日志配置
     */
    private static final String TRANCE_APPENDER = "traceLog";
    
    /**
     * 异常日志配置
     */
    private static final String ERROR_APPENDER = "errorLog";

    /**
      * 
      * 纪录跟踪日志
      * @param message 
      *     消息
      */
    public static void tranceLog(Object message) {
        Logger logger = Logger.getLogger(TRANCE_APPENDER);
        logger.trace(message);
    }
    
    /**
     * 
     * 纪录异常日志
     * @param message 
     *     消息
     */
    public static void errorLog(Object message) {
        Logger logger = Logger.getLogger(ERROR_APPENDER);
        logger.error(message);
    }

}
