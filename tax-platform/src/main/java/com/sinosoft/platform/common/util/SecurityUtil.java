package com.sinosoft.platform.common.util;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.digest.DigestUtils;

import com.sinosoft.platform.common.exception.SignErrException;

/**
 * 安全加密工具
 * 
 * @Title: SecurityUtil.java
 * @Package com.sinosoft.platform.common.util
 * @Description: 安全加密工具，提供MD5、SHA1等加密方法
 * @author Gavin
 * @date 2015-3-20 下午5:39:58
 * @version V1.0
 */
public class SecurityUtil {
	private static final int MD5_LENGTH = 1024 * 2;

	/**
	 * 根据key生成MD加密信息
	 * 
	 * @param requestMessage
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public static String signMD5(String requestMessage, String key)
			throws Exception {
		// 数字签名校验
		String source = key + requestMessage;
		return DigestUtils.md5Hex(source.getBytes("UTF-8"));
	}

	/**
	 * MD5加密
	 * 
	 * @param plainText
	 * @return
	 */
	public static String Md5(String plainText) {
		StringBuffer buf = new StringBuffer();
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(plainText.getBytes("utf-8"));
			byte b[] = md.digest();
			int i;
			for (int offset = 0; offset < b.length; offset++) {
				i = b[offset];
				if (i < 0) {
					i += 256;
				}
				if (i < 16) {
					buf.append("0");
				}
				buf.append(Integer.toHexString(i));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return buf.toString();
	}

	/**
	 * 保留长度MD5加密
	 * @param plainText
	 * @return
	 */
	public static String generateMD5(String plainText) {
		try {
			if (null == plainText || plainText.length() == 0)
				return null;
			plainText = (plainText.length() > MD5_LENGTH) ? plainText
					.substring(0, MD5_LENGTH) : plainText;
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] bytes = plainText.getBytes(Charset.forName("UTF-8"));
			md.update(bytes);
			byte b[] = md.digest();
			int i;
			StringBuffer buf = new StringBuffer("");
			for (int offset = 0, j = b.length; offset < j; offset++) {
				i = b[offset];
				if (i < 0)
					i += 256;
				if (i < 16)
					buf.append("0");
				buf.append(Integer.toHexString(i));
			}
			return buf.toString().toLowerCase();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * SHA1加密方法
	 * 
	 * @Title: SHA1
	 * @Description: TODO
	 * @param decript
	 * @return
	 */
	public static String SHA1(String decript) {
		StringBuffer hexString = new StringBuffer();
		try {
			MessageDigest digest = java.security.MessageDigest
					.getInstance("SHA-1");
			digest.update(decript.getBytes());
			byte messageDigest[] = digest.digest();
			// Create Hex String
			// 字节数组转换为 十六进制 数
			for (int i = 0; i < messageDigest.length; i++) {
				String shaHex = Integer.toHexString(messageDigest[i] & 0xFF);
				if (shaHex.length() < 2) {
					hexString.append(0);
				}
				hexString.append(shaHex);
			}

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return hexString.toString();
	}

	/**
	 * des加密
	 * 
	 * @history 1.add by chenxin 2016-1-18 13:25
	 * @param msg
	 * @return
	 */
	public static String encryptDES(String msg) {
		DESSecurityUtil desUtil;
		String result = "";
		try {
			desUtil = new DESSecurityUtil();
			result = desUtil.encrypt(msg);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SignErrException(e.getMessage());
		}
		return result;
	}

	/**
	 * des解密
	 * 
	 * @history 1.add by chenxin 2016-1-18 13:25
	 * @param msg
	 * @return
	 */
	public static String decryptDES(String msg) {
		DESSecurityUtil desUtil;
		String result = "";
		try {
			desUtil = new DESSecurityUtil();
			result = desUtil.decrypt(msg);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SignErrException(e.getMessage());
		}
		return result;
	}

	/**
	 * main函数
	 * 
	 * @param agrs
	 */
	public static void main(String agrs[]) {
		System.out.println(SecurityUtil.Md5("1"));
	}

}
