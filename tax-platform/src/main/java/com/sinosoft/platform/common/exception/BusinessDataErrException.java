/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.platform.common.exception<br/>
 * @FileName: BusinessDataErrException.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.platform.common.exception;

import com.sinosoft.platform.base.TaxBaseException;

/**  
 * 【业务数据错误异常】
 * @Description: 业务数据错误异常
 * @author Gavin
 * @date 2014-6-16 上午11:53:01 
 * @version V1.0  
 */
@SuppressWarnings("serial")
public class BusinessDataErrException extends TaxBaseException{
	public BusinessDataErrException(String msg){ super(msg);}
}


