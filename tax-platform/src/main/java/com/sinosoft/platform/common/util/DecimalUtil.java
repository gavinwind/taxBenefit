package com.sinosoft.platform.common.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DecimalUtil {
	// 保留小数点后几位小数的方法（四舍五入）
	public static double subDecimalDigits(double dist, int d) {
		 NumberFormat ddf =NumberFormat.getNumberInstance();
		 ddf.setMaximumFractionDigits(d);
		 String result = ddf.format(dist);
		 // 将数值中的逗号去掉
		 result = result.replaceAll(",", "");
		 return Double.valueOf(result);
	}
	
	public static double[] calcPercentage(List<BigDecimal> shares) {
		BigDecimal sum = new BigDecimal("0");
		for (BigDecimal share : shares) {
			sum = sum.add(share);
		}
		double[] arr = new double[shares.size()];
		for (int i = 0; i < shares.size(); i++) {
			double percentage = shares.get(i).divide(sum, 10, RoundingMode.HALF_DOWN).doubleValue();
			arr[i] = subDecimalDigits(percentage, 4);
		}
		return arr;
	}
	
	@SuppressWarnings("rawtypes")
	public static BigDecimal sumByField(List list, String fieldName) throws Exception {
		BigDecimal sum = new BigDecimal("0");
		for (Object obj : list) {
			Object value = ClassInvokeUtil.getMethodValue(obj, fieldName);
			if (value instanceof BigDecimal) {
				sum = sum.add((BigDecimal)value);
			} else {
				throw new RuntimeException(list+"中元素属性"+fieldName+"不支持非Decimal类型的求和操作");
			}
		}
		return sum;
	}
	
	/**
	 * 
	 * 方法功能描述
	 * @Title: sumByFieldWithFilter 
	 * @Description: 根据多重的过滤条件对List中的指定字段求和
	 * @param list
	 * @param fieldName
	 * @param filterMap
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public static BigDecimal sumByFieldWithFilter(List list, String fieldName, Map<String, Object> filterMap) throws Exception {
		BigDecimal sum = new BigDecimal("0");
		for (Object obj : list) {
			Object value = ClassInvokeUtil.getMethodValue(obj, fieldName);
			boolean matched = true;
			for(Map.Entry<String, Object> entry : filterMap.entrySet()){
			    String field = entry.getKey();
			    Object mappingValue = entry.getValue();
			    Object actualValue = ClassInvokeUtil.getMethodValue(obj, field);
			    if (!actualValue.equals(mappingValue)) {
			    	matched = false;
			    	break;
			    }
			}
			if (matched) {
				if (value instanceof BigDecimal) {
					sum = sum.add((BigDecimal)value);
				} else {
					throw new RuntimeException(list+"中元素属性"+fieldName+"不支持非Decimal类型的求和操作");
				}
			}
		}
		return sum;
	}
	
	@SuppressWarnings("rawtypes")
	public static Map<Object, BigDecimal> sumByField(List list, String fieldName, String groupBy) throws Exception {
		Map<Object, BigDecimal> resultMap = new HashMap<Object, BigDecimal>();
		for (Object obj : list) {
			Object groupVal = ClassInvokeUtil.getMethodValue(obj, groupBy);
			Object value = ClassInvokeUtil.getMethodValue(obj, fieldName);
			if (resultMap.containsKey(groupVal)) {
				resultMap.put(groupVal, resultMap.get(groupVal).add((BigDecimal)value));
			} else {
				resultMap.put(groupVal, (BigDecimal)value);
			}
		}
		return resultMap;
	}
	
	public static void main(String[] args) {
		System.out.println(DecimalUtil.subDecimalDigits(12112.135534475, 6));
	}
}
