package com.sinosoft.platform.common.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * 
  *【类或接口功能描述】
  *
  * @author chenxin
  * @date  2011-8-24 
  * @version 1.0
  *
  *【修改描述】
  *
  * @author chenxin
  * @date  2011-8-24
 */
@XmlType(name = "serviceResponse", namespace = "dto.webservice.api.common.platform.guohualife.com")
@XmlAccessorType(XmlAccessType.FIELD)
public class ServiceResponse {

    /**
     * 返回结果
     */
    private Object resObject;
    
    /**
     * 返回码
     */
    private String resCode;
    
    /**
     * 返回信息
     */
    private String resMsg;

    /**
     * @return the resObject
     */
    public Object getResObject() {
        return resObject;
    }

    /**
     * @param resObject the resObject to set
     */
    public void setResObject(Object resObject) {
        this.resObject = resObject;
    }

    /**
     * @return the resCode
     */
    public String getResCode() {
        return resCode;
    }

    /**
     * @param resCode the resCode to set
     */
    public void setResCode(String resCode) {
        this.resCode = resCode;
    }

    /**
     * @return the resMsg
     */
    public String getResMsg() {
        return resMsg;
    }

    /**
     * @param resMsg the resMsg to set
     */
    public void setResMsg(String resMsg) {
        this.resMsg = resMsg;
    }
    
}
