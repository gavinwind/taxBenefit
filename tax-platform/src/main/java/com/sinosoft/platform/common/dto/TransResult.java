package com.sinosoft.platform.common.dto;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * 交易结果实体类
 * 
 * @author chenxin
 * @date 2015/05/12
 * @version 1.0
 */
public class TransResult {

    /**
     * 和第三方服务交互返回交易编码<br/> 1为成功,0为失败,-1为发生异常 1:交易处理成功 0:交易处理失败 -1:交易处理发生内部异常
     */
    private String resultCode = "1";

    /**
     * 和第三方服务交互返回交易描述
     */
    private String resultInfoDesc = "交易处理成功";

    /**
     * 验证失败错误信息实体类集合
     */
    private List < ValidateError > validateErrors;

    /**
     * @return the resultCode
     */
    public String getResultCode() {
        return resultCode;
    }

    /**
     * @param resultCode the resultCode to set
     */
    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    /**
     * @return the resultInfoDesc
     */
    public String getResultInfoDesc() {
        return resultInfoDesc;
    }

    /**
     * @param resultInfoDesc the resultInfoDesc to set
     */
    public void setResultInfoDesc(String resultInfoDesc) {
        this.resultInfoDesc = resultInfoDesc;
    }

    /**
     * @return the validateErrors
     */
    public List < ValidateError > getValidateErrors() {
        return validateErrors;
    }

    /**
     * @param validateErrors the resultInfoDesc to set
     */
    public void setValidateErrors(List < ValidateError > validateErrors) {
        this.resultCode = "0";
        this.resultInfoDesc = "交易处理失败";
        this.validateErrors = validateErrors;
    }

    /**
     * 执行期间发生异常,封装异常消息返回给调用端
     * 
     * @param ex
     *      参数
     */
    public void addException(Exception ex) {
        this.setResultCode("-1");
        StringWriter sw = new StringWriter();
        ex.printStackTrace(new PrintWriter(sw));
        this.setResultInfoDesc(sw.toString());
    }

    /**
     * 判断服务是否发生异常
     * 
     * @return true 发生异常
     */
    public boolean hasException() {
        if (this.resultCode.equals("-1")) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * 判断是否有错误
     * 
     * @return true 发生错误
     */
    public boolean hasErrors() {
        if (this.validateErrors != null && this.validateErrors.size() > 0) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * 判断交易是否成功
     * 
     * @return true 交易成功 , false 交易失败(非异常)
     */
    public boolean isSuccess() {
        if (this.resultCode.equals("1")) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * 执行交易成功
     */
    public void success() {
        this.setResultCode("1");
        this.setResultInfoDesc("交易处理成功");
    }

    /**
     * 执行交易失败 并发发生异常情况，失败指发生逻辑校验未通过的情况
     */
    public void failure() {
        this.setResultCode("0");
        this.setResultInfoDesc("交易处理失败");
    }

    /**
     * 执行交易发生异常
     */
    public void exception() {
        this.setResultCode("-1");
        this.setResultInfoDesc("交易处理异常");
    }

    /**
     * 添加错误到错误队列
     * 
     * @param validateError
     *      参数
     */
    public void addValidateError(ValidateError validateError) {
        if (this.validateErrors == null) {
            this.validateErrors = new ArrayList < ValidateError > ();
        }
        this.resultCode = "0";
        this.resultInfoDesc = "交易处理失败";
        this.validateErrors.add(validateError);
    }

    /**
     * 添加错误到错误队列
     * 
     * @param code 错误代码
     * @param msg  错误信息
     */
    public void addValidateError(String code, String msg) {
        ValidateError error = new ValidateError();
        error.setCode(code);
        error.setMessage(msg);
        this.addValidateError(error);
    }
}
