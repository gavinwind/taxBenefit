package com.sinosoft.platform.common.exception;

import com.sinosoft.platform.base.TaxBaseException;
/**
 * 【回滚型业务异常】
 * @Description: 业务服务异常
 * @author Gavin
 * @date 2014-6-15 下午06:39:59 
 * @version V1.0
 */
@SuppressWarnings("serial")
public class BusinessServiceException extends TaxBaseException {
   
    public BusinessServiceException(String message) {
        super(message);
    }
    /**
     * @param message
     * @param cause
     */
    public BusinessServiceException(String message, Throwable cause) {
        super(message, cause);
    }

}
