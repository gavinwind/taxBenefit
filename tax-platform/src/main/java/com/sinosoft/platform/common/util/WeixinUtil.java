/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.platform.common.util<br/>
 * @FileName: WeixinUtil.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.platform.common.util;

import java.util.Arrays;

import org.apache.log4j.Logger;

/**  
 * @Title: WeixinUtil.java 
 * @Package com.sinosoft.platform.common.util 
 * @Description: TODO
 * @author Gavin
 * @date 2015-3-11 上午6:01:01 
 * @version V1.0  
 */
public class WeixinUtil {
	
	private static String WX_TOKEN = "";
	/**
	 * 获取微信的token
	 * @Title: getToken 
	 * @Description: 获取微信的token
	 * @return
	 */
	public static String getToken(){
		return WX_TOKEN;
	}
	/**
	 * 微信生成验证签名
	 * @Title: getSignature 
	 * @Description: 根据微信服务器传来的信息，生成签名，验证消息是否来自微信服务器
	 * @param strArray
	 * @return
	 */
	public static String getSignature(String[] strArray) {
		Arrays.sort(strArray);
		// 将排序后的结果拼接成一个字符串
		StringBuffer toSecretStr = new StringBuffer();
		for(String str : strArray){
			toSecretStr.append(str);
		}
		String result  = SecurityUtil.SHA1(toSecretStr.toString()); 
		return result;
	}
	
	public String getUrlSignature(String timestamp,String nonce,String jsurl){
		return "";
	}
}


