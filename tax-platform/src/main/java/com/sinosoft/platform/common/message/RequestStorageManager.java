package com.sinosoft.platform.common.message;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

/**
 * 
  *【类或接口功能描述】
  *
  * @author WL
  * @date  2011-7-12 
  * @version 1.0
  *
  *【修改描述】
  *
  * @author WL
  * @date  2011-7-12
 */
public class RequestStorageManager {

    private static RequestStorageManager c_instance;

    private static final String SETUP_FOR_REQUEST_INDICATOR = "ghlife.request";

    private Map storagePerThreadMap = new Hashtable();

    /**
     * 返回RequestStorageManager的实例
     */
    public synchronized static final RequestStorageManager getInstance() {
        if (c_instance == null) {
            c_instance = new RequestStorageManager();
        }
        return c_instance;
    }

    /**
     * 判断是否存在存储对象
     * 
     * @param key
     */
    public boolean has(String key) {
        Object value = getRequestStorageMap().get(key);
        boolean valueExists = value != null;
        return valueExists;
    }

    /**
     * 返回存储对象
     * 
     * @param key
     */
    public Object get(String key) {
        Map rsm = getRequestStorageMap();
        Object value = rsm.get(key);
        if (value == null) {
            IllegalArgumentException e = new IllegalArgumentException("不存在键值为 '" + key + "' 的存储对象");

            throw e;
        }
        return value;
    }

    /**
     * 在当前Request范围内存储对象
     * 
     * @param key
     * @param value
     * @see
     */
    public void set(String key, Object value) {
        getRequestStorageMap().put(key, value);
    }

    /**
     * 在当前Request范围内移除存储对象
     * 
     * @param key
     * @return the value that was removed.
     * @throws IllegalArgumentException
     *             if the value was not found.
     */
    public Object remove(String key) {
        Map rsm = getRequestStorageMap();
        Object value = rsm.remove(key);
        if (value == null) {
            IllegalArgumentException e = new IllegalArgumentException("不存在键值为 '" + key + "' 的存储对象");
            throw e;
        }

        return value;
    }

    /**
     * 清空所有Request存储
     */
    public void clear() {
        getRequestStorageMap().clear();
    }

    /**
     * 判断是否已经为当前Request建立了存储池
     * 
     * @return
     */
    public boolean isSetupForRequest() {
        boolean setupForRequest = false;
        if (has(SETUP_FOR_REQUEST_INDICATOR)) {
            setupForRequest = ((Boolean) get(SETUP_FOR_REQUEST_INDICATOR)).booleanValue();
        }
        return setupForRequest;
    }

    /**
     * 用指定Map结构为Request创建一个存储区
     */
    public void setupForRequest(Map map) {
        // map不是同一个对象时，进行重设
        if (map != this.getRequestStorageMap()) {
            clear();
            if (map != null) {
                storagePerThreadMap.put(Thread.currentThread(), map);
            }
            else {
                storagePerThreadMap.put(Thread.currentThread(), new HashMap());
            }
            set(SETUP_FOR_REQUEST_INDICATOR, Boolean.TRUE);
        }
    }

    /**
     * 清空当前Request的存储区
     */
    public void cleanupFromRequest() {
        clear();
        set(SETUP_FOR_REQUEST_INDICATOR, Boolean.FALSE);
    }

    /**
     * 返回当前Request对应的存储区
     */
    public Map getRequestStorageMap() {
        Map map = (Map) storagePerThreadMap.get(Thread.currentThread());
        if (map == null) {
            map = new HashMap();
            storagePerThreadMap.put(Thread.currentThread(), map);
        }
        return map;
    }

    public RequestStorageManager() {

    }

}
