/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.platform.common.exception<br/>
 * @FileName: HXNetEception.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.platform.common.exception;

import com.sinosoft.platform.base.TaxBaseException;

/**  
 * 【网络连接异常】
 * @Description: 网络连接异常
 * @author Gavin
 * @date 2014-6-15 下午06:44:16 
 * @version V1.0  
 */
@SuppressWarnings("serial")
public class NetConnectionException extends TaxBaseException{
	public NetConnectionException(String msg){
		super();
	}
}


