package com.sinosoft.platform.common.exception;



import com.sinosoft.platform.base.TaxBaseException;

/**
 * 【数据库DAO异常】
 * @Description: 数据库DAO异常
 * @author Gavin
 * @date 2014-6-15 下午06:40:48 
 * @version V1.0
 */
@SuppressWarnings("serial")
public class DataBaseDAOException extends TaxBaseException {
	public DataBaseDAOException(String msg){
		super(msg);
	}
}

