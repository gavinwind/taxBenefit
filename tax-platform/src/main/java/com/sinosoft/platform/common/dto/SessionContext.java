package com.sinosoft.platform.common.dto;

import java.io.IOException;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoader;

import com.sinosoft.platform.common.util.IpUtil;

/**
 *【系统缓存工具类】
 * @author WangL
 * @date May 24, 2011
 * @version 1.0
 *【修改描述】
 * @author Administrator
 * @date May 24, 2011
 */
@SuppressWarnings("unchecked")

public class SessionContext {

    /**
     * log4J
     */
    protected final Logger logger = Logger.getLogger(getClass());

    /**
     * 前缀
     */
    @SuppressWarnings("unused")
	private static final String SUFF_KEY = "project.name";

    /**
     * SessionKey值
     */
    public static final String SESSION_KEY = "SESSION_DTO";

    /**
     * 请求Id
     */
    public static final String REQUEST_ID = "REQUEST_ID";

    /**
     * 消息类型
     */
    public static final String MESSAGE_TYPE = "MESSAGE_TYPE";
    
    /**
     * 标题信息
     */
    public static final String TITLE_INFO = "TITLE_INFO";

    /**
     * IP
     */
    public static final String LOCAL_HOST = "LOCAL_HOST";
    
    /**
     * 本地Ip地址
     */
    @SuppressWarnings("unused")
	private static InetAddress localhost;


    static {
        try {
            localhost = InetAddress.getLocalHost();
        }
        catch (IOException e) {
        }
    }

    /**
     * 用于存储一些Request Scope临时变量，包括
     */
    private static ThreadLocal<Object> sysParameters = new ThreadLocal<Object>() {
        protected Object initialValue() {
            return new HashMap<String,Object>();
        }
    };

    public static String getServerIP() {
        return IpUtil.getLocalIP();
    }
    
    /**
     * 获取系统IP
     * 
     * @return String
     */
    public static String getLocalIP() {
        return (String) getSysParameter(LOCAL_HOST);
    }

    /**
     * 
     * get方法
     * 
     * @return String
     */
    public static String getRequestId() {
        return (String) getSysParameter(REQUEST_ID);
    }


    /**
     * 
     * 清除sessionDTO
     * 
     */
    public static void clearSessionContext() {
        ((Map<String,Object>) sysParameters.get()).clear();
    }

    /**
     * 
     * 线程中增加值
     * 
     * @param key key
     * @param value value
     */
    public static void setSysParameter(String key, Object value) {
    	Map<String,Object> parameters = (Map<String,Object>) sysParameters.get();
        if (null == parameters) {
            // shit happen: parameters inited with customParameters
            parameters = new HashMap<String,Object>();
            sysParameters.set(parameters);
        }
        parameters.put(key, value);
    }

    /**
     * 
     * 获取线程中的值
     * 
     * @param key key
     * @return Object
     */

	public static Object getSysParameter(String key) {
		Map<String,Object> parameters = (Map<String,Object>) sysParameters.get();
        if (null == parameters) {
            return null;
        }
        return parameters.get(key);
    }

    public static ApplicationContext getAppContext() {
        return ContextLoader.getCurrentWebApplicationContext();
    }
  
}
