package com.sinosoft.platform.common.web.pagequery;

import java.util.Date;

/**
 * Lis Model抽象基类
 * 
 * @author Chenxin
 * @date July 31, 2015
 * @version 1.0
 */
public class BaseLisModel extends AbstractModel {

    /**
     * 创建日期
     */
    private Date makeDate;

    /**
     * 创建时间
     */
    private String makeTime;

    /**
     * 修改日期
     */
    private Date modifyDate;

    /**
     * 修改时间
     */
    private String modifyTime;

    /**
     * @return the makeDate
     */
    public Date getMakeDate() {
        return makeDate;
    }

    /**
     * @param makeDate the makeDate to set
     */
    public void setMakeDate(Date makeDate) {
        this.makeDate = makeDate;
    }

    /**
     * @return the makeTime
     */
    public String getMakeTime() {
        return makeTime;
    }

    /**
     * @param makeTime the makeTime to set
     */
    public void setMakeTime(String makeTime) {
        this.makeTime = makeTime;
    }

    /**
     * @return the modifyDate
     */
    public Date getModifyDate() {
        return modifyDate;
    }

    /**
     * @param modifyDate the modifyDate to set
     */
    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    /**
     * @return the modifyTime
     */
    public String getModifyTime() {
        return modifyTime;
    }

    /**
     * @param modifyTime the modifyTime to set
     */
    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

}
