package com.sinosoft.platform.common.aop.advice;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;

import org.springframework.aop.AfterReturningAdvice;
import org.springframework.aop.MethodBeforeAdvice;
import org.springframework.aop.ThrowsAdvice;

import com.sinosoft.platform.common.dto.SessionContext;
import com.sinosoft.platform.common.logger.DevLogger;

/**
 * 
 * 【日志拦截器】
 * 
 * @author WangL
 * @date May 24, 2011
 * @version 1.0
 * 【修改描述】
 * @author Administrator
 * @date May 24, 2011
 */
public class LogAdvice implements MethodBeforeAdvice, AfterReturningAdvice, ThrowsAdvice {
    /**
     * 拦截开始时间
     */
    private long beforeRunTime;

    /**
     * 拦截结束时间
     */
    private long afterRunTime;

    /**
     * 分隔符
     */
    private static final String SEQARATOR = " ";

    /**
     * 
     * 方法执行之前被调用
     * 
     * @param method
     *            拦截方法
     * @param pramArgs
     *            方法参数
     * @param target
     *            目标代理对象
     * @throws Throwable
     *             异常
     */
    public void before(Method method, Object[] pramArgs, Object target) throws Throwable {
        // 记录当前时间
        beforeRunTime = System.currentTimeMillis();
    }

    /**
     * 
     * 方法执行之后被调用
     * 
     * @param clazz
     *            返回对象
     * @param method
     *            拦截方法
     * @param pramArgs
     *            拦截方法参数
     * @param target
     *            目标代理对象
     * @throws Throwable
     *             异常
     */
    public void afterReturning(Object clazz, Method method, Object[] pramArgs, Object target) throws Throwable {
        // 记录当前时间
        afterRunTime = System.currentTimeMillis();
        // 取得该方法运行所消耗的时间
        long durationTimes = afterRunTime - beforeRunTime;
        // 类名
        String clazzName = target.getClass().getName();
        // 方法名
        String methodName = method.getName();

        String localIp = SessionContext.getLocalIP();

        DevLogger.tranceLog("RequestIp:" + localIp + SEQARATOR 
        		+ "ClassName:" + clazzName + SEQARATOR 
        		+ "MethodName:" + methodName + SEQARATOR
                + "DurationTimes:" + durationTimes);
    }

    /**
     * 
     * 抛出Exception之后被调用
     * 
     * @param method
     *            拦截方法
     * @param args
     *            拦截方法参数
     * @param target
     *            目标代理对象
     * @param ex
     *            异常类
     * @throws Throwable
     *             异常
     */
    public void afterThrowing(Method method, Object[] args, Object target, Exception ex) throws Throwable {
        String clazzName = target.getClass().getName();
        String methodName = method.getName();
        String exceptionClazz = ex.getClass().getName();

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String exceptionMessage = sw.toString();
        // 取得该方法运行所消耗的时间
        long durationTimes = afterRunTime - beforeRunTime;


        String localIp = SessionContext.getLocalIP();
        
        DevLogger.errorLog("RequestIp:"+ localIp + SEQARATOR 
        		+ "ClassName:" + clazzName + SEQARATOR 
        		+ "MethodName:" + methodName + SEQARATOR
        		+ "DurationTimes:" + durationTimes + SEQARATOR 
        		+ "ExceptionClass:" + exceptionClazz + SEQARATOR 
        		+ "ExceptionMessage:" + exceptionMessage);
        
//        SessionContext.clearSessionContext();
    }
}
