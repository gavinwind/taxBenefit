/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.platform.common.exception.normal<br/>
 * @FileName: WeixinCallException.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.platform.common.exception;

import com.sinosoft.platform.base.TaxBaseException;

/**  
 * @Title: WeixinCallException.java 
 * @Package com.sinosoft.platform.common.exception.normal 
 * @Description: 微信调用异常
 * @author Gavin
 * @date 2015-3-13 上午7:36:15 
 * @version V1.0  
 */
@SuppressWarnings("serial")
public class WeixinCallException extends TaxBaseException{
	public WeixinCallException(String msg){
		super(msg);
	}
}


