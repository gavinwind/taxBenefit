package com.sinosoft.platform.common.util;

/**
 * 序列号工具类
 * 
 * @date 2010-11-26
 * @since JDK 1.5
 * @author dingkui
 * @version 1.0
 * 
 */
public class SerialNoUtil {

    /**
     * 
     */
    public static final int CHAR_TYPE_NUMBER = 1;

    /**
     * 
     */
    public static final int CHAR_TYPE_LOWER_CASE_LETTER = 2;

    /**
     * 
     */
    public static final int CHAR_TYPE_UPPER_CASE_LETTER = 3;

    /**
     * 
     */
    private static final char[] NUMBER_SET = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

    /**
     * 
     */
    private static final char[] LOWER_CASE_CHAR_SET = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
            'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

    /**
     * 
     */
    private static final char[] UPPER_CASE_CHAR_SET = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
            'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

    /**
     * 根据指定位数，指定的集合，生成随机字符串 目前集合有三种，数字、大写字母和小写字母
     * 
     * @param bit
     *            要生成随机字符串的位数
     * @param types
     *            类型列表
     * @return 随机字符串
     */
    public static String genetateRandomSerialNo(int bit, int[] types) {
        int newSetLength = 0;
        for (int i = 0; i < types.length; ++i) {
            switch (types[i]) {
            case CHAR_TYPE_NUMBER:
                newSetLength += NUMBER_SET.length;
                break;
            case CHAR_TYPE_LOWER_CASE_LETTER:
                newSetLength += LOWER_CASE_CHAR_SET.length;
                break;
            case CHAR_TYPE_UPPER_CASE_LETTER:
                newSetLength += UPPER_CASE_CHAR_SET.length;
                break;
            }
        }
        if (newSetLength <= 0) {
            return "";
        }
        char[] newSet = new char[newSetLength];
        int totalSetCount = 0;
        for (int i = 0; i < types.length; ++i) {
            switch (types[i]) {
            case CHAR_TYPE_NUMBER:
                totalSetCount = montage(newSet, NUMBER_SET, totalSetCount);
                break;
            case CHAR_TYPE_LOWER_CASE_LETTER:
                totalSetCount = montage(newSet, LOWER_CASE_CHAR_SET, totalSetCount);
                break;
            case CHAR_TYPE_UPPER_CASE_LETTER:
                totalSetCount = montage(newSet, UPPER_CASE_CHAR_SET, totalSetCount);
                break;
            }
        }
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bit; ++i) {
            int curIndex = (int) (Math.random() * (double) newSetLength);
            sb.append(newSet[curIndex]);
        }
        return sb.toString();
    }

    /**
     * 内部方法，拼接数组
     * @param dest 目标数组
     * @param ori 源数组
     * @param destLength 目标数组起始下标
     * @return 目标数组终止下标
     */
    private static int montage(char[] dest, char[] ori, int destLength) {
        for (int i = 0; i < ori.length; ++i) {
            dest[destLength + i] = ori[i];
        }
        return ori.length + destLength;
    }

}