package com.sinosoft.platform.common.util;

import java.io.IOException;
import java.util.Properties;


/**
 * 
  *【webservice密钥工具】
  *
  * @author Administrator
  * @date  May 24, 2011 
  * @version 1.0
  *
  *【修改描述】
  *
  * @author Administrator
  * @date  May 24, 2011
 */
public class WsSecutitySDK {
    
    /**
     *  密钥地址
     */
    private static final String LOCATION = "appUser.properties";
    
    /**
     *  系统路径
     */
    private static final String SYS_PROP = "/platform/config/config.properties";

    /**
     *  前缀
     */
    private static final String SUFF_KEY = "cyberArk.";

    /**
     *  密钥路径
     */
    private static final String SECUTITY_PATH = "ws.secutity.path";
    
    /**
     * 
      * 获取密码
      * 
      * @param key
      *     参数
      * @return String
      * @throws IOException 
      *     异常
     */
    public static String getPassWord(String key) throws IOException {
        Properties props = PropertiesUtil.loadPropertie(SYS_PROP);
        String sysPath = props.getProperty(SECUTITY_PATH);
        
        Properties secProps = PropertiesUtil.loadPropertie(sysPath + LOCATION);
        return secProps.getProperty(SUFF_KEY + key);
    }

   
}
