package com.sinosoft.platform.common.web.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.context.ContextLoader;

import com.sinosoft.version.Version;


public class ContextLoaderListener implements ServletContextListener {
	
	private static final Log logger = LogFactory.getLog(ContextLoaderListener.class);;
	
	private ContextLoader contextLoader;

	public ContextLoaderListener() {
	}

	public void contextInitialized(ServletContextEvent event) {
		logger.info("*****************************************************************************************");
		logger.info("                                      "+Version.APP_NAME+"                             版本："+Version.VERSION_NO);
		logger.info("                                                                                         ");
		logger.info("                      " + Version.WEB_SITE + "                                            ");
		logger.info("*****************************************************************************************");
		contextLoader = createContextLoader();
		contextLoader.initWebApplicationContext(event.getServletContext());
	}

	protected ContextLoader createContextLoader() {
		return new ContextLoader();
	}

	public ContextLoader getContextLoader() {
		return contextLoader;
	}

	public void contextDestroyed(ServletContextEvent event) {
		if (contextLoader != null)
			contextLoader.closeWebApplicationContext(event.getServletContext());
	}

}