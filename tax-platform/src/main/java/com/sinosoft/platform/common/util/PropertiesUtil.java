package com.sinosoft.platform.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
/**
 * 
  *【配置文件工具】
  *
  * @author Administrator
  * @date  May 24, 2011 
  * @version 1.0
  *
  *【修改描述】
  *
  * @author Administrator
  * @date  May 24, 2011
 */
public class PropertiesUtil {

    /**
     * 配置地址
     */
    private static final String SYS_PROPERTIE_LOCAL = "/config-context.properties";

    /**
     * 
      * 加载系统配置
      * 
      * @param location
      *         地址
      * @throws IOException 
      *         异常
      * @return Properties
      *         异常
     */
    public static Properties loadPropertie(String location) throws IOException {
        Properties props = new Properties();
        String url = PropertiesUtil.class.getClassLoader().getResource("").getPath();
        InputStream is = new FileInputStream(new File(url).getPath() + location);
        try {
            props.load(is);
        }
        finally {
            is.close();
        }
        return props;

    }

    /**
     * 
      * 获取配置文件中的值
      * 
      * @param key
      *      key值
      * @return String
     */
    public static String getSysPropertie(String key) {
        String propValue = null;
        try {
            Properties props = loadPropertie(SYS_PROPERTIE_LOCAL);
            propValue = (String) props.get(key);
        }
        catch (IOException e) {
            propValue = "";
        }
        return propValue;
    }
}
