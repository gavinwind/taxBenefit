/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.platform.common.exception<br/>
 * @FileName: SMSSendException.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.platform.common.exception;

import com.sinosoft.platform.base.TaxBaseException;

/**  
 * 【短信发送异常】
 * @Description: 短信发送异常
 * @author chenxin@sinosoft.com.cn
 * @date 2015年8月26日 下午1:44:25 
 * @version V1.0  
 */
public class SMSSendException extends TaxBaseException{
	/** 
	* @Fields serialVersionUID : TODO
	*/
	private static final long serialVersionUID = 1L;

	public SMSSendException(String errMsg){
		super(errMsg);
	}
}


