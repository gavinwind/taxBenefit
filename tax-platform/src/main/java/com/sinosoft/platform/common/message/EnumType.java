package com.sinosoft.platform.common.message;

import java.io.Serializable;

/**
 * 
  *【Enumerated Types的基类】
  *
  * @author WL
  * @date  2011-7-12 
  * @version 1.0
  *
  *【修改描述】
  *
  * @author WL
  * @date  2011-7-12
 */
public abstract class EnumType implements Serializable {

    private String name;

    private int value;

    protected EnumType(int value, String name) {
        this.name = name;
        this.value = value;
    }

    /**
     * 返回enumerated type的int值 这个int值用来比较EnumType对象的引用
     * 
     * @return int
     */
    public int intValue() {
        return value;
    }

    /**
     * 返回名称
     * 
     * @return String
     */
    public String getName() {
        return name;
    }

    public String toString() {
        return name;
    }

    public boolean equals(Object o) {
        if (null == o) {
            return false;
        }

        if (this == o) {
            return true;
        }

        if (o instanceof EnumType) {
            final EnumType enumType = (EnumType) o;

            if (value != enumType.value) {
                return false;
            }

            if (!name.equals(enumType.name)) {
                return false;
            }

        }
        else if (o instanceof String) {
            return name.equals(o);
        }
        return true;
    }

    /**
     * 
     * 方法功能描述
     * 
     * @return int
     */
    public int hashCode() {
        int result;
        result = name.hashCode();
        return result;
    }

    /**
     * 
     * 【用于序列化】
     * 
     * @author Administrator
     * @date Jun 9, 2011
     * @version 1.0
     * 
     * 【修改描述】
     * 
     * @author Administrator
     * @date Jun 9, 2011
     */
    public EnumType() {
    }

}
