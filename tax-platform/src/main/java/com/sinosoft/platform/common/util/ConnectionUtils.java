package com.sinosoft.platform.common.util;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.log4j.Logger;

public class ConnectionUtils {

	private static Logger loger = Logger.getLogger(ConnectionUtils.class);

	private static HttpClient connectionUtils;

	private ConnectionUtils() {

	}

	private static synchronized HttpClient getHttpClient() {
		if (null == connectionUtils) {
			connectionUtils = new HttpClient();
			connectionUtils.getHttpConnectionManager().getParams()
					.setConnectionTimeout(100000);
			connectionUtils.getParams().setSoTimeout(100000);
			connectionUtils.getParams().setParameter(
					HttpMethodParams.RETRY_HANDLER,
					new DefaultHttpMethodRetryHandler(0, false));
		}
		return connectionUtils;
	}

	/**
	 * post方式发送请求
	 * 
	 * @param url
	 *            请求的url地址
	 * @param reqXml
	 *            发送的报文
	 * @param method
	 *            请求的方法
	 * @return
	 */
	public static String sendPostXml(String url, String reqXml, String method) {
		String rspXml = "";
		PostMethod post = null;
		try {
			HttpClient client = getHttpClient();
			post = new PostMethod(url);
			loger.info("URL : " + url);
			post.getParams().setParameter(
					HttpMethodParams.HTTP_CONTENT_CHARSET, "UTF-8");
			post.addParameter("action", method);
			post.addParameter("rexml", reqXml);
			int statusCode = client.executeMethod(post);
			if (statusCode != HttpStatus.SC_OK) {
				rspXml = "201";
				throw new Exception("请求失败:" + statusCode + ":"
						+ HttpStatus.getStatusText(statusCode));
			} else {
				rspXml = post.getResponseBodyAsString();
				loger.info("响应的报文：\n" + rspXml);
			}
		} catch (Exception e) {
			e.printStackTrace();
			loger.error("POST发送请求失败！错误信息：" + e.getMessage());
			System.out.println("POST发送请求失败！错误信息：" + e.getMessage());
		}
		return rspXml;
	}

	/**
	 * 向支付系统发送报文，暂时不用
	 * 
	 * @Title: sendPaymentXml
	 * @Description: TODO
	 * @param method
	 *            请求方法
	 * @param reqXml
	 *            要发送的报文
	 * @return
	 */
	public static String sendPaymentXml(String method, String reqXml) {
		String url = "http://192.168.10.34:8080/payment/sinosoft/pay.do";
		String rspXml = "";
		PostMethod post = null;
		try {
			HttpClient client = getHttpClient();
			post = new PostMethod(url);
			post.getParams().setParameter(
					HttpMethodParams.HTTP_CONTENT_CHARSET, "UTF-8");
			post.addParameter("action", method);
			post.addParameter("dataXml", reqXml);
			loger.info("请求的报文：" + reqXml);
			int statusCode = client.executeMethod(post);
			byte[] data = null;
			if (statusCode != HttpStatus.SC_OK) {
				rspXml = "201";
				throw new Exception("请求失败:" + statusCode + ":"
						+ HttpStatus.getStatusText(statusCode));
			} else {
				data = post.getResponseBody();
				rspXml = new String(data, "gbk");
				loger.info("响应的报文：\n" + rspXml);
			}
		} catch (Exception e) {
			e.printStackTrace();
			loger.error("POST发送请求失败！错误信息：" + e.getMessage());
			System.out.println("POST发送请求失败！错误信息：" + e.getMessage());
		}
		return rspXml;
	}

	/**
	 * @param url
	 * @param reqXml
	 * @param method
	 * @return
	 */
	private static String sendGetXml(String url, String reqXml, String method) {
		return "";
	}

	public static void main(String args[]) {
//		String dataXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><SharingReq>"
//				+ "<msgHead>"
//				+ "<transactionCode>100001</transactionCode>"
//				+ "<dealType>register</dealType>"
//				+ "<signedMsg>4a634533cb2f9ba7dffc9c51fe572e7c</signedMsg>"
//				+ "</msgHead>"
//				+ "<msgBody>"
//				+ "<requestData>"
//				+ "<Register>"
//				+ "<loginName>王五</loginName>"
//				+ "<password>e10adc3949ba59abbe56e057f20f883e</password>"
//				+ "<cardId>411303198807157295</cardId>"
//				+ "<mobile>15993141336</mobile>"
//				+ "</Register>"
//				+ "</requestData>" + "</msgBody>" + "</SharingReq>";
//		String xml = sendPostXml(
//				"http://127.0.0.1:8080/tutorial/customer.action", dataXml,
//				"register");
		
		String report = "test";
		System.out.println(sendPaymentXml("sinosoftTrade",report));
	}
}
