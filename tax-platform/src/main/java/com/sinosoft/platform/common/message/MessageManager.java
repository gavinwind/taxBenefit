package com.sinosoft.platform.common.message;

import java.util.Iterator;

import org.springframework.validation.BindException;

import com.sinosoft.platform.common.message.impl.MessageManagerImpl;



/**
 * 
  *【类或接口功能描述】
  *
  * @author WL
  * @date  2011-7-12 
  * @version 1.0
  *
  *【修改描述】
  *
  * @author WL
  * @date  2011-7-12
 */
public abstract class MessageManager {
    private static MessageManager c_instance;

    /**
     * 返回MessageManager实例
     * 
     * @return MessageManager
     */
    public synchronized static final MessageManager getInstance() {
        if (c_instance == null) {
            c_instance = new MessageManagerImpl();
        }
        return c_instance;
    }

    /**
     * 是否存在Message.
     * 
     * @param messageKey
     *            
     */
    public abstract boolean hasMessage(String messageKey);

    /**
     * 是否存在错误消息
     */
    public abstract boolean hasErrorMessages();

    /**
     * 是否存在警告消息
     */
    public abstract boolean hasWarningMessages();

    /**
     * 是否存在普通消息
     */
    public abstract boolean hasInfoMessages();

    /**
     * 是否存在消息
     */
    public abstract boolean hasMessages();

    /**
     * 添加新错误消息
     * 
     * @param messageKey        
     */
    public abstract void addErrorMessage(String messageKey);

    /**
     * 添加新错误消息
     * 
     * @param messageKey          
     * @param messageFieldId
     */
    public abstract void addErrorMessage(String messageKey, String messageFieldId);

    /**
     * 添加带参数的错误消息
     * 
     * @param messageKey           
     * @param messageParameters
     *        
     */
    public abstract void addErrorMessage(String messageKey, Object[] messageParameters);

    /**
     * 添加带参数的错误消息
     * 
     * @param messageKey           
     * @param messageParameters
     * @param messageFieldId 
     */
    public abstract void addErrorMessage(String messageKey, Object[] messageParameters, String messageFieldId);

    /**
     * 添加带参数的错误消息
     * 
     * @param messageKey           
     * @param messageParameters
     * @param messageFieldId
     * @param rejectValue 
     */
    public abstract void addErrorMessage(String messageKey, Object[] messageParameters, String messageFieldId,
            Object rejectValue);

    /**
     * 添加新警告消息
     * 
     * @param messageKey
     *           
     */
    public abstract void addWarningMessage(String messageKey);

    /**
     * 添加带参数新警告消息
     * 
     * @param messageKey
     * @param messageParameters
     */
    public abstract void addWarningMessage(String messageKey, Object[] messageParameters);

    /**
     * 添加普通消息
     * 
     * @param messageKey
     */
    public abstract void addInfoMessage(String messageKey);

    /**
     * 添加带参数普通消息
     * 
     * @param messageKey
     * @param messageParameters
     */
    public abstract void addInfoMessage(String messageKey, Object[] messageParameters);

    /**
     * 返回所有错误消息
     */
    public abstract Iterator getErrorMessages();

    /**
     * 返回所有警告消息
     */
    public abstract Iterator getWarningMessages();

    /**
     * 返回所有普通消息
     */
    public abstract Iterator getInfoMessages();

    /**
     * 返回所有消息
     */
    public abstract Iterator getMessages();

    /**
     * 将BindException中的错误信息加入到消息列表中
     * 
     * @param bindException        
     */
    public abstract void addBindingErrorMessages(BindException bindException);

    /**
     *返回所有消息，以逗号分割
     * 
     * @param separator         
     * @return the messages as a String
     */
    public abstract String getMessagesAsString(String separator);

    protected MessageManager() {
    }
}
