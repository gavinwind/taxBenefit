/**
 * @Copyright ®2012 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.platform.common.exception<br/>
 * @FileName: ReportException.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.platform.common.exception;

import com.sinosoft.platform.base.TaxBaseException;

/**  
 * 【对外报文请求处理返回异常】
 * @Description: TODO
 * @author Gavin@sinosoft.com.cn
 * @date 2014-6-17 上午10:08:43 
 * @version V1.0  
 */
@SuppressWarnings("serial")
public class ReportException extends TaxBaseException{
	public ReportException(String msg){
		super(msg);
	}
}


