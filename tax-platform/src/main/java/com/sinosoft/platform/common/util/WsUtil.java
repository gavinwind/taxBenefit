package com.sinosoft.platform.common.util;

import java.util.HashMap;
import java.util.Map;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.dynamic.DynamicClientFactory;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.apache.log4j.Logger;

/**
 * 
 * 【类或接口功能描述】
 * @Description: webservice的客户端调用工具类
 * @author chenxin
 * @date 2014-10-8 上午9:23:28 
 * @version V1.0
 */
@Deprecated 
public class WsUtil {
	protected static final Logger logger = Logger.getLogger(WsUtil.class);

	@SuppressWarnings("rawtypes")
	private static Map clientMap = new HashMap();

	private static DynamicClientFactory dynamicClientFactory = DynamicClientFactory
			.newInstance();

	public static Object[] invoke(String url, String method, Object[] args) {
		return invoke(url, method, args, "0");
	}

	@SuppressWarnings("unchecked")
	public static Object[] invoke(String url, String method, Object[] args,
			String urlflag) {
		try {
			Client client = (Client) clientMap.get(url);
			if (client == null) {
				if (dynamicClientFactory == null) {
					dynamicClientFactory = DynamicClientFactory.newInstance();
				}
				client = dynamicClientFactory.createClient(url);

				HTTPConduit conduit = (HTTPConduit) client.getConduit();
				HTTPClientPolicy policy = new HTTPClientPolicy();
				policy.setConnectionTimeout(1800000L);
				policy.setReceiveTimeout(1800000L);
				conduit.setClient(policy);
				clientMap.put(url, client);
			}

			Object[] results = client.invoke(method, args);
			return results;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("调用Webservice发生异常", e);
		}
		return null;
	}

	/**
	 * 
	 * 方法功能描述
	 * @Title: invoke 
	 * @Description: webservice调用方法
	 * @param url 远程请求地址
	 * @param method webservice发布的方法
	 * @param xmlRequest 请求报文
	 * @return
	 */
	public static String invoke(String url, String method, String xmlRequest) {
		Object[] results = invoke(url, method, new Object[] { xmlRequest });

		if ((results != null) && results.length > 0 && (results[0] != null)) {
			return String.valueOf(results[0]);
		}
		return "";
	}

	public static void main(String[] args) {
		String result = WsUtil.invoke("http://localhost:8080/ws_server/api/rule/executeRuleWebservice?wsdl", "executeRules", "haha");
		System.out.println(result);
	}
}