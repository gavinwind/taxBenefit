/**
 * @Copyright ®2012 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.platform.common.util<br/>
 * @FileName: XStreamDateConverter.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.platform.common.util;

import java.util.Date;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**  
 * 【类或接口功能描述】
 * @Description: TODO
 * @author chenxin
 * @date 2014-6-4 上午11:11:05 
 * @version V1.0  
 */
public class XStreamDateConverter implements Converter {

	/* 
	 * 方法功能描述
	 * <p>Title: canConvert</p> 
	 * <p>Description: </p> 
	 * @param arg0
	 * @return
	 * @see com.thoughtworks.xstream.converters.ConverterMatcher#canConvert(java.lang.Class) 
	*/
	@SuppressWarnings("rawtypes")
	public boolean canConvert(Class type) {
		// 指定要转换的类型
		return type.equals(Date.class);
	}

	/* 
	 * 方法功能描述
	 * <p>Title: marshal</p> 
	 * <p>Description: </p> 
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @see com.thoughtworks.xstream.converters.Converter#marshal(java.lang.Object, com.thoughtworks.xstream.io.HierarchicalStreamWriter, com.thoughtworks.xstream.converters.MarshallingContext) 
	*/
	public void marshal(Object source, HierarchicalStreamWriter writer,
			MarshallingContext context) {
		// 完成转换逻辑
		Date date = (Date) source;
        writer.setValue(DateUtil.getDateStr(date, "yyyy-MM-dd"));
	}

	/* 
	 * 方法功能描述
	 * <p>Title: unmarshal</p> 
	 * <p>Description: </p> 
	 * @param arg0
	 * @param arg1
	 * @return
	 * @see com.thoughtworks.xstream.converters.Converter#unmarshal(com.thoughtworks.xstream.io.HierarchicalStreamReader, com.thoughtworks.xstream.converters.UnmarshallingContext) 
	*/
	public Object unmarshal(HierarchicalStreamReader reader,
			UnmarshallingContext context) {
		// 完成逆向转换逻辑
        return DateUtil.parseDate(reader.getValue(), "yyyy-MM-dd");
	}
}


