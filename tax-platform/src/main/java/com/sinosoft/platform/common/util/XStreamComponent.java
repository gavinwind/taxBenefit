package com.sinosoft.platform.common.util;

import org.apache.commons.lang.StringUtils;

import com.sinosoft.platform.common.dto.Response;
import com.sinosoft.platform.common.dto.TransResult;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.mapper.DefaultMapper;
import com.thoughtworks.xstream.mapper.XStream11XmlFriendlyMapper;

/**
 * xtream xml 序列化反序列化组件
 * 
 * @author fusq
 * @version 1.0
 * @created 30-七月-2010 16:21:53
 */
@SuppressWarnings("unchecked")
public class XStreamComponent {

    /**
     * 
     */
    private static final String delimiter = ".";

    /**
     * xstream xml转换引擎
     */
    private XStream xstream;

    /**
     * 创建XStreamComponent实例
     * 
     * @return XStreamComponent
     */
    public static XStreamComponent newInstance() {
        XStreamComponent xmlComponent = new XStreamComponent();
        return xmlComponent;
    }

    /**
     * XStreamComponent构造函数
     */
    public XStreamComponent() {
        this.xstream = new XStream(new DomDriver());
    }

    /**
     * 将对象序列化为xml string
     * 
     * @param obj
     *      参数
     * @return String
     */
    public String toXML(Object obj) {
        return this.xstream.toXML(obj);
    }

    /**
     * 将分页对象序列化为xml string
     * @param obj
     *      参数
     * @return String
     */
    public String toPageXML(Object obj) {
        this.registerConverter(new MapCustomConverter(new DefaultMapper(XStream11XmlFriendlyMapper.class
                .getClassLoader())));
        return this.toXML(obj);
    }

    /**
     * 将分页对象反序列化为Object
     * @param xml
     *      参数
     * @return Object
     */
    public Object fromPageXML(String xml) {
        this.registerConverter(new MapCustomConverter(new DefaultMapper(XStream11XmlFriendlyMapper.class
                .getClassLoader())));
        return this.fromXML(xml);
    }

    /**
     * 将xml string反序列化为object
     * 
     * @param xml
     *      参数
     * @return Object
     */
    public Object fromXML(String xml) {
    	int index = xml.indexOf("?>");
    	if(index>=0){
    		xml= xml.substring(index+2);
    		return this.xstream.fromXML(xml);
    	}else{
    		return this.xstream.fromXML(xml);
    	}
    	
        
    }

    /**
     * 激活xstream注解引擎
     * 
     * @param type
     *            Class
     */
    public void processAnnotations(Class type) {
        this.xstream.processAnnotations(type);
    }

    /**
     * 激活xstream注解引擎
     * 
     * @param types
     *            Class[]
     */
    public void processAnnotations(Class[] types) {
        this.xstream.processAnnotations(types);
        //xstream.aliasSystemAttribute(null, "class");
    }

    
    /**
     * 激活xstream注解引擎
     * 去除class属性
     * @param types
     *            Class[]
     */
    public void processAnnotationsNoClass(Class[] types) {
        this.xstream.processAnnotations(types);
        xstream.aliasAttribute(null, "class");
    }

    /**
     * alias Class
     * 
     * @param name
     *      参数
     * @param type
     *      参数
     */
    public void alias(String name, Class type) {
        this.xstream.alias(name, type);
    }

    /**
     * alias Class
     * 
     * @param types
     *            Class[]
     */
    public void alias(Class[] types) {
        for (Class type : types) {
            String className = type.getName();
            try {
                String[] classNames = StringUtils.split(className, XStreamComponent.delimiter);
                this.xstream.alias(classNames[classNames.length - 1], type);
            }
            catch (Exception ex) {
                this.xstream.alias(className, type);
            }
//            xstream.aliasSystemAttribute(null, "class");
        }
    }


    /**
     * alias Response model of Webservice
     * 
     * @param types
     *      参数
     */
    public void aliasResponse(Class[] types) {
        this.alias("Response", Response.class);
        this.alias("TransResult", TransResult.class);
        this.alias(types);
    }

    /**
     * 注册xstream's converter
     * @param converter
     *      参数
     */
    public void registerConverter(Converter converter) {
        this.xstream.registerConverter(converter);
    }

    /**
     * 设置bean属性为xml的属性
     * @param definedIn
     *      参数
     * @param fieldName
     *      参数
     */
    public void useAttributeFor(Class definedIn, String fieldName) {
        this.xstream.useAttributeFor(definedIn, fieldName);
    }
    
    public static void main(String[] args){
    	String xml = "dafdfadfadsfadsfas";
    	int index = xml.indexOf("?>");
    	System.out.println(index);
    	if(index>=0){
    		xml= xml.substring(index+2);
    	}
    	System.out.println(xml);
    }
}
