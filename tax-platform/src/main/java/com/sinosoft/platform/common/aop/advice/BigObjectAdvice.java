package com.sinosoft.platform.common.aop.advice;

import java.lang.reflect.Method;
import java.util.List;

import org.springframework.aop.AfterReturningAdvice;
import org.springframework.aop.MethodBeforeAdvice;

import com.sinosoft.platform.common.logger.DevLogger;


/**
 * 
  *【大对象监控拦截器】
  * 对批量新增、删除进行监控，如果超过指定数量则记录日志
  * @author WangL
  * @date  May 24, 2011 
  * @version 1.0
  *
  *【修改描述】
  *
  * @author Administrator
  * @date  May 24, 2011
 */
public class BigObjectAdvice implements MethodBeforeAdvice, AfterReturningAdvice {

    /**
     * 最大数量
     */
    private int maxSize;

    /**
     * 
    * 方法执行之后被调用
    * 
    * @param returnObj
    *       返回对象
    * @param method
    *       拦截方法
    * @param pram
    *       拦截方法参数
    * @param target
    *       目标代理对象
    * @throws Throwable
    *       异常
     */
    @SuppressWarnings("unchecked")
	public void afterReturning(Object returnObj, Method method, Object[] pram, Object target) throws Throwable {
        if (returnObj instanceof List) {
            List rsList = (List) returnObj;
            if (rsList.size() > maxSize) {
                DevLogger.tranceLog("返回超过允许最大长度,返回结果集长度为" + rsList.size());
            }
        }
    }

    /**
     * 
    * 方法执行之前被调用
    * 
    * @param method
    *       拦截方法
    * @param pramObj
    *       方法参数
    * @param target
    *       目标代理对象
    * @throws Throwable
    *       异常
     */
    @SuppressWarnings("unchecked")
	public void before(Method method, Object[] pramObj, Object target) throws Throwable {
        if (pramObj.length > 2) {
            Object secObj = pramObj[1];
            if (secObj instanceof List) {
                List rsList = (List) secObj;
                if (rsList.size() > maxSize) {
                    DevLogger.tranceLog("返回超过允许最大长度,返回结果集长度为" + rsList.size());
                }
            }
        }
    }

    /**
     * @return the maxSize
     */
    public int getMaxSize() {
        return maxSize;
    }

    /**
     * @param maxSize the maxSize to set
     */
    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

   
}
