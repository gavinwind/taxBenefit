/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.hexin.platform.common.exception<br/>
 * @FileName: UnRollBackBusinessException.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.platform.common.exception;

/**  
 * 【非回滚业务异常】
 * @Description: TODO
 * @author Gavin
 * @date 2014-6-15 下午06:38:39 
 * @version V1.0  
 */
@SuppressWarnings("serial")
public class TaxBizException extends Exception{
	public TaxBizException(String msg){
		super(msg);
	}
}


