/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.platform.common.util<br/>
 * @FileName: UUIDGenerator.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.platform.common.util;

import java.util.UUID;

/**  
 * 【融资项目服务类】
 * @Description: TODO
 * @author Gavin
 * @date 2014-6-3 上午11:36:10 
 * @version V1.0  
 */
public class FinancingProjectUtil {

	public FinancingProjectUtil() {

	}
	/**
	 * 获取UUID唯一识别码
	 * @Title: getUUID 
	 * @Description: TODO
	 * @return
	 */
	public static String getUUID() {

		UUID uuid = UUID.randomUUID();
		String str = uuid.toString();
		// 去掉"-"符号
		String temp = str.substring(0, 4) + str.substring(9, 13)
				+ str.substring(14, 18) + str.substring(19, 23)
				+ str.substring(24);
		return temp;
	}

	// 获得指定数量的UUID
	public static String[] getUUID(int number) {
		if (number < 1) {
			return null;
		}

		String[] ss = new String[number];

		for (int i = 0; i < number; i++) {
			ss[i] = getUUID();
		}
		return ss;
	}
	/**
	 * 生成中农批项目默认名称
	 * @Title: createZNPProjectName 
	 * @Description: TODO
	 * @return
	 */
	public static String  createZNPProjectName(){
		return "";
	}
	
	/**
	 * 获得项目宽限期
	 * @Title: getProjectGracePeriod 
	 * @Description: TODO
	 * @param projectType
	 * @param loanTime
	 * @param loanTimeUnit
	 * @return
	 */
	public static Integer getProjectGracePeriod(String projectType, Integer loanTime,String loanTimeUnit){
		// to do   通过数据字典表获取宽限期规则
		
		
		return 3;
	}
	
	public static void main(String[] args) {
		String ss = getUUID();
		System.out.println("ss=====" + ss);
	}
}
