package com.sinosoft.platform.common.message;

import java.util.HashMap;
import java.util.Map;

/**
 * 
  *【类或接口功能描述】
  *
  * @author WL
  * @date  2011-7-12 
  * @version 1.0
  *
  *【修改描述】
  *
  * @author WL
  * @date  2011-7-12
 */
public class MessageCategory extends EnumType {

    private static int c_nextIntValue = 1;
    private static int getNextIntValue() { return c_nextIntValue++; }
    private static Map c_validTypes = new HashMap();

    public final static int ERROR_VALUE = getNextIntValue();
    public final static MessageCategory ERROR = new MessageCategory(ERROR_VALUE, "ERROR_MESSAGE");

    public final static int WARNING_VALUE = getNextIntValue();
    public final static MessageCategory WARNING = new MessageCategory(WARNING_VALUE, "WARNING_MESSAGE");

    public final static int INFORMATION_VALUE = getNextIntValue();
    public final static MessageCategory INFORMATION = new MessageCategory(INFORMATION_VALUE, "INFORMATION_MESSAGE");

    public static MessageCategory getInstance(String messageCategory) {
        MessageCategory result = (MessageCategory) c_validTypes.get(messageCategory.toUpperCase());
        if (result == null) {
            throw new IllegalArgumentException("The messageCategory '" + messageCategory + "' is not a valid MessageCategory.");
        }
        return result;
    }

    public boolean isError() {
        return intValue() == ERROR_VALUE;
    }

    public boolean isWarning() {
        return intValue() == WARNING_VALUE;
    }

    public boolean isInformation() {
        return intValue() == INFORMATION_VALUE;
    }

    public MessageCategory(int value, String name) {
        super(value, name);
        c_validTypes.put(name, this);
    }


    public MessageCategory() {
    }
}
