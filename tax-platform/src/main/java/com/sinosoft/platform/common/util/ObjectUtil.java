/**
 * Copyright ®2015 sinosoft Co. Ltd. All rights reserved. 
 */
package com.sinosoft.platform.common.util;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.BeanUtils;

/**
 * 字符串操作工具类
 * 
 * @date 2010-11-19
 * @since JDK 1.5
 * @author dingkui
 * @version 1.0
 * 
 */
public class ObjectUtil {

    /**
     * 将orig中的同名属性复制到dest
     * 
     * @param dest
     *            源对象
     * @param orig
     *            目标对象
     * @return 复制结果
     */
    public static boolean fullCopy(Object dest, Object orig) {
        boolean resultValue = false;
        try {
            BeanUtils.copyProperties(dest, orig);
            resultValue = true;
        }
        catch (IllegalAccessException e) {
            resultValue = false;
            e.printStackTrace();
        }
        catch (InvocationTargetException e) {
            resultValue = false;
            e.printStackTrace();
        }
        return resultValue;
    }
}
