/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.platform.common.web.config<br/>
 * @FileName: CommonConfig.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.platform.common.web.config;

/**  
 * 【基础架构配置类】
 * @Description: 基础架构配置类
 * @author chenxin@sinosoft.com.cn
 * @date 2015年8月31日 上午11:10:32 
 * @version V1.0  
 */
public class CommonConfig {
	 /** sessionKey值  */
    public static final String MANAGE_USER_KEY = "user.sessionDTO";
}


