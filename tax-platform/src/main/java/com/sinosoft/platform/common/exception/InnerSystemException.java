/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.platform.common.exception<br/>
 * @FileName: InnerSystemException.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.platform.common.exception;

import com.sinosoft.platform.base.TaxBaseException;

/**  
 * 【系统内部异常】
 * @Description: TODO
 * @author Gavin
 * @date 2014-6-11 下午03:49:16 
 * @version V1.0  
 */
@SuppressWarnings("serial")
public class InnerSystemException extends TaxBaseException{
	public InnerSystemException(String msg){
		super(msg);
	}
}


