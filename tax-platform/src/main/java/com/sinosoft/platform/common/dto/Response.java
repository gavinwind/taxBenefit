package com.sinosoft.platform.common.dto;

/**
 * 
  *【类或接口功能描述】
  *
  * @author chenxin
  * @date  May 24, 2011 
  * @version 1.0
  *
  *【修改描述】
  *
  * @author chenxin
  * @date  May 24, 2011
 */
public class Response {

    /**
     * 处理结果
     */
    private TransResult transResult;

    /**
     * 返回实体
     */
    private Object object;

    /**
     * @return the transResult
     */
    public TransResult getTransResult() {
        return transResult;
    }

    /**
     * @param transResult the transResult to set
     */
    public void setTransResult(TransResult transResult) {
        this.transResult = transResult;
    }

    /**
     * @return the object
     */
    public Object getObject() {
        return object;
    }

    /**
     * @param object the object to set
     */
    public void setObject(Object object) {
        this.object = object;
    }

}
