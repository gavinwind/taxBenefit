package com.sinosoft.platform.common.util;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * JSON转换工具类
 * 
 * @date 2010-11-23
 * @since JDK 1.5
 * @author dingkui
 * @version 1.0
 * 
 */
public class JsonUtil {

    /**
     * 
     */
    public static ObjectMapper mapper;

    static {
        JsonUtil.mapper = new ObjectMapper();
    }

    /**
     * 将object转为json字符串
     * 
     * @param pObj
     *      参数
     * @throws IOException
     *      异常
     * @throws JsonMappingException
     *      异常
     * @throws JsonGenerationException
     *      异常
     * @return String
     */
    public static String writeValue(Object pObj) throws JsonGenerationException, JsonMappingException, IOException {
        return JsonUtil.mapper.writeValueAsString(pObj);
    }

    /**
     * 将content转为java object
     * 
     * @param <T>
     *      参数
     * @param pContent
     *      参数
     * @param pObj
     *      参数
     * @throws JsonParseException
     *      异常
     * @throws JsonMappingException
     *      异常
     * @throws IOException
     *      异常
     * @return Object
     */
    @SuppressWarnings("unchecked")
    public static <T> Object readValue(String pContent, Object pObj) throws JsonParseException, JsonMappingException,
            IOException {
        return JsonUtil.mapper.readValue(pContent, (Class<T>) pObj);
    }
}
