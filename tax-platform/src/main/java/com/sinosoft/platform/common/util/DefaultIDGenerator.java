package com.sinosoft.platform.common.util;

/**
 * 请求ID
 * 
 * @author WangL
 * 
 */
public class DefaultIDGenerator {
    /**
     * 
     * 生成请求ID
     * 
     * @return
     * @return String
     */
    public static String generateID() {
        StringBuffer sb = new StringBuffer();
        // current time stamp
        Long id = new Long(System.currentTimeMillis());
        sb.append(id.longValue());
        // random id
        Double randomId = new Double(Math.floor(Math.random() * 1000));
        sb.append(randomId.intValue());
        return sb.toString();
    }
}
