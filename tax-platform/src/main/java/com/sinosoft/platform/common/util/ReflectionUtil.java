package com.sinosoft.platform.common.util;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 反射工具
 * 
 * @author WangL
 * 
 */
public class ReflectionUtil {

    /**
     * 
      * map转DTO
      * 
      * @param source
      * @param target 
      * @return void
     */
    public static void objectToMap(Object source, Map target) {
        try {
            target.putAll(ClassInvokeUtil.initCommonDTO(source));
        }
        catch (Exception e) {
            throw new RuntimeException("fail to  mapToObject!");
        }
    }

    /**
     * 
      * dto转Map
      * 
      * @param source
      * @param target 
      * @return void
     */
    public static void mapToObject(Map source, Object target) {
        try {
            ClassInvokeUtil.objectToMap(source, target);
        }
        catch (Exception e) {
            throw new RuntimeException("fail to  objectToMap!");
        }
    }

    /**
     * 把source中的内容Copy到指定target
     * @param src
     * @param target
     */
    public static void copyProperties(Object source, Object target) {
        copyProperties(source, target, null);
    }

    /**
     * 把src中的内容Copy到指定target，但是忽略指定的属性
     * 
     * @param source 源对象
     * @param target 目标对象
     * @param ignoreProperties 指定忽略的属性
     */
    @SuppressWarnings("unchecked")
	public static void copyProperties(Object source, Object target, String[] ignoreProperties) {
        if (null == source || null == target) {
            return;
        }
        Set excludes = new HashSet();
        if (null != ignoreProperties) {
            excludes.addAll(Arrays.asList(ignoreProperties));
        }
        copyProperties(source, target, excludes, false);
    }

    /**
     * 
     * 方法功能描述
     * 
     * @param source 源对象
     * @param target 目标对象
     * @param properties 参数
     * @param included 参数
     */

    private static void copyProperties(Object source, Object target, Set properties, boolean included) {
        try {
            BeanInfo sourceInfo = Introspector.getBeanInfo(source.getClass());
            Map targetDescrs = new HashMap();
            // init
            {
                BeanInfo targetInfo = Introspector.getBeanInfo(target.getClass());
                PropertyDescriptor[] targetPds = targetInfo.getPropertyDescriptors();
                for (int i = 0; i < targetPds.length; i++) {
                    targetDescrs.put(targetPds[i].getName(), targetPds[i]);
                }
            }
            PropertyDescriptor[] pds = sourceInfo.getPropertyDescriptors();
            Object[] params = new Object[1];

            // collect some stat info for debug later...

            for (int i = 0; i < pds.length; i++) {
                String property = pds[i].getName();
                if (included) {
                    if (!properties.contains(property)) {
                        continue;
                    }
                }
                else {
                    if (properties.contains(property)) {
                        continue;
                    }
                }
                PropertyDescriptor targetPD = (PropertyDescriptor) targetDescrs.get(property);
                if (null != targetPD) {
                    Method readMethod = pds[i].getReadMethod();
                    Method writeMethod = targetPD.getWriteMethod();
                    if (null == readMethod || null == writeMethod) {
                        // warning: no read or write
                    }
                    else {
                        try {
                            Class[] ps = writeMethod.getParameterTypes();
                            if (ps == null || ps.length != 1) {
                                continue;
                            }
                            Object [] param = null;
                            Object propSrc = readMethod.invoke(source, param);
                            params[0] = convertIfNeeded(ps[0], propSrc, pds[i].getName());
                            writeMethod.invoke(target, params);
                        }
                        catch (IllegalArgumentException e) {
                            // warning: failed read or write
                        }
                        catch (IllegalAccessException e) {
                            // warning: failed read or write
                        }
                        catch (InvocationTargetException e) {
                            // warning: failed read or write
                        }
                    }
                }
                else {
                    // warning: no target property
                }
            }
        }
        catch (IntrospectionException e) {
            handleEx(e);
        }
    }

    /**
     * 设置指定对象的属性
     * 
     * @param target 对象
     * @param property 属性名
     * @param value 新的属性值
     * @throws RuntimeException
     */
    public static void setProperty(Object target, String property, Object value) {
        try {
            BeanInfo beanInfo = Introspector.getBeanInfo(target.getClass());
            PropertyDescriptor[] pds = beanInfo.getPropertyDescriptors();
            for (int i = 0; i < pds.length; i++) {
                if (pds[i].getName().equals(property)) {
                    Method method = pds[i].getWriteMethod();
                    if (null == method) {
                        throw new IllegalArgumentException("No WriteMethod of property: " + property);
                    }
                    Class[] ps = method.getParameterTypes();
                    if (ps == null || ps.length != 1) {
                        continue;
                    }
                    Object[] params = new Object[1];
                    // method.invoke(source, params);
                    params[0] = convertIfNeeded(ps[0], value, pds[i].getName());
                    method.invoke(target, params);

                    return;
                }
            }
            throw new IllegalArgumentException("No Such property: " + property);
        }
        catch (IntrospectionException e) {
            handleEx(e);
        }
        catch (IllegalAccessException e) {
            handleEx(e);
        }
        catch (InvocationTargetException e) {
            handleEx(e);
        }

        throw new RuntimeException("No Such property: " + property);
    }

    /**
     * 
     * 方法功能描述
     * 
     * @param type 参数
     * @param value 参数
     * @param property 参数
     * @return Object 返回
     */
    private static Object convertIfNeeded(Class type, Object value, String property) {
        if (null != value) {
            if (value instanceof Number) {
                return convertNumber(type, (Number) value);
            }
            return value;
        }
        // 处理原始类型
        if (Integer.class == type || Integer.TYPE == type) {
            return new Integer(0);
        }
        if (Short.class == type || Short.TYPE == type) {
            return new Short((short) 0);
        }
        if (Long.class == type || Long.TYPE == type) {
            return new Long(0);
        }
        if (Float.class == type || Float.TYPE == type) {
            return new Float(0);
        }
        if (Double.class == type || Double.TYPE == type) {
            return new Double(0);
        }
        if (Byte.class == type || Byte.TYPE == type) {
            return new Integer(0);
        }
        if (Character.class == type || Character.TYPE == type) {
            return new Character('\0');
        }
        if (Boolean.class == type || Boolean.TYPE == type) {
            return Boolean.FALSE;
        }

        return value;
    }

    private static Number convertNumber(Class destType, Number value) {
        if (destType == BigDecimal.class) {
            return new BigDecimal(value.toString());
        }
        if (destType == BigInteger.class) {
            return new BigInteger(value.toString());
        }
        if (destType == Long.class || destType == Long.TYPE) {
            return new Long(value.longValue());
        }
        if (destType == Integer.class || destType == Integer.TYPE) {
            return new Integer(value.intValue());
        }
        if (destType == Short.class || destType == Short.TYPE) {
            return new Short(value.shortValue());
        }
        if (destType == Float.class || destType == Float.TYPE) {
            return new Float(value.floatValue());
        }
        if (destType == Double.class || destType == Double.TYPE) {
            return new Double(value.doubleValue());
        }
        if (destType == Byte.class || destType == Byte.TYPE) {
            return new Byte(value.byteValue());
        }

        return value;
    }

    /**
     * 
     * 方法功能描述
     * 
     * @param ex 异常
     */
    private static void handleEx(IntrospectionException ex) {
        throw new RuntimeException("Access Error", ex);
    }

    /**
     * 
     * 方法功能描述
     * 
     * @param ex 异常
     */
    private static void handleEx(IllegalAccessException ex) {
        throw new RuntimeException("Access Error", ex);
    }

    /**
     * 
     * 方法功能描述
     * 
     * @param ex 异常
     */
    private static void handleEx(InvocationTargetException ex) {
        throw new RuntimeException("Invocation Error", ex);
    }
    /**
     * 直接调用对象方法, 无视private/protected修饰符.
     * 
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     */
    public static Object invokeMethod(final Object object, final String methodName, final Class < ? >[] parameterTypes,
            final Object[] parameters) throws Exception {
        Method method = getDeclaredMethod(object, methodName, parameterTypes);
        if (method == null) {
            throw new IllegalArgumentException("Could not find method [" + methodName + "] on target [" + object + "]");
        }
        method.setAccessible(true);
        return method.invoke(object, parameters);
    }
    /**
     * 循环向上转型, 获取对象的DeclaredMethod.
     * 
     * 如向上转型到Object仍无法找到, 返回null.
     */
    protected static Method getDeclaredMethod(Object object, String methodName, Class < ? >[] parameterTypes) {
        if (object == null) {
            return null;
        }
        for (Class < ? > superClass = object.getClass(); superClass != Object.class; superClass = superClass
                .getSuperclass()) {
            try {
                return superClass.getDeclaredMethod(methodName, parameterTypes);
            }
            catch (NoSuchMethodException e) {// NOSONAR
                // Method不在当前类定义,继续向上转型
            }
        }
        return null;
    }

}
