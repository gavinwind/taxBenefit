/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.platform.common.exception<br/>
 * @FileName: SignErrException.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.platform.common.exception;

import com.sinosoft.platform.base.TaxBaseException;

/**  
 * 【签名错误异常】
 * @Description: TODO
 * @author Gavin
 * @date 2014-6-30 下午02:36:57 
 * @version V1.0  
 */
@SuppressWarnings("serial")
public class SignErrException extends TaxBaseException{
	public SignErrException(String msg){
		super(msg);
	}
}


