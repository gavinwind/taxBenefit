/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.platform.common.exception<br/>
 * @FileName: WebserviceInvokeException.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.platform.common.exception;

import com.sinosoft.platform.base.TaxBaseException;

/**  
 * 【WebService调用异常】
 * @Description: WebService调用异常
 * @author chenxin@sinosoft.com.cn
 * @date 2015年8月26日 上午10:40:38 
 * @version V1.0  
 */
public class WebserviceInvokeException extends TaxBaseException{
	/** 
	* @Fields serialVersionUID : TODO
	*/
	private static final long serialVersionUID = -5493587986074002920L;

	public WebserviceInvokeException(String msg){
		super(msg);
	}
}

