package com.sinosoft.platform.common.web.pagequery;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("pageGrid")
public class PageGrid {

    /**
     * 当前页
     */
    private int page;

    /**
     * 总条数
     */
    private int total;

    /**
     * 总页数
     */
    private int records;

    /**
     * 查询数据
     */
    private List rows;
    
    /** 
    * 每页多少条
    */
    private int size;

    /**
     * @return the page
     */
    public int getPage() {
        return page;
    }
    
    /**
     * @param page the page to set
     */
    public void setPage(int page) {
        this.page = page;
    }

    /**
     * @return the total
     */
    public int getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(int total) {
        this.total = total;
    }

	/**
	 * @return the records
	 */
	public int getRecords() {
		return records;
	}

	/**
	 * @param records the records to set
	 */
	public void setRecords(int records) {
		this.records = records;
	}

	/**
	 * @return the rows
	 */
	public List getRows() {
		return rows;
	}

	/**
	 * @param rows the rows to set
	 */
	public void setRows(List rows) {
		this.rows = rows;
	}

	/**
	 * @return the size
	 */
	public int getSize() {
		return size;
	}

	/**
	 * @param size the size to set
	 */
	public void setSize(int size) {
		this.size = size;
	}

	
}
