package com.sinosoft.platform.common.web.pagequery;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * 【分页查询】
 * 
 * @author Chenxin
 * @date July 31, 2015
 * @version 1.0
 * 
 * 【修改描述】
 * 
 * @author Chenxin
 * @date July 31, 2015
 */
@SuppressWarnings("unchecked")
public class PageQueryModel {

    /**
     * 
     */
    public static final String COUNT_ALL_NAME = "COUNT";

    /**
     * 
     */
    private boolean doCount = true;

    /**
     * 
     */
    private boolean queryAll = false;

    /**
     * 
     */
    private int startPosition = 0;

    /**
     * 
     */
    private int pageSize = 20;

    /**
     * 
     */
    private int currentPage = 1;

    /**
     * 
     */
    private String sortCol = "";

    /**
     * 
     */
    private Map queryParam = new HashMap();
    
    /** 
    * 每页显示条数
    */
    public static int PAGE_SIZE = 5;

    /**
     * 
     * 方法功能描述
     * 
     * @return 
     * @return boolean
     */
    public boolean isDoCount() {
        return doCount;
    }

    /**
     * 
     * 方法功能描述
     * 
     * @return 
     * @return boolean
     */
    public boolean isQueryAll() {
        return queryAll;
    }

    /**
     * 
     * 方法功能描述
     * 
     * @return 
     * @return int
     */
    public int getFirstCursorPosition() {
        return startPosition;
    }

    /**
     * 
     * 方法功能描述
     * 
     * @return 
     * @return int
     */
    public int getLastCursorPosition() {
        return (startPosition + pageSize) - 1;
    }

    /**
     * 
     * 方法功能描述
     * 
     * @param page 
     *          当前页
     */
    public void setCurrentPage(int page) {
        if (page < 1) {
            currentPage = 1;
            startPosition = 0;
            return;
        }
        else {
            currentPage = page;
            startPosition = pageSize * (page - 1) ;
            return;
        }
    }

    /**
     * 
     * 方法功能描述
     * 
     * @return 
     * @return int
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * 
     * 方法功能描述
     * 
     * @param pageSize 
     *          分页数量
     */
    public void setPageSize(int pageSize) {
        if (0 >= pageSize) {
            pageSize = 1;
        }
        this.pageSize = pageSize;
        startPosition = pageSize * (currentPage - 1) ;
    }

    /**
     * 
     * 方法功能描述
     * 
     * @return 
     * @return int
     */
    public int getStartPosition() {
        return startPosition;
    }

    /**
     * 
     * 方法功能描述
     * 
     * @param startPosition 
     *             参数
     */
    public void setStartPosition(int startPosition) {
        this.startPosition = startPosition;
    }

    /**
     * 
     * 方法功能描述
     * 
     * @param doCount 
     *        參數
     */
    public void setDoCount(boolean doCount) {
        this.doCount = doCount;
    }

    /**
     * 
     * 方法功能描述
     * 
     * @param queryAll 
     *           參數
     */
    public void setQueryAll(boolean queryAll) {
        this.queryAll = queryAll;
    }

    /**
     * 
     * 方法功能描述
     * 
     * @return 
     * @return String
     */
    public String getQueryAll() {
        return String.valueOf(queryAll);
    }

    /**
     * 
     * 方法功能描述
     * 
     * @return 
     * @return String
     */
    public String getDoCount() {
        return String.valueOf(doCount);
    }

    /**
     * 
     * 方法功能描述
     */
    public void nextPage() {
        setCurrentPage(getCurrentPage() + 1);
    }

    /**
     * 
     * 方法功能描述
     */
    public void prePage() {
        setCurrentPage(getCurrentPage() - 1);
    }

    /**
     * 
     * 方法功能描述
     * 
     * @return 
     * @return int
     */
    public int getCurrentPage() {
        return currentPage;
    }

    /**
     * 
      * 方法功能描述
      * 
      * @return 
      * @return Map
     */
    public Map getQueryParam() {
        return queryParam;
    }

    /**
     * 
      * 方法功能描述
      * 
      * @return 
      * @return String
     */
    public String getSortCol() {
        return sortCol;
    }

    /**
     * 
      * 方法功能描述
      * 
      * @param sortCol 
     */
    public void setSortCol(String sortCol) {
        this.sortCol = sortCol;
    }


    /**
     * 
      * 方法功能描述
      * 
      * @param queryParam 
      *        查询条件
     */
    public void setQueryParam(Map queryParam) {
        this.queryParam = queryParam;
    }

    /**
     * 
      * 方法功能描述
      * 
      * @param key
      *         key值
      * @param value 
      *         value值
     */
    public void addParameter(Object key, Object value) {
        this.queryParam.put(key, value);
    }

}
