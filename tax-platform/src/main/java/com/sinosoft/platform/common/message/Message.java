package com.sinosoft.platform.common.message;

import org.springframework.context.support.DefaultMessageSourceResolvable;

/**
 * 
  *【类或接口功能描述】
  *
  * @author WL
  * @date  2011-7-12 
  * @version 1.0
  *
  *【修改描述】
  *
  * @author WL
  * @date  2011-7-12
 */
public class Message extends DefaultMessageSourceResolvable {

  private MessageCategory messageCategory;
  private String fieldId;
  private Object rejectedValue;

  public Message(String[] codes, Object[] arguments, String defaultMessage) {
    super(codes, arguments, defaultMessage);
  }

  public Message(MessageCategory messageCategory, String messageKey,
      Object[] arguments, String fieldId) {
    super(new String[] { messageKey }, arguments, messageKey);
    this.messageCategory = messageCategory;
    this.fieldId = fieldId;
  }  
  
  public Message(MessageCategory messageCategory, String messageKey,
      Object[] argument, String fieldId, Object rejectedValue) {
    super(new String[] { messageKey }, argument, messageKey);
    this.messageCategory = messageCategory;
    this.fieldId = fieldId;
    this.rejectedValue = rejectedValue;
  }

  public MessageCategory getMessageCategory() {
    return messageCategory;
  }

  public void setMessageCategory(MessageCategory messageCategory) {
    this.messageCategory = messageCategory;
  }

  public String getFieldId() {
    return fieldId;
  }

  public void setFieldId(String fieldId) {
    this.fieldId = fieldId;
  }

  public Object getRejectedValue() {
    return rejectedValue;
  }

  public void setRejectedValue(Object rejectedValue) {
    this.rejectedValue = rejectedValue;
  }

}
