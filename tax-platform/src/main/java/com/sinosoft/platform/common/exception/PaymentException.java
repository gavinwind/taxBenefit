/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.platform.common.exception<br/>
 * @FileName: PaymentException.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.platform.common.exception;

import com.sinosoft.platform.base.TaxBaseException;

/**  
 * 【支付业务异常】
 * @Description: TODO
 * @author Gavin@sinosoft.com.cn
 * @date 2014-6-27 上午08:06:11 
 * @version V1.0  
 */
public class PaymentException extends TaxBaseException{

}


