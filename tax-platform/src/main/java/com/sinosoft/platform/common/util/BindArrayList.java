package com.sinosoft.platform.common.util;

import java.util.ArrayList;

public class BindArrayList<T> extends ArrayList {
    
    private Class clazz = null;

    public BindArrayList(Class clazz) {
        this.clazz = clazz;
    }

    public Object get(int index) {
        try {
            while (index >= size()) {
                add(clazz.newInstance());
            }
        }
        catch (Exception e) {
            throw new RuntimeException("AutoInitArrayList Error", e);
        }
        return super.get(index);
    }
}
