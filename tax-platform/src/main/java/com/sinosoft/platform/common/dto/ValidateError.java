/**
 * Copyright ®2015 sinosoft Co. Ltd. All rights reserved. Package: com.focoon.eagle.platform.common.api.model FileName:
 * ValidateError.java Description: 验证错误实体基类
 */
package com.sinosoft.platform.common.dto;

/**
 * 验证错误实体基类
 * 
 * @author chenxin
 * @date 2015/07/12
 * @version 1.0
 */
public class ValidateError {

    /**
     * 错误代码
     */
    private String code;

    /**
     * 错误信息
     */
    private String message;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

}
