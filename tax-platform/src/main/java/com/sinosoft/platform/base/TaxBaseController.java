/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.platform.base<br/>
 * @FileName: HXBaseController.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.platform.base;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sinosoft.platform.common.dto.Response;
import com.sinosoft.platform.common.util.StringUtil;
import com.sinosoft.platform.common.util.XStreamComponent;
import com.sinosoft.platform.common.web.pagequery.PageGrid;
import com.sinosoft.platform.common.web.pagequery.PageQueryModel;


/**  
 * 【Controller基础类】
 * @Description: TODO
 * @author Gavin
 * @date 2014-5-7 上午10:47:10 
 * @version V1.0  
 */
@SuppressWarnings("unchecked")
public class TaxBaseController {
	/** */
	protected final Logger logger = Logger.getLogger(getClass());
	
    /**
     * 【类或接口功能描述】解析json的类型
     * @Description: TODO
     * @author chenxin
     * @date 2014-5-15 上午10:58:24 
     * @version V1.0
     */
    public enum JsonType {
		JSON_OBJECT, JSON_ARRAY
	}
	/**
	 * 方法功能描述
	 * @param binder 参数
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, false));
	}

	protected ModelAndView prepareModelAndView(String view) throws Exception {
		// 此方法作用为初始化页面元素
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName(view);
		return modelAndView;
	}

	/**
	 * 方法功能描述
	 * @param res  参数
	 * @param xmlResponse  参数
	 */
	protected void renderPageGrid(HttpServletResponse res, PageGrid pageGrid) {
		Response rsponse = new Response();
		rsponse.setObject(pageGrid);
		XStreamComponent xstreamComponent = XStreamComponent.newInstance();
		xstreamComponent.alias(new Class[] { PageQueryModel.class,
				Response.class, PageGrid.class });
		String xmlResponse = xstreamComponent.toPageXML(rsponse);
		renderXml(res, xmlResponse);
	}

	/**
	 * 方法功能描述
	 * @param res  参数
	 * @param xmlResponse   参数
	 */
	protected void renderXml(HttpServletResponse res, String xmlResponse) {
		try {
			res.setCharacterEncoding("UTF-8");
			res.setHeader("Content-type", "text/xml");
			res.getWriter().print(xmlResponse);
		} catch (IOException ex) {
			logger.error(ex);
		}
	}

	/**
	 * 分页查询初始化分页相关基础查询参数
	 * @Title: getPageQueryModel 
	 * @Description: TODO
	 * @param req
	 * @return
	 */
	protected PageQueryModel getPageQueryModel(HttpServletRequest req){
		PageQueryModel pageQueryModel = new PageQueryModel();
    	int currentPage = StringUtil.parseStringToInt(req.getParameter("current"));
    	pageQueryModel.setPageSize(PageQueryModel.PAGE_SIZE);
    	pageQueryModel.setCurrentPage(currentPage);
    	return pageQueryModel;
	}



	protected Map<String,Object> getRequestValue(HttpServletRequest request) {
		Map<String,Object> paraMap = new HashMap<String,Object>();
		Enumeration<String> enume = request.getParameterNames();
		while (enume.hasMoreElements()) {
			String key = enume.nextElement();
			String value = request.getParameter(key);
			paraMap.put(key, value);
		}
		return paraMap;
	}
	
	/**
	 * 
	 * 方法功能描述 向页面响应json数据
	 * @Title: renderJsonData 
	 * @Description: TODO
	 * @author xupengfei@focoom.com.cn
	 * @param data 要解析的Java对象
	 * @param type 要解析的json类型
	 * @param request http请求对象
	 * @param response http响应对象
	 * @throws Exception
	 */
	protected void renderJsonData(Object data, JsonType type,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String strJson = type == JsonType.JSON_OBJECT ? JSONObject.toJSONString(
				data).toString() : JSONArray.toJSONString(data);
 		PrintWriter out = response.getWriter();
 		out.print(strJson);
 		System.err.println(strJson);
 		out.flush();
 		out.close();
	}
	
	protected void renderJsonData(Object data, JsonType type, String content,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String strJson = type == JsonType.JSON_OBJECT ? JSONObject.toJSONString(data) : JSONArray.toJSON(data).toString();
		response.setContentType("text/" + content + "; charset=utf-8");
 		response.setHeader("cache-control", "no-cache"); 
 		PrintWriter out = response.getWriter();
 		out.print(strJson);
 		out.flush();
 		out.close();
	}
	
}