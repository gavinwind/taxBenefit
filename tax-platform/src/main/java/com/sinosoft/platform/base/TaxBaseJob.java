/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.platform.components.quartz.job<br/>
 * @FileName: HXBaseJob.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.platform.base;

import java.io.IOException;

import javax.mail.MessagingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.lowagie.text.DocumentException;

/**  
 * 【定时任务基础类】
 * @Description: TODO
 * @author chenxin@sinosoft.com.cn
 * @date 2015-7-31 11:09:53 
 * @version V1.0  
 */
public abstract class TaxBaseJob {
	
	protected final Log logger = LogFactory.getLog(getClass());
	
	public abstract void executeJob() throws IOException, DocumentException, MessagingException;
}


