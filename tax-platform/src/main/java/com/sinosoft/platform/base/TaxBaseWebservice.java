/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.platform.base<br/>
 * @FileName: HxBaseWebservice.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.platform.base;
import org.apache.log4j.Logger;
/**  
 * 【WEBSERVICE基类】
 * @Description: WEBSERVICE基类
 * @author chenxin@sinosoft.com.cn
 * @date 2015年8月23日 下午9:28:54 
 * @version V1.0  
 */
public class TaxBaseWebservice {
	/** */
	protected final Logger logger = Logger.getLogger(getClass());
}


