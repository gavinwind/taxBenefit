/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.platform.base<br/>
 * @FileName: HXBaseAction.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.platform.base;

import org.apache.log4j.Logger;


/**  
 * 【Action基础类】
 * @Description: TODO
 * @author Gavin
 * @date 2014-5-7 上午11:00:37 
 * @version V1.0  
 */
public abstract class TaxBaseAction {
	/** */
	protected final Logger logger = Logger.getLogger(getClass());
}


