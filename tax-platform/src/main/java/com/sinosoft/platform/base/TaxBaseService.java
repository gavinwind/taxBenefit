/**
 * @Copyright ®2012 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.platform.base<br/>
 * @FileName: HXBaseService.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.platform.base;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**  
 * 【类或接口功能描述】
 * @Description: TODO
 * @author Gavin@focoom.com.cn
 * @date 2014-5-11 上午10:45:35 
 * @version V1.0  
 */
public class TaxBaseService {
	/**
     * logger of log4j
     */
    protected final Log logger = LogFactory.getLog(getClass());
}


