package com.sinosoft.platform.base;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@ContextConfiguration(locations = {"classpath:/test/config/common-context.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class TaxBaseTest extends AbstractTransactionalJUnit4SpringContextTests
{
	
}
