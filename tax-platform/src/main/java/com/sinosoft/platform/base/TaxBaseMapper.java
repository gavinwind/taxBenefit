package com.sinosoft.platform.base;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mybatis.spring.SqlSessionTemplate;

import com.sinosoft.platform.common.web.pagequery.PageGrid;
import com.sinosoft.platform.common.web.pagequery.PageQueryModel;

/**  
 * 【系统dao基类】
 * @Description: TODO
 * @author chenxin@sinosoft.com.cn
 * @date 2014-5-8 下午04:26:35 
 * @version V1.0  
 */
public class TaxBaseMapper {
	/** sql模版类*/
	private SqlSessionTemplate sqlSessionTemplate;

	public SqlSessionTemplate getSqlSessionTemplate() {
		return sqlSessionTemplate;
	}
	@Resource(name = "sqlSession")
	public void setSqlSessionTemplate(SqlSessionTemplate sqlSessionTemplate) {
		this.sqlSessionTemplate = sqlSessionTemplate;
	}
	
	/**
     * logger of log4j
     */
    protected final Log logger = LogFactory.getLog(getClass());
    
    /**
     * 插入数据到数据库
     * @param statementName SQL名称
     * @param object  参数
     * @return
     * @return Object
     */
    protected Object saveObject(String statementName, Object object) {
        return this.getSqlSessionTemplate().insert(statementName, object);
    }

    /**
     * 更新数据到数据库
     * @param statementName  SQL名称
     * @param object   参数
     * @return
     * @return Object
     */
    protected int updateObject(String statementName, Object object) {
        return this.getSqlSessionTemplate().update(statementName, object);
    }


    /**
     * 删除数据库数据
     * @param statementName  SQL名称
     * @return Object
     */
    protected int deleteObject(String statementName) {
        return this.getSqlSessionTemplate().delete(statementName);
    }

    /**
     * 删除数据库数据
     * @param statementName  SQL名称
     * @param parameterObject  参数        
     * @return Object
     */
    protected int deleteObject(String statementName, Object parameterObject) {
        return this.getSqlSessionTemplate().delete(statementName, parameterObject);
    }

    /**
     * 查询数据返回List数据集
     * @param statementName   SQL名称
     * @return Object
     */
	protected List<Object> queryForList(String statementName) {
        return this.getSqlSessionTemplate().selectList(statementName);
    }

    /**
     * 查询数据返回List数据集
     * @param statementName  SQL名称
     * @param parameterObject  参数        
     * @return Object
     */

	protected List<Object> queryForList(String statementName, Object parameterObject) {
        //setSqlCondition(statementName, parameterObject);
        return this.getSqlSessionTemplate().selectList(statementName, parameterObject);
    }

    /**
     * 查询数据返回Object数据
     * @param statementName   SQL名称
     * @return Object
     */
    protected Object queryForObject(String statementName) {
        return this.getSqlSessionTemplate().selectOne(statementName);
    }

    /**
     * 查询数据返回Object数据
     * 
     * @param statementName 
     *          SQL名称
     * @param parameterObject    
     *          参数        
     * @return
     * @return Object
     */
    protected Object queryForObject(String statementName, Object parameterObject) {
        //setSqlCondition(statementName, parameterObject);
        return this.getSqlSessionTemplate().selectOne(statementName, parameterObject);
    }

    /**
     * 分页查询方法
     * @param statementName  SQL名称
     * @param parameterObject    参数        
     * @return Object
     */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected PageGrid pageForObject(String statementName, PageQueryModel parameterObject) {
        //setSqlCondition(statementName, parameterObject);

        int total = 0;
        PageGrid pageGrid = new PageGrid();
        PageQueryModel queryModel = parameterObject;
        if (queryModel == null) {
            queryModel = new PageQueryModel();
            // 设置查询全部
            queryModel.setQueryAll(true);
            // 不进行数量查询
            queryModel.setDoCount(false);
        }
        Map queryParam = new HashMap();
        if (queryModel.getQueryParam() != null) {
            queryParam = queryModel.getQueryParam();
        }
        queryParam.put("doCount", queryModel.getDoCount());
        queryParam.put("queryAll", queryModel.getQueryAll());
        queryParam.put("lastCursorPosition", queryModel.getLastCursorPosition());
        queryParam.put("firstCursorPosition", queryModel.getFirstCursorPosition());
        queryParam.put("pageSize", queryModel.getPageSize());
        if (queryModel.isDoCount()) {
        	total = (Integer) this.getSqlSessionTemplate().selectOne(statementName + "Sum", queryParam); //可以优化为常量
        }
        // 设置总条数
//        pageGrid.setRecords(records);
        pageGrid.setTotal(total);
        // 查询数据
        queryModel.setDoCount(false);
        queryParam.put("RESULT_COUNT", PageQueryModel.COUNT_ALL_NAME);
        queryParam.put("orderByColums", queryModel.getSortCol());
        List data = this.queryForList(statementName, queryParam);
        pageGrid.setRows(data);
        pageGrid.setPage(queryModel.getCurrentPage());
        if(parameterObject.getPageSize()!=5){
        	pageGrid.setSize(parameterObject.getPageSize());
        }else{
        	pageGrid.setSize(PageQueryModel.PAGE_SIZE);
        }
        // 如果查询全部
        if (!queryModel.isQueryAll()) {
            // 设置总页数
            int pageSize = queryModel.getPageSize();
            int Records = 0;
            if (total != 0) {
            	Records = (total % pageSize) == 0 ? (total / pageSize) : (total / pageSize + 1);
            }
            pageGrid.setRecords(Records);
            // 设置当前页
            int currentPage = queryModel.getCurrentPage();
            pageGrid.setPage(currentPage);
        }
        return pageGrid;
    }


}
