/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.platform.base<br/>
 * @FileName: HXBaseException.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.platform.base;

import org.apache.log4j.Logger;

/**  
 * 【基础异常类】
 * @Description: 系统的基础异常 ,继承次异常的类 可以进行事务回滚
 * @author chenxin
 * @date 2014-6-15 下午06:25:33 
 * @version V1.0  
 */
@SuppressWarnings("serial")
public abstract class TaxBaseException extends RuntimeException{
	/** 日志记录工具 */
	protected final Logger logger = Logger.getLogger(getClass());
	
	public TaxBaseException (String msg){
		super(msg);
	}
	public TaxBaseException (){
		super();
	}
    /**
     * @param message
     * @param cause
     */
    public TaxBaseException(String message, Throwable cause) {
        super(message, cause);
    }

}


