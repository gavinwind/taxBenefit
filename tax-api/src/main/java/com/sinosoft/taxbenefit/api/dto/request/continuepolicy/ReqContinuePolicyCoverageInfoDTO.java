package com.sinosoft.taxbenefit.api.dto.request.continuepolicy;

import java.math.BigDecimal;
import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * Title:核心-险种信息
 * @author yangdongkai@outlook.com
 * @date 2016年3月2日 上午10:06:31
 */

public class ReqContinuePolicyCoverageInfoDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1963689795615635971L;
	// 公司产品组代码
	private String coveragePackageCode;
	// 公司险种代码
	private String comCoverageCode;
	// 险种生效日期
	private String coverageEffectiveDate;
	// 险种期满日期
	private String coverageExpireDate;
	// 险种保额
	private BigDecimal insuranceCoverage;
	// 险种保费
	private BigDecimal premium;
	// 责任信息
	private List<ReqContinuePolicyLiabilityInfoDTO> liabilityList;
	// 核保信息
	private ReqContinuePolicyUwinfoInfoDTO underwriting;
	/**
	 * @return the underwriting
	 */
	public ReqContinuePolicyUwinfoInfoDTO getUnderwriting() {
		return underwriting;
	}

	/**
	 * @param underwriting the underwriting to set
	 */
	public void setUnderwriting(ReqContinuePolicyUwinfoInfoDTO underwriting) {
		this.underwriting = underwriting;
	}

	/**
	 * @return the liabilityList
	 */
	public List<ReqContinuePolicyLiabilityInfoDTO> getLiabilityList() {
		return liabilityList;
	}
	
	/**
	 * @return the insuranceCoverage
	 */
	public BigDecimal getInsuranceCoverage() {
		return insuranceCoverage;
	}

	/**
	 * @param insuranceCoverage the insuranceCoverage to set
	 */
	public void setInsuranceCoverage(BigDecimal insuranceCoverage) {
		this.insuranceCoverage = insuranceCoverage;
	}

	/**
	 * @param liabilityList the liabilityList to set
	 */
	public void setLiabilityList(List<ReqContinuePolicyLiabilityInfoDTO> liabilityList) {
		this.liabilityList = liabilityList;
	}
	
	/**
	 * @return the coveragePackageCode
	 */
	public String getCoveragePackageCode() {
		return coveragePackageCode;
	}
	/**
	 * @param coveragePackageCode the coveragePackageCode to set
	 */
	public void setCoveragePackageCode(String coveragePackageCode) {
		this.coveragePackageCode = coveragePackageCode;
	}
	/**
	 * @return the comCoverageCode
	 */
	public String getComCoverageCode() {
		return comCoverageCode;
	}
	/**
	 * @param comCoverageCode the comCoverageCode to set
	 */
	public void setComCoverageCode(String comCoverageCode) {
		this.comCoverageCode = comCoverageCode;
	}
	/**
	 * @return the coverageEffectiveDate
	 */
	public String getCoverageEffectiveDate() {
		return coverageEffectiveDate;
	}
	/**
	 * @param coverageEffectiveDate the coverageEffectiveDate to set
	 */
	public void setCoverageEffectiveDate(String coverageEffectiveDate) {
		this.coverageEffectiveDate = coverageEffectiveDate;
	}
	/**
	 * @return the coverageExpireDate
	 */
	public String getCoverageExpireDate() {
		return coverageExpireDate;
	}
	/**
	 * @param coverageExpireDate the coverageExpireDate to set
	 */
	public void setCoverageExpireDate(String coverageExpireDate) {
		this.coverageExpireDate = coverageExpireDate;
	}
	/**
	 * @return the premium
	 */
	public BigDecimal getPremium() {
		return premium;
	}
	/**
	 * @param premium the premium to set
	 */
	public void setPremium(BigDecimal premium) {
		this.premium = premium;
	}
}
