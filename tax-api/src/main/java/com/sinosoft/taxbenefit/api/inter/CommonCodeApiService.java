package com.sinosoft.taxbenefit.api.inter;

/**
 * 公用字典接口
 * @author zhangke
 *
 */
public interface CommonCodeApiService {
	String queryCommonCodeInfo(String sysCode,String codeType);
}
