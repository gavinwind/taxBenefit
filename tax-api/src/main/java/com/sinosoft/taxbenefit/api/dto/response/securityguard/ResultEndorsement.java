package com.sinosoft.taxbenefit.api.dto.response.securityguard;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

public class ResultEndorsement extends TaxBaseApiDTO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3512339278682326787L;
	/** 业务号 **/
	private String bizNo;
//	/** 结果编码 **/
//	private String resultCode;
//	/** 结果描述 **/
//	private String message;
	/** 保全编码 **/
	private String endorsementSequenceNo;
	/** 原投保人编码 **/
	private String customerNo;
	/** 新投保人编码 **/
	private String customerNoNew;
	/** 税优提示 **/
	private String taxWarning;
	private List<ResultInsured> insuredList;

	
	public String getBizNo() {
		return bizNo;
	}


	public List<ResultInsured> getInsuredList() {
		return insuredList;
	}


	public void setInsuredList(List<ResultInsured> insuredList) {
		this.insuredList = insuredList;
	}


	public void setBizNo(String bizNo) {
		this.bizNo = bizNo;
	}

//	public String getResultCode() {
//		return resultCode;
//	}
//
//	public void setResultCode(String resultCode) {
//		this.resultCode = resultCode;
//	}
//
//	public String getMessage() {
//		return message;
//	}
//
//	public void setMessage(String message) {
//		this.message = message;
//	}

	public String getEndorsementSequenceNo() {
		return endorsementSequenceNo;
	}

	public void setEndorsementSequenceNo(String endorsementSequenceNo) {
		this.endorsementSequenceNo = endorsementSequenceNo;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getCustomerNoNew() {
		return customerNoNew;
	}

	public void setCustomerNoNew(String customerNoNew) {
		this.customerNoNew = customerNoNew;
	}

	public String getTaxWarning() {
		return taxWarning;
	}

	public void setTaxWarning(String taxWarning) {
		this.taxWarning = taxWarning;
	}

}
