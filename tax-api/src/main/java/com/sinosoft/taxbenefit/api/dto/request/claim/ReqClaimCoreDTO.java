package com.sinosoft.taxbenefit.api.dto.request.claim;

import com.sinosoft.taxbenefit.api.dto.request.base.BaseRequestTax;
/**
 * (核心)理赔上传请求dto(单笔)(最外层)
 * @author zhangke
 *
 */
public class ReqClaimCoreDTO extends BaseRequestTax{
	/**
	 * 
	 */
	private static final long serialVersionUID = 254238036593995845L;
	/** 报文体信息 */
	private ReqClaimInfoDTO body;
	
	public void setBody(ReqClaimInfoDTO body){
		this.body= body;
	}
	@Override
	public Object getBody() {
		return body;
	}

}
