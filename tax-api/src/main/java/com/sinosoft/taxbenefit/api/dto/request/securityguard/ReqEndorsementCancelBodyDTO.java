package com.sinosoft.taxbenefit.api.dto.request.securityguard;

import com.sinosoft.taxbenefit.api.dto.request.base.BaseRequestTax;

/**
 * 保全撤销 核心请求DTO
 * @author zhangke
 *
 */
public class ReqEndorsementCancelBodyDTO extends BaseRequestTax{
	private static final long serialVersionUID = -8710794958210479600L;

	private ReqEndorsementCancelInfoDTO body;

	public ReqEndorsementCancelInfoDTO getBody() {
		return body;
	}

	public void setBody(ReqEndorsementCancelInfoDTO body) {
		this.body = body;
	}


}
