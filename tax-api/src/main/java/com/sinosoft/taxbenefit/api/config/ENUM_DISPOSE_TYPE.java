package com.sinosoft.taxbenefit.api.config;

/**
 * 任务类型枚举
 * @author SheChunMing
 */
public enum ENUM_DISPOSE_TYPE {
	APPOINTMENT("01","预约码类型"), ASYNC("02","异步类型"), ERROR("03","异常类型");
	private final String code;
	private final String desc;
	ENUM_DISPOSE_TYPE(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public String code() {
		return code;
	}

	public String desc() {
		return desc;
	}
	/**
	 * 根据key获得value
	 * @param key
	 * @return
	 */
	public static String getEnumValueByKey(String key){
		for(ENUM_DISPOSE_TYPE enumItem : ENUM_DISPOSE_TYPE.values()){
			if(key.equals(enumItem.code())){
				return enumItem.desc();
			}
		}
		return "";
	}
}
