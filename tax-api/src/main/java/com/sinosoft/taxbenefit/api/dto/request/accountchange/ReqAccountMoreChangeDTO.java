package com.sinosoft.taxbenefit.api.dto.request.accountchange;

import java.util.List;
import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;
/**
 * 
 * title：核心请求中保信列表DTO（多笔）
 * @author zhangyu
 * @date 2016年3月18日 下午1:30:28
 */
public class ReqAccountMoreChangeDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = -412850282595220929L;
	
	private List<ReqAccountInfoDTO> savingAccountFeeList;
	
	public List<ReqAccountInfoDTO> getSavingAccountFeeList() {
		return savingAccountFeeList;
	}
	public void setSavingAccountFeeList(List<ReqAccountInfoDTO> savingAccountFeeList) {
		this.savingAccountFeeList = savingAccountFeeList;
	}
}