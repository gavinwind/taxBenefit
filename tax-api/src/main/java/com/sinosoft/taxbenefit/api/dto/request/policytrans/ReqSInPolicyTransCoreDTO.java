package com.sinosoft.taxbenefit.api.dto.request.policytrans;

import com.sinosoft.taxbenefit.api.dto.request.base.BaseRequestTax;

/**
 * （核心）保单转移（单笔）外部转入申请上传请求
 * @author SheChunMing
 */
public class ReqSInPolicyTransCoreDTO extends BaseRequestTax {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1011730163306031237L;
	/** 报文体信息 */
	private ReqSInPolicyTransferDTO body;
	
	public void setBody(ReqSInPolicyTransferDTO body) {
		this.body = body;
	}
	@Override
	public Object getBody() {
		return body;
	}

}
