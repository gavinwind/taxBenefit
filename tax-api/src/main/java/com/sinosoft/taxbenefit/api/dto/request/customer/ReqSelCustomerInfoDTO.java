package com.sinosoft.taxbenefit.api.dto.request.customer;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;
/**
 * 
 * title：客户概要信息查询
 * @author zhangyu
 * @date 2016年3月6日 下午1:52:52
 */
public class ReqSelCustomerInfoDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = -299366200901198658L;
	//保险公司操作人员DTO
	private String operator;	
	//客戶DTO
	private SelCustomerDTO customer;	
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public SelCustomerDTO getCustomer() {
		return customer;
	}
	public void setCustomer(SelCustomerDTO customer) {
		this.customer = customer;
	}
}