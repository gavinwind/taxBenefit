package com.sinosoft.taxbenefit.api.dto.request.securityguard;

import java.math.BigDecimal;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * 受益人信息
 * 
 * @author zhanghaosh@sinosoft.com.cn
 *
 */
public class ReqKeepBeneficiaryInfoDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3202284289939474316L;
	/** 受益人类型 Y **/
	private String beneficiaryType;
	/** 姓名 Y **/
	private String name;
	/** 性别 Y **/
	private String gender;
	/** 出生日期 Y **/
	private String birthday;
	/** 证件类型 Y **/
	private String certiType;
	/** 证件号码 Y **/
	private String certiNo;
	/** 国籍 Y **/
	private String nationality;
	/** 被保险人与受益人关系 Y **/
	private String benInsuredRelation;
	/** 受益顺位 Y **/
	private Integer priority;
	/** 受益比例 Y **/
	private BigDecimal proportion;
	public String getBeneficiaryType() {
		return beneficiaryType;
	}
	public void setBeneficiaryType(String beneficiaryType) {
		this.beneficiaryType = beneficiaryType;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getCertiType() {
		return certiType;
	}
	public void setCertiType(String certiType) {
		this.certiType = certiType;
	}
	
	public String getCertiNo() {
		return certiNo;
	}
	public void setCertiNo(String certiNo) {
		this.certiNo = certiNo;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getBenInsuredRelation() {
		return benInsuredRelation;
	}
	public void setBenInsuredRelation(String benInsuredRelation) {
		this.benInsuredRelation = benInsuredRelation;
	}
	
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	public BigDecimal getProportion() {
		return proportion;
	}
	public void setProportion(BigDecimal proportion) {
		this.proportion = proportion;
	}
	
	

}
