package com.sinosoft.taxbenefit.api.dto.request.claim;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * 理赔保单信息
 * 
 * @author zhangke
 *
 */
public class ReqClaimPolicyInfoDTO  extends TaxBaseApiDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8658115223896322728L;
	/** 保单号 **/
	private String policyNo;
	/** 分单号 **/
	private String sequenceNo;
	/** 出险人客户编码 **/
	private String customerNo;
	/** 当前保单状态 **/
	private String policyStatus;
	/** 保单赔付金额 **/
	private String claimAmount;
	/** 险种赔付信息 */
	private List<ReqClaimCoveragePaymentInfoDTO> coveragePaymentList;

	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public String getSequenceNo() {
		return sequenceNo;
	}

	public void setSequenceNo(String sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getPolicyStatus() {
		return policyStatus;
	}

	public void setPolicyStatus(String policyStatus) {
		this.policyStatus = policyStatus;
	}

	public String getClaimAmount() {
		return claimAmount;
	}

	public void setClaimAmount(String claimAmount) {
		this.claimAmount = claimAmount;
	}

	public List<ReqClaimCoveragePaymentInfoDTO> getCoveragePaymentList() {
		return coveragePaymentList;
	}

	public void setCoveragePaymentList(
			List<ReqClaimCoveragePaymentInfoDTO> coveragePaymentList) {
		this.coveragePaymentList = coveragePaymentList;
	}

}
