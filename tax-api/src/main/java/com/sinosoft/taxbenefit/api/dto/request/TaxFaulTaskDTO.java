package com.sinosoft.taxbenefit.api.dto.request;

import java.util.Date;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * 异常任务表DTO
 * @author zhangke
 */
public class TaxFaulTaskDTO extends TaxBaseApiDTO{

	private static final long serialVersionUID = -1210197152186319281L;
	/** 服务接口编码 */
	private String serverPortCode;
	/** 服务接口名称 */
	private String serverPortName;
	/** 地区编码 */
	private String areaCode;
	/** 批次号 */
	private String batchBizNo;
	/** 流水号 */
	private String serialNo;
	/** 处理类型 */
	private String disposeType;
	/** 预约码 */
	private String bookSequenceNo;
	/** 处理状态 */
	private String disposeResult;
	/** 异步多笔请求报文*/
	private String asynContent;
	/** 成功返回报文 */
	private String successMessage;
	/** 处理时间 */
	private Date disposeDate;

	public String getServerPortCode() {
		return serverPortCode;
	}

	public void setServerPortCode(String serverPortCode) {
		this.serverPortCode = serverPortCode;
	}

	public String getServerPortName() {
		return serverPortName;
	}

	public void setServerPortName(String serverPortName) {
		this.serverPortName = serverPortName;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getBatchBizNo() {
		return batchBizNo;
	}

	public void setBatchBizNo(String batchBizNo) {
		this.batchBizNo = batchBizNo;
	}

	public String getDisposeType() {
		return disposeType;
	}

	public void setDisposeType(String disposeType) {
		this.disposeType = disposeType;
	}

	public String getBookSequenceNo() {
		return bookSequenceNo;
	}

	public void setBookSequenceNo(String bookSequenceNo) {
		this.bookSequenceNo = bookSequenceNo;
	}

	public String getDisposeResult() {
		return disposeResult;
	}

	public void setDisposeResult(String disposeResult) {
		this.disposeResult = disposeResult;
	}

	public String getAsynContent() {
		return asynContent;
	}

	public void setAsynContent(String asynContent) {
		this.asynContent = asynContent;
	}

	public String getSuccessMessage() {
		return successMessage;
	}

	public void setSuccessMessage(String successMessage) {
		this.successMessage = successMessage;
	}

	public Date getDisposeDate() {
		return disposeDate;
	}

	public void setDisposeDate(Date disposeDate) {
		this.disposeDate = disposeDate;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
}
