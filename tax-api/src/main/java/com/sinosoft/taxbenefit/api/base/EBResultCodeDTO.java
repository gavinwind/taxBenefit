package com.sinosoft.taxbenefit.api.base;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;
/**
 * 中保信返回结果对象
 * @author Shechunming
 */
public class EBResultCodeDTO extends TaxBaseApiDTO{

	private static final long serialVersionUID = 5545425352798097030L;
	// 返回编码
	private String resultCode;
	// 返回描述(多个)
	private List<String> resultMessage;

	public String getResultCode() {
		return resultCode;
	}
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	public List<String> getResultMessage() {
		return resultMessage;
	}
	public void setResultMessage(List<String> resultMessage) {
		this.resultMessage = resultMessage;
	}
}
