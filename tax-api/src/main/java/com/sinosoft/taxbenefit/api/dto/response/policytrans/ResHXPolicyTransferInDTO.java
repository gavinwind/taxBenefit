package com.sinosoft.taxbenefit.api.dto.response.policytrans;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * （核心）保单转移信息查询 -转出信息
 * @author SheChunMing
 */
public class ResHXPolicyTransferInDTO extends TaxBaseApiDTO{
	private static final long serialVersionUID = -3078826942127629956L;
	// 转入保险公司
	private String companyName;
	// 开户行名称
	private String bankName;
	// 户名
	private String accountHolder;
	// 账号
	private String accountNo;
	// 保单转移联系人
	private String contactName;
	// 电话
	private String contactTele;
	// Email
	private String contactEmail;
	// 保单转入余额信息查询 -转出信息
	private List<ResHXPolicyTransferOutDTO> transferOutList;
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getAccountHolder() {
		return accountHolder;
	}
	public void setAccountHolder(String accountHolder) {
		this.accountHolder = accountHolder;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getContactTele() {
		return contactTele;
	}
	public void setContactTele(String contactTele) {
		this.contactTele = contactTele;
	}
	public String getContactEmail() {
		return contactEmail;
	}
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	public List<ResHXPolicyTransferOutDTO> getResHXPolicyTransferOutList() {
		return transferOutList;
	}
	public void setResHXPolicyTransferOutList(
			List<ResHXPolicyTransferOutDTO> transferOutList) {
		this.transferOutList = transferOutList;
	}
}
