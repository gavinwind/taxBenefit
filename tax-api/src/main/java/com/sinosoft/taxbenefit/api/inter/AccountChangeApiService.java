package com.sinosoft.taxbenefit.api.inter;

import java.util.List;
import com.sinosoft.taxbenefit.api.dto.request.accountchange.ReqAccountInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
/**
 * 
 * title：万能账户信息变更上传接口
 * @author zhangyu
 * @date 2016年3月4日 上午9:40:11
 */
public interface AccountChangeApiService {
	
	/**
	 *万能账户信息变更上传(单笔)
	 * @param reqAccountInfoDTO
	 */
	ThirdSendResponseDTO AccountChange(ReqAccountInfoDTO reqAccountInfoDTO,ParamHeadDTO paramHeadDTO);
	/**
	 * 万能账户信息变更上传(多笔)
	 * @param reqAccountInfoDTO
	 * @param paramHeadDTO
	 * @return
	 */
	ThirdSendResponseDTO AccountListChange(List<ReqAccountInfoDTO> reqAccountInfoDTO,ParamHeadDTO paramHeadDTO);
}
