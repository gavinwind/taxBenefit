package com.sinosoft.taxbenefit.api.dto.request.policystate;

import com.sinosoft.taxbenefit.api.dto.request.base.BaseRequestTax;


/**
 * @author Administrator
 *
 */
public class PolicyStateOneBody  extends BaseRequestTax{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8509463979842013705L;
	private PolicyStateEndorsement body;
	

	public void setBody(PolicyStateEndorsement body) {
		this.body = body;
	}
	@Override
	public Object getBody() {
		return body;
	}

}
