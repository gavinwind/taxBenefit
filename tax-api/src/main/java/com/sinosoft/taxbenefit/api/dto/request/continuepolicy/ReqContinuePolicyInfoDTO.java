package com.sinosoft.taxbenefit.api.dto.request.continuepolicy;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * Title:核心-续保信息上传请求(保单信息)
 * @author yangdongkai@outlook.com
 * @date 2016年2月29日 下午5:15:53
 */
public class ReqContinuePolicyInfoDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3604467727790715414L;
	// 业务号
	private String bizNo;
	//保单号
	private String policyNo;
	//保单生效日期
	private String effectiveDate;
	//保单满期日期
	private String expireDate;
	//续保批单号
	private String renewalEndorsementNo;
	//客户信息
	private List<ReqContinuePolicyCustomerInfoDTO> customerList;
	/**
	 * @return the customerList
	 */
	public List<ReqContinuePolicyCustomerInfoDTO> getCustomerList() {
		return customerList;
	}
	/**
	 * @param customerList the customerList to set
	 */
	public void setCustomerList(List<ReqContinuePolicyCustomerInfoDTO> customerList) {
		this.customerList = customerList;
	}
	/**
	 * @return the effectiveDate
	 */
	public String getEffectiveDate() {
		return effectiveDate;
	}
	/**
	 * @param effectiveDate the effectiveDate to set
	 */
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	/**
	 * @return the expireDate
	 */
	public String getExpireDate() {
		return expireDate;
	}
	/**
	 * @param expireDate the expireDate to set
	 */
	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}
	/**
	 * @return the renewalEndorsemetNo
	 */
	public String getRenewalEndorsementNo() {
		return renewalEndorsementNo;
	}
	/**
	 * @param renewalEndorsemetNo the renewalEndorsemetNo to set
	 */
	public void setRenewalEndorsementNo(String renewalEndorsementNo) {
		this.renewalEndorsementNo = renewalEndorsementNo;
	}
	/**
	 * @return the bizNo
	 */
	public String getBizNo() {
		return bizNo;
	}
	/**
	 * @param bizNo the bizNo to set
	 */
	public void setBizNo(String bizNo) {
		this.bizNo = bizNo;
	}
	/**
	 * @return the policyNo
	 */
	public String getPolicyNo() {
		return policyNo;
	}
	/**
	 * @param policyNo the policyNo to set
	 */
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

}
