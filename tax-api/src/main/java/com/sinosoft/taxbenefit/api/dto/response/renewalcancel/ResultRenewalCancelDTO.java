package com.sinosoft.taxbenefit.api.dto.response.renewalcancel;

import com.sinosoft.taxbenefit.api.base.HXResultCodeDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;
/**
 * 核心-续保撤销信息返回
 * @author yangdongkai@outlook.com
 * @date 2016年3月22日 下午2:55:55
 */
public class ResultRenewalCancelDTO extends TaxBaseApiDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -412207842295616009L;
	//业务号
	private String bizNo;
	//续保批单号
	private String renewalEndorsementNo;
	//续保撤销确认编码
	private String renewalCancelSequenceNo;
	//返回编码
	private HXResultCodeDTO result;
	
	/**
	 * @return the result
	 */
	public HXResultCodeDTO getResult() {
		return result;
	}
	/**
	 * @param result the result to set
	 */
	public void setResult(HXResultCodeDTO result) {
		this.result = result;
	}
	/**
	 * @return the bizNo
	 */
	public String getBizNo() {
		return bizNo;
	}
	/**
	 * @param bizNo the bizNo to set
	 */
	public void setBizNo(String bizNo) {
		this.bizNo = bizNo;
	}
	/**
	 * @return the renewalEndorsementNo
	 */
	public String getRenewalEndorsementNo() {
		return renewalEndorsementNo;
	}
	/**
	 * @param renewalEndorsementNo the renewalEndorsementNo to set
	 */
	public void setRenewalEndorsementNo(String renewalEndorsementNo) {
		this.renewalEndorsementNo = renewalEndorsementNo;
	}
	/**
	 * @return the renewalCancelSequenceNo
	 */
	public String getRenewalCancelSequenceNo() {
		return renewalCancelSequenceNo;
	}
	/**
	 * @param renewalCancelSequenceNo the renewalCancelSequenceNo to set
	 */
	public void setRenewalCancelSequenceNo(String renewalCancelSequenceNo) {
		this.renewalCancelSequenceNo = renewalCancelSequenceNo;
	}
}