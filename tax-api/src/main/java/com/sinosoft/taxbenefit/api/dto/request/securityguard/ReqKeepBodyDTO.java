package com.sinosoft.taxbenefit.api.dto.request.securityguard;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

public class ReqKeepBodyDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6325458282944511889L;
	private ReqKeepEndorsementInfoDTO endorsement;

	public ReqKeepEndorsementInfoDTO getEndorsement() {
		return endorsement;
	}

	public void setEndorsement(ReqKeepEndorsementInfoDTO endorsement) {
		this.endorsement = endorsement;
	}
	
}
