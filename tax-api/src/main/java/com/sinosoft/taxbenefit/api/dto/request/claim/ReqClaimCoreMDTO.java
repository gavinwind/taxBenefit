package com.sinosoft.taxbenefit.api.dto.request.claim;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.BaseRequestTax;
/**
 * (核心)理赔上传请求dto(多笔)(最外层)
 * @author zhangke
 *
 */
public class ReqClaimCoreMDTO extends BaseRequestTax{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5796064860280313749L;
	/** 报文体信息 */
	private List<ReqClaimInfoDTO> body;
	
	public void setBody(List<ReqClaimInfoDTO> body){
		this.body= body;
	}
	@Override
	public Object getBody() {
		return body;
	}

}
