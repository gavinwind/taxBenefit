package com.sinosoft.taxbenefit.api.dto.request.policytrans;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * （核心）保单转移-客户信息
 * @author SheChunMing
 */
public class ReqPolicyTransClientDTO extends TaxBaseApiDTO{
	
	private static final long serialVersionUID = -1565812713339882933L;
	// 转出分单号
	private String sequenceNo;
	// 转出登记时间(yyyy-MM-dd hh:mm:ss)
	private String registerDate;
	// 预计终止时间
	private String expectedTerminiateDate;
	// 无法转出的原因
	private String rejectReason;
	// 转出公司联系人
	private String contactName;
	// 电话
	private String contactTele;
	// Email
	private String contactEmail;

	public String getSequenceNo() {
		return sequenceNo;
	}
	public void setSequenceNo(String sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
	public String getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(String registerDate) {
		this.registerDate = registerDate;
	}
	public String getExpectedTerminiateDate() {
		return expectedTerminiateDate;
	}
	public void setExpectedTerminiateDate(String expectedTerminiateDate) {
		this.expectedTerminiateDate = expectedTerminiateDate;
	}
	public String getRejectReason() {
		return rejectReason;
	}
	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getContactTele() {
		return contactTele;
	}
	public void setContactTele(String contactTele) {
		this.contactTele = contactTele;
	}
	public String getContactEmail() {
		return contactEmail;
	}
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	
}
