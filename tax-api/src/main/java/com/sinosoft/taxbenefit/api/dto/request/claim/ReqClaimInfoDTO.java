package com.sinosoft.taxbenefit.api.dto.request.claim;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * 理赔信息上传请求DTO对象
 * 
 * @author zhangke
 *
 */
public class ReqClaimInfoDTO extends TaxBaseApiDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6605418089415033878L;
	/** 业务号 **/
	private String bizNo;
	/** 理赔赔案号 **/
	private String claimNo;
	/** 报案日期 **/
	private String reportDate;
	/** 申请日期 **/
	private String applyDate;
	/** 立案日期 **/
	private String registrationDate;
	/** 出险日期 **/
	private String accidentDate;
	/** 出险原因 **/
	private String accidentReason;
	/** 出险地点 **/
	private String accidentPlace;
	/** 是否经过理赔调查 **/
	private String claimInvestigation;
	/** 是否照会 **/
	private String pendingIndi;
	/** 照会完成日期 **/
	private String pendingFinishDate;
	/** 理赔结论代码 **/
	private String claimConclusionCode;
	/** 非正常赔付原因 **/
	private String conclusionReason;
	/** 申请金额 **/
	private String applyAmount;
	/** 保险公司赔付金额 **/
	private String claimAmount;
	/** 基本医保实际补偿金额 **/
	private String socialInsuPayment;
	/** 结案日期 **/
	private String endCaseDate;
	/** 案件状态 **/
	private String status;
	/** 出险人姓名 **/
	private String claimantName;
	/** 出险人性别 **/
	private String claimantGender;
	/** 出险人出生日期 **/
	private String claimantBirthday;
	/** 出险人证件类别 **/
	private String claimantCertiType;
	/** 出险人证件号码 **/
	private String claimantCertiCode;
	/** 身故与否标识 **/
	private String deathIndi;
	/** 身故日期 **/
	private String deathDate;
	/** 警示标识 **/
	private String warningIndi;
	/** 警示原因描述 **/
	private String warningDesc;
	/** 最后一笔赔款支付时间 */
	private String lastPaymentTime;
	/** 拒赔案件的预估赔付金额 */
	private String estimatePayAmount;

	/** 理赔保单信息 **/
	private List<ReqClaimPolicyInfoDTO> policyList;
	/** 医疗事件信息 **/
	private List<ReqClaimEventInfoLDTO> eventList;

	public String getBizNo() {
		return bizNo;
	}

	public void setBizNo(String bizNo) {
		this.bizNo = bizNo;
	}

	public String getClaimNo() {
		return claimNo;
	}

	public void setClaimNo(String claimNo) {
		this.claimNo = claimNo;
	}

	public String getReportDate() {
		return reportDate;
	}

	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}

	public String getApplyDate() {
		return applyDate;
	}

	public void setApplyDate(String applyDate) {
		this.applyDate = applyDate;
	}

	public String getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getAccidentDate() {
		return accidentDate;
	}

	public void setAccidentDate(String accidentDate) {
		this.accidentDate = accidentDate;
	}

	public String getAccidentReason() {
		return accidentReason;
	}

	public void setAccidentReason(String accidentReason) {
		this.accidentReason = accidentReason;
	}

	public String getAccidentPlace() {
		return accidentPlace;
	}

	public void setAccidentPlace(String accidentPlace) {
		this.accidentPlace = accidentPlace;
	}

	public String getClaimInvestigation() {
		return claimInvestigation;
	}

	public void setClaimInvestigation(String claimInvestigation) {
		this.claimInvestigation = claimInvestigation;
	}

	public String getPendingIndi() {
		return pendingIndi;
	}

	public void setPendingIndi(String pendingIndi) {
		this.pendingIndi = pendingIndi;
	}

	public String getPendingFinishDate() {
		return pendingFinishDate;
	}

	public void setPendingFinishDate(String pendingFinishDate) {
		this.pendingFinishDate = pendingFinishDate;
	}

	public String getClaimConclusionCode() {
		return claimConclusionCode;
	}

	public void setClaimConclusionCode(String claimConclusionCode) {
		this.claimConclusionCode = claimConclusionCode;
	}

	public String getConclusionReason() {
		return conclusionReason;
	}

	public void setConclusionReason(String conclusionReason) {
		this.conclusionReason = conclusionReason;
	}

	public String getApplyAmount() {
		return applyAmount;
	}

	public String getLastPaymentTime() {
		return lastPaymentTime;
	}

	public void setLastPaymentTime(String lastPaymentTime) {
		this.lastPaymentTime = lastPaymentTime;
	}

	public String getEstimatePayAmount() {
		return estimatePayAmount;
	}

	public void setEstimatePayAmount(String estimatePayAmount) {
		this.estimatePayAmount = estimatePayAmount;
	}

	public void setApplyAmount(String applyAmount) {
		this.applyAmount = applyAmount;
	}

	public String getClaimAmount() {
		return claimAmount;
	}

	public List<ReqClaimEventInfoLDTO> getEventList() {
		return eventList;
	}

	public void setEventList(List<ReqClaimEventInfoLDTO> eventList) {
		this.eventList = eventList;
	}

	public void setClaimAmount(String claimAmount) {
		this.claimAmount = claimAmount;
	}

	public String getSocialInsuPayment() {
		return socialInsuPayment;
	}

	public void setSocialInsuPayment(String socialInsuPayment) {
		this.socialInsuPayment = socialInsuPayment;
	}

	public String getEndCaseDate() {
		return endCaseDate;
	}

	public void setEndCaseDate(String endCaseDate) {
		this.endCaseDate = endCaseDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getClaimantName() {
		return claimantName;
	}

	public void setClaimantName(String claimantName) {
		this.claimantName = claimantName;
	}

	public String getClaimantGender() {
		return claimantGender;
	}

	public void setClaimantGender(String claimantGender) {
		this.claimantGender = claimantGender;
	}

	public String getClaimantBirthday() {
		return claimantBirthday;
	}

	public void setClaimantBirthday(String claimantBirthday) {
		this.claimantBirthday = claimantBirthday;
	}

	public String getClaimantCertiType() {
		return claimantCertiType;
	}

	public void setClaimantCertiType(String claimantCertiType) {
		this.claimantCertiType = claimantCertiType;
	}

	public String getClaimantCertiCode() {
		return claimantCertiCode;
	}

	public void setClaimantCertiCode(String claimantCertiCode) {
		this.claimantCertiCode = claimantCertiCode;
	}

	public String getDeathIndi() {
		return deathIndi;
	}

	public void setDeathIndi(String deathIndi) {
		this.deathIndi = deathIndi;
	}

	public String getDeathDate() {
		return deathDate;
	}

	public void setDeathDate(String deathDate) {
		this.deathDate = deathDate;
	}

	public String getWarningIndi() {
		return warningIndi;
	}

	public void setWarningIndi(String warningIndi) {
		this.warningIndi = warningIndi;
	}

	public String getWarningDesc() {
		return warningDesc;
	}

	public void setWarningDesc(String warningDesc) {
		this.warningDesc = warningDesc;
	}

	public List<ReqClaimPolicyInfoDTO> getPolicyList() {
		return policyList;
	}

	public void setPolicyList(List<ReqClaimPolicyInfoDTO> policyList) {
		this.policyList = policyList;
	}
}
