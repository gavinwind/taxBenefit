package com.sinosoft.taxbenefit.api.inter;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqMPremiumInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqSPremiumInfoDTO;


public interface RenewalPremiumInfoApiService {
	/**
	 * 续期信息上传（单个）
	 * @param ReqSRenewalPremiumInfoDTO
	 */
	ThirdSendResponseDTO renewalPremium(ReqSPremiumInfoDTO premiumList,ParamHeadDTO paramHead);
	/**
	 * 续期信息上传（多个）
	 * @param premiumList
	 * @param paramHead
	 * @return
	 */
	ThirdSendResponseDTO renewalMPremium(ReqMPremiumInfoDTO premiumList,ParamHeadDTO paramHead); 
}