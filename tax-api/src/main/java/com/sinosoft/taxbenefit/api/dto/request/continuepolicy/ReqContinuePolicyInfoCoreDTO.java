package com.sinosoft.taxbenefit.api.dto.request.continuepolicy;

import com.sinosoft.taxbenefit.api.dto.request.base.BaseRequestTax;

/**
 * 核心-续期信息上传请求
 * Title:ReqContinuePolicyInfoCoreDTO
 * @author yangdongkai@outlook.com
 * @date 2016年3月8日 下午3:15:10
 */
public class ReqContinuePolicyInfoCoreDTO extends BaseRequestTax{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7126393250478414024L;
	//报文体信息
	private ReqContinuePolicyInfoDTO body;
	
	/**
	 * @param body the body to set
	 */
	public void setBody(ReqContinuePolicyInfoDTO body) {
		this.body = body;
	}

	@Override
	public Object getBody() {
		// TODO Auto-generated method stub
		return body;
	}
	
}
