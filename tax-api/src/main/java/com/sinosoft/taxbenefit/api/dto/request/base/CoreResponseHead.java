package com.sinosoft.taxbenefit.api.dto.request.base;

/**
 * 响应核心报文头
 * 
 * @author zhangke
 *
 */
public class CoreResponseHead {
	/** 交易流水号 */
	private String serialNo;
	/** 交易时间 */
	private String transDate;
	/** 签名 */
	private String signKey;

	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public String getTransDate() {
		return transDate;
	}
	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}
	public String getSignKey() {
		return signKey;
	}
	public void setSignKey(String signKey) {
		this.signKey = signKey;
	}

}
