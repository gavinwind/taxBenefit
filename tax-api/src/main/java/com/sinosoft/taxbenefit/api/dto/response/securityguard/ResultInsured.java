package com.sinosoft.taxbenefit.api.dto.response.securityguard;

import com.sinosoft.taxbenefit.api.base.HXResultCodeDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

public class ResultInsured extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3678964596113382330L;
	/**分单号 **/
	private String sequenceNo;
	/**原客户编码 	**/
	private String customerNo;
	/**新客户编码**/
	private String customerNoNew;
	
	private	HXResultCodeDTO  result;
	
	
	public HXResultCodeDTO getResult() {
		return result;
	}
	public void setResult(HXResultCodeDTO result) {
		this.result = result;
	}
	public String getSequenceNo() {
		return sequenceNo;
	}
	public void setSequenceNo(String sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public String getCustomerNoNew() {
		return customerNoNew;
	}
	public void setCustomerNoNew(String customerNoNew) {
		this.customerNoNew = customerNoNew;
	}
	
	

}
