package com.sinosoft.taxbenefit.api.inter;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalcancel.RequestRenewalCancelDTO;

public interface RenewalCancelApiService {
	/**
	 * 续保撤销
	 * @param requestRenewalCancelDTO
	 * @param paramHead
	 * @return
	 */
	ThirdSendResponseDTO renewalCancel(RequestRenewalCancelDTO requestRenewalCancelDTO,ParamHeadDTO paramHead);
}