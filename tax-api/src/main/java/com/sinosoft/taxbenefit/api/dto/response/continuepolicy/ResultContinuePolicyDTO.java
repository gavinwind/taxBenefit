package com.sinosoft.taxbenefit.api.dto.response.continuepolicy;

import com.sinosoft.taxbenefit.api.base.HXResultCodeDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * 核心-续保信息上传返回数据
 * Title:ContinuePolicyResultDTO
 * @author yangdongkai@outlook.com
 * @date 2016年3月8日 下午2:48:03
 */
public class ResultContinuePolicyDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6809929038847071573L;
	//业务号
	private String bizNo;
	//续保确认编码
	private String renewalSequenceNo;
	//返回编码
	private HXResultCodeDTO result;	

	public HXResultCodeDTO getResult() {
		return result;
	}
	public void setResult(HXResultCodeDTO result) {
		this.result = result;
	}
	/**
	 * @return the bizNo
	 */
	public String getBizNo() {
		return bizNo;
	}
	/**
	 * @param bizNo the bizNo to set
	 */
	public void setBizNo(String bizNo) {
		this.bizNo = bizNo;
	}
	/**
	 * @return the renewalSequenceNo
	 */
	public String getRenewalSequenceNo() {
		return renewalSequenceNo;
	}
	/**
	 * @param renewalSequenceNo the renewalSequenceNo to set
	 */
	public void setRenewalSequenceNo(String renewalSequenceNo) {
		this.renewalSequenceNo = renewalSequenceNo;
	}
	
}
