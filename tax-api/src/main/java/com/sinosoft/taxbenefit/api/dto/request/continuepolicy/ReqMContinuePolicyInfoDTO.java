package com.sinosoft.taxbenefit.api.dto.request.continuepolicy;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;
/**
 * Title:核心-续保信息上传请求(多个)
 * @author yangdongkai@outlook.com
 * @date 2016年3月18日 上午9:35:45
 */
public class ReqMContinuePolicyInfoDTO extends TaxBaseApiDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4167434568900375438L;
	//保单信息
	private List<ReqContinuePolicyInfoDTO> policyList;
	/**
	 * @return the policyList
	 */
	public List<ReqContinuePolicyInfoDTO> getPolicyList() {
		return policyList;
	}
	/**
	 * @param policyList the policyList to set
	 */
	public void setPolicyList(List<ReqContinuePolicyInfoDTO> policyList) {
		this.policyList = policyList;
	}
	
}
