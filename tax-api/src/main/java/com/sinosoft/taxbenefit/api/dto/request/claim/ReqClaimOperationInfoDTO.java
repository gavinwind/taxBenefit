package com.sinosoft.taxbenefit.api.dto.request.claim;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * 手术
 * 
 * @author zhangke
 *
 */
public class ReqClaimOperationInfoDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3626691505772840825L;
	/** 手术代码 **/
	private String operationCode;

	public String getOperationCode() {
		return operationCode;
	}

	public void setOperationCode(String operationCode) {
		this.operationCode = operationCode;
	}

}
