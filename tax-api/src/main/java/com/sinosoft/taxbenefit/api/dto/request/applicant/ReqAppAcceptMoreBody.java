package com.sinosoft.taxbenefit.api.dto.request.applicant;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.BaseRequestTax;

/**
 * 投保信息上传请求(多笔)DTO
 * @author zhangke
 *
 */
public class ReqAppAcceptMoreBody extends BaseRequestTax {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2125283146760870510L;
	private List<ReqAppPolicyDTO> body;

	public void setBody(List<ReqAppPolicyDTO> body) {
		this.body = body;
	}

	@Override
	public Object getBody() {
		return body;
	}

}

	
	
