package com.sinosoft.taxbenefit.api.dto.response.applicant;

import com.sinosoft.taxbenefit.api.base.HXResultCodeDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

public class ResultClient extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6663024985420454295L;
	/** 分单号 **/
	private String sequenceNo;
	/** 税优识别码 **/
	private String taxCode;
	
	private HXResultCodeDTO result;

	public String getSequenceNo() {
		return sequenceNo;
	}

	public void setSequenceNo(String sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public HXResultCodeDTO getResult() {
		return result;
	}

	public void setResult(HXResultCodeDTO result) {
		this.result = result;
	}

	
	
}
