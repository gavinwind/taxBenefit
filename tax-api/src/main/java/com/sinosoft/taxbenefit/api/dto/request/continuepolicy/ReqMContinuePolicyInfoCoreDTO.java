package com.sinosoft.taxbenefit.api.dto.request.continuepolicy;

import com.sinosoft.taxbenefit.api.dto.request.base.BaseRequestTax;
/**
 * 核心-续期信息上传请求(多个)
 * @author yangdongkai@outlook.com
 * @date 2016年3月18日 上午9:39:55
 */
public class ReqMContinuePolicyInfoCoreDTO extends BaseRequestTax {
	/**
	 * 
	 */
	private static final long serialVersionUID = 377922648363811835L;
	//报文体信息
	private ReqMContinuePolicyInfoDTO body;
	/**
	 * @param body the body to set
	 */
	public void setBody(ReqMContinuePolicyInfoDTO body) {
		this.body = body;
	}

	@Override
	public Object getBody() {
		return body;
	}
}
