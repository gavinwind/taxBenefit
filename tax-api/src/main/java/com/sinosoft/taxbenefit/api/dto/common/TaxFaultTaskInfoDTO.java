package com.sinosoft.taxbenefit.api.dto.common;

import java.util.Date;

public class TaxFaultTaskInfoDTO {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_FAULT_TASK.SID
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	private Integer sid;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_FAULT_TASK.SERIAL_NO
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	private String serialNo;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_FAULT_TASK.SERVER_PORT_CODE
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	private String serverPortCode;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_FAULT_TASK.SERVER_PORT_NAME
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	private String serverPortName;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_FAULT_TASK.BATCH_BIZ_NO
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	private String batchBizNo;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_FAULT_TASK.DISPOSE_RESULT
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	private String disposeResult;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_FAULT_TASK.SUCCESS_MESSAGE
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	private String successMessage;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_FAULT_TASK.CREATE_ID
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	private Integer createId;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_FAULT_TASK.CREATE_DATE
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	private Date createDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_FAULT_TASK.DISPOSE_DATE
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	private Date disposeDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_FAULT_TASK.REMARK
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	private String remark;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_FAULT_TASK.SID
	 * @return  the value of TAX_FAULT_TASK.SID
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	public Integer getSid() {
		return sid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_FAULT_TASK.SID
	 * @param sid  the value for TAX_FAULT_TASK.SID
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	public void setSid(Integer sid) {
		this.sid = sid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_FAULT_TASK.SERIAL_NO
	 * @return  the value of TAX_FAULT_TASK.SERIAL_NO
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	public String getSerialNo() {
		return serialNo;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_FAULT_TASK.SERIAL_NO
	 * @param serialNo  the value for TAX_FAULT_TASK.SERIAL_NO
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_FAULT_TASK.SERVER_PORT_CODE
	 * @return  the value of TAX_FAULT_TASK.SERVER_PORT_CODE
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	public String getServerPortCode() {
		return serverPortCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_FAULT_TASK.SERVER_PORT_CODE
	 * @param serverPortCode  the value for TAX_FAULT_TASK.SERVER_PORT_CODE
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	public void setServerPortCode(String serverPortCode) {
		this.serverPortCode = serverPortCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_FAULT_TASK.SERVER_PORT_NAME
	 * @return  the value of TAX_FAULT_TASK.SERVER_PORT_NAME
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	public String getServerPortName() {
		return serverPortName;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_FAULT_TASK.SERVER_PORT_NAME
	 * @param serverPortName  the value for TAX_FAULT_TASK.SERVER_PORT_NAME
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	public void setServerPortName(String serverPortName) {
		this.serverPortName = serverPortName;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_FAULT_TASK.BATCH_BIZ_NO
	 * @return  the value of TAX_FAULT_TASK.BATCH_BIZ_NO
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	public String getBatchBizNo() {
		return batchBizNo;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_FAULT_TASK.BATCH_BIZ_NO
	 * @param batchBizNo  the value for TAX_FAULT_TASK.BATCH_BIZ_NO
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	public void setBatchBizNo(String batchBizNo) {
		this.batchBizNo = batchBizNo;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_FAULT_TASK.DISPOSE_RESULT
	 * @return  the value of TAX_FAULT_TASK.DISPOSE_RESULT
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	public String getDisposeResult() {
		return disposeResult;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_FAULT_TASK.DISPOSE_RESULT
	 * @param disposeResult  the value for TAX_FAULT_TASK.DISPOSE_RESULT
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	public void setDisposeResult(String disposeResult) {
		this.disposeResult = disposeResult;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_FAULT_TASK.SUCCESS_MESSAGE
	 * @return  the value of TAX_FAULT_TASK.SUCCESS_MESSAGE
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	public String getSuccessMessage() {
		return successMessage;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_FAULT_TASK.SUCCESS_MESSAGE
	 * @param successMessage  the value for TAX_FAULT_TASK.SUCCESS_MESSAGE
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	public void setSuccessMessage(String successMessage) {
		this.successMessage = successMessage;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_FAULT_TASK.CREATE_ID
	 * @return  the value of TAX_FAULT_TASK.CREATE_ID
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	public Integer getCreateId() {
		return createId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_FAULT_TASK.CREATE_ID
	 * @param createId  the value for TAX_FAULT_TASK.CREATE_ID
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	public void setCreateId(Integer createId) {
		this.createId = createId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_FAULT_TASK.CREATE_DATE
	 * @return  the value of TAX_FAULT_TASK.CREATE_DATE
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_FAULT_TASK.CREATE_DATE
	 * @param createDate  the value for TAX_FAULT_TASK.CREATE_DATE
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_FAULT_TASK.DISPOSE_DATE
	 * @return  the value of TAX_FAULT_TASK.DISPOSE_DATE
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	public Date getDisposeDate() {
		return disposeDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_FAULT_TASK.DISPOSE_DATE
	 * @param disposeDate  the value for TAX_FAULT_TASK.DISPOSE_DATE
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	public void setDisposeDate(Date disposeDate) {
		this.disposeDate = disposeDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_FAULT_TASK.REMARK
	 * @return  the value of TAX_FAULT_TASK.REMARK
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_FAULT_TASK.REMARK
	 * @param remark  the value for TAX_FAULT_TASK.REMARK
	 * @mbggenerated  Mon Mar 14 11:45:36 CST 2016
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
}