package com.sinosoft.taxbenefit.api.dto.request.renewalpremium;

import java.math.BigDecimal;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * Title:核心-险种信息
 * @author yangdongkai@outlook.com
 * @date 2016年3月2日 上午9:42:49
 */
public class ReqRenewalPremiumCoverageInfoDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6473412706643022974L;
	// 公司产品组代码
	private String coveragePackageCode;
	// 公司险种代码
	private String comCoverageCode;
	// 险种保费
	private BigDecimal coveragePremium;
	// 险种当期风险保费
	private BigDecimal riskPremium;
	
	/**
	 * @return the coveragePackageCode
	 */
	public String getCoveragePackageCode() {
		return coveragePackageCode;
	}
	/**
	 * @param coveragePackageCode the coveragePackageCode to set
	 */
	public void setCoveragePackageCode(String coveragePackageCode) {
		this.coveragePackageCode = coveragePackageCode;
	}
	/**
	 * @return the comCoverageCode
	 */
	public String getComCoverageCode() {
		return comCoverageCode;
	}
	/**
	 * @param comCoverageCode the comCoverageCode to set
	 */
	public void setComCoverageCode(String comCoverageCode) {
		this.comCoverageCode = comCoverageCode;
	}
	/**
	 * @return the coveragePremium
	 */
	public BigDecimal getCoveragePremium() {
		return coveragePremium;
	}
	/**
	 * @param coveragePremium the coveragePremium to set
	 */
	public void setCoveragePremium(BigDecimal coveragePremium) {
		this.coveragePremium = coveragePremium;
	}
	/**
	 * @return the riskPremium
	 */
	public BigDecimal getRiskPremium() {
		return riskPremium;
	}
	/**
	 * @param riskPremium the riskPremium to set
	 */
	public void setRiskPremium(BigDecimal riskPremium) {
		this.riskPremium = riskPremium;
	}
}