package com.sinosoft.taxbenefit.api.dto.response.customer;

import java.math.BigDecimal;
import com.sinosoft.taxbenefit.api.base.HXResultCodeDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;
/**
 * (核心)被保人既往理赔额返回对象
 * @author zhangyu
 * @date 2016年3月10日 上午9:37:30
 */
public class InsuredPayResultDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = -298591584719409471L;
	//业务号
	private String bizNo;
	//平台客户编码 
	private String customerNo;
	//原公司险种年度保额
	private BigDecimal insuranceCoverage;
	//当年度累计赔付金额
	private BigDecimal annualClaimSa;
	//终身累计赔付金额
	private BigDecimal wholeLifeClaimSa;
	
	private HXResultCodeDTO result;
	
	public HXResultCodeDTO getResult() {
		return result;
	}
	public void setResult(HXResultCodeDTO result) {
		this.result = result;
	}
	public String getBizNo() {
		return bizNo;
	}
	public void setBizNo(String bizNo) {
		this.bizNo = bizNo;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public BigDecimal getInsuranceCoverage() {
		return insuranceCoverage;
	}
	public void setInsuranceCoverage(BigDecimal insuranceCoverage) {
		this.insuranceCoverage = insuranceCoverage;
	}
	public BigDecimal getAnnualClaimSa() {
		return annualClaimSa;
	}
	public void setAnnualClaimSa(BigDecimal annualClaimSa) {
		this.annualClaimSa = annualClaimSa;
	}
	public BigDecimal getWholeLifeClaimSa() {
		return wholeLifeClaimSa;
	}
	public void setWholeLifeClaimSa(BigDecimal wholeLifeClaimSa) {
		this.wholeLifeClaimSa = wholeLifeClaimSa;
	}
}