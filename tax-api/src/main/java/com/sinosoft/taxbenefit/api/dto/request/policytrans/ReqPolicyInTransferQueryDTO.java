package com.sinosoft.taxbenefit.api.dto.request.policytrans;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * （核心）保单转移信息查询对象
 * @author SheChunMing
 */
public class ReqPolicyInTransferQueryDTO extends TaxBaseApiDTO {

	private static final long serialVersionUID = 5358388329327083473L;
	// 查询起期
	private String queryStartDate;
	// 查询止期
	private String queryEndDate;

	public String getQueryStartDate() {
		return queryStartDate;
	}

	public void setQueryStartDate(String queryStartDate) {
		this.queryStartDate = queryStartDate;
	}

	public String getQueryEndDate() {
		return queryEndDate;
	}

	public void setQueryEndDate(String queryEndDate) {
		this.queryEndDate = queryEndDate;
	}
	
}
