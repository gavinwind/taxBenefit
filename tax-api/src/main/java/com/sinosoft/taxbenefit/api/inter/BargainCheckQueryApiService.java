package com.sinosoft.taxbenefit.api.inter;

import com.sinosoft.taxbenefit.api.dto.request.bargaincheckquery.ReqBargainCheckQueryDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;

public interface BargainCheckQueryApiService {
	/**
	 * 交易核对信息查询接口
	 * @param reqBargainCheckQueryDTO
	 * @param paramHead
	 * @return
	 */
	ThirdSendResponseDTO bargainCheck(ReqBargainCheckQueryDTO reqBargainCheckQueryDTO,ParamHeadDTO paramHead);
}
