package com.sinosoft.taxbenefit.api.dto.request.policytrans;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;


/**
 * （核心）保单转移-转入信息(多笔)
 * @author SheChunMing
 */
public class ReqMInPolicyTransferDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = -90857895937261733L;
	// 转移申请日期
	private String bizNo;
	// 转移申请日期
	private String transApplyDate;
	// 开户行名称
	private String bankName;
	// 户名
	private String accountHolder;
	// 账号
	private String accountNo;
	// 保单转移联系人
	private String contactName;
	// 电话
	private String contactTele;
	// Email
	private String contactEmail;
	// 转出信息
	private List<ReqPolicyTransferOutDTO> transferOutList;
	
	public String getBizNo() {
		return bizNo;
	}
	public void setBizNo(String bizNo) {
		this.bizNo = bizNo;
	}
	public String getTransApplyDate() {
		return transApplyDate;
	}
	public void setTransApplyDate(String transApplyDate) {
		this.transApplyDate = transApplyDate;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getAccountHolder() {
		return accountHolder;
	}
	public void setAccountHolder(String accountHolder) {
		this.accountHolder = accountHolder;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getContactTele() {
		return contactTele;
	}
	public void setContactTele(String contactTele) {
		this.contactTele = contactTele;
	}
	public String getContactEmail() {
		return contactEmail;
	}
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	public List<ReqPolicyTransferOutDTO> getTransferOutList() {
		return transferOutList;
	}
	public void setTransferOutList(List<ReqPolicyTransferOutDTO> transferOutList) {
		this.transferOutList = transferOutList;
	}
}
