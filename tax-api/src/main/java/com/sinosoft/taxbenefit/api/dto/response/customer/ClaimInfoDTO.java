package com.sinosoft.taxbenefit.api.dto.response.customer;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;
/**
 * 赔案信息
 * @author zhangyu
 * @date 2016年3月14日 上午9:17:37
 */
public class ClaimInfoDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = 494153193023943851L;
	//出险日期
    private String accidentDate;
	//结案时间
    private String endcaseDate;
	//理赔结论代码
    private String claimConclusionCode;
	//警示标识
    private String warningIndi;
	//警示原因描述
    private String warningDesc;
    
	public String getWarningDesc() {
		return warningDesc;
	}
	public void setWarningDesc(String warningDesc) {
		this.warningDesc = warningDesc;
	}
	public String getAccidentDate() {
		return accidentDate;
	}
	public void setAccidentDate(String accidentDate) {
		this.accidentDate = accidentDate;
	}
	public String getEndcaseDate() {
		return endcaseDate;
	}
	public void setEndcaseDate(String endcaseDate) {
		this.endcaseDate = endcaseDate;
	}
	public String getClaimConclusionCode() {
		return claimConclusionCode;
	}
	public void setClaimConclusionCode(String claimConclusionCode) {
		this.claimConclusionCode = claimConclusionCode;
	}
	public String getWarningIndi() {
		return warningIndi;
	}
	public void setWarningIndi(String warningIndi) {
		this.warningIndi = warningIndi;
	}
}