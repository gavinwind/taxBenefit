package com.sinosoft.taxbenefit.api.dto.request.claim;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * 收据分项费用
 * 
 * @author zhangke
 *
 */
public class ReqClaimReceiptFeeInfoDTO extends TaxBaseApiDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3847930291297148071L;
	/** 收据分项费用类型 **/
	private String receiptFeeType;
	/** 金额 **/
	private String amount;

	public String getReceiptFeeType() {
		return receiptFeeType;
	}

	public void setReceiptFeeType(String receiptFeeType) {
		this.receiptFeeType = receiptFeeType;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
