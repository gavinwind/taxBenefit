package com.sinosoft.taxbenefit.api.dto.response.bargaincheckquery;

import java.math.BigDecimal;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;
/**
 * 返回核心汇总信息
 * Title:ResBargainCheckTotalDTO
 * @author yangdongkai@outlook.com
 * @date 2016年3月11日 下午2:03:42
 */
public class ResBargainCheckTotalDTO extends TaxBaseApiDTO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4716012904474087819L;
	// 保险公司代码
	private String companyCode;
	// 指定核对日期
	private String checkDate;
	// 总记录数
	private BigDecimal totalNum;
	// 成功记录数
	private BigDecimal successNum;
	// 失败记录数
	private BigDecimal failNum;

	/**
	 * @return the companyCode
	 */
	public String getCompanyCode() {
		return companyCode;
	}

	/**
	 * @param companyCode
	 *            the companyCode to set
	 */
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	/**
	 * @return the checkDate
	 */
	public String getCheckDate() {
		return checkDate;
	}

	/**
	 * @param checkDate
	 *            the checkDate to set
	 */
	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}

	/**
	 * @return the totalNum
	 */
	public BigDecimal getTotalNum() {
		return totalNum;
	}

	/**
	 * @param totalNum
	 *            the totalNum to set
	 */
	public void setTotalNum(BigDecimal totalNum) {
		this.totalNum = totalNum;
	}

	/**
	 * @return the successNum
	 */
	public BigDecimal getSuccessNum() {
		return successNum;
	}

	/**
	 * @param successNum
	 *            the successNum to set
	 */
	public void setSuccessNum(BigDecimal successNum) {
		this.successNum = successNum;
	}

	/**
	 * @return the failNum
	 */
	public BigDecimal getFailNum() {
		return failNum;
	}

	/**
	 * @param failNum
	 *            the failNum to set
	 */
	public void setFailNum(BigDecimal failNum) {
		this.failNum = failNum;
	}

}
