package com.sinosoft.taxbenefit.api.dto.response.renewalpremium;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;
/**
 * Title:核心-续期保费信息上传返回数据（多个）
 * @author yangdongkai@outlook.com
 * @date 2016年3月17日 上午9:46:28
 */
public class ResultMRenewalPremiumDTO extends TaxBaseApiDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2290631065594492025L;
	//返回结果
	private List<ResultRenewalPremiumDTO> resultList;
	/**
	 * @return the resultList
	 */
	public List<ResultRenewalPremiumDTO> getResultList() {
		return resultList;
	}
	/**
	 * @param resultList the resultList to set
	 */
	public void setResultList(List<ResultRenewalPremiumDTO> resultList) {
		this.resultList = resultList;
	}
	
}
