package com.sinosoft.taxbenefit.api.inter;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqSelCustomerInfoDTO;

public interface CustomerInfoQueryApiService {
	/**
	 * 客户信息查询API
	 * @param reqSelCustomerInfoDTO
	 * @param paramHeadDTO
	 * @return
	 */
	ThirdSendResponseDTO queryCustomerInfo(ReqSelCustomerInfoDTO reqSelCustomerInfoDTO,ParamHeadDTO paramHeadDTO);

}
