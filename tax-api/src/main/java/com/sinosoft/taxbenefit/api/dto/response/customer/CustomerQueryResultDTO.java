package com.sinosoft.taxbenefit.api.dto.response.customer;

import java.util.List;

import com.sinosoft.taxbenefit.api.base.HXResultCodeDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;
/**
 * 客户信息查询DTO
 * @author zhangyu
 * @date 2016年3月14日 上午9:18:40
 */
public class CustomerQueryResultDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4335812326617116256L;
	//平台客户编码 
	private String customerNo;
	//国籍 
	private String nationality;
	//保单列表
	private List<PolicyInfoDTO> policyList ;
	//险种信息列表
	private List<ClaimInfoDTO> claimList;
	
	private HXResultCodeDTO result;
	
	public HXResultCodeDTO getResult() {
		return result;
	}
	public void setResult(HXResultCodeDTO result) {
		this.result = result;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public List<PolicyInfoDTO> getPolicyList() {
		return policyList;
	}
	public void setPolicyList(List<PolicyInfoDTO> policyList) {
		this.policyList = policyList;
	}
	public List<ClaimInfoDTO> getClaimList() {
		return claimList;
	}
	public void setClaimList(List<ClaimInfoDTO> claimList) {
		this.claimList = claimList;
	}
}