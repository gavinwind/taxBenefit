package com.sinosoft.taxbenefit.api.dto.request.securityguard;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * 保全撤销 核心请求信息DTO
 * 
 * @author zhangke
 *
 */
public class ReqEndorsementCancelInfoDTO extends TaxBaseApiDTO{
	private static final long serialVersionUID = -907962212150437645L;
	/** 保单号 */
	private String policyNo;
	/** 保全批单号 */
	private String endorsementNo;
	/** 业务号 */
	private String bizNo;

	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public String getEndorsementNo() {
		return endorsementNo;
	}

	public void setEndorsementNo(String endorsementNo) {
		this.endorsementNo = endorsementNo;
	}

	public String getBizNo() {
		return bizNo;
	}

	public void setBizNo(String bizNo) {
		this.bizNo = bizNo;
	}
}
