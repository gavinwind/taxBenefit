package com.sinosoft.taxbenefit.api.dto.request.renewalpremium;

import com.sinosoft.taxbenefit.api.dto.request.base.BaseRequestTax;

/**
 * 核心-续期保费上传请求(单个)
 * @author yangdongkai@outlook.com
 * @date 2016年3月7日 下午5:44:23
 */
public class ReqRenewalPremiumCoreDTO extends BaseRequestTax{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4358819550070334982L;
	//报文体信息
	private ReqSPremiumInfoDTO body;

	public void setBody(ReqSPremiumInfoDTO body) {
		this.body = body;
	}
	@Override
	public Object getBody() {
		return body;
	}
	
}
