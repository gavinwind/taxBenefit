package com.sinosoft.taxbenefit.api.dto.request.applicant;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * 核保信息
 * 
 * @author zhangke
 *
 */
public class ReqAppUnderwritingDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8710158261547635419L;
	/** 核保结论日期 **/
	private String underwritingDate;
	/** 核保决定 **/
	private String underwritingDecision;
	/** 核保描述 **/
	private String underwritingDes;

	public String getUnderwritingDate() {
		return underwritingDate;
	}

	public void setUnderwritingDate(String underwritingDate) {
		this.underwritingDate = underwritingDate;
	}

	public String getUnderwritingDecision() {
		return underwritingDecision;
	}

	public void setUnderwritingDecision(String underwritingDecision) {
		this.underwritingDecision = underwritingDecision;
	}

	public String getUnderwritingDes() {
		return underwritingDes;
	}

	public void setUnderwritingDes(String underwritingDes) {
		this.underwritingDes = underwritingDes;
	}

}
