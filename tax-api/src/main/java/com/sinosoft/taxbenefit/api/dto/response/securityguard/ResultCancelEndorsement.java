package com.sinosoft.taxbenefit.api.dto.response.securityguard;

import com.sinosoft.taxbenefit.api.base.HXResultCodeDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * 保全撤消响应核心对象
 * @author zhanghao
 *
 */
public class ResultCancelEndorsement  extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3333654019319811504L;
	private HXResultCodeDTO result;

	public HXResultCodeDTO getResult() {
		return result;
	}

	public void setResult(HXResultCodeDTO result) {
		this.result = result;
	}
	
	
}
