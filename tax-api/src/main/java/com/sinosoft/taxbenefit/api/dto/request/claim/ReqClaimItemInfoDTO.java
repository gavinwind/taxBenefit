package com.sinosoft.taxbenefit.api.dto.request.claim;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * 医疗费用
 * 
 * @author zhangke
 *
 */
public class ReqClaimItemInfoDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = 941456545320264975L;
	/** 医疗费用类型 **/
	private String itemCategory;
	/** 金额 **/
	private String amount;
	/** 费用扣减金额 **/
	private String deductibleAmount;
	/** 医疗费用明细 **/
	private List<ReqClaimDetailInfoDTO> detailList;

	public String getItemCategory() {
		return itemCategory;
	}

	public void setItemCategory(String itemCategory) {
		this.itemCategory = itemCategory;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getDeductibleAmount() {
		return deductibleAmount;
	}

	public void setDeductibleAmount(String deductibleAmount) {
		this.deductibleAmount = deductibleAmount;
	}

	public List<ReqClaimDetailInfoDTO> getDetailList() {
		return detailList;
	}

	public void setDetailList(List<ReqClaimDetailInfoDTO> detailList) {
		this.detailList = detailList;
	}

}
