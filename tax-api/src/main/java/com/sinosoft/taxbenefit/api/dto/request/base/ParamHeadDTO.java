package com.sinosoft.taxbenefit.api.dto.request.base;

/**
 * 核心请求头部参数
 * @author zhangke
 */
public class ParamHeadDTO extends TaxBaseApiDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5115297806721758325L;
	/** 交易流水号 */
	private String serialNo;
	/** 地区代码 */
	private String areaCode;
	/** 请求记录数 */
	private String recordNum;
	/**平台接口编码*/
	private String portCode;
	/**平台接口名称*/
	private String portName;

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getRecordNum() {
		return recordNum;
	}

	public void setRecordNum(String recordNum) {
		this.recordNum = recordNum;
	}

	public String getPortCode() {
		return portCode;
	}

	public void setPortCode(String portCode) {
		this.portCode = portCode;
	}

	public String getPortName() {
		return portName;
	}

	public void setPortName(String portName) {
		this.portName = portName;
	}
}
