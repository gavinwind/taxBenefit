package com.sinosoft.taxbenefit.api.dto.response.continuepolicy;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;
/**
 * Title:核心-续保信息上传返回数据(多个)
 * @author yangdongkai@outlook.com
 * @date 2016年3月18日 上午9:42:40
 */
public class ResultMContinuePolicyDTO extends TaxBaseApiDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6342405183937599243L;
	private List<ResultContinuePolicyDTO> resultList;
	/**
	 * @return the resultList
	 */
	public List<ResultContinuePolicyDTO> getResultList() {
		return resultList;
	}
	/**
	 * @param resultList the resultList to set
	 */
	public void setResultList(List<ResultContinuePolicyDTO> resultList) {
		this.resultList = resultList;
	}
	
}
