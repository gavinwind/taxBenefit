package com.sinosoft.taxbenefit.api.base;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;
/**
 * 返回核心结果对象
 * @author Shechunming
 */
public class HXResultCodeDTO extends TaxBaseApiDTO{

	private static final long serialVersionUID = 5545425352798097030L;
	// 返回编码
	private String resultCode;
	// 返回描述(多个)
	private String resultMessage;

	public String getResultCode() {
		return resultCode;
	}
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	public String getResultMessage() {
		return resultMessage;
	}
	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
