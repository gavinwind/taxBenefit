package com.sinosoft.taxbenefit.api.dto.request.securityguard;

import java.math.BigDecimal;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;
/**
 *个人投保信息
 * @author zhanghaosh@sinosoft.com.cn
 *
 */
public class ReqKeepSinglePolicyHolderInfoDTO extends TaxBaseApiDTO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7191658363072554336L;
	/** 投保人客户编码 Y **/
	private String customerNo;
	/** 投保人姓名 Y **/
	private String name;
	/** 性别 Y **/
	private String gender;
	/** 出生日期 Y **/
	private String birthday;
	/** 证件类型 Y **/
	private String certiType;
	/** 证件号码 Y **/
	private String certiNo;
	/** 国籍 Y **/
	private String nationality;
	/** 手机号码 Y **/
	private BigDecimal mobileNo;
	/** 联系地址 Y **/
	private String residencePlace;
	/** 纳税人类型 S **/
	private String taxPayerType;
	/** 纳税标志 Y **/
	private String taxIndi;
	/** 工作单位名称 Y **/
	private String workUnit;
	/** 税务登记号 S **/
	private String organCode1;
	/** 组织机构代码 **/
	private String organCode2;
	/** 营业证号 **/
	private String organCode3;
	/** 社会信用代码 S **/
	private String organCode4;
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getCertiType() {
		return certiType;
	}
	public void setCertiType(String certiType) {
		this.certiType = certiType;
	}
	public String getCertiNo() {
		return certiNo;
	}
	public void setCertiNo(String certiNo) {
		this.certiNo = certiNo;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	

	public BigDecimal getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(BigDecimal mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getResidencePlace() {
		return residencePlace;
	}
	public void setResidencePlace(String residencePlace) {
		this.residencePlace = residencePlace;
	}
	public String getTaxPayerType() {
		return taxPayerType;
	}
	public void setTaxPayerType(String taxPayerType) {
		this.taxPayerType = taxPayerType;
	}
	public String getTaxIndi() {
		return taxIndi;
	}
	public void setTaxIndi(String taxIndi) {
		this.taxIndi = taxIndi;
	}
	public String getWorkUnit() {
		return workUnit;
	}
	public void setWorkUnit(String workUnit) {
		this.workUnit = workUnit;
	}
	public String getOrganCode1() {
		return organCode1;
	}
	public void setOrganCode1(String organCode1) {
		this.organCode1 = organCode1;
	}
	public String getOrganCode2() {
		return organCode2;
	}
	public void setOrganCode2(String organCode2) {
		this.organCode2 = organCode2;
	}
	public String getOrganCode3() {
		return organCode3;
	}
	public void setOrganCode3(String organCode3) {
		this.organCode3 = organCode3;
	}
	public String getOrganCode4() {
		return organCode4;
	}
	public void setOrganCode4(String organCode4) {
		this.organCode4 = organCode4;
	}

	
}
