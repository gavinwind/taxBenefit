package com.sinosoft.taxbenefit.api.dto.request.claim;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * 医疗事件信息
 * 
 * @author zhangke
 *
 */
public class ReqClaimEventInfoLDTO extends TaxBaseApiDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4282565176020191797L;
	/** 日期 **/
	private String eventDate;
	/** 医疗事件类型 **/
	private String eventType;
	/** 医疗机构代码 **/
	private String hospitalCode;
	/**机构名称*/
	private String hospitalName;
	/**是否手术*/
	private String operation;
	
	/** 西医疾病 **/
	private List<ReqClaimWesternMedInfoDTO> westernMediList;
	/** 手术 */
	private List<ReqClaimOperationInfoDTO> operationList;
	/** 收据 **/
	private List<ReqClaimReceiptInfoDTO> receiptList;

	public List<ReqClaimWesternMedInfoDTO> getWesternMediList() {
		return westernMediList;
	}

	public String getEventDate() {
		return eventDate;
	}

	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}

	public void setWesternMediList(
			List<ReqClaimWesternMedInfoDTO> westernMediList) {
		this.westernMediList = westernMediList;
	}

	public List<ReqClaimOperationInfoDTO> getOperationList() {
		return operationList;
	}

	public void setOperationList(List<ReqClaimOperationInfoDTO> operationList) {
		this.operationList = operationList;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getHospitalCode() {
		return hospitalCode;
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

	public List<ReqClaimReceiptInfoDTO> getReceiptList() {
		return receiptList;
	}

	public void setReceiptList(List<ReqClaimReceiptInfoDTO> receiptList) {
		this.receiptList = receiptList;
	}

	public String getHospitalName() {
		return hospitalName;
	}

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

}
