package com.sinosoft.taxbenefit.api.dto.request.customer;

import com.sinosoft.taxbenefit.api.dto.request.base.BaseRequestTax;
/**
 * 
 * title：核心请求客户验证DTO（多笔）
 * @author zhangyu
 * @date 2016年3月17日 下午6:09:16
 */
public class ReqCustomerMoreCoreDTO extends BaseRequestTax{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3858269156644930834L;
	
	private ReqCustomerMoreDTO body;
	
	public void setBody(ReqCustomerMoreDTO body) {
		this.body = body;
	}
	@Override
	public Object getBody() {
		return body;
	}
}