package com.sinosoft.taxbenefit.api.dto.request.base;
/**
 * 核心请求报文
 * @author zhangke
 *
 */
@SuppressWarnings("serial")
public abstract class BaseRequestTax extends TaxBaseApiDTO{
	
	private CoreRequestHead requestHead;

	public CoreRequestHead getRequestHead() {
		return requestHead;
	}

	public void setRequestHead(CoreRequestHead requestHead) {
		this.requestHead = requestHead;
	}	
	
	public abstract Object getBody();
}
