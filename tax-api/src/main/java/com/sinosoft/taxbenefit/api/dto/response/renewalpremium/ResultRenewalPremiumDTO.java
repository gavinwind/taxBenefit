package com.sinosoft.taxbenefit.api.dto.response.renewalpremium;

import com.sinosoft.taxbenefit.api.base.HXResultCodeDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * 核心-续期保费信息上传返回数据（单个）
 * Title:ResultRenewalPremiumDTO
 * @author yangdongkai@outlook.com
 * @date 2016年3月7日 下午11:48:24
 */
public class ResultRenewalPremiumDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6280066557296963293L;
	//业务号
	private String bizNo;
	//续期费用确认编码
	private String feeSequenceNo;
	//返回编码
	private HXResultCodeDTO result;
	
	public HXResultCodeDTO getResult() {
		return result;
	}
	public void setResult(HXResultCodeDTO result) {
		this.result = result;
	}
	/**
	 * @return the bizNo
	 */
	public String getBizNo() {
		return bizNo;
	}
	/**
	 * @param bizNo the bizNo to set
	 */
	public void setBizNo(String bizNo) {
		this.bizNo = bizNo;
	}
	/**
	 * @return the feeSequenceNo
	 */
	public String getFeeSequenceNo() {
		return feeSequenceNo;
	}
	/**
	 * @param feeSequenceNo the feeSequenceNo to set
	 */
	public void setFeeSequenceNo(String feeSequenceNo) {
		this.feeSequenceNo = feeSequenceNo;
	}
	
}
