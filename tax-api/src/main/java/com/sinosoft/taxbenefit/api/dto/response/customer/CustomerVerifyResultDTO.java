package com.sinosoft.taxbenefit.api.dto.response.customer;

import com.sinosoft.taxbenefit.api.base.HXResultCodeDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;
/**
 * (核心)客户验证返回对象
 * @author zhangyu
 * @date 2016年3月10日 上午9:36:26
 */
public class CustomerVerifyResultDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8611450441054854710L;
	//业务号
	private String bizNo;
	//平台客户编码
	private String customerCode;
	//税优保单存在标志
	private String taxDiscountedExistIndi;
	
	private HXResultCodeDTO result;
	
	public HXResultCodeDTO getResult() {
		return result;
	}
	public void setResult(HXResultCodeDTO result) {
		this.result = result;
	}
	public String getBizNo() {
		return bizNo;
	}
	public void setBizNo(String bizNo) {
		this.bizNo = bizNo;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getTaxDiscountedExistIndi() {
		return taxDiscountedExistIndi;
	}
	public void setTaxDiscountedExistIndi(String taxDiscountedExistIndi) {
		this.taxDiscountedExistIndi = taxDiscountedExistIndi;
	}
	
}