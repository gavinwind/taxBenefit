package com.sinosoft.taxbenefit.api.dto.response.policytrans;

import java.util.List;

import com.sinosoft.taxbenefit.api.base.HXResultCodeDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * （核心）保单转出登记信息查询返回数据
 * @author SheChunMing
 */
public class ResHXPolicyTransOutRegisterDTO extends TaxBaseApiDTO{

	private static final long serialVersionUID = 851864318809647616L;

	// 转出保单
	private List<ResHXPolicyTransferOutDTO> policyTransferOutList;
	
	private HXResultCodeDTO result;

	public List<ResHXPolicyTransferOutDTO> getPolicyTransferOutList() {
		return policyTransferOutList;
	}

	public void setPolicyTransferOutList(
			List<ResHXPolicyTransferOutDTO> policyTransferOutList) {
		this.policyTransferOutList = policyTransferOutList;
	}

	public HXResultCodeDTO getResult() {
		return result;
	}

	public void setResult(HXResultCodeDTO result) {
		this.result = result;
	}
	
}
