package com.sinosoft.taxbenefit.api.dto.response.securityguard;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

public class ResultBody extends TaxBaseApiDTO{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7227223464209673499L;
	private ResultEndorsement endorsement;

	public ResultEndorsement getEndorsement() {
		return endorsement;
	}

	public void setEndorsement(ResultEndorsement endorsement) {
		this.endorsement = endorsement;
	}
	
	
	
	
	
	
}
