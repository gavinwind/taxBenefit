package com.sinosoft.taxbenefit.api.dto.response.accountchange;

import com.sinosoft.taxbenefit.api.base.HXResultCodeDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;
/**
 * 账户变更上传返回核心DTO
 * @author zhangyu
 * @date 2016年3月14日 下午1:26:55
 */
public class AccountChangeResultDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1217816864189440664L;
	//业务号
	private String bizNo;
	//公司费用 ID
	private String comFeeId;
	//账户变更确认编码 
	private String accountFeeSequenceNo;
	
	private HXResultCodeDTO result;
	
	public String getBizNo() {
		return bizNo;
	}
	public void setBizNo(String bizNo) {
		this.bizNo = bizNo;
	}
	public String getComFeeId() {
		return comFeeId;
	}
	public void setComFeeId(String comFeeId) {
		this.comFeeId = comFeeId;
	}
	public String getAccountFeeSequenceNo() {
		return accountFeeSequenceNo;
	}
	public void setAccountFeeSequenceNo(String accountFeeSequenceNo) {
		this.accountFeeSequenceNo = accountFeeSequenceNo;
	}
	public HXResultCodeDTO getResult() {
		return result;
	}
	public void setResult(HXResultCodeDTO result) {
		this.result = result;
	}
}