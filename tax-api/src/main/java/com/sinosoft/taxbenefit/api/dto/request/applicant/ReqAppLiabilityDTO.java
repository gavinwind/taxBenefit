package com.sinosoft.taxbenefit.api.dto.request.applicant;

import java.math.BigDecimal;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;


/**
 * 责任信息
 * 
 * @author zhangke
 *
 */
public class ReqAppLiabilityDTO extends TaxBaseApiDTO{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9052711254105894347L;
	/** 公司责任代码 **/
	private String liabilityCode;
	/** 责任保额 **/
	private BigDecimal liabilitySa;
	/** 责任状态 **/
	private String liabilityStatus;

	public String getLiabilityCode() {
		return liabilityCode;
	}

	public void setLiabilityCode(String liabilityCode) {
		this.liabilityCode = liabilityCode;
	}

	

	public BigDecimal getLiabilitySa() {
		return liabilitySa;
	}

	public void setLiabilitySa(BigDecimal liabilitySa) {
		this.liabilitySa = liabilitySa;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getLiabilityStatus() {
		return liabilityStatus;
	}

	public void setLiabilityStatus(String liabilityStatus) {
		this.liabilityStatus = liabilityStatus;
	}


}
