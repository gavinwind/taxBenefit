package com.sinosoft.taxbenefit.api.dto.response.customer;

import com.sinosoft.taxbenefit.api.base.HXResultCodeDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;
/**
 * (核心)税优验证返回对象
 * @author zhangyu
 * @date 2016年3月10日 上午9:38:09
 */
public class TaxBenVerifyResultDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7743609463637689691L;
	//业务号
	private String bizNo;
	// 客户编码
    private String customerNo;
	// 保单转移次数
    private Integer transferTimes;
    
    private HXResultCodeDTO result;
    
    public String getBizNo() {
		return bizNo;
	}
	public void setBizNo(String bizNo) {
		this.bizNo = bizNo;
	}
	public HXResultCodeDTO getResult() {
		return result;
	}
	public void setResult(HXResultCodeDTO result) {
		this.result = result;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public Integer getTransferTimes() {
		return transferTimes;
	}
	public void setTransferTimes(Integer transferTimes) {
		this.transferTimes = transferTimes;
	}
}