package com.sinosoft.taxbenefit.api.dto.request.base;


/**
 * 核心请求报文头
 * 
 * @author zhangke
 *
 */
public class CoreRequestHead {
	/** 交易流水号 */
	private String serialNo;
	/** 交易时间 */
	private String transDate;
	/** 地区代码 */
	private String areaCode;
	/** 签名 */
	private String signKey;
	/** 请求发起来源 */
	private String sourceCode;
	/** 请求记录数 */
	private String recordNum;
	/**请求接口服务编码*/
	private String transType;

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getSignKey() {
		return signKey;
	}

	public void setSignKey(String signKey) {
		this.signKey = signKey;
	}

	public String getSourceCode() {
		return sourceCode;
	}

	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}

	public String getTransDate() {
		return transDate;
	}

	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}

	public String getRecordNum() {
		return recordNum;
	}

	public void setRecordNum(String recordNum) {
		this.recordNum = recordNum;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getTransType() {
		return transType;
	}

	public void setTransType(String transType) {
		this.transType = transType;
	}
}
