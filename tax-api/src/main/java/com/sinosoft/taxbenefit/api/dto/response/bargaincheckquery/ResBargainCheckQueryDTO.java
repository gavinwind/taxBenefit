package com.sinosoft.taxbenefit.api.dto.response.bargaincheckquery;


import java.util.List;

import com.sinosoft.taxbenefit.api.base.HXResultCodeDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;
/**
 * 核心-交易核对信息查询返回DTO
 * Title:ResBargainCheckQuery
 * @author yangdongkai@outlook.com
 * @date 2016年3月10日 下午5:48:00
 */
public class ResBargainCheckQueryDTO extends TaxBaseApiDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 985363688316840405L;
	//汇总信息
	private ResBargainCheckTotalDTO total;
	//核对明细
	private List<ResBargainCheckDetailDTO> detail;
	//返回编码
	private HXResultCodeDTO result;
	/**
	 * @return the total
	 */
	public ResBargainCheckTotalDTO getTotal() {
		return total;
	}
	/**
	 * @param total the total to set
	 */
	public void setTotal(ResBargainCheckTotalDTO total) {
		this.total = total;
	}
	/**
	 * @return the detail
	 */
	public List<ResBargainCheckDetailDTO> getDetail() {
		return detail;
	}
	/**
	 * @param detail the detail to set
	 */
	public void setDetail(List<ResBargainCheckDetailDTO> detail) {
		this.detail = detail;
	}
	/**
	 * @return the result
	 */
	public HXResultCodeDTO getResult() {
		return result;
	}
	/**
	 * @param result the result to set
	 */
	public void setResult(HXResultCodeDTO result) {
		this.result = result;
	}	
	
}
