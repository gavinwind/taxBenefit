/**
 * @Copyright ®2015 Sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.ebiz.entry.report.config<br/>
 * @FileName: ENUM_BZ_RESPONSE_RESULT.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package  com.sinosoft.taxbenefit.api.config;


/**  
 * 【标准报文response报文的result字段枚举类】
 * @Description: 标准报文response报文的result字段枚举类
 * @author chenxin@sinosoft.com.cn
 * @date 2015年12月25日 下午9:33:36 
 * @version V1.0  
*/
public enum ENUM_BZ_RESPONSE_RESULT {
	SUCCESS("01","处理成功"),FAIL("02","处理失败"),EXCEPTION("03","处理中");
	/** 枚举code */
	private String code;
	/** 枚举value或者code说明 */
	private String value;
	ENUM_BZ_RESPONSE_RESULT(String code,String value){
		this.code = code;
		this.value = value;
	}
	/**
	 * 获得枚举code值
	 * @Title: code 
	 * @Description: TODO
	 * @return
	 */
	public String code(){
		return code;
	}
	/**
	 * 获得枚举value值
	 * @Title: decription 
	 * @Description: TODO
	 * @return
	 */
	public String description(){
		return value;
	}
	
	/**
	 * 根据key获得value
	 * @param key
	 * @return
	 */
	public static String getEnumValueByKey(String key){
		for(ENUM_BZ_RESPONSE_RESULT enumItem:ENUM_BZ_RESPONSE_RESULT.values()){
			if(key.equals(enumItem.code())){
				return enumItem.description();
			}
		}
		return "";
	}
}
