package com.sinosoft.taxbenefit.api.dto.response.policytrans;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;


/**
 * （核心）保单转移信息查询 -转出信息
 * @author SheChunMing
 */
public class ResHXPolicyTransferOutDTO extends TaxBaseApiDTO{
	private static final long serialVersionUID = -3078826942127629956L;
	// 业务号
	private String bizNo;
	// 转出保险公司代码
	private String companyCode;
	// 转出保险公司名称
	private String companyName;
	// 转出保单号
	private String policyNo;
	// 客户编号
	private String customerNo;
	// 客户信息
	private List<ResHXPolicyTransClientDTO> clientList;
	
	public String getBizNo() {
		return bizNo;
	}
	public void setBizNo(String bizNo) {
		this.bizNo = bizNo;
	}
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getPolicyNo() {
		return policyNo;
	}
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public List<ResHXPolicyTransClientDTO> getClientList() {
		return clientList;
	}
	public void setClientList(List<ResHXPolicyTransClientDTO> clientList) {
		this.clientList = clientList;
	}
}
