package com.sinosoft.taxbenefit.api.dto.request.claim;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * 理赔险种赔付信息
 * 
 * @author zhangke
 *
 */
public class ReqClaimCoveragePaymentInfoDTO extends TaxBaseApiDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3567456863300297706L;
	/** 公司产品组代码 **/
	private String coveragePackageCode;
	/** 平台险种分类代码 **/
	private String coverageType;
	/** 公司险种代码 **/
	private String comCoverageCode;
	/** 公司险种名称 **/
	private String comCoverageName;
	/** 理赔意见 **/
	private String claimOpinion;
	/** 理赔结论代码 **/
	private String claimConclusionCode;
	/** 非正常赔付原因 **/
	private String conclusionReason;
	/** 险种赔付金额 **/
	private String claimAmount;
	/** 责任赔付信息 **/
	private List<ReqClaimLiabilityPaymentInfoDTO> liabilityPaymentList;

	public String getCoveragePackageCode() {
		return coveragePackageCode;
	}

	public void setCoveragePackageCode(String coveragePackageCode) {
		this.coveragePackageCode = coveragePackageCode;
	}

	public String getCoverageType() {
		return coverageType;
	}

	public void setCoverageType(String coverageType) {
		this.coverageType = coverageType;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getComCoverageCode() {
		return comCoverageCode;
	}

	public void setComCoverageCode(String comCoverageCode) {
		this.comCoverageCode = comCoverageCode;
	}

	public String getComCoverageName() {
		return comCoverageName;
	}

	public void setComCoverageName(String comCoverageName) {
		this.comCoverageName = comCoverageName;
	}

	public String getClaimOpinion() {
		return claimOpinion;
	}

	public void setClaimOpinion(String claimOpinion) {
		this.claimOpinion = claimOpinion;
	}

	public String getClaimConclusionCode() {
		return claimConclusionCode;
	}

	public void setClaimConclusionCode(String claimConclusionCode) {
		this.claimConclusionCode = claimConclusionCode;
	}

	public String getConclusionReason() {
		return conclusionReason;
	}

	public void setConclusionReason(String conclusionReason) {
		this.conclusionReason = conclusionReason;
	}

	public String getClaimAmount() {
		return claimAmount;
	}

	public void setClaimAmount(String claimAmount) {
		this.claimAmount = claimAmount;
	}

	public List<ReqClaimLiabilityPaymentInfoDTO> getLiabilityPaymentList() {
		return liabilityPaymentList;
	}

	public void setLiabilityPaymentList(List<ReqClaimLiabilityPaymentInfoDTO> liabilityPaymentList) {
		this.liabilityPaymentList = liabilityPaymentList;
	}

}
