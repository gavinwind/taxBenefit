package com.sinosoft.taxbenefit.api.config;

public enum ENUM_SENDSERVICE_CODE {
	CORE("01","核心"),EBAO("02","中保信");
	private String code;
	/** 枚举value或者code说明 */
	private String value;
	ENUM_SENDSERVICE_CODE(String code,String value){
		this.code = code;
		this.value = value;
	}
	/**
	 * 获得枚举code值
	 * @Title: code 
	 * @Description: TODO
	 * @return
	 */
	public String code(){
		return code;
	}
	/**
	 * 获得枚举value值
	 * @Title: decription 
	 * @Description: TODO
	 * @return
	 */
	public String description(){
		return value;
	}
	
	/**
	 * 根据key获得value
	 * @param key
	 * @return
	 */
	public static String getEnumValueByKey(String key){
		for(ENUM_SENDSERVICE_CODE enumItem:ENUM_SENDSERVICE_CODE.values()){
			if(key.equals(enumItem.code())){
				return enumItem.description();
			}
		}
		return "";
	}
}
