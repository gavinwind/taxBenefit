package com.sinosoft.taxbenefit.api.dto.request.securityguard;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * 核保信息
 * 
 * @author zhanghaosh@sinosoft.com.cn
 *
 */
public class ReqkeepUnderwritingInfoDTO extends TaxBaseApiDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2989467921404884599L;
	/** 核保结论日期 Y **/
	private String underwritingDate;
	/** 核保决定 Y **/
	private String underwritingDecision;
	/** 核保描述 S **/
	private String underwritingDes;

	public String getUnderwritingDate() {
		return underwritingDate;
	}

	public void setUnderwritingDate(String underwritingDate) {
		this.underwritingDate = underwritingDate;
	}

	public String getUnderwritingDecision() {
		return underwritingDecision;
	}

	public void setUnderwritingDecision(String underwritingDecision) {
		this.underwritingDecision = underwritingDecision;
	}

	public String getUnderwritingDes() {
		return underwritingDes;
	}

	public void setUnderwritingDes(String underwritingDes) {
		this.underwritingDes = underwritingDes;
	}
}
