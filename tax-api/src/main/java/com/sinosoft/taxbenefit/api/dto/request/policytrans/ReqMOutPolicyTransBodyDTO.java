package com.sinosoft.taxbenefit.api.dto.request.policytrans;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * (核心)保单转移（多笔）转出申请上传请求
 * @author SheChunMing
 */
public class ReqMOutPolicyTransBodyDTO extends TaxBaseApiDTO{

	private static final long serialVersionUID = -2920323656863231904L;
	// 保单转移-转出信息
	private  List<ReqSOutPolicyTransferDTO> transferOutList;

	public List<ReqSOutPolicyTransferDTO> getTransferOutList() {
		return transferOutList;
	}

	public void setTransferOutList(List<ReqSOutPolicyTransferDTO> transferOutList) {
		this.transferOutList = transferOutList;
	}

}
