package com.sinosoft.taxbenefit.api.inter;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqTaxBeneVerifyInfoDTO;
/**
 * 
 * title：税优验证接口
 * @author zhangyu
 * @date 2016年3月4日 上午11:28:57
 */
public interface TaxBeneVerifyApiService {
	/**
	 *税优验证
	 * @param reqTaxBenefitInfoDTO
	 */
	ThirdSendResponseDTO taxBenefitService(ReqTaxBeneVerifyInfoDTO reqTaxBenefitInfoDTO,ParamHeadDTO paramHeadDTO);
}
