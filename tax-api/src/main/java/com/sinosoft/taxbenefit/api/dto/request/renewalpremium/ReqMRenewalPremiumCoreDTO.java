package com.sinosoft.taxbenefit.api.dto.request.renewalpremium;

import com.sinosoft.taxbenefit.api.dto.request.base.BaseRequestTax;
/**
 * 核心-续期保费上传请求(多个)
 * @author yangdongkai@outlook.com
 * @date 2016年3月17日 上午10:38:53
 */
public class ReqMRenewalPremiumCoreDTO extends BaseRequestTax {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6695649406996389962L;
	//报文体信息
	private ReqMPremiumInfoDTO body;
	
	public void setBody(ReqMPremiumInfoDTO body) {
		this.body = body;
	}
	@Override
	public Object getBody() {
		return body;
	}

}
