package com.sinosoft.taxbenefit.api.dto.request.policystate;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

public class PolicyStateInsured extends TaxBaseApiDTO {
/**
	 * 
	 */
	private static final long serialVersionUID = 4422391471153814681L;



	//	分单号 
	private String sequenceNo;
	
	

	private List<PolciyStateCoverage> coverageList;

	public List<PolciyStateCoverage> getCoverageList() {
		return coverageList;
	}

	public void setCoverageList(List<PolciyStateCoverage> coverageList) {
		this.coverageList = coverageList;
	}

	public String getSequenceNo() {
		return sequenceNo;
	}

	public void setSequenceNo(String sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
	
	
}
