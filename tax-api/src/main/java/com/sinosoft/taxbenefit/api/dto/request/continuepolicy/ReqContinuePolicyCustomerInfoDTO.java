package com.sinosoft.taxbenefit.api.dto.request.continuepolicy;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * 核心-客户信息
 * Title:ReqContinuePolicyCustomerInfoDTO
 * @author yangdongkai@outlook.com
 * @date 2016年3月11日 下午5:18:47
 */
public class ReqContinuePolicyCustomerInfoDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2665110475351983424L;
	//分单号
	private String sequenceNo;
	//被保人客户编码
	private String customerNo;
	//险种列表
	private List<ReqContinuePolicyCoverageInfoDTO> coverageList;
	/**
	 * @return the coverageList
	 */
	public List<ReqContinuePolicyCoverageInfoDTO> getCoverageList() {
		return coverageList;
	}
	/**
	 * @param coverageList the coverageList to set
	 */
	public void setCoverageList(List<ReqContinuePolicyCoverageInfoDTO> coverageList) {
		this.coverageList = coverageList;
	}
	/**
	 * @return the sequenceNo
	 */
	public String getSequenceNo() {
		return sequenceNo;
	}
	/**
	 * @param sequenceNo the sequenceNo to set
	 */
	public void setSequenceNo(String sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
	/**
	 * @return the customerNo
	 */
	public String getCustomerNo() {
		return customerNo;
	}
	/**
	 * @param customerNo the customerNo to set
	 */
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	
	
}
