package com.sinosoft.taxbenefit.api.dto.request.securityguard;

import java.math.BigDecimal;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;
/**
 * 险种责任
 * @author zhanghaosh@sinosoft.com.cn
 *
 */
public class ReqKeepLiabilityInfoDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6260793528865861798L;
	/** 公司责任代码 Y **/
	private String liabilityCode;
	/** 责任保额 S **/
	private BigDecimal liabilitySa;
	/** 责任状态 Y **/
	private String liabilityStatus;
	public String getLiabilityCode() {
		return liabilityCode;
	}
	public void setLiabilityCode(String liabilityCode) {
		this.liabilityCode = liabilityCode;
	}
	public BigDecimal getLiabilitySa() {
		return liabilitySa;
	}
	public void setLiabilitySa(BigDecimal liabilitySa) {
		this.liabilitySa = liabilitySa;
	}
	public String getLiabilityStatus() {
		return liabilityStatus;
	}
	public void setLiabilityStatus(String liabilityStatus) {
		this.liabilityStatus = liabilityStatus;
	}

}
