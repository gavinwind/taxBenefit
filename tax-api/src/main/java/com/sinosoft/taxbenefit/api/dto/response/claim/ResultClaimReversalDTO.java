package com.sinosoft.taxbenefit.api.dto.response.claim;

import com.sinosoft.taxbenefit.api.base.HXResultCodeDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * 理赔撤销上传返回核心DTO
 * 
 * @author zhangke
 *
 */
public class ResultClaimReversalDTO extends TaxBaseApiDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4133295364889489036L;
	private HXResultCodeDTO result ;

	public HXResultCodeDTO getClaimResult() {
		return result;
	}

	public void setClaimResult(HXResultCodeDTO result) {
		this.result = result;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
