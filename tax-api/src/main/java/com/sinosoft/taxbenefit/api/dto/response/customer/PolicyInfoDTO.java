package com.sinosoft.taxbenefit.api.dto.response.customer;

import java.math.BigDecimal;
import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;
/**
 * 保单信息
 * @author zhangyu
 * @date 2016年3月14日 上午9:19:16
 */
public class PolicyInfoDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3343501412823155167L;
		//保险公司代码			
		private String companyCode;
		//保单号
		private String policyNo;
		//保单类别			
		private String policyType;			
		//分单号
		private String sequenceNo;
		//保单投保来源
		private String policySource;
		//保单状态
		private String policyStatus;
		//保单生效日期
		private String effectiveDate;
		//保单满期日期
		private String expireDate;
		//保单终止日期
		private String terminationDate;
		//保单终止原因
		private String terminationReason;
		//是否有有效的税优识别码
		private String taxFavorIndi;
		//当前保单年度内赔付金额
		private BigDecimal periodPayment;
		//保单累计赔付金额 
		private BigDecimal policytotalPayment;
		//险种列表
		private List<CoverageInfoDTO> coverageList ;
		
		public List<CoverageInfoDTO> getCoverageList() {
			return coverageList;
		}
		public void setCoverageList(List<CoverageInfoDTO> coverageList) {
			this.coverageList = coverageList;
		}
		public String getCompanyCode() {
			return companyCode;
		}
		public void setCompanyCode(String companyCode) {
			this.companyCode = companyCode;
		}
		public String getPolicyNo() {
			return policyNo;
		}
		public void setPolicyNo(String policyNo) {
			this.policyNo = policyNo;
		}
		public String getPolicyType() {
			return policyType;
		}
		public void setPolicyType(String policyType) {
			this.policyType = policyType;
		}
		public String getSequenceNo() {
			return sequenceNo;
		}
		public void setSequenceNo(String sequenceNo) {
			this.sequenceNo = sequenceNo;
		}
		public String getPolicySource() {
			return policySource;
		}
		public void setPolicySource(String policySource) {
			this.policySource = policySource;
		}
		public String getPolicyStatus() {
			return policyStatus;
		}
		public void setPolicyStatus(String policyStatus) {
			this.policyStatus = policyStatus;
		}
		public String getEffectiveDate() {
			return effectiveDate;
		}
		public void setEffectiveDate(String effectiveDate) {
			this.effectiveDate = effectiveDate;
		}
		public String getExpireDate() {
			return expireDate;
		}
		public void setExpireDate(String expireDate) {
			this.expireDate = expireDate;
		}
		public String getTerminationDate() {
			return terminationDate;
		}
		public void setTerminationDate(String terminationDate) {
			this.terminationDate = terminationDate;
		}
		public String getTerminationReason() {
			return terminationReason;
		}
		public void setTerminationReason(String terminationReason) {
			this.terminationReason = terminationReason;
		}
		public String getTaxFavorIndi() {
			return taxFavorIndi;
		}
		public void setTaxFavorIndi(String taxFavorIndi) {
			this.taxFavorIndi = taxFavorIndi;
		}
		public BigDecimal getPeriodPayment() {
			return periodPayment;
		}
		public void setPeriodPayment(BigDecimal periodPayment) {
			this.periodPayment = periodPayment;
		}
		public BigDecimal getPolicytotalPayment() {
			return policytotalPayment;
		}
		public void setPolicytotalPayment(BigDecimal policytotalPayment) {
			this.policytotalPayment = policytotalPayment;
		}
}