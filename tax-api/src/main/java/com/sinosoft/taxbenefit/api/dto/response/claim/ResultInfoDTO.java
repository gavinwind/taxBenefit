package com.sinosoft.taxbenefit.api.dto.response.claim;

import com.sinosoft.taxbenefit.api.base.HXResultCodeDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * 理赔返回核心的报文
 * @author zhangke
 */
public class ResultInfoDTO extends TaxBaseApiDTO {
	
	private static final long serialVersionUID = -8358037017428988099L;
	/** 业务号 */
	private String bizNo;
	/** 理赔编码 */
	private String claimCodeP;
	
	private HXResultCodeDTO result;

	public String getBizNo() {
		return bizNo;
	}

	public void setBizNo(String bizNo) {
		this.bizNo = bizNo;
	}

	public String getClaimCodeP() {
		return claimCodeP;
	}

	public void setClaimCodeP(String claimCodeP) {
		this.claimCodeP = claimCodeP;
	}

	public HXResultCodeDTO getResult() {
		return result;
	}

	public void setResult(HXResultCodeDTO result) {
		this.result = result;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
