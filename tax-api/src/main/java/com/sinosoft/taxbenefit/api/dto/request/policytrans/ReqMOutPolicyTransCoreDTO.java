package com.sinosoft.taxbenefit.api.dto.request.policytrans;

import com.sinosoft.taxbenefit.api.dto.request.base.BaseRequestTax;

/**
 * （核心）保单转移（多笔）转出申请上传请求
 * @author SheChunMing
 *
 */
public class ReqMOutPolicyTransCoreDTO extends BaseRequestTax{
	
	private static final long serialVersionUID = -4082817605213089654L;
	
	private ReqMOutPolicyTransBodyDTO body;

	public void setBody(ReqMOutPolicyTransBodyDTO body) {
		this.body = body;
	}
	@Override
	public Object getBody() {
		return body;
	}
	
}
