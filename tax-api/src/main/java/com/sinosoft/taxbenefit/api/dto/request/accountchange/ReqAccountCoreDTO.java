package com.sinosoft.taxbenefit.api.dto.request.accountchange;

import com.sinosoft.taxbenefit.api.dto.request.base.BaseRequestTax;
/**
 * 
 * title：(核心)客户信息变更信息上传
 * @author zhangyu
 * @date 2016年3月10日 下午3:51:28
 */
public class ReqAccountCoreDTO extends BaseRequestTax{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4624173151262472560L;
	
	private ReqAccountInfoDTO body;
	
	public void setBody(ReqAccountInfoDTO body) {
		this.body = body;
	}
	
	@Override
	public Object getBody() {		
		return body;
	}	
}