package com.sinosoft.taxbenefit.api.dto.request.continuepolicy;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * Title:核心-核保信息
 * @author yangdongkai@outlook.com
 * @date 2016年3月2日 上午10:13:22
 */

public class ReqContinuePolicyUwinfoInfoDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3298014559050616335L;
	// 核保结论日期
	private String underwritingDate;
	// 核保决定
	private String underwritingDecision;
	// 核保描述
	private String underwritingDes;
	/**
	 * @return the underwritingDate
	 */
	public String getUnderwritingDate() {
		return underwritingDate;
	}
	/**
	 * @param underwritingDate the underwritingDate to set
	 */
	public void setUnderwritingDate(String underwritingDate) {
		this.underwritingDate = underwritingDate;
	}
	/**
	 * @return the underwritingDecision
	 */
	public String getUnderwritingDecision() {
		return underwritingDecision;
	}
	/**
	 * @param underwritingDecision the underwritingDecision to set
	 */
	public void setUnderwritingDecision(String underwritingDecision) {
		this.underwritingDecision = underwritingDecision;
	}
	/**
	 * @return the underwritingDes
	 */
	public String getUnderwritingDes() {
		return underwritingDes;
	}
	/**
	 * @param underwritingDes the underwritingDes to set
	 */
	public void setUnderwritingDes(String underwritingDes) {
		this.underwritingDes = underwritingDes;
	}
}
