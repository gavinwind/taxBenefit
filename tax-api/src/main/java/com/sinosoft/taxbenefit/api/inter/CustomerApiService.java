/**
 * @Copyright ®2015 Sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.ebiz.api.inter<br/>
 * @FileName: EdorService.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.taxbenefit.api.inter;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqCustomerInfoDTO;

/**
 * 客户相关信息服务类
 * @author SheChunMing
 */
public interface CustomerApiService {
	/**
	 * 客户身份信息上传(单笔)
	 * @return
	 */
	ThirdSendResponseDTO uploadCustomerInfo(ReqCustomerInfoDTO reqcustomerDTO,ParamHeadDTO paramHeadDTO);
	/**
	 * 客户身份信息上传(多笔)
	 * @param reqcustomersDTO
	 * @param paramHeadDTO
	 * @return
	 */
	ThirdSendResponseDTO uploadCustomersInfo(List<ReqCustomerInfoDTO> reqcustomersDTO,ParamHeadDTO paramHeadDTO);
	
}
