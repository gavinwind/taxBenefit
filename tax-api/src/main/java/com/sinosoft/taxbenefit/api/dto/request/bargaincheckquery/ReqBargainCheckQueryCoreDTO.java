package com.sinosoft.taxbenefit.api.dto.request.bargaincheckquery;

import com.sinosoft.taxbenefit.api.dto.request.base.BaseRequestTax;
/**
 * 核心-交易核对信息（报文体信息）
 * Title:ReqBargainCheckQueryCoreDTO
 * @author yangdongkai@outlook.com
 * @date 2016年3月10日 下午5:46:54
 */
public class ReqBargainCheckQueryCoreDTO extends BaseRequestTax{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3474643237579303914L;
	//报文体信息
	private ReqBargainCheckQueryDTO body;
	
	/**
	 * @param body the body to set
	 */
	public void setBody(ReqBargainCheckQueryDTO body) {
		this.body = body;
	}

	@Override
	public Object getBody() {
		return body;
	}
	
}
