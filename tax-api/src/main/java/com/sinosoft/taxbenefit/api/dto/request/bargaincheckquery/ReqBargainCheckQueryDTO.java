package com.sinosoft.taxbenefit.api.dto.request.bargaincheckquery;

import java.math.BigDecimal;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;
/**
 * 核心-交易核对信息查询DTO
 * Title:ReqBargainCheckQueryDTO
 * @author yangdongkai@outlook.com
 * @date 2016年3月10日 下午5:42:33
 */
public class ReqBargainCheckQueryDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = 692051292265286519L;
	//指定核对日期
	private String checkDate;
	//保险公司总记录数
	private BigDecimal requestTotalNum;
	//保险公司成功记录数
	private BigDecimal comSuccessNum;
	//保险公司失败记录数
	private BigDecimal comFailNum;
	/**
	 * @return the checkDate
	 */
	public String getCheckDate() {
		return checkDate;
	}
	/**
	 * @param checkDate the checkDate to set
	 */
	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}
	/**
	 * @return the requestTotalNum
	 */
	public BigDecimal getRequestTotalNum() {
		return requestTotalNum;
	}
	/**
	 * @param requestTotalNum the requestTotalNum to set
	 */
	public void setRequestTotalNum(BigDecimal requestTotalNum) {
		this.requestTotalNum = requestTotalNum;
	}
	/**
	 * @return the comSuccessNum
	 */
	public BigDecimal getComSuccessNum() {
		return comSuccessNum;
	}
	/**
	 * @param comSuccessNum the comSuccessNum to set
	 */
	public void setComSuccessNum(BigDecimal comSuccessNum) {
		this.comSuccessNum = comSuccessNum;
	}
	/**
	 * @return the comFailNum
	 */
	public BigDecimal getComFailNum() {
		return comFailNum;
	}
	/**
	 * @param comFailNum the comFailNum to set
	 */
	public void setComFailNum(BigDecimal comFailNum) {
		this.comFailNum = comFailNum;
	}
	
}
