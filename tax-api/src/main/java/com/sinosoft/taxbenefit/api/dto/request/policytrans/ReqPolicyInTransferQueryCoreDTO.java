package com.sinosoft.taxbenefit.api.dto.request.policytrans;

import com.sinosoft.taxbenefit.api.dto.request.base.BaseRequestTax;

/**
 * （核心）保单转移查询请求对象
 * @author SheChunMing
 */
public class ReqPolicyInTransferQueryCoreDTO extends BaseRequestTax{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6960656575536078462L;
	/** 报文体信息 */
	private ReqPolicyInTransferQueryDTO body;

	public void setBody(ReqPolicyInTransferQueryDTO body) {
		this.body = body;
	}

	@Override
	public Object getBody() {
		return body;
	}

}
