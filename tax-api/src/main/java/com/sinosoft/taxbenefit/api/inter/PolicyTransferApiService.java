package com.sinosoft.taxbenefit.api.inter;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqMInPolicyTransferDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqPolicyInTransferQueryDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqSInPolicyTransferDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqSOutPolicyTransferDTO;

/**
 * 保单转移 Dubbo 服务类
 * @author SheChunMing
 */
public interface PolicyTransferApiService {

	/**
	 * 保单转入申请上传(单笔)
	 * @param transInfo （核心）保单转移-转入信息(单笔)
	 * @param paramHead 核心请求头部参数
	 * @return 响应给核心的报文内容
	 */
	ThirdSendResponseDTO policyTransSInSettlement(ReqSInPolicyTransferDTO transInfo, ParamHeadDTO paramHead);
	/**
	 * 保单转入申请上传(多笔)
	 * @param transInfo （核心）保单转移-转入信息(多笔)
	 * @param paramHead 核心请求头部参数
	 * @return 响应给核心的报文内容
	 */
	ThirdSendResponseDTO policyTransMInSettlement(ReqMInPolicyTransferDTO transInfo, ParamHeadDTO paramHead);
	/**
	 * 保单转出登记上传(单笔)
	 * @param transInfo （核心）保单转移-转出信息(单笔)
	 * @param paramHead 核心请求头部参数
	 * @return 响应给核心的报文内容
	 */
	ThirdSendResponseDTO policyTransSOutSettlement(ReqSOutPolicyTransferDTO transInfo, ParamHeadDTO paramHead);
	
	/**
	 * 保单转出登记上传(多笔)
	 * @param transInfo （核心）保单转移-转出信息(多笔)
	 * @param paramHead 核心请求头部参数
	 * @return 响应给核心的报文内容
	 */
	ThirdSendResponseDTO policyTransMOutSettlement(List<ReqSOutPolicyTransferDTO> transInfoList, ParamHeadDTO paramHead);
	
	/**
	 * 保单转入余额信息查询
	 * @param transInfo （核心）保单转移-转出信息(单笔)
	 * @param paramHead 核心请求头部参数
	 * @return 响应给核心的报文内容
	 */
	ThirdSendResponseDTO policyTransInBalanceSettlement(ReqPolicyInTransferQueryDTO transInfo, ParamHeadDTO paramHead);
	
	/**
	 * 保单转出信息查询
	 * @param transInfo （核心）保单转移信息查询对象
	 * @param paramHead 核心请求头部参数
	 * @return 响应给核心的报文内容
	 */
	ThirdSendResponseDTO policyTransOutQuerySettlement(ReqPolicyInTransferQueryDTO transInfo, ParamHeadDTO paramHead);
	
	/**
	 * 保单转出登记信息查询
	 * @param transInfo （核心）保单转移信息查询对象
	 * @param paramHead 核心请求头部参数
	 * @return 响应给核心的报文内容
	 */
	ThirdSendResponseDTO policyTransOutRegisterQuerySettlement(ReqPolicyInTransferQueryDTO transInfo, ParamHeadDTO paramHead);

}
