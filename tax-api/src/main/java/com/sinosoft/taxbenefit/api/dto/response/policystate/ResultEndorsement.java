package com.sinosoft.taxbenefit.api.dto.response.policystate;

import com.sinosoft.taxbenefit.api.base.HXResultCodeDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * 保单状态变更
 * @author gyas-itchunmingsh
 *
 */
public class ResultEndorsement extends TaxBaseApiDTO{

	private static final long serialVersionUID = 4074710199091600570L;

	/** 业务号 **/
	private String bizNo;

	/** 保全编码 **/
	private String endorsementSequenceNo;

	private HXResultCodeDTO result;

	public HXResultCodeDTO getResult() {
		return result;
	}

	public void setResult(HXResultCodeDTO result) {
		this.result = result;
	}

	public String getBizNo() {
		return bizNo;
	}

	public void setBizNo(String bizNo) {
		this.bizNo = bizNo;
	}

	public String getEndorsementSequenceNo() {
		return endorsementSequenceNo;
	}

	public void setEndorsementSequenceNo(String endorsementSequenceNo) {
		this.endorsementSequenceNo = endorsementSequenceNo;
	}

}
