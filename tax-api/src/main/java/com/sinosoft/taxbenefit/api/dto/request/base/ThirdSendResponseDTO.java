package com.sinosoft.taxbenefit.api.dto.request.base;

/**
 * 
 * 响应给核心的报文内容
 * 
 * @author zhangke
 *
 */
public class ThirdSendResponseDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8630478163879141733L;
	/** 返回结果编码 */
	private String resultCode;
	/** 返回结果描述 */
	private String resultDesc;
	/** 返回信息内容 */
	private Object resJson;

	public Object getResJson() {
		return resJson;
	}

	public void setResJson(Object resJson) {
		this.resJson = resJson;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getResultDesc() {
		return resultDesc;
	}

	public void setResultDesc(String resultDesc) {
		this.resultDesc = resultDesc;
	}
	
}
