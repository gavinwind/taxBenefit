package com.sinosoft.taxbenefit.api.dto.request.securityguard;

import com.sinosoft.taxbenefit.api.dto.request.base.BaseRequestTax;

public class ReqkeepEndorsementDTO extends BaseRequestTax {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7778123993848953632L;
	private ReqKeepBodyDTO body;

	public void setBody(ReqKeepBodyDTO body) {
		this.body = body;
	}

	@Override
	public Object getBody() {
		return body;
	}

}
