package com.sinosoft.taxbenefit.api.dto.response.policytrans;

import com.sinosoft.taxbenefit.api.base.EBResultCodeDTO;
import com.sinosoft.taxbenefit.api.base.HXResultCodeDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * （核心）保单转入返回数据
 * @author SheChunMing
 *
 */
public class ResHXPolicyTransferResultDTO extends TaxBaseApiDTO{

	private static final long serialVersionUID = -1803541698173946728L;
	// 业务号
	private String bizNo;
	// 保单转出登记编码
	private String transferSequenceNo;
	// 返回编码
	private HXResultCodeDTO result;
	
	public String getBizNo() {
		return bizNo;
	}
	public void setBizNo(String bizNo) {
		this.bizNo = bizNo;
	}
	public String getTransferSequenceNo() {
		return transferSequenceNo;
	}
	public void setTransferSequenceNo(String transferSequenceNo) {
		this.transferSequenceNo = transferSequenceNo;
	}
	public HXResultCodeDTO getResult() {
		return result;
	}
	public void setResult(HXResultCodeDTO result) {
		this.result = result;
	}
}
