package com.sinosoft.taxbenefit.api.dto.request.policystate;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

public class PolicyStateLiability extends TaxBaseApiDTO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 425459298591208906L;
	/** 公司责任代码 **/
	private String liabilityCode;
	/** 责任状态 **/
	private String liabilityStatus;
	/** 责任终止原因 **/
	private String liabilityTermReason;
	
	public String getLiabilityCode() {
		return liabilityCode;
	}
	public void setLiabilityCode(String liabilityCode) {
		this.liabilityCode = liabilityCode;
	}
	public String getLiabilityStatus() {
		return liabilityStatus;
	}
	public void setLiabilityStatus(String liabilityStatus) {
		this.liabilityStatus = liabilityStatus;
	}
	public String getLiabilityTermReason() {
		return liabilityTermReason;
	}
	public void setLiabilityTermReason(String liabilityTermReason) {
		this.liabilityTermReason = liabilityTermReason;
	}

	
	
}
