package com.sinosoft.taxbenefit.api.dto.request.applicant;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * 核心请求承保撤销上传信息DTO
 * 
 * @author zhangke
 *
 */
public class ReqPolicyCancelDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1195611503448744885L;
	/**业务号*/
	private String bizNo;
	/**保单号*/
	private String policyNo;
	/**撤销日期*/
	private String cancelDate;
	/**撤销原因*/
	private String cancelReason;
	public String getPolicyNo() {
		return policyNo;
	}
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	public String getCancelDate() {
		return cancelDate;
	}
	public void setCancelDate(String cancelDate) {
		this.cancelDate = cancelDate;
	}
	public String getCancelReason() {
		return cancelReason;
	}
	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}
	public String getBizNo() {
		return bizNo;
	}
	public void setBizNo(String bizNo) {
		this.bizNo = bizNo;
	}
}
