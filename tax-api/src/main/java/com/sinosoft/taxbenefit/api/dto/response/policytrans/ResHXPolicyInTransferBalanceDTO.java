package com.sinosoft.taxbenefit.api.dto.response.policytrans;

import java.util.List;

import com.sinosoft.taxbenefit.api.base.HXResultCodeDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * （核心）保单转入余额信息查询返回数据
 * @author SheChunMing
 */
public class ResHXPolicyInTransferBalanceDTO extends TaxBaseApiDTO {

	private static final long serialVersionUID = -6785987857509738882L;
	// 转出保单
	private List<ResHXPolicyTransferOutDTO> transferOutList;
	
	private HXResultCodeDTO result;

	public List<ResHXPolicyTransferOutDTO> getTransferOutList() {
		return transferOutList;
	}

	public void setTransferOutList(List<ResHXPolicyTransferOutDTO> transferOutList) {
		this.transferOutList = transferOutList;
	}

	public HXResultCodeDTO getResult() {
		return result;
	}

	public void setResult(HXResultCodeDTO result) {
		this.result = result;
	}

}
