package com.sinosoft.taxbenefit.api.dto.request.policytrans;

import java.math.BigDecimal;
import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * （核心）保单转移-转出信息(单笔)
 * @author SheChunMing
 */
public class ReqSOutPolicyTransferDTO extends TaxBaseApiDTO{

	private static final long serialVersionUID = -7164641005158140926L;
	// 业务号
	private String bizNo;
	// 转出保单号
	private String policyNo;
	// 客户信息
	private List<ReqPolicyTransClientDTO> clientList;
	
	public String getBizNo() {
		return bizNo;
	}
	public void setBizNo(String bizNo) {
		this.bizNo = bizNo;
	}
	public String getPolicyNo() {
		return policyNo;
	}
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	public List<ReqPolicyTransClientDTO> getClientList() {
		return clientList;
	}
	public void setClientList(List<ReqPolicyTransClientDTO> clientList) {
		this.clientList = clientList;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}	
}
