package com.sinosoft.taxbenefit.api.dto.response.applicant;

import com.sinosoft.taxbenefit.api.base.HXResultCodeDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * 承保撤销 返回核心DTO
 * 
 * @author zhangke
 *
 */
public class ResultApplicantReversalDTO extends TaxBaseApiDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -156889983658476187L;
	private HXResultCodeDTO result;

	public HXResultCodeDTO getResult() {
		return result;
	}

	public void setResult(HXResultCodeDTO result) {
		this.result = result;
	}
}
