package com.sinosoft.taxbenefit.api.inter;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.securityguard.ReqEndorsementCancelInfoDTO;

/**
 * 保全撤销
 * 
 * @author zhanghao
 *
 */
public interface PreservationOfCancelApiService {

	public ThirdSendResponseDTO PreservationOfChange(
			ReqEndorsementCancelInfoDTO body, ParamHeadDTO paramHeadDTO);

}
