package com.sinosoft.taxbenefit.api.inter;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqMContinuePolicyInfoDTO;

/**
 * Title:续保信息上传服务接口
 * @author yangdongkai@outlook.com
 * @date 2016年3月2日 下午2:58:23
 */
public interface ContinuePolicyInfoApiService {
	/**
	 * 续保信息上传
	 * @param policyDTO
	 */
	ThirdSendResponseDTO continuePolicySettlement(ReqContinuePolicyInfoDTO continuePolicyInfo,ParamHeadDTO paramHead);
	/**
	 * 续保信息上传（多个）
	 * @param continueMPolicyInfo
	 * @param paramHead
	 * @return
	 */
	ThirdSendResponseDTO continueMPolicySettlement(ReqMContinuePolicyInfoDTO continueMPolicyInfo,ParamHeadDTO paramHead);
}
