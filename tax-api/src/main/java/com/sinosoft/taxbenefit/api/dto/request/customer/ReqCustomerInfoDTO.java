package com.sinosoft.taxbenefit.api.dto.request.customer;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;
/**
 * 
 * title： 客户信息上传请求对象
 * @author zhangyu
 * @date 2016年3月18日 下午2:36:00
 */
public class ReqCustomerInfoDTO extends TaxBaseApiDTO{	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3627017676602658388L;
	//业务号
	private String bizNo;
	//姓名
	private String name;
	//性别
	private String gender;
	//生日
	private String  birthday;
	//证件类型
	private String certiType;
	//证件号码
	private String certiNo;
	// 税优保单存在标记
	private String taxExistFlag;
	public String getTaxExistFlag() {
		return taxExistFlag;
	}
	public void setTaxExistFlag(String taxExistFlag) {
		this.taxExistFlag = taxExistFlag;
	}
	public String getBizNo() {
		return bizNo;
	}
	public void setBizNo(String bizNo) {
		this.bizNo = bizNo;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCertiType() {
		return certiType;
	}
	public void setCertiType(String certiType) {
		this.certiType = certiType;
	}
	public String getCertiNo() {
		return certiNo;
	}
	public void setCertiNo(String certiNo) {
		this.certiNo = certiNo;
	}	
}