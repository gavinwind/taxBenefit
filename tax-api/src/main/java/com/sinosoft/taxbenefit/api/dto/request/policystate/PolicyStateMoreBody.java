package com.sinosoft.taxbenefit.api.dto.request.policystate;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.BaseRequestTax;

public class PolicyStateMoreBody extends BaseRequestTax {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4799502866893952549L;
	private List<PolicyStateEndorsement> body;
	


	public void setBody(List<PolicyStateEndorsement> body) {
		this.body = body;
	}

	@Override
	public Object getBody() {
		return body;
	}
	
	
	
	
}
