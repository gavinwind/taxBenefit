package com.sinosoft.taxbenefit.api.dto.request.policytrans;

import com.sinosoft.taxbenefit.api.dto.request.base.BaseRequestTax;

/**
 * （核心）保单转移（单笔）转出申请上传请求
 * @author SheChunMing
 *
 */
public class ReqSOutPolicyTransCoreDTO extends BaseRequestTax{
	
	private static final long serialVersionUID = 9090644769041633528L;
	
	private ReqSOutPolicyTransferDTO body;
	
	public void setBody(ReqSOutPolicyTransferDTO body) {
		this.body = body;
	}
	@Override
	public Object getBody() {
		return body;
	}

}
