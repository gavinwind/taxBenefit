package com.sinosoft.taxbenefit.api.dto.request.policytrans;

import com.sinosoft.taxbenefit.api.dto.request.base.BaseRequestTax;

/**
 * （核心）保单转移（多笔）外部转入申请上传请求
 * @author SheChunMing
 */
public class ReqMInPolicyTransCoreDTO extends BaseRequestTax {
	/**
	 * 
	 */
	private static final long serialVersionUID = -355791962968566333L;
	/** 报文体信息 */
	private ReqMInPolicyTransferDTO body;
	
	public void setBody(ReqMInPolicyTransferDTO body) {
		this.body = body;
	}
	@Override
	public Object getBody() {
		return body;
	}

}
