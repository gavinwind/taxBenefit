package com.sinosoft.taxbenefit.api.dto.request.claim;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * 核心理赔撤销请求信息DTO
 * 
 * @author zhangke
 *
 */
public class ReqClaimReversalInfoDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7807570092138823107L;
	/** 理赔赔案号 **/
	private String claimNo;
	/** 撤销日期 */
	private String cancelDate;
	/** 撤销原因 */
	private String cancellationReason;

	public String getClaimNo() {
		return claimNo;
	}

	public void setClaimNo(String claimNo) {
		this.claimNo = claimNo;
	}

	public String getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(String cancelDate) {
		this.cancelDate = cancelDate;
	}

	public String getCancellationReason() {
		return cancellationReason;
	}

	public void setCancellationReason(String cancellationReason) {
		this.cancellationReason = cancellationReason;
	}
}
