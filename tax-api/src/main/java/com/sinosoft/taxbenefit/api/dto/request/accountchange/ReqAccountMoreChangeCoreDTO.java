package com.sinosoft.taxbenefit.api.dto.request.accountchange;

import com.sinosoft.taxbenefit.api.dto.request.base.BaseRequestTax;
/**
 * 
 * title：(核心)客户信息变更信息上传(多笔)
 * @author zhangyu
 * @date 2016年3月18日 下午1:29:53
 */
public class ReqAccountMoreChangeCoreDTO extends BaseRequestTax{
	/**
	 * 
	 */
	private static final long serialVersionUID = 695360038073923741L;
	
	private ReqAccountMoreChangeDTO body;
	
	@Override
	public Object getBody() {
		return body;
	}

	public void setBody(ReqAccountMoreChangeDTO body) {
		this.body = body;
	}
}