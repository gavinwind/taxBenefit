package com.sinosoft.taxbenefit.api.inter;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.common.FaultTaskDTO;
import com.sinosoft.taxbenefit.api.dto.common.TaxFaultTaskInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.TaxFaulTaskDTO;

/**
 * 异常任务服务接口
 * 
 * @author zhangke
 *
 */
public interface TaxFaultTaskApiService {
	/**
	 * 添加新的异常任务
	 * 
	 * @param taxFaultTaskDTO
	 */
	void addTaxFaultTask(TaxFaulTaskDTO taxFaultTaskDTO);

	/**
	 * 根据批次号查询异常任务信息
	 * 
	 * @param serialNo
	 *            流水号
	 * @return 处理状态为成功:返回成功报文 否则返回null
	 */
	FaultTaskDTO queryTaxFaultTaskBySerialNo(String serialNo);

	

}
