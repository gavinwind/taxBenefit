package com.sinosoft.taxbenefit.api.dto.request.base;

/**
 * 响应核心报文
 * 
 * @author zhangke
 *
 */
public class CoreResponseTax {
	private CoreResponseHead head;
	private Object body;

	public CoreResponseHead getHead() {
		return head;
	}

	public void setHead(CoreResponseHead head) {
		this.head = head;
	}

	public Object getBody() {
		return body;
	}

	public void setBody(Object body) {
		this.body = body;
	}

}
