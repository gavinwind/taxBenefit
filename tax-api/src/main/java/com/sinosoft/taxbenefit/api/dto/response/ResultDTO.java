package com.sinosoft.taxbenefit.api.dto.response;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * （核心）逐单返回结果对象
 * @author SheChunMing
 */
public class ResultDTO extends TaxBaseApiDTO{
	
	private static final long serialVersionUID = -5951561034411860949L;
	// 逐单返回类型代码
	private String resultCode;
	// 结果信息
	private String message;

	public String getResultCode() {
		return resultCode;
	}
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
