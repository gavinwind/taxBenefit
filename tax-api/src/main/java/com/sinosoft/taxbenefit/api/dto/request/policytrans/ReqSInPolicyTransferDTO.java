package com.sinosoft.taxbenefit.api.dto.request.policytrans;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;


/**
 * （核心）保单转移-转入信息(单笔)
 * @author SheChunMing
 */
public class ReqSInPolicyTransferDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = -465170961795180857L;
	// 转移申请日期
	private String transApplyDate;
	// 开户行名称
	private String bankName;
	// 户名
	private String accountHolder;
	// 账号
	private String accountNo;
	// 保单转移联系人
	private String contactName;
	// 电话
	private String contactTele;
	// Email
	private String contactEmail;
	// 转出信息
	private ReqPolicyTransferOutDTO transferOut;
	
	public String getTransApplyDate() {
		return transApplyDate;
	}
	public void setTransApplyDate(String transApplyDate) {
		this.transApplyDate = transApplyDate;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getAccountHolder() {
		return accountHolder;
	}
	public void setAccountHolder(String accountHolder) {
		this.accountHolder = accountHolder;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getContactTele() {
		return contactTele;
	}
	public void setContactTele(String contactTele) {
		this.contactTele = contactTele;
	}
	public String getContactEmail() {
		return contactEmail;
	}
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	public ReqPolicyTransferOutDTO getTransferOut() {
		return transferOut;
	}
	public void setTransferOut(ReqPolicyTransferOutDTO transferOut) {
		this.transferOut = transferOut;
	}
}
