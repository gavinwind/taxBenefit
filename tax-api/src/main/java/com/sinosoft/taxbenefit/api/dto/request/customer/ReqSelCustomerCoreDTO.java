package com.sinosoft.taxbenefit.api.dto.request.customer;

import com.sinosoft.taxbenefit.api.dto.request.base.BaseRequestTax;
/**
 *
 * title： (核心)请求客户查询DTO
 * @author zhangyu
 * @date 2016年3月12日 下午6:50:01
 */
public class ReqSelCustomerCoreDTO extends BaseRequestTax{	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4820576095486440029L;
	
	private ReqSelCustomerInfoDTO body;

	@Override
	public Object getBody() {		
		return body;
	}
	public void setBody(ReqSelCustomerInfoDTO body) {
		this.body = body;
	}
}