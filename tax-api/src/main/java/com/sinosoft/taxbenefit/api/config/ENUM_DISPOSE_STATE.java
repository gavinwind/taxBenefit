package com.sinosoft.taxbenefit.api.config;
/**
 * 任务处理状态
 * @author zhangke
 *
 */
public enum ENUM_DISPOSE_STATE {
	SUCCES("01","成功"),FAIL("02","失败"),WAIT("03","待处理");
	private final String code;
	private final String desc;
	ENUM_DISPOSE_STATE(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public String code() {
		return code;
	}

	public String desc() {
		return desc;
	}
	
}
