package com.sinosoft.taxbenefit.api.dto.response.policytrans;

import java.util.List;

import com.sinosoft.taxbenefit.api.base.HXResultCodeDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * （核心）保单转出信息查询返回数据
 * @author SheChunMing
 */
public class ReqHXPolicyInTransferQueryDTO  extends TaxBaseApiDTO{

	private static final long serialVersionUID = -3884947815989881152L;

	// 转出保单
	private List<ResHXPolicyTransferInDTO> policyTransferInList;
	
	private HXResultCodeDTO result;

	public List<ResHXPolicyTransferInDTO> getPolicyTransferInList() {
		return policyTransferInList;
	}

	public void setPolicyTransferInList(
			List<ResHXPolicyTransferInDTO> policyTransferInList) {
		this.policyTransferInList = policyTransferInList;
	}

	public HXResultCodeDTO getResult() {
		return result;
	}

	public void setResult(HXResultCodeDTO result) {
		this.result = result;
	}
	
}
