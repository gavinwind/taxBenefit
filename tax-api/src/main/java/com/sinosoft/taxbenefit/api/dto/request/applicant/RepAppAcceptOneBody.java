package com.sinosoft.taxbenefit.api.dto.request.applicant;

import com.sinosoft.taxbenefit.api.dto.request.base.BaseRequestTax;

/**
 * 投保信息上传请求(单笔)DTO
 * @author zhangke
 *
 */
public class RepAppAcceptOneBody extends BaseRequestTax{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5776353584905488072L;
	private ReqAppPolicyDTO body;
	
	public void setBody(ReqAppPolicyDTO body) {
		this.body = body;
	}
	@Override
	public Object getBody() {	
		return body;
	}
}
