package com.sinosoft.taxbenefit.api.inter;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimReversalInfoDTO;

/**
 * 理赔接口服务
 * 
 * @author zhangke
 *
 */
public interface ClaimInfoApiService {
	/**
	 * 理赔信息上传(单个上传)
	 * @param reqClaimInfoDTO
	 * @param paramHeadDTO
	 *  @return
	 */
	ThirdSendResponseDTO claimSettlement(ReqClaimInfoDTO reqClaimInfoDTO, ParamHeadDTO paramHeadDTO);
	
	/**
	 * 理赔信息上传(多个上传)
	 * @param reqClaimInfoDTOList
	 * @param paramHeadDTO
	 * @return
	 */
	ThirdSendResponseDTO claimSettlementM(List<ReqClaimInfoDTO>reqClaimInfoDTOList, ParamHeadDTO paramHeadDTO);

	/**
	 * 理赔案件撤销
	 * @param reqClaimReversalBodyDTO 
	 * @return 
	 */
	ThirdSendResponseDTO claimReversal(ReqClaimReversalInfoDTO reqClaimReversalInfoDTO, ParamHeadDTO paramHeadDTO);
}
