package com.sinosoft.taxbenefit.api.dto.request.customer;

import com.sinosoft.taxbenefit.api.dto.request.base.BaseRequestTax;
/**
 * 
 * title：(核心)税优验证请求
 * @author zhangyu
 * @date 2016年3月10日 上午9:35:42
 */
public class ReqTaxBeneVerifyCoreDTO extends BaseRequestTax{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5868859761947945367L;
	
	private ReqTaxBeneVerifyInfoDTO body;
	@Override
	public Object getBody() {
		
		return body;
	}
	public void setBody(ReqTaxBeneVerifyInfoDTO body) {
		this.body = body;
	}	
}