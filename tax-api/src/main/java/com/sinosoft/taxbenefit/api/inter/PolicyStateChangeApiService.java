package com.sinosoft.taxbenefit.api.inter;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.policystate.PolicyStateEndorsement;

public interface PolicyStateChangeApiService {


	ThirdSendResponseDTO policyStateChange(List<PolicyStateEndorsement> policybody,
			ParamHeadDTO paramHeadDTO);
	
}
