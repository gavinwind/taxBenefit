package com.sinosoft.taxbenefit.api.dto.request.applicant;

import com.sinosoft.taxbenefit.api.dto.request.base.BaseRequestTax;

/**
 * 核心请求承保撤销上传(最外层)DTO
 * 
 * @author zhangke
 *
 */
public class ReqPolicyReversalDTO extends BaseRequestTax {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2965092184673127924L;
	private ReqPolicyCancelDTO body;

	public void setBody(ReqPolicyCancelDTO body) {
		this.body = body;
	}

	@Override
	public Object getBody() {
		return body;
	}
}
