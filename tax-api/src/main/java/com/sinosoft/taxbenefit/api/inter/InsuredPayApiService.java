package com.sinosoft.taxbenefit.api.inter;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqInsuredPayInfoDTO;
/**
 * 
 * title：被保人既往理赔额查询
 * @author zhangyu
 * @date 2016年3月4日 上午9:41:10
 */
public interface InsuredPayApiService {
	/**
	 * 被保人既往理賠額
	 * @param reqInsuredPayInfoDTO
	 */
	ThirdSendResponseDTO InsuredPayInfo(ReqInsuredPayInfoDTO reqInsuredPayInfoDTO,ParamHeadDTO paramHeadDTO);

}
