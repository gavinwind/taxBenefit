package com.sinosoft.taxbenefit.api.dto.request.claim;

import com.sinosoft.taxbenefit.api.dto.request.base.BaseRequestTax;

/**
 * 核心请求理赔撤销请求报文DTO(最外层)
 * 
 * @author zhangke
 *
 */
public class ReqClaimReversalDTO extends BaseRequestTax {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3603583367047511444L;
	/** 报文体信息 */
	private ReqClaimReversalInfoDTO body;

	public void setBody(ReqClaimReversalInfoDTO body) {
		this.body = body;
	}

	@Override
	public Object getBody() {
		return body;
	}
}
