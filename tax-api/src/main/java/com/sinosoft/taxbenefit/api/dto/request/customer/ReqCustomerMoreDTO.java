package com.sinosoft.taxbenefit.api.dto.request.customer;

import java.util.List;
import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;
/**
 * 
 * title：（多笔）客户验证列表DTO
 * @author zhangyu
 * @date 2016年3月17日 下午5:13:54
 */
public class ReqCustomerMoreDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5540793420633080821L;
	
	private List<ReqCustomerInfoDTO> clientList;

	public List<ReqCustomerInfoDTO> getClientList() {
		return clientList;
	}

	public void setClientList(List<ReqCustomerInfoDTO> clientList) {
		this.clientList = clientList;
	}
}