package com.sinosoft.taxbenefit.api.dto.response.customer;

import java.math.BigDecimal;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;
/**
 * 险种信息
 * @author zhangyu
 * @date 2016年3月14日 上午9:18:12
 */
public class CoverageInfoDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6353888530852799686L;
		//公司险种名称
		private String comCoverageName;
	    //险种保额
		private BigDecimal sa;
		//核保决定
		private String underwritingDecision;
		
		public String getComCoverageName() {
			return comCoverageName;
		}
		public void setComCoverageName(String comCoverageName) {
			this.comCoverageName = comCoverageName;
		}
		public BigDecimal getSa() {
			return sa;
		}
		public void setSa(BigDecimal sa) {
			this.sa = sa;
		}
		public String getUnderwritingDecision() {
			return underwritingDecision;
		}
		public void setUnderwritingDecision(String underwritingDecision) {
			this.underwritingDecision = underwritingDecision;
		}
}