package com.sinosoft.taxbenefit.api.dto.response.applicant;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * (核心)承保上传返回数据
 * @author zhangke
 *
 */
public class ResultApplicantDTO extends TaxBaseApiDTO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9193599916415868913L;
	/** 业务号 **/
	private String bizNo;
	/** 保单号 **/
	private String policyNo;
	
	private List<ResultClient> clientList;
	
	
	public List<ResultClient> getClientList() {
		return clientList;
	}
	public void setClientList(List<ResultClient> clientList) {
		this.clientList = clientList;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getBizNo() {
		return bizNo;
	}
	public void setBizNo(String bizNo) {
		this.bizNo = bizNo;
	}
	public String getPolicyNo() {
		return policyNo;
	}
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	
}
