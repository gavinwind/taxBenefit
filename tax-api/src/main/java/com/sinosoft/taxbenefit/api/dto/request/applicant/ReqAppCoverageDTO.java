package com.sinosoft.taxbenefit.api.dto.request.applicant;

import java.math.BigDecimal;
import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

public class ReqAppCoverageDTO extends TaxBaseApiDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 698033401465888151L;
	// 核保
	private ReqAppUnderwritingDTO underwriting;

	// 责任
	private List<ReqAppLiabilityDTO> liabilityList;

	/** 产品组代码 Y **/
	private String coveragePackageCode;
	/** 险种代码 Y **/
	private String comCoverageCode;
	/** 险种名称 Y **/
	private String comCoverageName;
	/** 险种保费 Y **/
	private BigDecimal initialPremium;
	/** 险种当期风险保费 Y **/
	private BigDecimal riskPremium;
	/** 投保年龄 Y **/
	private Integer ageOfInception;
	/** 险种申请日期 Y **/
	private String coverageApplicationDate;
	/** 险种生效日期 **/
	private String coverageEffectiveDate;
	/** 险种满期日期 Y **/
	private String coverageExpirationDate;
	/** 主附险标志 Y **/
	private String coverageType;
	/** 险种保额 Y **/
	private BigDecimal sa;
	/** 自动续保标志 Y **/
	private String renewIndi;
	/** 险种状态 Y **/
	private String coverageStatus;

	public ReqAppUnderwritingDTO getUnderwriting() {
		return underwriting;
	}

	public void setUnderwriting(ReqAppUnderwritingDTO underwriting) {
		this.underwriting = underwriting;
	}

	

	public List<ReqAppLiabilityDTO> getLiabilityList() {
		return liabilityList;
	}

	public void setLiabilityList(List<ReqAppLiabilityDTO> liabilityList) {
		this.liabilityList = liabilityList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getCoveragePackageCode() {
		return coveragePackageCode;
	}

	public void setCoveragePackageCode(String coveragePackageCode) {
		this.coveragePackageCode = coveragePackageCode;
	}

	public String getComCoverageCode() {
		return comCoverageCode;
	}

	public void setComCoverageCode(String comCoverageCode) {
		this.comCoverageCode = comCoverageCode;
	}

	public String getComCoverageName() {
		return comCoverageName;
	}

	public void setComCoverageName(String comCoverageName) {
		this.comCoverageName = comCoverageName;
	}

	public BigDecimal getInitialPremium() {
		return initialPremium;
	}

	public void setInitialPremium(BigDecimal initialPremium) {
		this.initialPremium = initialPremium;
	}

	public BigDecimal getRiskPremium() {
		return riskPremium;
	}

	public void setRiskPremium(BigDecimal riskPremium) {
		this.riskPremium = riskPremium;
	}

	public Integer getAgeOfInception() {
		return ageOfInception;
	}

	public void setAgeOfInception(Integer ageOfInception) {
		this.ageOfInception = ageOfInception;
	}

	public String getCoverageApplicationDate() {
		return coverageApplicationDate;
	}

	public void setCoverageApplicationDate(String coverageApplicationDate) {
		this.coverageApplicationDate = coverageApplicationDate;
	}

	public String getCoverageEffectiveDate() {
		return coverageEffectiveDate;
	}

	public void setCoverageEffectiveDate(String coverageEffectiveDate) {
		this.coverageEffectiveDate = coverageEffectiveDate;
	}

	public String getCoverageExpirationDate() {
		return coverageExpirationDate;
	}

	public void setCoverageExpirationDate(String coverageExpirationDate) {
		this.coverageExpirationDate = coverageExpirationDate;
	}

	public String getCoverageType() {
		return coverageType;
	}

	public void setCoverageType(String coverageType) {
		this.coverageType = coverageType;
	}

	public BigDecimal getSa() {
		return sa;
	}

	public void setSa(BigDecimal sa) {
		this.sa = sa;
	}

	public String getRenewIndi() {
		return renewIndi;
	}

	public void setRenewIndi(String renewIndi) {
		this.renewIndi = renewIndi;
	}

	public String getCoverageStatus() {
		return coverageStatus;
	}

	public void setCoverageStatus(String coverageStatus) {
		this.coverageStatus = coverageStatus;
	}
}
