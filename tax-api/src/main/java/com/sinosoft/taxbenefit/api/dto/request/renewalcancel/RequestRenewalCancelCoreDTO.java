package com.sinosoft.taxbenefit.api.dto.request.renewalcancel;

import com.sinosoft.taxbenefit.api.dto.request.base.BaseRequestTax;
/**
 * 核心-续保撤销上传请求
 * @author yangdongkai@outlook.com
 * @date 2016年3月22日 下午3:51:22
 */
public class RequestRenewalCancelCoreDTO extends BaseRequestTax{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7910395616182466146L;
	//报文体信息
	private RequestRenewalCancelDTO body;
	/**
	 * @param body the body to set
	 */
	public void setBody(RequestRenewalCancelDTO body) {
		this.body = body;
	}
	@Override
	public Object getBody() {
		return body;
	}
}