package com.sinosoft.taxbenefit.api.dto.request.customer;

import com.sinosoft.taxbenefit.api.dto.request.base.BaseRequestTax;
/**
 * 
 * title：(核心)客户验证上传请求
 * @author zhangyu
 * @date 2016年3月18日 下午2:36:18
 */
public class ReqCustomerCoreDTO extends BaseRequestTax{	
	
	private static final long serialVersionUID = 8174261783880066311L;
	
	private ReqCustomerInfoDTO body;

	public void setBody(ReqCustomerInfoDTO body) {
		this.body = body;
	}
	@Override
	public Object getBody() {		
		return body;
	}
}