package com.sinosoft.taxbenefit.api.dto.request.applicant;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * 被保人信息DTO
 * @author zhangke
 *
 */
public class ReqAppInsuredDTO extends TaxBaseApiDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4982632764833555863L;
	/** 分单号 Y **/
	private String sequenceNo;
	/** 被保人客户编码 Y **/
	private String customerNo;
	/** 被保人客户姓名 Y **/
	private String name;
	/** 性别 Y **/
	private String gender;
	/** 出生日期 Y **/
	private String birthday;
	/** 被保险人证件类型 Y **/
	private String certiType;
	/** 被保险人证件号码 Y **/
	private String certiNo;
	/** 国籍 Y **/
	private String nationality;
	/** 手机号码 Y **/
	private String mobileNo;
	/** 常驻地 Y **/
	private String residencePlace;
	/** 医保标识 Y **/
	private String healthFlag;
	/** 社保卡号 **/
	private String socialcareNo;
	/** 职业代码 Y **/
	private String jobCode;
	/** 被保险人类型 Y **/
	private String insuredType;
	/** 主被保人编码 S **/
	private String mainInsuredNo;
	/** 投保人与被保险人关系 Y **/
	private String phInsuredRelation;
	/** 是否指定受益人 **/
	private String beneficiaryIndi;
	//	缴费
	private ReqAppPremiumDTO premium;
	// 受益人
	private List<ReqAppBeneficiaryDTO> beneficiaryList;
	// 险种
	private List<ReqAppCoverageDTO> coverageList;

	public List<ReqAppBeneficiaryDTO> getBeneficiaryList() {
		return beneficiaryList;
	}

	public void setBeneficiaryList(List<ReqAppBeneficiaryDTO> beneficiaryList) {
		this.beneficiaryList = beneficiaryList;
	}

	public List<ReqAppCoverageDTO> getCoverageList() {
		return coverageList;
	}

	public void setCoverageList(List<ReqAppCoverageDTO> coverageList) {
		this.coverageList = coverageList;
	}

	

	public String getSequenceNo() {
		return sequenceNo;
	}

	public void setSequenceNo(String sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getCertiType() {
		return certiType;
	}

	public void setCertiType(String certiType) {
		this.certiType = certiType;
	}

	public String getCertiNo() {
		return certiNo;
	}

	public void setCertiNo(String certiNo) {
		this.certiNo = certiNo;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getResidencePlace() {
		return residencePlace;
	}

	public void setResidencePlace(String residencePlace) {
		this.residencePlace = residencePlace;
	}

	public String getHealthFlag() {
		return healthFlag;
	}

	public void setHealthFlag(String healthFlag) {
		this.healthFlag = healthFlag;
	}

	public String getSocialcareNo() {
		return socialcareNo;
	}

	public void setSocialcareNo(String socialcareNo) {
		this.socialcareNo = socialcareNo;
	}

	public String getJobCode() {
		return jobCode;
	}

	public void setJobCode(String jobCode) {
		this.jobCode = jobCode;
	}

	public String getInsuredType() {
		return insuredType;
	}

	public void setInsuredType(String insuredType) {
		this.insuredType = insuredType;
	}

	public String getMainInsuredNo() {
		return mainInsuredNo;
	}

	public void setMainInsuredNo(String mainInsuredNo) {
		this.mainInsuredNo = mainInsuredNo;
	}

	public String getPhInsuredRelation() {
		return phInsuredRelation;
	}

	public void setPhInsuredRelation(String phInsuredRelation) {
		this.phInsuredRelation = phInsuredRelation;
	}

	public String getBeneficiaryIndi() {
		return beneficiaryIndi;
	}

	public void setBeneficiaryIndi(String beneficiaryIndi) {
		this.beneficiaryIndi = beneficiaryIndi;
	}

	public ReqAppPremiumDTO getPremium() {
		return premium;
	}

	public void setPremium(ReqAppPremiumDTO premium) {
		this.premium = premium;
	}

}
