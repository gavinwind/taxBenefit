package com.sinosoft.taxbenefit.api.dto.request.applicant;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;
/**
 * cuo
 * @author Administrator
 *
 */
public class ReqAppAcceptMore extends TaxBaseApiDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3013990348198291596L;
	// 保单
	private List<ReqAppPolicyDTO> policyList;
	// 个人保单
	private ReqAppSinglePolicyHolderDTO singlePolicyHolder;
	// 团单
	private ReqAppGroupPolicyHolderDTO groupPolicyHolder;
	// 被保人
	private List<ReqAppInsuredDTO> insuredList;

	public List<ReqAppPolicyDTO> getPolicyList() {
		return policyList;
	}

	public void setPolicyList(List<ReqAppPolicyDTO> policyList) {
		this.policyList = policyList;
	}

	public ReqAppSinglePolicyHolderDTO getSinglePolicyHolder() {
		return singlePolicyHolder;
	}

	public void setSinglePolicyHolder(
			ReqAppSinglePolicyHolderDTO singlePolicyHolder) {
		this.singlePolicyHolder = singlePolicyHolder;
	}

	public ReqAppGroupPolicyHolderDTO getGroupPolicyHolder() {
		return groupPolicyHolder;
	}

	public void setGroupPolicyHolder(
			ReqAppGroupPolicyHolderDTO groupPolicyHolder) {
		this.groupPolicyHolder = groupPolicyHolder;
	}

	public List<ReqAppInsuredDTO> getInsuredList() {
		return insuredList;
	}

	public void setInsuredList(List<ReqAppInsuredDTO> insuredList) {
		this.insuredList = insuredList;
	}


}
