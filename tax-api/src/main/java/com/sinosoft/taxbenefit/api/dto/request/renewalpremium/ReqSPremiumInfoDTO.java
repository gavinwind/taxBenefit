package com.sinosoft.taxbenefit.api.dto.request.renewalpremium;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * Title:核心-续期保费信息上传请求(单个)
 * @author yangdongkai@outlook.com
 * @date 2016年3月16日 下午6:37:29
 */
public class ReqSPremiumInfoDTO extends TaxBaseApiDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7131763536126951863L;
	//保费信息
	private List<ReqRenewalPremiumInfoDTO> premiumList;
	/**
	 * @return the premiumList
	 */
	public List<ReqRenewalPremiumInfoDTO> getPremiumList() {
		return premiumList;
	}
	/**
	 * @param premiumList the premiumList to set
	 */
	public void setPremiumList(List<ReqRenewalPremiumInfoDTO> premiumList) {
		this.premiumList = premiumList;
	}
}