package com.sinosoft.taxbenefit.api.dto.request.customer;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;
/**
 * 
 * title：被保人既往理赔额查询对象
 * @author zhangyu
 * @date 2016年2月29日 下午7:17:03
 */
public class ReqInsuredPayInfoDTO extends TaxBaseApiDTO{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8983456449372186561L;
	//业务号
	private String bizNo;
	//姓名
	private String name;
	//性别
	private String gender;
	//生日
	private String birthday;
	//证件类型
	private String certiType;
	//证件号码
	private String certiNo;
	//投保单号
	private String proposalNo;
	//保单投保来源
	private String policySource;

	public String getBizNo() {
		return bizNo;
	}
	public void setBizNo(String bizNo) {
		this.bizNo = bizNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getCertiType() {
		return certiType;
	}
	public void setCertiType(String certiType) {
		this.certiType = certiType;
	}
	public String getCertiNo() {
		return certiNo;
	}
	public void setCertiNo(String certiNo) {
		this.certiNo = certiNo;
	}
	public String getProposalNo() {
		return proposalNo;
	}
	public void setProposalNo(String proposalNo) {
		this.proposalNo = proposalNo;
	}
	public String getPolicySource() {
		return policySource;
	}
	public void setPolicySource(String policySource) {
		this.policySource = policySource;
	}
}