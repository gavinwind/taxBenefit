package com.sinosoft.taxbenefit.api.dto.request.customer;

import com.sinosoft.taxbenefit.api.dto.request.base.BaseRequestTax;
/**
 * 
 * title：被保人既往理赔额查询请求DTO(核心)
 * @author zhangyu
 * @date 2016年3月12日 下午6:48:56
 */
public class ReqInsuredPayCoreDTO extends BaseRequestTax{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7954040625108625926L;
	
	private ReqInsuredPayInfoDTO body;
	
	public void setBody(ReqInsuredPayInfoDTO body) {
		this.body = body;
	}
	@Override
	public Object getBody() {		
		return body;
	}
}