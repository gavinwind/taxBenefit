package com.sinosoft.taxbenefit.api.config;
/**
 * 任务表的处理结果类型
 * @author zhangke
 *
 */
public enum ENUM_DISPOSE_RESULT {
	SUCCESS("01", "成功"), FAIL("02", "失败");
	/** 枚举code */
	private String code;
	/** 枚举value或者code说明 */
	private String value;

	ENUM_DISPOSE_RESULT(String code, String value) {
		this.code = code;
		this.value = value;
	}

	/**
	 * 获得枚举code值
	 * 
	 * @Title: code
	 * @Description: TODO
	 * @return
	 */
	public String code() {
		return code;
	}

	/**
	 * 获得枚举value值
	 * 
	 * @Title: decription
	 * @Description: TODO
	 * @return
	 */
	public String description() {
		return value;
	}

	/**
	 * 根据key获得value
	 * 
	 * @param key
	 * @return
	 */
	public static String getEnumValueByKey(String key) {
		for (ENUM_DISPOSE_RESULT enumItem : ENUM_DISPOSE_RESULT
				.values()) {
			if (key.equals(enumItem.code())) {
				return enumItem.description();
			}
		}
		return "";
	}
}
