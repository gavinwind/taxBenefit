package com.sinosoft.taxbenefit.api.dto.common;

import com.sinosoft.taxbenefit.api.dto.request.base.TaxBaseApiDTO;

/**
 * 异常任务日志DTO
 * @author zhangke
 */
public class FaultTaskDTO extends TaxBaseApiDTO{

	private static final long serialVersionUID = 165543997932238186L;
	private String state;//状态
	private String message;//返回报文

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
