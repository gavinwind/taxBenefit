package com.sinosoft.taxbenefit.api.inter;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.securityguard.ReqKeepBodyDTO;

/**
 * 保全
 * 
 * @author zhanghao
 *
 */
public interface PreservationOfChangeApiService {

	public ThirdSendResponseDTO PreservationOfChange(ReqKeepBodyDTO body,ParamHeadDTO paramHeadDTO);
}
