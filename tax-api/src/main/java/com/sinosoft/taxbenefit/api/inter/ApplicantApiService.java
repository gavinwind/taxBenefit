package com.sinosoft.taxbenefit.api.inter;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.applicant.ReqAppPolicyDTO;
import com.sinosoft.taxbenefit.api.dto.request.applicant.ReqPolicyCancelDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;

/**
 * 承保接口服务
 * @author zhanghao@sinosoft.com.cn
 *
 */
public interface ApplicantApiService {
	
	/**
	 * 承保信息上传(支持多笔)
	 * @param body
	 * @return
	 */
	ThirdSendResponseDTO policyInforceService(List<ReqAppPolicyDTO>  body,ParamHeadDTO paramHeadDTO);

	/**
	 * 承保撤销上传
	 * @param reqPolicyCancelDTO
	 * @param paramHeadDTO
	 * @return
	 */
	ThirdSendResponseDTO policyReversal(ReqPolicyCancelDTO reqPolicyCancelDTO,ParamHeadDTO paramHeadDTO);
}
