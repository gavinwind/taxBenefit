package test.comsinosoft.taxbenefit.entry.customer;




import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.platform.base.TaxBaseTest;
import com.sinosoft.taxbenefit.api.dto.request.base.CoreRequestHead;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqCustomerCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqCustomerInfoDTO;
import com.sinosoft.taxbenefit.entry.customer.webservice.CustomerVerifyWebService;

public class TestCustomerVerify extends TaxBaseTest{
    String bodyJson;
         
    @Autowired
    CustomerVerifyWebService customerVerifyWebService;
	@Before
	public void prepareData(){
		//
		ReqCustomerCoreDTO reqCustomerCoreDTO = new ReqCustomerCoreDTO();
		// 报文头
		CoreRequestHead coreRequestHead = new CoreRequestHead();
		coreRequestHead.setAreaCode("shanghai");
		coreRequestHead.setTransType("PTY001");
		coreRequestHead.setRecordNum("1");
		coreRequestHead.setSerialNo("A11000009");
		coreRequestHead.setSignKey("14118469dd199ca6745efdf821f4cefb");
		coreRequestHead.setSourceCode("01");
		coreRequestHead.setTransDate("2015-02-16 10:20:25");
		// 报文体
		
		ReqCustomerInfoDTO reqCustomerInfoDTO = new ReqCustomerInfoDTO();
		reqCustomerInfoDTO.setBizNo("345");
		reqCustomerInfoDTO.setBirthday("2015-02-16");
		reqCustomerInfoDTO.setName("小明");
		reqCustomerInfoDTO.setCertiNo("23534342543654756");
		reqCustomerInfoDTO.setCertiType("學生證");
		reqCustomerInfoDTO.setGender("女");
		
		
		
		reqCustomerCoreDTO.setBody(reqCustomerInfoDTO);
		reqCustomerCoreDTO.setRequestHead(coreRequestHead);
		
		bodyJson = JSONObject.toJSONString(reqCustomerCoreDTO);
	}
	
	@Test
	public void tsetPolicyTrensfer(){
		
		System.out.println(bodyJson);
		
		customerVerifyWebService.dealMainBiz(bodyJson);
		
		
	}

}
