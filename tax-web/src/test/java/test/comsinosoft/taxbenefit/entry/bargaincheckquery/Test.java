package test.comsinosoft.taxbenefit.entry.bargaincheckquery;

import java.math.BigDecimal;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.taxbenefit.api.dto.request.bargaincheckquery.ReqBargainCheckQueryCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.bargaincheckquery.ReqBargainCheckQueryDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.CoreRequestHead;

public class Test {
	public static void main(String[] args) {
		String bodyJson;
		ReqBargainCheckQueryCoreDTO reqBargainCheckQueryCoreDTO = new ReqBargainCheckQueryCoreDTO();
		//报文头
		CoreRequestHead coreRequestHead = new CoreRequestHead();
		coreRequestHead.setAreaCode("shanghai");
		coreRequestHead.setTransType("CHK001");
		coreRequestHead.setRecordNum("1");
		coreRequestHead.setSerialNo("A111111");
		coreRequestHead.setSignKey("57347b88ea203a3be86f807f25e4118f");
		coreRequestHead.setSourceCode("01");
		coreRequestHead.setTransDate("2015-02-16 10:20:25");
		//报文体
		ReqBargainCheckQueryDTO reqBargainCheckQueryDTO = new ReqBargainCheckQueryDTO();
		reqBargainCheckQueryDTO.setCheckDate("2016-01-01");
		reqBargainCheckQueryDTO.setComFailNum(new BigDecimal("0.098"));
		reqBargainCheckQueryDTO.setComSuccessNum(new BigDecimal("0.098"));
		reqBargainCheckQueryDTO.setRequestTotalNum(new BigDecimal("0.098"));
		reqBargainCheckQueryCoreDTO.setBody(reqBargainCheckQueryDTO);
		reqBargainCheckQueryCoreDTO.setRequestHead(coreRequestHead);
		
		bodyJson = JSONObject.toJSONString(reqBargainCheckQueryCoreDTO);
		System.out.println(bodyJson);
	}
}
