package test.comsinosoft.taxbenefit.entry.customer;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.platform.base.TaxBaseTest;
import com.sinosoft.taxbenefit.api.dto.request.base.CoreRequestHead;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqInsuredPayCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqInsuredPayInfoDTO;
import com.sinosoft.taxbenefit.entry.customer.webservice.InsuredPayWebService;

public class TestInsuredPayQuery extends TaxBaseTest{
	String bodyJson;
	
	@Autowired
	InsuredPayWebService insuredPayWebService;
	@Before
	public void prepareData(){
		ReqInsuredPayCoreDTO reqInsuredPayCoreDTO=new ReqInsuredPayCoreDTO();
		ReqInsuredPayInfoDTO reqInsuredPayInfoDTO=new ReqInsuredPayInfoDTO();
	
		reqInsuredPayInfoDTO.setBizNo("453");
		reqInsuredPayInfoDTO.setBirthday("1992-02-21");
		reqInsuredPayInfoDTO.setName("周杰伦");
		reqInsuredPayInfoDTO.setGender("男");
		reqInsuredPayInfoDTO.setCertiNo("565ewtwyytr432654");
		reqInsuredPayInfoDTO.setCertiType("身份证");
		reqInsuredPayInfoDTO.setProposalNo("78979878");
		reqInsuredPayInfoDTO.setPolicySource("台湾");
		
		
		CoreRequestHead coreRequestHead = new CoreRequestHead();
		coreRequestHead.setAreaCode("shanghai");
		coreRequestHead.setRecordNum("1");
		coreRequestHead.setSerialNo("A11000342");
		coreRequestHead.setSignKey("65d1badff001d43da7f488254d8cb064");
		coreRequestHead.setSourceCode("01");
		coreRequestHead.setTransDate("2015-02-16 10:20:25");
		coreRequestHead.setTransType("PTY004");
		reqInsuredPayCoreDTO.setBody(reqInsuredPayInfoDTO);
		reqInsuredPayCoreDTO.setRequestHead(coreRequestHead);
		
		bodyJson=JSONObject.toJSONString(reqInsuredPayCoreDTO);
	}
	
	
	@Test
	public void InsuredPay(){
		
		System.out.println(bodyJson);
		insuredPayWebService.dealMainBiz(bodyJson);
	}

}
