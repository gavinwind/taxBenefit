package test.comsinosoft.taxbenefit.entry.renewalpremium;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.taxbenefit.api.dto.request.base.CoreRequestHead;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqRenewalPremiumCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqRenewalPremiumCoverageInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqRenewalPremiumInfoDTO;

public class Test {
	public static void main(String[] args) {
		String bodyJson;
		ReqRenewalPremiumCoreDTO reqRenewalPremiumCoreDTO = new ReqRenewalPremiumCoreDTO();
		//报文头
		CoreRequestHead coreRequestHead = new CoreRequestHead();
		coreRequestHead.setAreaCode("shanghai");
		coreRequestHead.setTransType("PRM001");
		coreRequestHead.setRecordNum("1");
		coreRequestHead.setSerialNo("A1100001");
		coreRequestHead.setSignKey("bac387816a2cec26b80b133c919f03f4");
		coreRequestHead.setSourceCode("01");
		coreRequestHead.setTransDate("2015-02-16 10:20:25");
		//报文体
		ReqRenewalPremiumInfoDTO reqRenewalPremiumInfoDTO = new ReqRenewalPremiumInfoDTO();
		
		ReqRenewalPremiumCoverageInfoDTO reqPremiumCoverageInfoDTO = new ReqRenewalPremiumCoverageInfoDTO();
		List<ReqRenewalPremiumCoverageInfoDTO> reqPremiumCoverageInfoList = new ArrayList<ReqRenewalPremiumCoverageInfoDTO>();
		
		reqPremiumCoverageInfoDTO.setComCoverageCode("A111");
		reqPremiumCoverageInfoDTO.setCoveragePackageCode("A111-1");
		reqPremiumCoverageInfoDTO.setCoveragePremium(new BigDecimal("0.098"));
		reqPremiumCoverageInfoDTO.setRiskPremium(new BigDecimal("0.098"));
		reqPremiumCoverageInfoList.add(reqPremiumCoverageInfoDTO);
		
		
		reqRenewalPremiumInfoDTO.setBizNo("111");
		reqRenewalPremiumInfoDTO.setPolicyNo("123");
		reqRenewalPremiumInfoDTO.setSequenceNo("123");
		reqRenewalPremiumInfoDTO.setConfirmDate("2016-01-01");
		reqRenewalPremiumInfoDTO.setFeeId("111");
		reqRenewalPremiumInfoDTO.setFeeStatus("222");
		reqRenewalPremiumInfoDTO.setPayDueDate("2016-01-01");
		reqRenewalPremiumInfoDTO.setPayFromDate("2016-01-01");
		reqRenewalPremiumInfoDTO.setPayToDate("2016-01-01");
		reqRenewalPremiumInfoDTO.setPaymentFrequency("123456");
		reqRenewalPremiumInfoDTO.setPremiumAmount(new BigDecimal("0.098"));
		reqRenewalPremiumInfoDTO.setFeeStatus("123456");
		reqRenewalPremiumInfoDTO.setCoverageList(reqPremiumCoverageInfoList);

		
//		reqRenewalPremiumCoreDTO.setBody(reqRenewalPremiumInfoDTO);
		reqRenewalPremiumCoreDTO.setRequestHead(coreRequestHead);
		
		bodyJson = JSONObject.toJSONString(reqRenewalPremiumCoreDTO);
		System.out.println(bodyJson);
	}
}
