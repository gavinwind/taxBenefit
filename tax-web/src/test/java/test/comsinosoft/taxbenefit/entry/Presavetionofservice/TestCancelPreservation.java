package test.comsinosoft.taxbenefit.entry.Presavetionofservice;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.platform.base.TaxBaseTest;
import com.sinosoft.taxbenefit.api.dto.request.base.CoreRequestHead;
import com.sinosoft.taxbenefit.api.dto.request.securityguard.ReqEndorsementCancelInfoDTO;
import com.sinosoft.taxbenefit.entry.securityguard.webservice.PreservationOfCancelWebService;

public class TestCancelPreservation  extends TaxBaseTest{
	
	static	String requestjson;
	@Autowired
	PreservationOfCancelWebService   pService;
	
	
	@Before
	public void DToPro() {

		CoreRequestHead requestHead=new CoreRequestHead();
		
		requestHead.setAreaCode("zhN");
		requestHead.setTransType("END003");
		requestHead.setRecordNum("123");
		requestHead.setSerialNo("BUS1110");
		requestHead.setSignKey("d47cf9a15baefb127db466990bf4287b");
		requestHead.setSourceCode("01"); //01-核心
		requestHead.setTransDate("2016-3-7");
		
		
		ReqEndorsementCancelInfoDTO a=new ReqEndorsementCancelInfoDTO();
		a.setBizNo("999999999999");
		a.setEndorsementNo("99999999999");
		a.setPolicyNo("8888888");
		Map<String,Object> map=new HashMap<String,Object>();
		
		map.put("requestHead", requestHead);
		map.put("endorsementCancel","a");
		requestjson=		JSONObject.toJSONString(map);
		System.out.println(requestjson);
	}
	@Test
	public void test(){
		pService.dealMainBiz(requestjson);
	}
	
	public static void main(String[] args) {
	CoreRequestHead requestHead=new CoreRequestHead();
		
		requestHead.setAreaCode("zhN");
		requestHead.setTransType("END003");
		requestHead.setRecordNum("123");
		requestHead.setSerialNo("BUS1110");
		requestHead.setSignKey("d47cf9a15baefb127db466990bf4287b");
		requestHead.setSourceCode("01"); //01-核心
		requestHead.setTransDate("2016-3-7");
		
		
		ReqEndorsementCancelInfoDTO a=new ReqEndorsementCancelInfoDTO();
		a.setBizNo("999999999999");
		a.setEndorsementNo("99999999999");
		a.setPolicyNo("8888888");
		Map<String,Object> map=new HashMap<String,Object>();
		
		map.put("requestHead", requestHead);
		map.put("endorsementCancel",a);
		requestjson=		JSONObject.toJSONString(map);
		System.out.println(requestjson);
	}
	
	
}
