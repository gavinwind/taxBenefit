package test.comsinosoft.taxbenefit.entry.policystate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.platform.base.TaxBaseTest;
import com.sinosoft.taxbenefit.api.dto.request.base.CoreRequestHead;
import com.sinosoft.taxbenefit.api.dto.request.policystate.PolciyStateCoverage;
import com.sinosoft.taxbenefit.api.dto.request.policystate.PolicyStateEndorsement;
import com.sinosoft.taxbenefit.api.dto.request.policystate.PolicyStateInsured;
import com.sinosoft.taxbenefit.api.dto.request.policystate.PolicyStateLiability;
import com.sinosoft.taxbenefit.entry.policystate.webservice.PolicyStateChangeMoreWebService;
import com.sinosoft.taxbenefit.entry.policystate.webservice.PolicyStateChangeOneWebService;

public class TestPolicyState extends TaxBaseTest{
	
	static String requestjson;
	
	@Autowired
	private PolicyStateChangeMoreWebService changpolicyState;
	
	@Autowired
	private PolicyStateChangeOneWebService changpolicyOneState;
	
	
	@Before
	public void jass() {
		
		CoreRequestHead requestHead=new CoreRequestHead();
		
		requestHead.setAreaCode("zhN");
		requestHead.setTransType("END003");
		requestHead.setRecordNum("123");
		requestHead.setSerialNo("BUS1110");
		requestHead.setSignKey("d47cf9a15baefb127db466990bf4287b");
//		单笔
//		requestHead.setSignKey("d8ba453fdcdc2cf083179596bf440b30");
		requestHead.setSourceCode("01"); //01-核心
		requestHead.setTransDate("2016-3-7");
		
		
		
		List<PolicyStateEndorsement> policylist=new ArrayList<PolicyStateEndorsement>();
		
		PolicyStateEndorsement policy=new PolicyStateEndorsement();
		
		PolicyStateInsured insured=new PolicyStateInsured();
		
		List<PolicyStateInsured> insuredList=new ArrayList<PolicyStateInsured>();
		List<PolciyStateCoverage> coverageList=new ArrayList<PolciyStateCoverage>();
		
		List<PolicyStateLiability> liabilityList =new ArrayList<PolicyStateLiability>();
		
		PolicyStateLiability liability=new PolicyStateLiability();
		
		policy.setBizNo("123");
		policy.setEndorsementNo("policy123123");
		policy.setFinishTime("2016-1-1");
		policy.setPolEndSeq("asdf");
		policy.setPolicyNo("policy123123");
		policy.setPolicyStatus("有效");
		policy.setPolicyStatusUpdateType("riskUpdate");
		policy.setSuspendDate("2015-1-1");
		policy.setTerminationDate("2016-1-2");
		policy.setTerminationReason("afdfasdf");
		
		insured.setSequenceNo("3453453");
		liability.setLiabilityCode("libaCode");
		liability.setLiabilityStatus("有效");
		liability.setLiabilityTermReason("afd");
		liabilityList.add(liability);
		PolciyStateCoverage coverage=new PolciyStateCoverage();
		
		coverage.setComCoverageCode("asdf");
		coverage.setComCoverageName("riskCode");
		coverage.setCoveragePackageCode("riskCode");
		coverage.setCoverageStatus("riskCode");
		coverage.setCoverageType("riskCode");
		coverage.setLiabilityList(liabilityList);
		coverage.setSuspendDate("2012-2-2");
		coverage.setTerminationDate("2012-2-2");
		coverage.setTerminationReason("asfasf");
		coverageList.add(coverage);
		insured.setCoverageList(coverageList);
		
		insuredList.add(insured);
		Map<String,Object> map=new HashMap<String,Object>();
		
		
	
		policylist.add(policy);
		policy.setInsuredList(insuredList);
		map.put("requestHead",requestHead );
//		map.put("body", policylist);
		
		map.put("body", policylist);
		
		
		
		requestjson=		JSONObject.toJSONString(map);
		System.out.println(requestjson);
	}
	@Test
	public void test(){
		changpolicyState.dealMainBiz(requestjson);
	}
//	@Test
//	public void testone(){
//		changpolicyOneState.dealMainBiz(requestjson);
//	}
	
	
	
	public static void main(String[] args) {

		CoreRequestHead requestHead=new CoreRequestHead();
		
		requestHead.setAreaCode("zhN");
		requestHead.setTransType("END003");
		requestHead.setRecordNum("123");
		requestHead.setSerialNo("BUS1110");
		requestHead.setSignKey("d47cf9a15baefb127db466990bf4287b");
//		单笔
		requestHead.setSignKey("d8ba453fdcdc2cf083179596bf440b30");
		requestHead.setSourceCode("01"); //01-核心
		requestHead.setTransDate("2016-3-7");
		
		
		
		List<PolicyStateEndorsement> policylist=new ArrayList<PolicyStateEndorsement>();
		
		PolicyStateEndorsement policy=new PolicyStateEndorsement();
		
		PolicyStateInsured insured=new PolicyStateInsured();
		
		List<PolicyStateInsured> insuredList=new ArrayList<PolicyStateInsured>();
		List<PolciyStateCoverage> coverageList=new ArrayList<PolciyStateCoverage>();
		
		List<PolicyStateLiability> liabilityList =new ArrayList<PolicyStateLiability>();
		
		PolicyStateLiability liability=new PolicyStateLiability();
		
		policy.setBizNo("123");
		policy.setEndorsementNo("policy123123");
		policy.setFinishTime("2016-1-1");
		policy.setPolEndSeq("asdf");
		policy.setPolicyNo("policy123123");
		policy.setPolicyStatus("有效");
		policy.setPolicyStatusUpdateType("riskUpdate");
		policy.setSuspendDate("2015-1-1");
		policy.setTerminationDate("2016-1-2");
		policy.setTerminationReason("afdfasdf");
		
		insured.setSequenceNo("3453453");
		liability.setLiabilityCode("libaCode");
		liability.setLiabilityStatus("有效");
		liability.setLiabilityTermReason("afd");
		liabilityList.add(liability);
		PolciyStateCoverage coverage=new PolciyStateCoverage();
		
		coverage.setComCoverageCode("asdf");
		coverage.setComCoverageName("riskCode");
		coverage.setCoveragePackageCode("riskCode");
		coverage.setCoverageStatus("riskCode");
		coverage.setCoverageType("riskCode");
		coverage.setLiabilityList(liabilityList);
		coverage.setSuspendDate("2012-2-2");
		coverage.setTerminationDate("2012-2-2");
		coverage.setTerminationReason("asfasf");
		coverageList.add(coverage);
		insured.setCoverageList(coverageList);
		
		insuredList.add(insured);
		Map<String,Object> map=new HashMap<String,Object>();
		
		
	
		policylist.add(policy);
		policy.setInsuredList(insuredList);
		map.put("requestHead",requestHead );
//		map.put("body", policylist);
		
		map.put("body", policy);
		
		
		
		requestjson=		JSONObject.toJSONString(map);
		System.out.println(requestjson);
	}
}
