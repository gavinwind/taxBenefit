package test.comsinosoft.taxbenefit.entry.policyTrans;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.taxbenefit.api.dto.request.base.CoreRequestHead;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqPolicyInTransferQueryCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqPolicyInTransferQueryDTO;
import com.sinosoft.taxbenefit.entry.policytransfer.webservice.PolicyTransOutQueryWebService;
import com.sinosoft.taxbenefit.entry.policytransfer.webservice.PolicyTransOutRegisterQueryWebService;

/**
 * 保单转出信息查询 -测试用例
 * @author SheChunMing
 */
public class TestPolicyTransOutQuery {
	
	String bodyJson;
	@Autowired
	PolicyTransOutQueryWebService policyTransOutQueryWebService;
	@Autowired
	PolicyTransOutRegisterQueryWebService policyTransOutRegisterQueryWebService;
	
	@Before
	public void prepareData(){
		ReqPolicyInTransferQueryCoreDTO reqPolicyInTransferQueryCore = new ReqPolicyInTransferQueryCoreDTO();
		// 报文头
		CoreRequestHead coreRequestHead = new CoreRequestHead();
		coreRequestHead.setAreaCode("shanghai");
		coreRequestHead.setTransType("END008");//END007
		coreRequestHead.setRecordNum("1");
		coreRequestHead.setSerialNo("END00001");
		coreRequestHead.setSignKey("0011667d6f5b7e1ec0c419ea7293e020");
		coreRequestHead.setSourceCode("01");
		coreRequestHead.setTransDate("2015-02-16 10:20:25");
		// 报文体
		ReqPolicyInTransferQueryDTO reqPolicyInTransferQuery = new ReqPolicyInTransferQueryDTO();
		reqPolicyInTransferQuery.setQueryStartDate("2015-02-15");
		reqPolicyInTransferQuery.setQueryEndDate("2015-05-15");
		
		reqPolicyInTransferQueryCore.setBody(reqPolicyInTransferQuery);
		reqPolicyInTransferQueryCore.setRequestHead(coreRequestHead);
		
		bodyJson = JSONObject.toJSONString(reqPolicyInTransferQueryCore);
	}
	/**
	 * 保单转出信息查询
	 */
	@Test
	public void tsetPolicyTrensOutQuery(){
		try {
			policyTransOutQueryWebService.dealMainBiz(bodyJson);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 保单转出登记信息查询
	 */
	@Test
	public void tsetPolicyTrensOutRegisterQuery(){
		try {
			policyTransOutRegisterQueryWebService.dealMainBiz(bodyJson);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
