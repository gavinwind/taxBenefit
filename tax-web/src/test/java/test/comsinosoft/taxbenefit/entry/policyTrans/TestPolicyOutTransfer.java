package test.comsinosoft.taxbenefit.entry.policyTrans;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.taxbenefit.api.dto.request.base.CoreRequestHead;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqPolicyTransClientDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqSOutPolicyTransCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqSOutPolicyTransferDTO;
import com.sinosoft.taxbenefit.entry.policytransfer.webservice.PolicySOutTransferWebService;

/**
 * 保单转出登记 -测试用例
 * @author SheChunMing
 */
public class TestPolicyOutTransfer {
	String bodyJson;
	@Autowired
	PolicySOutTransferWebService policySOutTransferWebService;
	
	@Before
	public void prepareData(){
		ReqSOutPolicyTransCoreDTO reqSOutPolicyTransCore = new ReqSOutPolicyTransCoreDTO();
		// 报文头
		CoreRequestHead coreRequestHead = new CoreRequestHead();
		coreRequestHead.setAreaCode("shanghai");
		coreRequestHead.setTransType("END007");//END007
		coreRequestHead.setRecordNum("1");
		coreRequestHead.setSerialNo("QQQ00001");
		coreRequestHead.setSignKey("0011667d6f5b7e1ec0c419ea7293e020");
		coreRequestHead.setSourceCode("01");
		coreRequestHead.setTransDate("2015-02-16 10:20:25");
		// 报文体
		List<ReqPolicyTransClientDTO> reqPolicyTransClientList = new ArrayList<ReqPolicyTransClientDTO>();
		ReqPolicyTransClientDTO reqPolicyTransClientDTO = new ReqPolicyTransClientDTO();
		reqPolicyTransClientDTO.setSequenceNo("A01");
		reqPolicyTransClientList.add(reqPolicyTransClientDTO);
		
		List<ReqSOutPolicyTransferDTO> reqPolicyTransferOutList = new ArrayList<ReqSOutPolicyTransferDTO>();
		ReqSOutPolicyTransferDTO reqSOutPolicyTransfer = new ReqSOutPolicyTransferDTO();
		reqSOutPolicyTransfer.setBizNo("A001");
		reqSOutPolicyTransfer.setPolicyNo("8888");
		reqSOutPolicyTransfer.setClientList(reqPolicyTransClientList);
		reqPolicyTransferOutList.add(reqSOutPolicyTransfer);
		
		reqSOutPolicyTransCore.setBody(reqSOutPolicyTransfer);
		reqSOutPolicyTransCore.setRequestHead(coreRequestHead);
		
		bodyJson = JSONObject.toJSONString(reqSOutPolicyTransCore);
	}
	
	//{"body":{"bizNo":"A001","clientList":[{"sequenceNo":"A01"}],"policyNo":"8888"},"requestHead":{"areaCode":"shanghai","recordNum":"1","serialNo":"QQQ00001","signKey":"0011667d6f5b7e1ec0c419ea7293e020","sourceCode":"01","transDate":"2015-02-16 10:20:25","transType":"END007"}}
	@Test
	public void tsetPolicyTrensfer(){
		try {
			policySOutTransferWebService.dealMainBiz(bodyJson);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
