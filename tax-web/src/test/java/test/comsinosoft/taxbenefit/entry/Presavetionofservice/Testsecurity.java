package test.comsinosoft.taxbenefit.entry.Presavetionofservice;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.platform.base.TaxBaseTest;
import com.sinosoft.taxbenefit.api.dto.request.base.CoreRequestHead;
import com.sinosoft.taxbenefit.api.dto.request.securityguard.ReqKeepBeneficiaryInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.securityguard.ReqKeepBodyDTO;
import com.sinosoft.taxbenefit.api.dto.request.securityguard.ReqKeepCoverageInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.securityguard.ReqKeepEndorsementInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.securityguard.ReqKeepInsuredInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.securityguard.ReqKeepLiabilityInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.securityguard.ReqKeepSinglePolicyHolderInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.securityguard.ReqkeepUnderwritingInfoDTO;
import com.sinosoft.taxbenefit.entry.securityguard.webservice.PreservationOfChangeWebService;


public class Testsecurity extends TaxBaseTest{
	
	@Autowired
	private PreservationOfChangeWebService preservation;
	
	static String	requestJson;
	
	@Before
	public void ssss(){
		System.out.println(909090);
		
	ReqKeepBodyDTO body=new ReqKeepBodyDTO();
	
	CoreRequestHead requestHead=new CoreRequestHead();
	
	requestHead.setAreaCode("zhN");
	requestHead.setTransType("END001");
	requestHead.setRecordNum("123");
	requestHead.setSerialNo("BUS1110");
	requestHead.setSignKey("6505dffe9bdc089f9683d215f6eb6b5a");
	requestHead.setSourceCode("01"); //01-核心
	requestHead.setTransDate("2016-3-7");
	
/////////////////
	ReqKeepEndorsementInfoDTO endorsement=new ReqKeepEndorsementInfoDTO(); 
	endorsement.setPolicyNo("43434");
	endorsement.setBizNo("00001");
	endorsement.setPolicyType("01");//01-个单
	endorsement.setPolicyOrg("二级机构名称");
	endorsement.setPolicyArea("保单所属区域--地区代码");
	endorsement.setEndorsementType("保全类型-保全类型代码");
	endorsement.setEndorsementNo("保全批单号");
	endorsement.setPolEndSeq("保单批单序号");
	endorsement.setFinishTime("2016-2-6");
	endorsement.setEndorsementStatus("有效-代码");
	endorsement.setEndorsementApplicationDate("2016-2-3");
	endorsement.setEndorsementEffectiveDate("2016-2-5");
	endorsement.setTerminationDate("2020-1-1");
	endorsement.setSuspendDate("2015-1-2");
	endorsement.setRecoverDate("2015-2-1");
	endorsement.setPolicySource("保单来源");
	endorsement.setPolicyStatus("有效-代码");
	endorsement.setTerminationReason("");
	endorsement.setEffectiveDate("2016-3-6");
	endorsement.setPaymentFrequency("代码");
	endorsement.setSurrenderAmount(new BigDecimal("99"));
	endorsement.setTaxDeduct(new BigDecimal("99"));
	endorsement.setTopUpAmount(new BigDecimal("99"));
	
	ReqKeepSinglePolicyHolderInfoDTO singlePolicyHolder=new ReqKeepSinglePolicyHolderInfoDTO();
	
	
	singlePolicyHolder.setCustomerNo("a");
	singlePolicyHolder.setName("zhangsan");
	singlePolicyHolder.setGender("男-代码");
	singlePolicyHolder.setBirthday("2015-5-6");
	singlePolicyHolder.setCertiNo("423117199012041324");
	singlePolicyHolder.setCertiType("身份证-代码");
	singlePolicyHolder.setNationality("ChZ");
	singlePolicyHolder.setTaxIndi("Y");
	singlePolicyHolder.setTaxPayerType("S");
	singlePolicyHolder.setWorkUnit("gyas");
	singlePolicyHolder.setOrganCode1("税务登记号");
	singlePolicyHolder.setOrganCode2("组织机构代码");
	singlePolicyHolder.setOrganCode3("营业证号");
	singlePolicyHolder.setOrganCode4("社会信用代码");
	singlePolicyHolder.setResidencePlace("联系地址");
	singlePolicyHolder.setMobileNo(new BigDecimal("13103781223"));

	List<ReqKeepInsuredInfoDTO>  insuredList=new ArrayList<ReqKeepInsuredInfoDTO>();
	
	ReqKeepInsuredInfoDTO insured=new ReqKeepInsuredInfoDTO();
	insured.setSequenceNo("123123");
	insured.setCustomerNo("909");
	insured.setName("zhangsan");
	insured.setGender("性别代码");
	insured.setBirthday("1992-1-2");
	insured.setCertiCode("423117199012041324");
	insured.setCertiType("身份证-代码");
	insured.setNationality("ChZ");
	insured.setMobileNo(new BigDecimal("13103781223"));
	insured.setResidencePlace("地区代码");
	insured.setHealthFlag("Y");
	insured.setSocialcareNo("社保卡号 ");
	insured.setJobCode("职业代码");
	insured.setInsuredType("被保险人类型 ");
	insured.setMainInsuredNo("1");
	insured.setPhInsuredRelation("9");
			
	List<ReqKeepBeneficiaryInfoDTO> beneficiaryList=new ArrayList<ReqKeepBeneficiaryInfoDTO>();
	ReqKeepBeneficiaryInfoDTO beneficiary=new ReqKeepBeneficiaryInfoDTO();
	beneficiary.setBeneficiaryType("1");
	beneficiary.setBenInsuredRelation("1");
	beneficiary.setBirthday("2012-1-2");
	beneficiary.setCertiNo("423117199012041324");
	beneficiary.setCertiType("1");
	beneficiary.setGender("性别代码");
	beneficiary.setName("lisi");
	beneficiary.setNationality("CHZ");
	beneficiary.setPriority(1);
	beneficiary.setProportion(new BigDecimal("99"));
//	受益人list
	beneficiaryList.add(beneficiary);
	
	List<ReqKeepCoverageInfoDTO> coverageList=new ArrayList<ReqKeepCoverageInfoDTO>();
		
	ReqKeepCoverageInfoDTO coverage=new ReqKeepCoverageInfoDTO();
	coverage.setComCoverageCode("险种代码");
	coverage.setComCoverageName("产品名字");
	coverage.setCoveragePackageCode("产品组code");
	coverage.setCoveragePremium("12");
	coverage.setCoverageEffectiveDate("2010-1-1");
	coverage.setAgeOfInception(1);
	coverage.setRiskPremium(new BigDecimal("111"));
	
	coverage.setCoverageExpirationDate("2010-1-1");
	coverage.setTerminationDate("2010-1-1");
	coverage.setSuspendDate("2010-1-1");
	coverage.setRecoverDate("2010-1-1");
	coverage.setCoverageType("Y");
	coverage.setSa(new BigDecimal("111"));
	coverage.setPayDueDay("2010-1-1");
	coverage.setRenewIndi("Y");
	coverage.setCoverageStatus("有效代码");
	coverage.setTerminationReason("终止原因");
	coverage.setSurrenderAmount(new BigDecimal("111"));
	
	
	
	
	ReqkeepUnderwritingInfoDTO underwriting=new  ReqkeepUnderwritingInfoDTO();
	underwriting.setUnderwritingDate("2014-2-4");
	underwriting.setUnderwritingDecision("核保决定");
	underwriting.setUnderwritingDes("核保描述");
	
	List<ReqKeepLiabilityInfoDTO> liabilityList =new ArrayList<ReqKeepLiabilityInfoDTO>();
	ReqKeepLiabilityInfoDTO liability=new ReqKeepLiabilityInfoDTO();
	liability.setLiabilityCode("ZR123");
	liability.setLiabilitySa(new BigDecimal("99"));
	liability.setLiabilityStatus("有效");
//	责任list
	liabilityList.add(liability);
	coverage.setLiabilityList(liabilityList);
//	核保
	coverage.setUnderwriting(underwriting);
//	险种list
	coverageList.add(coverage);
//
	insured.setCoverageList(coverageList);
	
	
	insured.setCoverageList(coverageList);
	insured.setBeneficiaryList(beneficiaryList);
	insuredList.add(insured);
	body.setEndorsement(endorsement);
	
	endorsement.setSinglePolicyHolder(singlePolicyHolder);
	endorsement.setInsuredList(insuredList);
	
	
//	TEst jjtest=new TEst();
//	jjtest.setHead(head);
//	jjtest.setBody(body);
	
	Map<String,Object> map=new HashMap<String,Object>();
	map.put("requestHead", requestHead);
	map.put("body",body);
	requestJson=JSONObject.toJSONString(map);
	System.out.println(requestJson);

	}
	
	
	@Test
	public void dealMaTest(){
	System.out.println(requestJson);
	preservation.dealMainBiz(requestJson);
	}
	
//	
	public static void main(String[] args) {
		
		ReqKeepBodyDTO body=new ReqKeepBodyDTO();
		
		CoreRequestHead requestHead=new CoreRequestHead();
		
		requestHead.setAreaCode("zhN");
		requestHead.setTransType("END001");
		requestHead.setRecordNum("123");
		requestHead.setSerialNo("BUS1110");
		requestHead.setSignKey("6505dffe9bdc089f9683d215f6eb6b5a");
		requestHead.setSourceCode("01"); //01-核心
		requestHead.setTransDate("2016-3-7");
		
	/////////////////
		ReqKeepEndorsementInfoDTO endorsement=new ReqKeepEndorsementInfoDTO(); 
		endorsement.setPolicyNo("43434");
//		endorsement.setBizNo("00001");
		endorsement.setPolicyType("01");//01-个单
		endorsement.setPolicyOrg("二级机构名称");
		endorsement.setPolicyArea("保单所属区域--地区代码");
		endorsement.setEndorsementType("保全类型-保全类型代码");
		endorsement.setEndorsementNo("保全批单号");
		endorsement.setPolEndSeq("保单批单序号");
		endorsement.setFinishTime("2016-2-6");
		endorsement.setEndorsementStatus("有效-代码");
		endorsement.setEndorsementApplicationDate("2016-2-3");
		endorsement.setEndorsementEffectiveDate("2016-2-5");
		endorsement.setTerminationDate("2020-1-1");
		endorsement.setSuspendDate("2015-1-2");
		endorsement.setRecoverDate("2015-2-1");
		endorsement.setPolicySource("保单来源");
		endorsement.setPolicyStatus("有效-代码");
		endorsement.setTerminationReason("");
		endorsement.setEffectiveDate("2016-3-6");
		endorsement.setPaymentFrequency("代码");
		endorsement.setSurrenderAmount(new BigDecimal("99"));
		endorsement.setTaxDeduct(new BigDecimal("99"));
		endorsement.setTopUpAmount(new BigDecimal("99"));
		
		ReqKeepSinglePolicyHolderInfoDTO singlePolicyHolder=new ReqKeepSinglePolicyHolderInfoDTO();
		
		
		singlePolicyHolder.setCustomerNo("a");
		singlePolicyHolder.setName("zhangsan");
		singlePolicyHolder.setGender("男-代码");
		singlePolicyHolder.setBirthday("2015-5-6");
		singlePolicyHolder.setCertiNo("423117199012041324");
		singlePolicyHolder.setCertiType("身份证-代码");
		singlePolicyHolder.setNationality("ChZ");
		singlePolicyHolder.setTaxIndi("Y");
		singlePolicyHolder.setTaxPayerType("S");
		singlePolicyHolder.setWorkUnit("gyas");
		singlePolicyHolder.setOrganCode1("税务登记号");
		singlePolicyHolder.setOrganCode2("组织机构代码");
		singlePolicyHolder.setOrganCode3("营业证号");
		singlePolicyHolder.setOrganCode4("社会信用代码");
		singlePolicyHolder.setResidencePlace("联系地址");
		singlePolicyHolder.setMobileNo(new BigDecimal("13103781223"));

		List<ReqKeepInsuredInfoDTO>  insuredList=new ArrayList<ReqKeepInsuredInfoDTO>();
		
		ReqKeepInsuredInfoDTO insured=new ReqKeepInsuredInfoDTO();
		insured.setSequenceNo("123123");
		insured.setCustomerNo("909");
		insured.setName("zhangsan");
		insured.setGender("性别代码");
		insured.setBirthday("1992-1-2");
		insured.setCertiCode("423117199012041324");
		insured.setCertiType("身份证-代码");
		insured.setNationality("ChZ");
		insured.setMobileNo(new BigDecimal("13103781223"));
		insured.setResidencePlace("地区代码");
		insured.setHealthFlag("Y");
		insured.setSocialcareNo("社保卡号 ");
		insured.setJobCode("职业代码");
		insured.setInsuredType("被保险人类型 ");
		insured.setMainInsuredNo("1");
		insured.setPhInsuredRelation("9");
				
		List<ReqKeepBeneficiaryInfoDTO> beneficiaryList=new ArrayList<ReqKeepBeneficiaryInfoDTO>();
		ReqKeepBeneficiaryInfoDTO beneficiary=new ReqKeepBeneficiaryInfoDTO();
		beneficiary.setBeneficiaryType("1");
		beneficiary.setBenInsuredRelation("1");
		beneficiary.setBirthday("2012-1-2");
		beneficiary.setCertiNo("423117199012041324");
		beneficiary.setCertiType("1");
		beneficiary.setGender("性别代码");
		beneficiary.setName("lisi");
		beneficiary.setNationality("CHZ");
		beneficiary.setPriority(1);
		beneficiary.setProportion(new BigDecimal("99"));
//		受益人list
		beneficiaryList.add(beneficiary);
		
		List<ReqKeepCoverageInfoDTO> coverageList=new ArrayList<ReqKeepCoverageInfoDTO>();
			
		ReqKeepCoverageInfoDTO coverage=new ReqKeepCoverageInfoDTO();
		coverage.setComCoverageCode("险种代码");
		coverage.setComCoverageName("产品名字");
		coverage.setCoveragePackageCode("产品组code");
		coverage.setCoveragePremium("12");
		coverage.setCoverageEffectiveDate("2010-1-1");
		coverage.setAgeOfInception(1);
		coverage.setRiskPremium(new BigDecimal("111"));
		
		coverage.setCoverageExpirationDate("2010-1-1");
		coverage.setTerminationDate("2010-1-1");
		coverage.setSuspendDate("2010-1-1");
		coverage.setRecoverDate("2010-1-1");
		coverage.setCoverageType("Y");
		coverage.setSa(new BigDecimal("111"));
		coverage.setPayDueDay("2010-1-1");
		coverage.setRenewIndi("Y");
		coverage.setCoverageStatus("有效代码");
		coverage.setTerminationReason("终止原因");
		coverage.setSurrenderAmount(new BigDecimal("111"));
		
		
		
		
		ReqkeepUnderwritingInfoDTO underwriting=new  ReqkeepUnderwritingInfoDTO();
		underwriting.setUnderwritingDate("2014-2-4");
		underwriting.setUnderwritingDecision("核保决定");
		underwriting.setUnderwritingDes("核保描述");
		
		List<ReqKeepLiabilityInfoDTO> liabilityList =new ArrayList<ReqKeepLiabilityInfoDTO>();
		ReqKeepLiabilityInfoDTO liability=new ReqKeepLiabilityInfoDTO();
		liability.setLiabilityCode("ZR123");
		liability.setLiabilitySa(new BigDecimal("99"));
		liability.setLiabilityStatus("有效");
//		责任list
		liabilityList.add(liability);
		coverage.setLiabilityList(liabilityList);
//		核保
		coverage.setUnderwriting(underwriting);
//		险种list
		coverageList.add(coverage);
	//
		insured.setCoverageList(coverageList);
		
		
		insured.setCoverageList(coverageList);
		insured.setBeneficiaryList(beneficiaryList);
		insuredList.add(insured);
	
		endorsement.setSinglePolicyHolder(singlePolicyHolder);
		endorsement.setInsuredList(insuredList);
		body.setEndorsement(endorsement);
		
		
		
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("requestHead", requestHead);
		map.put("body",body);
		requestJson=JSONObject.toJSONString(map);
		System.out.println(requestJson);

	}
}
