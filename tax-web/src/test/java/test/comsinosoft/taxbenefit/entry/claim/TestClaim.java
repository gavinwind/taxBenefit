package test.comsinosoft.taxbenefit.entry.claim;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.platform.base.TaxBaseTest;
import com.sinosoft.taxbenefit.api.dto.request.base.CoreRequestHead;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimCoveragePaymentInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimDetailInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimEventInfoLDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimItemInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimLiabilityPaymentInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimOperationInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimPolicyInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimReceiptFeeInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimReceiptInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimWesternMedInfoDTO;
import com.sinosoft.taxbenefit.entry.claims.webservice.ClaimWebService;
import com.sinosoft.taxbenefit.entry.generated.mapper.TaxRequestReceiveLogMapper;
import com.sinosoft.taxbenefit.entry.generated.model.TaxRequestReceiveLog;

public class TestClaim extends TaxBaseTest{
	String	requestJson;
	@Autowired
	ClaimWebService claimWebService;
	@Autowired
	TaxRequestReceiveLogMapper taxRequestReceiveLogMapper;
	@Before
	public void prepareData(){
		ReqClaimCoreDTO reqClaimCoreDTO=new ReqClaimCoreDTO();
		// 报文头
		CoreRequestHead requestHead=new CoreRequestHead();
		requestHead.setAreaCode("01");
		requestHead.setTransType("CLM001");
		requestHead.setRecordNum("1");
		requestHead.setSerialNo("0001");
		requestHead.setSignKey("bbc6b525b305d3883abd13c7fba6a4d4");
		requestHead.setSourceCode("01");
		requestHead.setTransDate("2000-01-01");
		ReqClaimInfoDTO claimInfoDTO = new ReqClaimInfoDTO();
		claimInfoDTO.setBizNo("0001");
		claimInfoDTO.setClaimNo("0008843784738025");
		claimInfoDTO.setReportDate("2000-01-01");
		claimInfoDTO.setApplyDate("2000-01-01");
		claimInfoDTO.setRegistrationDate("2000-01-01");
		claimInfoDTO.setAccidentReason("ACC");
		claimInfoDTO.setAccidentDate("2000-01-01");
		claimInfoDTO.setAccidentPlace("出险地点");
		claimInfoDTO.setClaimInvestigation("1");
		claimInfoDTO.setPendingIndi("1");
		claimInfoDTO.setPendingFinishDate("2000-01-01");
		claimInfoDTO.setClaimConclusionCode("01");
		claimInfoDTO.setConclusionReason("2 ");
		claimInfoDTO.setApplyAmount("5000");
		claimInfoDTO.setClaimAmount("4000");
		claimInfoDTO.setSocialInsuPayment("1");
		claimInfoDTO.setEndCaseDate("2000-01-01");
		claimInfoDTO.setStatus("01");
		claimInfoDTO.setClaimantName("张三");
		claimInfoDTO.setClaimantGender("M");
		claimInfoDTO.setClaimantBirthday("1990-01-01");
		claimInfoDTO.setClaimantCertiType("00");
		claimInfoDTO.setClaimantCertiCode("421002199004100518");
		claimInfoDTO.setDeathIndi("1");
		claimInfoDTO.setDeathDate("2000-01-01");
		claimInfoDTO.setWarningIndi("1");
		claimInfoDTO.setWarningDesc("2");
		/** 理赔保单信息 **/
		List<ReqClaimPolicyInfoDTO> policyList = new ArrayList<ReqClaimPolicyInfoDTO>();
		ReqClaimPolicyInfoDTO reqClaimPolicyInfoDTO = new ReqClaimPolicyInfoDTO();
		reqClaimPolicyInfoDTO.setPolicyNo("1");
		reqClaimPolicyInfoDTO.setSequenceNo("1");
		reqClaimPolicyInfoDTO.setCustomerNo("1");
		reqClaimPolicyInfoDTO.setPolicyStatus("1");
		reqClaimPolicyInfoDTO.setClaimAmount("5000");
		/** 险种赔付信息 */
		List<ReqClaimCoveragePaymentInfoDTO> coveragePaymentList = new ArrayList<ReqClaimCoveragePaymentInfoDTO>();
		ReqClaimCoveragePaymentInfoDTO reqClaimCoveragePaymentInfoDTO = new ReqClaimCoveragePaymentInfoDTO();
		reqClaimCoveragePaymentInfoDTO.setCoveragePackageCode("rr");
		reqClaimCoveragePaymentInfoDTO.setCoverageType("1-1");
		reqClaimCoveragePaymentInfoDTO.setComCoverageCode("rrr");
		reqClaimCoveragePaymentInfoDTO.setComCoverageName("险种名称2");
		reqClaimCoveragePaymentInfoDTO.setClaimOpinion("1");
		reqClaimCoveragePaymentInfoDTO.setClaimConclusionCode("1");
		reqClaimCoveragePaymentInfoDTO.setConclusionReason("1 ");
		reqClaimCoveragePaymentInfoDTO.setClaimAmount("2000");
		/** 责任赔付信息 **/
		List<ReqClaimLiabilityPaymentInfoDTO> liabilityPaymentList = new ArrayList<ReqClaimLiabilityPaymentInfoDTO>();
		ReqClaimLiabilityPaymentInfoDTO reqClaimLiabilityPaymentInfoDTO = new ReqClaimLiabilityPaymentInfoDTO();
		reqClaimLiabilityPaymentInfoDTO.setLiabilityType("1");
		reqClaimLiabilityPaymentInfoDTO.setLiabilityName("公司责任名称B");
		reqClaimLiabilityPaymentInfoDTO.setLiabilityCode("2");
		reqClaimLiabilityPaymentInfoDTO.setClaimOpinion("1");
		reqClaimLiabilityPaymentInfoDTO.setClaimConclusionCode("1");
		reqClaimLiabilityPaymentInfoDTO.setConclusionReason("1");
		reqClaimLiabilityPaymentInfoDTO.setPaymentAmount("5000");
		reqClaimLiabilityPaymentInfoDTO.setOperation("1");
		List<ReqClaimWesternMedInfoDTO> westernMediList = new ArrayList<ReqClaimWesternMedInfoDTO>();
		/** 西医疾病 **/
		ReqClaimWesternMedInfoDTO reqClaimWesternMedInfoDTO = new ReqClaimWesternMedInfoDTO();
		reqClaimWesternMedInfoDTO.setHospitalCode("1");
		reqClaimWesternMedInfoDTO.setHospitalName("医疗机构名称");
		reqClaimWesternMedInfoDTO.setWesternMediCode("1");
		westernMediList.add(reqClaimWesternMedInfoDTO);
		reqClaimLiabilityPaymentInfoDTO.setWesternMediList(westernMediList);
		/** 手术 **/
		List<ReqClaimOperationInfoDTO> operationList = new ArrayList<ReqClaimOperationInfoDTO>();
		ReqClaimOperationInfoDTO reqClaimOperationInfoDTO = new ReqClaimOperationInfoDTO();
		reqClaimOperationInfoDTO.setOperationCode("43");
		operationList.add(reqClaimOperationInfoDTO);
		reqClaimLiabilityPaymentInfoDTO.setOperationList(operationList);
		liabilityPaymentList.add(reqClaimLiabilityPaymentInfoDTO);
		reqClaimCoveragePaymentInfoDTO.setLiabilityPaymentList(liabilityPaymentList);
		coveragePaymentList.add(reqClaimCoveragePaymentInfoDTO);
		reqClaimPolicyInfoDTO.setCoveragePaymentList(coveragePaymentList);
		policyList.add(reqClaimPolicyInfoDTO);
		claimInfoDTO.setPolicyList(policyList);
		/** 医疗事件信息 **/
		List<ReqClaimEventInfoLDTO> eventList = new ArrayList<ReqClaimEventInfoLDTO>();
		ReqClaimEventInfoLDTO reqClaimEventInfoLDTO = new ReqClaimEventInfoLDTO();
		reqClaimEventInfoLDTO.setEventDate("2001-01-01");
		reqClaimEventInfoLDTO.setEventType("1");
		reqClaimEventInfoLDTO.setHospitalCode("0000039");
		reqClaimEventInfoLDTO.setHospitalName("123");
		reqClaimEventInfoLDTO.setOperation("1");
		reqClaimEventInfoLDTO.setOperationList(operationList);
		
		/** 收据 **/
		List<ReqClaimReceiptInfoDTO> receiptList = new ArrayList<ReqClaimReceiptInfoDTO>();
		ReqClaimReceiptInfoDTO reqClaimReceiptInfoDTO = new ReqClaimReceiptInfoDTO();
		reqClaimReceiptInfoDTO.setReceiptNo("1");
		reqClaimReceiptInfoDTO.setReceiptType("2");
		reqClaimReceiptInfoDTO.setHospitalCode("0000039");
		reqClaimReceiptInfoDTO.setHospitalName("机构名称");
		reqClaimReceiptInfoDTO.setOccurDate("2001-01-01");
		reqClaimReceiptInfoDTO.setAmount("5000");
		reqClaimReceiptInfoDTO.setHospitalDate("2001-01-01");
		reqClaimReceiptInfoDTO.setDischargeDate("2001-01-01");
		reqClaimReceiptInfoDTO.setHospitalStay("1");
		reqClaimReceiptInfoDTO.setHospitalDept("3");
		reqClaimReceiptInfoDTO.setIssueUnit("2");
		reqClaimReceiptInfoDTO.setRelatedClaimNo("3");
		/** 收据分项费用 **/
		List<ReqClaimReceiptFeeInfoDTO> receiptFeeList = new ArrayList<ReqClaimReceiptFeeInfoDTO>();
		ReqClaimReceiptFeeInfoDTO reqClaimReceiptFeeInfoDTO = new ReqClaimReceiptFeeInfoDTO();
		reqClaimReceiptFeeInfoDTO.setReceiptFeeType("3");
		reqClaimReceiptFeeInfoDTO.setAmount("3000");
		receiptFeeList.add(reqClaimReceiptFeeInfoDTO);
		reqClaimReceiptInfoDTO.setReceiptFeeList(receiptFeeList);
		/** 医疗费用 **/
		List<ReqClaimItemInfoDTO> itemList = new ArrayList<ReqClaimItemInfoDTO>();
		ReqClaimItemInfoDTO reqClaimItemInfoDTO = new ReqClaimItemInfoDTO();
		reqClaimItemInfoDTO.setItemCategory("3");
		reqClaimItemInfoDTO.setAmount("4000");
		reqClaimItemInfoDTO.setDeductibleAmount("1500");
		/** 医疗费用明细 **/
		List<ReqClaimDetailInfoDTO> detailList = new ArrayList<ReqClaimDetailInfoDTO>();
		ReqClaimDetailInfoDTO reqClaimDetailInfoDTO = new ReqClaimDetailInfoDTO();
		reqClaimDetailInfoDTO.setName("2");
		reqClaimDetailInfoDTO.setAmount("2000");
		reqClaimDetailInfoDTO.setDeductibleAmount("1000");
		reqClaimDetailInfoDTO.setQuantity("5");
		reqClaimDetailInfoDTO.setUnitPrice("200");
		reqClaimDetailInfoDTO.setUnit("2 ");	
		detailList.add(reqClaimDetailInfoDTO);
		reqClaimItemInfoDTO.setDetailList(detailList);
		itemList.add(reqClaimItemInfoDTO);
		reqClaimReceiptInfoDTO.setItemList(itemList);
		receiptList.add(reqClaimReceiptInfoDTO);
		reqClaimEventInfoLDTO.setReceiptList(receiptList);
		eventList.add(reqClaimEventInfoLDTO);
		List<ReqClaimWesternMedInfoDTO> westernMedInfoDTOs=new ArrayList<ReqClaimWesternMedInfoDTO>();
		ReqClaimWesternMedInfoDTO reqwes=new ReqClaimWesternMedInfoDTO();
		reqwes.setWesternMediCode("12");
		westernMedInfoDTOs.add(reqwes);
		/** 西医疾病 **/
		reqClaimEventInfoLDTO.setWesternMediList(westernMedInfoDTOs);
		/** 手术 **/
		List<ReqClaimOperationInfoDTO> oerList=new ArrayList<ReqClaimOperationInfoDTO>();
		ReqClaimOperationInfoDTO oer=new ReqClaimOperationInfoDTO();
		oer.setOperationCode("2");
		oerList.add(oer);
		reqClaimEventInfoLDTO.setOperationList(oerList);
		claimInfoDTO.setEventList(eventList);
		reqClaimCoreDTO.setBody(claimInfoDTO);
		reqClaimCoreDTO.setRequestHead(requestHead);
		reqClaimCoreDTO.setBody(claimInfoDTO);
		requestJson = JSONObject.toJSONString(reqClaimCoreDTO);
		System.out.println(requestJson);
	}
	@Test
	public void testClaim(){
		claimWebService.dealMainBiz(requestJson);
	}
}
