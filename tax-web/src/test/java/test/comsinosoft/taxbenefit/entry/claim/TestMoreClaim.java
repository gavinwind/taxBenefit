package test.comsinosoft.taxbenefit.entry.claim;

import java.util.ArrayList;
import java.util.List;

import org.bouncycastle.ocsp.Req;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.platform.base.TaxBaseTest;
import com.sinosoft.taxbenefit.api.dto.request.base.CoreRequestHead;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimCoreMDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimCoveragePaymentInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimDetailInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimEventInfoLDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimItemInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimLiabilityPaymentInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimOperationInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimPolicyInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimReceiptFeeInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimReceiptInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimWesternMedInfoDTO;
import com.sinosoft.taxbenefit.entry.claims.webservice.ClaimMWebService;
import com.sinosoft.taxbenefit.entry.claims.webservice.ClaimWebService;

public class TestMoreClaim extends TaxBaseTest{
	String	requestJson;
	@Autowired
	ClaimWebService claimWebService;//单个上传
	
	@Autowired
	ClaimMWebService claimMWebService;//多个上传
	@Before
	public void prepareData(){
	//	ReqClaimCoreDTO reqClaimCoreDTO=new ReqClaimCoreDTO();
		// 报文头
		CoreRequestHead requestHead=new CoreRequestHead();
		requestHead.setAreaCode("01");
		requestHead.setTransType("CLM002");
		requestHead.setRecordNum("1");
		requestHead.setSerialNo("0001");
		requestHead.setSignKey("bbc6b525b305d3883abd13c7fba6a4d4");
		requestHead.setSourceCode("01");
		requestHead.setTransDate("2000-01-01");
		ReqClaimInfoDTO claimInfoDTO = new ReqClaimInfoDTO();
		claimInfoDTO.setBizNo("0001");
		claimInfoDTO.setClaimNo("0008843784738025");
		claimInfoDTO.setReportDate("2000-01-01");
		claimInfoDTO.setApplyDate("2000-01-01");
		claimInfoDTO.setRegistrationDate("2000-01-01");
		claimInfoDTO.setAccidentReason("ACC");
		claimInfoDTO.setAccidentDate("2000-01-01");
		claimInfoDTO.setAccidentPlace("出险地点");
		claimInfoDTO.setClaimInvestigation("1");
		claimInfoDTO.setPendingIndi("1");
		claimInfoDTO.setPendingFinishDate("2000-01-01");
		claimInfoDTO.setClaimConclusionCode("01");
		claimInfoDTO.setConclusionReason("2 ");
		claimInfoDTO.setApplyAmount("5000");
		claimInfoDTO.setClaimAmount("4000");
		claimInfoDTO.setSocialInsuPayment("1");
		claimInfoDTO.setEndCaseDate("2000-01-01");
		claimInfoDTO.setStatus("01");
		claimInfoDTO.setClaimantName("张三");
		claimInfoDTO.setClaimantGender("M");
		claimInfoDTO.setClaimantBirthday("1990-01-01");
		claimInfoDTO.setClaimantCertiType("00");
		claimInfoDTO.setClaimantCertiCode("421002199004100518");
		claimInfoDTO.setDeathIndi("1");
		claimInfoDTO.setDeathDate("2000-01-01");
		claimInfoDTO.setWarningIndi("1");
		claimInfoDTO.setWarningDesc("2");
		/** 理赔保单信息 **/
		List<ReqClaimPolicyInfoDTO> policyList = new ArrayList<ReqClaimPolicyInfoDTO>();
		ReqClaimPolicyInfoDTO reqClaimPolicyInfoDTO = new ReqClaimPolicyInfoDTO();
		reqClaimPolicyInfoDTO.setPolicyNo("1");
		reqClaimPolicyInfoDTO.setSequenceNo("1");
		reqClaimPolicyInfoDTO.setCustomerNo("1");
		reqClaimPolicyInfoDTO.setPolicyStatus("1");
		reqClaimPolicyInfoDTO.setClaimAmount("5000");
		/** 险种赔付信息 */
		List<ReqClaimCoveragePaymentInfoDTO> coveragePaymentList = new ArrayList<ReqClaimCoveragePaymentInfoDTO>();
		ReqClaimCoveragePaymentInfoDTO reqClaimCoveragePaymentInfoDTO = new ReqClaimCoveragePaymentInfoDTO();
		reqClaimCoveragePaymentInfoDTO.setCoveragePackageCode("rr");
		reqClaimCoveragePaymentInfoDTO.setCoverageType("1-1");
		reqClaimCoveragePaymentInfoDTO.setComCoverageCode("rrr");
		reqClaimCoveragePaymentInfoDTO.setComCoverageName("险种名称1");
		reqClaimCoveragePaymentInfoDTO.setClaimOpinion("1");
		reqClaimCoveragePaymentInfoDTO.setClaimConclusionCode("1");
		reqClaimCoveragePaymentInfoDTO.setConclusionReason("1 ");
		reqClaimCoveragePaymentInfoDTO.setClaimAmount("2000");
		/** 责任赔付信息 **/
		List<ReqClaimLiabilityPaymentInfoDTO> liabilityPaymentList = new ArrayList<ReqClaimLiabilityPaymentInfoDTO>();
		ReqClaimLiabilityPaymentInfoDTO reqClaimLiabilityPaymentInfoDTO = new ReqClaimLiabilityPaymentInfoDTO();
		reqClaimLiabilityPaymentInfoDTO.setLiabilityType("Clinic-aroundHR");
		reqClaimLiabilityPaymentInfoDTO.setLiabilityName("公司责任名称A");
		reqClaimLiabilityPaymentInfoDTO.setLiabilityCode("1");
		reqClaimLiabilityPaymentInfoDTO.setClaimOpinion("1");
		reqClaimLiabilityPaymentInfoDTO.setClaimConclusionCode("1");
		reqClaimLiabilityPaymentInfoDTO.setConclusionReason("1");
		reqClaimLiabilityPaymentInfoDTO.setPaymentAmount("5000");
		reqClaimLiabilityPaymentInfoDTO.setOperation("1");
		List<ReqClaimWesternMedInfoDTO> westernMediList = new ArrayList<ReqClaimWesternMedInfoDTO>();
		/** 西医疾病 **/
		ReqClaimWesternMedInfoDTO reqClaimWesternMedInfoDTO = new ReqClaimWesternMedInfoDTO();
		reqClaimWesternMedInfoDTO.setHospitalCode("1");
		reqClaimWesternMedInfoDTO.setHospitalName("医疗机构名称");
		reqClaimWesternMedInfoDTO.setWesternMediCode("1");
		westernMediList.add(reqClaimWesternMedInfoDTO);
		reqClaimLiabilityPaymentInfoDTO.setWesternMediList(westernMediList);
		/** 手术 **/
		List<ReqClaimOperationInfoDTO> operationList = new ArrayList<ReqClaimOperationInfoDTO>();
		ReqClaimOperationInfoDTO reqClaimOperationInfoDTO = new ReqClaimOperationInfoDTO();
		reqClaimOperationInfoDTO.setOperationCode("43");
		operationList.add(reqClaimOperationInfoDTO);
		reqClaimLiabilityPaymentInfoDTO.setOperationList(operationList);
		liabilityPaymentList.add(reqClaimLiabilityPaymentInfoDTO);
		reqClaimCoveragePaymentInfoDTO.setLiabilityPaymentList(liabilityPaymentList);
		coveragePaymentList.add(reqClaimCoveragePaymentInfoDTO);
		reqClaimPolicyInfoDTO.setCoveragePaymentList(coveragePaymentList);
		policyList.add(reqClaimPolicyInfoDTO);
		claimInfoDTO.setPolicyList(policyList);
		/** 医疗事件信息 **/
		List<ReqClaimEventInfoLDTO> eventList = new ArrayList<ReqClaimEventInfoLDTO>();
		ReqClaimEventInfoLDTO reqClaimEventInfoLDTO = new ReqClaimEventInfoLDTO();
		reqClaimEventInfoLDTO.setEventDate("2001-01-01");
		reqClaimEventInfoLDTO.setEventType("1");
		reqClaimEventInfoLDTO.setHospitalCode("0000039");
		reqClaimEventInfoLDTO.setHospitalName("123");
		reqClaimEventInfoLDTO.setOperation("1");
		reqClaimEventInfoLDTO.setOperationList(operationList);
		
		/** 收据 **/
		List<ReqClaimReceiptInfoDTO> receiptList = new ArrayList<ReqClaimReceiptInfoDTO>();
		ReqClaimReceiptInfoDTO reqClaimReceiptInfoDTO = new ReqClaimReceiptInfoDTO();
		reqClaimReceiptInfoDTO.setReceiptNo("1");
		reqClaimReceiptInfoDTO.setReceiptType("2");
		reqClaimReceiptInfoDTO.setHospitalCode("0000039");
		reqClaimReceiptInfoDTO.setHospitalName("机构名称");
		reqClaimReceiptInfoDTO.setOccurDate("2001-01-01");
		reqClaimReceiptInfoDTO.setAmount("5000");
		reqClaimReceiptInfoDTO.setHospitalDate("2001-01-01");
		reqClaimReceiptInfoDTO.setDischargeDate("2001-01-01");
		reqClaimReceiptInfoDTO.setHospitalStay("1");
		reqClaimReceiptInfoDTO.setHospitalDept("3");
		reqClaimReceiptInfoDTO.setIssueUnit("2");
		reqClaimReceiptInfoDTO.setRelatedClaimNo("3");
		/** 收据分项费用 **/
		List<ReqClaimReceiptFeeInfoDTO> receiptFeeList = new ArrayList<ReqClaimReceiptFeeInfoDTO>();
		ReqClaimReceiptFeeInfoDTO reqClaimReceiptFeeInfoDTO = new ReqClaimReceiptFeeInfoDTO();
		reqClaimReceiptFeeInfoDTO.setReceiptFeeType("3 ");
		reqClaimReceiptFeeInfoDTO.setAmount("3000");
		receiptFeeList.add(reqClaimReceiptFeeInfoDTO);
		reqClaimReceiptInfoDTO.setReceiptFeeList(receiptFeeList);
		/** 医疗费用 **/
		List<ReqClaimItemInfoDTO> itemList = new ArrayList<ReqClaimItemInfoDTO>();
		ReqClaimItemInfoDTO reqClaimItemInfoDTO = new ReqClaimItemInfoDTO();
		reqClaimItemInfoDTO.setItemCategory("3");
		reqClaimItemInfoDTO.setAmount("4000");
		reqClaimItemInfoDTO.setDeductibleAmount("1500");
		/** 医疗费用明细 **/
		List<ReqClaimDetailInfoDTO> detailList = new ArrayList<ReqClaimDetailInfoDTO>();
		ReqClaimDetailInfoDTO reqClaimDetailInfoDTO = new ReqClaimDetailInfoDTO();
		reqClaimDetailInfoDTO.setName("2");
		reqClaimDetailInfoDTO.setAmount("2000");
		reqClaimDetailInfoDTO.setDeductibleAmount("1000");
		reqClaimDetailInfoDTO.setQuantity("5");
		reqClaimDetailInfoDTO.setUnitPrice("200");
		reqClaimDetailInfoDTO.setUnit("2 ");	
		detailList.add(reqClaimDetailInfoDTO);
		reqClaimItemInfoDTO.setDetailList(detailList);
		itemList.add(reqClaimItemInfoDTO);
		reqClaimReceiptInfoDTO.setItemList(itemList);
		receiptList.add(reqClaimReceiptInfoDTO);
		reqClaimEventInfoLDTO.setReceiptList(receiptList);
		eventList.add(reqClaimEventInfoLDTO);
		List<ReqClaimWesternMedInfoDTO> westernMedInfoDTOs=new ArrayList<ReqClaimWesternMedInfoDTO>();
		ReqClaimWesternMedInfoDTO reqwes=new ReqClaimWesternMedInfoDTO();
		reqwes.setWesternMediCode("12");
		westernMedInfoDTOs.add(reqwes);
		/** 西医疾病 **/
		reqClaimEventInfoLDTO.setWesternMediList(westernMedInfoDTOs);
		/** 手术 **/
		List<ReqClaimOperationInfoDTO> oerList=new ArrayList<ReqClaimOperationInfoDTO>();
		ReqClaimOperationInfoDTO oer=new ReqClaimOperationInfoDTO();
		oer.setOperationCode("2");
		oerList.add(oer);
		reqClaimEventInfoLDTO.setOperationList(oerList);
		claimInfoDTO.setEventList(eventList);
		/**********************************************************/
		ReqClaimInfoDTO claimInfoDTO1 = new ReqClaimInfoDTO();
		claimInfoDTO1.setBizNo("0001");
		claimInfoDTO1.setClaimNo("0008843784738025");
		claimInfoDTO1.setReportDate("2000-01-01");
		claimInfoDTO1.setApplyDate("2000-01-01");
		claimInfoDTO1.setRegistrationDate("2000-01-01");
		claimInfoDTO1.setAccidentReason("ACC");
		claimInfoDTO1.setAccidentDate("2000-01-01");
		claimInfoDTO1.setAccidentPlace("出险地点");
		claimInfoDTO1.setClaimInvestigation("1");
		claimInfoDTO1.setPendingIndi("1");
		claimInfoDTO1.setPendingFinishDate("2000-01-01");
		claimInfoDTO1.setClaimConclusionCode("01");
		claimInfoDTO1.setConclusionReason("2 ");
		claimInfoDTO1.setApplyAmount("5000");
		claimInfoDTO1.setClaimAmount("4000");
		claimInfoDTO1.setSocialInsuPayment("1");
		claimInfoDTO1.setEndCaseDate("2000-01-01");
		claimInfoDTO1.setStatus("01");
		claimInfoDTO1.setClaimantName("张三");
		claimInfoDTO1.setClaimantGender("M");
		claimInfoDTO1.setClaimantBirthday("1990-01-01");
		claimInfoDTO1.setClaimantCertiType("00");
		claimInfoDTO1.setClaimantCertiCode("421002199004100518");
		claimInfoDTO1.setDeathIndi("1");
		claimInfoDTO1.setDeathDate("2000-01-01");
		claimInfoDTO1.setWarningIndi("1");
		claimInfoDTO1.setWarningDesc("2");
		/** 理赔保单信息 **/
		List<ReqClaimPolicyInfoDTO> policyList1 = new ArrayList<ReqClaimPolicyInfoDTO>();
		ReqClaimPolicyInfoDTO reqClaimPolicyInfoDTO1 = new ReqClaimPolicyInfoDTO();
		reqClaimPolicyInfoDTO1.setPolicyNo("1");
		reqClaimPolicyInfoDTO1.setSequenceNo("1");
		reqClaimPolicyInfoDTO1.setCustomerNo("1");
		reqClaimPolicyInfoDTO1.setPolicyStatus("1");
		reqClaimPolicyInfoDTO1.setClaimAmount("5000");
		/** 险种赔付信息 */
		List<ReqClaimCoveragePaymentInfoDTO> coveragePaymentList1 = new ArrayList<ReqClaimCoveragePaymentInfoDTO>();
		ReqClaimCoveragePaymentInfoDTO reqClaimCoveragePaymentInfoDTO1 = new ReqClaimCoveragePaymentInfoDTO();
		reqClaimCoveragePaymentInfoDTO1.setCoveragePackageCode("rr");
		reqClaimCoveragePaymentInfoDTO1.setCoverageType("1-1");
		reqClaimCoveragePaymentInfoDTO1.setComCoverageCode("rrr");
		reqClaimCoveragePaymentInfoDTO1.setComCoverageName("险种名称1");
		reqClaimCoveragePaymentInfoDTO1.setClaimOpinion("1");
		reqClaimCoveragePaymentInfoDTO1.setClaimConclusionCode("1");
		reqClaimCoveragePaymentInfoDTO1.setConclusionReason("1 ");
		reqClaimCoveragePaymentInfoDTO1.setClaimAmount("2000");
		/** 责任赔付信息 **/
		List<ReqClaimLiabilityPaymentInfoDTO> liabilityPaymentList1 = new ArrayList<ReqClaimLiabilityPaymentInfoDTO>();
		ReqClaimLiabilityPaymentInfoDTO reqClaimLiabilityPaymentInfoDTO1 = new ReqClaimLiabilityPaymentInfoDTO();
		reqClaimLiabilityPaymentInfoDTO1.setLiabilityType("Clinic-aroundHR");
		reqClaimLiabilityPaymentInfoDTO1.setLiabilityName("公司责任名称A");
		reqClaimLiabilityPaymentInfoDTO1.setLiabilityCode("1");
		reqClaimLiabilityPaymentInfoDTO1.setClaimOpinion("1");
		reqClaimLiabilityPaymentInfoDTO1.setClaimConclusionCode("1");
		reqClaimLiabilityPaymentInfoDTO1.setConclusionReason("1");
		reqClaimLiabilityPaymentInfoDTO1.setPaymentAmount("5000");
		reqClaimLiabilityPaymentInfoDTO1.setOperation("1");
		List<ReqClaimWesternMedInfoDTO> westernMediList1 = new ArrayList<ReqClaimWesternMedInfoDTO>();
		/** 西医疾病 **/
		ReqClaimWesternMedInfoDTO reqClaimWesternMedInfoDTO1 = new ReqClaimWesternMedInfoDTO();
		reqClaimWesternMedInfoDTO1.setHospitalCode("1");
		reqClaimWesternMedInfoDTO1.setHospitalName("医疗机构名称");
		reqClaimWesternMedInfoDTO1.setWesternMediCode("1");
		ReqClaimWesternMedInfoDTO reqClaimWesternMedInfoDTO2 = new ReqClaimWesternMedInfoDTO();
		reqClaimWesternMedInfoDTO2.setHospitalCode("1");
		reqClaimWesternMedInfoDTO2.setHospitalName("医疗机构名称");
		reqClaimWesternMedInfoDTO2.setWesternMediCode("1");
		westernMediList1.add(reqClaimWesternMedInfoDTO1);
		westernMediList1.add(reqClaimWesternMedInfoDTO2);
		reqClaimLiabilityPaymentInfoDTO1.setWesternMediList(westernMediList1);
		/** 手术 **/
		List<ReqClaimOperationInfoDTO> operationList1 = new ArrayList<ReqClaimOperationInfoDTO>();
		ReqClaimOperationInfoDTO reqClaimOperationInfoDTO1 = new ReqClaimOperationInfoDTO();
		reqClaimOperationInfoDTO1.setOperationCode("43");
		ReqClaimOperationInfoDTO reqClaimOperationInfoDTO2 = new ReqClaimOperationInfoDTO();
		reqClaimOperationInfoDTO2.setOperationCode("43");
		operationList1.add(reqClaimOperationInfoDTO1);
		operationList1.add(reqClaimOperationInfoDTO2);
		reqClaimLiabilityPaymentInfoDTO1.setOperationList(operationList1);
		reqClaimLiabilityPaymentInfoDTO1.setWesternMediList(westernMediList1);
		liabilityPaymentList1.add(reqClaimLiabilityPaymentInfoDTO1);
		reqClaimCoveragePaymentInfoDTO1.setLiabilityPaymentList(liabilityPaymentList1);
		coveragePaymentList1.add(reqClaimCoveragePaymentInfoDTO1);
		reqClaimPolicyInfoDTO1.setCoveragePaymentList(coveragePaymentList1);
		policyList1.add(reqClaimPolicyInfoDTO1);
		claimInfoDTO1.setPolicyList(policyList1);
		/** 医疗事件信息 **/
		List<ReqClaimEventInfoLDTO> eventList1 = new ArrayList<ReqClaimEventInfoLDTO>();
		ReqClaimEventInfoLDTO reqClaimEventInfoLDTO1 = new ReqClaimEventInfoLDTO();
		reqClaimEventInfoLDTO1.setEventDate("2001-01-01");
		reqClaimEventInfoLDTO1.setEventType("1");
		reqClaimEventInfoLDTO1.setHospitalCode("0000039");
		reqClaimEventInfoLDTO1.setHospitalName("123");
		reqClaimEventInfoLDTO1.setOperation("1");
		reqClaimEventInfoLDTO1.setOperationList(operationList1);
		
		/** 收据 **/
		List<ReqClaimReceiptInfoDTO> receiptList1 = new ArrayList<ReqClaimReceiptInfoDTO>();
		ReqClaimReceiptInfoDTO reqClaimReceiptInfoDTO1 = new ReqClaimReceiptInfoDTO();
		reqClaimReceiptInfoDTO1.setReceiptNo("1");
		reqClaimReceiptInfoDTO1.setReceiptType("2");
		reqClaimReceiptInfoDTO1.setHospitalCode("0000039");
		reqClaimReceiptInfoDTO1.setHospitalName("机构名称");
		reqClaimReceiptInfoDTO1.setOccurDate("2001-01-01");
		reqClaimReceiptInfoDTO1.setAmount("5000");
		reqClaimReceiptInfoDTO1.setHospitalDate("2001-01-01");
		reqClaimReceiptInfoDTO1.setDischargeDate("2001-01-01");
		reqClaimReceiptInfoDTO1.setHospitalStay("1");
		reqClaimReceiptInfoDTO1.setHospitalDept("3");
		reqClaimReceiptInfoDTO1.setIssueUnit("2");
		reqClaimReceiptInfoDTO1.setRelatedClaimNo("3");
		/** 收据分项费用 **/
		List<ReqClaimReceiptFeeInfoDTO> receiptFeeList1 = new ArrayList<ReqClaimReceiptFeeInfoDTO>();
		ReqClaimReceiptFeeInfoDTO reqClaimReceiptFeeInfoDTO1 = new ReqClaimReceiptFeeInfoDTO();
		reqClaimReceiptFeeInfoDTO1.setReceiptFeeType("3 ");
		reqClaimReceiptFeeInfoDTO1.setAmount("3000");
		receiptFeeList1.add(reqClaimReceiptFeeInfoDTO1);
		reqClaimReceiptInfoDTO1.setReceiptFeeList(receiptFeeList1);
		/** 医疗费用 **/
		List<ReqClaimItemInfoDTO> itemList1 = new ArrayList<ReqClaimItemInfoDTO>();
		ReqClaimItemInfoDTO reqClaimItemInfoDTO1 = new ReqClaimItemInfoDTO();
		reqClaimItemInfoDTO1.setItemCategory("3");
		reqClaimItemInfoDTO1.setAmount("4000");
		reqClaimItemInfoDTO1.setDeductibleAmount("1500");
		/** 医疗费用明细 **/
		List<ReqClaimDetailInfoDTO> detailList1 = new ArrayList<ReqClaimDetailInfoDTO>();
		ReqClaimDetailInfoDTO reqClaimDetailInfoDTO1 = new ReqClaimDetailInfoDTO();
		reqClaimDetailInfoDTO1.setName("2");
		reqClaimDetailInfoDTO1.setAmount("2000");
		reqClaimDetailInfoDTO1.setDeductibleAmount("1000");
		reqClaimDetailInfoDTO1.setQuantity("5");
		reqClaimDetailInfoDTO1.setUnitPrice("200");
		reqClaimDetailInfoDTO1.setUnit("2 ");	
		detailList1.add(reqClaimDetailInfoDTO1);
		reqClaimItemInfoDTO1.setDetailList(detailList1);
		itemList1.add(reqClaimItemInfoDTO1);
		reqClaimReceiptInfoDTO1.setItemList(itemList1);
		receiptList1.add(reqClaimReceiptInfoDTO1);
		reqClaimEventInfoLDTO1.setReceiptList(receiptList1);
		eventList1.add(reqClaimEventInfoLDTO1);
		List<ReqClaimWesternMedInfoDTO> westernMedInfoDTOs1=new ArrayList<ReqClaimWesternMedInfoDTO>();
		ReqClaimWesternMedInfoDTO reqwes1=new ReqClaimWesternMedInfoDTO();
		reqwes1.setWesternMediCode("12");
		westernMedInfoDTOs1.add(reqwes1);
		/** 西医疾病 **/
		reqClaimEventInfoLDTO1.setWesternMediList(westernMedInfoDTOs1);
		/** 手术 **/
		List<ReqClaimOperationInfoDTO> oerList1=new ArrayList<ReqClaimOperationInfoDTO>();
		ReqClaimOperationInfoDTO oer1=new ReqClaimOperationInfoDTO();
		oer1.setOperationCode("2");
		oerList1.add(oer1);
		reqClaimEventInfoLDTO1.setOperationList(oerList1);
		claimInfoDTO1.setEventList(eventList1);
		
		
		
		
		List<ReqClaimInfoDTO> list=new ArrayList<ReqClaimInfoDTO>();
		list.add(claimInfoDTO);
		list.add(claimInfoDTO1);
		ReqClaimCoreMDTO reqClaimCoreMDTO=new ReqClaimCoreMDTO();
		reqClaimCoreMDTO.setBody(list);
		reqClaimCoreMDTO.setRequestHead(requestHead);	
		requestJson = JSONObject.toJSONString(reqClaimCoreMDTO);
		System.out.println(requestJson);
	}
	@Test
	public void testClaim(){
		claimMWebService.dealMainBiz(requestJson);
	}
}
