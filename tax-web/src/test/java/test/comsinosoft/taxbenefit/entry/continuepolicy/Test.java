package test.comsinosoft.taxbenefit.entry.continuepolicy;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.taxbenefit.api.dto.request.base.CoreRequestHead;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyCoverageInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyCustomerInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyInfoCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyLiabilityInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyUwinfoInfoDTO;

public class Test {
	public static void main(String[] args) {
		String bodyJson;
		ReqContinuePolicyInfoCoreDTO reqContinuePolicyInfoCoreDTO = new ReqContinuePolicyInfoCoreDTO();
		//报文头
		CoreRequestHead coreRequestHead = new CoreRequestHead();
		coreRequestHead.setAreaCode("shanghai");
		coreRequestHead.setTransType("RNW001");
		coreRequestHead.setRecordNum("1");
		coreRequestHead.setSerialNo("A1100001");
		coreRequestHead.setSignKey("8bbd19fa345ad784fbc32e67cea072ed");
		coreRequestHead.setSourceCode("01");
		coreRequestHead.setTransDate("2015-02-16 10:20:25");
		//报文体
		List<ReqContinuePolicyCoverageInfoDTO> policyCoverageInfoList = new ArrayList<ReqContinuePolicyCoverageInfoDTO>();
		List<ReqContinuePolicyUwinfoInfoDTO> policyUwinfoInfoList = new ArrayList<ReqContinuePolicyUwinfoInfoDTO>();
		List<ReqContinuePolicyLiabilityInfoDTO> policyLiabilityInfoList = new ArrayList<ReqContinuePolicyLiabilityInfoDTO>();
		List<ReqContinuePolicyCustomerInfoDTO> policyCustomerInfoList =new ArrayList<ReqContinuePolicyCustomerInfoDTO>();
		
		ReqContinuePolicyInfoDTO reqPolicyInfoDTO =new ReqContinuePolicyInfoDTO();
		ReqContinuePolicyCoverageInfoDTO reqPolicyCoverageInfoDTO = new ReqContinuePolicyCoverageInfoDTO();
		ReqContinuePolicyUwinfoInfoDTO reqPolicyUwinfoInfoDTO = new ReqContinuePolicyUwinfoInfoDTO();
		ReqContinuePolicyLiabilityInfoDTO reqPolicyLiabilityInfoDTO = new ReqContinuePolicyLiabilityInfoDTO();
		ReqContinuePolicyCustomerInfoDTO reqPolicyCustomerInfoDTO = new ReqContinuePolicyCustomerInfoDTO();
		
		reqPolicyUwinfoInfoDTO.setUnderwritingDate("2016-01-01");
		reqPolicyUwinfoInfoDTO.setUnderwritingDecision("123");
		reqPolicyUwinfoInfoDTO.setUnderwritingDes("123");
		policyUwinfoInfoList.add(reqPolicyUwinfoInfoDTO);
		
		reqPolicyLiabilityInfoDTO.setLiabilityCode("123");
		reqPolicyLiabilityInfoDTO.setLiabilitySa(new BigDecimal("0.098"));
		reqPolicyLiabilityInfoDTO.setLiabilityStatus("123");
		policyLiabilityInfoList.add(reqPolicyLiabilityInfoDTO);
		
		reqPolicyCoverageInfoDTO.setComCoverageCode("123");
		reqPolicyCoverageInfoDTO.setCoverageEffectiveDate("2016-01-01");
		reqPolicyCoverageInfoDTO.setCoverageExpireDate("2016-01-01");
		reqPolicyCoverageInfoDTO.setCoveragePackageCode("123");
		reqPolicyCoverageInfoDTO.setPremium(new BigDecimal("0.098"));
		reqPolicyCoverageInfoDTO.setInsuranceCoverage(new BigDecimal("0.098"));
		reqPolicyCoverageInfoDTO.setLiabilityList(policyLiabilityInfoList);
//		reqPolicyCoverageInfoDTO.setUwinfoList(policyUwinfoInfoList);
		policyCoverageInfoList.add(reqPolicyCoverageInfoDTO);
		
		reqPolicyCustomerInfoDTO.setSequenceNo("123");
		reqPolicyCustomerInfoDTO.setCustomerNo("123");
		reqPolicyCustomerInfoDTO.setCoverageList(policyCoverageInfoList);
		policyCustomerInfoList.add(reqPolicyCustomerInfoDTO);
		
		
		reqPolicyInfoDTO.setBizNo("123");
		reqPolicyInfoDTO.setPolicyNo("123");
		reqPolicyInfoDTO.setEffectiveDate("2016-01-01");
		reqPolicyInfoDTO.setExpireDate("2016-01-01");
		reqPolicyInfoDTO.setRenewalEndorsementNo("123");
		reqPolicyInfoDTO.setCustomerList(policyCustomerInfoList);
		
		reqContinuePolicyInfoCoreDTO.setBody(reqPolicyInfoDTO);
		reqContinuePolicyInfoCoreDTO.setRequestHead(coreRequestHead);
		
		bodyJson = JSONObject.toJSONString(reqContinuePolicyInfoCoreDTO);
		System.out.println(bodyJson);
	}
}
