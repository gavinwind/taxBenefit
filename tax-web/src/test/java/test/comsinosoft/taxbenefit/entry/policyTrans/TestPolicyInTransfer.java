package test.comsinosoft.taxbenefit.entry.policyTrans;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.platform.base.TaxBaseTest;
import com.sinosoft.taxbenefit.api.dto.request.base.CoreRequestHead;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqMInPolicyTransCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqMInPolicyTransferDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqPolicyTransClientDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqPolicyTransferOutDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqSInPolicyTransCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqSInPolicyTransferDTO;
import com.sinosoft.taxbenefit.entry.policytransfer.webservice.PolicySInTransferWebService;
/**
 * 保单转入申请 -测试用例
 * @author SheChunMing
 */
public class TestPolicyInTransfer extends TaxBaseTest{
	
	String bodyJson;
	@Autowired
	PolicySInTransferWebService PolicySInTransferWebService;
	
	@Before
	public void prepareData(){
		ReqMInPolicyTransCoreDTO reqSPolicyTransCoreDTO = new ReqMInPolicyTransCoreDTO();
		// 报文头
		CoreRequestHead coreRequestHead = new CoreRequestHead();
		coreRequestHead.setAreaCode("shanghai");
		coreRequestHead.setTransType("END004"); //END005
		coreRequestHead.setRecordNum("1");
		coreRequestHead.setSerialNo("A1100001");
		coreRequestHead.setSignKey("0011667d6f5b7e1ec0c419ea7293e020");
		coreRequestHead.setSourceCode("01");
		coreRequestHead.setTransDate("2015-02-16 10:20:25");
		// 报文体
		List<ReqPolicyTransClientDTO> reqPolicyTransClientList = new ArrayList<ReqPolicyTransClientDTO>();
		ReqPolicyTransClientDTO reqPolicyTransClientDTO = new ReqPolicyTransClientDTO();
		reqPolicyTransClientDTO.setSequenceNo("A01");
		reqPolicyTransClientList.add(reqPolicyTransClientDTO);
		
		List<ReqPolicyTransferOutDTO> reqPolicyTransferOutList = new ArrayList<ReqPolicyTransferOutDTO>();
		ReqPolicyTransferOutDTO reqPolicyTransferOutDTO = new ReqPolicyTransferOutDTO();
		reqPolicyTransferOutDTO.setBizNo("A001");
		reqPolicyTransferOutDTO.setCompanyCode("Z0001");
		reqPolicyTransferOutDTO.setCompanyName("asawfd");
		reqPolicyTransferOutDTO.setCustomerNo("Q120");
		reqPolicyTransferOutDTO.setPolicyNo("1535");
		reqPolicyTransferOutDTO.setClientList(reqPolicyTransClientList);
		reqPolicyTransferOutList.add(reqPolicyTransferOutDTO);
		
		ReqMInPolicyTransferDTO reqSPolicyTransferDTO = new ReqMInPolicyTransferDTO();
		reqSPolicyTransferDTO.setBankName("ACBD");
		reqSPolicyTransferDTO.setAccountNo("13545");
		reqSPolicyTransferDTO.setAccountHolder("dafgs");
		reqSPolicyTransferDTO.setContactName("AAA");
		reqSPolicyTransferDTO.setContactEmail("dadw2312@qq.com");
		reqSPolicyTransferDTO.setContactTele("13543");
		reqSPolicyTransferDTO.setTransApplyDate("2015-2-15");
		reqSPolicyTransferDTO.setTransferOutList(reqPolicyTransferOutList);
		
		reqSPolicyTransCoreDTO.setBody(reqSPolicyTransferDTO);
		reqSPolicyTransCoreDTO.setRequestHead(coreRequestHead);
		
		bodyJson = JSONObject.toJSONString(reqSPolicyTransCoreDTO);
	}
	@Test
	public void tsetPolicyTrensfer(){
		try {
			PolicySInTransferWebService.dealMainBiz(bodyJson);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
}
