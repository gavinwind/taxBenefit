package test.comsinosoft.taxbenefit.entry.renewalpremium;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.platform.base.TaxBaseTest;
import com.sinosoft.taxbenefit.api.dto.request.base.CoreRequestHead;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqRenewalPremiumCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqRenewalPremiumCoverageInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqRenewalPremiumInfoDTO;
import com.sinosoft.taxbenefit.entry.renewalpremium.webservice.PremiumUploadWebService;

/**
 * 续期保费上传测试类
 * Title:RenewalPremium
 * @author yangdongkai@outlook.com
 * @date 2016年3月9日 上午9:16:18
 */
public class TestRenewalPremium extends TaxBaseTest{
	@Autowired
	PremiumUploadWebService remiumUploadWebService;
	
	String bodyJson;
	@Before
	public void prepareData(){
		ReqRenewalPremiumCoreDTO reqRenewalPremiumCoreDTO = new ReqRenewalPremiumCoreDTO();
		//报文头
		CoreRequestHead coreRequestHead = new CoreRequestHead();
		coreRequestHead.setAreaCode("shanghai");
		coreRequestHead.setTransType("PRM001");
		coreRequestHead.setRecordNum("1");
		coreRequestHead.setSerialNo("A5131132314629");
		coreRequestHead.setSignKey("4af347bb350ef2db9f59ac365d4db220");
		coreRequestHead.setSourceCode("01");
		coreRequestHead.setTransDate("2015-02-16 10:20:25");
		//报文体
		ReqRenewalPremiumInfoDTO reqRenewalPremiumInfoDTO = new ReqRenewalPremiumInfoDTO();
		
		ReqRenewalPremiumCoverageInfoDTO reqPremiumCoverageInfoDTO = new ReqRenewalPremiumCoverageInfoDTO();
		List<ReqRenewalPremiumCoverageInfoDTO> reqPremiumCoverageInfoList = new ArrayList<ReqRenewalPremiumCoverageInfoDTO>();
		
		reqPremiumCoverageInfoDTO.setComCoverageCode("A111");
		reqPremiumCoverageInfoDTO.setCoveragePackageCode("A111-1");
		reqPremiumCoverageInfoDTO.setCoveragePremium(new BigDecimal("0.098"));
		reqPremiumCoverageInfoDTO.setRiskPremium(new BigDecimal("0.098"));
		reqPremiumCoverageInfoList.add(reqPremiumCoverageInfoDTO);
		
		
		reqRenewalPremiumInfoDTO.setBizNo("111");
		reqRenewalPremiumInfoDTO.setPolicyNo("123");
		reqRenewalPremiumInfoDTO.setSequenceNo("123");
		reqRenewalPremiumInfoDTO.setConfirmDate("2016-01-01");
		reqRenewalPremiumInfoDTO.setFeeId("111");
		reqRenewalPremiumInfoDTO.setFeeStatus("222");
		reqRenewalPremiumInfoDTO.setPayDueDate("2016-01-01");
		reqRenewalPremiumInfoDTO.setPayFromDate("2016-01-01");
		reqRenewalPremiumInfoDTO.setPayToDate("2016-01-01");
		reqRenewalPremiumInfoDTO.setPaymentFrequency("123456");
		reqRenewalPremiumInfoDTO.setPremiumAmount(new BigDecimal("0.098"));
		reqRenewalPremiumInfoDTO.setFeeStatus("123456");
		reqRenewalPremiumInfoDTO.setCoverageList(reqPremiumCoverageInfoList);

		
//		reqRenewalPremiumCoreDTO.setBody(reqRenewalPremiumInfoDTO);
		reqRenewalPremiumCoreDTO.setRequestHead(coreRequestHead);
		
		bodyJson = JSONObject.toJSONString(reqRenewalPremiumCoreDTO);
	}
	
	@Test
	public void RenewalPremiumTest(){
		System.out.println(bodyJson);
		try {
			remiumUploadWebService.dealMainBiz(bodyJson);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
