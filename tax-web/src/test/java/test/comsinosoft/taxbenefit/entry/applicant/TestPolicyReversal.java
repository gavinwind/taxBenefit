package test.comsinosoft.taxbenefit.entry.applicant;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.platform.base.TaxBaseTest;
import com.sinosoft.taxbenefit.api.dto.request.applicant.ReqPolicyCancelDTO;
import com.sinosoft.taxbenefit.api.dto.request.applicant.ReqPolicyReversalDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.CoreRequestHead;
import com.sinosoft.taxbenefit.entry.applicant.webservice.PolicyReversalWebService;

public class TestPolicyReversal extends TaxBaseTest {
	@Autowired
	PolicyReversalWebService policyReversalWebService;
	String json="";
	@Before
	public void jass() {
		// 报文头
		CoreRequestHead requestHead = new CoreRequestHead();
		requestHead.setAreaCode("01");
		requestHead.setTransType("NBU003");
		requestHead.setRecordNum("1");
		requestHead.setSerialNo("0001");
		requestHead.setSignKey("bbc6b525b305d3883abd13c7fba6a4d4");
		requestHead.setSourceCode("01");
		requestHead.setTransDate("2000-01-01");
		ReqPolicyReversalDTO reqPolicyReversalDTO = new ReqPolicyReversalDTO();
		ReqPolicyCancelDTO reqPolicyCancelDTO = new ReqPolicyCancelDTO();
		reqPolicyCancelDTO.setBizNo("fgfgfgfg");
		reqPolicyCancelDTO.setCancelDate("2016-01-01");
		reqPolicyCancelDTO.setCancelReason("12358");
		reqPolicyCancelDTO.setPolicyNo("12354");
		reqPolicyReversalDTO.setBody(reqPolicyCancelDTO);
		reqPolicyReversalDTO.setRequestHead(requestHead);
		json=JSONObject.toJSONString(reqPolicyReversalDTO);
	}

	@Test
	public void testReversal() {
		System.out.println(json);
		policyReversalWebService.dealMainBiz(json);
	}

}
