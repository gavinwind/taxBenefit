package test.comsinosoft.taxbenefit.entry.customer;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.platform.base.TaxBaseTest;
import com.sinosoft.taxbenefit.api.dto.request.base.CoreRequestHead;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqTaxBeneVerifyCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqTaxBeneVerifyInfoDTO;

import com.sinosoft.taxbenefit.entry.customer.webservice.TaxBeneVerifyWebService;

public class TsetTaxBeneVerify extends TaxBaseTest{
	
	    String bodyJson;
	    
	    @Autowired
	    TaxBeneVerifyWebService taxBeneVerifyService;
	    
		@Before
		public void prepareData(){
			//
			ReqTaxBeneVerifyCoreDTO reqTaxBeneVerifyDTO = new ReqTaxBeneVerifyCoreDTO();
			// 报文头
			CoreRequestHead coreRequestHead = new CoreRequestHead();
			coreRequestHead.setAreaCode("shanghai");
			coreRequestHead.setTransType("PTY003");
			coreRequestHead.setRecordNum("1");
			coreRequestHead.setSerialNo("A110005675");
			coreRequestHead.setSignKey("6f0add3cc7745a5d946034706a82806b");
			coreRequestHead.setSourceCode("01");
			coreRequestHead.setTransDate("2015-02-16 10:20:25");
			// 报文体
			
			ReqTaxBeneVerifyInfoDTO reqCustomerInfoDTO = new ReqTaxBeneVerifyInfoDTO();
			reqCustomerInfoDTO.setName("小米");
			reqCustomerInfoDTO.setGender("女");
			reqCustomerInfoDTO.setBirthday("1993-01-02");
			reqCustomerInfoDTO.setCertiNo("2343567658");
			reqCustomerInfoDTO.setCertiType("身份证");
			reqCustomerInfoDTO.setNationality("中国");
			reqCustomerInfoDTO.setProposalNo("6256784678");
			reqCustomerInfoDTO.setPolicySource("网销");
			reqCustomerInfoDTO.setCoverageEffectiveDate("2015-01-01");
			reqCustomerInfoDTO.setBizNo("326546");
			
			
			reqTaxBeneVerifyDTO.setBody(reqCustomerInfoDTO);
			reqTaxBeneVerifyDTO.setRequestHead(coreRequestHead);
			
			bodyJson = JSONObject.toJSONString(reqTaxBeneVerifyDTO);
		}
		
		@Test
		public void tsetPolicyTrensfer(){
			
			
			System.out.println(bodyJson);
			taxBeneVerifyService.dealMainBiz(bodyJson);
		    			
		}


}
