package test.comsinosoft.taxbenefit.entry.customer;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.platform.base.TaxBaseTest;
import com.sinosoft.taxbenefit.api.dto.request.base.CoreRequestHead;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqSelCustomerCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqSelCustomerInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.SelCustomerDTO;
import com.sinosoft.taxbenefit.entry.customer.webservice.SelCustomerInfoWebService;

public class TestCustomerQueryInfo extends TaxBaseTest{
	
	 String bodyJson;
     
	    @Autowired
	    SelCustomerInfoWebService selCustomerInfoWebService;
		@Before
		public void prepareData(){
			//
			ReqSelCustomerCoreDTO reqSelCustomerCoreDTO = new ReqSelCustomerCoreDTO();
			// 报文头
			CoreRequestHead coreRequestHead = new CoreRequestHead();
			coreRequestHead.setAreaCode("shanghai");
			coreRequestHead.setTransType("PTY001");
			coreRequestHead.setRecordNum("1");
			coreRequestHead.setSerialNo("A11000009");
			coreRequestHead.setSignKey("14118469dd199ca6745efdf821f4cefb");
			coreRequestHead.setSourceCode("01");
			coreRequestHead.setTransDate("2015-02-16 10:20:25");
			// 报文体
			ReqSelCustomerInfoDTO reqSelCustomerInfoDTO=new ReqSelCustomerInfoDTO();
			SelCustomerDTO selCustomerDTO=new SelCustomerDTO();
			selCustomerDTO.setApplyNo("34534265");
			selCustomerDTO.setCustomerNo("3534654657");
			selCustomerDTO.setEndDate("2012-01-02");
			selCustomerDTO.setStartDate("2011-01-02");
			selCustomerDTO.setPolicyNo("4354356");
			selCustomerDTO.setTaxDiscountedIndi("Y");
			reqSelCustomerInfoDTO.setCustomer(selCustomerDTO);
			reqSelCustomerInfoDTO.setOperator("A01");
			
			reqSelCustomerCoreDTO.setBody(reqSelCustomerInfoDTO);
			reqSelCustomerCoreDTO.setRequestHead(coreRequestHead);
			
			
			bodyJson = JSONObject.toJSONString(reqSelCustomerCoreDTO);
		}
		
		@Test
		public void tsetPolicyTrensfer(){
			
			System.out.println(bodyJson);
			
			selCustomerInfoWebService.dealMainBiz(bodyJson);
			
			
		}


}
