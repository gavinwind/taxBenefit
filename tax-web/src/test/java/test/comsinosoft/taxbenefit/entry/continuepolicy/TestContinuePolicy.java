package test.comsinosoft.taxbenefit.entry.continuepolicy;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.platform.base.TaxBaseTest;
import com.sinosoft.taxbenefit.api.dto.request.base.CoreRequestHead;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyCoverageInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyCustomerInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyInfoCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyLiabilityInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyUwinfoInfoDTO;
import com.sinosoft.taxbenefit.entry.continuepolicy.webservice.ContinuePolicyUploadWebService;

/**
 * 续保信息上传测试类
 * Title:ContinuePolicy
 * @author yangdongkai@outlook.com
 * @date 2016年3月8日 下午5:08:14
 */
public class TestContinuePolicy extends TaxBaseTest{
	@Autowired
	ContinuePolicyUploadWebService continuePolicyUploadWebService;
	
	String bodyJson;
	@Before
	public void prepareData(){
		ReqContinuePolicyInfoCoreDTO reqContinuePolicyInfoCoreDTO = new ReqContinuePolicyInfoCoreDTO();
		//报文头
		CoreRequestHead coreRequestHead = new CoreRequestHead();
		coreRequestHead.setAreaCode("shanghai");
		coreRequestHead.setTransType("RNW001");
		coreRequestHead.setRecordNum("1");
		coreRequestHead.setSerialNo("A121021");
		coreRequestHead.setSignKey("3842b05480d0072f9bb0bd379de87f57");
		coreRequestHead.setSourceCode("01");
		coreRequestHead.setTransDate("2015-02-16 10:20:25");
		//报文体
		List<ReqContinuePolicyCoverageInfoDTO> policyCoverageInfoList = new ArrayList<ReqContinuePolicyCoverageInfoDTO>();
		List<ReqContinuePolicyUwinfoInfoDTO> policyUwinfoInfoList = new ArrayList<ReqContinuePolicyUwinfoInfoDTO>();
		List<ReqContinuePolicyLiabilityInfoDTO> policyLiabilityInfoList = new ArrayList<ReqContinuePolicyLiabilityInfoDTO>();
		List<ReqContinuePolicyCustomerInfoDTO> policyCustomerInfoList =new ArrayList<ReqContinuePolicyCustomerInfoDTO>();
		
		ReqContinuePolicyInfoDTO reqPolicyInfoDTO =new ReqContinuePolicyInfoDTO();
		ReqContinuePolicyCoverageInfoDTO reqPolicyCoverageInfoDTO = new ReqContinuePolicyCoverageInfoDTO();
		ReqContinuePolicyUwinfoInfoDTO reqPolicyUwinfoInfoDTO = new ReqContinuePolicyUwinfoInfoDTO();
		ReqContinuePolicyLiabilityInfoDTO reqPolicyLiabilityInfoDTO = new ReqContinuePolicyLiabilityInfoDTO();
		ReqContinuePolicyCustomerInfoDTO reqPolicyCustomerInfoDTO = new ReqContinuePolicyCustomerInfoDTO();
		
		reqPolicyUwinfoInfoDTO.setUnderwritingDate("2016-01-01");
		reqPolicyUwinfoInfoDTO.setUnderwritingDecision("123");
		reqPolicyUwinfoInfoDTO.setUnderwritingDes("123");
		policyUwinfoInfoList.add(reqPolicyUwinfoInfoDTO);
		
		reqPolicyLiabilityInfoDTO.setLiabilityCode("123");
		reqPolicyLiabilityInfoDTO.setLiabilitySa(new BigDecimal("0.098"));
		reqPolicyLiabilityInfoDTO.setLiabilityStatus("123");
		policyLiabilityInfoList.add(reqPolicyLiabilityInfoDTO);
		
		reqPolicyCoverageInfoDTO.setComCoverageCode("123");
		reqPolicyCoverageInfoDTO.setCoverageEffectiveDate("2016-01-01");
		reqPolicyCoverageInfoDTO.setCoverageExpireDate("2016-01-01");
		reqPolicyCoverageInfoDTO.setCoveragePackageCode("123");
		reqPolicyCoverageInfoDTO.setPremium(new BigDecimal("0.098"));
		reqPolicyCoverageInfoDTO.setInsuranceCoverage(new BigDecimal("0.098"));
		reqPolicyCoverageInfoDTO.setLiabilityList(policyLiabilityInfoList);
//		reqPolicyCoverageInfoDTO.setUwinfoList(policyUwinfoInfoList);
		policyCoverageInfoList.add(reqPolicyCoverageInfoDTO);
		
		reqPolicyCustomerInfoDTO.setSequenceNo("123");
		reqPolicyCustomerInfoDTO.setCustomerNo("123");
		reqPolicyCustomerInfoDTO.setCoverageList(policyCoverageInfoList);
		policyCustomerInfoList.add(reqPolicyCustomerInfoDTO);
		
		
		reqPolicyInfoDTO.setBizNo("123");
		reqPolicyInfoDTO.setPolicyNo("123");
		reqPolicyInfoDTO.setEffectiveDate("2016-01-01");
		reqPolicyInfoDTO.setExpireDate("2016-01-01");
		reqPolicyInfoDTO.setRenewalEndorsementNo("123");
		reqPolicyInfoDTO.setCustomerList(policyCustomerInfoList);
		
		reqContinuePolicyInfoCoreDTO.setBody(reqPolicyInfoDTO);
		reqContinuePolicyInfoCoreDTO.setRequestHead(coreRequestHead);
		
		bodyJson = JSONObject.toJSONString(reqContinuePolicyInfoCoreDTO);
	}
	
	
	@Test
	public void ContinuePolicyTest(){
		try {
			System.out.println(bodyJson);
			continuePolicyUploadWebService.dealMainBiz(bodyJson);				
		} catch (Exception e) {
			e.printStackTrace();;
		}

	}
}
