package test.comsinosoft.taxbenefit.entry.customer;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.taxbenefit.api.config.ENUM_BZ_RESPONSE_RESULT;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.response.customer.CustomerVerifyResultDTO;

public class ResultCustomerTest {

	public static void main(String[] args) {

		ThirdSendResponseDTO ThirdSendResponse = new ThirdSendResponseDTO();
		ThirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
		ThirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
		CustomerVerifyResultDTO customerVerifyResultDTO=new CustomerVerifyResultDTO();
		customerVerifyResultDTO.setBizNo("343");
		customerVerifyResultDTO.setCustomerCode("45435326");
		customerVerifyResultDTO.setTaxDiscountedExistIndi("1");
		ThirdSendResponse.setResJson(customerVerifyResultDTO);

		System.out.println(JSONObject.toJSONString(ThirdSendResponse));
	}

}
