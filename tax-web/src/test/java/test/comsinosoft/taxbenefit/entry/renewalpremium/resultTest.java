package test.comsinosoft.taxbenefit.entry.renewalpremium;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.taxbenefit.api.config.ENUM_BZ_RESPONSE_RESULT;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.response.renewalpremium.ResultRenewalPremiumDTO;
/**
 * 续期保费信息上传中保信返回Json
 * Title:resultTest
 * @author yangdongkai@outlook.com
 * @date 2016年3月11日 下午4:40:48
 */
public class resultTest {
	public static void main(String[] args) {
		ThirdSendResponseDTO ThirdSendResponse = new ThirdSendResponseDTO();
		ThirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
		ThirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
		ResultRenewalPremiumDTO resultRenewalPremiumDTO = new ResultRenewalPremiumDTO();
		resultRenewalPremiumDTO.setBizNo("1");
		resultRenewalPremiumDTO.setFeeSequenceNo("123");
		ThirdSendResponse.setResJson(resultRenewalPremiumDTO);
		System.out.println(JSONObject.toJSONString(ThirdSendResponse));
	}
}
