package test.comsinosoft.taxbenefit.entry.bargaincheckquery;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.platform.base.TaxBaseTest;
import com.sinosoft.taxbenefit.api.dto.request.bargaincheckquery.ReqBargainCheckQueryCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.bargaincheckquery.ReqBargainCheckQueryDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.CoreRequestHead;
import com.sinosoft.taxbenefit.entry.bargaincheckquery.webservice.BargainCheckQueryWebService;

public class TestBargainCheckQuery extends TaxBaseTest{
	@Autowired
	BargainCheckQueryWebService bargainCheckQueryWebService;
	
	String bodyJson;
	@Before
	public void prepareData(){
		ReqBargainCheckQueryCoreDTO reqBargainCheckQueryCoreDTO = new ReqBargainCheckQueryCoreDTO();
		//报文头
		CoreRequestHead coreRequestHead = new CoreRequestHead();
		coreRequestHead.setAreaCode("shanghai");
		coreRequestHead.setTransType("CHK001");
		coreRequestHead.setRecordNum("1");
		coreRequestHead.setSerialNo("A11111134");
		coreRequestHead.setSignKey("57347b88ea203a3be86f807f25e4118f");
		coreRequestHead.setSourceCode("01");
		coreRequestHead.setTransDate("2015-02-16 10:20:25");
		//报文体
		ReqBargainCheckQueryDTO reqBargainCheckQueryDTO = new ReqBargainCheckQueryDTO();
		reqBargainCheckQueryDTO.setCheckDate("2016-01-01");
		reqBargainCheckQueryDTO.setComFailNum(new BigDecimal("0.098"));
		reqBargainCheckQueryDTO.setComSuccessNum(new BigDecimal("0.098"));
		reqBargainCheckQueryDTO.setRequestTotalNum(new BigDecimal("0.098"));
		reqBargainCheckQueryCoreDTO.setBody(reqBargainCheckQueryDTO);
		reqBargainCheckQueryCoreDTO.setRequestHead(coreRequestHead);
		
		bodyJson = JSONObject.toJSONString(reqBargainCheckQueryCoreDTO);
	}
	
	@Test
	public void TestBargainCheck(){
		System.out.println(bodyJson);
		try {
			bargainCheckQueryWebService.dealMainBiz(bodyJson);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
