package test.comsinosoft.taxbenefit.entry.accountchange;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.fastjson.JSONObject;
import com.sinosoft.platform.base.TaxBaseTest;
import com.sinosoft.taxbenefit.api.dto.request.accountchange.ReqAccountCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.accountchange.ReqAccountInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.CoreRequestHead;
import com.sinosoft.taxbenefit.entry.accountchange.webservice.AccountChangeWebService;



public class TestAccountChange extends TaxBaseTest{
	
	 String bodyJson;
     
	    @Autowired
	    AccountChangeWebService accountChangeWebService;
		@Before
		public void prepareData(){
			//
			ReqAccountCoreDTO reqAccountCoreDTO = new ReqAccountCoreDTO();
			// 报文头
			CoreRequestHead coreRequestHead = new CoreRequestHead();
			coreRequestHead.setAreaCode("shanghai");
			coreRequestHead.setTransType("ACT001");
			coreRequestHead.setRecordNum("1");
			coreRequestHead.setSerialNo("A11000023432");
			coreRequestHead.setSignKey("14118469dd199ca6745efdf821f4cefb");
			coreRequestHead.setSourceCode("01");
			coreRequestHead.setTransDate("2015-02-16 10:20:25");
			// 报文体
			ReqAccountInfoDTO reqAccountInfoDTO=new ReqAccountInfoDTO();
			reqAccountInfoDTO.setAccountFeeSequenceNo("42432");
			reqAccountInfoDTO.setAdjustReason("dsfjksdl");
			reqAccountInfoDTO.setBalanceAmount(null);
			reqAccountInfoDTO.setComCoverageCode("A123");
			reqAccountInfoDTO.setComCoverageName("人寿");
			reqAccountInfoDTO.setComFeeId("234");
			reqAccountInfoDTO.setCoveragePackageCode("32543");
			reqAccountInfoDTO.setFeeAmount(null);
			reqAccountInfoDTO.setFeeDate("2013-03-23");
			reqAccountInfoDTO.setFeeType("sha");
			reqAccountInfoDTO.setInterestRate(null);
			reqAccountInfoDTO.setPolicyNo("365476576");
			reqAccountInfoDTO.setRevComFeeId("24325");
			reqAccountInfoDTO.setRiskComCoverageCode("A12");
			reqAccountInfoDTO.setRiskCoverageEndDate("2015-01-01");
			reqAccountInfoDTO.setRiskCoverageStartDate("2014-01-01");
			reqAccountInfoDTO.setSequenceNo("B3435");
			
			reqAccountCoreDTO.setBody(reqAccountInfoDTO);
			reqAccountCoreDTO.setRequestHead(coreRequestHead);
			
			
			bodyJson = JSONObject.toJSONString(reqAccountCoreDTO);
		}
		
		@Test
		public void tsetAccountChange(){
			
			System.out.println(bodyJson);
			
			accountChangeWebService.dealMainBiz(bodyJson);
			
		}
}
