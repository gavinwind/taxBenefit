package test.comsinosoft.taxbenefit.entry.claim;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.platform.base.TaxBaseTest;
import com.sinosoft.taxbenefit.api.dto.request.base.CoreRequestHead;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimReversalInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimReversalDTO;
import com.sinosoft.taxbenefit.entry.claims.webservice.ClaimReversalWebService;
import com.sinosoft.taxbenefit.entry.claims.webservice.ClaimWebService;

public class ClaimReversalTest extends TaxBaseTest {
	@Autowired
	ClaimReversalWebService claimReversalWebService;
	String requestJson = "";

	@Before
	public void prepareData() {
		// 报文头
		CoreRequestHead requestHead = new CoreRequestHead();
		requestHead.setAreaCode("01");
		requestHead.setTransType("CLM003");
		requestHead.setRecordNum("1");
		requestHead.setSerialNo("0443001");
		requestHead.setSignKey("bbc6b525b305d3883abd13c7fba6a4d4");
		requestHead.setSourceCode("01");
		requestHead.setTransDate("2016-01-01");
		ReqClaimReversalInfoDTO body = new ReqClaimReversalInfoDTO();
		body.setCancelDate("2016-02-02");
		body.setClaimNo("0008843784738025");
		body.setCancellationReason("567");
		ReqClaimReversalDTO reqClaimReversalDTO = new ReqClaimReversalDTO();
		reqClaimReversalDTO.setBody(body);
		reqClaimReversalDTO.setRequestHead(requestHead);
		requestJson = JSONObject.toJSONString(reqClaimReversalDTO);
		System.out.println(requestJson);
	}

	@Test
	public void testClaimRever() {
		claimReversalWebService.dealMainBiz(requestJson);
	}
}
