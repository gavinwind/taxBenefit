package com.sinosoft.taxbenefit.entry.customer.biz.service;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqSelCustomerInfoDTO;

public interface SelCustomerInfoBizService {
    /**
     * 客户查询
     * @param paramHeadDTO
     * @return
     */
	ThirdSendResponseDTO queryCustomerInfo(ReqSelCustomerInfoDTO reqSelCustomerInfoDTO,ParamHeadDTO paramHeadDTO);
}
