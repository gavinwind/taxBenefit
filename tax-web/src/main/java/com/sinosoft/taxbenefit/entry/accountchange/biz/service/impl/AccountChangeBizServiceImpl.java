package com.sinosoft.taxbenefit.entry.accountchange.biz.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.taxbenefit.api.dto.request.accountchange.ReqAccountInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.inter.AccountChangeApiService;
import com.sinosoft.taxbenefit.entry.accountchange.biz.service.AccountChangeBizService;
/**
 * title：账户变更信息数据服务
 * @author zhangyu
 * @date 2016年3月6日 下午7:53:04
 */
@Service
public class AccountChangeBizServiceImpl extends TaxBaseService implements AccountChangeBizService{
    @Autowired
	AccountChangeApiService accountChangeApiService;
	
	@Override
	public ThirdSendResponseDTO AccountChange(
			ReqAccountInfoDTO reqAccountInfoDTO, ParamHeadDTO paramHeadDTO) {
		return accountChangeApiService.AccountChange(reqAccountInfoDTO,paramHeadDTO);
	}

	@Override
	public ThirdSendResponseDTO AccountMoreChange(
			List<ReqAccountInfoDTO> reqAccountInfoDTO, ParamHeadDTO paramHeadDTO) {
		return accountChangeApiService.AccountListChange(reqAccountInfoDTO, paramHeadDTO);
	}
}