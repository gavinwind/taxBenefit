package com.sinosoft.taxbenefit.entry.policytransfer.webservice.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqMInPolicyTransCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqMInPolicyTransferDTO;
import com.sinosoft.taxbenefit.entry.common.base.TaxBaseRequestWebService;
import com.sinosoft.taxbenefit.entry.policytransfer.biz.service.PolicyTransBizService;
import com.sinosoft.taxbenefit.entry.policytransfer.webservice.PolicyMInTransferWebService;

/**
 * 保单多笔转入申请
 * @author SheChunMing
 */
@Service(value="policyMInTransferWebServiceImpl")
public class PolicyMInTransferWebServiceImpl extends TaxBaseRequestWebService implements PolicyMInTransferWebService{

	@Autowired
	PolicyTransBizService policyTransBizService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Class getJsonParentClass() {
		return ReqMInPolicyTransCoreDTO.class;
	}

	@Override
	protected ThirdSendResponseDTO callBizService(Object obj, ParamHeadDTO paramHead) {
		ReqMInPolicyTransCoreDTO requestDTO = (ReqMInPolicyTransCoreDTO)obj;
		ReqMInPolicyTransferDTO transInfo = (ReqMInPolicyTransferDTO) requestDTO.getBody();
		if(Integer.parseInt(paramHead.getRecordNum()) == transInfo.getTransferOutList().size()){
			return policyTransBizService.policyTransMInSettlement(transInfo, paramHead);
		}else{
			throw new BusinessDataErrException("请求记录数与实际数据记录不符");
		}
	}
}
