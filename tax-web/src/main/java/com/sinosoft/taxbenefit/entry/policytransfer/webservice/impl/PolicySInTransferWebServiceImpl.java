package com.sinosoft.taxbenefit.entry.policytransfer.webservice.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqSInPolicyTransCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqSInPolicyTransferDTO;
import com.sinosoft.taxbenefit.entry.common.base.TaxBaseRequestWebService;
import com.sinosoft.taxbenefit.entry.policytransfer.biz.service.PolicyTransBizService;
import com.sinosoft.taxbenefit.entry.policytransfer.webservice.PolicySInTransferWebService;

/**
 * 保单单笔转入申请
 * @author SheChunMing
 */
@Service(value="policySInTransferWebServiceImpl")
public class PolicySInTransferWebServiceImpl extends TaxBaseRequestWebService implements PolicySInTransferWebService{
	
	@Autowired
	PolicyTransBizService policyTransBizService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Class getJsonParentClass() {
		return ReqSInPolicyTransCoreDTO.class;
	}

	@Override
	protected ThirdSendResponseDTO callBizService(Object obj, ParamHeadDTO paramHead) {
		ReqSInPolicyTransCoreDTO requestDTO = (ReqSInPolicyTransCoreDTO)obj;
		ReqSInPolicyTransferDTO transInfo = (ReqSInPolicyTransferDTO) requestDTO.getBody();
		if(1 == Integer.parseInt(paramHead.getRecordNum())){
			return policyTransBizService.policyTransSInSettlement(transInfo, paramHead);
		}else{
			throw new BusinessDataErrException("请求记录数与实际数据记录不符");
		}
	}
}
