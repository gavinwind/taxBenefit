package com.sinosoft.taxbenefit.entry.policytransfer.webservice;

import javax.jws.WebService;

/**
 * 保单单笔转入申请
 * @author SheChunMing
 */
@WebService
public interface PolicySInTransferWebService {
	/**
	 * 保单单笔转入申请
	 * @param requestJson
	 * @return
	 */
	public String dealMainBiz(String requestJson);
}
