package com.sinosoft.taxbenefit.entry.securityguard.biz.service;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.securityguard.ReqKeepBodyDTO;

public interface PreservationChangeService {
		public ThirdSendResponseDTO DataPersistence(ReqKeepBodyDTO obj,ParamHeadDTO paramHeadDTO);
}
