package com.sinosoft.taxbenefit.entry.business.config;

/**
 * 报文请求返回结果
 * 
 * @author zhangke
 *
 */
public enum ENUM_STATEMENT {
	REQUEST_SUCCESS("01", "请求成功"), VERIFY_FAILED("02", "请求失败"), DEAL_ING("03",
			"处理中"),MESSAGE_ERROR("04","报文格式错误");
	private final String code;
	private final String desc;

	ENUM_STATEMENT(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public String code() {
		return code;
	}

	public String desc() {
		return desc;
	}

}
