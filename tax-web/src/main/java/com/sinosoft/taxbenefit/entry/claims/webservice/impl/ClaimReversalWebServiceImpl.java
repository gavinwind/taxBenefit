package com.sinosoft.taxbenefit.entry.claims.webservice.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimReversalDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimReversalInfoDTO;
import com.sinosoft.taxbenefit.entry.claims.biz.service.ClaimBizService;
import com.sinosoft.taxbenefit.entry.claims.webservice.ClaimReversalWebService;
import com.sinosoft.taxbenefit.entry.common.base.TaxBaseRequestWebService;
@Service(value="claimReversalWebServiceImpl")
public class ClaimReversalWebServiceImpl extends TaxBaseRequestWebService implements ClaimReversalWebService{
	@Autowired
	private ClaimBizService claimBizService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Class getJsonParentClass() {
		return ReqClaimReversalDTO.class;
	}

	@Override
	protected ThirdSendResponseDTO callBizService(Object obj,ParamHeadDTO paramHeadDTO) {
		ReqClaimReversalDTO requestDTO = (ReqClaimReversalDTO) obj;
		ReqClaimReversalInfoDTO reqClaimReversalDTO=(ReqClaimReversalInfoDTO)requestDTO.getBody();
		if(Integer.parseInt(paramHeadDTO.getRecordNum()) == 1){
			return claimBizService.modifyClaimState(reqClaimReversalDTO, paramHeadDTO);
		}else{
			throw new BusinessDataErrException("请求记录数与实际数据记录不符");
		}
	}
}
