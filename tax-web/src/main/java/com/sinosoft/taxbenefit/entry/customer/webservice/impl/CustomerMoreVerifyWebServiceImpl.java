package com.sinosoft.taxbenefit.entry.customer.webservice.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqCustomerInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqCustomerMoreCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqCustomerMoreDTO;
import com.sinosoft.taxbenefit.entry.common.base.TaxBaseRequestWebService;
import com.sinosoft.taxbenefit.entry.customer.biz.service.CustomerVerifyBizService;
import com.sinosoft.taxbenefit.entry.customer.webservice.CustomerMoreVerifyWebService;
/**
 * 客戶验证上传服务(多笔)
 * @author zhangyu
 * @date 2016年3月6日 下午5:59:55
 */
@Service(value="customerMoreVerifyWebServiceImpl")
public class CustomerMoreVerifyWebServiceImpl extends TaxBaseRequestWebService implements CustomerMoreVerifyWebService{
	@Autowired
	CustomerVerifyBizService customerVerifyBizService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Class getJsonParentClass() {
		return ReqCustomerMoreCoreDTO.class;
	}

	@Override
	protected ThirdSendResponseDTO callBizService(Object obj,
			ParamHeadDTO paramHeadDTO) {
		ReqCustomerMoreCoreDTO reqCustomerMoreCoreDTO=(ReqCustomerMoreCoreDTO)obj;
		ReqCustomerMoreDTO reqCustomerMoreDTO=(ReqCustomerMoreDTO)reqCustomerMoreCoreDTO.getBody();
		List<ReqCustomerInfoDTO> clientList=(List<ReqCustomerInfoDTO>)reqCustomerMoreDTO.getClientList();
		return customerVerifyBizService.CustomerMoreVerify(clientList, paramHeadDTO);
	}
}