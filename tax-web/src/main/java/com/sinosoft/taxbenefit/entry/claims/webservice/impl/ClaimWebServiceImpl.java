package com.sinosoft.taxbenefit.entry.claims.webservice.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimInfoDTO;
import com.sinosoft.taxbenefit.entry.claims.biz.service.ClaimBizService;
import com.sinosoft.taxbenefit.entry.claims.webservice.ClaimWebService;
import com.sinosoft.taxbenefit.entry.common.base.TaxBaseRequestWebService;


/**
 * 理赔上传(单个)WebService实现类
 * @author zhangke
 *
 */
@Service(value="claimWebServiceImpl")
public class ClaimWebServiceImpl extends TaxBaseRequestWebService implements ClaimWebService{
	@Autowired
	private ClaimBizService claimBizService;

	@SuppressWarnings("rawtypes")
	@Override
	protected Class getJsonParentClass() {
		return ReqClaimCoreDTO.class;
	}
	
	@Override
	protected ThirdSendResponseDTO callBizService(Object obj,ParamHeadDTO parmDto) {
		ReqClaimCoreDTO requestDTO = (ReqClaimCoreDTO) obj;
		ReqClaimInfoDTO claimInfo = (ReqClaimInfoDTO) requestDTO.getBody();
		if(1==Integer.parseInt(parmDto.getRecordNum())){
			return claimBizService.addClaimSettlement(claimInfo, parmDto);
		}else{
			throw new BusinessDataErrException("请求记录数与实际数据记录不符");
		}
	}
}
