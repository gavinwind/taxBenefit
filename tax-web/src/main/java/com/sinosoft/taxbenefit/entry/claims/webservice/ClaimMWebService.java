package com.sinosoft.taxbenefit.entry.claims.webservice;

import javax.jws.WebService;
/**
 * 理赔上传(多个) WebService
 * @author zhangke
 *
 */
@WebService
public interface ClaimMWebService {
	public String dealMainBiz(String requestJson);
}
