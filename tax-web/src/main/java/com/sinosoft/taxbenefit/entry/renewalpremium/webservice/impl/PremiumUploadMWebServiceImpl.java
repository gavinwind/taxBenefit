package com.sinosoft.taxbenefit.entry.renewalpremium.webservice.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqMPremiumInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqMRenewalPremiumCoreDTO;
import com.sinosoft.taxbenefit.entry.common.base.TaxBaseRequestWebService;
import com.sinosoft.taxbenefit.entry.renewalpremium.biz.service.RenewalPremiumBizService;
import com.sinosoft.taxbenefit.entry.renewalpremium.webservice.PremiumUploadMWebService;

@Service(value="premiumUploadMWebServiceImpl")
public class PremiumUploadMWebServiceImpl extends TaxBaseRequestWebService implements PremiumUploadMWebService{
	@Autowired
	RenewalPremiumBizService renewalPremiumBizService;

	@SuppressWarnings("rawtypes")
	@Override
	protected Class getJsonParentClass() {
		return ReqMRenewalPremiumCoreDTO.class;
	}

	@Override
	protected ThirdSendResponseDTO callBizService(Object obj,ParamHeadDTO paramHeadDTO) {
		ReqMRenewalPremiumCoreDTO requestDTO = (ReqMRenewalPremiumCoreDTO) obj;
		ReqMPremiumInfoDTO premiumInfo = (ReqMPremiumInfoDTO)requestDTO.getBody();
		if(Integer.parseInt(paramHeadDTO.getRecordNum()) == premiumInfo.getPremiumList().size()){
			return renewalPremiumBizService.RenewalMPremium(premiumInfo, paramHeadDTO);
		}else{
			throw new BusinessDataErrException("请求记录数与实际数据记录不符");
		}
	}

}
