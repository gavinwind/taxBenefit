package com.sinosoft.taxbenefit.entry.bargaincheckquery.webservice.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.taxbenefit.api.dto.request.bargaincheckquery.ReqBargainCheckQueryCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.bargaincheckquery.ReqBargainCheckQueryDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.entry.bargaincheckquery.biz.service.BargainCheckQueryBizSercvice;
import com.sinosoft.taxbenefit.entry.bargaincheckquery.webservice.BargainCheckQueryWebService;
import com.sinosoft.taxbenefit.entry.common.base.TaxBaseRequestWebService;
@Service(value="bargainCheckQueryWebServiceImpl")
public class BargainCheckQueryWebServiceImpl extends TaxBaseRequestWebService implements BargainCheckQueryWebService{
	@Autowired
	BargainCheckQueryBizSercvice bargainCheckQueryBizSercvice;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Class getJsonParentClass() {
		return ReqBargainCheckQueryCoreDTO.class;
	}

	@Override
	protected ThirdSendResponseDTO callBizService(Object obj,ParamHeadDTO paramHeadDTO) {
		ReqBargainCheckQueryCoreDTO reqBargainCheckQueryCoreDTO = (ReqBargainCheckQueryCoreDTO)obj;
		ReqBargainCheckQueryDTO reqBargainCheckQueryDTO = (ReqBargainCheckQueryDTO)reqBargainCheckQueryCoreDTO.getBody();
		return bargainCheckQueryBizSercvice.bargainCheckQuery(reqBargainCheckQueryDTO, paramHeadDTO);
	}

}
