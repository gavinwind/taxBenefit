/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.ewealth.common.utils<br/>
 * @FileName: MoneyFormatUtil.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.taxbenefit.entry.common.utils;

import java.math.BigDecimal;

import com.sinosoft.taxbenefit.entry.business.dto.MoneyShowDTO;
/**  
 * 【金额格式化工具类】
 * @Description: 金额格式化工具类
 * @author chenxin@sinosoft.com.cn
 * @date 2015年9月16日 下午4:59:27 
 * @version V1.0  
 */
public class MoneyFormatUtil {
	/**
	 *  将阿拉伯数字转换成中式金额数字
	 * @Title: transToALBAndChinese 
	 * @Description: 将阿拉伯数字转换成中式金额数字
	 * @param number
	 * @return
	 */
	public static MoneyShowDTO transToALBAndChinese(BigDecimal number){
		MoneyShowDTO showDTO = new MoneyShowDTO();
		BigDecimal temp1= number.divide(new BigDecimal(10000),2,BigDecimal.ROUND_DOWN);
		String temp1Str = temp1.toString();
		//System.out.println("temp1Str:"+temp1Str);
		if(temp1Str.endsWith(".00")){
			//String[] str = temp1Str.split("\\.00");
			temp1Str = temp1Str.split("\\.00")[0];
		}
		BigDecimal temp2= number.divide(new BigDecimal(100000000),2,BigDecimal.ROUND_DOWN);
		String temp2Str = temp2.toString();
		if(temp2Str.endsWith(".00")){
			temp2Str = temp2Str.split("\\.00")[0];
		}
		if(temp2.compareTo(new BigDecimal(1))>=0){
			showDTO.setAmount(temp2Str);
			showDTO.setAmountUnit("亿元");
		}
		else if(temp1.compareTo(new BigDecimal(1))>=0){
			showDTO.setAmount(temp1Str);
			showDTO.setAmountUnit("万元");
		}else{
			String tempAmount = number.setScale(2, BigDecimal.ROUND_HALF_EVEN).toString();
			if(tempAmount.endsWith(".00")){
				tempAmount = tempAmount.split("\\.00")[0];
			}
			showDTO.setAmount(tempAmount);
			showDTO.setAmountUnit("元");
			
		}
		return showDTO;
	}
	
	public static void main(String[] args){
		String a = "0.00";
		if(a.endsWith("//.00")){
			System.out.println(1111);
		}
		System.out.println(transToALBAndChinese(new BigDecimal("10000000000")).getAmount() + transToALBAndChinese(new BigDecimal("10000000000")).getAmountUnit());
	}
}


