package com.sinosoft.taxbenefit.entry.customer.biz.service;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqInsuredPayInfoDTO;

public interface InsuredPayBizService {
    /**
     * 既往理赔额查询
     * @param paramHeadDTO
     * @return
     */
	ThirdSendResponseDTO InsuredPay(ReqInsuredPayInfoDTO reqInsuredPayInfoDTO,ParamHeadDTO paramHeadDTO);
}
