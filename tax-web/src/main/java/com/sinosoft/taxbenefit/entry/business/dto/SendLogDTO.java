package com.sinosoft.taxbenefit.entry.business.dto;

/**
 * 【报文日志DTO】
 * 
 * @Description: 发送报文日志DTO
 * @author shechunming@sinosoft.com.cn
 * @date 2016年2月17日 下午10:50:59
 * @version V1.0
 */
public class SendLogDTO {

	/** 交易流水号 */
	private String serialNo;
	/** 交易批次号 */
	private String batchBizNo;

	/** 发送服务方 */
	private String targetCode;
	/** 发送服务接口编码 */
	private String portCode;
	/** 发送报文 */
	private String requestContent;
	/** 相应报文 */
	private String responseContent;
	/** 状态 */
	private String state;
	/** 备注信息 */
	private String description;

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getTargetCode() {
		return targetCode;
	}

	public void setTargetCode(String targetCode) {
		this.targetCode = targetCode;
	}

	public String getPortCode() {
		return portCode;
	}

	public void setPortCode(String portCode) {
		this.portCode = portCode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBatchBizNo() {
		return batchBizNo;
	}

	public void setBatchBizNo(String batchBizNo) {
		this.batchBizNo = batchBizNo;
	}

	public String getRequestContent() {
		return requestContent;
	}

	public void setRequestContent(String requestContent) {
		this.requestContent = requestContent;
	}

	public String getResponseContent() {
		return responseContent;
	}

	public void setResponseContent(String responseContent) {
		this.responseContent = responseContent;
	}
}
