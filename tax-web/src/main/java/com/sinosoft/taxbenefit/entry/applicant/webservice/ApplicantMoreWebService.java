package com.sinosoft.taxbenefit.entry.applicant.webservice;

import javax.jws.WebService;

/**
 * 承保信息上传ApplicantWebService(多笔)
 * 
 * @author zhangke
 *
 */
@WebService
public interface ApplicantMoreWebService {
	public String dealMainBiz(String requestJson);
}
