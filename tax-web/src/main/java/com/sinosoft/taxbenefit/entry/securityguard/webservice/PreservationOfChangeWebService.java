package com.sinosoft.taxbenefit.entry.securityguard.webservice;

import javax.jws.WebService;
/**
 * 保全变更信息上传
 * @author SheChunMing
 */
@WebService
public interface PreservationOfChangeWebService 
{
	/**
	 * 保全变更
	 * @param requestJson
	 * @return
	 */
	public String dealMainBiz(String requestJson);
}