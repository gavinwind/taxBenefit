package com.sinosoft.taxbenefit.entry.customer.webservice.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqTaxBeneVerifyCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqTaxBeneVerifyInfoDTO;
import com.sinosoft.taxbenefit.entry.common.base.TaxBaseRequestWebService;
import com.sinosoft.taxbenefit.entry.customer.biz.service.TaxBenefitBizService;
import com.sinosoft.taxbenefit.entry.customer.webservice.TaxBeneVerifyWebService;
/**
 * 税优验证
 * @author zhangyu
 * @date 2016年3月10日 上午9:52:34
 */
@Service(value="taxBeneVerifyWebServiceImpl")
public class TaxBeneVerifyWebServiceImpl extends TaxBaseRequestWebService implements TaxBeneVerifyWebService{
    @Autowired
    TaxBenefitBizService taxBenefitBizService;
    
    @SuppressWarnings("rawtypes")
	@Override
	protected Class getJsonParentClass() {
		return ReqTaxBeneVerifyCoreDTO.class;
	}
	@Override
	protected ThirdSendResponseDTO callBizService(Object obj,
			ParamHeadDTO paramHeadDTO) {
		ReqTaxBeneVerifyCoreDTO reqTaxBeneVerifyCore=(ReqTaxBeneVerifyCoreDTO)obj;
		ReqTaxBeneVerifyInfoDTO TaxBeneVerifyInfoDTO=(ReqTaxBeneVerifyInfoDTO)reqTaxBeneVerifyCore.getBody();
		return taxBenefitBizService.taxBenefit(TaxBeneVerifyInfoDTO,paramHeadDTO);
	}
}