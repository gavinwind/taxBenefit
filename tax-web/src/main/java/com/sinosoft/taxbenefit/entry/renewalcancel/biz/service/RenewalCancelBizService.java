package com.sinosoft.taxbenefit.entry.renewalcancel.biz.service;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalcancel.RequestRenewalCancelDTO;

public interface RenewalCancelBizService {
	/**
	 * 续保撤销
	 * @param requestRenewalCancelDTO
	 * @param paramHead
	 * @return
	 */
	ThirdSendResponseDTO renewalCancel(RequestRenewalCancelDTO requestRenewalCancelDTO,ParamHeadDTO paramHead);
}