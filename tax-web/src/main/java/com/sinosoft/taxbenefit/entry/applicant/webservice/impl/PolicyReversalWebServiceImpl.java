package com.sinosoft.taxbenefit.entry.applicant.webservice.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.taxbenefit.api.dto.request.applicant.ReqPolicyCancelDTO;
import com.sinosoft.taxbenefit.api.dto.request.applicant.ReqPolicyReversalDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.entry.applicant.biz.service.ApplicantBizService;
import com.sinosoft.taxbenefit.entry.applicant.webservice.PolicyReversalWebService;
import com.sinosoft.taxbenefit.entry.common.base.TaxBaseRequestWebService;
				
@Service(value = "policyReversalWebServiceImpl")
public class PolicyReversalWebServiceImpl extends TaxBaseRequestWebService implements PolicyReversalWebService{
	@Autowired
    private	ApplicantBizService applicantBizService;
	@SuppressWarnings("rawtypes")
	@Override
	protected Class getJsonParentClass() {
		return ReqPolicyReversalDTO.class;
	}

	@Override
	protected ThirdSendResponseDTO callBizService(Object obj,
			ParamHeadDTO paramHeadDTO) {
		ReqPolicyReversalDTO reqPolicyReversalDTO=(ReqPolicyReversalDTO)obj;
		ReqPolicyCancelDTO reqPolicyCancelDTO=(ReqPolicyCancelDTO)reqPolicyReversalDTO.getBody();
		if(1==Integer.parseInt(paramHeadDTO.getRecordNum())){
			return applicantBizService.modifyPolicyState(reqPolicyCancelDTO, paramHeadDTO);
		}else{
			throw new BusinessDataErrException("请求记录数与实际数据记录不符");
		}
	}
}
