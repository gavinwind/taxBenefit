package com.sinosoft.taxbenefit.entry.business.config;
/**  
 * 【类或接口功能描述】
 * @Description: TODO
 * @author wuyu@sinosoft.com.cn
 * @date 2015年10月10日 上午9:27:14 
 * @version V1.0  
 */
public enum ENUM_REPORTLOG_DEALTYPE {
	
    SYNC_DEAL("01","同步处理"),ASYNC_DEAL("02","异步处理");
    
    private final String code;
    private final String desc;
    
    ENUM_REPORTLOG_DEALTYPE(String code,String desc){
    	this.code = code;
    	this.desc = desc;
    }
    
    public String code(){
    	return code;
    }
    public String desc(){
    	return desc;
    }
}
