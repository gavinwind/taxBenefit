package com.sinosoft.taxbenefit.entry.applicant.webservice;

import javax.jws.WebService;

/**
 * 承保撤销
 * @author zhangke
 *
 */
@WebService
public interface PolicyReversalWebService {
	public String dealMainBiz(String requestJson);
}
