package com.sinosoft.taxbenefit.entry.renewalpremium.biz.service;


import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqMPremiumInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqSPremiumInfoDTO;

public interface RenewalPremiumBizService {
	/**
	 * 续期保费信息上传
	 * @param reqRenewalPremiumInfoDTOs
	 * @param paramHead
	 * @return
	 */
	ThirdSendResponseDTO RenewalPremium(ReqSPremiumInfoDTO reqSPremiumInfoDTO,ParamHeadDTO paramHead);
	/**
	 * 续期保费信息上传(多个)
	 * @param reqMPremiumInfoDTO
	 * @param paramHead
	 * @return
	 */
	ThirdSendResponseDTO RenewalMPremium(ReqMPremiumInfoDTO reqMPremiumInfoDTO,ParamHeadDTO paramHead);
}