package com.sinosoft.taxbenefit.entry.accountchange.biz.service;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.accountchange.ReqAccountInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;

public interface AccountChangeBizService {
	/**
	 * 账户变更接口(单笔)
	 * @param reqAccountInfoDTO
	 */
	ThirdSendResponseDTO AccountChange(ReqAccountInfoDTO reqAccountInfoDTO,
			ParamHeadDTO paramHeadDTO);
	/**
	 * 账户变更接口(多笔)
	 * @param reqAccountInfoDTO
	 * @param paramHeadDTO
	 * @return
	 */
	ThirdSendResponseDTO AccountMoreChange(List<ReqAccountInfoDTO> reqAccountInfoDTO,
			ParamHeadDTO paramHeadDTO);
}