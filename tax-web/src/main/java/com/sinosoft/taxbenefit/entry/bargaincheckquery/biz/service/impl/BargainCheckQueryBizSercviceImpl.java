package com.sinosoft.taxbenefit.entry.bargaincheckquery.biz.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.taxbenefit.api.dto.request.bargaincheckquery.ReqBargainCheckQueryDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.inter.BargainCheckQueryApiService;
import com.sinosoft.taxbenefit.entry.bargaincheckquery.biz.service.BargainCheckQueryBizSercvice;
@Service
public class BargainCheckQueryBizSercviceImpl implements BargainCheckQueryBizSercvice{
	@Autowired
	BargainCheckQueryApiService bargainCheckQueryApiService;
	@Override
	public ThirdSendResponseDTO bargainCheckQuery(ReqBargainCheckQueryDTO reqBargainCheckQueryDTO,ParamHeadDTO paramHead) {
		return bargainCheckQueryApiService.bargainCheck(reqBargainCheckQueryDTO, paramHead);
	}

}
