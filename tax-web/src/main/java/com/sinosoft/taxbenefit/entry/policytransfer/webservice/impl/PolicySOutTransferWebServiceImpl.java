package com.sinosoft.taxbenefit.entry.policytransfer.webservice.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqSOutPolicyTransCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqSOutPolicyTransferDTO;
import com.sinosoft.taxbenefit.entry.common.base.TaxBaseRequestWebService;
import com.sinosoft.taxbenefit.entry.policytransfer.biz.service.PolicyTransBizService;
import com.sinosoft.taxbenefit.entry.policytransfer.webservice.PolicySOutTransferWebService;

/**
 * 保单单笔转出登记请求
 * @author SheChunMing
 */
@Service(value="policySOutTransferWebServiceImpl")
public class PolicySOutTransferWebServiceImpl extends TaxBaseRequestWebService implements PolicySOutTransferWebService{

	@Autowired
	PolicyTransBizService policyTransBizService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Class getJsonParentClass() {
		return ReqSOutPolicyTransCoreDTO.class;
	}

	@Override
	protected ThirdSendResponseDTO callBizService(Object obj, ParamHeadDTO paramHead) {
		ReqSOutPolicyTransCoreDTO requestDTO = (ReqSOutPolicyTransCoreDTO)obj;
		ReqSOutPolicyTransferDTO transInfo = (ReqSOutPolicyTransferDTO) requestDTO.getBody();
		if(1 == Integer.parseInt(paramHead.getRecordNum())){
			return policyTransBizService.policyTransSOutSettlement(transInfo, paramHead);
		}else{
			throw new BusinessDataErrException("请求记录数与实际数据记录不符");
		}
	}
}
