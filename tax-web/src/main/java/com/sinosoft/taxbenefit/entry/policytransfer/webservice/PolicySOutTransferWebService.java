package com.sinosoft.taxbenefit.entry.policytransfer.webservice;

import javax.jws.WebService;

/**
 * 保单单笔转出登记请求
 * @author SheChunMing
 *
 */
@WebService
public interface PolicySOutTransferWebService {
	/**
	 * 保单单笔转出登记请求
	 * @param requestJson
	 * @return
	 */
	public String dealMainBiz(String requestJson);
}
