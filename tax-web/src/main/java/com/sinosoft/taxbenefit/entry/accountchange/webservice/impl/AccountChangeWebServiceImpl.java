package com.sinosoft.taxbenefit.entry.accountchange.webservice.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.taxbenefit.api.dto.request.accountchange.ReqAccountCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.accountchange.ReqAccountInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.entry.accountchange.biz.service.AccountChangeBizService;
import com.sinosoft.taxbenefit.entry.accountchange.webservice.AccountChangeWebService;
import com.sinosoft.taxbenefit.entry.common.base.TaxBaseRequestWebService;
/**
 *账户信息变更上传请求
 * @author zhangyu
 * @date 2016年3月6日 下午6:01:30
 */
@Service(value = "accountChangeWebServiceImpl")
public class AccountChangeWebServiceImpl extends TaxBaseRequestWebService implements AccountChangeWebService{
	@Autowired
	AccountChangeBizService accountChangeBizService;
	@SuppressWarnings("rawtypes")
	@Override
	protected Class getJsonParentClass() {
		return ReqAccountCoreDTO.class;
	}
	@Override
	protected ThirdSendResponseDTO callBizService(Object obj, ParamHeadDTO paramHeadDTO) {
		ReqAccountCoreDTO reqAccountCoreDTO = (ReqAccountCoreDTO) obj;
		ReqAccountInfoDTO reqAccountInfoDTO = (ReqAccountInfoDTO) reqAccountCoreDTO.getBody();
		if(1 == Integer.parseInt(paramHeadDTO.getRecordNum())){
			return accountChangeBizService.AccountChange(reqAccountInfoDTO,paramHeadDTO);
		}else{
			throw new BusinessDataErrException("请求记录数与实际数据记录不符");
		}
	}
}