package com.sinosoft.taxbenefit.entry.policytransfer.webservice;

import javax.jws.WebService;

/**
 * 保单转出登记信息查询
 * @author SheChunMing
 */
@WebService
public interface PolicyTransOutRegisterQueryWebService {

	/**
	 * 保单转出登记信息查询
	 * @param requestJson
	 * @return
	 */
	public String dealMainBiz(String requestJson);
}
