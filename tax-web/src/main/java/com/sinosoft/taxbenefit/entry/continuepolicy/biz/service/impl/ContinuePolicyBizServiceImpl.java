package com.sinosoft.taxbenefit.entry.continuepolicy.biz.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqMContinuePolicyInfoDTO;
import com.sinosoft.taxbenefit.api.inter.ContinuePolicyInfoApiService;
import com.sinosoft.taxbenefit.entry.continuepolicy.biz.service.ContinuePolicyBizService;
@Service
public class ContinuePolicyBizServiceImpl implements ContinuePolicyBizService {
	@Autowired
	ContinuePolicyInfoApiService continuePolicyInfoApiService;

	@Override
	public ThirdSendResponseDTO continuePolicySettlement(
			ReqContinuePolicyInfoDTO continuePolicyInfo, ParamHeadDTO paramHead) {
		return continuePolicyInfoApiService.continuePolicySettlement(continuePolicyInfo, paramHead);
	}

	@Override
	public ThirdSendResponseDTO continueMPolicySettlement(
			ReqMContinuePolicyInfoDTO policyInfoList, ParamHeadDTO paramHead) {
		return continuePolicyInfoApiService.continueMPolicySettlement(policyInfoList, paramHead);
	}


}