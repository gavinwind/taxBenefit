package com.sinosoft.taxbenefit.entry.applicant.webservice.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.taxbenefit.api.dto.request.applicant.RepAppAcceptOneBody;
import com.sinosoft.taxbenefit.api.dto.request.applicant.ReqAppPolicyDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.entry.applicant.biz.service.ApplicantBizService;
import com.sinosoft.taxbenefit.entry.applicant.webservice.ApplicantOneWebService;
import com.sinosoft.taxbenefit.entry.common.base.TaxBaseRequestWebService;

@Service(value = "applicantOneWebServiceImpl")
public class ApplicantOneWebServiceImpl extends TaxBaseRequestWebService
		implements ApplicantOneWebService {
	@Autowired
	ApplicantBizService applicantBizService;

	@SuppressWarnings("rawtypes")
	@Override
	protected Class getJsonParentClass() {
		return RepAppAcceptOneBody.class;
	}

	@Override
	protected ThirdSendResponseDTO callBizService(Object obj,
			ParamHeadDTO paramHeadDTO) {
		RepAppAcceptOneBody body = (RepAppAcceptOneBody) obj;
		ReqAppPolicyDTO reqAppAcceptMore = (ReqAppPolicyDTO) body.getBody();
		if(1==Integer.parseInt(paramHeadDTO.getRecordNum())){
			List<ReqAppPolicyDTO> reqAppAcceptMores = new ArrayList<ReqAppPolicyDTO>();
			reqAppAcceptMores.add(reqAppAcceptMore);
			return applicantBizService.addApplicant(reqAppAcceptMores, paramHeadDTO);
		}
		throw new BusinessDataErrException("请求记录数与实际数据记录不符");
	}
}
