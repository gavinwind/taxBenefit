package com.sinosoft.taxbenefit.entry.business.dto;


/**
 * 异常报文DTO
 * @author zhangke
 */
public class ErrorViewLogDTO {
	/** 业务号 */
	private String serialNo;
	/** 接口编码 */
	private String interCode;
	/** 错误信息 */
	private String errorInfo;
	/** 错误类型 */
	private String erroType;
	/** 发生时间 */
	private String createDate;
	/** 更新时间 */
	private String modifyDate;

	public String getInterCode() {
		return interCode;
	}

	public void setInterCode(String interCode) {
		this.interCode = interCode;
	}

	public String getErrorInfo() {
		return errorInfo;
	}

	public void setErrorInfo(String errorInfo) {
		this.errorInfo = errorInfo;
	}

	public String getErroType() {
		return erroType;
	}

	public void setErroType(String erroType) {
		this.erroType = erroType;
	}
	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(String modifyDate) {
		this.modifyDate = modifyDate;
	}

}
