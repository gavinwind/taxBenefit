package com.sinosoft.taxbenefit.entry.accountchange.webservice;

import javax.jws.WebService;

@WebService
public interface AccountChangeWebService {
	/**
	 * 账户变更上传(单笔)
	 * @param requestJson
	 * @return
	 */
	public String dealMainBiz(String requestJson);
}