package com.sinosoft.taxbenefit.entry.securityguard.webservice.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.securityguard.ReqKeepBodyDTO;
import com.sinosoft.taxbenefit.api.dto.request.securityguard.ReqkeepEndorsementDTO;
import com.sinosoft.taxbenefit.entry.common.base.TaxBaseRequestWebService;
import com.sinosoft.taxbenefit.entry.securityguard.biz.service.PreservationChangeService;
import com.sinosoft.taxbenefit.entry.securityguard.webservice.PreservationOfChangeWebService;

@Service(value = "preservationOfChangeWebServiceImpl")
public class PreservationOfChangeWebServiceImpl extends
		TaxBaseRequestWebService implements PreservationOfChangeWebService {
	@Autowired
	private PreservationChangeService Pchange;

	@Override
	protected ThirdSendResponseDTO callBizService(Object obj,
			ParamHeadDTO paramHeadDTO) {
		// 获取报文体
		ReqkeepEndorsementDTO jsonTax = (ReqkeepEndorsementDTO) obj;
		ReqKeepBodyDTO bodyDto = (ReqKeepBodyDTO) jsonTax.getBody();

		return Pchange.DataPersistence(bodyDto, paramHeadDTO);
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected Class getJsonParentClass() {
		return ReqkeepEndorsementDTO.class;
	}
}
