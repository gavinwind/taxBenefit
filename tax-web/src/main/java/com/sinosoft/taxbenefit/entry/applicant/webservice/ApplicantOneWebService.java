package com.sinosoft.taxbenefit.entry.applicant.webservice;

import javax.jws.WebService;

/**
 * 承保信息上传ApplicantWebService(单笔)
 * 
 * @author zhangke
 *
 */
@WebService
public interface ApplicantOneWebService {
	public String dealMainBiz(String requestJson);
}
