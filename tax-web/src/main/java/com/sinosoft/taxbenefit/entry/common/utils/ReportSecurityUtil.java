/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.ewealth.common.biz.service.utils<br/>
 * @FileName: ReportSecurityUtil.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.taxbenefit.entry.common.utils;

import com.sinosoft.platform.common.util.SecurityUtil;

/**
 * 【报文加密工具类】
 * @Description: TODO
 * @author chenxin@sinosoft.com.cn
 * @date 2015年9月7日 下午11:01:30
 * @version V1.0
 */
public class ReportSecurityUtil {
	private static String hexinKey = "tax";
	private static String ebaoKey = "tax";

	/**
	 * 生成与核心保密的加密密钥
	 * @Title: encryptReport
	 * @Description: 生成保密的加密密钥
	 * @param reportXml
	 * @return
	 */
	public static String encryptHXReportByBody(String toDealStr) {
		return SecurityUtil.generateMD5(hexinKey + toDealStr);
	}
	/**
	 * 生成中保信保密的加密密钥
	 * @Title: encryptReport
	 * @Description: 生成保密的加密密钥
	 * @param reportXml
	 * @return
	 */
	public static String encryptEBReportByBody(String toDealStr) {
		return SecurityUtil.generateMD5(ebaoKey + toDealStr);
	}

	/**
	 * 校验报文是否合法
	 * @Title: checkEncryReport
	 * @Description: 校验报文是否合法
	 * @param reportXml
	 *            交易报文
	 * @param signMsg
	 *            验证签名信息
	 * @return
	 */
	public static boolean checkEncryReport(String reportXml, String signMsg) {
		String toDealStr = generatedDealStr(reportXml, "<request>",
				"</request>");
		// System.out.println(toDealStr+ewealthKey);
		String greatSignKey = encryptHXReportByBody(toDealStr);
		if (greatSignKey == null || !greatSignKey.equals(signMsg)) {
			return false;
			// throw new SignErrException("签名验证错误，有人篡改报文信息！");
		}
		return true;
	}
	
	
	/**
	 * 校验核心交互报文是否合法
	 * @param report 交易报文
	 * @param signMsg  验证签名信息
	 * @return
	 */
	public static boolean checkHXReport(String report, String signMsg){
		String greatSignKey = encryptHXReportByBody(report);
		if (greatSignKey == null || !greatSignKey.equals(signMsg)) {
			return false;
		}
		return true;
	}
	/**
	 * 校验中报信报文是否合法
	 * @param report 交易报文
	 * @param signMsg  验证签名信息
	 * @return
	 */
	public static boolean checkEBReport(String report, String signMsg){
		String greatSignKey = encryptEBReportByBody(report);
		System.out.println(greatSignKey);
		if (greatSignKey == null || !greatSignKey.equals(signMsg)) {
			return false;
		}
		return true;
	}

	/**
	 * 获得需要处理的报文片段
	 * 
	 * @Title: generatedDealStr
	 * @Description: 获得需要处理的报文片段
	 * @param reportXml
	 * @param startSubStr
	 * @param endSubStr
	 * @return
	 */
	private static String generatedDealStr(String reportXml,
			String startSubStr, String endSubStr) {
		String toDealStr = reportXml.substring(reportXml.indexOf(startSubStr),
				reportXml.indexOf(endSubStr) + endSubStr.length());
		return toDealStr;
	}

}
