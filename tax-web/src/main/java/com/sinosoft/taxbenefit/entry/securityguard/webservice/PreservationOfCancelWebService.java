package com.sinosoft.taxbenefit.entry.securityguard.webservice;

import javax.jws.WebService;

@WebService
public interface PreservationOfCancelWebService {

	/**
	 * 保全撤销信息上传
	 * 
	 * @author SheChunMing
	 */
	public String dealMainBiz(String requestJson);

}
