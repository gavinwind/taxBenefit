/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.ewealth.general.biz.service.impl<br/>
 * @FileName: ReportLogServiceImpl.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.taxbenefit.entry.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.taxbenefit.entry.business.dto.ErrorViewLogDTO;
import com.sinosoft.taxbenefit.entry.business.dto.ReceiveLogDTO;
import com.sinosoft.taxbenefit.entry.business.service.LogService;
import com.sinosoft.taxbenefit.entry.business.thread.ErrorLogThread;
import com.sinosoft.taxbenefit.entry.business.thread.ReceiveLogThread;
import com.sinosoft.taxbenefit.entry.generated.mapper.TaxRequestReceiveLogMapper;
import com.sinosoft.taxbenefit.entry.generated.mapper.TaxSystemErrorLogMapper;

/**
 * 【报文日志服务类】
 * 
 * @Description: 报文日志服务类
 * @author chenxin@sinosoft.com.cn
 * @date 2015年9月15日 下午11:13:15
 * @version V1.0
 */
@Service
public class ReceiveLogServiceImpl extends TaxBaseService implements LogService {
	@Autowired
	private TaxRequestReceiveLogMapper receiveLogMapper;
	
	@Autowired
	private TaxSystemErrorLogMapper errorLogMapper;
	@Autowired
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;

	@Override
	public void logReport(ReceiveLogDTO reportLogDTO) {
		ReceiveLogThread reportsLogThread = new ReceiveLogThread(reportLogDTO,
				receiveLogMapper);
		threadPoolTaskExecutor.execute(reportsLogThread);
	}

	@Override
	public void logError(ErrorViewLogDTO errorViewLogDTO) {
		ErrorLogThread errorLogThread=new ErrorLogThread(errorLogMapper, errorViewLogDTO);
		threadPoolTaskExecutor.execute(errorLogThread);
	}
}
