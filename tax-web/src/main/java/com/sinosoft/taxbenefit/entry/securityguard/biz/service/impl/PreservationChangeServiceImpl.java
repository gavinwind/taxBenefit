package com.sinosoft.taxbenefit.entry.securityguard.biz.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.securityguard.ReqKeepBodyDTO;
import com.sinosoft.taxbenefit.api.inter.PreservationOfChangeApiService;
import com.sinosoft.taxbenefit.entry.securityguard.biz.service.PreservationChangeService;

@Service
public class PreservationChangeServiceImpl extends TaxBaseService implements
		PreservationChangeService {

	@Autowired
	private PreservationOfChangeApiService preservationChangeApiService;

	// @Override
	// public ThirdSendResponseDTO DataPersistence(Object obj, String serialNo)
	// {
	// return
	// }

	@Override
	public ThirdSendResponseDTO DataPersistence(ReqKeepBodyDTO obj,
			ParamHeadDTO paramHeadDTO) {
		
		if(1 == Integer.parseInt(paramHeadDTO.getRecordNum())){
			return preservationChangeApiService.PreservationOfChange(obj,paramHeadDTO);
		}else{
			throw new BusinessDataErrException("请求记录数与实际数据记录不符");
		}
	}

}
