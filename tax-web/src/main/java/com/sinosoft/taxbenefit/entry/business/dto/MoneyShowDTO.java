/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.ewealth.common.dto<br/>
 * @FileName: MoneyShowDTO.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.taxbenefit.entry.business.dto;

/**  
 * 【类或接口功能描述】
 * @Description: TODO
 * @author chenxin@sinosoft.com.cn
 * @date 2015年9月16日 下午5:00:27 
 * @version V1.0  
 */
public class MoneyShowDTO {
	private String amount;
	private String amountUnit;
	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}
	/**
	 * @return the amountUnit
	 */
	public String getAmountUnit() {
		return amountUnit;
	}
	/**
	 * @param amountUnit the amountUnit to set
	 */
	public void setAmountUnit(String amountUnit) {
		this.amountUnit = amountUnit;
	}
	
	
}


