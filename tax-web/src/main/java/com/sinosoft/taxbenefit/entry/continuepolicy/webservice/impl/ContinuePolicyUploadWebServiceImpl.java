package com.sinosoft.taxbenefit.entry.continuepolicy.webservice.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyInfoCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyInfoDTO;
import com.sinosoft.taxbenefit.entry.common.base.TaxBaseRequestWebService;
import com.sinosoft.taxbenefit.entry.continuepolicy.biz.service.ContinuePolicyBizService;
import com.sinosoft.taxbenefit.entry.continuepolicy.webservice.ContinuePolicyUploadWebService;
/**
 * Title:续保信息上传
 * @author yangdongkai@outlook.com
 * @date 2016年3月6日 下午6:00:41
 */
@Service(value="continuePolicyUploadWebServiceImpl")
public class ContinuePolicyUploadWebServiceImpl extends TaxBaseRequestWebService implements ContinuePolicyUploadWebService{
	@Autowired
	ContinuePolicyBizService continuePolicyBizService;
	
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Class getJsonParentClass() {
		return ReqContinuePolicyInfoCoreDTO.class;
	}

	@Override
	protected ThirdSendResponseDTO callBizService(Object obj,
			ParamHeadDTO paramHeadDTO) {
		ReqContinuePolicyInfoCoreDTO reqContinuePolicyInfoCoreDTO = (ReqContinuePolicyInfoCoreDTO)obj;
		ReqContinuePolicyInfoDTO reqContinuePolicyInfoDTO = (ReqContinuePolicyInfoDTO)reqContinuePolicyInfoCoreDTO.getBody();
		if(1 == Integer.parseInt(paramHeadDTO.getRecordNum())){
			return continuePolicyBizService.continuePolicySettlement(reqContinuePolicyInfoDTO, paramHeadDTO);
		}else{
			throw new BusinessDataErrException("请求记录数与实际数据记录不符");
		}
	}


}
