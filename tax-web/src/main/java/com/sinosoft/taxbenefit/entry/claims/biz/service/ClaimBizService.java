package com.sinosoft.taxbenefit.entry.claims.biz.service;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimReversalInfoDTO;

/**
 * 理赔接口(前置)
 * @author zhangke
 *
 */
public interface ClaimBizService {
	/**
	 * 理赔上传(多笔)
	 * @param reqClaimInfoDTO
	 * @param parmDto
	 * @return
	 */
	ThirdSendResponseDTO addClaimSettlementM(List<ReqClaimInfoDTO> reqClaimInfoDTOs, ParamHeadDTO parmDto);
	
	/**
	 * 理赔上传(单笔)
	 * @param reqClaimInfoDTO
	 * @param parmDto
	 * @return
	 */
	ThirdSendResponseDTO addClaimSettlement(ReqClaimInfoDTO reqClaimInfoDTO,ParamHeadDTO parmDto);

	/**
	 * 理赔撤销上传
	 * @param reqClaimReversalDTO
	 * @param parmDto
	 * @return
	 */
	ThirdSendResponseDTO modifyClaimState(ReqClaimReversalInfoDTO reqClaimReversalDTO,ParamHeadDTO parmDto);
}
