package com.sinosoft.taxbenefit.entry.customer.biz.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqTaxBeneVerifyInfoDTO;
import com.sinosoft.taxbenefit.api.inter.TaxBeneVerifyApiService;
import com.sinosoft.taxbenefit.entry.customer.biz.service.TaxBenefitBizService;
/**
 * 税优验证
 * @author zhangyu
 * @date 2016年3月6日 下午8:45:35
 */
@Service
public class TaxBenefitBizServiceImpl extends TaxBaseService implements TaxBenefitBizService{
    @Autowired
	private TaxBeneVerifyApiService taxBeneVerifyApiService;
	@Override
	public ThirdSendResponseDTO taxBenefit(ReqTaxBeneVerifyInfoDTO reqTaxBeneVerifyInfoDTO,ParamHeadDTO paramHeadDTO) {
		return taxBeneVerifyApiService.taxBenefitService(reqTaxBeneVerifyInfoDTO,paramHeadDTO);
	}
}