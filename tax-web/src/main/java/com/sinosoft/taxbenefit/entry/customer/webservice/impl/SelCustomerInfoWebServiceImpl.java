package com.sinosoft.taxbenefit.entry.customer.webservice.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqSelCustomerCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqSelCustomerInfoDTO;
import com.sinosoft.taxbenefit.entry.common.base.TaxBaseRequestWebService;
import com.sinosoft.taxbenefit.entry.customer.biz.service.SelCustomerInfoBizService;
import com.sinosoft.taxbenefit.entry.customer.webservice.SelCustomerInfoWebService;
/**
 * 查询客户信息
 * @author zhangyu
 * @date 2016年3月10日 上午9:57:39
 */
@Service(value="selCustomerInfoWebServiceImpl")
public class SelCustomerInfoWebServiceImpl extends TaxBaseRequestWebService implements SelCustomerInfoWebService{
    @Autowired
	SelCustomerInfoBizService selCustomerInfoBizService;
    @SuppressWarnings("rawtypes")
	@Override
	protected Class getJsonParentClass() {
		return ReqSelCustomerCoreDTO.class;
	}
	@Override
	protected ThirdSendResponseDTO callBizService(Object obj,
			ParamHeadDTO paramHeadDTO) {
		ReqSelCustomerCoreDTO reqSelCustomerCoreDTO=(ReqSelCustomerCoreDTO)obj;
		ReqSelCustomerInfoDTO reqSelCustomerInfoDTO=(ReqSelCustomerInfoDTO)reqSelCustomerCoreDTO.getBody();
		return selCustomerInfoBizService.queryCustomerInfo(reqSelCustomerInfoDTO, paramHeadDTO);
	}
}
