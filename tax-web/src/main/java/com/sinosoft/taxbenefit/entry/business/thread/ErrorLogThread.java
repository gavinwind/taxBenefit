package com.sinosoft.taxbenefit.entry.business.thread;

import java.util.Date;

import com.sinosoft.platform.common.util.DateUtil;
import com.sinosoft.taxbenefit.entry.business.dto.ErrorViewLogDTO;
import com.sinosoft.taxbenefit.entry.generated.mapper.TaxSystemErrorLogMapper;
import com.sinosoft.taxbenefit.entry.generated.model.TaxSystemErrorLog;

/**
 * 异常日志记录线程
 * 
 * @author zhangke
 *
 */
public class ErrorLogThread implements Runnable {
	private TaxSystemErrorLogMapper errorLogMapper;
	private ErrorViewLogDTO errorLogDTO;

	public ErrorLogThread(TaxSystemErrorLogMapper errorLogMapper,
			ErrorViewLogDTO errorLogDTO) {
		this.errorLogMapper = errorLogMapper;
		this.errorLogDTO = errorLogDTO;
	}

	@Override
	public void run() {
		Date current = DateUtil.getCurrentDate();
		TaxSystemErrorLog errorLog = new TaxSystemErrorLog();
		errorLog.setSerialNo(errorLogDTO.getSerialNo());
		errorLog.setCreateDate(current);
		errorLog.setErrorInfo(errorLogDTO.getErrorInfo());
		errorLog.setErrorType(errorLogDTO.getErroType());
		errorLog.setInterCode(errorLogDTO.getInterCode());
		errorLog.setModifyDate(current);
		errorLogMapper.insert(errorLog);
	}

}
