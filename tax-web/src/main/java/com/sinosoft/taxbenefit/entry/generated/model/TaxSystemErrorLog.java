package com.sinosoft.taxbenefit.entry.generated.model;

import java.util.Date;

public class TaxSystemErrorLog {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_SYSTEM_ERROR_LOG.SID
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	private Integer sid;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_SYSTEM_ERROR_LOG.SERIAL_NO
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	private String serialNo;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_SYSTEM_ERROR_LOG.INTER_CODE
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	private String interCode;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_SYSTEM_ERROR_LOG.ERROR_TYPE
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	private String errorType;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_SYSTEM_ERROR_LOG.ERROR_INFO
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	private String errorInfo;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_SYSTEM_ERROR_LOG.CREATE_DATE
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	private Date createDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_SYSTEM_ERROR_LOG.MODIFY_DATE
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	private Date modifyDate;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_SYSTEM_ERROR_LOG.SID
	 * @return  the value of TAX_SYSTEM_ERROR_LOG.SID
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	public Integer getSid() {
		return sid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_SYSTEM_ERROR_LOG.SID
	 * @param sid  the value for TAX_SYSTEM_ERROR_LOG.SID
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	public void setSid(Integer sid) {
		this.sid = sid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_SYSTEM_ERROR_LOG.SERIAL_NO
	 * @return  the value of TAX_SYSTEM_ERROR_LOG.SERIAL_NO
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	public String getSerialNo() {
		return serialNo;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_SYSTEM_ERROR_LOG.SERIAL_NO
	 * @param serialNo  the value for TAX_SYSTEM_ERROR_LOG.SERIAL_NO
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_SYSTEM_ERROR_LOG.INTER_CODE
	 * @return  the value of TAX_SYSTEM_ERROR_LOG.INTER_CODE
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	public String getInterCode() {
		return interCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_SYSTEM_ERROR_LOG.INTER_CODE
	 * @param interCode  the value for TAX_SYSTEM_ERROR_LOG.INTER_CODE
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	public void setInterCode(String interCode) {
		this.interCode = interCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_SYSTEM_ERROR_LOG.ERROR_TYPE
	 * @return  the value of TAX_SYSTEM_ERROR_LOG.ERROR_TYPE
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	public String getErrorType() {
		return errorType;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_SYSTEM_ERROR_LOG.ERROR_TYPE
	 * @param errorType  the value for TAX_SYSTEM_ERROR_LOG.ERROR_TYPE
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_SYSTEM_ERROR_LOG.ERROR_INFO
	 * @return  the value of TAX_SYSTEM_ERROR_LOG.ERROR_INFO
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	public String getErrorInfo() {
		return errorInfo;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_SYSTEM_ERROR_LOG.ERROR_INFO
	 * @param errorInfo  the value for TAX_SYSTEM_ERROR_LOG.ERROR_INFO
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	public void setErrorInfo(String errorInfo) {
		this.errorInfo = errorInfo;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_SYSTEM_ERROR_LOG.CREATE_DATE
	 * @return  the value of TAX_SYSTEM_ERROR_LOG.CREATE_DATE
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_SYSTEM_ERROR_LOG.CREATE_DATE
	 * @param createDate  the value for TAX_SYSTEM_ERROR_LOG.CREATE_DATE
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_SYSTEM_ERROR_LOG.MODIFY_DATE
	 * @return  the value of TAX_SYSTEM_ERROR_LOG.MODIFY_DATE
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	public Date getModifyDate() {
		return modifyDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_SYSTEM_ERROR_LOG.MODIFY_DATE
	 * @param modifyDate  the value for TAX_SYSTEM_ERROR_LOG.MODIFY_DATE
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
}