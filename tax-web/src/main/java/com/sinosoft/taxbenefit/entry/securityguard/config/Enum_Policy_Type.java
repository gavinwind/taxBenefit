package com.sinosoft.taxbenefit.entry.securityguard.config;

public enum Enum_Policy_Type {

	SINGLE_POLICY_HOLDER("01", "个人投保单"), GROUP_POLICY_HOLDER("02", "团体投保单");

	private final String code;
	private final String desc;

	Enum_Policy_Type(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public String code() {
		return code;
	}

	public String desc() {
		return desc;
	}

	public static Enum_Policy_Type getEnumByKey(String key) {
		for (Enum_Policy_Type enumItem : Enum_Policy_Type.values()) {
			if (key.equals(enumItem.code())) {
				return enumItem;
			}
		}
		return null;
	}
}
