package com.sinosoft.taxbenefit.entry.business.config;

public class TaxWebConstants {

	/** 真 */
	public final static String YES = "Y";
	/** 假 */
	public final static String NO = "N";

	public final static String PORTUNDEFINED = "该接口不可用";
}
