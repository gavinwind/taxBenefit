package com.sinosoft.taxbenefit.entry.claims.webservice;

import javax.jws.WebService;
/**
 * 理赔上传(单个)WebService
 * @author zhangke
 *
 */
@WebService
public interface ClaimWebService {
	public String dealMainBiz(String requestJson) ;
}
