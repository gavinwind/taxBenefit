package com.sinosoft.taxbenefit.entry.policystate.webservice;

import javax.jws.WebService;

@WebService
public interface PolicyStateChangeMoreWebService {
	public String dealMainBiz(String requestJson);
}
