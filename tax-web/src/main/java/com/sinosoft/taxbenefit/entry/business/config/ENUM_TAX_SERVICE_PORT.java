/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.ewealth.general.config<br/>
 * @FileName: ENUM_REPORTLOG_BIZTYPE.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.taxbenefit.entry.business.config;

/**
 * 服务接口类
 * @author SheChunMing
 *
 */
public enum ENUM_TAX_SERVICE_PORT {

	PORT_CUSTOMER_VERIFY("PTY001","客户身份验证"),
	PORT_CLAIMS_SETTLEMENT("CLM001","理赔信息上传");
	private final String code;
	private final String desc;

	ENUM_TAX_SERVICE_PORT(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public String code() {
		return code;
	}

	public String desc() {
		return desc;
	}
}


