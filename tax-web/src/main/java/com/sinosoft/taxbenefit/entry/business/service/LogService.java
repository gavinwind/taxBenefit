/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.ewealth.general.biz.service<br/>
 * @FileName: ReportLogService.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.taxbenefit.entry.business.service;

import com.sinosoft.taxbenefit.entry.business.dto.ErrorViewLogDTO;
import com.sinosoft.taxbenefit.entry.business.dto.ReceiveLogDTO;

/**  
 * 【报文日志服务接口】
 * @Description:报文日志服务接口
 * @author chenxin@sinosoft.com.cn
 * @date 2015年9月15日 下午10:47:11 
 * @version V1.0  
 */
public interface LogService {
	/**
	 * 记录请求报文交互日志 
	 * @Title: logReport 
	 * @Description: 记录报文交互日志 
	 * @param reportLogDTO
	 */
	void logReport(ReceiveLogDTO reportLogDTO);
	
	
	/**
	 * 记录异常报文日志
	 * @param errorViewLogDTO
	 */
	void logError(ErrorViewLogDTO errorViewLogDTO);
}


