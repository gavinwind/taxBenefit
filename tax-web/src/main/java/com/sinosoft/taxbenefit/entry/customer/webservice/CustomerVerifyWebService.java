package com.sinosoft.taxbenefit.entry.customer.webservice;

import javax.jws.WebService;
@WebService
public interface CustomerVerifyWebService {
	/**
	 * 客户验证
	 * @param requestJson
	 * @return
	 */
	public String dealMainBiz(String requestJson);
}
