package com.sinosoft.taxbenefit.entry.generated.mapper;

import com.sinosoft.taxbenefit.entry.generated.model.TaxSystemErrorLog;
import com.sinosoft.taxbenefit.entry.generated.model.TaxSystemErrorLogExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
/**
 * 系统接口异常记录
 * @author zhangke
 *
 */
public interface TaxSystemErrorLogMapper {

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_SYSTEM_ERROR_LOG
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	int countByExample(TaxSystemErrorLogExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_SYSTEM_ERROR_LOG
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	int deleteByExample(TaxSystemErrorLogExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_SYSTEM_ERROR_LOG
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	int deleteByPrimaryKey(Integer sid);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_SYSTEM_ERROR_LOG
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	int insert(TaxSystemErrorLog record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_SYSTEM_ERROR_LOG
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	int insertSelective(TaxSystemErrorLog record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_SYSTEM_ERROR_LOG
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	List<TaxSystemErrorLog> selectByExample(TaxSystemErrorLogExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_SYSTEM_ERROR_LOG
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	TaxSystemErrorLog selectByPrimaryKey(Integer sid);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_SYSTEM_ERROR_LOG
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	int updateByExampleSelective(@Param("record") TaxSystemErrorLog record,
			@Param("example") TaxSystemErrorLogExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_SYSTEM_ERROR_LOG
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	int updateByExample(@Param("record") TaxSystemErrorLog record,
			@Param("example") TaxSystemErrorLogExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_SYSTEM_ERROR_LOG
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	int updateByPrimaryKeySelective(TaxSystemErrorLog record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_SYSTEM_ERROR_LOG
	 * @mbggenerated  Fri May 13 09:51:08 CST 2016
	 */
	int updateByPrimaryKey(TaxSystemErrorLog record);
}