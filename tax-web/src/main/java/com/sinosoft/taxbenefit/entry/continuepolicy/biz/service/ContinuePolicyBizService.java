package com.sinosoft.taxbenefit.entry.continuepolicy.biz.service;


import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqMContinuePolicyInfoDTO;

public interface ContinuePolicyBizService {
	/**
	 * 续保信息上传
	 * @param policyInfoList
	 * @param paramHead
	 * @return
	 */
	ThirdSendResponseDTO continuePolicySettlement(ReqContinuePolicyInfoDTO policyInfoList,ParamHeadDTO paramHead);
	/**
	 * 续保信息上传（多个）
	 * @param policyInfoList
	 * @param paramHead
	 * @return
	 */
	ThirdSendResponseDTO continueMPolicySettlement(ReqMContinuePolicyInfoDTO policyInfoList,ParamHeadDTO paramHead);
}
