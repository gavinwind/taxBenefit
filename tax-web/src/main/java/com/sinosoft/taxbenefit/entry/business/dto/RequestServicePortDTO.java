package com.sinosoft.taxbenefit.entry.business.dto;

/**
 * 服务接口DTO参数
 * @author zhangke
 */
public class RequestServicePortDTO {
	// 接口编码
	private String portCode;
	// 接口名称
	private String portName;
	// 同步标记
	private String asynFlag;
	// 启用标记
	private String portFlag;

	public String getPortCode() {
		return portCode;
	}

	public void setPortCode(String portCode) {
		this.portCode = portCode;
	}

	public String getPortName() {
		return portName;
	}

	public void setPortName(String portName) {
		this.portName = portName;
	}

	public String getAsynFlag() {
		return asynFlag;
	}

	public void setAsynFlag(String asynFlag) {
		this.asynFlag = asynFlag;
	}

	public String getPortFlag() {
		return portFlag;
	}

	public void setPortFlag(String portFlag) {
		this.portFlag = portFlag;
	}

}
