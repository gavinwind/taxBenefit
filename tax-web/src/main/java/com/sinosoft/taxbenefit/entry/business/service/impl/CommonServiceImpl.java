package com.sinosoft.taxbenefit.entry.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.taxbenefit.entry.business.service.CommonService;
import com.sinosoft.taxbenefit.entry.generated.mapper.TaxRequestReceiveLogMapper;
import com.sinosoft.taxbenefit.entry.generated.model.TaxRequestReceiveLog;
import com.sinosoft.taxbenefit.entry.generated.model.TaxRequestReceiveLogExample;

/**
 * 【公共服务实现类】
 * 
 * @Description: 公共服务实现类
 * @author chenxin
 * @date 2016年1月29日 下午4:58:39
 * @version V1.0
 */
@Service
public class CommonServiceImpl extends TaxBaseService implements CommonService {
	@Autowired
	private TaxRequestReceiveLogMapper requestLogMapper;
	
	

	@Override
	public void checkRepeatRequest(String serialNo)
			throws BusinessDataErrException {
		// TODO Auto-generated method stub
		TaxRequestReceiveLogExample example = new TaxRequestReceiveLogExample();
		example.createCriteria().andSerialNoEqualTo(serialNo);
		List<TaxRequestReceiveLog> result = requestLogMapper.selectByExample(example);
		if (result != null && result.size() > 0) {
			throw new BusinessDataErrException("流水号为" + serialNo + "的交易请求重复！");
		}
	}

	@Override
	public void checkAlikeRequest(String serialNo) {
		TaxRequestReceiveLogExample example = new TaxRequestReceiveLogExample();
		example.createCriteria().andSerialNoEqualTo(serialNo);
		List<TaxRequestReceiveLog> result = requestLogMapper.selectByExample(example);
		if (result != null && result.size() > 0) {
			throw new BusinessDataErrException("重复请求,请求流水号为" +serialNo);
		}
		
	}
}
