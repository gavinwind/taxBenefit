package com.sinosoft.taxbenefit.entry.customer.webservice;

import javax.jws.WebService;
@WebService
public interface SelCustomerInfoWebService {
	/**
	 * 客戶信息查詢
	 * @param requestJson
	 * @return
	 */
	public String dealMainBiz(String requestJson);
}
