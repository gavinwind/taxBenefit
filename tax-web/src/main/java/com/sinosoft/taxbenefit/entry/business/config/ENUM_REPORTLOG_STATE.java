/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.ewealth.general.config<br/>
 * @FileName: ENUM_REPORTLOG_STATE.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.taxbenefit.entry.business.config;

/**  
 * 【类或接口功能描述】
 * @Description: TODO
 * @author chenxin@sinosoft.com.cn
 * @date 2015年9月15日 下午11:20:31 
 * @version V1.0  
 */
public enum ENUM_REPORTLOG_STATE {
	SUCC("01","处理成功"),FAIL("02","处理失败"),UNAUTH("03","非法请求");
	private final String code;
	private final String desc;

	ENUM_REPORTLOG_STATE(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public String code() {
		return code;
	}

	public String desc() {
		return desc;
	}
}


