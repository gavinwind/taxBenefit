package com.sinosoft.taxbenefit.entry.renewalcancel.webservice;

import javax.jws.WebService;

@WebService
public interface RenewalCancelWebService {
	/**
	 * 续保撤销
	 * @param requestJson
	 * @return
	 */
	public String dealMainBiz(String requestJson);
}
