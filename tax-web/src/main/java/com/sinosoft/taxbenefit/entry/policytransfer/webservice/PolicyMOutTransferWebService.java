package com.sinosoft.taxbenefit.entry.policytransfer.webservice;

import javax.jws.WebService;

/**
 * 保单多笔转出登记请求
 * @author SheChunMing
 *
 */
@WebService
public interface PolicyMOutTransferWebService {
	/**
	 * 保单多笔转出登记请求
	 * @param requestJson
	 * @return
	 */
	public String dealMainBiz(String requestJson);
}
