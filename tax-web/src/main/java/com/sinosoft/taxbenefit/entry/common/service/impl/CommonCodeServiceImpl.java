package com.sinosoft.taxbenefit.entry.common.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.taxbenefit.api.inter.CommonCodeApiService;
import com.sinosoft.taxbenefit.entry.business.dto.RequestServicePortDTO;
import com.sinosoft.taxbenefit.entry.common.service.CommonCodeService;
import com.sinosoft.taxbenefit.entry.generated.mapper.TaxServerPortMinuteMapper;
import com.sinosoft.taxbenefit.entry.generated.model.TaxServerPortMinute;
import com.sinosoft.taxbenefit.entry.generated.model.TaxServerPortMinuteExample;
@Service
public class CommonCodeServiceImpl extends TaxBaseService implements
		CommonCodeService {
	
	@Autowired
	TaxServerPortMinuteMapper taxServerPortMinuteMapper;
	@Autowired
	CommonCodeApiService commonCodeApiService;
	
	@Override
	public RequestServicePortDTO queryTaxServerPortDTOByPortCode(String portCode) {
		RequestServicePortDTO requestServicePortDTO = new RequestServicePortDTO();
		TaxServerPortMinuteExample example = new TaxServerPortMinuteExample();
		example.createCriteria().andServerPortCodeEqualTo(portCode);
		TaxServerPortMinute taxServerPortMinute = taxServerPortMinuteMapper.selectByExample(example).get(0);
		requestServicePortDTO.setPortCode(taxServerPortMinute.getServerPortCode());
		requestServicePortDTO.setPortName(taxServerPortMinute.getServerPortName());
		requestServicePortDTO.setPortFlag(taxServerPortMinute.getPortStartState());
		requestServicePortDTO.setAsynFlag(taxServerPortMinute.getPortDisposeType());
		return requestServicePortDTO;
	}
	@Override
	public String queryCommonCodeInfo(String sysCode, String codeType) {
		return commonCodeApiService.queryCommonCodeInfo(sysCode, codeType);
	}
}
