package com.sinosoft.taxbenefit.entry.accountchange.webservice.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.taxbenefit.api.dto.request.accountchange.ReqAccountInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.accountchange.ReqAccountMoreChangeCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.accountchange.ReqAccountMoreChangeDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.entry.accountchange.biz.service.AccountChangeBizService;
import com.sinosoft.taxbenefit.entry.accountchange.webservice.AccountMoreChangeWebService;
import com.sinosoft.taxbenefit.entry.common.base.TaxBaseRequestWebService;
/**
 * 账户信息变更上传请求(多笔)
 * @author zhangyu
 * @date 2016年3月18日 上午11:27:46
 */
@Service(value="accountMoreChangeWebServiceImpl")
public class AccountMoreChangeWebServiceImpl extends TaxBaseRequestWebService implements
		AccountMoreChangeWebService {
	@Autowired
	AccountChangeBizService accountChangeBizService;
	@SuppressWarnings("rawtypes")
	@Override
	protected Class getJsonParentClass() {
		return ReqAccountMoreChangeCoreDTO.class;
	}

	@Override
	protected ThirdSendResponseDTO callBizService(Object obj,ParamHeadDTO paramHeadDTO) {
		ReqAccountMoreChangeCoreDTO reqAccountMoreChangeCoreDTO=(ReqAccountMoreChangeCoreDTO)obj;
		ReqAccountMoreChangeDTO reqAccountMoreChangeDTO=(ReqAccountMoreChangeDTO)reqAccountMoreChangeCoreDTO.getBody();
		List<ReqAccountInfoDTO> reqAccountInfoList = (List<ReqAccountInfoDTO>)reqAccountMoreChangeDTO.getSavingAccountFeeList();
		if(Integer.parseInt(paramHeadDTO.getRecordNum()) == reqAccountInfoList.size()){
			return accountChangeBizService.AccountMoreChange(reqAccountInfoList, paramHeadDTO);
		}else{
			throw new BusinessDataErrException("请求记录数与实际数据记录不符");
		}
	}
}