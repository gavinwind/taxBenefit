package com.sinosoft.taxbenefit.entry.common.utils;
/**
 * Token处理工具类
 * @author chenxin
 * @date 2015-9-27
 */
public class TokenProcessor {
	private static TokenProcessor proccessor;
	public static TokenProcessor getInstance(){
		if (proccessor == null) {    
            synchronized (TokenProcessor.class) {    
               if (proccessor == null) {    
            	   proccessor = new TokenProcessor();   
               }    
            }    
        }    
        return proccessor;   
	}
	/**
	 * 获得
	 * @return
	 */
	public String generateToken(){
		return UUIDGenerator.getUUID();
	}
}
