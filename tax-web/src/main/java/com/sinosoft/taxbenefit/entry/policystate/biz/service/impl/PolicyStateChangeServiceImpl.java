package com.sinosoft.taxbenefit.entry.policystate.biz.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.policystate.PolicyStateEndorsement;
import com.sinosoft.taxbenefit.api.inter.PolicyStateChangeApiService;
import com.sinosoft.taxbenefit.entry.policystate.biz.service.PolicyStateChangeService;
@Service
public class PolicyStateChangeServiceImpl implements PolicyStateChangeService{

	@Autowired
	private PolicyStateChangeApiService policyStateApiService;
	
	@Override
	public ThirdSendResponseDTO modifyPolicyState(List<PolicyStateEndorsement> policybody,
			ParamHeadDTO paramHeadDTO) {
		
		return  policyStateApiService.policyStateChange(policybody, paramHeadDTO);
		
	}

}
