package com.sinosoft.taxbenefit.entry.claims.biz.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimReversalInfoDTO;
import com.sinosoft.taxbenefit.api.inter.ClaimInfoApiService;
import com.sinosoft.taxbenefit.entry.claims.biz.service.ClaimBizService;
/**
 * 理赔接口(前置)实现类型
 * @author zhangke
 *
 */
@Service
public class ClaimBizServiceImpl extends TaxBaseService implements ClaimBizService {
	@Autowired
	ClaimInfoApiService claimInfoApiService;

	@Override
	public ThirdSendResponseDTO addClaimSettlementM(List<ReqClaimInfoDTO> reqClaimInfoDTOs, ParamHeadDTO paramHeadDTO) {
		//理赔接口不需要地区代码
		paramHeadDTO.setAreaCode("");
		return claimInfoApiService.claimSettlementM(reqClaimInfoDTOs,
				paramHeadDTO);
	}

	@Override
	public ThirdSendResponseDTO addClaimSettlement(ReqClaimInfoDTO reqClaimInfoDTO, ParamHeadDTO parmDto) {
		//理赔接口不需要地区代码
		parmDto.setAreaCode("");
		return claimInfoApiService.claimSettlement(reqClaimInfoDTO, parmDto);
	}

	@Override
	public ThirdSendResponseDTO modifyClaimState(ReqClaimReversalInfoDTO reqClaimReversalDTO, ParamHeadDTO parmDto) {
		//理赔接口不需要地区代码
		parmDto.setAreaCode("");
		return claimInfoApiService.claimReversal(reqClaimReversalDTO, parmDto);
	}
}
