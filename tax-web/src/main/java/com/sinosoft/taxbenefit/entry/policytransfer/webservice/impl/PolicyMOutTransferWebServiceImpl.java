package com.sinosoft.taxbenefit.entry.policytransfer.webservice.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqMOutPolicyTransBodyDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqMOutPolicyTransCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqSOutPolicyTransferDTO;
import com.sinosoft.taxbenefit.entry.common.base.TaxBaseRequestWebService;
import com.sinosoft.taxbenefit.entry.policytransfer.biz.service.PolicyTransBizService;
import com.sinosoft.taxbenefit.entry.policytransfer.webservice.PolicyMOutTransferWebService;

/**
 * 保单多笔转出登记请求
 * @author SheChunMing
 */
@Service(value="policyMOutTransferWebServiceImpl")
public class PolicyMOutTransferWebServiceImpl extends TaxBaseRequestWebService implements PolicyMOutTransferWebService {

	@Autowired
	PolicyTransBizService policyTransBizService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Class getJsonParentClass() {
		return ReqMOutPolicyTransCoreDTO.class;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected ThirdSendResponseDTO callBizService(Object obj, ParamHeadDTO paramHead) {
		ReqMOutPolicyTransCoreDTO requestDTO = (ReqMOutPolicyTransCoreDTO)obj;
		ReqMOutPolicyTransBodyDTO policyTransBody = (ReqMOutPolicyTransBodyDTO)requestDTO.getBody();
		List<ReqSOutPolicyTransferDTO> transInfoList = policyTransBody.getTransferOutList();
		if(Integer.parseInt(paramHead.getRecordNum()) == transInfoList.size()){
			return policyTransBizService.policyTransMOutSettlement(transInfoList, paramHead);
		}else{
			throw new BusinessDataErrException("请求记录数与实际数据记录不符");
		}
	}
}
