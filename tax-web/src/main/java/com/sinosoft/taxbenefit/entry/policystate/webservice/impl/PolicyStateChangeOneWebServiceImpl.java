package com.sinosoft.taxbenefit.entry.policystate.webservice.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.policystate.PolicyStateEndorsement;
import com.sinosoft.taxbenefit.api.dto.request.policystate.PolicyStateOneBody;
import com.sinosoft.taxbenefit.entry.common.base.TaxBaseRequestWebService;
import com.sinosoft.taxbenefit.entry.policystate.biz.service.PolicyStateChangeService;
import com.sinosoft.taxbenefit.entry.policystate.webservice.PolicyStateChangeOneWebService;

@Service(value="policyStateChangeOneWebServiceImpl")
public class PolicyStateChangeOneWebServiceImpl extends TaxBaseRequestWebService 
implements PolicyStateChangeOneWebService{

	@Autowired
	private PolicyStateChangeService policyStateChangeService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Class getJsonParentClass() {
		return PolicyStateOneBody.class;
	}

	@Override
	protected ThirdSendResponseDTO callBizService(Object obj,
			ParamHeadDTO paramHeadDTO) {
		List<PolicyStateEndorsement> body=new ArrayList<PolicyStateEndorsement>();
		
		PolicyStateOneBody   OneBody =(PolicyStateOneBody) obj;
		PolicyStateEndorsement policyState =(PolicyStateEndorsement) OneBody.getBody();	
		body.add(policyState);
	
		if(1 == Integer.parseInt(paramHeadDTO.getRecordNum())){
			return policyStateChangeService.modifyPolicyState(body, paramHeadDTO);
		}else{
			throw new BusinessDataErrException("请求记录数与实际数据记录不符");
		}
	}

}
