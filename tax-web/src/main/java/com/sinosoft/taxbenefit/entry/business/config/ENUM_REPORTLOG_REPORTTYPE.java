/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.ewealth.general.config<br/>
 * @FileName: ENUM_REPORTLOG_REPORTTYPE.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.taxbenefit.entry.business.config;

/**  
 * 【类或接口功能描述】
 * @Description: TODO
 * @author chenxin@sinosoft.com.cn
 * @date 2015年9月15日 下午11:20:46 
 * @version V1.0  
 */
public enum ENUM_REPORTLOG_REPORTTYPE {

	SEND("01","发送报文"),RECEIVE("02","接收报文");
	private final String code;
	private final String desc;

	ENUM_REPORTLOG_REPORTTYPE(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public String code() {
		return code;
	}

	public String desc() {
		return desc;
	}
}


