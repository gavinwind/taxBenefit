package com.sinosoft.taxbenefit.entry.customer.webservice;

import javax.jws.WebService;
@WebService
public interface TaxBeneVerifyWebService {
	/**
	 * 税优验证
	 * @param requestJson
	 * @return
	 */
	public String dealMainBiz(String requestJson);
}
