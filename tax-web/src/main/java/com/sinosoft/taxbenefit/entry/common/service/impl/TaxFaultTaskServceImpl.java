package com.sinosoft.taxbenefit.entry.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.taxbenefit.api.dto.common.FaultTaskDTO;
import com.sinosoft.taxbenefit.api.dto.common.TaxFaultTaskInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.TaxFaulTaskDTO;
import com.sinosoft.taxbenefit.api.inter.TaxFaultTaskApiService;
import com.sinosoft.taxbenefit.entry.common.service.TaxFaultTaskService;
@Service
public class TaxFaultTaskServceImpl extends TaxBaseService implements TaxFaultTaskService {
	@Autowired
	private TaxFaultTaskApiService faultTaskApiService;

	@Override
	public void addTaxFaultTask(TaxFaulTaskDTO taxFaulTaskDTO) {
		faultTaskApiService.addTaxFaultTask(taxFaulTaskDTO);
	}

	@Override
	public FaultTaskDTO queryTaxFaultTaskBySerialNo(String serialNo) {
		return faultTaskApiService.queryTaxFaultTaskBySerialNo(serialNo);
	}

}
