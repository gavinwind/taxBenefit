package com.sinosoft.taxbenefit.entry.securityguard.biz.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.securityguard.ReqEndorsementCancelInfoDTO;
import com.sinosoft.taxbenefit.api.inter.PreservationOfCancelApiService;
import com.sinosoft.taxbenefit.entry.securityguard.biz.service.PreservationCancelService;

@Service
public class PreservationCancelServiceImpl extends TaxBaseService implements
		PreservationCancelService {
	@Autowired
	private PreservationOfCancelApiService preservationCancelApiService;

	@Override
	public ThirdSendResponseDTO preservationCancel(
			ReqEndorsementCancelInfoDTO obj, ParamHeadDTO paramHeadDTO) {

		if (1 == Integer.parseInt(paramHeadDTO.getRecordNum())) {
			return preservationCancelApiService.PreservationOfChange(obj,paramHeadDTO);
		} else {
			throw new BusinessDataErrException("请求记录数与实际数据记录不符");
		}
	}

}
