package com.sinosoft.taxbenefit.entry.customer.webservice.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqCustomerCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqCustomerInfoDTO;
import com.sinosoft.taxbenefit.entry.common.base.TaxBaseRequestWebService;
import com.sinosoft.taxbenefit.entry.customer.biz.service.CustomerVerifyBizService;
import com.sinosoft.taxbenefit.entry.customer.webservice.CustomerVerifyWebService;
/**
 * 客戶验证上传服务
 * @author zhangyu
 * @date 2016年3月6日 下午5:59:55
 */
@Service(value="customerVerifyWebServiceImpl")
public class CustomerVerifyWebServiceImpl extends TaxBaseRequestWebService implements CustomerVerifyWebService{
	@Autowired
	CustomerVerifyBizService customerVerifyBizService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Class getJsonParentClass() {
		return ReqCustomerCoreDTO.class;
	}
	@Override
	protected ThirdSendResponseDTO callBizService(Object obj,ParamHeadDTO paramHeadDTO) {
		ReqCustomerCoreDTO RequestDTO=(ReqCustomerCoreDTO)obj;
	    ReqCustomerInfoDTO customerDTO=(ReqCustomerInfoDTO)RequestDTO.getBody();
		ThirdSendResponseDTO thirdSendResponseDTO = customerVerifyBizService.CustomerVerify(customerDTO,paramHeadDTO);
		return thirdSendResponseDTO;		
	}
}