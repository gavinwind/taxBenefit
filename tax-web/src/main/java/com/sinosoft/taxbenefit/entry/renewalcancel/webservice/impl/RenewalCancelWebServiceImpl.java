package com.sinosoft.taxbenefit.entry.renewalcancel.webservice.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalcancel.RequestRenewalCancelCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalcancel.RequestRenewalCancelDTO;
import com.sinosoft.taxbenefit.entry.common.base.TaxBaseRequestWebService;
import com.sinosoft.taxbenefit.entry.renewalcancel.biz.service.RenewalCancelBizService;
import com.sinosoft.taxbenefit.entry.renewalcancel.webservice.RenewalCancelWebService;

@Service(value="renewalCancelWebServiceImpl")
public class RenewalCancelWebServiceImpl extends TaxBaseRequestWebService implements RenewalCancelWebService {
	@Autowired
	RenewalCancelBizService renewalCancelBizService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Class getJsonParentClass() {
		return RequestRenewalCancelCoreDTO.class;
	}

	@Override
	protected ThirdSendResponseDTO callBizService(Object obj,ParamHeadDTO paramHeadDTO) {
		RequestRenewalCancelCoreDTO requestDTO = (RequestRenewalCancelCoreDTO) obj;
		RequestRenewalCancelDTO renewalCancelDTO = (RequestRenewalCancelDTO)requestDTO.getBody();
		if(1 == Integer.parseInt(paramHeadDTO.getRecordNum())){
			return renewalCancelBizService.renewalCancel(renewalCancelDTO, paramHeadDTO);
		}else{
			throw new BusinessDataErrException("请求记录数与实际数据记录不符");
		}
	}
}