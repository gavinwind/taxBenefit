package com.sinosoft.taxbenefit.entry.policystate.webservice.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.policystate.PolicyStateEndorsement;
import com.sinosoft.taxbenefit.api.dto.request.policystate.PolicyStateMoreBody;
import com.sinosoft.taxbenefit.entry.common.base.TaxBaseRequestWebService;
import com.sinosoft.taxbenefit.entry.policystate.biz.service.PolicyStateChangeService;
import com.sinosoft.taxbenefit.entry.policystate.webservice.PolicyStateChangeMoreWebService;

@Service(value = "policyStateChangeMoreWebServiceImpl")
public class PolicyStateChangeMoreWebServiceImpl extends
		TaxBaseRequestWebService implements PolicyStateChangeMoreWebService {

	@Autowired
	private PolicyStateChangeService policyStateChangeService;

	@SuppressWarnings("rawtypes")
	@Override
	protected Class getJsonParentClass() {
		return PolicyStateMoreBody.class;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected ThirdSendResponseDTO callBizService(Object obj,
			ParamHeadDTO paramHeadDTO) {

		PolicyStateMoreBody PolicyBody = (PolicyStateMoreBody) obj;

		List<PolicyStateEndorsement> bodyDto = (List<PolicyStateEndorsement>) PolicyBody
				.getBody();
		
		if(Integer.parseInt(paramHeadDTO.getRecordNum()) == bodyDto.size()){
			return policyStateChangeService.modifyPolicyState(bodyDto, paramHeadDTO);
		}else{
			throw new BusinessDataErrException("请求记录数与实际数据记录不符");
		}
	}

}
