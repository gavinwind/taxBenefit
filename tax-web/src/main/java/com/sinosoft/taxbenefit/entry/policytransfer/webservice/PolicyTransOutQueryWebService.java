package com.sinosoft.taxbenefit.entry.policytransfer.webservice;

import javax.jws.WebService;

/**
 * 保单转出信息查询
 * @author SheChunMing
 */
@WebService
public interface PolicyTransOutQueryWebService {

	/**
	 * 保单转出信息查询
	 * @param requestJson
	 * @return
	 */
	public String dealMainBiz(String requestJson);
}
