package com.sinosoft.taxbenefit.entry.policytransfer.biz.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqMInPolicyTransferDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqPolicyInTransferQueryDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqSInPolicyTransferDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqSOutPolicyTransferDTO;
import com.sinosoft.taxbenefit.api.inter.PolicyTransferApiService;
import com.sinosoft.taxbenefit.entry.policytransfer.biz.service.PolicyTransBizService;

@Service
public class PolicyTransBizServiceImpl extends TaxBaseService implements PolicyTransBizService {
	
	@Autowired
	PolicyTransferApiService policyTransferApiService;
	@Override
	public ThirdSendResponseDTO policyTransSInSettlement(ReqSInPolicyTransferDTO transInfo, ParamHeadDTO paramHead) {
		return policyTransferApiService.policyTransSInSettlement(transInfo, paramHead);
	}
	@Override
	public ThirdSendResponseDTO policyTransMInSettlement(ReqMInPolicyTransferDTO transInfo, ParamHeadDTO paramHead) {
		return policyTransferApiService.policyTransMInSettlement(transInfo, paramHead);
	}
	@Override
	public ThirdSendResponseDTO policyTransSOutSettlement(ReqSOutPolicyTransferDTO transInfo, ParamHeadDTO paramHead) {
		return policyTransferApiService.policyTransSOutSettlement(transInfo, paramHead);
	}
	@Override
	public ThirdSendResponseDTO policyTransMOutSettlement(List<ReqSOutPolicyTransferDTO> transInfoList, ParamHeadDTO paramHead) {
		return policyTransferApiService.policyTransMOutSettlement(transInfoList, paramHead);
	}
	@Override
	public ThirdSendResponseDTO policyTransInBalanceSettlement(ReqPolicyInTransferQueryDTO transInfo, ParamHeadDTO paramHead) {
		return policyTransferApiService.policyTransInBalanceSettlement(transInfo, paramHead);
	}
	@Override
	public ThirdSendResponseDTO policyTransOutQuerySettlement(ReqPolicyInTransferQueryDTO transInfo, ParamHeadDTO paramHead) {
		return policyTransferApiService.policyTransOutQuerySettlement(transInfo, paramHead);
	}
	@Override
	public ThirdSendResponseDTO policyTransOutRegisterQuerySettlement(ReqPolicyInTransferQueryDTO transInfo, ParamHeadDTO paramHead) {
		return policyTransferApiService.policyTransOutRegisterQuerySettlement(transInfo, paramHead);
	}

}
