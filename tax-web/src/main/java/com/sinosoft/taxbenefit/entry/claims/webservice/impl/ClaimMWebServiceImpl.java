package com.sinosoft.taxbenefit.entry.claims.webservice.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimCoreMDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimInfoDTO;
import com.sinosoft.taxbenefit.entry.claims.biz.service.ClaimBizService;
import com.sinosoft.taxbenefit.entry.claims.webservice.ClaimMWebService;
import com.sinosoft.taxbenefit.entry.common.base.TaxBaseRequestWebService;

/**
 * 理赔上传(多个)WebService实现类
 * @author zhangke
 *
 */
@Service(value="claimMWebServiceImpl")
public class ClaimMWebServiceImpl extends TaxBaseRequestWebService implements ClaimMWebService {
	@Autowired
	private	ClaimBizService claimBizService;
	@SuppressWarnings("rawtypes")
	@Override
	protected Class getJsonParentClass() {
		return ReqClaimCoreMDTO.class;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected ThirdSendResponseDTO callBizService(Object obj,ParamHeadDTO paramHeadDTO) {
		ReqClaimCoreMDTO requestDTO = (ReqClaimCoreMDTO) obj;
		List<ReqClaimInfoDTO> reqClaimCoreDTOs = (List<ReqClaimInfoDTO>) requestDTO.getBody();
		if(Integer.parseInt(paramHeadDTO.getRecordNum()) == reqClaimCoreDTOs.size()){
			return claimBizService.addClaimSettlementM(reqClaimCoreDTOs, paramHeadDTO);
		}else{
			throw new BusinessDataErrException("请求记录数与实际数据记录不符");
		}		
	}
}
