package com.sinosoft.taxbenefit.entry.customer.webservice;

import javax.jws.WebService;
@WebService
public interface InsuredPayWebService {
    /**
     * 被保人既往理赔额查询
     * @param requestJson
     * @return
     */
	public String dealMainBiz(String requestJson);
}
