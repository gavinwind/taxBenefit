package com.sinosoft.taxbenefit.entry.continuepolicy.webservice;

import javax.jws.WebService;

@WebService
public interface ContinuePolicyUploadWebService {
	/**
	 * 续保信息上传
	 * @param requestJson
	 * @return
	 */
	public String dealMainBiz(String requestJson);
}
