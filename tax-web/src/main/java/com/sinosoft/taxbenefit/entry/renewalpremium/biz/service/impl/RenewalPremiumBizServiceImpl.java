package com.sinosoft.taxbenefit.entry.renewalpremium.biz.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqMPremiumInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqSPremiumInfoDTO;
import com.sinosoft.taxbenefit.api.inter.RenewalPremiumInfoApiService;
import com.sinosoft.taxbenefit.entry.renewalpremium.biz.service.RenewalPremiumBizService;
@Service
public class RenewalPremiumBizServiceImpl implements RenewalPremiumBizService {
	@Autowired
	RenewalPremiumInfoApiService renewalPremiumInfoService;

	@Override
	public ThirdSendResponseDTO RenewalPremium(ReqSPremiumInfoDTO reqSPremiumInfoDTO,ParamHeadDTO paramHead) {
		return renewalPremiumInfoService.renewalPremium(reqSPremiumInfoDTO, paramHead);
	}

	@Override
	public ThirdSendResponseDTO RenewalMPremium(ReqMPremiumInfoDTO reqMPremiumInfoDTO, ParamHeadDTO paramHead) {
		return renewalPremiumInfoService.renewalMPremium(reqMPremiumInfoDTO, paramHead);
	}
}