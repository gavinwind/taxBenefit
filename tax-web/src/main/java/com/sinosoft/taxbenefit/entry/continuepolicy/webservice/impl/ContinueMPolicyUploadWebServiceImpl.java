package com.sinosoft.taxbenefit.entry.continuepolicy.webservice.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqMContinuePolicyInfoCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqMContinuePolicyInfoDTO;
import com.sinosoft.taxbenefit.entry.common.base.TaxBaseRequestWebService;
import com.sinosoft.taxbenefit.entry.continuepolicy.biz.service.ContinuePolicyBizService;
import com.sinosoft.taxbenefit.entry.continuepolicy.webservice.ContinueMPolicyUploadWebService;
/**
 * Title:续保信息上传（多个）
 * @author yangdongkai@outlook.com
 * @date 2016年3月18日 上午10:13:20
 */
@Service(value="continueMPolicyUploadWebServiceImpl")
public class ContinueMPolicyUploadWebServiceImpl extends TaxBaseRequestWebService implements ContinueMPolicyUploadWebService {
	@Autowired
	ContinuePolicyBizService continuePolicyBizService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Class getJsonParentClass() {
		return ReqMContinuePolicyInfoCoreDTO.class;
	}


	@Override
	protected ThirdSendResponseDTO callBizService(Object obj,ParamHeadDTO paramHeadDTO) {
		ReqMContinuePolicyInfoCoreDTO reqMContinuePolicyInfoCoreDTO = (ReqMContinuePolicyInfoCoreDTO)obj;
		ReqMContinuePolicyInfoDTO reqMContinuePolicyInfoDTO = (ReqMContinuePolicyInfoDTO)reqMContinuePolicyInfoCoreDTO.getBody();
		if(Integer.parseInt(paramHeadDTO.getRecordNum()) == reqMContinuePolicyInfoDTO.getPolicyList().size()){
			return continuePolicyBizService.continueMPolicySettlement(reqMContinuePolicyInfoDTO, paramHeadDTO);
		}else{
			throw new BusinessDataErrException("请求记录数与实际数据记录不符");
		}
	}

}
