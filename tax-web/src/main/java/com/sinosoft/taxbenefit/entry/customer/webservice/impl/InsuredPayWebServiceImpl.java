package com.sinosoft.taxbenefit.entry.customer.webservice.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqInsuredPayCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqInsuredPayInfoDTO;
import com.sinosoft.taxbenefit.entry.common.base.TaxBaseRequestWebService;
import com.sinosoft.taxbenefit.entry.customer.biz.service.InsuredPayBizService;
import com.sinosoft.taxbenefit.entry.customer.webservice.InsuredPayWebService;
/**
 * 
 * title：被保人既往理赔额查询
 * @author zhangyu
 * @date 2016年3月10日 上午9:51:27
 */
@Service(value="insuredPayWebServiceImpl")
public class InsuredPayWebServiceImpl extends TaxBaseRequestWebService implements InsuredPayWebService{
    @Autowired
    InsuredPayBizService insuredPayBizService;
    
    @SuppressWarnings("rawtypes")
	@Override
	protected Class getJsonParentClass() {
		return ReqInsuredPayCoreDTO.class;
	}
	@Override
	protected ThirdSendResponseDTO callBizService(Object obj,
			ParamHeadDTO paramHeadDTO) {
		ReqInsuredPayCoreDTO customerCoreDTO=(ReqInsuredPayCoreDTO)obj;
		ReqInsuredPayInfoDTO insuredPayInfoDTO=(ReqInsuredPayInfoDTO)customerCoreDTO.getBody();
		return insuredPayBizService.InsuredPay(insuredPayInfoDTO,paramHeadDTO);
	}
}