package com.sinosoft.taxbenefit.entry.claims.webservice;

import javax.jws.WebService;

/**
 * 理赔撤销上传
 * @author zhangke
 *
 */
@WebService
public interface ClaimReversalWebService {
	public String dealMainBiz(String requestJson) ;
}
