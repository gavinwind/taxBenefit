package com.sinosoft.taxbenefit.entry.applicant.webservice.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.taxbenefit.api.dto.request.applicant.ReqAppAcceptMoreBody;
import com.sinosoft.taxbenefit.api.dto.request.applicant.ReqAppPolicyDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.entry.applicant.biz.service.ApplicantBizService;
import com.sinosoft.taxbenefit.entry.applicant.webservice.ApplicantMoreWebService;
import com.sinosoft.taxbenefit.entry.common.base.TaxBaseRequestWebService;

@Service(value = "applicantMoreWebServiceImpl")
public class ApplicantMoreWebServiceImpl extends TaxBaseRequestWebService
		implements ApplicantMoreWebService {

	@Autowired
	private ApplicantBizService applicantBizService;

	@SuppressWarnings("rawtypes")
	@Override
	protected Class getJsonParentClass() {
		return ReqAppAcceptMoreBody.class;

	}

	@SuppressWarnings("unchecked")
	@Override
	protected ThirdSendResponseDTO callBizService(Object obj,
			ParamHeadDTO paramHeadDTO) {

		ReqAppAcceptMoreBody MoreBody = (ReqAppAcceptMoreBody) obj;
		List<ReqAppPolicyDTO> reqAppAcceptMores = (List<ReqAppPolicyDTO>) MoreBody.getBody();
		if (reqAppAcceptMores.size() == Integer.parseInt(paramHeadDTO.getPortName())) {
			return applicantBizService.addApplicant(reqAppAcceptMores,paramHeadDTO);
		} else {
			throw new BusinessDataErrException("请求记录数与实际数据记录不符");
		}
	}

}
