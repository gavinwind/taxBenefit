package com.sinosoft.taxbenefit.entry.policytransfer.webservice.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqPolicyInTransferQueryCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqPolicyInTransferQueryDTO;
import com.sinosoft.taxbenefit.entry.common.base.TaxBaseRequestWebService;
import com.sinosoft.taxbenefit.entry.policytransfer.biz.service.PolicyTransBizService;
import com.sinosoft.taxbenefit.entry.policytransfer.webservice.PolicyTransOutRegisterQueryWebService;

/**
 * 保单转出登记信息查询
 * @author SheChunMing
 */
@Service(value="policyTransOutRegisterQueryWebServiceImpl")
public class PolicyTransOutRegisterQueryWebServiceImpl extends TaxBaseRequestWebService implements PolicyTransOutRegisterQueryWebService {

	@Autowired
	PolicyTransBizService policyTransBizService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Class getJsonParentClass() {
		return ReqPolicyInTransferQueryCoreDTO.class;
	}

	@Override
	protected ThirdSendResponseDTO callBizService(Object obj, ParamHeadDTO paramHead) {
		ReqPolicyInTransferQueryCoreDTO requestDTO = (ReqPolicyInTransferQueryCoreDTO)obj;
		ReqPolicyInTransferQueryDTO transInfo = (ReqPolicyInTransferQueryDTO) requestDTO.getBody();
		return policyTransBizService.policyTransOutRegisterQuerySettlement(transInfo, paramHead);
	}
}
