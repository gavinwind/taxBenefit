package com.sinosoft.taxbenefit.entry.renewalpremium.webservice.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqRenewalPremiumCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqSPremiumInfoDTO;
import com.sinosoft.taxbenefit.entry.common.base.TaxBaseRequestWebService;
import com.sinosoft.taxbenefit.entry.renewalpremium.biz.service.RenewalPremiumBizService;
import com.sinosoft.taxbenefit.entry.renewalpremium.webservice.PremiumUploadWebService;

/**
 * Title: 续期保费信息上传
 * 
 * @author yangdongkai@outlook.com
 * @date 2016年3月6日 下午6:01:09
 */
@Service(value="premiumUploadWebServiceImpl")
public class PremiumUploadWebServiceImpl extends TaxBaseRequestWebService implements PremiumUploadWebService{
	@Autowired
	RenewalPremiumBizService renewalPremiumBizService;

	@SuppressWarnings("rawtypes")
	@Override
	protected Class getJsonParentClass() {
		return ReqRenewalPremiumCoreDTO.class;
	}

	@Override
	protected ThirdSendResponseDTO callBizService(Object obj,ParamHeadDTO paramHead) {
		ReqRenewalPremiumCoreDTO requestDTO = (ReqRenewalPremiumCoreDTO) obj;
		ReqSPremiumInfoDTO premiumInfo = (ReqSPremiumInfoDTO) requestDTO.getBody();
		if(1 == Integer.parseInt(paramHead.getRecordNum())){
			return renewalPremiumBizService.RenewalPremium(premiumInfo, paramHead);
		}else{
			throw new BusinessDataErrException("请求记录数与实际数据记录不符");
		}
	}
}