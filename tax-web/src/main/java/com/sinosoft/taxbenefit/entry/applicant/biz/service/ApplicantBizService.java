package com.sinosoft.taxbenefit.entry.applicant.biz.service;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.applicant.ReqAppPolicyDTO;
import com.sinosoft.taxbenefit.api.dto.request.applicant.ReqPolicyCancelDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;

/**
 * 承保信息服务接口(前置)
 * 
 * @author zhangke
 *
 */
public interface ApplicantBizService {
	/**
	 * 承保信息上传(支持多笔)
	 * @param reqAppAcceptMores
	 * @param paramHeadDTO
	 * @return
	 */
	ThirdSendResponseDTO addApplicant(List<ReqAppPolicyDTO> reqAppAcceptMores,ParamHeadDTO paramHeadDTO);
	/**
	 * 承保撤销
	 * @param reqPolicyCancelDTO
	 * @param paramHeadDTO
	 * @return
	 */
	ThirdSendResponseDTO modifyPolicyState(ReqPolicyCancelDTO reqPolicyCancelDTO,ParamHeadDTO paramHeadDTO);
}
