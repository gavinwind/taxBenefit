package com.sinosoft.taxbenefit.entry.policystate.biz.service;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.policystate.PolicyStateEndorsement;

public interface PolicyStateChangeService {
	public ThirdSendResponseDTO modifyPolicyState(List<PolicyStateEndorsement> obj,ParamHeadDTO paramHeadDTO);
}
