package com.sinosoft.taxbenefit.entry.common.service;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.common.FaultTaskDTO;
import com.sinosoft.taxbenefit.api.dto.common.TaxFaultTaskInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.TaxFaulTaskDTO;

public interface TaxFaultTaskService {
	/**
	 * 添加新的异常任务
	 * @param taxFaultTaskDTO
	 */
	void addTaxFaultTask(TaxFaulTaskDTO taxFaulTaskDTO);
	
	/**
	 * 根据批次号查询异常任务信息
	 * @param serialNo 流水号
	 * @return 
	 */
	FaultTaskDTO queryTaxFaultTaskBySerialNo(String serialNo);
}
