package com.sinosoft.taxbenefit.entry.continuepolicy.webservice;

import javax.jws.WebService;

@WebService
public interface ContinueMPolicyUploadWebService {
	/**
	 * 续保信息上传(多个)
	 * @param requestJson
	 * @return
	 */
	public String dealMainBiz(String requestJson);
}
