package com.sinosoft.taxbenefit.entry.renewalpremium.webservice;

import javax.jws.WebService;

@WebService
public interface PremiumUploadMWebService {
	/**
	 * 续期保费信息上传(多个)
	 * @param requestJson
	 * @return
	 */
	public String dealMainBiz(String requestJson);
}
