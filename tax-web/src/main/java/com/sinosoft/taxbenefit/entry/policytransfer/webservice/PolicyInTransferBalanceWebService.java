package com.sinosoft.taxbenefit.entry.policytransfer.webservice;

import javax.jws.WebService;

/**
 * 保单转入余额信息查询
 * @author SheChunMing
 */
@WebService
public interface PolicyInTransferBalanceWebService {

	/**
	 * 保单转入余额信息查询
	 * @param requestJson
	 * @return
	 */
	public String dealMainBiz(String requestJson);
}
