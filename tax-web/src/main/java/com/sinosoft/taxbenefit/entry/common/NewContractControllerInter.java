/**
 * @Copyright ®2015 Sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.ebiz.entry.common<br/>
 * @FileName: NewContractControllerInter.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.taxbenefit.entry.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;

/**  
 * 【类或接口功能描述】
 * @Description: TODO
 * @author chenxin@sinosoft.com.cn
 * @date 2015年12月25日 下午7:56:20 
 * @version V1.0  
*/
public interface NewContractControllerInter {
	/**
	 * 新契约承保请求
	 * @Title: newContract 
	 * @Description: 保险新契约承保接口，处理新契约承保请求
	 * @param request
	 * @param response
	 * @param model
	 */
	void newContract(HttpServletRequest request,HttpServletResponse response,ModelMap model);
	/**
	 * 保单核保请求
	 * @Title: underWriting 
	 * @Description: 处理保单核保请求
	 * @param request
	 * @param response
	 * @param model
	 */
	void underWriting(HttpServletRequest request,HttpServletResponse response,ModelMap model);
	/**
	 * 保单撤单请求处理
	 * @Title: cancelContract 
	 * @Description: TODO
	 * @param request
	 * @param response
	 * @param model
	 */
	void cancelContract(HttpServletRequest request,HttpServletResponse response,ModelMap model);
	
}
