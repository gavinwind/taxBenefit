package com.sinosoft.taxbenefit.entry.securityguard.webservice.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.securityguard.ReqEndorsementCancelBodyDTO;
import com.sinosoft.taxbenefit.api.dto.request.securityguard.ReqEndorsementCancelInfoDTO;
import com.sinosoft.taxbenefit.entry.common.base.TaxBaseRequestWebService;
import com.sinosoft.taxbenefit.entry.securityguard.biz.service.PreservationCancelService;
import com.sinosoft.taxbenefit.entry.securityguard.webservice.PreservationOfCancelWebService;

@Service(value="preservationOfCancelWebServiceImpl")
public class PreservationOfCancelWebServiceImpl extends TaxBaseRequestWebService implements PreservationOfCancelWebService{

	@Autowired
	PreservationCancelService preservationCancelService;
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Class getJsonParentClass() {
		
		return ReqEndorsementCancelBodyDTO.class;
	}

	@Override
	protected ThirdSendResponseDTO callBizService(Object obj,
			ParamHeadDTO paramHeadDTO) {
//		获取报文体
		ReqEndorsementCancelBodyDTO jsonTax=(ReqEndorsementCancelBodyDTO) obj;
		ReqEndorsementCancelInfoDTO bodyDto = (ReqEndorsementCancelInfoDTO) jsonTax.getBody();
		return preservationCancelService.preservationCancel(bodyDto, paramHeadDTO);
	}

}
