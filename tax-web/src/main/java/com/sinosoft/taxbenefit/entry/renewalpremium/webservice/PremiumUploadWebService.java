package com.sinosoft.taxbenefit.entry.renewalpremium.webservice;

import javax.jws.WebService;

@WebService
public interface PremiumUploadWebService {
	/**
	 * 续期保费信息上传
	 * @param requestJson
	 * @return
	 */
	public String dealMainBiz(String requestJson);
}
