package com.sinosoft.taxbenefit.entry.policytransfer.webservice;

import javax.jws.WebService;
/**
 * 保单多笔转入申请
 * @author SheChunMing
 */
@WebService
public interface PolicyMInTransferWebService {
	
	/**
	 * 保单多笔转入申请
	 * @param requestJson
	 * @return
	 */
	public String dealMainBiz(String requestJson);
}
