package com.sinosoft.taxbenefit.entry.customer.biz.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqSelCustomerInfoDTO;
import com.sinosoft.taxbenefit.api.inter.CustomerInfoQueryApiService;
import com.sinosoft.taxbenefit.entry.customer.biz.service.SelCustomerInfoBizService;
/**
 * 客戶信息查詢
 * @author zhangyu
 * @date 2016年3月11日 下午4:53:29
 */
@Service
public class SelCustomerInfoBizServiceImpl extends TaxBaseService implements SelCustomerInfoBizService{
	@Autowired
	CustomerInfoQueryApiService customerInfoQueryApiService;
	@Override
	public ThirdSendResponseDTO queryCustomerInfo(
			ReqSelCustomerInfoDTO reqSelCustomerInfoDTO,
			ParamHeadDTO paramHeadDTO) {
		//客户概要信息查询不需要地区代码
		paramHeadDTO.setAreaCode("");
		return customerInfoQueryApiService.queryCustomerInfo(reqSelCustomerInfoDTO, paramHeadDTO);
	}
}