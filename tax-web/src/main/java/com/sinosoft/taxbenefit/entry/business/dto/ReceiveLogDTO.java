/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.ewealth.common.dto<br/>
 * @FileName: ReportLogDTO.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.taxbenefit.entry.business.dto;

/**
 * 【请求报文日志DTO】
 * 
 * @Description: 请求报文日志DTO
 * @author shechunming@sinosoft.com.cn
 * @date 2016年2月17日 下午10:50:59
 * @version V1.0
 */
public class ReceiveLogDTO {
	/** 交易流水号 */
	private String serialNo;
	/** 请求ip */
	private String requestIp;
	/** 请求来源 */
	private String sourceCode;
	/** 报文业务类型 */
	private String businessType;
	/** 请求报文 */
	private String requestJson;
	/** 相应报文 */
	private String responseJson;
	/** 状态 */
	private String state;

	/** 备注信息 */
	private String description;
	/** 同步异步 */
	private String isSync;

	public String getRequestJson() {
		return requestJson;
	}

	public void setRequestJson(String requestJson) {
		this.requestJson = requestJson;
	}

	public String getResponseJson() {
		return responseJson;
	}

	public void setResponseJson(String responseJson) {
		this.responseJson = responseJson;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getRequestIp() {
		return requestIp;
	}

	public void setRequestIp(String requestIp) {
		this.requestIp = requestIp;
	}

	public String getSourceCode() {
		return sourceCode;
	}

	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIsSync() {
		return isSync;
	}

	public void setIsSync(String isSync) {
		this.isSync = isSync;
	}
}
