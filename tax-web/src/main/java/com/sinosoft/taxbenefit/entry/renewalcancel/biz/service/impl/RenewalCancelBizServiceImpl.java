package com.sinosoft.taxbenefit.entry.renewalcancel.biz.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalcancel.RequestRenewalCancelDTO;
import com.sinosoft.taxbenefit.api.inter.RenewalCancelApiService;
import com.sinosoft.taxbenefit.entry.renewalcancel.biz.service.RenewalCancelBizService;
@Service
public class RenewalCancelBizServiceImpl implements RenewalCancelBizService{
	@Autowired
	RenewalCancelApiService renewalCancelApiService;
	
	@Override
	public ThirdSendResponseDTO renewalCancel(RequestRenewalCancelDTO requestRenewalCancelDTO,ParamHeadDTO paramHead) {
		return renewalCancelApiService.renewalCancel(requestRenewalCancelDTO, paramHead);
	}
}