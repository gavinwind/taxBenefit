package com.sinosoft.taxbenefit.entry.customer.biz.service;

import java.util.List;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqCustomerInfoDTO;

public interface CustomerVerifyBizService {
	/**
	 * 客户验证上传(单笔)
	 * @param paramHeadDTO
	 * @return
	 */
	ThirdSendResponseDTO CustomerVerify(ReqCustomerInfoDTO reqCustomerInfoDTO,ParamHeadDTO paramHeadDTO);
	/**
	 * 客户验证上传(多笔)
	 * @param reqCustomerInfoDTO
	 * @param paramHeadDTO
	 * @return
	 */
	ThirdSendResponseDTO CustomerMoreVerify(List<ReqCustomerInfoDTO> reqCustomerInfoDTO,ParamHeadDTO paramHeadDTO);
}