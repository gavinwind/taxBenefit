package com.sinosoft.taxbenefit.entry.common.service;

import com.sinosoft.taxbenefit.entry.business.dto.RequestServicePortDTO;


public interface CommonCodeService {
	
	/**
	 * 获取接口编码信息
	 * @param portCode
	 * @return
	 */
	RequestServicePortDTO queryTaxServerPortDTOByPortCode(String portCode);
	
	/**
	 * 根据字典编码和字典类型获取字典对应的内容
	 * @param sysCode 字典编码
	 * @param codeType 字典编码
	 * @return
	 */
	String queryCommonCodeInfo(String sysCode, String codeType);
}
