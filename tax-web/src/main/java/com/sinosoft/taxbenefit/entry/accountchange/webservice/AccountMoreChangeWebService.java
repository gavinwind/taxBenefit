package com.sinosoft.taxbenefit.entry.accountchange.webservice;

import javax.jws.WebService;

@WebService
public interface AccountMoreChangeWebService {
	/**
	 * 账户变更上传（多笔）
	 * @param requestJson
	 * @return
	 */
	public String dealMainBiz(String requestJson);
}