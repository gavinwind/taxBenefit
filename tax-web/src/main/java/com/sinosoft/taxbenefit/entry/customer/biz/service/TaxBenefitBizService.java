package com.sinosoft.taxbenefit.entry.customer.biz.service;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqTaxBeneVerifyInfoDTO;

public interface TaxBenefitBizService {
	/**
	 * 税优验证数据服务
	 * @param paramHeadDTO
	 * @return
	 */
	ThirdSendResponseDTO taxBenefit(ReqTaxBeneVerifyInfoDTO reqTaxBeneVerifyInfoDTO,ParamHeadDTO paramHeadDTO);
}
