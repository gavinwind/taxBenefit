package com.sinosoft.taxbenefit.entry.business.thread;

import java.util.Date;

import com.sinosoft.platform.common.util.DateUtil;
import com.sinosoft.taxbenefit.entry.business.dto.ReceiveLogDTO;
import com.sinosoft.taxbenefit.entry.generated.mapper.TaxRequestReceiveLogMapper;
import com.sinosoft.taxbenefit.entry.generated.model.TaxRequestReceiveLog;

/**
 * 【请求日志记录线程】
 * 
 * @Description: 日志记录线程
 * @author chenxin
 * @date 2016年1月29日 下午5:34:39
 * @version V1.0
 */
public class ReceiveLogThread implements Runnable {
	private TaxRequestReceiveLogMapper receiveLogMapper;
	private ReceiveLogDTO reportLogDTO;

	public ReceiveLogThread(ReceiveLogDTO reportLogDTO,
			TaxRequestReceiveLogMapper receiveLogMapper) {
		this.receiveLogMapper = receiveLogMapper;
		this.reportLogDTO = reportLogDTO;
	}

	@Override
	public void run() {
		Date current = DateUtil.getCurrentDate();
		TaxRequestReceiveLog report = new TaxRequestReceiveLog();
		// 接收请求持久化
		report.setBusinessType(reportLogDTO.getBusinessType());
		report.setCreateDate(current);
		report.setRequestContent(reportLogDTO.getRequestJson());
		report.setSourceIp(reportLogDTO.getRequestIp());	
		report.setRequestTime(current);
		report.setResponseContent(reportLogDTO.getResponseJson());
		report.setSerialNo(reportLogDTO.getSerialNo());
		report.setSourceCode(reportLogDTO.getSourceCode());
		report.setState(reportLogDTO.getState());
		report.setSerialNo(reportLogDTO.getSerialNo());
		report.setDescription(reportLogDTO.getDescription());
		report.setDisposeType(reportLogDTO.getIsSync());
		receiveLogMapper.insert(report);
	}
}
