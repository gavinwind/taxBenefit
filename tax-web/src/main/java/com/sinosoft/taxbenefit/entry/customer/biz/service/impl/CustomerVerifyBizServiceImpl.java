package com.sinosoft.taxbenefit.entry.customer.biz.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqCustomerInfoDTO;
import com.sinosoft.taxbenefit.api.inter.CustomerApiService;
import com.sinosoft.taxbenefit.entry.customer.biz.service.CustomerVerifyBizService;
/**
 * 客戶验证服务
 * @author zhangyu
 * @date 2016年3月6日 下午8:47:58
 */
@Service 
public class CustomerVerifyBizServiceImpl extends TaxBaseService implements CustomerVerifyBizService{
    @Autowired
	private CustomerApiService customerService;
	@Override
	public ThirdSendResponseDTO CustomerVerify(ReqCustomerInfoDTO reqCustomerInfoDTO,ParamHeadDTO paramHeadDTO) {
		//客戶验证服务不需要地区代码
		paramHeadDTO.setAreaCode("");
		return customerService.uploadCustomerInfo(reqCustomerInfoDTO,paramHeadDTO);
	}
	@Override
	public ThirdSendResponseDTO CustomerMoreVerify(
			List<ReqCustomerInfoDTO> reqCustomersInfoDTO,
			ParamHeadDTO paramHeadDTO) {
		//客戶验证服务不需要地区代码
		paramHeadDTO.setAreaCode("");
		return customerService.uploadCustomersInfo(reqCustomersInfoDTO, paramHeadDTO);
	}
}