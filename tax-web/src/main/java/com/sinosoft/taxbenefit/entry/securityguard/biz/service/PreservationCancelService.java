package com.sinosoft.taxbenefit.entry.securityguard.biz.service;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.securityguard.ReqEndorsementCancelInfoDTO;

public interface PreservationCancelService {
	public ThirdSendResponseDTO preservationCancel(ReqEndorsementCancelInfoDTO obj,ParamHeadDTO paramHeadDTO);

}
