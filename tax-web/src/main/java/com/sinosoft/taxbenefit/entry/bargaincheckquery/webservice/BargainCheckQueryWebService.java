package com.sinosoft.taxbenefit.entry.bargaincheckquery.webservice;

import javax.jws.WebService;

@WebService
public interface BargainCheckQueryWebService {
	/**
	 * 交易核对信息查询
	 * @param requestJson
	 * @return
	 */
	public String dealMainBiz(String requestJson);
}
