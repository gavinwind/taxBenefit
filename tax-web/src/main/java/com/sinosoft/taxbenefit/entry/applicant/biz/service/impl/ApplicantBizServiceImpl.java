package com.sinosoft.taxbenefit.entry.applicant.biz.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.taxbenefit.api.dto.request.applicant.ReqAppPolicyDTO;
import com.sinosoft.taxbenefit.api.dto.request.applicant.ReqPolicyCancelDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.inter.ApplicantApiService;
import com.sinosoft.taxbenefit.entry.applicant.biz.service.ApplicantBizService;
@Service
public class ApplicantBizServiceImpl extends TaxBaseService implements
		ApplicantBizService {
	@Autowired
	ApplicantApiService applicantApiService;
	@Override
	public ThirdSendResponseDTO addApplicant(List<ReqAppPolicyDTO> reqAppAcceptMores,ParamHeadDTO paramHeadDTO) {
		return applicantApiService.policyInforceService(reqAppAcceptMores,paramHeadDTO);
	}
	@Override
	public ThirdSendResponseDTO modifyPolicyState(ReqPolicyCancelDTO reqPolicyCancelDTO, ParamHeadDTO paramHeadDTO) {
		return applicantApiService.policyReversal(reqPolicyCancelDTO, paramHeadDTO);
	}

}
