package com.sinosoft.taxbenefit.entry.policystate.webservice;

import javax.jws.WebService;

@WebService
public interface PolicyStateChangeOneWebService {
	public String dealMainBiz(String requestJson);
}
