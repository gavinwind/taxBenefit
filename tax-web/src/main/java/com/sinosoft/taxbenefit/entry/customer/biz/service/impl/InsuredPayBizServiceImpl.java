package com.sinosoft.taxbenefit.entry.customer.biz.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqInsuredPayInfoDTO;
import com.sinosoft.taxbenefit.api.inter.InsuredPayApiService;
import com.sinosoft.taxbenefit.entry.customer.biz.service.InsuredPayBizService;
/**
 * 被保人既往理赔额查询
 * @author zhangyu
 * @date 2016年3月10日 上午9:55:20
 */
@Service
public class InsuredPayBizServiceImpl extends TaxBaseService implements InsuredPayBizService{
    @Autowired
	private InsuredPayApiService insuredPayServiceApi;
	@Override
	public ThirdSendResponseDTO InsuredPay(ReqInsuredPayInfoDTO reqInsuredPayInfoDTO,ParamHeadDTO paramHeadDTO) {
		return insuredPayServiceApi.InsuredPayInfo(reqInsuredPayInfoDTO,paramHeadDTO);
	}
}