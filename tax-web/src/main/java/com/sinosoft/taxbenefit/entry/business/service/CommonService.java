package com.sinosoft.taxbenefit.entry.business.service;

import com.sinosoft.platform.common.exception.BusinessDataErrException;

/**
 * 【通用功能服务】
 * 
 * @Description: 通用功能服务
 * @author chenxin
 * @date 2016年1月29日 下午4:06:09
 * @version V1.0
 */
public interface CommonService {
	/**
	 * 检查请求是否重复
	 * @Title: checkRepeatRequest
	 * @Description:<p>如果为重复交易，则抛出异常；如果为正常过交易，不做处理；重复交易的判断标准为业务流水号是否重复</p>
	 * @param source
	 * @param serialNo
	 */
	void checkRepeatRequest(String serialNo) throws BusinessDataErrException;
	/**
	 * 检查请求是否重复
	 * @Title: checkRepeatRequest
	 * @Description: 如果为重复交易，则查询自身系统结果
	 * @param source
	 * @param serialNo
	 */
	void checkAlikeRequest(String serialNo);
}
