package com.sinosoft.taxbenefit.entry.bargaincheckquery.biz.service;

import com.sinosoft.taxbenefit.api.dto.request.bargaincheckquery.ReqBargainCheckQueryDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;

public interface BargainCheckQueryBizSercvice {
	/**
	 * 交易核对信息查询
	 * @param reqBargainCheckQueryDTO
	 * @param paramHead
	 * @return
	 */
	ThirdSendResponseDTO bargainCheckQuery(ReqBargainCheckQueryDTO reqBargainCheckQueryDTO,ParamHeadDTO paramHead);
}
