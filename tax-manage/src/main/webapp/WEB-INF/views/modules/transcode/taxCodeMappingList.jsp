<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>平台转码管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/transcode/taxCodeMapping/">平台转码列表</a></li>
		<shiro:hasPermission name="transcode:taxCodeMapping:edit"><li><a href="${ctx}/transcode/taxCodeMapping/form">平台转码添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="taxCodeMapping" action="${ctx}/transcode/taxCodeMapping/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>转换业务码：</label>
				<form:input path="codeType" htmlEscape="false" maxlength="50" class="input-medium"/>
			</li>
			<li><label>原始码：</label>
				<form:input path="originalCode" htmlEscape="false" maxlength="30" class="input-medium"/>
			</li>
			<li><label>转换码：</label>
				<form:input path="targetCode" htmlEscape="false" maxlength="30" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th style="text-align:center;">转码类型</th><th style="text-align:center;">转码类型说明</th>
				<th style="text-align:center;">核心编码</th><th style="text-align:center;">核心编码说明</th>
				<th style="text-align:center;">中保信编码</th><th style="text-align:center;">中保信编码说明</th>
				<shiro:hasPermission name="transcode:taxCodeMapping:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="taxCodeMapping">
			<tr>
				<td style="text-align:center;"><a href="${ctx}/transcode/taxCodeMapping/form?sid=${taxCodeMapping.sid}">${taxCodeMapping.codeType}</td>
				<td style="text-align:center;">${taxCodeMapping.remark}</td>
				<td style="text-align:center;">${taxCodeMapping.originalCode}</td>
				<td style="text-align:center;">${taxCodeMapping.originalDesc}</td>
				<td style="text-align:center;">${taxCodeMapping.targetCode}</td>
				<td style="text-align:center;">${taxCodeMapping.targetDesc}</td>
				<shiro:hasPermission name="transcode:taxCodeMapping:edit"><td>
    				<a href="${ctx}/transcode/taxCodeMapping/form?sid=${taxCodeMapping.sid}">修改</a>
					<a href="${ctx}/transcode/taxCodeMapping/delete?sid=${taxCodeMapping.sid}" onclick="return confirmx('确认要删除该平台转码吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>