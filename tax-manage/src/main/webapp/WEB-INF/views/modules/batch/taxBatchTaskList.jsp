<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>批处理任务记录管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/batch/taxBatchTask/">批处理任务记录列表</a></li>
		<shiro:hasPermission name="batch:taxBatchTask:edit"><li><a href="${ctx}/batch/taxBatchTask/form">批处理任务记录添加</a></li></shiro:hasPermission>
	</ul>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr><th style="text-align:center;">批处理名称</th><th style="text-align:center;">触发时间</th><th style="text-align:center;">批处理调用类名</th><th style="text-align:center;">批处理调用方法名</th><th style="text-align:center;">调用状态</th><th style="text-align:center;">批处理任务描述</th><shiro:hasPermission name="batch:taxBatchTask:edit"><th>操作</th></shiro:hasPermission></tr>
		</thead>
		<tbody>
		<c:forEach items="${list}" var="taxBatchTask">
			<tr>
				<td style="text-align:center;"><a href="${ctx}/batch/taxBatchTask/form?sid=${taxBatchTask.sid}">${taxBatchTask.taskName}</td>
				<td style="text-align:center;">${taxBatchTask.executeTime}</td>
				<td style="text-align:center;">${taxBatchTask.dealClassName}</td>
				<td style="text-align:center;">${taxBatchTask.dealMethodName}</td>
				<td style="text-align:center;">${taxBatchTask.batchType eq '01'?'启用':'停用'}</td>
				<td style="text-align:center;">${taxBatchTask.description}</td>
				<shiro:hasPermission name="batch:taxBatchTask:edit"><td>
    				<a href="${ctx}/batch/taxBatchTask/form?sid=${taxBatchTask.sid}">修改</a>
					<a href="${ctx}/batch/taxBatchTask/delete?sid=${taxBatchTask.sid}" onclick="return confirmx('确认要删除该批处理任务记录吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
</body>
</html>