<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>请求交易核对记录管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/business/taxRequestBusinessCollate/">请求交易核对记录列表</a></li>
		<li class="active"><a href="${ctx}/business/taxRequestBusinessCollate/form?sid=${taxRequestBusinessCollate.sid}">请求交易核对记录<shiro:hasPermission name="business:taxRequestBusinessCollate:edit">${not empty taxRequestBusinessCollate.sid?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="business:taxRequestBusinessCollate:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="taxRequestBusinessCollate" action="${ctx}/business/taxRequestBusinessCollate/save" method="post" class="form-horizontal">
		<form:hidden path="sid"/>
		<sys:message content="${message}"/>		
		<div class="control-group">
			<label class="control-label">交易时间：</label>
			<div class="controls">
				<input name="businessDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate "
					value="<fmt:formatDate value="${taxRequestBusinessCollate.businessDate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">核心请求记录总数：</label>
			<div class="controls">
				<form:input path="requetNum" htmlEscape="false" maxlength="8" class="input-xlarge  digits"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">核心请求记录成功数：</label>
			<div class="controls">
				<form:input path="reqSuccessNum" htmlEscape="false" maxlength="8" class="input-xlarge  digits"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">核心请求记录失败数：</label>
			<div class="controls">
				<form:input path="reqFailNum" htmlEscape="false" maxlength="8" class="input-xlarge  digits"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">交易核对记录总数：</label>
			<div class="controls">
				<form:input path="collateNum" htmlEscape="false" maxlength="8" class="input-xlarge  digits"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">交易核对记录成功数：</label>
			<div class="controls">
				<form:input path="colSuccessNum" htmlEscape="false" maxlength="8" class="input-xlarge  digits"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">交易核对记录失败数：</label>
			<div class="controls">
				<form:input path="colFailNum" htmlEscape="false" maxlength="8" class="input-xlarge  digits"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">成功标识：</label>
			<div class="controls">
				<form:input path="successFlag" htmlEscape="false" maxlength="10" class="input-xlarge "/>
			</div>
		</div>
		<%-- <div class="control-group">
			<label class="control-label">update_id：</label>
			<div class="controls">
				<form:input path="updateId" htmlEscape="false" maxlength="8" class="input-xlarge  digits"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">update_time：</label>
			<div class="controls">
				<input name="updateTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate "
					value="<fmt:formatDate value="${taxRequestBusinessCollate.updateTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</div>
		</div> --%>
		<div class="control-group">
			<label class="control-label">remark：</label>
			<div class="controls">
				<form:input path="remark" htmlEscape="false" maxlength="400" class="input-xlarge "/>
			</div>
		</div>
		<div class="form-actions">
			<shiro:hasPermission name="business:taxRequestBusinessCollate:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>