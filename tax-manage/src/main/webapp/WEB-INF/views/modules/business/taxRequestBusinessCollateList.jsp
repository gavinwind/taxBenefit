<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>请求交易核对记录管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/business/taxRequestBusinessCollate/">请求交易核对记录列表</a></li>
		<shiro:hasPermission name="business:taxRequestBusinessCollate:edit"><li><a href="${ctx}/business/taxRequestBusinessCollate/form">请求交易核对记录添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="taxRequestBusinessCollate" action="${ctx}/business/taxRequestBusinessCollate/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>交易时间：</label>
				<input name="beginBusinessDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${taxRequestBusinessCollate.beginBusinessDate}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/> - 
				<input name="endBusinessDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${taxRequestBusinessCollate.endBusinessDate}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th style="text-align:center;">交易日时间</th><th style="text-align:center;">请求记录数</th>
				<th style="text-align:center;">请求成功数</th><th style="text-align:center;">请求失败数</th>
				<th style="text-align:center;">核对记录数</th><th style="text-align:center;">核对成功数</th>
				<th style="text-align:center;">核对失败数</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="taxRequestBusinessCollate">
			<tr>
				<td style="text-align:center;"><fmt:formatDate  value="${taxRequestBusinessCollate.businessDate}" type="date" /></td>
				<td style="text-align:center;">${taxRequestBusinessCollate.requetNum}</td>
				<td style="text-align:center;">${taxRequestBusinessCollate.reqSuccessNum}</td>
				<td style="text-align:center;">${taxRequestBusinessCollate.reqFailNum}</td>
				<td style="text-align:center;">${taxRequestBusinessCollate.collateNum}</td>
				<td style="text-align:center;">${taxRequestBusinessCollate.colSuccessNum}</td>
				<td style="text-align:center;">${taxRequestBusinessCollate.colFailNum}</td>
				<%-- <shiro:hasPermission name="business:taxRequestBusinessCollate:edit"><td>
    				<a href="${ctx}/business/taxRequestBusinessCollate/form?sid=${taxRequestBusinessCollate.sid}">修改</a>
					<a href="${ctx}/business/taxRequestBusinessCollate/delete?sid=${taxRequestBusinessCollate.sid}" onclick="return confirmx('确认要删除该请求交易核对记录吗？', this.href)">删除</a>
				</td></shiro:hasPermission> --%>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>