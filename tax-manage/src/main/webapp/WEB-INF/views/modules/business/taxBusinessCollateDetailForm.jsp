<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>交易核对明细管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/business/taxBusinessCollateDetail/">交易核对明细列表</a></li>
		<li class="active"><a href="${ctx}/business/taxBusinessCollateDetail/form?sid=${taxBusinessCollateDetail.sid}">交易核对明细<shiro:hasPermission name="business:taxBusinessCollateDetail:edit">${not empty taxBusinessCollateDetail.sid?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="business:taxBusinessCollateDetail:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="taxBusinessCollateDetail" action="${ctx}/business/taxBusinessCollateDetail/save" method="post" class="form-horizontal">
		<form:hidden path="sid"/>
		<sys:message content="${message}"/>		
		<div class="control-group">
			<label class="control-label">交易编号：</label>
			<div class="controls">
				<form:input path="busId" htmlEscape="false" maxlength="8" class="input-xlarge  digits"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">保单号码：</label>
			<div class="controls">
				<form:input path="policyNo" htmlEscape="false" maxlength="30" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">分单号码：</label>
			<div class="controls">
				<form:input path="sequenceNo" htmlEscape="false" maxlength="30" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">保全批单号：</label>
			<div class="controls">
				<form:input path="edorNo" htmlEscape="false" maxlength="30" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">理赔赔案号：</label>
			<div class="controls">
				<form:input path="claimNo" htmlEscape="false" maxlength="30" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">费用编码：</label>
			<div class="controls">
				<form:input path="renewFeeNo" htmlEscape="false" maxlength="20" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">保费状态：</label>
			<div class="controls">
				<form:input path="feeStatus" htmlEscape="false" maxlength="10" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">续保批单号：</label>
			<div class="controls">
				<form:input path="renewEndorsementNo" htmlEscape="false" maxlength="30" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">公司费用编号：</label>
			<div class="controls">
				<form:input path="feeId" htmlEscape="false" maxlength="30" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">交易时间：</label>
			<div class="controls">
				<input name="createTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate "
					value="<fmt:formatDate value="${taxBusinessCollateDetail.createTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</div>
		</div>
		<div class="form-actions">
			<shiro:hasPermission name="business:taxBusinessCollateDetail:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>