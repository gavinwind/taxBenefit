<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>交易核对明细管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/business/taxBusinessCollateDetail/">交易核对明细列表</a></li>
		<shiro:hasPermission name="business:taxBusinessCollateDetail:edit"><li><a href="${ctx}/business/taxBusinessCollateDetail/form">交易核对明细添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="taxBusinessCollateDetail" action="${ctx}/business/taxBusinessCollateDetail/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>交易时间：</label>
				<input name="createTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${taxBusinessCollateDetail.createTime}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th style="text-align:center;">交易日时间</th>
				<th style="text-align:center;">保单号码</th><th style="text-align:center;">分单号码</th>
				<th style="text-align:center;">保全批单号</th><th style="text-align:center;">理赔赔案号</th>
				<th style="text-align:center;">费用编码</th><th style="text-align:center;">保费状态</th>
				<th style="text-align:center;">续保批单号</th><th style="text-align:center;">公司费用编号</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="taxBusinessCollateDetail">
			<tr>
				<td style="text-align:center;"><fmt:formatDate  value="${taxBusinessCollateDetail.createTime}" type="date" /></td>
				<td style="text-align:center;">${taxBusinessCollateDetail.policyNo}</td>
				<td style="text-align:center;">${taxBusinessCollateDetail.sequenceNo}</td>
				<td style="text-align:center;">${taxBusinessCollateDetail.edorNo}</td>
				<td style="text-align:center;">${taxBusinessCollateDetail.claimNo}</td>
				<td style="text-align:center;">${taxBusinessCollateDetail.renewFeeNo}</td>
				<td style="text-align:center;">${taxBusinessCollateDetail.feeStatus}</td>
				<td style="text-align:center;">${taxBusinessCollateDetail.renewEndorsementNo}</td>
				<td style="text-align:center;">${taxBusinessCollateDetail.feeId}</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>