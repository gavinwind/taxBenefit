<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>请求记录明细管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/business/taxRequestBusinessDetail/">请求记录明细列表</a></li>
		<shiro:hasPermission name="business:taxRequestBusinessDetail:edit"><li><a href="${ctx}/business/taxRequestBusinessDetail/form">请求记录明细添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="taxRequestBusinessDetail" action="${ctx}/business/taxRequestBusinessDetail/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>交易时间：</label>
				<input name="businessDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${taxRequestBusinessDetail.businessDate}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th style="text-align:center;">交易日时间</th>
				<th style="text-align:center;">交易流水号</th><th style="text-align:center;">接口编码</th>
				<th style="text-align:center;">处理结果</th><th style="text-align:center;">结果描述</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="taxRequestBusinessDetail">
			<tr>
				<td style="text-align:center; width:15%"><fmt:formatDate  value="${taxRequestBusinessDetail.businessDate}" type="date" /></td>
				<td style="text-align:center; width:20%">${taxRequestBusinessDetail.serialNo}</td>
				<td style="text-align:center; width:15%">${taxRequestBusinessDetail.portCode}</td>
				<td style="text-align:center; width:15%">${taxRequestBusinessDetail.resultCode}</td>
				<td style="text-align:center;">${taxRequestBusinessDetail.resultMessage}</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>