/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.business.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.business.entity.TaxBusinessCollateDetail;
import com.thinkgem.jeesite.modules.business.service.TaxBusinessCollateDetailService;

/**
 * 交易核对明细Controller
 * @author shechunming
 * @version 2016-08-15
 */
@Controller
@RequestMapping(value = "${adminPath}/business/taxBusinessCollateDetail")
public class TaxBusinessCollateDetailController extends BaseController {

	@Autowired
	private TaxBusinessCollateDetailService taxBusinessCollateDetailService;
	
	@ModelAttribute
	public TaxBusinessCollateDetail get(@RequestParam(required=false) String id) {
		TaxBusinessCollateDetail entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = taxBusinessCollateDetailService.get(id);
		}
		if (entity == null){
			entity = new TaxBusinessCollateDetail();
		}
		return entity;
	}
	
	@RequiresPermissions("business:taxBusinessCollateDetail:view")
	@RequestMapping(value = {"list", ""})
	public String list(TaxBusinessCollateDetail taxBusinessCollateDetail, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TaxBusinessCollateDetail> page = taxBusinessCollateDetailService.findPage(new Page<TaxBusinessCollateDetail>(request, response), taxBusinessCollateDetail); 
		model.addAttribute("page", page);
		return "modules/business/taxBusinessCollateDetailList";
	}

	@RequiresPermissions("business:taxBusinessCollateDetail:view")
	@RequestMapping(value = "form")
	public String form(TaxBusinessCollateDetail taxBusinessCollateDetail, Model model) {
		model.addAttribute("taxBusinessCollateDetail", taxBusinessCollateDetail);
		return "modules/business/taxBusinessCollateDetailForm";
	}

	@RequiresPermissions("business:taxBusinessCollateDetail:edit")
	@RequestMapping(value = "save")
	public String save(TaxBusinessCollateDetail taxBusinessCollateDetail, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, taxBusinessCollateDetail)){
			return form(taxBusinessCollateDetail, model);
		}
		taxBusinessCollateDetailService.save(taxBusinessCollateDetail);
		addMessage(redirectAttributes, "保存交易核对明细成功");
		return "redirect:"+Global.getAdminPath()+"/business/taxBusinessCollateDetail/?repage";
	}
	
	@RequiresPermissions("business:taxBusinessCollateDetail:edit")
	@RequestMapping(value = "delete")
	public String delete(TaxBusinessCollateDetail taxBusinessCollateDetail, RedirectAttributes redirectAttributes) {
		taxBusinessCollateDetailService.delete(taxBusinessCollateDetail);
		addMessage(redirectAttributes, "删除交易核对明细成功");
		return "redirect:"+Global.getAdminPath()+"/business/taxBusinessCollateDetail/?repage";
	}

}