/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.business.entity;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.thinkgem.jeesite.common.persistence.DataEntity;
import com.thinkgem.jeesite.modules.business.service.TaxBusinessCollateDetailService;

/**
 * 交易核对明细Entity
 * @author shechunming
 * @version 2016-08-15
 */
public class TaxBusinessCollateDetail extends DataEntity<TaxBusinessCollateDetail> {
	
	private static final long serialVersionUID = 1L;
	private Integer sid;		// 编号
	private Integer busId;		// 交易编号
	private String policyNo;		// 保单号码
	private String sequenceNo;		// 分单号码
	private String edorNo;		// 保全批单号
	private String claimNo;		// 理赔赔案号
	private String renewFeeNo;		// 费用编码
	private String feeStatus;		// 保费状态
	private String renewEndorsementNo;		// 续保批单号
	private String feeId;		// 公司费用编号
	private Integer createId;		// create_id
	private Date createTime;		// create_time
	private String remark;		// remark
	private TaxBusinessCollateDetailService businessCollateDetailService;
	
	public TaxBusinessCollateDetail() {
		super();
	}
	public TaxBusinessCollateDetail(TaxBusinessCollateDetailService businessCollateDetailService) {
		super();
		this.businessCollateDetailService = businessCollateDetailService;
	}

	public TaxBusinessCollateDetail(String id){
		super(id);
	}

	public Integer getBusId() {
		return busId;
	}

	public void setBusId(Integer busId) {
		this.busId = busId;
	}
	
	@Length(min=0, max=30, message="保单号码长度必须介于 0 和 30 之间")
	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	
	@Length(min=0, max=30, message="分单号码长度必须介于 0 和 30 之间")
	public String getSequenceNo() {
		return sequenceNo;
	}

	public void setSequenceNo(String sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
	
	@Length(min=0, max=30, message="保全批单号长度必须介于 0 和 30 之间")
	public String getEdorNo() {
		return edorNo;
	}

	public void setEdorNo(String edorNo) {
		this.edorNo = edorNo;
	}
	
	@Length(min=0, max=30, message="理赔赔案号长度必须介于 0 和 30 之间")
	public String getClaimNo() {
		return claimNo;
	}

	public void setClaimNo(String claimNo) {
		this.claimNo = claimNo;
	}
	
	@Length(min=0, max=20, message="费用编码长度必须介于 0 和 20 之间")
	public String getRenewFeeNo() {
		return renewFeeNo;
	}

	public void setRenewFeeNo(String renewFeeNo) {
		this.renewFeeNo = renewFeeNo;
	}
	
	@Length(min=0, max=10, message="保费状态长度必须介于 0 和 10 之间")
	public String getFeeStatus() {
		return feeStatus;
	}

	public void setFeeStatus(String feeStatus) {
		this.feeStatus = feeStatus;
	}
	
	@Length(min=0, max=30, message="续保批单号长度必须介于 0 和 30 之间")
	public String getRenewEndorsementNo() {
		return renewEndorsementNo;
	}

	public void setRenewEndorsementNo(String renewEndorsementNo) {
		this.renewEndorsementNo = renewEndorsementNo;
	}
	
	@Length(min=0, max=30, message="公司费用编号长度必须介于 0 和 30 之间")
	public String getFeeId() {
		return feeId;
	}

	public void setFeeId(String feeId) {
		this.feeId = feeId;
	}
	
	public Integer getCreateId() {
		return createId;
	}

	public void setCreateId(Integer createId) {
		this.createId = createId;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	@Length(min=0, max=400, message="remark长度必须介于 0 和 400 之间")
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	protected void cerateSid() {
		setSid(businessCollateDetailService.getSequence());
	}
	
}