/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.business.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.business.entity.TaxRequestBusinessCollate;
import com.thinkgem.jeesite.modules.business.service.TaxRequestBusinessCollateService;

/**
 * 请求交易核对记录Controller
 * @author shechunming
 * @version 2016-08-14
 */
@Controller
@RequestMapping(value = "${adminPath}/business/taxRequestBusinessCollate")
public class TaxRequestBusinessCollateController extends BaseController {

	@Autowired
	private TaxRequestBusinessCollateService taxRequestBusinessCollateService;
	
	@ModelAttribute
	public TaxRequestBusinessCollate get(@RequestParam(required=false) String sid) {
		TaxRequestBusinessCollate entity = null;
		if (StringUtils.isNotBlank(sid)){
			entity = taxRequestBusinessCollateService.get(sid);
		}
		if (entity == null){
			entity = new TaxRequestBusinessCollate();
		}
		return entity;
	}
	
	@RequiresPermissions("business:taxRequestBusinessCollate:view")
	@RequestMapping(value = {"list", ""})
	public String list(TaxRequestBusinessCollate taxRequestBusinessCollate, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TaxRequestBusinessCollate> page = taxRequestBusinessCollateService.findPage(new Page<TaxRequestBusinessCollate>(request, response), taxRequestBusinessCollate); 
		model.addAttribute("page", page);
		return "modules/business/taxRequestBusinessCollateList";
	}

	@RequiresPermissions("business:taxRequestBusinessCollate:view")
	@RequestMapping(value = "form")
	public String form(TaxRequestBusinessCollate taxRequestBusinessCollate, Model model) {
		model.addAttribute("taxRequestBusinessCollate", taxRequestBusinessCollate);
		return "modules/business/taxRequestBusinessCollateForm";
	}

	@RequiresPermissions("business:taxRequestBusinessCollate:edit")
	@RequestMapping(value = "save")
	public String save(TaxRequestBusinessCollate taxRequestBusinessCollate, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, taxRequestBusinessCollate)){
			return form(taxRequestBusinessCollate, model);
		}
		taxRequestBusinessCollateService.save(taxRequestBusinessCollate);
		addMessage(redirectAttributes, "保存请求交易核对记录成功");
		return "redirect:"+Global.getAdminPath()+"/business/taxRequestBusinessCollate/?repage";
	}
	
	@RequiresPermissions("business:taxRequestBusinessCollate:edit")
	@RequestMapping(value = "delete")
	public String delete(TaxRequestBusinessCollate taxRequestBusinessCollate, RedirectAttributes redirectAttributes) {
		taxRequestBusinessCollateService.delete(taxRequestBusinessCollate);
		addMessage(redirectAttributes, "删除请求交易核对记录成功");
		return "redirect:"+Global.getAdminPath()+"/business/taxRequestBusinessCollate/?repage";
	}

}