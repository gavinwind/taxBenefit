/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.business.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.business.entity.TaxRequestBusinessDetail;
import com.thinkgem.jeesite.modules.business.service.TaxRequestBusinessDetailService;

/**
 * 请求记录明细Controller
 * @author shechunming
 * @version 2016-08-15
 */
@Controller
@RequestMapping(value = "${adminPath}/business/taxRequestBusinessDetail")
public class TaxRequestBusinessDetailController extends BaseController {

	@Autowired
	private TaxRequestBusinessDetailService taxRequestBusinessDetailService;
	
	@ModelAttribute
	public TaxRequestBusinessDetail get(@RequestParam(required=false) String id) {
		TaxRequestBusinessDetail entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = taxRequestBusinessDetailService.get(id);
		}
		if (entity == null){
			entity = new TaxRequestBusinessDetail();
		}
		return entity;
	}
	
	@RequiresPermissions("business:taxRequestBusinessDetail:view")
	@RequestMapping(value = {"list", ""})
	public String list(TaxRequestBusinessDetail taxRequestBusinessDetail, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TaxRequestBusinessDetail> page = taxRequestBusinessDetailService.findPage(new Page<TaxRequestBusinessDetail>(request, response), taxRequestBusinessDetail); 
		model.addAttribute("page", page);
		return "modules/business/taxRequestBusinessDetailList";
	}

	@RequiresPermissions("business:taxRequestBusinessDetail:view")
	@RequestMapping(value = "form")
	public String form(TaxRequestBusinessDetail taxRequestBusinessDetail, Model model) {
		model.addAttribute("taxRequestBusinessDetail", taxRequestBusinessDetail);
		return "modules/business/taxRequestBusinessDetailForm";
	}

	@RequiresPermissions("business:taxRequestBusinessDetail:edit")
	@RequestMapping(value = "save")
	public String save(TaxRequestBusinessDetail taxRequestBusinessDetail, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, taxRequestBusinessDetail)){
			return form(taxRequestBusinessDetail, model);
		}
		taxRequestBusinessDetailService.save(taxRequestBusinessDetail);
		addMessage(redirectAttributes, "保存请求记录明细成功");
		return "redirect:"+Global.getAdminPath()+"/business/taxRequestBusinessDetail/?repage";
	}
	
	@RequiresPermissions("business:taxRequestBusinessDetail:edit")
	@RequestMapping(value = "delete")
	public String delete(TaxRequestBusinessDetail taxRequestBusinessDetail, RedirectAttributes redirectAttributes) {
		taxRequestBusinessDetailService.delete(taxRequestBusinessDetail);
		addMessage(redirectAttributes, "删除请求记录明细成功");
		return "redirect:"+Global.getAdminPath()+"/business/taxRequestBusinessDetail/?repage";
	}

}