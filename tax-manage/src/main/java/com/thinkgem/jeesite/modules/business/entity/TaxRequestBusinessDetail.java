/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.business.entity;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.thinkgem.jeesite.common.persistence.DataEntity;
import com.thinkgem.jeesite.modules.business.service.TaxRequestBusinessDetailService;

/**
 * 请求记录明细Entity
 * @author shechunming
 * @version 2016-08-15
 */
public class TaxRequestBusinessDetail extends DataEntity<TaxRequestBusinessDetail> {
	
	private static final long serialVersionUID = 1L;
	private String serialNo;		// 水流号
	private String portCode;		// 请求编码
	private String resultCode;		// 交易结果
	private String resultMessage;		// 失败信息描述
	private Date businessDate;		// 交易时间
	private TaxRequestBusinessDetailService requestBusinessDetailService;
	
	public TaxRequestBusinessDetail() {
		super();
	}
	
	public TaxRequestBusinessDetail(TaxRequestBusinessDetailService requestBusinessDetailService) {
		super();
		this.requestBusinessDetailService = requestBusinessDetailService;
	}

	public TaxRequestBusinessDetail(String sid){
		super(sid);
	}
	
	@Length(min=0, max=40, message="水流号长度必须介于 0 和 40 之间")
	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	
	@Length(min=0, max=30, message="请求编码长度必须介于 0 和 30 之间")
	public String getPortCode() {
		return portCode;
	}

	public void setPortCode(String portCode) {
		this.portCode = portCode;
	}
	
	@Length(min=0, max=10, message="交易结果长度必须介于 0 和 10 之间")
	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	
	@Length(min=0, max=2000, message="失败信息描述长度必须介于 0 和 2000 之间")
	public String getResultMessage() {
		return resultMessage;
	}

	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getBusinessDate() {
		return businessDate;
	}

	public void setBusinessDate(Date businessDate) {
		this.businessDate = businessDate;
	}

	@Override
	protected void cerateSid() {
		setSid(requestBusinessDetailService.getSequence());
	}
	
}