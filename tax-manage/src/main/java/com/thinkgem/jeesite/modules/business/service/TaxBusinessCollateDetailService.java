/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.business.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.batch.dao.TaxBatchTaskDao;
import com.thinkgem.jeesite.modules.business.entity.TaxBusinessCollateDetail;
import com.thinkgem.jeesite.modules.business.dao.TaxBusinessCollateDetailDao;

/**
 * 交易核对明细Service
 * @author shechunming
 * @version 2016-08-15
 */
@Service
@Transactional(readOnly = true)
public class TaxBusinessCollateDetailService extends CrudService<TaxBusinessCollateDetailDao, TaxBusinessCollateDetail> {

	@Autowired
	protected TaxBusinessCollateDetailDao businessCollateDetailDao;
	
	public TaxBusinessCollateDetail get(String id) {
		return super.get(id);
	}
	
	public List<TaxBusinessCollateDetail> findList(TaxBusinessCollateDetail taxBusinessCollateDetail) {
		return super.findList(taxBusinessCollateDetail);
	}
	
	public Page<TaxBusinessCollateDetail> findPage(Page<TaxBusinessCollateDetail> page, TaxBusinessCollateDetail taxBusinessCollateDetail) {
		return super.findPage(page, taxBusinessCollateDetail);
	}
	
	@Transactional(readOnly = false)
	public void save(TaxBusinessCollateDetail taxBusinessCollateDetail) {
		super.save(taxBusinessCollateDetail);
	}
	
	@Transactional(readOnly = false)
	public void delete(TaxBusinessCollateDetail taxBusinessCollateDetail) {
		super.delete(taxBusinessCollateDetail);
	}
	public String getSequence(){
		return businessCollateDetailDao.getSequence()+"";
	}
}