/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.batch.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.batch.entity.TaxBatchTask;
import com.thinkgem.jeesite.modules.batch.dao.TaxBatchTaskDao;
import com.thinkgem.jeesite.modules.transcode.dao.TaxCodeMappingDao;

/**
 * 批处理任务记录Service
 * @author shechunming
 * @version 2016-08-14
 */
@Service
@Transactional(readOnly = true)
public class TaxBatchTaskService extends CrudService<TaxBatchTaskDao, TaxBatchTask> {

	@Autowired
	protected TaxBatchTaskDao batchTaskDao;
	
	public TaxBatchTask get(String sid) {
		return super.get(sid);
	}
	
	public List<TaxBatchTask> findList(TaxBatchTask taxBatchTask) {
		return super.findList(taxBatchTask);
	}
	
	public Page<TaxBatchTask> findPage(Page<TaxBatchTask> page, TaxBatchTask taxBatchTask) {
		return super.findPage(page, taxBatchTask);
	}
	
	@Transactional(readOnly = false)
	public void save(TaxBatchTask taxBatchTask) {
		super.save(taxBatchTask);
	}
	
	@Transactional(readOnly = false)
	public void delete(TaxBatchTask taxBatchTask) {
		super.delete(taxBatchTask);
	}
	public String getSequence(){
		return batchTaskDao.getSequence()+"";
	}
}