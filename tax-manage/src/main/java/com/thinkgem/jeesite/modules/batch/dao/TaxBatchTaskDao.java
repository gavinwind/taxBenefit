/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.batch.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.batch.entity.TaxBatchTask;

/**
 * 批处理任务记录DAO接口
 * @author shechunming
 * @version 2016-08-14
 */
@MyBatisDao
public interface TaxBatchTaskDao extends CrudDao<TaxBatchTask> {
	/**
	 * 获取CodeMap 序列号
	 * @param sid
	 * @return
	 */
	public int getSequence();
}