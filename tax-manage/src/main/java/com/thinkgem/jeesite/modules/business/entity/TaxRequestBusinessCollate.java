/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.business.entity;

import java.util.Date;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.thinkgem.jeesite.common.persistence.DataEntity;
import com.thinkgem.jeesite.modules.business.service.TaxRequestBusinessCollateService;

/**
 * 请求交易核对记录Entity
 * @author shechunming
 * @version 2016-08-14
 */
public class TaxRequestBusinessCollate extends DataEntity<TaxRequestBusinessCollate> {
	
	private static final long serialVersionUID = 1L;
	private Date businessDate;		// 交易时间
	private Integer requetNum;		// 核心请求记录总数
	private Integer reqSuccessNum;		// 核心请求记录成功数
	private Integer reqFailNum;		// 核心请求记录失败数
	private Integer collateNum;		// 交易核对记录总数
	private Integer colSuccessNum;		// 交易核对记录成功数
	private Integer colFailNum;		// 交易核对记录失败数
	private String successFlag;		// 成功标识
	private Integer createId;		// create_id
	private Date createTime;		// create_time
	private Integer updateId;		// update_id
	private Date updateTime;		// update_time
	private String remark;		// remark
	private Date beginBusinessDate;		// 开始 交易时间
	private Date endBusinessDate;		// 结束 交易时间
	private TaxRequestBusinessCollateService requestBusinessCollateService;
	
	public TaxRequestBusinessCollate() {
		super();
	}
	
	public TaxRequestBusinessCollate(TaxRequestBusinessCollateService requestBusinessCollateService) {
		super();
		this.requestBusinessCollateService = requestBusinessCollateService;
	}

	public TaxRequestBusinessCollate(String sid){
		super(sid);
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getBusinessDate() {
		return businessDate;
	}

	public void setBusinessDate(Date businessDate) {
		this.businessDate = businessDate;
	}
	
	public Integer getRequetNum() {
		return requetNum;
	}

	public void setRequetNum(Integer requetNum) {
		this.requetNum = requetNum;
	}
	
	public Integer getReqSuccessNum() {
		return reqSuccessNum;
	}

	public void setReqSuccessNum(Integer reqSuccessNum) {
		this.reqSuccessNum = reqSuccessNum;
	}
	
	public Integer getReqFailNum() {
		return reqFailNum;
	}

	public void setReqFailNum(Integer reqFailNum) {
		this.reqFailNum = reqFailNum;
	}
	
	public Integer getCollateNum() {
		return collateNum;
	}

	public void setCollateNum(Integer collateNum) {
		this.collateNum = collateNum;
	}
	
	public Integer getColSuccessNum() {
		return colSuccessNum;
	}

	public void setColSuccessNum(Integer colSuccessNum) {
		this.colSuccessNum = colSuccessNum;
	}
	
	public Integer getColFailNum() {
		return colFailNum;
	}

	public void setColFailNum(Integer colFailNum) {
		this.colFailNum = colFailNum;
	}
	
	@Length(min=0, max=10, message="成功标识长度必须介于 0 和 10 之间")
	public String getSuccessFlag() {
		return successFlag;
	}

	public void setSuccessFlag(String successFlag) {
		this.successFlag = successFlag;
	}
	
	public Integer getCreateId() {
		return createId;
	}

	public void setCreateId(Integer createId) {
		this.createId = createId;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	public Integer getUpdateId() {
		return updateId;
	}

	public void setUpdateId(Integer updateId) {
		this.updateId = updateId;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
	@Length(min=0, max=400, message="remark长度必须介于 0 和 400 之间")
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	public Date getBeginBusinessDate() {
		return beginBusinessDate;
	}

	public void setBeginBusinessDate(Date beginBusinessDate) {
		this.beginBusinessDate = beginBusinessDate;
	}
	
	public Date getEndBusinessDate() {
		return endBusinessDate;
	}

	public void setEndBusinessDate(Date endBusinessDate) {
		this.endBusinessDate = endBusinessDate;
	}

	@Override
	protected void cerateSid() {
		setSid(requestBusinessCollateService.getSequence());
	}
}