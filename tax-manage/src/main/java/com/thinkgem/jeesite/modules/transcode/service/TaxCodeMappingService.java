/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.transcode.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.transcode.entity.TaxCodeMapping;
import com.thinkgem.jeesite.modules.transcode.dao.TaxCodeMappingDao;

/**
 * 平台转码Service
 * @author shechunming
 * @version 2016-08-14
 */
@Service
@Transactional(readOnly = true)
public class TaxCodeMappingService extends CrudService<TaxCodeMappingDao, TaxCodeMapping> {

	@Autowired
	protected TaxCodeMappingDao codeMapDao;
	
	public TaxCodeMapping get(String id) {
		return super.get(id);
	}
	
	public List<TaxCodeMapping> findList(TaxCodeMapping taxCodeMapping) {
		return super.findList(taxCodeMapping);
	}
	
	public Page<TaxCodeMapping> findPage(Page<TaxCodeMapping> page, TaxCodeMapping taxCodeMapping) {
		return super.findPage(page, taxCodeMapping);
	}
	
	@Transactional(readOnly = false)
	public void save(TaxCodeMapping taxCodeMapping) {
		super.save(taxCodeMapping);
	}
	
	@Transactional(readOnly = false)
	public void delete(TaxCodeMapping taxCodeMapping) {
		super.delete(taxCodeMapping);
	}
	
	public String getSequence(){
		return codeMapDao.getSequence()+"";
	}
	
}