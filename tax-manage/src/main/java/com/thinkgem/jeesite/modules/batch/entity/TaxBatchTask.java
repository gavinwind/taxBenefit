/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.batch.entity;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;
import com.thinkgem.jeesite.modules.batch.service.TaxBatchTaskService;

/**
 * 批处理任务记录Entity
 * @author shechunming
 * @version 2016-08-14
 */
public class TaxBatchTask extends DataEntity<TaxBatchTask> {
	
	private static final long serialVersionUID = 1L;
	private String taskName;		// 任务名称
	private String batchType;		// 批处理类型
	private String executeTime;		// 执行时间
	private String dealClassName;		// 处理类名
	private String dealMethodName;		// 处理方法名
	private String description;		// 描述
	private Integer creatorId;		// creator_id
	private Integer updaterId;		// updater_id
	private String isDelete;		// is_delete
	private TaxBatchTaskService taxBatchTaskService;
	
	public TaxBatchTask() {
		super();
	}
	
	public TaxBatchTask(TaxBatchTaskService taxBatchTaskService) {
		super();
		this.taxBatchTaskService = taxBatchTaskService;
	}
	
	public TaxBatchTask(String sid){
		super(sid);
	}
	
	@Length(min=0, max=100, message="任务名称长度必须介于 0 和 100 之间")
	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	
	@Length(min=0, max=20, message="批处理类型长度必须介于 0 和 20 之间")
	public String getBatchType() {
		return batchType;
	}

	public void setBatchType(String batchType) {
		this.batchType = batchType;
	}
	
	@Length(min=0, max=100, message="执行时间长度必须介于 0 和 100 之间")
	public String getExecuteTime() {
		return executeTime;
	}

	public void setExecuteTime(String executeTime) {
		this.executeTime = executeTime;
	}
	
	@Length(min=0, max=100, message="处理类名长度必须介于 0 和 100 之间")
	public String getDealClassName() {
		return dealClassName;
	}

	public void setDealClassName(String dealClassName) {
		this.dealClassName = dealClassName;
	}
	
	@Length(min=0, max=100, message="处理方法名长度必须介于 0 和 100 之间")
	public String getDealMethodName() {
		return dealMethodName;
	}

	public void setDealMethodName(String dealMethodName) {
		this.dealMethodName = dealMethodName;
	}
	
	@Length(min=0, max=200, message="描述长度必须介于 0 和 200 之间")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Integer getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(Integer creatorId) {
		this.creatorId = creatorId;
	}
	
	public Integer getUpdaterId() {
		return updaterId;
	}

	public void setUpdaterId(Integer updaterId) {
		this.updaterId = updaterId;
	}
	
	public String getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}

	public boolean isNewRecord() {
		return isNewRecord;
	}

	public void setNewRecord(boolean isNewRecord) {
		this.isNewRecord = isNewRecord;
	}

	@Override
	protected void cerateSid() {
		setSid(taxBatchTaskService.getSequence());
	}
	
}