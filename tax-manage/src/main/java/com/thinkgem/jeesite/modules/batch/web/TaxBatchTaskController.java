/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.batch.web;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.batch.entity.TaxBatchTask;
import com.thinkgem.jeesite.modules.batch.service.TaxBatchTaskService;

/**
 * 批处理任务记录Controller
 * @author shechunming
 * @version 2016-08-14
 */
@Controller
@RequestMapping(value = "${adminPath}/batch/taxBatchTask")
public class TaxBatchTaskController extends BaseController {

	@Autowired
	private TaxBatchTaskService taxBatchTaskService;
	
	@ModelAttribute
	public TaxBatchTask get(@RequestParam(required=false) String sid) {
		TaxBatchTask entity = null;
		if (StringUtils.isNotBlank(sid)){
			entity = taxBatchTaskService.get(sid);
		}
		if (entity == null){
			entity = new TaxBatchTask(taxBatchTaskService);
		}
		return entity;
	}
	
	@RequiresPermissions("batch:taxBatchTask:view")
	@RequestMapping(value = {"list", ""})
	public String list(TaxBatchTask taxBatchTask, HttpServletRequest request, HttpServletResponse response, Model model) {
		List<TaxBatchTask> list = taxBatchTaskService.findList(taxBatchTask); 
		model.addAttribute("list", list);
		return "modules/batch/taxBatchTaskList";
	}

	@RequiresPermissions("batch:taxBatchTask:view")
	@RequestMapping(value = "form")
	public String form(TaxBatchTask taxBatchTask, Model model) {
		if(null != taxBatchTask.getSid()){
			taxBatchTask = taxBatchTaskService.get(taxBatchTask.getSid().toString());
		}
		model.addAttribute("taxBatchTask", taxBatchTask);
		return "modules/batch/taxBatchTaskForm";
	}

	@RequiresPermissions("batch:taxBatchTask:edit")
	@RequestMapping(value = "save")
	public String save(TaxBatchTask taxBatchTask, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, taxBatchTask)){
			return form(taxBatchTask, model);
		}
		this.initTaxBatchTask(taxBatchTask);
		taxBatchTaskService.save(taxBatchTask);
		addMessage(redirectAttributes, "保存批处理任务记录成功");
		return "redirect:"+Global.getAdminPath()+"/batch/taxBatchTask/?repage";
	}
	
	public void initTaxBatchTask(TaxBatchTask taxBatchTask){
		taxBatchTask.setCreateDate(new Date());
		taxBatchTask.setCreatorId(0);
		taxBatchTask.setUpdateDate(new Date());
		taxBatchTask.setUpdaterId(0);
		taxBatchTask.setIsDelete("N");
	}
	@RequiresPermissions("batch:taxBatchTask:edit")
	@RequestMapping(value = "delete")
	public String delete(TaxBatchTask taxBatchTask, RedirectAttributes redirectAttributes) {
		taxBatchTaskService.delete(taxBatchTask);
		addMessage(redirectAttributes, "删除批处理任务记录成功");
		return "redirect:"+Global.getAdminPath()+"/batch/taxBatchTask/?repage";
	}

}