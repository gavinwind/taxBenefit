/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.business.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.business.entity.TaxRequestBusinessDetail;
import com.thinkgem.jeesite.modules.business.dao.TaxRequestBusinessDetailDao;

/**
 * 请求记录明细Service
 * @author shechunming
 * @version 2016-08-15
 */
@Service
@Transactional(readOnly = true)
public class TaxRequestBusinessDetailService extends CrudService<TaxRequestBusinessDetailDao, TaxRequestBusinessDetail> {

	@Autowired
	protected TaxRequestBusinessDetailDao requestBusinessDetailDao;
	
	public TaxRequestBusinessDetail get(String id) {
		return super.get(id);
	}
	
	public List<TaxRequestBusinessDetail> findList(TaxRequestBusinessDetail taxRequestBusinessDetail) {
		return super.findList(taxRequestBusinessDetail);
	}
	
	public Page<TaxRequestBusinessDetail> findPage(Page<TaxRequestBusinessDetail> page, TaxRequestBusinessDetail taxRequestBusinessDetail) {
		return super.findPage(page, taxRequestBusinessDetail);
	}
	
	@Transactional(readOnly = false)
	public void save(TaxRequestBusinessDetail taxRequestBusinessDetail) {
		super.save(taxRequestBusinessDetail);
	}
	
	@Transactional(readOnly = false)
	public void delete(TaxRequestBusinessDetail taxRequestBusinessDetail) {
		super.delete(taxRequestBusinessDetail);
	}
	public String getSequence(){
		return requestBusinessDetailDao.getSequence()+"";
	}
}