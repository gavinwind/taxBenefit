/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.transcode.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.transcode.entity.TaxCodeMapping;
import com.thinkgem.jeesite.modules.transcode.service.TaxCodeMappingService;

/**
 * 平台转码Controller
 * @author shechunming
 * @version 2016-08-14
 */
@Controller
@RequestMapping(value = "${adminPath}/transcode/taxCodeMapping")
public class TaxCodeMappingController extends BaseController {

	@Autowired
	private TaxCodeMappingService taxCodeMappingService;

	@ModelAttribute
	public TaxCodeMapping get(@RequestParam(required=false) String sid) {
		TaxCodeMapping entity = null;
		if (StringUtils.isNotBlank(sid)){
			entity = taxCodeMappingService.get(sid);
		}
		if (entity == null){
			entity = new TaxCodeMapping(taxCodeMappingService);
		}
		return entity;
	}
	
	@RequiresPermissions("transcode:taxCodeMapping:view")
	@RequestMapping(value = {"list", ""})
	public String list(TaxCodeMapping taxCodeMapping, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TaxCodeMapping> page = taxCodeMappingService.findPage(new Page<TaxCodeMapping>(request, response), taxCodeMapping); 
		model.addAttribute("page", page);
		return "modules/transcode/taxCodeMappingList";
	}

	@RequiresPermissions("transcode:taxCodeMapping:view")
	@RequestMapping(value = "form")
	public String form(TaxCodeMapping taxCodeMapping, Model model) {
		model.addAttribute("taxCodeMapping", taxCodeMapping);
		return "modules/transcode/taxCodeMappingForm";
	}

	@RequiresPermissions("transcode:taxCodeMapping:edit")
	@RequestMapping(value = "save")
	public String save(TaxCodeMapping taxCodeMapping, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, taxCodeMapping)){
			return form(taxCodeMapping, model);
		}
		taxCodeMappingService.save(taxCodeMapping);
		addMessage(redirectAttributes, "保存平台转码成功");
		return "redirect:"+Global.getAdminPath()+"/transcode/taxCodeMapping/?repage";
	}
	
	@RequiresPermissions("transcode:taxCodeMapping:edit")
	@RequestMapping(value = "delete")
	public String delete(TaxCodeMapping taxCodeMapping, RedirectAttributes redirectAttributes) {
		taxCodeMappingService.delete(taxCodeMapping);
		addMessage(redirectAttributes, "删除平台转码成功");
		return "redirect:"+Global.getAdminPath()+"/transcode/taxCodeMapping/?repage";
	}

}