/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.business.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.business.entity.TaxRequestBusinessDetail;

/**
 * 请求记录明细DAO接口
 * @author shechunming
 * @version 2016-08-15
 */
@MyBatisDao
public interface TaxRequestBusinessDetailDao extends CrudDao<TaxRequestBusinessDetail> {
	/**
	 * 获取CodeMap 序列号
	 * @param sid
	 * @return
	 */
	public int getSequence();
}