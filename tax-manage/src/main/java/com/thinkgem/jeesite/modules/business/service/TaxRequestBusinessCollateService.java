/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.business.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.business.entity.TaxRequestBusinessCollate;
import com.thinkgem.jeesite.modules.business.dao.TaxRequestBusinessCollateDao;

/**
 * 请求交易核对记录Service
 * @author shechunming
 * @version 2016-08-14
 */
@Service
@Transactional(readOnly = true)
public class TaxRequestBusinessCollateService extends CrudService<TaxRequestBusinessCollateDao, TaxRequestBusinessCollate> {

	@Autowired
	protected TaxRequestBusinessCollateDao requestBusinessCollateDao;
	
	public TaxRequestBusinessCollate get(String sid) {
		return super.get(sid);
	}
	
	public List<TaxRequestBusinessCollate> findList(TaxRequestBusinessCollate taxRequestBusinessCollate) {
		return super.findList(taxRequestBusinessCollate);
	}
	
	public Page<TaxRequestBusinessCollate> findPage(Page<TaxRequestBusinessCollate> page, TaxRequestBusinessCollate taxRequestBusinessCollate) {
		return super.findPage(page, taxRequestBusinessCollate);
	}
	
	@Transactional(readOnly = false)
	public void save(TaxRequestBusinessCollate taxRequestBusinessCollate) {
		super.save(taxRequestBusinessCollate);
	}
	
	@Transactional(readOnly = false)
	public void delete(TaxRequestBusinessCollate taxRequestBusinessCollate) {
		super.delete(taxRequestBusinessCollate);
	}
	public String getSequence(){
		return requestBusinessCollateDao.getSequence()+"";
	}
}