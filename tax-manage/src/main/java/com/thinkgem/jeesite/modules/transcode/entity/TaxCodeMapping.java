/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.transcode.entity;

import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;

import com.thinkgem.jeesite.common.persistence.DataEntity;
import com.thinkgem.jeesite.modules.transcode.service.TaxCodeMappingService;

/**
 * 平台转码Entity
 * @author shechunming
 * @version 2016-08-14
 */
public class TaxCodeMapping extends DataEntity<TaxCodeMapping> {
	
	
	private static final long serialVersionUID = 1L;
	private String codeType;		// 转换业务码
	private String originalCode;		// 原始码
	private String originalDesc;		// 原始码说明
	private String targetCode;		// 转换码
	private String targetDesc;		// 转换码说明
	private String otherSign;		// 其他标记
	private String manageCom;		// 归属机构
	private String remark;		// 备注
	TaxCodeMappingService codeMapService;
	
	public TaxCodeMapping(){
		super();
	}
	
	public TaxCodeMapping(TaxCodeMappingService codeMapService) {
		super();
		this.codeMapService = codeMapService;
	}

	public TaxCodeMapping(String id){
		super(id);
	}
	
	@Length(min=0, max=50, message="转换业务码长度必须介于 0 和 50 之间")
	public String getCodeType() {
		return codeType;
	}

	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}
	
	@Length(min=0, max=30, message="原始码长度必须介于 0 和 30 之间")
	public String getOriginalCode() {
		return originalCode;
	}

	public void setOriginalCode(String originalCode) {
		this.originalCode = originalCode;
	}
	
	@Length(min=0, max=100, message="原始码说明长度必须介于 0 和 100 之间")
	public String getOriginalDesc() {
		return originalDesc;
	}

	public void setOriginalDesc(String originalDesc) {
		this.originalDesc = originalDesc;
	}
	
	@Length(min=0, max=30, message="转换码长度必须介于 0 和 30 之间")
	public String getTargetCode() {
		return targetCode;
	}

	public void setTargetCode(String targetCode) {
		this.targetCode = targetCode;
	}
	
	@Length(min=0, max=100, message="转换码说明长度必须介于 0 和 100 之间")
	public String getTargetDesc() {
		return targetDesc;
	}

	public void setTargetDesc(String targetDesc) {
		this.targetDesc = targetDesc;
	}
	
	@Length(min=0, max=20, message="其他标记长度必须介于 0 和 20 之间")
	public String getOtherSign() {
		return otherSign;
	}

	public void setOtherSign(String otherSign) {
		this.otherSign = otherSign;
	}
	
	@Length(min=0, max=20, message="归属机构长度必须介于 0 和 20 之间")
	public String getManageCom() {
		return manageCom;
	}

	public void setManageCom(String manageCom) {
		this.manageCom = manageCom;
	}
	
	@Length(min=0, max=200, message="备注长度必须介于 0 和 200 之间")
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	@Override
	protected void cerateSid() {
		setSid(codeMapService.getSequence());
		setManageCom("00");
	}
}