
package com.sinosoft.taxbenefit.core.SelCustomerInfo.biz.service;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqSelCustomerInfoDTO;

public interface SelCustomerInfoService{
	/**
	 * 查询客户信息
	 * @param reqSelCustomerInfoDTO
	 * @param paramHeadDTO
	 * @return
	 */
	ThirdSendResponseDTO queryCusInfo(ReqSelCustomerInfoDTO reqSelCustomerInfoDTO,ParamHeadDTO paramHeadDTO);
}