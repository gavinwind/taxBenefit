package com.sinosoft.taxbenefit.core.continuePolicy.biz.service;


import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqMContinuePolicyInfoDTO;
import com.sinosoft.taxbenefit.core.generated.model.TaxFaultTask;

public interface ContinuePolicyCoreService {
	/**
	 * 续保信息信息上传
	 * @param ReqPolicyInfoDTO
	 * @return 
	 */
	ThirdSendResponseDTO continuePolicySettlement(ReqContinuePolicyInfoDTO reqContinuePolicyInfoDTO,ParamHeadDTO paramHead);
	/**
	 * 续保信息信息上传（多个）
	 * @param reqMContinuePolicyInfoDTO
	 * @param paramHead
	 * @return
	 */
	ThirdSendResponseDTO continueMPolicySettlement(ReqMContinuePolicyInfoDTO reqMContinuePolicyInfoDTO,ParamHeadDTO paramHead);
	/**
	 * 续保信息上传请求异常任务服务方法
	 * @param reqJSON	核心请求JSON
	 * @param portCode	异常任务接口编码
	 * @param faultTaskId 任务记录
	 * @return
	 */
	TaxFaultTask continuePolicyExceptionJob(String reqJSON, String portCode, Integer faultTaskId);

}
