package com.sinosoft.taxbenefit.core.securityguard.config;
/**
 * 保全变更类型
 * @author zhanghaosh@sinosoft.com.cn
 *
 */
public enum CORE_ENUM_ENDORSEMENT_TYPE {
	APPLICANT_DATA_CHANGE("1","投保人联系方式变更"),
	INSURED_DATA_CHANGE("2","被保人联系方式变更"),
	BENEFICIARY_DATA_CHANGE("7","变更身故受益人/资料"),
	AUTOFILL_SELECT_CHANGE("5","保险费逾期未付选项"),//自垫选择权变更
	RENEW_SELECT_CHANGE("14","满期续保/不续保申请"),//续保选择权变更
	RECOVER("31","复效"),
	WORK_CHANGE("32","职业变更"),
	INSURED_BIRTHDAY_GENDER_CHANGE("35","被保人生日性别更正"),
	APPLICANT_CHANGE("36","投保人变更"),
	REDUCE_RISK("38","减少险种"),
	REDUCE_MRISK_SECURITY_AMOUNT("39","降低主险保额"),//保额变更
	REDUCE_FRISK_SECURITY_AMOUNT("40","降低附加险保额"),//保额变更
	ADD_RISK("41","增加附加险"),
	PAYMENT_FREQUENCY("47","交费频率变更"),
	HESITATE_SURRENDER("62","犹豫期退保"),//犹豫期退保
	SURRENDER("63","退保"),//退保
	COMPANY_TERMINATE_CONTRACT("66","保险公司解除合同"),//保险公司解除合同
	PROTOCOL_CANCEL("67","协议解约"),//退保
	ADD_INVESTMENT("82","额外投资/趸交保险费"),//追加投资
	INVESTMENT_ACCOUNT_GET("84","领取账户价值"),//投资账户领取
	BREAK_CONTRACT("89","银行保单质押违约强卖"),//退保
	CANCEL_POLICY("68","取消保单"),//退保
	POLICY_SECOND_UNDERWRITING("90","二核"),//保单二次核保
	FRISK_CANCEL("48","附加险解约"),//保费变更
	FRISK_HESITATE_CANCEL("50","附加险犹豫期撤保"),//保费变更
	HEALTH_INFORM("52","健康告知"),//保费变更
	POLICY_CHANGE("87","保单转换"),//产品转换
	POLICY_OUT("70","保单转出"),//保单转出
	POLICY_IN("80","万能额外投资");//保单转入
	
	
	private final String code;
	private final String desc;
	
	CORE_ENUM_ENDORSEMENT_TYPE(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public String code() {
		return code;
	}

	public String desc() {
		return desc;
	}
	public static CORE_ENUM_ENDORSEMENT_TYPE getEnumByKey(String key){
		for(CORE_ENUM_ENDORSEMENT_TYPE enumItem:CORE_ENUM_ENDORSEMENT_TYPE.values()){
			if(key.equals(enumItem.code())){
				return enumItem;
			}
		}
		return null;
	}
	
	
}
