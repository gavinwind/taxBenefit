package com.sinosoft.taxbenefit.core.SelCustomerInfo.dto;

import java.math.BigDecimal;
/**
 * 险种信息
 * @author zhangyu
 * @date 2016年3月12日 下午6:44:25
 */
public class CoverageInfo {
	//公司险种名称
	private String comCoverageName;
	//险种保额
	private BigDecimal sa;
	//核保决定
	private String underwritingDecision;
	
	public String getComCoverageName() {
		return comCoverageName;
	}
	public void setComCoverageName(String comCoverageName) {
		this.comCoverageName = comCoverageName;
	}
	public BigDecimal getSa() {
		return sa;
	}
	public void setSa(BigDecimal sa) {
		this.sa = sa;
	}
	public String getUnderwritingDecision() {
		return underwritingDecision;
	}
	public void setUnderwritingDecision(String underwritingDecision) {
		this.underwritingDecision = underwritingDecision;
	}
}