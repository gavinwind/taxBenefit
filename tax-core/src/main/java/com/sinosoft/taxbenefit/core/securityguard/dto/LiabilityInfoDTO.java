package com.sinosoft.taxbenefit.core.securityguard.dto;

import java.math.BigDecimal;
/**
 * 险种责任
 * @author zhanghaosh@sinosoft.com.cn
 *
 */
public class LiabilityInfoDTO {
	/** 公司责任代码 Y **/
	private String liabilityCode;
	/** 责任保额 S **/
	private BigDecimal liabilitySa;
	/** 责任状态 Y **/
	private String liabilityStatus;
	public String getLiabilityCode() {
		return liabilityCode;
	}
	public void setLiabilityCode(String liabilityCode) {
		this.liabilityCode = liabilityCode;
	}
	public BigDecimal getLiabilitySa() {
		return liabilitySa;
	}
	public void setLiabilitySa(BigDecimal liabilitySa) {
		this.liabilitySa = liabilitySa;
	}
	public String getLiabilityStatus() {
		return liabilityStatus;
	}
	public void setLiabilityStatus(String liabilityStatus) {
		this.liabilityStatus = liabilityStatus;
	}

}
