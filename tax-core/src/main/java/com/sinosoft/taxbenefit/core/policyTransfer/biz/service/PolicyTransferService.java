package com.sinosoft.taxbenefit.core.policyTransfer.biz.service;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqMInPolicyTransferDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqPolicyInTransferQueryDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqSInPolicyTransferDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqSOutPolicyTransferDTO;
import com.sinosoft.taxbenefit.core.generated.model.TaxFaultTask;

/**
 * 保单转移Service
 * @author SheChunMing
 */
public interface PolicyTransferService {
	
	/**
	 * 保单转入申请上传(单笔)
	 * @param transInfo
	 * @param paramHead
	 * @return
	 */
	ThirdSendResponseDTO addPolicyTransSInSettlement(ReqSInPolicyTransferDTO transInfo, ParamHeadDTO paramHead);
	/**
	 * 保单转入申请上传(多笔)
	 * @param transInfo
	 * @param paramHead
	 * @return
	 */
	ThirdSendResponseDTO addPolicyTransMInSettlement(ReqMInPolicyTransferDTO transInfo, ParamHeadDTO paramHead);

	/**
	 * 保单转出登记上传(单笔)
	 * @param transInfo
	 * @param paramHead
	 * @return
	 */
	ThirdSendResponseDTO addPolicyTransSOutSettlement(ReqSOutPolicyTransferDTO transInfo, ParamHeadDTO paramHead);
	/**
	 * 保单转出登记上传(多笔)
	 * @param transInfo
	 * @param paramHead
	 * @return
	 */
	ThirdSendResponseDTO addPolicyTransMOutSettlement(List<ReqSOutPolicyTransferDTO> transInfoList, ParamHeadDTO paramHead);
	/**
	 * 保单转入余额信息查询
	 * @param policyInTransferBalance
	 * @param paramHead
	 * @return
	 */
	ThirdSendResponseDTO queryPolicyTransInBalanceSettlement(ReqPolicyInTransferQueryDTO policyInTransferBalance, ParamHeadDTO paramHead);
	/**
	 * 保单转出信息查询
	 * @param transInfo （核心）保单转移信息查询对象
	 * @param paramHead 核心请求头部参数
	 * @return 响应给核心的报文内容
	 */
	ThirdSendResponseDTO policyTransOutQuerySettlement(ReqPolicyInTransferQueryDTO transInfo, ParamHeadDTO paramHead);
	/**
	 * 保单转出登记信息查询
	 * @param transInfo （核心）保单转移信息查询对象
	 * @param paramHead 核心请求头部参数
	 * @return 响应给核心的报文内容
	 */
	ThirdSendResponseDTO policyTransOutRegisterQuerySettlement(ReqPolicyInTransferQueryDTO transInfo, ParamHeadDTO paramHead);
	
	/**
	 * 保单转入申请上传异常任务
	 * @param reqJSON	核心请求JSON
	 * @param portCode	异常任务接口编码
	 * @return
	 */
	ThirdSendResponseDTO policyTransInExceptionJob(String reqJSON, String portCode, TaxFaultTask faultTask);
	
	/**
	 * 保单转出登记异常任务
	 * @param reqJSON	核心请求JSON
	 * @param portCode	异常任务接口编码
	 * @param faultTask 任务记录
	 * @return
	 */
	TaxFaultTask policyTransOutRegisterExceptionJob(String reqJSON, String portCode, Integer faultTaskId);
}
