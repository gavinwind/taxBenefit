package com.sinosoft.taxbenefit.core.renewalcancel.biz.service;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalcancel.RequestRenewalCancelDTO;
import com.sinosoft.taxbenefit.core.generated.model.TaxFaultTask;

public interface RenewalCancelCoreService {
	/**
	 * 续保撤销
	 * @param requestRenewalCancelDTO
	 * @param paramHead
	 * @return
	 */
	ThirdSendResponseDTO renewlCancel(RequestRenewalCancelDTO requestRenewalCancelDTO,ParamHeadDTO paramHead);
	/**
	 * 续保撤销异常任务服务方法
	 * @param reqJSON	核心请求JSON
	 * @param portCode	异常任务接口编码
	 * @param disposeType	处理类型(预约码/异常/异步)
	 * @param sequenceNo	预约码
	 * @return
	 */
	ThirdSendResponseDTO renewalCancelExceptionJob(String reqJSON, String portCode, TaxFaultTask faultTask);
}
