package com.sinosoft.taxbenefit.core.AccountChange.biz.dubbo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.taxbenefit.api.dto.request.accountchange.ReqAccountInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.inter.AccountChangeApiService;
import com.sinosoft.taxbenefit.core.AccountChange.biz.service.AccountChangeService;
/**
 * 账户变更上传DUBBO服务
 * @author zhangyu
 * @date 2016年3月14日 上午9:53:40
 */
@Service("accountChangeApiServiceImpl")
public class AccountChangeApiServiceImpl extends TaxBaseService implements AccountChangeApiService {
    @Autowired
	private AccountChangeService accountChangeService;
    
	@Override
	public ThirdSendResponseDTO AccountChange(
			ReqAccountInfoDTO reqAccountInfoDTO, ParamHeadDTO paramHeadDTO) {
		return accountChangeService.addAccountChange(reqAccountInfoDTO,paramHeadDTO);
	}

	@Override
	public ThirdSendResponseDTO AccountListChange(
			List<ReqAccountInfoDTO> reqAccountInfoList, ParamHeadDTO paramHeadDTO) {
		return accountChangeService.addAccountListChange(reqAccountInfoList, paramHeadDTO);
	}
}