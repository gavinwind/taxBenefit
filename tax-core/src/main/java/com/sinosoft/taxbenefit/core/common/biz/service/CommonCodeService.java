package com.sinosoft.taxbenefit.core.common.biz.service;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.core.common.dto.EbaoServerPortDTO;
import com.sinosoft.taxbenefit.core.common.dto.ReqBusinessDTO;
import com.sinosoft.taxbenefit.core.common.dto.ReqBusinessDetailDTO;
import com.sinosoft.taxbenefit.core.common.dto.SendLogDTO;
import com.sinosoft.taxbenefit.core.common.dto.ThirdSendDTO;

/**
 * 公共方法类
 * @author SheChunMing
 */
public interface CommonCodeService {
	/**
	 * 根据字典编码和字典类型获取字典对应的内容
	 * @param sysCode 字典编码
	 * @param codeType 字典编码
	 * @return
	 */
	String queryCommonCodeInfo(String sysCode, String codeType);

	/**
	 * 创建请求中保信DTO对象
	 * @param transCode	接口编码
	 * @param paramHead 报文头信息
	 * @param body 报文体对象
	 * @return
	 */
	ThirdSendDTO createThirdSend(String transCode, ParamHeadDTO paramHead, Object body);
	
	/**
	 * 记录发送报文交互日志
	 * @param thirdSendDTO
	 */
	void addLogSend(SendLogDTO sendLog);
	
	/**
	 * 获取中保信接口请求路径
	 * @param portCode
	 * @return
	 */
	EbaoServerPortDTO queryEBaoServerInfoByPortCode(String portCode);
	
	/**
	 * 获取中保信接口全部请求路径
	 * @return
	 */
	List<EbaoServerPortDTO> queryEBaoServerInfoAll();
	
	/**
	 * 初始化ReqBusinessDTO对象
	 * @return
	 */
	ReqBusinessDTO initReqBusinessDTO();
	
	/**
	 * 添加请求记录数
	 * @param reqBusiness
	 */
	void addReqBusinessCollate(ReqBusinessDTO reqBusiness);
	
	/**
	 * 初始化ReqBusinessDetailDTO对象
	 * @return
	 */
	ReqBusinessDetailDTO initReqBusinessDetailDTO(ParamHeadDTO paramHead);
	
	/**
	 * 添加失败交易记录数
	 * @param reqBusiness
	 */
	void addReqBusinessDetail(List<ReqBusinessDetailDTO> reqBusinessDetail);
	
	/**
	 * 封装处理中的返回报文
	 * @return
	 */
	ThirdSendResponseDTO initResponseJSONLoading();
}
