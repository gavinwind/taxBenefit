package com.sinosoft.taxbenefit.core.policyTransfer.dto;

import java.util.List;


/**
 * 请求（中保信）保单转移-转入信息
 * @author SheChunMing
 */
public class ReqEPolicyTransferDTO {
	// 转入保险公司
	private String companyName;
	// 转移申请日期
	private String transApplyDate;
	// 开户行名称
	private String bankName;
	// 户名
	private String accountHolder;
	// 账号
	private String accountNo;
	// 保单转移联系人
	private String contactName;
	// 电话
	private String contactTele;
	// Email
	private String contactEmail;
	// 转出信息
	private List<EPolicyTransferOutDTO> transferOutList;
	
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getTransApplyDate() {
		return transApplyDate;
	}
	public void setTransApplyDate(String transApplyDate) {
		this.transApplyDate = transApplyDate;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getAccountHolder() {
		return accountHolder;
	}
	public void setAccountHolder(String accountHolder) {
		this.accountHolder = accountHolder;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getContactTele() {
		return contactTele;
	}
	public void setContactTele(String contactTele) {
		this.contactTele = contactTele;
	}
	public String getContactEmail() {
		return contactEmail;
	}
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	public List<EPolicyTransferOutDTO> getTransferOutList() {
		return transferOutList;
	}
	public void setTransferOutList(List<EPolicyTransferOutDTO> transferOutList) {
		this.transferOutList = transferOutList;
	}
}
