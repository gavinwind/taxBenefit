package com.sinosoft.taxbenefit.core.taxbenvrify.dto;

import com.sinosoft.taxbenefit.api.base.EBResultCodeDTO;
/**
 *
 * title： 税优验证返回信息
 * @author zhangyu
 * @date 2016年3月10日 上午9:44:46
 */
public class TaxBeneVerifyDTO {
	// 客户编码
    private String customerNo;
	// 保单转移次数
    private Integer transferTimes;
    
    private EBResultCodeDTO result; 
    
	public EBResultCodeDTO getResult() {
		return result;
	}
	public void setResult(EBResultCodeDTO result) {
		this.result = result;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public Integer getTransferTimes() {
		return transferTimes;
	}
	public void setTransferTimes(Integer transferTimes) {
		this.transferTimes = transferTimes;
	}
}