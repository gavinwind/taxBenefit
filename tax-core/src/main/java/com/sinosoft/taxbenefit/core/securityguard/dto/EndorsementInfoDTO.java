package com.sinosoft.taxbenefit.core.securityguard.dto;

import java.math.BigDecimal;
import java.util.List;

/**
 * 中保信dto 最外层Dto
 * 
 * @author zhanghaosh@sinosoft.com.cn
 *
 */
public class EndorsementInfoDTO {
	
//	个人投保单
	private SinglePolicyHolderInfoDTO singlePolicyHolder;
//	团体投保单
	private GroupPolicyHolderInfoDTO groupPolicyHolder;
//	被保人
	private List<InsuredInfoDTO> insuredList;
	
	public SinglePolicyHolderInfoDTO getSinglePolicyHolder() {
		return singlePolicyHolder;
	}

	public void setSinglePolicyHolder(SinglePolicyHolderInfoDTO singlePolicyHolder) {
		this.singlePolicyHolder = singlePolicyHolder;
	}

	public GroupPolicyHolderInfoDTO getGroupPolicyHolder() {
		return groupPolicyHolder;
	}

	public void setGroupPolicyHolder(GroupPolicyHolderInfoDTO groupPolicyHolder) {
		this.groupPolicyHolder = groupPolicyHolder;
	}

	public List<InsuredInfoDTO> getInsuredList() {
		return insuredList;
	}

	public void setInsuredList(List<InsuredInfoDTO> insuredList) {
		this.insuredList = insuredList;
	}

	/** 保单号 **/
	private String policyNo;
	
	/** 保单类别 **/
	private String policyType;
	/** 保单所属二级机构名称 **/
	private String policyOrg;
	/** 保单所属区域 **/
	private String policyArea;
	/** 保全类型 **/
	private String endorsementType;
	/** 保全批单号 **/
	private String endorsementNo;
	/** 保单批单序号 **/
	private String polEndSeq;
	/** 批单完成时间 **/
	private String finishTime;
	/** 批单状态 **/
	private String endorsementStatus;
	/** 保全申请日期 **/
	private String endorsementApplicationDate;
	/** 保全生效日期 **/
	private String endorsementEffectiveDate;
	/** 保单生效时间 **/
	private String effectiveDate;
	/** 保单终止日期 **/
	private String terminationDate;
	/** 保单中止日期 **/
	private String suspendDate;
	/** 保单效力恢复日期 **/
	private String recoverDate;
	/** 保单来源 **/
	private String policySource;
	/** 保单状态 **/
	private String policyStatus;
	/** 保单终止原因 **/
	private String terminationReason;
	/** 交费频率 **/
	private String paymentFrequency;
	/** 交费频率 **/
	private BigDecimal surrenderAmount;
	/** 补税金额 **/
	private BigDecimal taxDeduct;

	/** 追加交费金额 **/
	private BigDecimal topUpAmount;
	
	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}



	public String getPolicyType() {
		return policyType;
	}

	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}

	public String getPolicyOrg() {
		return policyOrg;
	}

	public void setPolicyOrg(String policyOrg) {
		this.policyOrg = policyOrg;
	}

	public String getPolicyArea() {
		return policyArea;
	}

	public void setPolicyArea(String policyArea) {
		this.policyArea = policyArea;
	}

	public String getEndorsementType() {
		return endorsementType;
	}

	public void setEndorsementType(String endorsementType) {
		this.endorsementType = endorsementType;
	}

	public String getEndorsementNo() {
		return endorsementNo;
	}

	public void setEndorsementNo(String endorsementNo) {
		this.endorsementNo = endorsementNo;
	}

	public String getPolEndSeq() {
		return polEndSeq;
	}

	public void setPolEndSeq(String polEndSeq) {
		this.polEndSeq = polEndSeq;
	}

	


	public String getFinishTime() {
		return finishTime;
	}

	public void setFinishTime(String finishTime) {
		this.finishTime = finishTime;
	}

	public String getEndorsementStatus() {
		return endorsementStatus;
	}

	public void setEndorsementStatus(String endorsementStatus) {
		this.endorsementStatus = endorsementStatus;
	}

	public String getEndorsementApplicationDate() {
		return endorsementApplicationDate;
	}

	public void setEndorsementApplicationDate(String endorsementApplicationDate) {
		this.endorsementApplicationDate = endorsementApplicationDate;
	}

	public String getEndorsementEffectiveDate() {
		return endorsementEffectiveDate;
	}

	public void setEndorsementEffectiveDate(String endorsementEffectiveDate) {
		this.endorsementEffectiveDate = endorsementEffectiveDate;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getTerminationDate() {
		return terminationDate;
	}

	public void setTerminationDate(String terminationDate) {
		this.terminationDate = terminationDate;
	}

	public String getSuspendDate() {
		return suspendDate;
	}

	public void setSuspendDate(String suspendDate) {
		this.suspendDate = suspendDate;
	}

	public String getRecoverDate() {
		return recoverDate;
	}

	public void setRecoverDate(String recoverDate) {
		this.recoverDate = recoverDate;
	}

	public String getPolicySource() {
		return policySource;
	}

	public void setPolicySource(String policySource) {
		this.policySource = policySource;
	}

	public String getPolicyStatus() {
		return policyStatus;
	}

	public void setPolicyStatus(String policyStatus) {
		this.policyStatus = policyStatus;
	}

	public String getTerminationReason() {
		return terminationReason;
	}

	public void setTerminationReason(String terminationReason) {
		this.terminationReason = terminationReason;
	}

	public String getPaymentFrequency() {
		return paymentFrequency;
	}

	public void setPaymentFrequency(String paymentFrequency) {
		this.paymentFrequency = paymentFrequency;
	}

	public BigDecimal getSurrenderAmount() {
		return surrenderAmount;
	}

	public void setSurrenderAmount(BigDecimal surrenderAmount) {
		this.surrenderAmount = surrenderAmount;
	}

	public BigDecimal getTaxDeduct() {
		return taxDeduct;
	}

	public void setTaxDeduct(BigDecimal taxDeduct) {
		this.taxDeduct = taxDeduct;
	}

	public BigDecimal getTopUpAmount() {
		return topUpAmount;
	}

	public void setTopUpAmount(BigDecimal topUpAmount) {
		this.topUpAmount = topUpAmount;
	}

}
