package com.sinosoft.taxbenefit.core.securityguard.dto;

import java.util.List;

public class EndorsementResult {

	/** 业务号 **/
	private String policyNo;
	/** 批单号 **/
	private String endorsementNo;
	/** 保全类型 **/
	private String endorsementType;
	/** 保全编码 **/
	private String endorsementSequenceNo;
	
	//投保人
	private	singlePolicyHolderResult singlePolicyHolder;
	
	//	被保人
	private List<InsuredResult> insuredList;

	public singlePolicyHolderResult getSinglePolicyHolder() {
		return singlePolicyHolder;
	}

	public void setSinglePolicyHolder(singlePolicyHolderResult singlePolicyHolder) {
		this.singlePolicyHolder = singlePolicyHolder;
	}

	
	public List<InsuredResult> getInsuredList() {
		return insuredList;
	}

	public void setInsuredList(List<InsuredResult> insuredList) {
		this.insuredList = insuredList;
	}


	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public String getEndorsementNo() {
		return endorsementNo;
	}

	public void setEndorsementNo(String endorsementNo) {
		this.endorsementNo = endorsementNo;
	}

	public String getEndorsementType() {
		return endorsementType;
	}

	public void setEndorsementType(String endorsementType) {
		this.endorsementType = endorsementType;
	}

	public String getEndorsementSequenceNo() {
		return endorsementSequenceNo;
	}

	public void setEndorsementSequenceNo(String endorsementSequenceNo) {
		this.endorsementSequenceNo = endorsementSequenceNo;
	}

	
}
