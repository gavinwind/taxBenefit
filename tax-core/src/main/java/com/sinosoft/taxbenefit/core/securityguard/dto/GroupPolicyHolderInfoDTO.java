package com.sinosoft.taxbenefit.core.securityguard.dto;

/**
 * 团体投保人信息
 * 
 * @author zhanghaosh@sinosoft.com.cn
 *
 */
public class GroupPolicyHolderInfoDTO {

	/** 团体客户编码 Y **/
	private String companyNo;
	/** 团体客户名称 Y **/
	private String companyName;
	/** 投保单位税务登记号 S **/
	private String organCode1;
	/** 组织机构代码 **/
	private String organCode2;
	/** 营业证号 **/
	private String organCode3;
	/** 社会信用代码 S **/
	private String organCode4;
	/** 常驻地 Y **/
	private String residencePlace;
	/** 行业分类 Y **/
	private String Industry;
	/** 单位性质 Y **/
	private String companyNature;
	/** 单位人数 **/
	private String employeeNumber;
	/** 是否上市 **/
	private String listedIndi;
	public String getCompanyNo() {
		return companyNo;
	}
	public void setCompanyNo(String companyNo) {
		this.companyNo = companyNo;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getOrganCode1() {
		return organCode1;
	}
	public void setOrganCode1(String organCode1) {
		this.organCode1 = organCode1;
	}
	public String getOrganCode2() {
		return organCode2;
	}
	public void setOrganCode2(String organCode2) {
		this.organCode2 = organCode2;
	}
	public String getOrganCode3() {
		return organCode3;
	}
	public void setOrganCode3(String organCode3) {
		this.organCode3 = organCode3;
	}
	public String getOrganCode4() {
		return organCode4;
	}
	public void setOrganCode4(String organCode4) {
		this.organCode4 = organCode4;
	}
	public String getResidencePlace() {
		return residencePlace;
	}
	public void setResidencePlace(String residencePlace) {
		this.residencePlace = residencePlace;
	}
	public String getIndustry() {
		return Industry;
	}
	public void setIndustry(String industry) {
		Industry = industry;
	}
	public String getCompanyNature() {
		return companyNature;
	}
	public void setCompanyNature(String companyNature) {
		this.companyNature = companyNature;
	}
	public String getEmployeeNumber() {
		return employeeNumber;
	}
	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}
	public String getListedIndi() {
		return listedIndi;
	}
	public void setListedIndi(String listedIndi) {
		this.listedIndi = listedIndi;
	}
	

}
