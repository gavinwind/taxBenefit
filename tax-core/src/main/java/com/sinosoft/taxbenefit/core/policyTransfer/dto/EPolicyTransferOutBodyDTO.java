package com.sinosoft.taxbenefit.core.policyTransfer.dto;

import java.util.List;

/**
 * 请求（中保信）保单转移-转出
 * @author SheChunMing
 */
public class EPolicyTransferOutBodyDTO {
	// 转出信息
	private List<EPolicyTransferOutDTO> transferOutList;

	public List<EPolicyTransferOutDTO> getTransferOutList() {
		return transferOutList;
	}

	public void setTransferOutList(List<EPolicyTransferOutDTO> transferOutList) {
		this.transferOutList = transferOutList;
	}
	
}
