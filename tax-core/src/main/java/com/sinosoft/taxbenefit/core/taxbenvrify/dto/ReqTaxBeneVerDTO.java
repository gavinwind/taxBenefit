package com.sinosoft.taxbenefit.core.taxbenvrify.dto;

import java.util.List;
/**
 * （中保信）税优验证请求DTO
 * @author zhangyu
 * @date 2016年3月14日 下午9:14:45
 */
public class ReqTaxBeneVerDTO {
	
	private List<TaxBeneClientDTO> clientList;
	
	public List<TaxBeneClientDTO> getClientList() {
		return clientList;
	}
	public void setClientList(List<TaxBeneClientDTO> clientList) {
		this.clientList = clientList;
	}
}