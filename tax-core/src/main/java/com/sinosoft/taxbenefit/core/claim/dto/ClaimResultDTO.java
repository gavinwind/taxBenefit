package com.sinosoft.taxbenefit.core.claim.dto;

import java.util.List;

/**
 * 中保信返回的DTO
 * 
 * @author zhangke
 *
 */
public class ClaimResultDTO {
	private List<ClaimResultInfo> claimResultList;

	public List<ClaimResultInfo> getClaimResultList() {
		return claimResultList;
	}

	public void setClaimResultList(List<ClaimResultInfo> claimResultList) {
		this.claimResultList = claimResultList;
	}
}
