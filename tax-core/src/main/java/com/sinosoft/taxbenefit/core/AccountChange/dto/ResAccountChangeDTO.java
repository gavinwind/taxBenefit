package com.sinosoft.taxbenefit.core.AccountChange.dto;

import java.util.List;
/**
 * 账户变更上传中保信返回DTO
 * @author zhangyu
 * @date 2016年3月14日 下午1:28:30
 */
public class ResAccountChangeDTO {
	
	private List<AccountChangeAffDTO> savingAccountFeeList;
	
	public List<AccountChangeAffDTO> getSavingAccountFeeList() {
		return savingAccountFeeList;
	}
	public void setSavingAccountFeeList(
			List<AccountChangeAffDTO> savingAccountFeeList) {
		this.savingAccountFeeList = savingAccountFeeList;
	}
}