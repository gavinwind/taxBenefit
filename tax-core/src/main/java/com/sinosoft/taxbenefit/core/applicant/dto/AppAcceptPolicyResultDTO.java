package com.sinosoft.taxbenefit.core.applicant.dto;

import java.util.List;

/**
 * 中保信返回保单信息DTO
 * 
 * @author zhangke
 *
 */
public class AppAcceptPolicyResultDTO {
	/**保单号*/
	private String policyNo;
	/**客户信息*/
	private List<AppAcceptClientResultDTO> clientList;

	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public List<AppAcceptClientResultDTO> getClientList() {
		return clientList;
	}

	public void setClientList(List<AppAcceptClientResultDTO> clientList) {
		this.clientList = clientList;
	}
}
