package com.sinosoft.taxbenefit.core.renewalpremium.dto;

import java.math.BigDecimal;
import java.util.List;
/**
 * 中保信-续期保费信息
 * Title:ReqPremiumDTO
 * @author yangdongkai@outlook.com
 * @date 2016年3月7日 下午5:34:36
 */
public class ReqPremiumDTO {
	//保单号
	private String policyNo;
	//分单号
	private String sequenceNo;
	//费用编码
	private String feeId;
	//交费频率
	private String paymentFrequency;
	//保费应收日期
	private String payDueDate;
	//实收保费日期
	private String confirmDate;
	//实收保费金额
	private BigDecimal premiumAmount;
	//交费起期
	private String payFromDate;
	//交费止期
	private String payToDate;
	//保单状态
	private String feeStatus;
	//险种信息
	private List<ReqRenewalPremiumCoverageDTO> coverageList;

	/**
	 * @return the policyNo
	 */
	public String getPolicyNo() {
		return policyNo;
	}
	/**
	 * @param policyNo the policyNo to set
	 */
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	/**
	 * @return the sequenceNo
	 */
	public String getSequenceNo() {
		return sequenceNo;
	}
	/**
	 * @param sequenceNo the sequenceNo to set
	 */
	public void setSequenceNo(String sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
	/**
	 * @return the feeId
	 */
	public String getFeeId() {
		return feeId;
	}
	/**
	 * @param feeId the feeId to set
	 */
	public void setFeeId(String feeId) {
		this.feeId = feeId;
	}
	/**
	 * @return the paymentFrequency
	 */
	public String getPaymentFrequency() {
		return paymentFrequency;
	}
	/**
	 * @param paymentFrequency the paymentFrequency to set
	 */
	public void setPaymentFrequency(String paymentFrequency) {
		this.paymentFrequency = paymentFrequency;
	}
	/**
	 * @return the payDueDate
	 */
	public String getPayDueDate() {
		return payDueDate;
	}
	/**
	 * @param payDueDate the payDueDate to set
	 */
	public void setPayDueDate(String payDueDate) {
		this.payDueDate = payDueDate;
	}
	/**
	 * @return the confirmDate
	 */
	public String getConfirmDate() {
		return confirmDate;
	}
	/**
	 * @param confirmDate the confirmDate to set
	 */
	public void setConfirmDate(String confirmDate) {
		this.confirmDate = confirmDate;
	}
	/**
	 * @return the premiumAmount
	 */
	public BigDecimal getPremiumAmount() {
		return premiumAmount;
	}
	/**
	 * @param premiumAmount the premiumAmount to set
	 */
	public void setPremiumAmount(BigDecimal premiumAmount) {
		this.premiumAmount = premiumAmount;
	}
	/**
	 * @return the payFromDate
	 */
	public String getPayFromDate() {
		return payFromDate;
	}
	/**
	 * @param payFromDate the payFromDate to set
	 */
	public void setPayFromDate(String payFromDate) {
		this.payFromDate = payFromDate;
	}
	/**
	 * @return the payToDate
	 */
	public String getPayToDate() {
		return payToDate;
	}
	/**
	 * @param payToDate the payToDate to set
	 */
	public void setPayToDate(String payToDate) {
		this.payToDate = payToDate;
	}
	/**
	 * @return the feeStatus
	 */
	public String getFeeStatus() {
		return feeStatus;
	}
	/**
	 * @param feeStatus the feeStatus to set
	 */
	public void setFeeStatus(String feeStatus) {
		this.feeStatus = feeStatus;
	}
	/**
	 * @return the coverageList
	 */
	public List<ReqRenewalPremiumCoverageDTO> getCoverageList() {
		return coverageList;
	}
	/**
	 * @param coverageList the coverageList to set
	 */
	public void setCoverageList(List<ReqRenewalPremiumCoverageDTO> coverageList) {
		this.coverageList = coverageList;
	}
	

}
