package com.sinosoft.taxbenefit.core.applicant.dto;

import java.util.List;
/**
 * 中保信返回json映射DTO
 * @author zhangke
 *
 */
public class AppAcceptResultBody {
	/**保单信息*/
	private List<AppAcceptPolicyResultDTO> policyList;

	public List<AppAcceptPolicyResultDTO> getPolicyList() {
		return policyList;
	}

	public void setPolicyList(List<AppAcceptPolicyResultDTO> policyList) {
		this.policyList = policyList;
	}

	
}
