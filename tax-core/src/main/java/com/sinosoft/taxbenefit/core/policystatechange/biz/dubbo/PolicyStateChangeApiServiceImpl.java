package com.sinosoft.taxbenefit.core.policystatechange.biz.dubbo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.policystate.PolicyStateEndorsement;
import com.sinosoft.taxbenefit.api.inter.PolicyStateChangeApiService;
import com.sinosoft.taxbenefit.core.policystatechange.biz.service.PolicyStateChangeService;

@Service("policyStateChangeApiServiceImpl")
public class PolicyStateChangeApiServiceImpl extends TaxBaseService implements PolicyStateChangeApiService {

	@Autowired
	PolicyStateChangeService policyStateChangeService;
	@Override
	public ThirdSendResponseDTO policyStateChange(List<PolicyStateEndorsement> policybody,
			ParamHeadDTO paramHeadDTO) {
		logger.info("保单状态修改转入后置处理");
		return policyStateChangeService.modifyPolicyState(policybody, paramHeadDTO);
	}

}
