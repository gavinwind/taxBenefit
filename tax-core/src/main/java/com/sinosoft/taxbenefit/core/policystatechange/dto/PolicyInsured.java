package com.sinosoft.taxbenefit.core.policystatechange.dto;

import java.util.List;

public class PolicyInsured {
//	分单号 
	private String sequenceNo;
	
	private List<PolicyCoverage> coverageList;

	public List<PolicyCoverage> getCoverageList() {
		return coverageList;
	}

	public void setCoverageList(List<PolicyCoverage> coverageList) {
		this.coverageList = coverageList;
	}

	public String getSequenceNo() {
		return sequenceNo;
	}

	public void setSequenceNo(String sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
	
	
}
