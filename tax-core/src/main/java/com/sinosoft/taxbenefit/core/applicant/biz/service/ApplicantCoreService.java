package com.sinosoft.taxbenefit.core.applicant.biz.service;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.applicant.ReqAppPolicyDTO;
import com.sinosoft.taxbenefit.api.dto.request.applicant.ReqPolicyCancelDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.core.generated.model.TaxFaultTask;

/**
 * 承保信息上传Coer接口
 * @author zhangke
 *
 */
public interface ApplicantCoreService {
	/**
	 * 承保上传
	 * @return
	 */
	ThirdSendResponseDTO addApplicant(List<ReqAppPolicyDTO> appAcceptMores, ParamHeadDTO paramHeadDTO);

	/**
	 * 承保撤销
	 * @param reqPolicyCancelDTO
	 * @param paramHeadDTO
	 * @return
	 */
	ThirdSendResponseDTO policyReversal(ReqPolicyCancelDTO reqPolicyCancelDTO, ParamHeadDTO paramHeadDTO);  
	/**
	 * 承保异常任务
	 * @param reqJSON	核心请求JSON
	 * @param portCode	异常任务接口编码
	 * @param faultTaskId 任务记录编号
	 * @return
	 */
	TaxFaultTask applicantExceptionJob(String reqJSON, String portCode, Integer faultTaskId);

	/**
	 * 承保撤销异常任务
	 * @param reqJSON 核心请求JSON
	 * @param portCode 异常任务接口编码
	 * @param faultTask 任务记录
	 * @return
	 */
	ThirdSendResponseDTO policyReversalExceptionJob(String reqJSON, String portCode, TaxFaultTask faultTask);
}
