package com.sinosoft.taxbenefit.core.policyTransfer.dto;

import java.util.List;


/**
 * （中保信）保单转移-转出信息
 * @author SheChunMing
 */
public class EPolicyTransferOutDTO {
	// 转出保险公司代码
	private String companyCode;
	// 转出保险公司名称
	private String companyName;
	// 转出保单号
	private String policyNo;
	// 客户编号
	private String customerNo;
	// 客户信息
	private List<EPolicyTransClientDTO> clientList;
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getPolicyNo() {
		return policyNo;
	}
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public List<EPolicyTransClientDTO> getClientList() {
		return clientList;
	}
	public void setClientList(List<EPolicyTransClientDTO> clientList) {
		this.clientList = clientList;
	}
}
