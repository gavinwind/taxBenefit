package com.sinosoft.taxbenefit.core.AccountChange.dto;

import com.sinosoft.taxbenefit.api.base.EBResultCodeDTO;
/**
 * title：账户变更上传返回DTO
 * @author zhangyu
 * @date 2016年3月6日 下午1:16:11
 */
public class AccountChangeAffDTO {
	//公司费用ID
	private String comFeeId;
	//账户变更确认编码
	private String accountFeeSequenceNo;
	
	private EBResultCodeDTO result;
	
	public String getComFeeId() {
		return comFeeId;
	}
	public void setComFeeId(String comFeeId) {
		this.comFeeId = comFeeId;
	}
	public EBResultCodeDTO getResult() {
		return result;
	}
	public void setResult(EBResultCodeDTO result) {
		this.result = result;
	}
	public String getAccountFeeSequenceNo() {
		return accountFeeSequenceNo;
	}
	public void setAccountFeeSequenceNo(String accountFeeSequenceNo) {
		this.accountFeeSequenceNo = accountFeeSequenceNo;
	}
}