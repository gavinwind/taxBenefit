package com.sinosoft.taxbenefit.core.claim.dto;
/**
 * 理赔撤销中保信返回DTO
 * @author zhangke
 *
 */
public class ClaimReversalResultDTO {
	/**理赔结果*/
	private ClaimReversalResultInfoDTO claimResult;

	public ClaimReversalResultInfoDTO getClaimResult() {
		return claimResult;
	}

	public void setClaimResult(ClaimReversalResultInfoDTO claimResult) {
		this.claimResult = claimResult;
	}
}
