package com.sinosoft.taxbenefit.core.SelCustomerInfo.dto;
/**
 * 客户概要信息查询
 * @author zhangyu
 * @date 2016年3月6日 下午12:00:31
 */
public class ReqCustomerQueryInfoDTO {
	//操作人
	private String operator;
	//客户DTO
	private CustomerInfo customer;
	
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public CustomerInfo getCustomer() {
		return customer;
	}
	public void setCustomer(CustomerInfo customer) {
		this.customer = customer;
	}
}