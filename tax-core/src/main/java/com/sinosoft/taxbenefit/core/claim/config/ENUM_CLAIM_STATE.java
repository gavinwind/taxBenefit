package com.sinosoft.taxbenefit.core.claim.config;
/**
 * 案件状态
 * @author zhangke
 *
 */
public enum ENUM_CLAIM_STATE {
	EFFECTIVE("4","结案"),REPEAL("5","撤销");
	private final String code;
	private final String desc;

	ENUM_CLAIM_STATE(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public String code() {
		return code;
	}

	public String desc() {
		return desc;
	}
}
