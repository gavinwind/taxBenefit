package com.sinosoft.taxbenefit.core.continuePolicy.dto;

import java.util.List;

/**
 * 中保信-续保信息返回解析DTO
 * Title:ResContinuePolicyDTO
 * @author yangdongkai@outlook.com
 * @date 2016年3月8日 下午3:37:15
 */
public class ResContinuePolicyDTO {
	//续保结果
	private List<ResContinuePolicyListDTO> renewalResultList;
	/**
	 * @return the renewalResultList
	 */
	public List<ResContinuePolicyListDTO> getRenewalResultList() {
		return renewalResultList;
	}
	public void setRenewalResultList(List<ResContinuePolicyListDTO> renewalResultList) {
		this.renewalResultList = renewalResultList;
	}
}
