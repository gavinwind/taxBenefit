package com.sinosoft.taxbenefit.core.continuePolicy.dto;

import java.math.BigDecimal;
import java.util.List;
/**
 * Title:中保信-险种信息
 * @author yangdongkai@outlook.com
 * @date 2016年3月2日 上午10:06:31
 */
public class ReqContinuePolicyCoverageDTO{
	// 公司产品组代码
	private String coveragePackageCode;
	// 公司险种代码
	private String comCoverageCode;
	// 险种生效日期
	private String coverageEffectiveDate;
	// 险种期满日期
	private String coverageExpireDate;
	// 险种保额
	private BigDecimal sa;
	// 险种保费
	private BigDecimal premium;
	// 责任信息
	private List<ReqContinuePolicyLiabilityDTO> liabilityList;
	// 核保信息
	private ReqContinuePolicyUwinfoDTO underwriting;
	
	public ReqContinuePolicyUwinfoDTO getUnderwriting() {
		return underwriting;
	}

	public void setUnderwriting(ReqContinuePolicyUwinfoDTO underwriting) {
		this.underwriting = underwriting;
	}
	/**
	 * @return the liabilityList
	 */
	public List<ReqContinuePolicyLiabilityDTO> getLiabilityList() {
		return liabilityList;
	}
	
	/**
	 * @param liabilityList the liabilityList to set
	 */
	public void setLiabilityList(List<ReqContinuePolicyLiabilityDTO> liabilityList) {
		this.liabilityList = liabilityList;
	}

	/**
	 * @return the coveragePackageCode
	 */
	public String getCoveragePackageCode() {
		return coveragePackageCode;
	}
	/**
	 * @param coveragePackageCode the coveragePackageCode to set
	 */
	public void setCoveragePackageCode(String coveragePackageCode) {
		this.coveragePackageCode = coveragePackageCode;
	}
	/**
	 * @return the comCoverageCode
	 */
	public String getComCoverageCode() {
		return comCoverageCode;
	}
	/**
	 * @param comCoverageCode the comCoverageCode to set
	 */
	public void setComCoverageCode(String comCoverageCode) {
		this.comCoverageCode = comCoverageCode;
	}
	/**
	 * @return the coverageEffectiveDate
	 */
	public String getCoverageEffectiveDate() {
		return coverageEffectiveDate;
	}
	/**
	 * @param coverageEffectiveDate the coverageEffectiveDate to set
	 */
	public void setCoverageEffectiveDate(String coverageEffectiveDate) {
		this.coverageEffectiveDate = coverageEffectiveDate;
	}
	/**
	 * @return the coverageExpireDate
	 */
	public String getCoverageExpireDate() {
		return coverageExpireDate;
	}
	/**
	 * @param coverageExpireDate the coverageExpireDate to set
	 */
	public void setCoverageExpireDate(String coverageExpireDate) {
		this.coverageExpireDate = coverageExpireDate;
	}

	/**
	 * @return the sa
	 */
	public BigDecimal getSa() {
		return sa;
	}

	/**
	 * @param sa the sa to set
	 */
	public void setSa(BigDecimal sa) {
		this.sa = sa;
	}

	/**
	 * @return the premium
	 */
	public BigDecimal getPremium() {
		return premium;
	}
	/**
	 * @param premium the premium to set
	 */
	public void setPremium(BigDecimal premium) {
		this.premium = premium;
	}
}
