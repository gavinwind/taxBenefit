package com.sinosoft.taxbenefit.core.claim.dto;


/**
 * 收据分项费用
 * 
 * @author zhangke
 *
 */
public class ClaimReceiptFeeInfoDTO  {
	/** 收据分项费用类型 **/
	private String receiptFeeType;
	/** 金额 **/
	private String value;

	public String getReceiptFeeType() {
		return receiptFeeType;
	}

	public void setReceiptFeeType(String receiptFeeType) {
		this.receiptFeeType = receiptFeeType;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
