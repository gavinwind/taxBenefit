package com.sinosoft.taxbenefit.core.taxbenvrify.biz.service;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqTaxBeneVerifyInfoDTO;
import com.sinosoft.taxbenefit.core.generated.model.TaxFaultTask;

public interface TaxBeneVerifyService {
/**
 * 税优验证接口
 * @param reqTaxBenefitInfoDTO
 * @param paramHeadDTO
 * @return
 */
	ThirdSendResponseDTO addtaxBeneVerify(ReqTaxBeneVerifyInfoDTO reqTaxBenefitInfoDTO,ParamHeadDTO paramHeadDTO);
	/**
	 * 税优验证异常任务服务方法
	 * @param reqJSON	核心请求JSON
	 * @param portCode	异常任务接口编码
	 * @param disposeType	处理类型(预约码/异常/异步)
	 * @param sequenceNo	预约码
	 * @return
	 */
	ThirdSendResponseDTO taxBeneVerifyExceptionJob(String reqJSON, String portCode, TaxFaultTask faultTask);
}
