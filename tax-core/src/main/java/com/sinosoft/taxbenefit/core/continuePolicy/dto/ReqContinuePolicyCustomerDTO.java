package com.sinosoft.taxbenefit.core.continuePolicy.dto;

import java.util.List;

/**
 * 中保信-客户信息
 * Title:ReqContinuePolicyCustomerDTO
 * @author yangdongkai@outlook.com
 * @date 2016年3月11日 下午5:27:04
 */
public class ReqContinuePolicyCustomerDTO{
	//分单号
	private String sequenceNo;
	//被保人客户编码
	private String customerNo;
	//险种列表
	private List<ReqContinuePolicyCoverageDTO> coverageList;
	
	/**
	 * @return the coverageList
	 */
	public List<ReqContinuePolicyCoverageDTO> getCoverageList() {
		return coverageList;
	}
	/**
	 * @param coverageList the coverageList to set
	 */
	public void setCoverageList(List<ReqContinuePolicyCoverageDTO> coverageList) {
		this.coverageList = coverageList;
	}
	/**
	 * @return the sequenceNo
	 */
	public String getSequenceNo() {
		return sequenceNo;
	}
	/**
	 * @param sequenceNo the sequenceNo to set
	 */
	public void setSequenceNo(String sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
	/**
	 * @return the customerNo
	 */
	public String getCustomerNo() {
		return customerNo;
	}
	/**
	 * @param customerNo the customerNo to set
	 */
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
}
