package com.sinosoft.taxbenefit.core.SelCustomerInfo.dto;

import com.sinosoft.taxbenefit.api.base.EBResultCodeDTO;
/**
 * 客户查询返回DTO
 * @author zhangyu
 * @date 2016年3月12日 下午6:45:06
 */
public class ResCustomerQueryInfoDTO {
	//客户DTO
	private CusInfoDTO customer;
	
	private EBResultCodeDTO result; 
	
	public EBResultCodeDTO getResult() {
		return result;
	}
	public void setResult(EBResultCodeDTO result) {
		this.result = result;
	}
	public CusInfoDTO getCustomer() {
		return customer;
	}
	public void setCustomer(CusInfoDTO customer) {
		this.customer = customer;
	}
}