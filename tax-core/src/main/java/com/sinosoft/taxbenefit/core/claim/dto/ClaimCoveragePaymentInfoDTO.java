package com.sinosoft.taxbenefit.core.claim.dto;


import java.util.List;

/**
 * 理赔险种赔付信息
 * 
 * @author zhangke
 *
 */
public class ClaimCoveragePaymentInfoDTO{
	/** 公司产品组代码 **/
	private String coveragePackageCode;
	/** 平台险种分类代码 **/
	private String coverageType;
	/** 公司险种代码 **/
	private String comCoverageCode;
	/** 公司险种名称 **/
	private String comCoverageName;
	/** 理赔意见 **/
	private String claimOpinion;
	/** 理赔结论代码 **/
	private String claimConclusionCode;
	/** 非正常赔付原因 **/
	private String conclusionReason;
	/** 险种赔付金额 **/
	private String claimAmount;
	/** 责任赔付信息 **/
	private List<ClaimLiabilityPaymentInfoDTO> liabilityPaymentList;

	public String getCoveragePackageCode() {
		return coveragePackageCode;
	}

	public void setCoveragePackageCode(String coveragePackageCode) {
		this.coveragePackageCode = coveragePackageCode;
	}

	public String getCoverageType() {
		return coverageType;
	}

	public void setCoverageType(String coverageType) {
		this.coverageType = coverageType;
	}

	public String getComCoverageCode() {
		return comCoverageCode;
	}

	public void setComCoverageCode(String comCoverageCode) {
		this.comCoverageCode = comCoverageCode;
	}

	public String getComCoverageName() {
		return comCoverageName;
	}

	public void setComCoverageName(String comCoverageName) {
		this.comCoverageName = comCoverageName;
	}

	public String getClaimOpinion() {
		return claimOpinion;
	}

	public void setClaimOpinion(String claimOpinion) {
		this.claimOpinion = claimOpinion;
	}

	public String getClaimConclusionCode() {
		return claimConclusionCode;
	}

	public void setClaimConclusionCode(String claimConclusionCode) {
		this.claimConclusionCode = claimConclusionCode;
	}

	public String getConclusionReason() {
		return conclusionReason;
	}

	public void setConclusionReason(String conclusionReason) {
		this.conclusionReason = conclusionReason;
	}

	public String getClaimAmount() {
		return claimAmount;
	}

	public void setClaimAmount(String claimAmount) {
		this.claimAmount = claimAmount;
	}

	public List<ClaimLiabilityPaymentInfoDTO> getLiabilityPaymentList() {
		return liabilityPaymentList;
	}

	public void setLiabilityPaymentList(
			List<ClaimLiabilityPaymentInfoDTO> liabilityPaymentList) {
		this.liabilityPaymentList = liabilityPaymentList;
	}

}
