package com.sinosoft.taxbenefit.core.common.dto;

import com.sinosoft.taxbenefit.core.ebaowebservice.RequestBody;
import com.sinosoft.taxbenefit.core.ebaowebservice.RequestHeader;

/**
 * 发送中报信报文DTO
 * @author SheChunMing
 */
public class ThirdSendDTO {
	private RequestBody body;
	private RequestHeader head;
	
	public RequestBody getBody() {
		return body;
	}
	public void setBody(RequestBody body) {
		this.body = body;
	}
	public RequestHeader getHead() {
		return head;
	}
	public void setHead(RequestHeader head) {
		this.head = head;
	}
	
}
