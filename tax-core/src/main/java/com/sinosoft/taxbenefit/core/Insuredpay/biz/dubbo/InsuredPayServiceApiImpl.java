package com.sinosoft.taxbenefit.core.Insuredpay.biz.dubbo;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqInsuredPayInfoDTO;
import com.sinosoft.taxbenefit.api.inter.InsuredPayApiService;
import com.sinosoft.taxbenefit.core.Insuredpay.biz.service.InsuredPayService;
/**
 * 被保人既往理赔额Dubbo服务
 * @author zhangyu
 * @date 2016年3月10日 上午9:50:12
 */
@Service("insuredPayServiceApiImpl")
public class InsuredPayServiceApiImpl implements InsuredPayApiService{
	@Autowired
	private InsuredPayService insuredPayService;
	@Override
	public ThirdSendResponseDTO InsuredPayInfo(ReqInsuredPayInfoDTO reqInsuredPayInfoDTO,ParamHeadDTO paramHeadDTO) {
		return insuredPayService.addInsuredPayInfo(reqInsuredPayInfoDTO,paramHeadDTO);
	}	
}