package com.sinosoft.taxbenefit.core.common.config;

/**
 * 保单变更类型
 * 
 * @author zhangke
 *
 */
public enum ENUM_TRACK_TYPE {

	RENEWALPREMLUM("01", "续保"), SECURITYGUARD("02", "保全"), CLAIM("03", "理赔"),
	APPLICANT("04", "承保"),POLICYSTATECHANGE("05","保单状态修改");
	private final String code;
	private final String desc;

	ENUM_TRACK_TYPE(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public String code() {
		return code;
	}

	public String desc() {
		return desc;
	}
}
