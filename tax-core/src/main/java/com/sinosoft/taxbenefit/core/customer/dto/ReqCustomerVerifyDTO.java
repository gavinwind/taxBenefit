package com.sinosoft.taxbenefit.core.customer.dto;

import java.util.List;
/**
 * 客户验证请求DTO（中保信）
 * @author zhangyu
 * @date 2016年3月16日 下午5:44:47
 */
public class ReqCustomerVerifyDTO {
	
	private List<CustomerVerifyDTO>  clientList;

	public List<CustomerVerifyDTO> getClientList() {
		return clientList;
	}
	public void setClientList(List<CustomerVerifyDTO> clientList) {
		this.clientList = clientList;
	}
}