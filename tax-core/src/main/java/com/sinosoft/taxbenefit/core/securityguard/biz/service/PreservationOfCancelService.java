package com.sinosoft.taxbenefit.core.securityguard.biz.service;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.securityguard.ReqEndorsementCancelInfoDTO;
import com.sinosoft.taxbenefit.core.generated.model.TaxFaultTask;

public interface PreservationOfCancelService {
	/***
	 * 保全撤销接口
	 */
	public ThirdSendResponseDTO modifyPreservationCancel(ReqEndorsementCancelInfoDTO body,ParamHeadDTO paramHeadDTO);

	/**
	 * 保全撤销
	 * @param reqJSON
	 * @param portCode
	 * @return
	 */
	public ThirdSendResponseDTO modifyCancelExceptionJob(String reqJSON, String portCode, TaxFaultTask faultTask);
}
