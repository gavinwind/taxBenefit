package com.sinosoft.taxbenefit.core.claim.biz.service;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimReversalInfoDTO;
import com.sinosoft.taxbenefit.core.generated.model.TaxFaultTask;

/**
 * 理赔接口(后置)
 * @author zhangke
 *
 */
public interface ClaimInfoCoreService{

	/**
	 * 理赔信息上传(支持单条和多条)
	 * @param reqClaimInfoDTOs 理赔信息DTO集合
	 * @param paramHeadDTO 
	 */
	ThirdSendResponseDTO addClaimSettlement(List<ReqClaimInfoDTO> reqClaimInfoDTOs,ParamHeadDTO paramHeadDTO);

	/**
	 * 理赔撤销
	 * @param reqClaimReversalDTO
	 * @param paramHeadDTO
	 * @return
	 */
	ThirdSendResponseDTO modifyClaimState(ReqClaimReversalInfoDTO bodyDTO, ParamHeadDTO paramHeadDTO);

	/**
	 * 理赔信息上传异常任务
	 * @param reqJSON 核心请求JSON
	 * @param portCode 接口编码
	 * @param faultTask 任务记录
	 * @return
	 */
	TaxFaultTask addClaimSettlementExceptionJob(String reqJSON, String portCode, Integer faultTaskId);
	
	/**
	 * 理赔撤销异常任务
	 * @param reqJSON 核心请求JSON
	 * @param portCode 接口编码
	 * @param faultTask 任务记录
	 * @return
	 */
	ThirdSendResponseDTO modifyClaimStateExceptionJob(String reqJSON, String portCode, TaxFaultTask faultTask);
}
