package com.sinosoft.taxbenefit.core.Insuredpay.dto;

import java.util.List;
/**
 * 被保人既往理赔额查询DTO（中保信）
 * @author zhangyu
 * @date 2016年3月16日 下午5:48:10
 */
public class ReqInsuredPayDTO {
	
	private List<InsuredPayClientDTO> clientList;
	
	public List<InsuredPayClientDTO> getClientList() {
		return clientList;
	}
	public void setClientList(List<InsuredPayClientDTO> clientList) {
		this.clientList = clientList;
	}
}