package com.sinosoft.taxbenefit.core.common.config;
/**
 * 续期续保类型
 * Title:ENUM_RENEWAL_TYPE
 * @author yangdongkai@outlook.com
 * @date 2016年3月10日 上午10:51:15
 */
public enum ENUM_RENEWAL_TYPE {
	POLICY_RENEWAL("01","续期"),POLICY_PREMIUM("02","续保");
	/** 枚举code */
	private String code;
	/** 枚举value或者code说明 */
	private String value;
	ENUM_RENEWAL_TYPE(String code, String value) {
		this.code = code;
		this.value = value;
	}

	public String getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}

}
