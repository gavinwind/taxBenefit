package com.sinosoft.taxbenefit.core.securityguard.dto;

import com.sinosoft.taxbenefit.api.base.EBResultCodeDTO;

public class InsuredResult {
	/**分单号 **/
	private String sequenceNo;
	/**原客户编码 	**/
	private String customerNo;
	/**新客户编码**/
	private String customerNoNew;
	
	private	EBResultCodeDTO  result;
	
	public String getSequenceNo() {
		return sequenceNo;
	}
	public void setSequenceNo(String sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public String getCustomerNoNew() {
		return customerNoNew;
	}
	public void setCustomerNoNew(String customerNoNew) {
		this.customerNoNew = customerNoNew;
	}
	public EBResultCodeDTO getResult() {
		return result;
	}
	public void setResult(EBResultCodeDTO result) {
		this.result = result;
	}
	
	

}
