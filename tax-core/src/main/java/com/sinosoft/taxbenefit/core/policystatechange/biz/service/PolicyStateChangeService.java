package com.sinosoft.taxbenefit.core.policystatechange.biz.service;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.policystate.PolicyStateEndorsement;
import com.sinosoft.taxbenefit.core.generated.model.TaxFaultTask;

public interface PolicyStateChangeService {

	/**
	 * 
	 * @param policybody
	 * @param paramHeadDTO
	 * @return
	 */
	public ThirdSendResponseDTO modifyPolicyState(List<PolicyStateEndorsement> policybody,ParamHeadDTO paramHeadDTO);

	/**
	 * 
	 * @param reqJSON
	 * @param portCode
	 * @param faultTask 任务记录
	 * @return
	 */
	public TaxFaultTask modifyPolicyStateExceptionJob(String reqJSON, String portCode, Integer faultTaskId);
}
