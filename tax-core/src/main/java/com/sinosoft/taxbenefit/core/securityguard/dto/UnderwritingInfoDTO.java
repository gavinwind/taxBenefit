package com.sinosoft.taxbenefit.core.securityguard.dto;


/**
 * 核保信息
 * 
 * @author zhanghaosh@sinosoft.com.cn
 *
 */
public class UnderwritingInfoDTO {
	/** 核保结论日期 Y **/
	private String underwritingDate;
	/** 核保决定 Y **/
	private String underwritingDecision;
	/** 核保描述 S **/
	private String underwritingDes;

	public String getUnderwritingDate() {
		return underwritingDate;
	}
	public void setUnderwritingDate(String underwritingDate) {
		this.underwritingDate = underwritingDate;
	}
	public String getUnderwritingDecision() {
		return underwritingDecision;
	}
	public void setUnderwritingDecision(String underwritingDecision) {
		this.underwritingDecision = underwritingDecision;
	}
	public String getUnderwritingDes() {
		return underwritingDes;
	}
	public void setUnderwritingDes(String underwritingDes) {
		this.underwritingDes = underwritingDes;
	}
}
