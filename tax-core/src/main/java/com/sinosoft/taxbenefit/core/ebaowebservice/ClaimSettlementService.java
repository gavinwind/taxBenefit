package com.sinosoft.taxbenefit.core.ebaowebservice;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * This class was generated by Apache CXF 2.7.17
 * 2015-08-26T03:13:50.034+08:00
 * Generated source version: 2.7.17
 * 
 */
@WebService(targetNamespace = "http://www.ciitc.com.cn/healthy", name = "ClaimSettlementService")
@XmlSeeAlso({ObjectFactory.class})
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface ClaimSettlementService {

    @WebMethod
    public void settlement(
            @WebParam(partName = "header", name = "header", targetNamespace = "http://www.ciitc.com.cn/healthy", header = true)
            RequestHeader requestHeader,
            @WebParam(partName = "request", name = "request", targetNamespace = "http://www.ciitc.com.cn/healthy")
            RequestBody request,
            @WebParam(partName = "header", mode = WebParam.Mode.OUT ,name = "header", targetNamespace = "http://www.ciitc.com.cn/healthy", header = true)
            javax.xml.ws.Holder<ResponseHeader> responseHeader,
            @WebParam(partName = "response", mode = WebParam.Mode.OUT, name = "response", targetNamespace = "http://www.ciitc.com.cn/healthy")
            javax.xml.ws.Holder<ResponseBody> response
    );
}
