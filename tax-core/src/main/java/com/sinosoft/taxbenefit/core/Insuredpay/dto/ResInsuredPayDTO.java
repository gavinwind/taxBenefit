package com.sinosoft.taxbenefit.core.Insuredpay.dto;

import java.util.List;
/**
 * 中保信被保人既往理赔额查询返回对象
 * @author zhangyu
 * @date 2016年3月10日 上午9:48:07
 */
public class ResInsuredPayDTO {
	
    private List<InsuredPayDTO> clientList;
    
	public List<InsuredPayDTO> getClientList() {
		return clientList;
	}
	public void setClientList(List<InsuredPayDTO> clientList) {
		this.clientList = clientList;
	}
}