package com.sinosoft.taxbenefit.core.generated.mapper;

import com.sinosoft.taxbenefit.core.generated.model.TaxCommonCode;
import com.sinosoft.taxbenefit.core.generated.model.TaxCommonCodeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TaxCommonCodeMapper {

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_COMMON_CODE
	 * @mbggenerated  Fri Mar 04 16:48:12 CST 2016
	 */
	int countByExample(TaxCommonCodeExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_COMMON_CODE
	 * @mbggenerated  Fri Mar 04 16:48:12 CST 2016
	 */
	int deleteByExample(TaxCommonCodeExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_COMMON_CODE
	 * @mbggenerated  Fri Mar 04 16:48:12 CST 2016
	 */
	int deleteByPrimaryKey(Integer sid);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_COMMON_CODE
	 * @mbggenerated  Fri Mar 04 16:48:12 CST 2016
	 */
	int insert(TaxCommonCode record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_COMMON_CODE
	 * @mbggenerated  Fri Mar 04 16:48:12 CST 2016
	 */
	int insertSelective(TaxCommonCode record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_COMMON_CODE
	 * @mbggenerated  Fri Mar 04 16:48:12 CST 2016
	 */
	List<TaxCommonCode> selectByExample(TaxCommonCodeExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_COMMON_CODE
	 * @mbggenerated  Fri Mar 04 16:48:12 CST 2016
	 */
	TaxCommonCode selectByPrimaryKey(Integer sid);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_COMMON_CODE
	 * @mbggenerated  Fri Mar 04 16:48:12 CST 2016
	 */
	int updateByExampleSelective(@Param("record") TaxCommonCode record,
			@Param("example") TaxCommonCodeExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_COMMON_CODE
	 * @mbggenerated  Fri Mar 04 16:48:12 CST 2016
	 */
	int updateByExample(@Param("record") TaxCommonCode record,
			@Param("example") TaxCommonCodeExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_COMMON_CODE
	 * @mbggenerated  Fri Mar 04 16:48:12 CST 2016
	 */
	int updateByPrimaryKeySelective(TaxCommonCode record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_COMMON_CODE
	 * @mbggenerated  Fri Mar 04 16:48:12 CST 2016
	 */
	int updateByPrimaryKey(TaxCommonCode record);
}