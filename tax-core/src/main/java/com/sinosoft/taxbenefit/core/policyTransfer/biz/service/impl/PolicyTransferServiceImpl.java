package com.sinosoft.taxbenefit.core.policyTransfer.biz.service.impl;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.platform.common.config.SystemConstants;
import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.platform.common.util.DateUtil;
import com.sinosoft.platform.common.util.ReflectionUtil;
import com.sinosoft.platform.common.util.StringUtil;
import com.sinosoft.taxbenefit.api.base.EBResultCodeDTO;
import com.sinosoft.taxbenefit.api.base.HXResultCodeDTO;
import com.sinosoft.taxbenefit.api.config.ENUM_BZ_RESPONSE_RESULT;
import com.sinosoft.taxbenefit.api.config.ENUM_DISPOSE_TYPE;
import com.sinosoft.taxbenefit.api.config.ENUM_SENDSERVICE_CODE;
import com.sinosoft.taxbenefit.api.dto.request.TaxFaulTaskDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.CoreRequestHead;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqMInPolicyTransCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqMInPolicyTransferDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqMOutPolicyTransBodyDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqMOutPolicyTransCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqPolicyInTransferQueryDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqPolicyTransClientDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqPolicyTransferOutDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqSInPolicyTransCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqSInPolicyTransferDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqSOutPolicyTransCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqSOutPolicyTransferDTO;
import com.sinosoft.taxbenefit.api.dto.response.policytrans.ReqHXPolicyInTransferQueryDTO;
import com.sinosoft.taxbenefit.api.dto.response.policytrans.ResHXPolicyInTransferBalanceDTO;
import com.sinosoft.taxbenefit.api.dto.response.policytrans.ResHXPolicyTransClientDTO;
import com.sinosoft.taxbenefit.api.dto.response.policytrans.ResHXPolicyTransOutRegisterDTO;
import com.sinosoft.taxbenefit.api.dto.response.policytrans.ResHXPolicyTransferInDTO;
import com.sinosoft.taxbenefit.api.dto.response.policytrans.ResHXPolicyTransferOutDTO;
import com.sinosoft.taxbenefit.api.dto.response.policytrans.ResHXPolicyTransferResultDTO;
import com.sinosoft.taxbenefit.core.common.biz.service.CommonCodeService;
import com.sinosoft.taxbenefit.core.common.config.ENUM_BUSINESS_RESULR;
import com.sinosoft.taxbenefit.core.common.config.ENUM_EBAO_SERVICE_PORT_CODE;
import com.sinosoft.taxbenefit.core.common.config.ENUM_HX_SERVICE_PORT_CODE;
import com.sinosoft.taxbenefit.core.common.config.ENUM_RC_STATE;
import com.sinosoft.taxbenefit.core.common.config.ENUM_RESULT_CODE;
import com.sinosoft.taxbenefit.core.common.dto.BookSequenceDTO;
import com.sinosoft.taxbenefit.core.common.dto.EbaoServerPortDTO;
import com.sinosoft.taxbenefit.core.common.dto.ReqBusinessDTO;
import com.sinosoft.taxbenefit.core.common.dto.ReqBusinessDetailDTO;
import com.sinosoft.taxbenefit.core.common.dto.SendLogDTO;
import com.sinosoft.taxbenefit.core.common.dto.ThirdSendDTO;
import com.sinosoft.taxbenefit.core.ebaowebservice.AsynResultQueryService;
import com.sinosoft.taxbenefit.core.ebaowebservice.AsynResultQueryService_Service;
import com.sinosoft.taxbenefit.core.ebaowebservice.PolicyTransOutRegisterQueryService;
import com.sinosoft.taxbenefit.core.ebaowebservice.PolicyTransOutRegisterQueryService_Service;
import com.sinosoft.taxbenefit.core.ebaowebservice.PolicyTransOutRegisterService;
import com.sinosoft.taxbenefit.core.ebaowebservice.PolicyTransOutRegisterService_Service;
import com.sinosoft.taxbenefit.core.ebaowebservice.PolicyTransformInService;
import com.sinosoft.taxbenefit.core.ebaowebservice.PolicyTransformInService_Service;
import com.sinosoft.taxbenefit.core.ebaowebservice.PolicyTransformOutService;
import com.sinosoft.taxbenefit.core.ebaowebservice.PolicyTransformOutService_Service;
import com.sinosoft.taxbenefit.core.ebaowebservice.PolicyTransformRemainderQueryService;
import com.sinosoft.taxbenefit.core.ebaowebservice.PolicyTransformRemainderQueryService_Service;
import com.sinosoft.taxbenefit.core.ebaowebservice.ResponseBody;
import com.sinosoft.taxbenefit.core.ebaowebservice.ResponseHeader;
import com.sinosoft.taxbenefit.core.ebaowebservice.ResponseJSON;
import com.sinosoft.taxbenefit.core.faultTask.biz.service.FaultTaskService;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxContTransferMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxFaultTaskMapper;
import com.sinosoft.taxbenefit.core.generated.model.TaxCont;
import com.sinosoft.taxbenefit.core.generated.model.TaxContTransfer;
import com.sinosoft.taxbenefit.core.generated.model.TaxFaultTask;
import com.sinosoft.taxbenefit.core.policy.biz.service.PolicyService;
import com.sinosoft.taxbenefit.core.policyTransfer.biz.service.PolicyTransferService;
import com.sinosoft.taxbenefit.core.policyTransfer.config.ENUM_POLICYTRANS_TYPE;
import com.sinosoft.taxbenefit.core.policyTransfer.dto.EPolicyTransClientDTO;
import com.sinosoft.taxbenefit.core.policyTransfer.dto.EPolicyTransferOutBodyDTO;
import com.sinosoft.taxbenefit.core.policyTransfer.dto.EPolicyTransferOutDTO;
import com.sinosoft.taxbenefit.core.policyTransfer.dto.ReqEPolicyInTransferQueryDTO;
import com.sinosoft.taxbenefit.core.policyTransfer.dto.ReqEPolicyTransferBodyDTO;
import com.sinosoft.taxbenefit.core.policyTransfer.dto.ReqEPolicyTransferDTO;
import com.sinosoft.taxbenefit.core.policyTransfer.dto.ResEPolicyInTransferBalanceDTO;
import com.sinosoft.taxbenefit.core.policyTransfer.dto.ResEPolicyTransOutQueryDTO;
import com.sinosoft.taxbenefit.core.policyTransfer.dto.ResEPolicyTransOutRegisterQueryDTO;
import com.sinosoft.taxbenefit.core.policyTransfer.dto.ResEPolicyTransferDTO;

@Service
public class PolicyTransferServiceImpl extends TaxBaseService implements PolicyTransferService {
	@Autowired
	TaxContTransferMapper taxContTransferMapper;
	@Autowired
	CommonCodeService commonCodeService;
	@Autowired
	FaultTaskService faultTaskService;
	@Autowired
	TaxFaultTaskMapper taxFaultTaskMapper;//异常任务
	@Autowired
	PolicyService policyService;
	@Override
	public ThirdSendResponseDTO addPolicyTransSInSettlement(ReqSInPolicyTransferDTO transInfo, ParamHeadDTO paramHead) {
		ThirdSendResponseDTO thirdSendResponse = new ThirdSendResponseDTO();
		try {
			List<ReqBusinessDetailDTO> reqBusinessDetailList = new ArrayList<ReqBusinessDetailDTO>();
			ReqBusinessDTO businessDTO = commonCodeService.initReqBusinessDTO();
			logger.info("单笔保单转入申请上传（核心请求）JSON 体数据内容："+JSONObject.toJSONString(transInfo));
			//1.将请求dto转换为中保信理赔请求
			ReqPolicyTransferOutDTO reqPolicyTransferOut = transInfo.getTransferOut();
			List<ReqPolicyTransferOutDTO> reqPolicyTransferList = new ArrayList<ReqPolicyTransferOutDTO>();
			reqPolicyTransferList.add(reqPolicyTransferOut);
			List<EPolicyTransferOutDTO> reqEPolicyTransferOutList = this.encapsulationPolicyTransferOutList(reqPolicyTransferList);
			// 封装转移保单中的转入信息
			ReqEPolicyTransferDTO reqEPolicyTransfer = new ReqEPolicyTransferDTO();
			ReflectionUtil.copyProperties(transInfo, reqEPolicyTransfer);
			reqEPolicyTransfer.setTransferOutList(reqEPolicyTransferOutList);
			
			ReqEPolicyTransferBodyDTO reqEPolicyTransferBody = new ReqEPolicyTransferBodyDTO();
			reqEPolicyTransferBody.setTransferIn(reqEPolicyTransfer);
			
			ThirdSendDTO thirdSendDTO = commonCodeService.createThirdSend(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_IN_TRANSFER_UPLOAD.code(), paramHead, reqEPolicyTransferBody);
			logger.info("单笔保单转入申请上传（请求中保信）JSON 数据内容："+JSONObject.toJSONString(thirdSendDTO));
			//2.调用中保信接口服务,获得返回结果   存储调用中保信日志表
			Holder<ResponseHeader> responseHeaderHolder = new Holder<ResponseHeader>();
			Holder<ResponseBody> responseBodyHolder = new Holder<ResponseBody>();
			EbaoServerPortDTO ebaoServerPort = commonCodeService.queryEBaoServerInfoByPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_IN_TRANSFER_UPLOAD.code());
			String portUrl = ebaoServerPort.getPortUrl();
			URL wsdlLocation = new URL(portUrl);
			PolicyTransformInService_Service service = new PolicyTransformInService_Service(wsdlLocation);
			PolicyTransformInService servicePort = service.getPolicyTransformInServicePort();
			BindingProvider bp = (BindingProvider) servicePort;
			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, portUrl);
			servicePort.transformIn(thirdSendDTO.getHead(), thirdSendDTO.getBody(), responseHeaderHolder, responseBodyHolder);
			
			ResponseHeader responseHeader = responseHeaderHolder.value;
			ResponseBody responseBody = responseBodyHolder.value;
			logger.info("单笔保单转入申请上传（中保信响应）JSON 数据内容："+responseBody.getJsonString());
			//封装中保信返回报文
			ResponseJSON responseJSON = new ResponseJSON();
			responseJSON.setResponseBody(responseBody);
			responseJSON.setResponseHeader(responseHeader);
			//封装发送报文日志对象
			SendLogDTO sendLog = new SendLogDTO();
			sendLog.setSerialNo(paramHead.getSerialNo());
			sendLog.setPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_IN_TRANSFER_UPLOAD.code());
			sendLog.setRequestContent(JSONObject.toJSONString(thirdSendDTO));
			sendLog.setResponseContent(JSONObject.toJSONString(responseJSON));
			sendLog.setTargetCode(ENUM_SENDSERVICE_CODE.EBAO.code());
			sendLog.setState(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
			sendLog.setDescription(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
			commonCodeService.addLogSend(sendLog);
			//3.封装返回核心dto;
			if(ENUM_RESULT_CODE.SUCCESS.code().equals(responseHeader.getResultCode())
					|| ENUM_RESULT_CODE.INPUT_LOCALITY_SUCCESS.code().equals(responseHeader.getResultCode())){
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
				thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
				// 成功数据持久化
				ReqMInPolicyTransferDTO transInfoMIn = new ReqMInPolicyTransferDTO();
				ReflectionUtil.copyProperties(transInfo, transInfoMIn);
				List<ReqPolicyTransferOutDTO> transferOutList = new ArrayList<ReqPolicyTransferOutDTO>();
				transferOutList.add(transInfo.getTransferOut());
				transInfoMIn.setTransferOutList(transferOutList);
				ResEPolicyTransferDTO resEPolicyTransfer = JSONObject.parseObject(responseBody.getJsonString(), ResEPolicyTransferDTO.class);
				this.savePolicyTransIn(transInfoMIn, resEPolicyTransfer);
			}else{
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
				List list = responseHeader.getResultMessage().getMessage();
				if(list != null && list.size() > 0){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : responseHeader.getResultMessage().getMessage()){
						sbStr.append(mess).append("|");
					}
					String message = sbStr.substring(0, sbStr.length()-1).toString();
					thirdSendResponse.setResultDesc(message);
				}else{
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
				}
			}
			// 获取业务数据
			if(!StringUtil.isEmpty(responseBody.getJsonString())){
				ResEPolicyTransferDTO resEPolicyTransfer = JSONObject.parseObject(responseBody.getJsonString(), ResEPolicyTransferDTO.class);
				if(null != resEPolicyTransfer.getTransferList()){
					List<EPolicyTransferOutDTO> ePolicyTransferOutList = resEPolicyTransfer.getTransferList();
					if(ePolicyTransferOutList.size() > 0){
						if(null != ePolicyTransferOutList.get(0).getClientList()){
							List<EPolicyTransClientDTO> ePolicyTransClientList = ePolicyTransferOutList.get(0).getClientList();
							if(ePolicyTransClientList.size() > 0){
								ResHXPolicyTransferResultDTO policyTransferResultDTO = new ResHXPolicyTransferResultDTO();
								String bizNo = transInfo.getTransferOut().getBizNo();
								String transferSequenceNo = ePolicyTransClientList.get(0).getTransferSequenceNo();
								EBResultCodeDTO eBResultCode = ePolicyTransClientList.get(0).getResult();
								HXResultCodeDTO hxResultCode = new HXResultCodeDTO();
								hxResultCode.setResultCode(eBResultCode.getResultCode());
								String message = "";
								if(null != eBResultCode.getResultMessage()){
									StringBuffer sbStr = new StringBuffer(); 
									for(String mess : eBResultCode.getResultMessage()){
										sbStr.append(mess).append("|");
									}
									message = sbStr.substring(0, sbStr.length()-1).toString();
								}
								hxResultCode.setResultMessage(message);
								policyTransferResultDTO.setBizNo(bizNo);
								policyTransferResultDTO.setTransferSequenceNo(transferSequenceNo);
								policyTransferResultDTO.setResult(hxResultCode);
								thirdSendResponse.setResJson(policyTransferResultDTO);
								
								if(ENUM_RESULT_CODE.SUCCESS.code().equals(eBResultCode.getResultCode())){
									businessDTO.setReqSuccessNum(businessDTO.getReqSuccessNum() + 1);
								}else{
									businessDTO.setReqFailNum(businessDTO.getReqFailNum() + 1);
									ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
									reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
									reqBusinessDetailDTO.setResultMessage(message);
									reqBusinessDetailList.add(reqBusinessDetailDTO);
								}
							}
						}
					}
				}
				
			}else{
				businessDTO.setReqFailNum(responseHeader.getRecordNum());
				ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
				reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
				reqBusinessDetailDTO.setResultMessage(thirdSendResponse.getResultDesc());
				reqBusinessDetailList.add(reqBusinessDetailDTO);
			}
			// 添加失败交易记录
			commonCodeService.addReqBusinessDetail(reqBusinessDetailList);
			businessDTO.setRequetNum(responseHeader.getRecordNum());
			// 添加请求交易记录数
			commonCodeService.addReqBusinessCollate(businessDTO);
		} catch (Exception e) {
			e.printStackTrace();
			thirdSendResponse.setResJson(e.getMessage());
			thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
			thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
			// 存储任务表
			TaxFaulTaskDTO taxFaulTask = new TaxFaulTaskDTO();
			taxFaulTask.setSerialNo(paramHead.getSerialNo());
			taxFaulTask.setAreaCode(paramHead.getAreaCode());
			taxFaulTask.setServerPortCode(paramHead.getPortCode());
			taxFaulTask.setServerPortName(paramHead.getPortName());
			taxFaulTask.setDisposeType(ENUM_DISPOSE_TYPE.ERROR.code());
			faultTaskService.addTaxFaultTask(taxFaulTask);
			logger.error("单笔保单转入申请上传后置处理发生异常!异常信息为："+e.getMessage());
		} 
		logger.info("单笔保单转入申请上传后置处理结束!");
		return thirdSendResponse;
	}

	@Override
	public ThirdSendResponseDTO addPolicyTransMInSettlement(ReqMInPolicyTransferDTO transInfo, ParamHeadDTO paramHead) {
		ThirdSendResponseDTO thirdSendResponse = new ThirdSendResponseDTO();
		try {
			List<ReqBusinessDetailDTO> reqBusinessDetailList = new ArrayList<ReqBusinessDetailDTO>();
			ReqBusinessDTO businessDTO = commonCodeService.initReqBusinessDTO();
			logger.info("多笔保单转入申请上传（核心请求）JSON 体数据内容："+JSONObject.toJSONString(transInfo));
			
			List<HXResultCodeDTO> resultCodeList = new ArrayList<HXResultCodeDTO>();
			List<ResHXPolicyTransferResultDTO> resHXPolicyTransferResultList = new ArrayList<ResHXPolicyTransferResultDTO>();
 			for(ReqPolicyTransferOutDTO reqPolicyTransferOut : transInfo.getTransferOutList()){
				ReqSInPolicyTransferDTO reqSInPolicyTransferDTO = new ReqSInPolicyTransferDTO();
				reqSInPolicyTransferDTO.setAccountHolder(transInfo.getAccountHolder());
				reqSInPolicyTransferDTO.setAccountNo(transInfo.getAccountNo());
				reqSInPolicyTransferDTO.setBankName(transInfo.getBankName());
				reqSInPolicyTransferDTO.setContactEmail(transInfo.getContactEmail());
				reqSInPolicyTransferDTO.setContactName(transInfo.getContactName());
				reqSInPolicyTransferDTO.setContactTele(transInfo.getContactTele());
				reqSInPolicyTransferDTO.setTransApplyDate(transInfo.getTransApplyDate());
				
				ReqPolicyTransferOutDTO reqPolicyTransferOutDTO = new ReqPolicyTransferOutDTO();
				reqPolicyTransferOutDTO.setBizNo(reqPolicyTransferOut.getBizNo());
				reqPolicyTransferOutDTO.setClientList(reqPolicyTransferOut.getClientList());
				reqPolicyTransferOutDTO.setCompanyCode(reqPolicyTransferOut.getCompanyCode());
				reqPolicyTransferOutDTO.setCompanyName(reqPolicyTransferOut.getCompanyName());
				reqPolicyTransferOutDTO.setCustomerNo(reqPolicyTransferOut.getCustomerNo());
				reqPolicyTransferOutDTO.setPolicyNo(reqPolicyTransferOut.getPolicyNo());
				
				reqSInPolicyTransferDTO.setTransferOut(reqPolicyTransferOutDTO);
				ThirdSendResponseDTO thirdSendResponseDTO = this.addPolicyTransSInSettlement(reqSInPolicyTransferDTO, paramHead);
				
				ResHXPolicyTransferResultDTO resHXPolicyTransfer = (ResHXPolicyTransferResultDTO) thirdSendResponseDTO.getResJson();
				resHXPolicyTransferResultList.add(resHXPolicyTransfer);
			}
 			thirdSendResponse.setResJson(resHXPolicyTransferResultList);
 			thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
			thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
		} catch (Exception e) {
			logger.error("多笔保单转入申请上传后置处理发生异常!异常信息为："+e.getMessage());
			thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
			thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
			// 存储任务表
			TaxFaulTaskDTO taxFaulTask = new TaxFaulTaskDTO();
			taxFaulTask.setSerialNo(paramHead.getSerialNo());
			taxFaulTask.setAreaCode(paramHead.getAreaCode());
			taxFaulTask.setServerPortCode(paramHead.getPortCode());
			taxFaulTask.setServerPortName(paramHead.getPortName());
			taxFaulTask.setDisposeType(ENUM_DISPOSE_TYPE.ERROR.code());
			faultTaskService.addTaxFaultTask(taxFaulTask);
			e.printStackTrace();
		}
		return thirdSendResponse;
			
			
//			//1.将请求dto转换为中保信理赔请求dto
//			List<ReqPolicyTransferOutDTO> reqPolicyTransferList = transInfo.getTransferOutList();
//			List<EPolicyTransferOutDTO> reqEPolicyTransferOutList = this.encapsulationPolicyTransferOutList(reqPolicyTransferList);
//			// 封装转移保单中的转入信息
//			ReqEPolicyTransferDTO reqEPolicyTransfer = new ReqEPolicyTransferDTO();
//			ReflectionUtil.copyProperties(transInfo, reqEPolicyTransfer);
//			reqEPolicyTransfer.setTransferOutList(reqEPolicyTransferOutList);
//			
//			ReqEPolicyTransferBodyDTO reqEPolicyTransferBody = new ReqEPolicyTransferBodyDTO();
//			reqEPolicyTransferBody.setTransferIn(reqEPolicyTransfer);
//			ThirdSendDTO thirdSendDTO = commonCodeService.createThirdSend(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_IN_TRANSFER_UPLOAD.code(), paramHead, reqEPolicyTransferBody);
//			logger.info("多笔保单转入申请上传（请求中保信）JSON 数据内容："+JSONObject.toJSONString(thirdSendDTO));
//			//2.调用中保信接口服务,获得返回结果   存储调用中保信日志表
//			Holder<ResponseHeader> responseHeaderHolder = new Holder<ResponseHeader>();
//			Holder<ResponseBody> responseBodyHolder = new Holder<ResponseBody>();
//			
//			EbaoServerPortDTO ebaoServerPort = commonCodeService.queryEBaoServerInfoByPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_IN_TRANSFER_UPLOAD.code());
//			String portUrl = ebaoServerPort.getPortUrl();
//			URL wsdlLocation = new URL(portUrl);
//			PolicyTransformInService_Service service = new PolicyTransformInService_Service(wsdlLocation);
//			PolicyTransformInService servicePort = service.getPolicyTransformInServicePort();
//			BindingProvider bp = (BindingProvider) servicePort;
//			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, portUrl);
//			servicePort.transformIn(thirdSendDTO.getHead(), thirdSendDTO.getBody(), responseHeaderHolder, responseBodyHolder);
//			
//			ResponseHeader responseHeader = responseHeaderHolder.value;
//			ResponseBody responseBody = responseBodyHolder.value;
//			logger.info("多笔保单转入申请上传（中保信响应）JSON 数据内容：" + responseBody.getJsonString());
//			//封装中保信返回报文
//			ResponseJSON responseJSON = new ResponseJSON();
//			responseJSON.setResponseBody(responseBody);
//			responseJSON.setResponseHeader(responseHeader);
//			//封装发送报文日志对象
//			SendLogDTO sendLog = new SendLogDTO();
//			sendLog.setSerialNo(paramHead.getSerialNo());
//			sendLog.setPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_IN_TRANSFER_UPLOAD.code());
//			sendLog.setRequestContent(JSONObject.toJSONString(thirdSendDTO));
//			sendLog.setResponseContent(JSONObject.toJSONString(responseJSON));
//			sendLog.setTargetCode(ENUM_SENDSERVICE_CODE.EBAO.code());
//			sendLog.setState(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
//			sendLog.setDescription(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
//			commonCodeService.addLogSend(sendLog);
//			//3.封装返回核心dto;
//			if(ENUM_RESULT_CODE.SUCCESS.code().equals(responseHeader.getResultCode())){
//				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
//				thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
//				//4.存储业务表数据
//				ResEPolicyTransferDTO resEPolicyTransfer = JSONObject.parseObject(responseBody.getJsonString(), ResEPolicyTransferDTO.class);
//				this.savePolicyTransIn(transInfo, resEPolicyTransfer);
//			}
////			else if(ENUM_RESULT_CODE.HEADER_ERROR.code().equals(responseHeader.getResultCode()) || ENUM_RESULT_CODE.INPUT_ERROR.code().equals(responseHeader.getResultCode())){
////				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
////				String message = ENUM_BZ_RESPONSE_RESULT.FAIL.description();
////				if(null != responseHeader.getResultMessage().getMessage() && responseHeader.getResultMessage().getMessage().size() > 0){
////					StringBuffer sbStr = new StringBuffer(); 
////					for(String mess : responseHeader.getResultMessage().getMessage()){
////						sbStr.append(mess).append("|");
////					}
////					message = sbStr.substring(0, sbStr.length()-1).toString();
////				}
////				thirdSendResponse.setResultDesc(message);
////			}
//			else{
//				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
//				List list = responseHeader.getResultMessage().getMessage();
//				if(list != null && list.size() > 0){
//					StringBuffer sbStr = new StringBuffer(); 
//					for(String mess : responseHeader.getResultMessage().getMessage()){
//						sbStr.append(mess).append("|");
//					}
//					String message = sbStr.substring(0, sbStr.length()-1).toString();
//					thirdSendResponse.setResultDesc(message);
//				}else{
//					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
//				}
//			}
//			// 获取业务数据
//			if(!StringUtil.isEmpty(responseBody.getJsonString())){
//				List<ResHXPolicyTransferResultDTO> resHXPolicyTransferResult = new ArrayList<ResHXPolicyTransferResultDTO>();
//				ResEPolicyTransferDTO resEPolicyTransfer = JSONObject.parseObject(responseBody.getJsonString(), ResEPolicyTransferDTO.class);
//				List<EPolicyTransferOutDTO> ePolicyTransferOutList = resEPolicyTransfer.getTransferList();
//				// 成功记录数
//				Integer successNum = 0;
//				// 失败记录数
//				Integer failNum = 0;
//				for (EPolicyTransferOutDTO ePolicyTransferOutDTO : ePolicyTransferOutList) {
//					for (ReqPolicyTransferOutDTO reqPolicyTransferOutDTO : reqPolicyTransferList) {
//						if(reqPolicyTransferOutDTO.getPolicyNo().equals(ePolicyTransferOutDTO.getPolicyNo())){
//							EBResultCodeDTO eBResultCode = ePolicyTransferOutDTO.getClientList().get(0).getResult();
//							HXResultCodeDTO hxResultCode = new HXResultCodeDTO();
//							String message = "";
//							if(null != eBResultCode.getResultMessage()){
//								StringBuffer sbStr = new StringBuffer(); 
//								for(String mess : eBResultCode.getResultMessage()){
//									sbStr.append(mess).append("|");
//								}
//								message = sbStr.substring(0, sbStr.length()-1).toString();
//							}
//							hxResultCode.setResultMessage(message);
//							// 获取多笔转出信息对应核心的业务号
//							ResHXPolicyTransferResultDTO policyTransferResultDTO = new ResHXPolicyTransferResultDTO();
//							policyTransferResultDTO.setBizNo(reqPolicyTransferOutDTO.getBizNo());
//							policyTransferResultDTO.setTransferSequenceNo(ePolicyTransferOutDTO.getClientList().get(0).getTransferSequenceNo());
//							policyTransferResultDTO.setResult(hxResultCode);
//							resHXPolicyTransferResult.add(policyTransferResultDTO);
//							//记录成功和失败记录数
//							if(ENUM_RESULT_CODE.SUCCESS.code().equals(eBResultCode.getResultCode())){
//								successNum+=1;
//							}else{
//								failNum+=1;
//								ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
//								reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
//								reqBusinessDetailDTO.setResultMessage(message);
//								reqBusinessDetailList.add(reqBusinessDetailDTO);
//							}
//						}
//					}
//				}
//				// 登记请求记录
//				businessDTO.setReqSuccessNum(successNum);
//				businessDTO.setReqFailNum(failNum);
//				thirdSendResponse.setResJson(resHXPolicyTransferResult);
//			}else{
//				businessDTO.setReqFailNum(responseHeader.getRecordNum());
//				for (int i = 0; i < responseHeader.getRecordNum(); i++) {
//					ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
//					reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
//					reqBusinessDetailDTO.setResultMessage(thirdSendResponse.getResultDesc());
//					reqBusinessDetailList.add(reqBusinessDetailDTO);
//				}
//			}
//			// 添加失败交易记录
//			commonCodeService.addReqBusinessDetail(reqBusinessDetailList);
//			businessDTO.setRequetNum(responseHeader.getRecordNum());
//			// 添加请求交易记录数
//			commonCodeService.addReqBusinessCollate(businessDTO);
//		} catch (Exception e) {
//			logger.error("多笔保单转入申请上传后置处理发生异常!异常信息为："+e.getMessage());
//			thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
//			thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
//			// 存储任务表
//			TaxFaulTaskDTO taxFaulTask = new TaxFaulTaskDTO();
//			taxFaulTask.setSerialNo(paramHead.getSerialNo());
//			taxFaulTask.setServerPortCode(paramHead.getPortCode());
//			taxFaulTask.setServerPortName(paramHead.getPortName());
//			taxFaulTask.setDisposeType(ENUM_DISPOSE_TYPE.ERROR.code());
//			faultTaskService.addTaxFaultTask(taxFaulTask);
//			e.printStackTrace();
//		}
//		logger.info("多笔保单转入申请上传后置处理结束!");
//		return thirdSendResponse;
	}
	/**
	 * 保单转入 - 转出信息(统一变成多笔交易)
	 * 核心对象 -- 封装  --中保信对象
	 * @param reqTransferOutList
	 * @return
	 */
	public List<EPolicyTransferOutDTO> encapsulationPolicyTransferOutList(List<ReqPolicyTransferOutDTO> reqTransferOutList){
		// 封装多条转出信息
		List<EPolicyTransferOutDTO> policyTransferOutList = new ArrayList<EPolicyTransferOutDTO>();
		for (ReqPolicyTransferOutDTO reqPolicyTransferOut : reqTransferOutList) {
			// 获取转移保单中的客户信息
			List<ReqPolicyTransClientDTO> reqClient = reqPolicyTransferOut.getClientList();
			// 封装转移保单中的客户信息
			List<EPolicyTransClientDTO> policyTransClientList = new ArrayList<EPolicyTransClientDTO>();
			for (ReqPolicyTransClientDTO reqPolicyTransClientDTO : reqClient) {
				EPolicyTransClientDTO policyTransClient = new EPolicyTransClientDTO();
				ReflectionUtil.copyProperties(reqPolicyTransClientDTO, policyTransClient);
				policyTransClientList.add(policyTransClient);
			}
			// 封装转移保单中的转出信息
			EPolicyTransferOutDTO policyTransferOut = new EPolicyTransferOutDTO();
			ReflectionUtil.copyProperties(reqPolicyTransferOut, policyTransferOut);
			policyTransferOut.setClientList(policyTransClientList);
			policyTransferOutList.add(policyTransferOut);
		}
		return policyTransferOutList;
	}
	@Override
	public ThirdSendResponseDTO addPolicyTransSOutSettlement(ReqSOutPolicyTransferDTO transInfo, ParamHeadDTO paramHead) {
		ThirdSendResponseDTO thirdSendResponse = new ThirdSendResponseDTO();
		try {
			List<ReqBusinessDetailDTO> reqBusinessDetailList = new ArrayList<ReqBusinessDetailDTO>();
			ReqBusinessDTO businessDTO = commonCodeService.initReqBusinessDTO();
			logger.info("单笔保单转出登记上传（核心请求）JSON 体数据内容：" + JSONObject.toJSONString(transInfo));
			//1.将保单转出登记上传请求dto转换为中保信保单转出登记上传请求dto
			List<ReqPolicyTransferOutDTO> reqTransferOutList = new ArrayList<ReqPolicyTransferOutDTO>();
			ReqPolicyTransferOutDTO reqPolicyTransferOut = new ReqPolicyTransferOutDTO();
			ReflectionUtil.copyProperties(transInfo, reqPolicyTransferOut);
			reqTransferOutList.add(reqPolicyTransferOut);
			List<EPolicyTransferOutDTO> reqEPolicyTransferOutList = this.encapsulationPolicyTransferOutList(reqTransferOutList);
			EPolicyTransferOutBodyDTO ePolicyTransferOutBody = new EPolicyTransferOutBodyDTO();
			ePolicyTransferOutBody.setTransferOutList(reqEPolicyTransferOutList);
			// 封装请求中保信报文
			ThirdSendDTO thirdSendDTO = commonCodeService.createThirdSend(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_RECORD.code(), paramHead, ePolicyTransferOutBody);
			logger.info("单笔保单转出登记上传（请求中保信）JSON 数据内容："+JSONObject.toJSONString(thirdSendDTO));
			//2.调用中保信接口服务,获得返回结果   存储调用中保信日志表
			Holder<ResponseHeader> responseHeaderHolder = new Holder<ResponseHeader>();
			Holder<ResponseBody> responseBodyHolder = new Holder<ResponseBody>();
			
			EbaoServerPortDTO ebaoServerPort = commonCodeService.queryEBaoServerInfoByPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_RECORD.code());
			String portUrl = ebaoServerPort.getPortUrl();
			URL wsdlLocation = new URL(portUrl);
			PolicyTransOutRegisterService_Service service = new PolicyTransOutRegisterService_Service(wsdlLocation);
			PolicyTransOutRegisterService servicePort = service.getPolicyTransOutRegisterServicePort();
			BindingProvider bp = (BindingProvider) servicePort;
			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, portUrl);
			servicePort.transOutRegister(thirdSendDTO.getHead(), thirdSendDTO.getBody(), responseHeaderHolder, responseBodyHolder);
			
			ResponseHeader responseHeader = responseHeaderHolder.value;
			ResponseBody responseBody = responseBodyHolder.value;
			logger.info("单笔保单转出登记上传（中保信响应）JSON 数据内容："+responseBody.getJsonString());
			//封装中保信返回报文
			ResponseJSON responseJSON = new ResponseJSON();
			responseJSON.setResponseBody(responseBody);
			responseJSON.setResponseHeader(responseHeader);
			//封装发送报文日志对象
			SendLogDTO sendLog = new SendLogDTO();
			sendLog.setSerialNo(paramHead.getSerialNo());
			sendLog.setPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_RECORD.code());
			sendLog.setRequestContent(JSONObject.toJSONString(thirdSendDTO));
			sendLog.setResponseContent(JSONObject.toJSONString(responseJSON));
			sendLog.setTargetCode(ENUM_SENDSERVICE_CODE.EBAO.code());
			sendLog.setState(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
			sendLog.setDescription(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
			commonCodeService.addLogSend(sendLog);
			//3.封装返回核心dto;
			if(ENUM_RESULT_CODE.SUCCESS.code().equals(responseHeader.getResultCode())
					|| ENUM_RESULT_CODE.INPUT_LOCALITY_SUCCESS.code().equals(responseHeader.getResultCode())){
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
				thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
				List<ReqSOutPolicyTransferDTO> transInfoList = new ArrayList<ReqSOutPolicyTransferDTO>();
				transInfoList.add(transInfo);
				ResEPolicyTransferDTO resEPolicyTransfer = JSONObject.parseObject(responseBody.getJsonString(), ResEPolicyTransferDTO.class);
				this.savePolicyTransOut(transInfoList, resEPolicyTransfer);
			}else{
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
				List list = responseHeader.getResultMessage().getMessage();
				if(list != null && list.size() > 0){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : responseHeader.getResultMessage().getMessage()){
						sbStr.append(mess).append("|");
					}
					String message = sbStr.substring(0, sbStr.length()-1).toString();
					thirdSendResponse.setResultDesc(message);
				}else{
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
				}
			}
			boolean isRegisterFaulTask = false;
			// 获取业务数据
			if(!StringUtil.isEmpty(responseBody.getJsonString())){
				// 解析中保信返回的json
				ResEPolicyTransferDTO resEPolicyTransfer = JSONObject.parseObject(responseBody.getJsonString(), ResEPolicyTransferDTO.class);
				if(null != resEPolicyTransfer.getTransferList()){
					EBResultCodeDTO eBResultCode = resEPolicyTransfer.getTransferList().get(0).getClientList().get(0).getResult();
					HXResultCodeDTO hxResultCode = new HXResultCodeDTO();
					hxResultCode.setResultCode(eBResultCode.getResultCode());
					String message = "";
					if(null != eBResultCode.getResultMessage()){
						StringBuffer sbStr = new StringBuffer(); 
						for(String mess : eBResultCode.getResultMessage()){
							sbStr.append(mess).append("|");
						}
						message = sbStr.substring(0, sbStr.length()-1).toString();
					}
					hxResultCode.setResultMessage(message);
					ResHXPolicyTransferResultDTO policyTransferResultDTO = new ResHXPolicyTransferResultDTO();
					policyTransferResultDTO.setBizNo(transInfo.getBizNo());
					policyTransferResultDTO.setTransferSequenceNo(resEPolicyTransfer.getTransferList().get(0).getClientList().get(0).getTransferSequenceNo());
					policyTransferResultDTO.setResult(hxResultCode);
					thirdSendResponse.setResJson(policyTransferResultDTO);
					// 登记请求记录
					if(ENUM_RESULT_CODE.SUCCESS.code().equals(eBResultCode.getResultCode())){
						businessDTO.setReqSuccessNum(businessDTO.getReqSuccessNum() + 1);
					}else{
						businessDTO.setReqFailNum(businessDTO.getReqFailNum() + 1);
						ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
						reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
						reqBusinessDetailDTO.setResultMessage(message);
						reqBusinessDetailList.add(reqBusinessDetailDTO);
					}
					isRegisterFaulTask = true;
				}else{
					BookSequenceDTO bookSequence = JSONObject.parseObject(responseBody.getJsonString(), BookSequenceDTO.class);
					TaxFaulTaskDTO taxFaulTask = new TaxFaulTaskDTO();
					taxFaulTask.setSerialNo(paramHead.getSerialNo());
					taxFaulTask.setAreaCode(paramHead.getAreaCode());
					taxFaulTask.setServerPortCode(paramHead.getPortCode());
					taxFaulTask.setServerPortName(paramHead.getPortName());
					taxFaulTask.setDisposeType(ENUM_DISPOSE_TYPE.APPOINTMENT.code());
					taxFaulTask.setBookSequenceNo(bookSequence.getBookingSequenceNo());
					faultTaskService.addTaxFaultTask(taxFaulTask);
					thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
				}
			}else{
				businessDTO.setReqFailNum(responseHeader.getRecordNum());
				for (int i = 0; i < responseHeader.getRecordNum(); i++) {
					ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
					reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
					reqBusinessDetailDTO.setResultMessage(thirdSendResponse.getResultDesc());
					reqBusinessDetailList.add(reqBusinessDetailDTO);
				}
			}
			if(isRegisterFaulTask){
				// 添加失败交易记录
				commonCodeService.addReqBusinessDetail(reqBusinessDetailList);
				businessDTO.setRequetNum(responseHeader.getRecordNum());
				// 添加请求交易记录数
				commonCodeService.addReqBusinessCollate(businessDTO);
			}
		} catch (Exception e) {
			logger.error("单笔保单转出申请上传后置处理发生异常!异常信息为："+e.getMessage());
			thirdSendResponse.setResJson(e.getMessage());
			thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
			thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
			// 存储任务表
			TaxFaulTaskDTO taxFaulTask = new TaxFaulTaskDTO();
			taxFaulTask.setSerialNo(paramHead.getSerialNo());
			taxFaulTask.setAreaCode(paramHead.getAreaCode());
			taxFaulTask.setServerPortCode(paramHead.getPortCode());
			taxFaulTask.setServerPortName(paramHead.getPortName());
			taxFaulTask.setDisposeType(ENUM_DISPOSE_TYPE.ERROR.code());
			faultTaskService.addTaxFaultTask(taxFaulTask);
			e.printStackTrace();
		}
		logger.info("单笔保单转出申请上传后置处理结束!");
		return thirdSendResponse;
	}

	@Override
	public ThirdSendResponseDTO addPolicyTransMOutSettlement(List<ReqSOutPolicyTransferDTO> transInfoList, ParamHeadDTO paramHead) {
		ThirdSendResponseDTO thirdSendResponse = new ThirdSendResponseDTO();
		try {
			List<ReqBusinessDetailDTO> reqBusinessDetailList = new ArrayList<ReqBusinessDetailDTO>();
			ReqBusinessDTO businessDTO = commonCodeService.initReqBusinessDTO();
			logger.info("多笔保单转出登记上传（核心请求）JSON 体数据内容："+JSONObject.toJSONString(transInfoList));
			//1.将保单转出登记上传请求dto转换为中保信保单转出登记上传请求dto
			List<ReqPolicyTransferOutDTO> reqTransferOutList = new ArrayList<ReqPolicyTransferOutDTO>();
			for (ReqSOutPolicyTransferDTO reqSOutPolicyTransfer : transInfoList) {
				ReqPolicyTransferOutDTO reqPolicyTransferOut = new ReqPolicyTransferOutDTO();
				ReflectionUtil.copyProperties(reqSOutPolicyTransfer, reqPolicyTransferOut);
				reqTransferOutList.add(reqPolicyTransferOut);
			}
			List<EPolicyTransferOutDTO> reqEPolicyTransferOutList = this.encapsulationPolicyTransferOutList(reqTransferOutList);
			EPolicyTransferOutBodyDTO ePolicyTransferOutBody = new EPolicyTransferOutBodyDTO();
			ePolicyTransferOutBody.setTransferOutList(reqEPolicyTransferOutList);
			// 封装请求中保信报文
			ThirdSendDTO thirdSendDTO = commonCodeService.createThirdSend(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_RECORD.code(), paramHead, ePolicyTransferOutBody);
			logger.info("多笔保单转出登记上传（请求中保信）JSON 数据内容："+JSONObject.toJSONString(thirdSendDTO));
			//2.调用中保信接口服务,获得返回结果   存储调用中保信日志表
			Holder<ResponseHeader> responseHeaderHolder = new Holder<ResponseHeader>();
			Holder<ResponseBody> responseBodyHolder = new Holder<ResponseBody>();
			
			EbaoServerPortDTO ebaoServerPort = commonCodeService.queryEBaoServerInfoByPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_RECORD.code());
			String portUrl = ebaoServerPort.getPortUrl();
			URL wsdlLocation = new URL(portUrl);
			PolicyTransOutRegisterService_Service service = new PolicyTransOutRegisterService_Service(wsdlLocation);
			PolicyTransOutRegisterService servicePort = service.getPolicyTransOutRegisterServicePort();
			BindingProvider bp = (BindingProvider) servicePort;
			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, portUrl);
			servicePort.transOutRegister(thirdSendDTO.getHead(), thirdSendDTO.getBody(), responseHeaderHolder, responseBodyHolder);
			
			ResponseHeader responseHeader = responseHeaderHolder.value;
			ResponseBody responseBody = responseBodyHolder.value;
			logger.info("多笔保单转出登记上传（中保信响应）JSON 数据内容：" + responseBody.getJsonString());
			//封装中保信返回报文
			ResponseJSON responseJSON = new ResponseJSON();
			responseJSON.setResponseBody(responseBody);
			responseJSON.setResponseHeader(responseHeader);
			//封装发送报文日志对象
			SendLogDTO sendLog = new SendLogDTO();
			sendLog.setSerialNo(paramHead.getSerialNo());
			sendLog.setPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_RECORD.code());
			sendLog.setRequestContent(JSONObject.toJSONString(thirdSendDTO));
			sendLog.setResponseContent(JSONObject.toJSONString(responseJSON));
			sendLog.setTargetCode(ENUM_SENDSERVICE_CODE.EBAO.code());
			sendLog.setState(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
			sendLog.setDescription(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
			commonCodeService.addLogSend(sendLog);
			//3.封装返回核心dto;
			if(ENUM_RESULT_CODE.SUCCESS.code().equals(responseHeader.getResultCode())
					|| ENUM_RESULT_CODE.INPUT_LOCALITY_SUCCESS.code().equals(responseHeader.getResultCode())){
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
				thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
				ResEPolicyTransferDTO resEPolicyTransfer = JSONObject.parseObject(responseBody.getJsonString(), ResEPolicyTransferDTO.class);
				this.savePolicyTransOut(transInfoList, resEPolicyTransfer);
			}else{
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
				List list = responseHeader.getResultMessage().getMessage();
				if(list != null && list.size() > 0){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : responseHeader.getResultMessage().getMessage()){
						sbStr.append(mess).append("|");
					}
					String message = sbStr.substring(0, sbStr.length()-1).toString();
					thirdSendResponse.setResultDesc(message);
				}else{
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
				}
			}
			boolean isRegisterFaulTask = false;
			// 获取业务数据
			if(!StringUtil.isEmpty(responseBody.getJsonString())){
				ResEPolicyTransferDTO resEPolicyTransfer = JSONObject.parseObject(responseBody.getJsonString(), ResEPolicyTransferDTO.class);
				if(null != resEPolicyTransfer.getTransferList()){
					List<ResHXPolicyTransferResultDTO> resHXPolicyTransferResultList = new ArrayList<ResHXPolicyTransferResultDTO>();
					// 成功记录数
					Integer successNum = 0;
					// 失败记录数
					Integer failNum = 0;
					for (ReqSOutPolicyTransferDTO reqSOutPolicyTransfer : transInfoList) {
						for (EPolicyTransferOutDTO ePolicyTransferOut : resEPolicyTransfer.getTransferList()) {
							if(reqSOutPolicyTransfer.getPolicyNo().equals(ePolicyTransferOut.getPolicyNo())){
								if(null != ePolicyTransferOut.getClientList()){
									EBResultCodeDTO eBResultCode = ePolicyTransferOut.getClientList().get(0).getResult();
									HXResultCodeDTO hxResultCode = new HXResultCodeDTO();
									hxResultCode.setResultCode(eBResultCode.getResultCode());
									String message = "";
									if(null != eBResultCode.getResultMessage()){
										StringBuffer sbStr = new StringBuffer(); 
										for(String mess : eBResultCode.getResultMessage()){
											sbStr.append(mess).append("|");
										}
										message = sbStr.substring(0, sbStr.length()-1).toString();
									}
									hxResultCode.setResultMessage(message);
									ResHXPolicyTransferResultDTO policyTransferResult = new ResHXPolicyTransferResultDTO();
									policyTransferResult.setBizNo(reqSOutPolicyTransfer.getBizNo());
									policyTransferResult.setTransferSequenceNo(ePolicyTransferOut.getClientList().get(0).getTransferSequenceNo());
									policyTransferResult.setResult(hxResultCode);
									resHXPolicyTransferResultList.add(policyTransferResult);
									if(ENUM_RESULT_CODE.SUCCESS.code().equals(eBResultCode.getResultCode())){
										successNum+=1;
									}else{
										failNum+=1;
										ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
										reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
										reqBusinessDetailDTO.setResultMessage(message);
										reqBusinessDetailList.add(reqBusinessDetailDTO);
									}
								}
							}
						}
						// 登记请求记录
						businessDTO.setReqSuccessNum(successNum);
						businessDTO.setReqFailNum(failNum);
					}
					thirdSendResponse.setResJson(resHXPolicyTransferResultList);
					isRegisterFaulTask = true;
				}else{
					BookSequenceDTO bookSequence = JSONObject.parseObject(responseBody.getJsonString(), BookSequenceDTO.class);
					TaxFaulTaskDTO taxFaulTask = new TaxFaulTaskDTO();
					taxFaulTask.setSerialNo(paramHead.getSerialNo());
					taxFaulTask.setAreaCode(paramHead.getAreaCode());
					taxFaulTask.setServerPortCode(paramHead.getPortCode());
					taxFaulTask.setServerPortName(paramHead.getPortName());
					taxFaulTask.setDisposeType(ENUM_DISPOSE_TYPE.APPOINTMENT.code());
					taxFaulTask.setBookSequenceNo(bookSequence.getBookingSequenceNo());
					faultTaskService.addTaxFaultTask(taxFaulTask);
					thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
				}
			}else{
				businessDTO.setReqFailNum(responseHeader.getRecordNum());
				for (int i = 0; i < responseHeader.getRecordNum(); i++) {
					ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
					reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
					reqBusinessDetailDTO.setResultMessage(thirdSendResponse.getResultDesc());
					reqBusinessDetailList.add(reqBusinessDetailDTO);
				}
			}
			if(isRegisterFaulTask){
				// 添加失败交易记录
				commonCodeService.addReqBusinessDetail(reqBusinessDetailList);
				businessDTO.setRequetNum(responseHeader.getRecordNum());
				// 添加请求交易记录数
				commonCodeService.addReqBusinessCollate(businessDTO);				
			}
		} catch (Exception e) {
			logger.error("多笔保单转出登记上传后置处理发生异常!异常信息为："+e.getMessage());
			thirdSendResponse.setResJson(e.getMessage());
			thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
			thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
			// 存储任务表
			TaxFaulTaskDTO taxFaulTask = new TaxFaulTaskDTO();
			taxFaulTask.setSerialNo(paramHead.getSerialNo());
			taxFaulTask.setAreaCode(paramHead.getAreaCode());
			taxFaulTask.setServerPortCode(paramHead.getPortCode());
			taxFaulTask.setServerPortName(paramHead.getPortName());
			taxFaulTask.setDisposeType(ENUM_DISPOSE_TYPE.ERROR.code());
			faultTaskService.addTaxFaultTask(taxFaulTask);
			e.printStackTrace();
		}
		logger.info("多笔保单转出登记上传后置处理结束!");
		return thirdSendResponse;
	}
	
	/**
	 * 保单转入申请上传成功返回持久化
	 * @param transInfo
	 */
	public void savePolicyTransIn(ReqMInPolicyTransferDTO transInfo, ResEPolicyTransferDTO resEPolicyTransfer){
		logger.info("保单转入申请上传成功持久化...");
		try {
			for (EPolicyTransferOutDTO ePolicyTransferOutDTO : resEPolicyTransfer.getTransferList()) {
				for(ReqPolicyTransferOutDTO reqPolicyTransferOut : transInfo.getTransferOutList()){
					if(ePolicyTransferOutDTO.getPolicyNo().equals(reqPolicyTransferOut.getPolicyNo())){
						// 获取返回数据中的客户信息
						EPolicyTransClientDTO ePolicyTransClient = ePolicyTransferOutDTO.getClientList().get(0);
						if(ENUM_RESULT_CODE.SUCCESS.code().equals(ePolicyTransClient.getResult().getResultCode())){
							TaxContTransfer taxContTransfer = new TaxContTransfer();
							TaxCont taxCont = policyService.queryPolicyByPolicyNo(reqPolicyTransferOut.getPolicyNo());
							if(null != taxCont){
								taxContTransfer.setContId(taxCont.getSid());
							}
							taxContTransfer.setBizNo(reqPolicyTransferOut.getBizNo());
							taxContTransfer.setContNo(reqPolicyTransferOut.getPolicyNo());
							taxContTransfer.setTransType(ENUM_POLICYTRANS_TYPE.POLICYTRANS_IN.code());
							taxContTransfer.setTransferName(transInfo.getContactName());
							taxContTransfer.setTransferMobile(transInfo.getContactTele());
							taxContTransfer.setTransferEmail(transInfo.getContactEmail());
							taxContTransfer.setTransCompanyCode(reqPolicyTransferOut.getCompanyCode());
							taxContTransfer.setTransCompanyName(reqPolicyTransferOut.getCompanyName());
							taxContTransfer.setTransContNo(reqPolicyTransferOut.getPolicyNo());
							taxContTransfer.setCustomerNo(reqPolicyTransferOut.getCustomerNo());
							taxContTransfer.setTransSequenceNo(reqPolicyTransferOut.getClientList().get(0).getSequenceNo());
							taxContTransfer.setRcState(ENUM_RC_STATE.EFFECTIVE.getCode());
							taxContTransfer.setApplyDate(DateUtil.parseDate(transInfo.getTransApplyDate()));
							taxContTransfer.setCreatorId(SystemConstants.SYS_OPERATOR);
							taxContTransfer.setCreateDate(DateUtil.getCurrentDate());
							taxContTransfer.setModifierId(SystemConstants.SYS_OPERATOR);
							taxContTransfer.setModifyDate(DateUtil.getCurrentDate());
							taxContTransferMapper.insert(taxContTransfer);
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("保单转入申请上传持久化失败");
			e.printStackTrace();
		}
	}
	/**
	 * 保单转出申请上传成功持久化
	 * @param transInfoList
	 */
	public void savePolicyTransOut(List<ReqSOutPolicyTransferDTO> transInfoList, ResEPolicyTransferDTO resEPolicyTransfer){
		logger.info("保单转出申请上传持久化...");
		try {
			for (EPolicyTransferOutDTO ePolicyTransferOut : resEPolicyTransfer.getTransferList()) {
				for (ReqSOutPolicyTransferDTO transInfo : transInfoList) {
					if(ePolicyTransferOut.getPolicyNo().equals(transInfo.getPolicyNo())){
						EPolicyTransClientDTO ePolicyTransClient = ePolicyTransferOut.getClientList().get(0);
						if(ENUM_RESULT_CODE.SUCCESS.code().equals(ePolicyTransClient.getResult().getResultCode())){
							ReqPolicyTransClientDTO reqPolicyTransClient = transInfo.getClientList().get(0);
							TaxContTransfer taxContTransfer = new TaxContTransfer();
							TaxCont taxCont = policyService.queryPolicyByPolicyNo(transInfo.getPolicyNo());
							if(null != taxCont){
								taxContTransfer.setContId(taxCont.getSid());
							}
							taxContTransfer.setBizNo(transInfo.getBizNo());
							taxContTransfer.setContNo(transInfo.getPolicyNo());
							taxContTransfer.setTransType(ENUM_POLICYTRANS_TYPE.POLICYTRANS_OUT.code());
							taxContTransfer.setTransferName(reqPolicyTransClient.getContactName());
							taxContTransfer.setTransferMobile(reqPolicyTransClient.getContactTele());
							taxContTransfer.setTransferEmail(reqPolicyTransClient.getContactEmail());
							taxContTransfer.setRcState(ENUM_RC_STATE.EFFECTIVE.getCode());
							taxContTransfer.setApplyDate(DateUtil.parseDate(reqPolicyTransClient.getRegisterDate())); //转出登记时间
							taxContTransfer.setCreatorId(SystemConstants.SYS_OPERATOR);
							taxContTransfer.setCreateDate(DateUtil.getCurrentDate());
							taxContTransfer.setModifierId(SystemConstants.SYS_OPERATOR);
							taxContTransfer.setModifyDate(DateUtil.getCurrentDate());
							taxContTransferMapper.insert(taxContTransfer);
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("保单转出申请上传持久化失败");
			e.printStackTrace();
		}
	}

	@Override
	public ThirdSendResponseDTO queryPolicyTransInBalanceSettlement(ReqPolicyInTransferQueryDTO policyInTransQuery, ParamHeadDTO paramHead) {
		ThirdSendResponseDTO thirdSendResponse = new ThirdSendResponseDTO();
		try {
			logger.info("保单转入余额信息查询（核心请求）JSON 体数据内容："+JSONObject.toJSONString(policyInTransQuery));
			//1.将请求dto转换为中保信理赔请求dto
			ReqEPolicyInTransferQueryDTO policyTransQuery = new ReqEPolicyInTransferQueryDTO();
			ReflectionUtil.copyProperties(policyInTransQuery, policyTransQuery);
			
			ThirdSendDTO thirdSendDTO = commonCodeService.createThirdSend(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_IN_TRANSFER_BALANCE_SEL.code(), paramHead, policyTransQuery);
			logger.info("保单转入余额信息查询（请求中保信）JSON 数据内容："+JSONObject.toJSONString(thirdSendDTO));
			//2.调用中保信接口服务,获得返回结果   存储调用中保信日志表
			Holder<ResponseHeader> responseHeaderHolder = new Holder<ResponseHeader>();
			Holder<ResponseBody> responseBodyHolder = new Holder<ResponseBody>();
			
			EbaoServerPortDTO ebaoServerPort = commonCodeService.queryEBaoServerInfoByPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_IN_TRANSFER_BALANCE_SEL.code());
			String portUrl = ebaoServerPort.getPortUrl();
			URL wsdlLocation = new URL(portUrl);
			PolicyTransformRemainderQueryService_Service service = new PolicyTransformRemainderQueryService_Service(wsdlLocation);
			PolicyTransformRemainderQueryService servicePort = service.getPolicyTransformRemainderQueryServicePort();
			BindingProvider bp = (BindingProvider) servicePort;
			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, portUrl);
			servicePort.transferRemainderQuery(thirdSendDTO.getHead(), thirdSendDTO.getBody(), responseHeaderHolder, responseBodyHolder);
			
			ResponseHeader responseHeader = responseHeaderHolder.value;
			ResponseBody responseBody = responseBodyHolder.value;
			logger.info("保单转入余额信息查询（中保信响应）JSON 数据内容："+responseBody.getJsonString());
			//封装中保信返回报文
			ResponseJSON responseJSON = new ResponseJSON();
			responseJSON.setResponseBody(responseBody);
			responseJSON.setResponseHeader(responseHeader);
			//封装发送报文日志对象
			SendLogDTO sendLog = new SendLogDTO();
			sendLog.setSerialNo(paramHead.getSerialNo());
			sendLog.setPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_IN_TRANSFER_BALANCE_SEL.code());
			sendLog.setRequestContent(JSONObject.toJSONString(thirdSendDTO));
			sendLog.setResponseContent(JSONObject.toJSONString(responseJSON));
			sendLog.setTargetCode(ENUM_SENDSERVICE_CODE.EBAO.code());
			sendLog.setState(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
			sendLog.setDescription(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
			commonCodeService.addLogSend(sendLog);
			//3.封装返回核心dto;
			if(ENUM_RESULT_CODE.SUCCESS.code().equals(responseHeader.getResultCode())
					|| ENUM_RESULT_CODE.INPUT_LOCALITY_SUCCESS.code().equals(responseHeader.getResultCode())){
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
				thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
			}else{
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
				List list = responseHeader.getResultMessage().getMessage();
				if(list != null && list.size() > 0){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : responseHeader.getResultMessage().getMessage()){
						sbStr.append(mess).append("|");
					}
					String message = sbStr.substring(0, sbStr.length()-1).toString();
					thirdSendResponse.setResultDesc(message);
				}else{
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
				}
			}
			// 获取业务数据
			if(!StringUtil.isEmpty(responseBody.getJsonString())){
				ResEPolicyInTransferBalanceDTO resEPolicyTransferBalance = JSONObject.parseObject(responseBody.getJsonString(), ResEPolicyInTransferBalanceDTO.class);
				ResHXPolicyInTransferBalanceDTO resHXpolicyTransferResultDTO = new ResHXPolicyInTransferBalanceDTO();
				if(null != resEPolicyTransferBalance.getTransferOutList()){
					List<ResHXPolicyTransferOutDTO> resHXPolicyTransferOutList = new ArrayList<ResHXPolicyTransferOutDTO>();
					List<EPolicyTransferOutDTO> ePolicyTransferOutList = resEPolicyTransferBalance.getTransferOutList();
					for (EPolicyTransferOutDTO ePolicyTransferOutDTO : ePolicyTransferOutList) {
						// 封装转出信息
						ResHXPolicyTransferOutDTO resHXPolicyTransferOut = new ResHXPolicyTransferOutDTO();
						resHXPolicyTransferOut.setPolicyNo(ePolicyTransferOutDTO.getPolicyNo());
						List<ResHXPolicyTransClientDTO> resHXPolicyTransClientList = new ArrayList<ResHXPolicyTransClientDTO>();
						for(EPolicyTransClientDTO ePolicyTransClientDTO : ePolicyTransferOutDTO.getClientList()){
							//封装客户信息
							ResHXPolicyTransClientDTO resHXPolicyTransClient = new ResHXPolicyTransClientDTO();
							resHXPolicyTransClient.setSequenceNo(ePolicyTransClientDTO.getSequenceNo());
							resHXPolicyTransClient.setContactName(ePolicyTransClientDTO.getContactName());
							resHXPolicyTransClient.setContactTele(ePolicyTransClientDTO.getContactTele());
							resHXPolicyTransClient.setContactEmail(ePolicyTransClientDTO.getContactEmail());
							resHXPolicyTransClient.setTransReceivDate(ePolicyTransClientDTO.getTransReceivDate());
							resHXPolicyTransClient.setTransAmount(ePolicyTransClientDTO.getTransAmount());
							resHXPolicyTransClientList.add(resHXPolicyTransClient);
						}
						resHXPolicyTransferOut.setClientList(resHXPolicyTransClientList);
						resHXPolicyTransferOutList.add(resHXPolicyTransferOut);
					}
					resHXpolicyTransferResultDTO.setTransferOutList(resHXPolicyTransferOutList);
				}
				// 封装查询结果对象
				HXResultCodeDTO hxResultCode = new HXResultCodeDTO();
				EBResultCodeDTO ebResultCode = resEPolicyTransferBalance.getResult();
				hxResultCode.setResultCode(ebResultCode.getResultCode());
				String message = "";
				if(null != ebResultCode.getResultMessage()){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : ebResultCode.getResultMessage()){
						sbStr.append(mess).append("|");
					}
					message = sbStr.substring(0, sbStr.length()-1).toString();
				}
				hxResultCode.setResultMessage(message);
				resHXpolicyTransferResultDTO.setResult(hxResultCode);
				thirdSendResponse.setResJson(resHXpolicyTransferResultDTO);
			}
		} catch (Exception e) {
			logger.error("保单转入余额信息查询后置处理发生异常! 异常信息为："+e.getMessage());
			thirdSendResponse.setResJson(e.getMessage());
			thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
			thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
			// 存储任务表
			TaxFaulTaskDTO taxFaulTask = new TaxFaulTaskDTO();
			taxFaulTask.setSerialNo(paramHead.getSerialNo());
			taxFaulTask.setAreaCode(paramHead.getAreaCode());
			taxFaulTask.setServerPortCode(paramHead.getPortCode());
			taxFaulTask.setServerPortName(paramHead.getPortName());
			taxFaulTask.setDisposeType(ENUM_DISPOSE_TYPE.ERROR.code());
			faultTaskService.addTaxFaultTask(taxFaulTask);
			e.printStackTrace();
		}
		logger.info("保单转入余额信息查询后置处理结束!");
		return thirdSendResponse;
	}

	@Override
	public ThirdSendResponseDTO policyTransOutQuerySettlement(ReqPolicyInTransferQueryDTO policyInTransferQuery, ParamHeadDTO paramHead) {
		ThirdSendResponseDTO thirdSendResponse = new ThirdSendResponseDTO();
		try {
			logger.info("保单转出信息查询（核心请求）JSON 体数据内容："+JSONObject.toJSONString(policyInTransferQuery));
			//1.将理赔请求dto转换为中保信理赔请求dto
			ReqEPolicyInTransferQueryDTO policyTransQuery = new ReqEPolicyInTransferQueryDTO();
			ReflectionUtil.copyProperties(policyInTransferQuery, policyTransQuery);
			ThirdSendDTO thirdSendDTO = commonCodeService.createThirdSend(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_SEL.code(), paramHead, policyTransQuery);
			logger.info("保单转出信息查询（请求中保信）JSON 数据内容：" + JSONObject.toJSONString(thirdSendDTO));
			//2.调用中保信接口服务,获得返回结果   存储调用中保信日志表
			Holder<ResponseHeader> responseHeaderHolder = new Holder<ResponseHeader>();
			Holder<ResponseBody> responseBodyHolder = new Holder<ResponseBody>();
			
			EbaoServerPortDTO ebaoServerPort = commonCodeService.queryEBaoServerInfoByPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_SEL.code());
			String portUrl = ebaoServerPort.getPortUrl();
			URL wsdlLocation = new URL(portUrl);
			PolicyTransformOutService_Service service = new PolicyTransformOutService_Service(wsdlLocation);
			PolicyTransformOutService servicePort = service.getPolicyTransformOutServicePort();
			BindingProvider bp = (BindingProvider) servicePort;
			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, portUrl);
			servicePort.transformOut(thirdSendDTO.getHead(), thirdSendDTO.getBody(), responseHeaderHolder, responseBodyHolder);
			
			ResponseHeader responseHeader = responseHeaderHolder.value;
			ResponseBody responseBody = responseBodyHolder.value;
			logger.info("保单转出信息查询（中保信响应）JSON 数据内容：" + responseBody.getJsonString());
			//封装中保信返回报文
			ResponseJSON responseJSON = new ResponseJSON();
			responseJSON.setResponseBody(responseBody);
			responseJSON.setResponseHeader(responseHeader);
			//封装发送报文日志对象
			SendLogDTO sendLog = new SendLogDTO();
			sendLog.setSerialNo(paramHead.getSerialNo());
			sendLog.setPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_SEL.code());
			sendLog.setRequestContent(JSONObject.toJSONString(thirdSendDTO));
			sendLog.setResponseContent(JSONObject.toJSONString(responseJSON));
			sendLog.setTargetCode(ENUM_SENDSERVICE_CODE.EBAO.code());
			sendLog.setState(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
			sendLog.setDescription(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
			commonCodeService.addLogSend(sendLog);
			//3.封装返回核心dto;
			if(ENUM_RESULT_CODE.SUCCESS.code().equals(responseHeader.getResultCode())
					|| ENUM_RESULT_CODE.INPUT_LOCALITY_SUCCESS.code().equals(responseHeader.getResultCode())){
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
				thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
			}else{
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
				List list = responseHeader.getResultMessage().getMessage();
				if(list != null && list.size() > 0){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : responseHeader.getResultMessage().getMessage()){
						sbStr.append(mess).append("|");
					}
					String message = sbStr.substring(0, sbStr.length()-1).toString();
					thirdSendResponse.setResultDesc(message);
				}else{
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
				}
			}
			// 获取业务数据
			if(!StringUtil.isEmpty(responseBody.getJsonString())){
				ResEPolicyTransOutQueryDTO resEPolicyTransOutQuery = JSONObject.parseObject(responseBody.getJsonString(), ResEPolicyTransOutQueryDTO.class);
				ReqHXPolicyInTransferQueryDTO reqHXPolicyInTransferQuery = new ReqHXPolicyInTransferQueryDTO();
				if(null != resEPolicyTransOutQuery.getTransferInList()){
					List<ResHXPolicyTransferInDTO> resHXPolicyTransferInList = new ArrayList<ResHXPolicyTransferInDTO>();
					for (ReqEPolicyTransferDTO reqEPolicyTransfer : resEPolicyTransOutQuery.getTransferInList()) {
						// 封装转出信息
						ResHXPolicyTransferInDTO resHXPolicyTransferIn = new ResHXPolicyTransferInDTO();
						resHXPolicyTransferIn.setCompanyName(reqEPolicyTransfer.getCompanyName());
						resHXPolicyTransferIn.setBankName(reqEPolicyTransfer.getBankName());
						resHXPolicyTransferIn.setAccountHolder(reqEPolicyTransfer.getAccountHolder());
						resHXPolicyTransferIn.setAccountNo(reqEPolicyTransfer.getAccountNo());
						resHXPolicyTransferIn.setContactName(reqEPolicyTransfer.getContactName());
						resHXPolicyTransferIn.setContactTele(reqEPolicyTransfer.getContactTele());
						resHXPolicyTransferIn.setContactEmail(reqEPolicyTransfer.getContactEmail());
						List<ResHXPolicyTransferOutDTO> resHXPolicyTransferOutList = new ArrayList<ResHXPolicyTransferOutDTO>();
						for(EPolicyTransferOutDTO ePolicyTransferOut : reqEPolicyTransfer.getTransferOutList()){
							// 封装转出信息
							ResHXPolicyTransferOutDTO resHXPolicyTransferOut = new ResHXPolicyTransferOutDTO();
							resHXPolicyTransferOut.setPolicyNo(ePolicyTransferOut.getPolicyNo());
							
							List<ResHXPolicyTransClientDTO> resHXPolicyTransClientList = new ArrayList<ResHXPolicyTransClientDTO>();
							for(EPolicyTransClientDTO ePolicyTransClient : ePolicyTransferOut.getClientList()){
								// 封装客户信息
								ResHXPolicyTransClientDTO resHXPolicyTransClient = new ResHXPolicyTransClientDTO();
								resHXPolicyTransClient.setSequenceNo(ePolicyTransClient.getSequenceNo());
								resHXPolicyTransClient.setTransferSequenceNo(ePolicyTransClient.getTransferSequenceNo());
								resHXPolicyTransClient.setTransApplyDate(ePolicyTransClient.getTransApplyDate());
								resHXPolicyTransClient.setTransReceivDate(ePolicyTransClient.getTransReceivDate());
								resHXPolicyTransClientList.add(resHXPolicyTransClient);						
							}
							resHXPolicyTransferOut.setClientList(resHXPolicyTransClientList);
							resHXPolicyTransferOutList.add(resHXPolicyTransferOut);
						}
						resHXPolicyTransferIn.setResHXPolicyTransferOutList(resHXPolicyTransferOutList);
						resHXPolicyTransferInList.add(resHXPolicyTransferIn);
					}
					reqHXPolicyInTransferQuery.setPolicyTransferInList(resHXPolicyTransferInList);
				}
				// 封装查询结果对象
				HXResultCodeDTO hxResultCode = new HXResultCodeDTO();
				EBResultCodeDTO ebResultCode = resEPolicyTransOutQuery.getResult();
				hxResultCode.setResultCode(ebResultCode.getResultCode());
				String message = "";
				if(null != ebResultCode.getResultMessage()){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : ebResultCode.getResultMessage()){
						sbStr.append(mess).append("|");
					}
					message = sbStr.substring(0, sbStr.length()-1).toString();
				}
				hxResultCode.setResultMessage(message);
				reqHXPolicyInTransferQuery.setResult(hxResultCode);
				thirdSendResponse.setResJson(reqHXPolicyInTransferQuery);
			}
		} catch (Exception e) {
			logger.error("保单转出信息查询后置处理发生异常! 异常信息为：" + e.getMessage());
			thirdSendResponse.setResJson(e.getMessage());
			thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
			thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
			e.printStackTrace();
		}
		logger.info("保单转出信息查询后置处理结束!");
		return thirdSendResponse;
	}

	@Override
	public ThirdSendResponseDTO policyTransOutRegisterQuerySettlement(ReqPolicyInTransferQueryDTO policyInTransferQuery, ParamHeadDTO paramHead) {
		ThirdSendResponseDTO thirdSendResponse = new ThirdSendResponseDTO();
		try {
			logger.info("保单转出登记信息查询（核心请求）JSON 体数据内容：" + JSONObject.toJSONString(policyInTransferQuery));
			//1.将请求dto转换为中保信理赔请求dto
			ReqEPolicyInTransferQueryDTO policyTransQuery = new ReqEPolicyInTransferQueryDTO();
			ReflectionUtil.copyProperties(policyInTransferQuery, policyTransQuery);
			ThirdSendDTO thirdSendDTO = commonCodeService.createThirdSend(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_REGISTER_SEL.code(), paramHead, policyTransQuery);
			logger.info("保单转出登记信息查询（请求中保信）JSON 数据内容：" + JSONObject.toJSONString(thirdSendDTO));
			//2.调用中保信接口服务,获得返回结果   存储调用中保信日志表
			Holder<ResponseHeader> responseHeaderHolder = new Holder<ResponseHeader>();
			Holder<ResponseBody> responseBodyHolder = new Holder<ResponseBody>();
			
			EbaoServerPortDTO ebaoServerPort = commonCodeService.queryEBaoServerInfoByPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_REGISTER_SEL.code());
			String portUrl = ebaoServerPort.getPortUrl();
			URL wsdlLocation = new URL(portUrl);
			PolicyTransOutRegisterQueryService_Service service = new PolicyTransOutRegisterQueryService_Service(wsdlLocation);
			PolicyTransOutRegisterQueryService servicePort = service.getPolicyTransOutRegisterQueryServicePort();
			BindingProvider bp = (BindingProvider) servicePort;
			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, portUrl);
			servicePort.registerQuery(thirdSendDTO.getHead(), thirdSendDTO.getBody(), responseHeaderHolder, responseBodyHolder);
			
			ResponseHeader responseHeader = responseHeaderHolder.value;
			ResponseBody responseBody = responseBodyHolder.value;
			logger.info("保单转出登记信息查询（中保信响应）JSON 数据内容："+responseBody.getJsonString());
			//封装中保信返回报文
			ResponseJSON responseJSON = new ResponseJSON();
			responseJSON.setResponseBody(responseBody);
			responseJSON.setResponseHeader(responseHeader);
			//封装发送报文日志对象
			SendLogDTO sendLog = new SendLogDTO();
			sendLog.setSerialNo(paramHead.getSerialNo());
			sendLog.setPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_REGISTER_SEL.code());
			sendLog.setRequestContent(JSONObject.toJSONString(thirdSendDTO));
			sendLog.setResponseContent(JSONObject.toJSONString(responseJSON));
			sendLog.setTargetCode(ENUM_SENDSERVICE_CODE.EBAO.code());
			sendLog.setState(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
			sendLog.setDescription(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
			commonCodeService.addLogSend(sendLog);
			//3.封装返回核心dto;
			if(ENUM_RESULT_CODE.SUCCESS.code().equals(responseHeader.getResultCode())
					|| ENUM_RESULT_CODE.INPUT_LOCALITY_SUCCESS.code().equals(responseHeader.getResultCode())){
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
				thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
			}else{
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
				List list = responseHeader.getResultMessage().getMessage();
				if(list != null && list.size() > 0){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : responseHeader.getResultMessage().getMessage()){
						sbStr.append(mess).append("|");
					}
					String message = sbStr.substring(0, sbStr.length()-1).toString();
					thirdSendResponse.setResultDesc(message);
				}else{
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
				}
			}
			// 获取业务数据
			if(!StringUtil.isEmpty(responseBody.getJsonString())){
				ResEPolicyTransOutRegisterQueryDTO ePolicyTransOutRegisterQuery = JSONObject.parseObject(responseBody.getJsonString(), ResEPolicyTransOutRegisterQueryDTO.class);
				ResHXPolicyTransOutRegisterDTO resHXPolicyTransOutRegister = new ResHXPolicyTransOutRegisterDTO();
				if(null != ePolicyTransOutRegisterQuery.getTransferOutList()){
					List<ResHXPolicyTransferOutDTO> resHXPolicyTransferOutList = new ArrayList<ResHXPolicyTransferOutDTO>();
					for(EPolicyTransferOutDTO ePolicyTransferOut : ePolicyTransOutRegisterQuery.getTransferOutList()){
						// 封装转出信息
						ResHXPolicyTransferOutDTO resHXPolicyTransferOut = new ResHXPolicyTransferOutDTO();
						resHXPolicyTransferOut.setPolicyNo(ePolicyTransferOut.getPolicyNo());
						
						List<ResHXPolicyTransClientDTO> resHXPolicyTransClientList = new ArrayList<ResHXPolicyTransClientDTO>();
						for(EPolicyTransClientDTO ePolicyTransClient : ePolicyTransferOut.getClientList()){
							// 封装客户信息
							ResHXPolicyTransClientDTO resHXPolicyTransClient = new ResHXPolicyTransClientDTO();
							resHXPolicyTransClient.setSequenceNo(ePolicyTransClient.getSequenceNo());
							resHXPolicyTransClient.setRegisterDate(ePolicyTransClient.getRegisterDate());
							resHXPolicyTransClient.setExpectedTerminiateDate(ePolicyTransClient.getExpectedTerminiateDate());
							resHXPolicyTransClient.setRejectReason(ePolicyTransClient.getRejectReason());
							resHXPolicyTransClient.setContactName(ePolicyTransClient.getContactName());
							resHXPolicyTransClient.setContactEmail(ePolicyTransClient.getContactEmail());
							resHXPolicyTransClient.setContactTele(ePolicyTransClient.getContactTele());
							resHXPolicyTransClient.setTransReceivDate(ePolicyTransClient.getTransReceivDate());
							resHXPolicyTransClientList.add(resHXPolicyTransClient);						
						}
						resHXPolicyTransferOut.setClientList(resHXPolicyTransClientList);
						resHXPolicyTransferOutList.add(resHXPolicyTransferOut);
					}
					resHXPolicyTransOutRegister.setPolicyTransferOutList(resHXPolicyTransferOutList);
				}
				// 封装查询结果对象
				HXResultCodeDTO hxResultCode = new HXResultCodeDTO();
				EBResultCodeDTO ebResultCode = ePolicyTransOutRegisterQuery.getResult();
				hxResultCode.setResultCode(ebResultCode.getResultCode());
				String message = "";
				if(null != ebResultCode.getResultMessage()){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : ebResultCode.getResultMessage()){
						sbStr.append(mess).append("|");
					}
					message = sbStr.substring(0, sbStr.length()-1).toString();
				}
				hxResultCode.setResultMessage(message);
				resHXPolicyTransOutRegister.setResult(hxResultCode);
				thirdSendResponse.setResJson(resHXPolicyTransOutRegister);
			}
		} catch (Exception e) {
			logger.error("保单转出登记信息查询后置处理发生异常! 异常信息为："+e.getMessage());
			thirdSendResponse.setResJson(e.getMessage());
			thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
			thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
			// 存储任务表
			TaxFaulTaskDTO taxFaulTask = new TaxFaulTaskDTO();
			taxFaulTask.setSerialNo(paramHead.getSerialNo());
			taxFaulTask.setAreaCode(paramHead.getAreaCode());
			taxFaulTask.setServerPortCode(paramHead.getPortCode());
			taxFaulTask.setServerPortName(paramHead.getPortName());
			taxFaulTask.setDisposeType(ENUM_DISPOSE_TYPE.ERROR.code());
			faultTaskService.addTaxFaultTask(taxFaulTask);
			e.printStackTrace();
		}
		logger.error("保单转出登记信息查询后置处理结束!");
		return thirdSendResponse;
	}

	@Override
	public ThirdSendResponseDTO policyTransInExceptionJob(String reqJSON, String portCode, TaxFaultTask faultTask) {
		logger.info("进入保单转入申请上传异常任务服务");
		ThirdSendResponseDTO thirdSendResponse = new ThirdSendResponseDTO();
		try {
			List<ReqBusinessDetailDTO> reqBusinessDetailList = new ArrayList<ReqBusinessDetailDTO>();
			ReqBusinessDTO businessDTO = commonCodeService.initReqBusinessDTO();
			ParamHeadDTO paramHead = new ParamHeadDTO();
			CoreRequestHead coreRequestHead = new CoreRequestHead();
			ReqMInPolicyTransferDTO transInfo = new ReqMInPolicyTransferDTO();
			//1. 解析请求报文
			//单笔请求
			if(ENUM_HX_SERVICE_PORT_CODE.POLICY_IN_TRANSFER_UPLOAD_ONE.code().equals(portCode)){
				ReqSInPolicyTransCoreDTO reqSInPolicyTransCore = JSONObject.parseObject(reqJSON, ReqSInPolicyTransCoreDTO.class);
				ReqSInPolicyTransferDTO transInfoSIn = (ReqSInPolicyTransferDTO) reqSInPolicyTransCore.getBody();
				// 单笔转入封装成多笔转入
				ReflectionUtil.copyProperties(transInfoSIn, transInfo);
				List<ReqPolicyTransferOutDTO> transferOutList = new ArrayList<ReqPolicyTransferOutDTO>();
				transferOutList.add(transInfoSIn.getTransferOut());
				transInfo.setTransferOutList(transferOutList);
				coreRequestHead = reqSInPolicyTransCore.getRequestHead();
			//多笔请求
			}else if(ENUM_HX_SERVICE_PORT_CODE.POLICY_IN_TRANSFER_UPLOAD_MORE.code().equals(portCode)){
				ReqMInPolicyTransCoreDTO reqMInPolicyTransCore = JSONObject.parseObject(reqJSON, ReqMInPolicyTransCoreDTO.class);
				transInfo = (ReqMInPolicyTransferDTO) reqMInPolicyTransCore.getBody();
				coreRequestHead = reqMInPolicyTransCore.getRequestHead();
			}else{
				throw new BusinessDataErrException("请求接口编码有误! 错误接口编码为："+portCode);
			}
			//2. 封装头部信息
			paramHead.setAreaCode(coreRequestHead.getAreaCode());
			paramHead.setRecordNum(coreRequestHead.getRecordNum());
			paramHead.setSerialNo(coreRequestHead.getSerialNo());
			paramHead.setPortCode(portCode);
			ThirdSendDTO thirdSendDTO = new ThirdSendDTO();
			
			// 将请求dto转换为中保信理赔请求dto
			List<ReqPolicyTransferOutDTO> reqPolicyTransferList = transInfo.getTransferOutList();
			List<EPolicyTransferOutDTO> reqEPolicyTransferOutList = this.encapsulationPolicyTransferOutList(reqPolicyTransferList);
			// 封装转移保单中的转入信息
			ReqEPolicyTransferDTO reqEPolicyTransfer = new ReqEPolicyTransferDTO();
			ReflectionUtil.copyProperties(transInfo, reqEPolicyTransfer);
			reqEPolicyTransfer.setTransferOutList(reqEPolicyTransferOutList);
			
			ReqEPolicyTransferBodyDTO reqEPolicyTransferBody = new ReqEPolicyTransferBodyDTO();
			reqEPolicyTransferBody.setTransferIn(reqEPolicyTransfer);
			thirdSendDTO = commonCodeService.createThirdSend(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_IN_TRANSFER_UPLOAD.code(), paramHead, reqEPolicyTransferBody);
			logger.info("异常任务 - 保单转入申请上传（请求中保信）JSON 数据内容(错误或异常类型)："+JSONObject.toJSONString(thirdSendDTO));
			Holder<ResponseHeader> responseHeaderHolder = new Holder<ResponseHeader>();
			Holder<ResponseBody> responseBodyHolder = new Holder<ResponseBody>();
			//4. 调用保单转入申请上传接口地址
			EbaoServerPortDTO ebaoServerPort = commonCodeService.queryEBaoServerInfoByPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_IN_TRANSFER_UPLOAD.code());
			String portUrl = ebaoServerPort.getPortUrl();
			URL wsdlLocation = new URL(portUrl);
			PolicyTransformInService_Service service = new PolicyTransformInService_Service(wsdlLocation);
			PolicyTransformInService servicePort = service.getPolicyTransformInServicePort();
			BindingProvider bp = (BindingProvider) servicePort;
			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, portUrl);
			servicePort.transformIn(thirdSendDTO.getHead(), thirdSendDTO.getBody(), responseHeaderHolder, responseBodyHolder);
			
			ResponseHeader responseHeader = responseHeaderHolder.value;
			ResponseBody responseBody = responseBodyHolder.value;
			logger.info("异常任务 - 保单转入申请上传异常任务服务方法（中保信响应）JSON 数据内容："+responseBody.getJsonString());
			
			ResponseJSON responseJSON = new ResponseJSON();
			responseJSON.setResponseBody(responseBody);
			responseJSON.setResponseHeader(responseHeader);
			// 异步请求需要记录发送报文
			if(ENUM_DISPOSE_TYPE.ASYNC.code().equals(faultTask.getDisposeType())){
				SendLogDTO sendLog = new SendLogDTO();
				sendLog.setSerialNo(paramHead.getSerialNo());
				sendLog.setPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_RECORD.code());
				sendLog.setRequestContent(JSONObject.toJSONString(thirdSendDTO));
				sendLog.setResponseContent(JSONObject.toJSONString(responseJSON));
				sendLog.setTargetCode(ENUM_SENDSERVICE_CODE.EBAO.code());
				sendLog.setState(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
				sendLog.setDescription(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
				commonCodeService.addLogSend(sendLog);
			}
			// 初始化头部错误信息
			String messageBody = ENUM_BZ_RESPONSE_RESULT.FAIL.description();
			//3.封装返回核心dto;
			if(ENUM_RESULT_CODE.SUCCESS.code().equals(responseHeader.getResultCode())
					|| ENUM_RESULT_CODE.INPUT_LOCALITY_SUCCESS.code().equals(responseHeader.getResultCode())){
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
				thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
				// 数据持久化
				ResEPolicyTransferDTO resEPolicyTransfer = JSONObject.parseObject(responseBody.getJsonString(), ResEPolicyTransferDTO.class);
				this.savePolicyTransIn(transInfo, resEPolicyTransfer);
			}else{
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
				List list = responseHeader.getResultMessage().getMessage();
				if(list != null && list.size() > 0){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : responseHeader.getResultMessage().getMessage()){
						sbStr.append(mess).append("|");
					}
					String message = messageBody = sbStr.substring(0, sbStr.length()-1).toString();
					thirdSendResponse.setResultDesc(message);
				}else{
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
				}
			}
			// 获取业务数据
			if(!StringUtil.isEmpty(responseBody.getJsonString())){
				List<ResHXPolicyTransferResultDTO> resHXPolicyTransferResult = new ArrayList<ResHXPolicyTransferResultDTO>();
				ResEPolicyTransferDTO resEPolicyTransfer = JSONObject.parseObject(responseBody.getJsonString(), ResEPolicyTransferDTO.class);
				List<EPolicyTransferOutDTO> ePolicyTransferOutList = resEPolicyTransfer.getTransferList();
				// 成功记录数
				Integer successNum = 0;
				// 失败记录数
				Integer failNum = 0;
				for (EPolicyTransferOutDTO ePolicyTransferOutDTO : ePolicyTransferOutList) {
					for (ReqPolicyTransferOutDTO reqPolicyTransferOutDTO : reqPolicyTransferList) {
						if(reqPolicyTransferOutDTO.getPolicyNo().equals(ePolicyTransferOutDTO.getPolicyNo())){
							EBResultCodeDTO eBResultCode = ePolicyTransferOutDTO.getClientList().get(0).getResult();
							HXResultCodeDTO hxResultCode = new HXResultCodeDTO();
							String message = "";
							if(null != eBResultCode.getResultMessage()){
								StringBuffer sbStr = new StringBuffer(); 
								for(String mess : eBResultCode.getResultMessage()){
									sbStr.append(mess).append("|");
								}
								message = sbStr.substring(0, sbStr.length()-1).toString();
							}
							hxResultCode.setResultMessage(message);
							// 获取多笔转出信息对应核心的业务号
							ResHXPolicyTransferResultDTO policyTransferResultDTO = new ResHXPolicyTransferResultDTO();
							policyTransferResultDTO.setBizNo(reqPolicyTransferOutDTO.getBizNo());
							policyTransferResultDTO.setTransferSequenceNo(ePolicyTransferOutDTO.getClientList().get(0).getTransferSequenceNo());
							policyTransferResultDTO.setResult(hxResultCode);
							resHXPolicyTransferResult.add(policyTransferResultDTO);
							//记录成功和失败记录数
							if(ENUM_RESULT_CODE.SUCCESS.code().equals(eBResultCode.getResultCode())){
								successNum+=1;
							}else{
								failNum+=1;
								ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
								reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
								reqBusinessDetailDTO.setResultMessage(message);
								reqBusinessDetailList.add(reqBusinessDetailDTO);
							}
						}
					}
				}
				// 登记请求记录
				businessDTO.setReqSuccessNum(successNum);
				businessDTO.setReqFailNum(failNum);
				thirdSendResponse.setResJson(resHXPolicyTransferResult);
			}else{
				thirdSendResponse.setResJson(messageBody);
				businessDTO.setReqFailNum(responseHeader.getRecordNum());
				for (int i = 0; i < responseHeader.getRecordNum(); i++) {
					ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
					reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
					reqBusinessDetailDTO.setResultMessage(thirdSendResponse.getResultDesc());
					reqBusinessDetailList.add(reqBusinessDetailDTO);
				}
			}
			// 添加失败交易记录
			commonCodeService.addReqBusinessDetail(reqBusinessDetailList);
			businessDTO.setRequetNum(responseHeader.getRecordNum());
			// 添加请求交易记录数
			commonCodeService.addReqBusinessCollate(businessDTO);
			faultTask.setDisposeResult(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
		} catch (Exception e) {
			e.printStackTrace();
			thirdSendResponse.setResJson(e.getMessage());
			thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
			thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
			logger.error("保单转入申请上传异常任务服务方法处理发生异常! 异常信息为："+e.getMessage());
			faultTask.setDisposeType(ENUM_DISPOSE_TYPE.ERROR.code());
			faultTask.setDisposeResult(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
		} finally{
			faultTask.setSuccessMessage(JSONObject.toJSONString(thirdSendResponse));
			faultTask.setDisposeDate(DateUtil.getCurrentDate());
			taxFaultTaskMapper.updateByPrimaryKeySelective(faultTask);
		}
		logger.info("保单转入申请上传异常任务服务方法处理结束!");
		return thirdSendResponse;
	}
	@Override
	public TaxFaultTask policyTransOutRegisterExceptionJob(String reqJSON, String portCode, Integer faultTaskId){
		logger.info("进入保单转出登记预约码/异常任务服务");
		ThirdSendResponseDTO thirdSendResponse = new ThirdSendResponseDTO();
		TaxFaultTask faultTask = taxFaultTaskMapper.selectByPrimaryKey(faultTaskId);
		try {
			List<ReqBusinessDetailDTO> reqBusinessDetailList = new ArrayList<ReqBusinessDetailDTO>();
			ReqBusinessDTO businessDTO = commonCodeService.initReqBusinessDTO();
			ParamHeadDTO paramHead = new ParamHeadDTO();
			CoreRequestHead coreRequestHead = new CoreRequestHead();
			List<ReqSOutPolicyTransferDTO> transInfoList = new ArrayList<ReqSOutPolicyTransferDTO>();
			//1. 解析请求报文
			//单笔请求
			if(ENUM_HX_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_RECORD_ONE.code().equals(portCode)){
				ReqSOutPolicyTransCoreDTO reqSOutPolicyTransCore = JSONObject.parseObject(reqJSON, ReqSOutPolicyTransCoreDTO.class);
				ReqSOutPolicyTransferDTO transInfo = (ReqSOutPolicyTransferDTO) reqSOutPolicyTransCore.getBody();
				transInfoList.add(transInfo);
				coreRequestHead = reqSOutPolicyTransCore.getRequestHead();
			//多笔请求
			}else if(ENUM_HX_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_RECORD_MORE.code().equals(portCode)){
				ReqMOutPolicyTransCoreDTO reqMOutPolicyTransCore = JSONObject.parseObject(reqJSON, ReqMOutPolicyTransCoreDTO.class);
				ReqMOutPolicyTransBodyDTO transInfoBody = (ReqMOutPolicyTransBodyDTO) reqMOutPolicyTransCore.getBody();
				transInfoList = transInfoBody.getTransferOutList();
				coreRequestHead = reqMOutPolicyTransCore.getRequestHead();
			}else{
				throw new BusinessDataErrException("请求接口编码有误! 错误接口编码为："+portCode);
			}
			//2. 封装头部信息
			paramHead.setAreaCode(coreRequestHead.getAreaCode());
			paramHead.setRecordNum(coreRequestHead.getRecordNum());
			paramHead.setSerialNo(coreRequestHead.getSerialNo());
			ThirdSendDTO thirdSendDTO = new ThirdSendDTO();
			EbaoServerPortDTO ebaoServerPort = new EbaoServerPortDTO();
			Holder<ResponseHeader> responseHeaderHolder = new Holder<ResponseHeader>();
			Holder<ResponseBody> responseBodyHolder = new Holder<ResponseBody>();
			// 预约码处理方式
			if(ENUM_DISPOSE_TYPE.APPOINTMENT.code().equals(faultTask.getDisposeType())){
				// 3.封装请求中保信预约码数据
				paramHead.setRecordNum("1"); //异步请求接口请求记录数为1
				BookSequenceDTO bookSequence = new BookSequenceDTO();
				bookSequence.setBookingSequenceNo(faultTask.getRemark());
				thirdSendDTO = commonCodeService.createThirdSend(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_APPOINTMENT.code(), paramHead, bookSequence);
				logger.info("预约码 - 转出登记上传（请求中保信）JSON 数据内容(预约码类型)："+JSONObject.toJSONString(thirdSendDTO));
				ebaoServerPort = commonCodeService.queryEBaoServerInfoByPortCode(ENUM_EBAO_SERVICE_PORT_CODE.ASYN_RESULT_QUERY.code());
				// 4.调用中保信预约码请求地址
				String portUrl = ebaoServerPort.getPortUrl();
				URL wsdlLocation = new URL(portUrl);
				AsynResultQueryService_Service service = new AsynResultQueryService_Service(wsdlLocation);
				AsynResultQueryService servicePort = service.getAsynResultQueryServicePort();
				BindingProvider bp = (BindingProvider) servicePort;
				bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, portUrl);
				servicePort.asynResultQuery(thirdSendDTO.getHead(), thirdSendDTO.getBody(),responseHeaderHolder, responseBodyHolder);
			// 错误或异常处理方式
			}else if(ENUM_DISPOSE_TYPE.ASYNC.code().equals(faultTask.getDisposeType()) || ENUM_DISPOSE_TYPE.ERROR.code().equals(faultTask.getDisposeType())){
				// 将保单转出登记上传请求dto转换为中保信保单转出登记上传请求dto
				List<ReqPolicyTransferOutDTO> reqTransferOutList = new ArrayList<ReqPolicyTransferOutDTO>();
				for (ReqSOutPolicyTransferDTO reqSOutPolicyTransfer : transInfoList) {
					ReqPolicyTransferOutDTO reqPolicyTransferOut = new ReqPolicyTransferOutDTO();
					ReflectionUtil.copyProperties(reqSOutPolicyTransfer, reqPolicyTransferOut);
					reqTransferOutList.add(reqPolicyTransferOut);
				}
				List<EPolicyTransferOutDTO> reqEPolicyTransferOutList = this.encapsulationPolicyTransferOutList(reqTransferOutList);
				EPolicyTransferOutBodyDTO ePolicyTransferOutBody = new EPolicyTransferOutBodyDTO();
				ePolicyTransferOutBody.setTransferOutList(reqEPolicyTransferOutList);
				thirdSendDTO = commonCodeService.createThirdSend(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_RECORD.code(), paramHead, ePolicyTransferOutBody);
				logger.info("异常任务 - 转出登记上传（请求中保信）JSON 数据内容(错误或异常类型)："+JSONObject.toJSONString(thirdSendDTO));
				ebaoServerPort = commonCodeService.queryEBaoServerInfoByPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_RECORD.code());
			
				// 4.调用中保信多笔请求地址
				String portUrl = ebaoServerPort.getPortUrl();
				URL wsdlLocation = new URL(portUrl);
				PolicyTransOutRegisterService_Service service = new PolicyTransOutRegisterService_Service(wsdlLocation);
				PolicyTransOutRegisterService servicePort = service.getPolicyTransOutRegisterServicePort();
				BindingProvider bp = (BindingProvider) servicePort;
				bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, portUrl);
				servicePort.transOutRegister(thirdSendDTO.getHead(), thirdSendDTO.getBody(), responseHeaderHolder, responseBodyHolder);
			}else{
				throw new BusinessDataErrException("异常任务 - 异常类型有误! 错误异常类型为："+ENUM_DISPOSE_TYPE.getEnumValueByKey(faultTask.getDisposeType()));
			}
			
			ResponseHeader responseHeader = responseHeaderHolder.value;
			ResponseBody responseBody = responseBodyHolder.value;
			logger.info("预约码/异常任务 - 保单转出登记上传（中保信响应）JSON 数据内容："+responseBody.getJsonString());
			ResponseJSON responseJSON = new ResponseJSON();
			responseJSON.setResponseBody(responseBody);
			responseJSON.setResponseHeader(responseHeader);
			// 异步请求需要记录发送报文
			if(!ENUM_DISPOSE_TYPE.APPOINTMENT.code().equals(faultTask.getDisposeType())){
				SendLogDTO sendLog = new SendLogDTO();
				sendLog.setSerialNo(paramHead.getSerialNo());
				sendLog.setPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_RECORD.code());
				sendLog.setRequestContent(JSONObject.toJSONString(thirdSendDTO));
				sendLog.setResponseContent(JSONObject.toJSONString(responseJSON));
				sendLog.setTargetCode(ENUM_SENDSERVICE_CODE.EBAO.code());
				sendLog.setState(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
				sendLog.setDescription(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
				commonCodeService.addLogSend(sendLog);
			}
			// 初始化头部错误信息
			String messageBody = ENUM_BZ_RESPONSE_RESULT.FAIL.description();
			//5. 封装返回核心对象
			if(ENUM_RESULT_CODE.SUCCESS.code().equals(responseHeader.getResultCode())
					|| ENUM_RESULT_CODE.INPUT_LOCALITY_SUCCESS.code().equals(responseHeader.getResultCode())){
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
				thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
				ResEPolicyTransferDTO resEPolicyTransfer = JSONObject.parseObject(responseBody.getJsonString(), ResEPolicyTransferDTO.class);
				this.savePolicyTransOut(transInfoList, resEPolicyTransfer);
			}else if(ENUM_RESULT_CODE.REQ_AWAIT.code().equals(responseHeader.getResultCode())){
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
				thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
			}else{
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
				List list = responseHeader.getResultMessage().getMessage();
				if(list != null && list.size() > 0){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : responseHeader.getResultMessage().getMessage()){
						sbStr.append(mess).append("|");
					}
					String message = messageBody = sbStr.substring(0, sbStr.length()-1).toString();
					thirdSendResponse.setResultDesc(message);
				}else{
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
				}
			}
			boolean isRegisterFaulTask = false;
			// 获取业务数据
			if(!StringUtil.isEmpty(responseBody.getJsonString())){
				ResEPolicyTransferDTO resEPolicyTransfer = JSONObject.parseObject(responseBody.getJsonString(), ResEPolicyTransferDTO.class);
				if(null != resEPolicyTransfer.getTransferList()){
					List<ResHXPolicyTransferResultDTO> resHXPolicyTransferResultList = new ArrayList<ResHXPolicyTransferResultDTO>();
					// 成功记录数
					Integer successNum = 0;
					// 失败记录数
					Integer failNum = 0;
					for (ReqSOutPolicyTransferDTO reqSOutPolicyTransfer : transInfoList) {
						for (EPolicyTransferOutDTO ePolicyTransferOut : resEPolicyTransfer.getTransferList()) {
							if(reqSOutPolicyTransfer.getPolicyNo().equals(ePolicyTransferOut.getPolicyNo())){
								if(null != ePolicyTransferOut.getClientList()){
									EBResultCodeDTO eBResultCode = ePolicyTransferOut.getClientList().get(0).getResult();
									HXResultCodeDTO hxResultCode = new HXResultCodeDTO();
									hxResultCode.setResultCode(eBResultCode.getResultCode());
									String message = "";
									if(null != eBResultCode.getResultMessage()){
										StringBuffer sbStr = new StringBuffer(); 
										for(String mess : eBResultCode.getResultMessage()){
											sbStr.append(mess).append("|");
										}
										message = sbStr.substring(0, sbStr.length()-1).toString();
									}
									hxResultCode.setResultMessage(message);
									ResHXPolicyTransferResultDTO policyTransferResult = new ResHXPolicyTransferResultDTO();
									policyTransferResult.setBizNo(reqSOutPolicyTransfer.getBizNo());
									policyTransferResult.setTransferSequenceNo(ePolicyTransferOut.getClientList().get(0).getTransferSequenceNo());
									policyTransferResult.setResult(hxResultCode);
									resHXPolicyTransferResultList.add(policyTransferResult);
									if(ENUM_RESULT_CODE.SUCCESS.code().equals(eBResultCode.getResultCode())){
										successNum+=1;
									}else{
										failNum+=1;
										ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
										reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
										reqBusinessDetailDTO.setResultMessage(message);
										reqBusinessDetailList.add(reqBusinessDetailDTO);
									}
								}
							}
						}
						// 登记请求记录
						businessDTO.setReqSuccessNum(successNum);
						businessDTO.setReqFailNum(failNum);
					}
					thirdSendResponse.setResJson(resHXPolicyTransferResultList);
					isRegisterFaulTask = true;
				}else{
					BookSequenceDTO bookSequence = JSONObject.parseObject(responseBody.getJsonString(),BookSequenceDTO.class);
					// 插入异常任务表
					faultTask.setRemark(bookSequence.getBookingSequenceNo());
					faultTask.setDisposeType(ENUM_DISPOSE_TYPE.APPOINTMENT.code());
					thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
				}
			}else{
				thirdSendResponse.setResJson(messageBody);
				businessDTO.setReqFailNum(responseHeader.getRecordNum());
				for (int i = 0; i < responseHeader.getRecordNum(); i++) {
					ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
					reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
					reqBusinessDetailDTO.setResultMessage(thirdSendResponse.getResultDesc());
					reqBusinessDetailList.add(reqBusinessDetailDTO);
				}
			}
			if(isRegisterFaulTask){
				// 添加失败交易记录
				commonCodeService.addReqBusinessDetail(reqBusinessDetailList);
				businessDTO.setRequetNum(responseHeader.getRecordNum());
				// 添加请求交易记录数
				commonCodeService.addReqBusinessCollate(businessDTO);
				faultTask.setDisposeResult(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
			}else{
				faultTask.setDisposeResult(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
			}
		}catch(Exception e){
			e.printStackTrace();
			thirdSendResponse.setResJson(e.getMessage());
			thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
			thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
			logger.error("保单转出登记预约码/异常任务服务方法处理发生异常! 异常信息为："+e.getMessage());
			faultTask.setDisposeResult(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
			if(ENUM_DISPOSE_TYPE.APPOINTMENT.code().equals(faultTask.getDisposeType())){
				faultTask.setDisposeType(ENUM_DISPOSE_TYPE.APPOINTMENT.code());
			}else{
				faultTask.setDisposeType(ENUM_DISPOSE_TYPE.ERROR.code());
			}
		} finally{
			faultTask.setSuccessMessage(JSONObject.toJSONString(thirdSendResponse));
			faultTask.setDisposeDate(DateUtil.getCurrentDate());
			taxFaultTaskMapper.updateByPrimaryKeySelective(faultTask);
		}
		logger.info("保单转出登记预约码/异常任务服务方法处理结束!");
		return faultTask;
	}

}
