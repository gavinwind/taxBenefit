package com.sinosoft.taxbenefit.core.renewalcancel.dto;

/**
 * 中保信-续保撤销上传请求
 * @author yangdongkai@outlook.com
 * @date 2016年3月22日 下午3:10:03
 */
public class ReqRenewalCancelDTO {
	//保单信息
	private ReqRenewalCancelPolicyDTO policy;
	/**
	 * @return the policy
	 */
	public ReqRenewalCancelPolicyDTO getPolicy() {
		return policy;
	}

	/**
	 * @param policy the policy to set
	 */
	public void setPolicy(ReqRenewalCancelPolicyDTO policy) {
		this.policy = policy;
	}
}