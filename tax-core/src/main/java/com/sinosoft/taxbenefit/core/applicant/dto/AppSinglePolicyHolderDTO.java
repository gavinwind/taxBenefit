package com.sinosoft.taxbenefit.core.applicant.dto;


/**
 * 个人投保人信息
 * 
 * @author zhangke
 *
 */
public class AppSinglePolicyHolderDTO  {

	/** 投保人客户编码 **/
	private String customerNo;
	/** 投保人姓名 **/
	private String name;
	/** 性别 **/
	private String gender;
	/** 出生日期 **/
	private String birthday;
	/** 证件类型 **/
	private String certiType;
	/** 证件号码 **/
	private String certiNo;
	/** 国籍 **/
	private String nationality;
	/** 纳税标志 **/
	private String taxIndi;
	/** 个税征收方式 **/
	private String taxPayerType;
	/** 工作单位名称 **/
	private String workUnit;
	/** 税务登记号 **/
	private String organCode1;
	/** 组织机构代码 **/
	private String organCode2;
	/** 营业证号 */
	private String organCode3;
	/** 社会信用代码 */
	private String organCode4;
	/** 手机号码 **/
	private String mobileNo;
	/** 常住地 **/
	private String residencePlace;

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getCertiType() {
		return certiType;
	}

	public void setCertiType(String certiType) {
		this.certiType = certiType;
	}

	public String getCertiNo() {
		return certiNo;
	}

	public void setCertiNo(String certiNo) {
		this.certiNo = certiNo;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getResidencePlace() {
		return residencePlace;
	}

	public void setResidencePlace(String residencePlace) {
		this.residencePlace = residencePlace;
	}

	public String getTaxPayerType() {
		return taxPayerType;
	}

	public void setTaxPayerType(String taxPayerType) {
		this.taxPayerType = taxPayerType;
	}

	public String getTaxIndi() {
		return taxIndi;
	}

	public void setTaxIndi(String taxIndi) {
		this.taxIndi = taxIndi;
	}

	public String getWorkUnit() {
		return workUnit;
	}

	public void setWorkUnit(String workUnit) {
		this.workUnit = workUnit;
	}

	public String getOrganCode1() {
		return organCode1;
	}

	public void setOrganCode1(String organCode1) {
		this.organCode1 = organCode1;
	}

	public String getOrganCode2() {
		return organCode2;
	}

	public void setOrganCode2(String organCode2) {
		this.organCode2 = organCode2;
	}

	public String getOrganCode3() {
		return organCode3;
	}

	public void setOrganCode3(String organCode3) {
		this.organCode3 = organCode3;
	}

	public String getOrganCode4() {
		return organCode4;
	}

	public void setOrganCode4(String organCode4) {
		this.organCode4 = organCode4;
	}

}
