package com.sinosoft.taxbenefit.core.common.biz.mapper;

import com.sinosoft.taxbenefit.core.common.dto.ReqBusinessDTO;

public interface CommonMapper {

	/**
	 * 查询当天是否有记录
	 * @return
	 */
	Integer selectBusinessNum();
	/**
	 * 查询当天的交易信息
	 * @return
	 */
	ReqBusinessDTO selectBusinessCollate();
}
