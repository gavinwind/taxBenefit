package com.sinosoft.taxbenefit.core.claim.dto;



/**
 * 医疗费用明细
 * 
 * @author zhangke
 *
 */
public class ClaimDetailInfoDTO  {
	/** 费用名称 **/
	private String name;
	/** 费用金额 **/
	private String amount;
	/** 费用扣减金额 **/
	private String deductibleAmount;
	/** 数量 **/
	private String quantity;
	/** 单价 **/
	private String unitPrice;
	/** 规格与单位 **/
	private String unit;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getDeductibleAmount() {
		return deductibleAmount;
	}

	public void setDeductibleAmount(String deductibleAmount) {
		this.deductibleAmount = deductibleAmount;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(String unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

}
