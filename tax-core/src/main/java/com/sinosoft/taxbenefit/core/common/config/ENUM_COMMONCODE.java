package com.sinosoft.taxbenefit.core.common.config;

/**
 * 公用字典
 * 
 * @author zhangke
 *
 */
public enum ENUM_COMMONCODE {
	USERNAME("01", "syscode"), PASSWORD("02", "syscode"), VERSION("03",
			"syscode");
	private final String code;
	private final String type;

	ENUM_COMMONCODE(String code, String type) {
		this.code = code;
		this.type = type;
	}

	public String code() {
		return code;
	}

	public String type() {
		return type;
	}
}
