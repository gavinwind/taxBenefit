package com.sinosoft.taxbenefit.core.applicant.dto;

/**
 * 承保撤销中保信返回DTO
 * @author zhangke
 *
 */
public class PolicyReversalResultDTO {
	private PolicyReversalResultInfoDTO policy;

	public PolicyReversalResultInfoDTO getPolicy() {
		return policy;
	}

	public void setPolicy(PolicyReversalResultInfoDTO policy) {
		this.policy = policy;
	}
}
