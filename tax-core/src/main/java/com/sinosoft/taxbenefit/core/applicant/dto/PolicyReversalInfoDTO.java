package com.sinosoft.taxbenefit.core.applicant.dto;


/**
 * 请求中保信承保撤销信息DTO
 * 
 * @author zhangke
 *
 */
public class PolicyReversalInfoDTO {
	/** 保单号 */
	private String policyNo;
	/** 撤销日期 */
	private String cancelDate;
	/** 撤销原因 */
	private String cancelReason;
	
	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public String getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(String cancelDate) {
		this.cancelDate = cancelDate;
	}

	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}


}
