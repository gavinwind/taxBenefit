package com.sinosoft.taxbenefit.core.policyTransfer.dto;

/**
 * 请求（中保信）保单转移-转入
 * @author SheChunMing
 */
public class ReqEPolicyTransferBodyDTO {
	// 转入信息
	private ReqEPolicyTransferDTO transferIn;

	public ReqEPolicyTransferDTO getTransferIn() {
		return transferIn;
	}

	public void setTransferIn(ReqEPolicyTransferDTO transferIn) {
		this.transferIn = transferIn;
	}
	
}
