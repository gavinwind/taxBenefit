package com.sinosoft.taxbenefit.core.policyTransfer.biz.dubbo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqMInPolicyTransferDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqPolicyInTransferQueryDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqSInPolicyTransferDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqSOutPolicyTransferDTO;
import com.sinosoft.taxbenefit.api.inter.PolicyTransferApiService;
import com.sinosoft.taxbenefit.core.policyTransfer.biz.service.PolicyTransferService;
/**
 * 保单转移Dubbo服务
 * @author SheChunMing
 */
@Service("policyTransferApiServiceImpl")
public class PolicyTransferApiServiceImpl extends TaxBaseService implements PolicyTransferApiService {
	@Autowired
	PolicyTransferService policyTransferService;
	@Override
	public ThirdSendResponseDTO policyTransSInSettlement(ReqSInPolicyTransferDTO transInfo, ParamHeadDTO paramHead) {
		logger.info("进入单笔保单转入申请上传后置服务");
		return policyTransferService.addPolicyTransSInSettlement(transInfo, paramHead);
	}
	@Override
	public ThirdSendResponseDTO policyTransMInSettlement(ReqMInPolicyTransferDTO transInfo, ParamHeadDTO paramHead) {
		logger.info("进入多笔保单转入申请上传后置服务");
		return policyTransferService.addPolicyTransMInSettlement(transInfo, paramHead);
	}
	@Override
	public ThirdSendResponseDTO policyTransSOutSettlement(ReqSOutPolicyTransferDTO transInfo, ParamHeadDTO paramHead) {
		logger.info("进入单笔保单转出登记上传后置服务");
		return policyTransferService.addPolicyTransSOutSettlement(transInfo, paramHead);
	}
	@Override
	public ThirdSendResponseDTO policyTransMOutSettlement(List<ReqSOutPolicyTransferDTO> transInfoList, ParamHeadDTO paramHead) {
		logger.info("进入多笔保单转出登记上传后置服务");
		return policyTransferService.addPolicyTransMOutSettlement(transInfoList, paramHead);
	}
	@Override
	public ThirdSendResponseDTO policyTransInBalanceSettlement(ReqPolicyInTransferQueryDTO policyInTransferBalance, ParamHeadDTO paramHead) {
		logger.info("进入保单转入余额信息查询后置服务");
		return policyTransferService.queryPolicyTransInBalanceSettlement(policyInTransferBalance, paramHead);
	}
	@Override
	public ThirdSendResponseDTO policyTransOutQuerySettlement(ReqPolicyInTransferQueryDTO transInfo, ParamHeadDTO paramHead) {
		logger.info("进入保单转出信息查询后置服务");
		return policyTransferService.policyTransOutQuerySettlement(transInfo, paramHead);
	}
	@Override
	public ThirdSendResponseDTO policyTransOutRegisterQuerySettlement(ReqPolicyInTransferQueryDTO transInfo, ParamHeadDTO paramHead) {
		logger.info("进入保单转出登记信息查询后置服务");
		return policyTransferService.policyTransOutRegisterQuerySettlement(transInfo, paramHead);
	}

}
