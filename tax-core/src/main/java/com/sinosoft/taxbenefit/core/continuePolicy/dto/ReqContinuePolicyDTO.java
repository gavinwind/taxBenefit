package com.sinosoft.taxbenefit.core.continuePolicy.dto;

import java.util.List;

/**
 * Title:中保信-续保信息上传请求
 * @author yangdongkai@outlook.com
 * @date 2016年2月29日 下午5:15:53
 */
public class ReqContinuePolicyDTO{
	//保单号
	private String policyNo;
	//保单生效日期
	private String effectiveDate;
	//保单满期日期
	private String expireDate;
	//续保批单号
	private String renewalEndorsementNo;
	//客户信息
	private List<ReqContinuePolicyCustomerDTO> customerList;
	/**
	 * @return the customerList
	 */
	public List<ReqContinuePolicyCustomerDTO> getCustomerList() {
		return customerList;
	}
	/**
	 * @param customerList the customerList to set
	 */
	public void setCustomerList(List<ReqContinuePolicyCustomerDTO> customerList) {
		this.customerList = customerList;
	}

	/**
	 * @return the effectiveDate
	 */
	public String getEffectiveDate() {
		return effectiveDate;
	}
	/**
	 * @param effectiveDate the effectiveDate to set
	 */
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	/**
	 * @return the expireDate
	 */
	public String getExpireDate() {
		return expireDate;
	}
	/**
	 * @param expireDate the expireDate to set
	 */
	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}
	/**
	 * @return the renewalEndorsemetNo
	 */
	public String getRenewalEndorsementNo() {
		return renewalEndorsementNo;
	}
	/**
	 * @param renewalEndorsemetNo the renewalEndorsemetNo to set
	 */
	public void setRenewalEndorsementNo(String renewalEndorsementNo) {
		this.renewalEndorsementNo = renewalEndorsementNo;
	}
	/**
	 * @return the policyNo
	 */
	public String getPolicyNo() {
		return policyNo;
	}
	/**
	 * @param policyNo the policyNo to set
	 */
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
}
