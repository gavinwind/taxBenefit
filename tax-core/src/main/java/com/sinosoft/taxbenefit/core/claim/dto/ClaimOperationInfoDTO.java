package com.sinosoft.taxbenefit.core.claim.dto;


/**
 * 手术
 * 
 * @author zhangke
 *
 */
public class ClaimOperationInfoDTO  {
	/** 手术代码 **/
	private String operationCode;

	public String getOperationCode() {
		return operationCode;
	}

	public void setOperationCode(String operationCode) {
		this.operationCode = operationCode;
	}

}
