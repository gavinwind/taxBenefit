package com.sinosoft.taxbenefit.core.customer.biz.dubbo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqCustomerInfoDTO;
import com.sinosoft.taxbenefit.api.inter.CustomerApiService;
import com.sinosoft.taxbenefit.core.customer.biz.service.CustomerServiceCore;
/**
 * 客户验证dubbo服务
 * @author zhangyu
 * @date 2016年3月10日 上午9:41:01
 */
@Service("customerApiServiceImpl")
public class CustomerApiServiceImpl extends TaxBaseService implements
		CustomerApiService {
	@Autowired
	private CustomerServiceCore customerServiceCore;

	@Override
	public ThirdSendResponseDTO uploadCustomerInfo(ReqCustomerInfoDTO customerDTO,ParamHeadDTO paramHeadDTO) {
		logger.info("进入客户身份信息上传（单笔）后置服务");
		return customerServiceCore.addCustomer(customerDTO,paramHeadDTO);
	}

	@Override
	public ThirdSendResponseDTO uploadCustomersInfo(
			List<ReqCustomerInfoDTO> reqcustomersDTO, ParamHeadDTO paramHeadDTO) {
		logger.info("进入客户身份信息上传（多笔）后置服务");
		return customerServiceCore.addCustomers(reqcustomersDTO, paramHeadDTO);
	}
}