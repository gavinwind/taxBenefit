package com.sinosoft.taxbenefit.core.policyTransfer.dto;

import java.math.BigDecimal;

import com.sinosoft.taxbenefit.api.base.EBResultCodeDTO;

/**
 * 中保信 -保单转移-客户信息
 * @author SheChunMing
 */
public class EPolicyTransClientDTO {
	// 转出分单号
	private String sequenceNo;
	// 转出登记时间
	private String registerDate;
	// 预计终止日期
	private String expectedTerminiateDate;
	// 无法转出原因
	private String rejectReason;
	// 转出公司联系人
	private String contactName;
	// 电话
	private String contactTele;
	// Email
	private String contactEmail;
	// 转出登记平台登记接收日期
	private String transApplyDate;
	// 转出登记平台登记接收日期
	private String transReceivDate;
	// 转出金额
	private BigDecimal transAmount;
	// 保单转出登记编码
	private String transferSequenceNo;
	// 返回编码
	private EBResultCodeDTO result;

	public String getSequenceNo() {
		return sequenceNo;
	}
	public void setSequenceNo(String sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
	public String getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(String registerDate) {
		this.registerDate = registerDate;
	}
	public String getExpectedTerminiateDate() {
		return expectedTerminiateDate;
	}
	public void setExpectedTerminiateDate(String expectedTerminiateDate) {
		this.expectedTerminiateDate = expectedTerminiateDate;
	}
	public String getRejectReason() {
		return rejectReason;
	}
	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getContactTele() {
		return contactTele;
	}
	public void setContactTele(String contactTele) {
		this.contactTele = contactTele;
	}
	public String getContactEmail() {
		return contactEmail;
	}
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	public String getTransApplyDate() {
		return transApplyDate;
	}
	public void setTransApplyDate(String transApplyDate) {
		this.transApplyDate = transApplyDate;
	}
	public String getTransReceivDate() {
		return transReceivDate;
	}
	public void setTransReceivDate(String transReceivDate) {
		this.transReceivDate = transReceivDate;
	}
	public BigDecimal getTransAmount() {
		return transAmount;
	}
	public void setTransAmount(BigDecimal transAmount) {
		this.transAmount = transAmount;
	}
	public String getTransferSequenceNo() {
		return transferSequenceNo;
	}
	public void setTransferSequenceNo(String transferSequenceNo) {
		this.transferSequenceNo = transferSequenceNo;
	}
	public EBResultCodeDTO getResult() {
		return result;
	}
	public void setResult(EBResultCodeDTO result) {
		this.result = result;
	}
}
