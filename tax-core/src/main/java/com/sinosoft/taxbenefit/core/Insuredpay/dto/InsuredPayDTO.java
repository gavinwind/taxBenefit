package com.sinosoft.taxbenefit.core.Insuredpay.dto;

import java.math.BigDecimal;

import com.sinosoft.taxbenefit.api.base.EBResultCodeDTO;
/**
 * title：被保人既往理赔额查询返回对象信息
 * @author zhangyu
 * @date 2016年3月6日 下午1:18:25
 */
public class InsuredPayDTO {
	//业务号
	private String busiNo;
	//平台客户编码 
	private String customerNo;
	//原公司险种年度保额
	private BigDecimal sa;
	//当年度累计赔付金额
	private BigDecimal annualClaimSa;
	//终身累计赔付金额
	private BigDecimal wholeLifeClaimSa;
	
	private EBResultCodeDTO result; 
	
	public BigDecimal getSa() {
		return sa;
	}
	public void setSa(BigDecimal sa) {
		this.sa = sa;
	}
	public EBResultCodeDTO getResult() {
		return result;
	}
	public void setResult(EBResultCodeDTO result) {
		this.result = result;
	}
	public String getBusiNo() {
		return busiNo;
	}
	public void setBusiNo(String busiNo) {
		this.busiNo = busiNo;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public BigDecimal getAnnualClaimSa() {
		return annualClaimSa;
	}
	public void setAnnualClaimSa(BigDecimal annualClaimSa) {
		this.annualClaimSa = annualClaimSa;
	}
	public BigDecimal getWholeLifeClaimSa() {
		return wholeLifeClaimSa;
	}
	public void setWholeLifeClaimSa(BigDecimal wholeLifeClaimSa) {
		this.wholeLifeClaimSa = wholeLifeClaimSa;
	}
}