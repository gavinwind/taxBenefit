package com.sinosoft.taxbenefit.core.SelCustomerInfo.dto;
/**
 * 中保信客户查询客户DTO
 * @author zhangyu
 * @date 2016年3月11日 下午7:14:29
 */
public class CustomerInfo {
    //平台客户编码
	private String customerNo;
	//查询起期
	private String startDate;
	//查询止期
	private String endDate;
	//投保税优标识
	private String taxDiscountedIndi;
	//投保单号
	private String applyNo;
	//保单号码
	private String policyNo;
	
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getTaxDiscountedIndi() {
		return taxDiscountedIndi;
	}
	public void setTaxDiscountedIndi(String taxDiscountedIndi) {
		this.taxDiscountedIndi = taxDiscountedIndi;
	}
	public String getApplyNo() {
		return applyNo;
	}
	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}
	public String getPolicyNo() {
		return policyNo;
	}
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
}