package com.sinosoft.taxbenefit.core.renewalcancel.dto;

/**
 * 中保信-续保撤销上传请求请求
 * @author yangdongkai@outlook.com
 * @date 2016年3月22日 下午3:13:25
 */
public class ReqRenewalCancelPolicyDTO {
	//保单号
	private String policyNo;
	//续保批单号
	private String renewalEndorsementNo;
	//撤销日期
	private String cancelDate;
	//撤销原因
	private String cancelReason;
	/**
	 * @return the policyNo
	 */
	public String getPolicyNo() {
		return policyNo;
	}
	/**
	 * @param policyNo the policyNo to set
	 */
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	/**
	 * @return the renewalEndorsementNo
	 */
	public String getRenewalEndorsementNo() {
		return renewalEndorsementNo;
	}
	/**
	 * @param renewalEndorsementNo the renewalEndorsementNo to set
	 */
	public void setRenewalEndorsementNo(String renewalEndorsementNo) {
		this.renewalEndorsementNo = renewalEndorsementNo;
	}
	/**
	 * @return the cancelDate
	 */
	public String getCancelDate() {
		return cancelDate;
	}
	/**
	 * @param cancelDate the cancelDate to set
	 */
	public void setCancelDate(String cancelDate) {
		this.cancelDate = cancelDate;
	}
	/**
	 * @return the cancelReason
	 */
	public String getCancelReason() {
		return cancelReason;
	}
	/**
	 * @param cancelReason the cancelReason to set
	 */
	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}
}
