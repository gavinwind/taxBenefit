package com.sinosoft.taxbenefit.core.common.dto;

/**
 * 【请求中保信报文头的必要参数dto】
 * 
 * @Description: 请求必要参数dto
 * @author chenxin
 * @date 2016年2月3日 下午3:59:13
 * @version V1.0srsh@m
 */
public class RequestParamsDTO {
	private String userName;
	private String password;
	private String version;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	
}
