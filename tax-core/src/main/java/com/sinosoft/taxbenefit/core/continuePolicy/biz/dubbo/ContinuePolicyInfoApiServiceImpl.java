package com.sinosoft.taxbenefit.core.continuePolicy.biz.dubbo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqMContinuePolicyInfoDTO;
import com.sinosoft.taxbenefit.api.inter.ContinuePolicyInfoApiService;
import com.sinosoft.taxbenefit.core.continuePolicy.biz.service.ContinuePolicyCoreService;

/**
 * Title:续保信息上传服务接口实现
 * @author yangdongkai@outlook.com
 * @date 2016年3月2日 下午3:31:22
 */
@Service("continuePolicyInfoApiServiceImpl")
public class ContinuePolicyInfoApiServiceImpl extends TaxBaseService implements ContinuePolicyInfoApiService {
	@Autowired
	private ContinuePolicyCoreService policyServiceCore;

	@Override
	public ThirdSendResponseDTO continuePolicySettlement(
			ReqContinuePolicyInfoDTO reqContinuePolicyInfo,ParamHeadDTO paramHead) {
		return policyServiceCore.continuePolicySettlement(reqContinuePolicyInfo, paramHead);
	}

	@Override
	public ThirdSendResponseDTO continueMPolicySettlement(
			ReqMContinuePolicyInfoDTO continueMPolicyInfo,ParamHeadDTO paramHead) {
		return policyServiceCore.continueMPolicySettlement(continueMPolicyInfo, paramHead);
	}
}