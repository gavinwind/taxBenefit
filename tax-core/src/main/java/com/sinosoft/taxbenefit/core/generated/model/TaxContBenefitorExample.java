package com.sinosoft.taxbenefit.core.generated.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TaxContBenefitorExample {
    /**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table TAX_CONT_BENEFITOR
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	protected String orderByClause;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table TAX_CONT_BENEFITOR
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	protected boolean distinct;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table TAX_CONT_BENEFITOR
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	protected List<Criteria> oredCriteria;

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_BENEFITOR
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public TaxContBenefitorExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_BENEFITOR
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_BENEFITOR
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public String getOrderByClause() {
		return orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_BENEFITOR
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_BENEFITOR
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public boolean isDistinct() {
		return distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_BENEFITOR
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_BENEFITOR
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_BENEFITOR
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_BENEFITOR
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_BENEFITOR
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_BENEFITOR
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the database table TAX_CONT_BENEFITOR
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	protected abstract static class GeneratedCriteria {
		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value,
				String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property
						+ " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1,
				Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property
						+ " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andSidIsNull() {
			addCriterion("SID is null");
			return (Criteria) this;
		}

		public Criteria andSidIsNotNull() {
			addCriterion("SID is not null");
			return (Criteria) this;
		}

		public Criteria andSidEqualTo(Integer value) {
			addCriterion("SID =", value, "sid");
			return (Criteria) this;
		}

		public Criteria andSidNotEqualTo(Integer value) {
			addCriterion("SID <>", value, "sid");
			return (Criteria) this;
		}

		public Criteria andSidGreaterThan(Integer value) {
			addCriterion("SID >", value, "sid");
			return (Criteria) this;
		}

		public Criteria andSidGreaterThanOrEqualTo(Integer value) {
			addCriterion("SID >=", value, "sid");
			return (Criteria) this;
		}

		public Criteria andSidLessThan(Integer value) {
			addCriterion("SID <", value, "sid");
			return (Criteria) this;
		}

		public Criteria andSidLessThanOrEqualTo(Integer value) {
			addCriterion("SID <=", value, "sid");
			return (Criteria) this;
		}

		public Criteria andSidIn(List<Integer> values) {
			addCriterion("SID in", values, "sid");
			return (Criteria) this;
		}

		public Criteria andSidNotIn(List<Integer> values) {
			addCriterion("SID not in", values, "sid");
			return (Criteria) this;
		}

		public Criteria andSidBetween(Integer value1, Integer value2) {
			addCriterion("SID between", value1, value2, "sid");
			return (Criteria) this;
		}

		public Criteria andSidNotBetween(Integer value1, Integer value2) {
			addCriterion("SID not between", value1, value2, "sid");
			return (Criteria) this;
		}

		public Criteria andContIdIsNull() {
			addCriterion("CONT_ID is null");
			return (Criteria) this;
		}

		public Criteria andContIdIsNotNull() {
			addCriterion("CONT_ID is not null");
			return (Criteria) this;
		}

		public Criteria andContIdEqualTo(Integer value) {
			addCriterion("CONT_ID =", value, "contId");
			return (Criteria) this;
		}

		public Criteria andContIdNotEqualTo(Integer value) {
			addCriterion("CONT_ID <>", value, "contId");
			return (Criteria) this;
		}

		public Criteria andContIdGreaterThan(Integer value) {
			addCriterion("CONT_ID >", value, "contId");
			return (Criteria) this;
		}

		public Criteria andContIdGreaterThanOrEqualTo(Integer value) {
			addCriterion("CONT_ID >=", value, "contId");
			return (Criteria) this;
		}

		public Criteria andContIdLessThan(Integer value) {
			addCriterion("CONT_ID <", value, "contId");
			return (Criteria) this;
		}

		public Criteria andContIdLessThanOrEqualTo(Integer value) {
			addCriterion("CONT_ID <=", value, "contId");
			return (Criteria) this;
		}

		public Criteria andContIdIn(List<Integer> values) {
			addCriterion("CONT_ID in", values, "contId");
			return (Criteria) this;
		}

		public Criteria andContIdNotIn(List<Integer> values) {
			addCriterion("CONT_ID not in", values, "contId");
			return (Criteria) this;
		}

		public Criteria andContIdBetween(Integer value1, Integer value2) {
			addCriterion("CONT_ID between", value1, value2, "contId");
			return (Criteria) this;
		}

		public Criteria andContIdNotBetween(Integer value1, Integer value2) {
			addCriterion("CONT_ID not between", value1, value2, "contId");
			return (Criteria) this;
		}

		public Criteria andBenefitorTypeIsNull() {
			addCriterion("BENEFITOR_TYPE is null");
			return (Criteria) this;
		}

		public Criteria andBenefitorTypeIsNotNull() {
			addCriterion("BENEFITOR_TYPE is not null");
			return (Criteria) this;
		}

		public Criteria andBenefitorTypeEqualTo(String value) {
			addCriterion("BENEFITOR_TYPE =", value, "benefitorType");
			return (Criteria) this;
		}

		public Criteria andBenefitorTypeNotEqualTo(String value) {
			addCriterion("BENEFITOR_TYPE <>", value, "benefitorType");
			return (Criteria) this;
		}

		public Criteria andBenefitorTypeGreaterThan(String value) {
			addCriterion("BENEFITOR_TYPE >", value, "benefitorType");
			return (Criteria) this;
		}

		public Criteria andBenefitorTypeGreaterThanOrEqualTo(String value) {
			addCriterion("BENEFITOR_TYPE >=", value, "benefitorType");
			return (Criteria) this;
		}

		public Criteria andBenefitorTypeLessThan(String value) {
			addCriterion("BENEFITOR_TYPE <", value, "benefitorType");
			return (Criteria) this;
		}

		public Criteria andBenefitorTypeLessThanOrEqualTo(String value) {
			addCriterion("BENEFITOR_TYPE <=", value, "benefitorType");
			return (Criteria) this;
		}

		public Criteria andBenefitorTypeLike(String value) {
			addCriterion("BENEFITOR_TYPE like", value, "benefitorType");
			return (Criteria) this;
		}

		public Criteria andBenefitorTypeNotLike(String value) {
			addCriterion("BENEFITOR_TYPE not like", value, "benefitorType");
			return (Criteria) this;
		}

		public Criteria andBenefitorTypeIn(List<String> values) {
			addCriterion("BENEFITOR_TYPE in", values, "benefitorType");
			return (Criteria) this;
		}

		public Criteria andBenefitorTypeNotIn(List<String> values) {
			addCriterion("BENEFITOR_TYPE not in", values, "benefitorType");
			return (Criteria) this;
		}

		public Criteria andBenefitorTypeBetween(String value1, String value2) {
			addCriterion("BENEFITOR_TYPE between", value1, value2,
					"benefitorType");
			return (Criteria) this;
		}

		public Criteria andBenefitorTypeNotBetween(String value1, String value2) {
			addCriterion("BENEFITOR_TYPE not between", value1, value2,
					"benefitorType");
			return (Criteria) this;
		}

		public Criteria andNameIsNull() {
			addCriterion("NAME is null");
			return (Criteria) this;
		}

		public Criteria andNameIsNotNull() {
			addCriterion("NAME is not null");
			return (Criteria) this;
		}

		public Criteria andNameEqualTo(String value) {
			addCriterion("NAME =", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotEqualTo(String value) {
			addCriterion("NAME <>", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameGreaterThan(String value) {
			addCriterion("NAME >", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameGreaterThanOrEqualTo(String value) {
			addCriterion("NAME >=", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameLessThan(String value) {
			addCriterion("NAME <", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameLessThanOrEqualTo(String value) {
			addCriterion("NAME <=", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameLike(String value) {
			addCriterion("NAME like", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotLike(String value) {
			addCriterion("NAME not like", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameIn(List<String> values) {
			addCriterion("NAME in", values, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotIn(List<String> values) {
			addCriterion("NAME not in", values, "name");
			return (Criteria) this;
		}

		public Criteria andNameBetween(String value1, String value2) {
			addCriterion("NAME between", value1, value2, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotBetween(String value1, String value2) {
			addCriterion("NAME not between", value1, value2, "name");
			return (Criteria) this;
		}

		public Criteria andGenderIsNull() {
			addCriterion("GENDER is null");
			return (Criteria) this;
		}

		public Criteria andGenderIsNotNull() {
			addCriterion("GENDER is not null");
			return (Criteria) this;
		}

		public Criteria andGenderEqualTo(String value) {
			addCriterion("GENDER =", value, "gender");
			return (Criteria) this;
		}

		public Criteria andGenderNotEqualTo(String value) {
			addCriterion("GENDER <>", value, "gender");
			return (Criteria) this;
		}

		public Criteria andGenderGreaterThan(String value) {
			addCriterion("GENDER >", value, "gender");
			return (Criteria) this;
		}

		public Criteria andGenderGreaterThanOrEqualTo(String value) {
			addCriterion("GENDER >=", value, "gender");
			return (Criteria) this;
		}

		public Criteria andGenderLessThan(String value) {
			addCriterion("GENDER <", value, "gender");
			return (Criteria) this;
		}

		public Criteria andGenderLessThanOrEqualTo(String value) {
			addCriterion("GENDER <=", value, "gender");
			return (Criteria) this;
		}

		public Criteria andGenderLike(String value) {
			addCriterion("GENDER like", value, "gender");
			return (Criteria) this;
		}

		public Criteria andGenderNotLike(String value) {
			addCriterion("GENDER not like", value, "gender");
			return (Criteria) this;
		}

		public Criteria andGenderIn(List<String> values) {
			addCriterion("GENDER in", values, "gender");
			return (Criteria) this;
		}

		public Criteria andGenderNotIn(List<String> values) {
			addCriterion("GENDER not in", values, "gender");
			return (Criteria) this;
		}

		public Criteria andGenderBetween(String value1, String value2) {
			addCriterion("GENDER between", value1, value2, "gender");
			return (Criteria) this;
		}

		public Criteria andGenderNotBetween(String value1, String value2) {
			addCriterion("GENDER not between", value1, value2, "gender");
			return (Criteria) this;
		}

		public Criteria andBirthdayIsNull() {
			addCriterion("BIRTHDAY is null");
			return (Criteria) this;
		}

		public Criteria andBirthdayIsNotNull() {
			addCriterion("BIRTHDAY is not null");
			return (Criteria) this;
		}

		public Criteria andBirthdayEqualTo(Date value) {
			addCriterion("BIRTHDAY =", value, "birthday");
			return (Criteria) this;
		}

		public Criteria andBirthdayNotEqualTo(Date value) {
			addCriterion("BIRTHDAY <>", value, "birthday");
			return (Criteria) this;
		}

		public Criteria andBirthdayGreaterThan(Date value) {
			addCriterion("BIRTHDAY >", value, "birthday");
			return (Criteria) this;
		}

		public Criteria andBirthdayGreaterThanOrEqualTo(Date value) {
			addCriterion("BIRTHDAY >=", value, "birthday");
			return (Criteria) this;
		}

		public Criteria andBirthdayLessThan(Date value) {
			addCriterion("BIRTHDAY <", value, "birthday");
			return (Criteria) this;
		}

		public Criteria andBirthdayLessThanOrEqualTo(Date value) {
			addCriterion("BIRTHDAY <=", value, "birthday");
			return (Criteria) this;
		}

		public Criteria andBirthdayIn(List<Date> values) {
			addCriterion("BIRTHDAY in", values, "birthday");
			return (Criteria) this;
		}

		public Criteria andBirthdayNotIn(List<Date> values) {
			addCriterion("BIRTHDAY not in", values, "birthday");
			return (Criteria) this;
		}

		public Criteria andBirthdayBetween(Date value1, Date value2) {
			addCriterion("BIRTHDAY between", value1, value2, "birthday");
			return (Criteria) this;
		}

		public Criteria andBirthdayNotBetween(Date value1, Date value2) {
			addCriterion("BIRTHDAY not between", value1, value2, "birthday");
			return (Criteria) this;
		}

		public Criteria andCardTypeIsNull() {
			addCriterion("CARD_TYPE is null");
			return (Criteria) this;
		}

		public Criteria andCardTypeIsNotNull() {
			addCriterion("CARD_TYPE is not null");
			return (Criteria) this;
		}

		public Criteria andCardTypeEqualTo(String value) {
			addCriterion("CARD_TYPE =", value, "cardType");
			return (Criteria) this;
		}

		public Criteria andCardTypeNotEqualTo(String value) {
			addCriterion("CARD_TYPE <>", value, "cardType");
			return (Criteria) this;
		}

		public Criteria andCardTypeGreaterThan(String value) {
			addCriterion("CARD_TYPE >", value, "cardType");
			return (Criteria) this;
		}

		public Criteria andCardTypeGreaterThanOrEqualTo(String value) {
			addCriterion("CARD_TYPE >=", value, "cardType");
			return (Criteria) this;
		}

		public Criteria andCardTypeLessThan(String value) {
			addCriterion("CARD_TYPE <", value, "cardType");
			return (Criteria) this;
		}

		public Criteria andCardTypeLessThanOrEqualTo(String value) {
			addCriterion("CARD_TYPE <=", value, "cardType");
			return (Criteria) this;
		}

		public Criteria andCardTypeLike(String value) {
			addCriterion("CARD_TYPE like", value, "cardType");
			return (Criteria) this;
		}

		public Criteria andCardTypeNotLike(String value) {
			addCriterion("CARD_TYPE not like", value, "cardType");
			return (Criteria) this;
		}

		public Criteria andCardTypeIn(List<String> values) {
			addCriterion("CARD_TYPE in", values, "cardType");
			return (Criteria) this;
		}

		public Criteria andCardTypeNotIn(List<String> values) {
			addCriterion("CARD_TYPE not in", values, "cardType");
			return (Criteria) this;
		}

		public Criteria andCardTypeBetween(String value1, String value2) {
			addCriterion("CARD_TYPE between", value1, value2, "cardType");
			return (Criteria) this;
		}

		public Criteria andCardTypeNotBetween(String value1, String value2) {
			addCriterion("CARD_TYPE not between", value1, value2, "cardType");
			return (Criteria) this;
		}

		public Criteria andCardNoIsNull() {
			addCriterion("CARD_NO is null");
			return (Criteria) this;
		}

		public Criteria andCardNoIsNotNull() {
			addCriterion("CARD_NO is not null");
			return (Criteria) this;
		}

		public Criteria andCardNoEqualTo(String value) {
			addCriterion("CARD_NO =", value, "cardNo");
			return (Criteria) this;
		}

		public Criteria andCardNoNotEqualTo(String value) {
			addCriterion("CARD_NO <>", value, "cardNo");
			return (Criteria) this;
		}

		public Criteria andCardNoGreaterThan(String value) {
			addCriterion("CARD_NO >", value, "cardNo");
			return (Criteria) this;
		}

		public Criteria andCardNoGreaterThanOrEqualTo(String value) {
			addCriterion("CARD_NO >=", value, "cardNo");
			return (Criteria) this;
		}

		public Criteria andCardNoLessThan(String value) {
			addCriterion("CARD_NO <", value, "cardNo");
			return (Criteria) this;
		}

		public Criteria andCardNoLessThanOrEqualTo(String value) {
			addCriterion("CARD_NO <=", value, "cardNo");
			return (Criteria) this;
		}

		public Criteria andCardNoLike(String value) {
			addCriterion("CARD_NO like", value, "cardNo");
			return (Criteria) this;
		}

		public Criteria andCardNoNotLike(String value) {
			addCriterion("CARD_NO not like", value, "cardNo");
			return (Criteria) this;
		}

		public Criteria andCardNoIn(List<String> values) {
			addCriterion("CARD_NO in", values, "cardNo");
			return (Criteria) this;
		}

		public Criteria andCardNoNotIn(List<String> values) {
			addCriterion("CARD_NO not in", values, "cardNo");
			return (Criteria) this;
		}

		public Criteria andCardNoBetween(String value1, String value2) {
			addCriterion("CARD_NO between", value1, value2, "cardNo");
			return (Criteria) this;
		}

		public Criteria andCardNoNotBetween(String value1, String value2) {
			addCriterion("CARD_NO not between", value1, value2, "cardNo");
			return (Criteria) this;
		}

		public Criteria andNationalityIsNull() {
			addCriterion("NATIONALITY is null");
			return (Criteria) this;
		}

		public Criteria andNationalityIsNotNull() {
			addCriterion("NATIONALITY is not null");
			return (Criteria) this;
		}

		public Criteria andNationalityEqualTo(String value) {
			addCriterion("NATIONALITY =", value, "nationality");
			return (Criteria) this;
		}

		public Criteria andNationalityNotEqualTo(String value) {
			addCriterion("NATIONALITY <>", value, "nationality");
			return (Criteria) this;
		}

		public Criteria andNationalityGreaterThan(String value) {
			addCriterion("NATIONALITY >", value, "nationality");
			return (Criteria) this;
		}

		public Criteria andNationalityGreaterThanOrEqualTo(String value) {
			addCriterion("NATIONALITY >=", value, "nationality");
			return (Criteria) this;
		}

		public Criteria andNationalityLessThan(String value) {
			addCriterion("NATIONALITY <", value, "nationality");
			return (Criteria) this;
		}

		public Criteria andNationalityLessThanOrEqualTo(String value) {
			addCriterion("NATIONALITY <=", value, "nationality");
			return (Criteria) this;
		}

		public Criteria andNationalityLike(String value) {
			addCriterion("NATIONALITY like", value, "nationality");
			return (Criteria) this;
		}

		public Criteria andNationalityNotLike(String value) {
			addCriterion("NATIONALITY not like", value, "nationality");
			return (Criteria) this;
		}

		public Criteria andNationalityIn(List<String> values) {
			addCriterion("NATIONALITY in", values, "nationality");
			return (Criteria) this;
		}

		public Criteria andNationalityNotIn(List<String> values) {
			addCriterion("NATIONALITY not in", values, "nationality");
			return (Criteria) this;
		}

		public Criteria andNationalityBetween(String value1, String value2) {
			addCriterion("NATIONALITY between", value1, value2, "nationality");
			return (Criteria) this;
		}

		public Criteria andNationalityNotBetween(String value1, String value2) {
			addCriterion("NATIONALITY not between", value1, value2,
					"nationality");
			return (Criteria) this;
		}

		public Criteria andToInsuredRelationIsNull() {
			addCriterion("TO_INSURED_RELATION is null");
			return (Criteria) this;
		}

		public Criteria andToInsuredRelationIsNotNull() {
			addCriterion("TO_INSURED_RELATION is not null");
			return (Criteria) this;
		}

		public Criteria andToInsuredRelationEqualTo(String value) {
			addCriterion("TO_INSURED_RELATION =", value, "toInsuredRelation");
			return (Criteria) this;
		}

		public Criteria andToInsuredRelationNotEqualTo(String value) {
			addCriterion("TO_INSURED_RELATION <>", value, "toInsuredRelation");
			return (Criteria) this;
		}

		public Criteria andToInsuredRelationGreaterThan(String value) {
			addCriterion("TO_INSURED_RELATION >", value, "toInsuredRelation");
			return (Criteria) this;
		}

		public Criteria andToInsuredRelationGreaterThanOrEqualTo(String value) {
			addCriterion("TO_INSURED_RELATION >=", value, "toInsuredRelation");
			return (Criteria) this;
		}

		public Criteria andToInsuredRelationLessThan(String value) {
			addCriterion("TO_INSURED_RELATION <", value, "toInsuredRelation");
			return (Criteria) this;
		}

		public Criteria andToInsuredRelationLessThanOrEqualTo(String value) {
			addCriterion("TO_INSURED_RELATION <=", value, "toInsuredRelation");
			return (Criteria) this;
		}

		public Criteria andToInsuredRelationLike(String value) {
			addCriterion("TO_INSURED_RELATION like", value, "toInsuredRelation");
			return (Criteria) this;
		}

		public Criteria andToInsuredRelationNotLike(String value) {
			addCriterion("TO_INSURED_RELATION not like", value,
					"toInsuredRelation");
			return (Criteria) this;
		}

		public Criteria andToInsuredRelationIn(List<String> values) {
			addCriterion("TO_INSURED_RELATION in", values, "toInsuredRelation");
			return (Criteria) this;
		}

		public Criteria andToInsuredRelationNotIn(List<String> values) {
			addCriterion("TO_INSURED_RELATION not in", values,
					"toInsuredRelation");
			return (Criteria) this;
		}

		public Criteria andToInsuredRelationBetween(String value1, String value2) {
			addCriterion("TO_INSURED_RELATION between", value1, value2,
					"toInsuredRelation");
			return (Criteria) this;
		}

		public Criteria andToInsuredRelationNotBetween(String value1,
				String value2) {
			addCriterion("TO_INSURED_RELATION not between", value1, value2,
					"toInsuredRelation");
			return (Criteria) this;
		}

		public Criteria andPriorityIsNull() {
			addCriterion("PRIORITY is null");
			return (Criteria) this;
		}

		public Criteria andPriorityIsNotNull() {
			addCriterion("PRIORITY is not null");
			return (Criteria) this;
		}

		public Criteria andPriorityEqualTo(Integer value) {
			addCriterion("PRIORITY =", value, "priority");
			return (Criteria) this;
		}

		public Criteria andPriorityNotEqualTo(Integer value) {
			addCriterion("PRIORITY <>", value, "priority");
			return (Criteria) this;
		}

		public Criteria andPriorityGreaterThan(Integer value) {
			addCriterion("PRIORITY >", value, "priority");
			return (Criteria) this;
		}

		public Criteria andPriorityGreaterThanOrEqualTo(Integer value) {
			addCriterion("PRIORITY >=", value, "priority");
			return (Criteria) this;
		}

		public Criteria andPriorityLessThan(Integer value) {
			addCriterion("PRIORITY <", value, "priority");
			return (Criteria) this;
		}

		public Criteria andPriorityLessThanOrEqualTo(Integer value) {
			addCriterion("PRIORITY <=", value, "priority");
			return (Criteria) this;
		}

		public Criteria andPriorityIn(List<Integer> values) {
			addCriterion("PRIORITY in", values, "priority");
			return (Criteria) this;
		}

		public Criteria andPriorityNotIn(List<Integer> values) {
			addCriterion("PRIORITY not in", values, "priority");
			return (Criteria) this;
		}

		public Criteria andPriorityBetween(Integer value1, Integer value2) {
			addCriterion("PRIORITY between", value1, value2, "priority");
			return (Criteria) this;
		}

		public Criteria andPriorityNotBetween(Integer value1, Integer value2) {
			addCriterion("PRIORITY not between", value1, value2, "priority");
			return (Criteria) this;
		}

		public Criteria andProportionIsNull() {
			addCriterion("PROPORTION is null");
			return (Criteria) this;
		}

		public Criteria andProportionIsNotNull() {
			addCriterion("PROPORTION is not null");
			return (Criteria) this;
		}

		public Criteria andProportionEqualTo(BigDecimal value) {
			addCriterion("PROPORTION =", value, "proportion");
			return (Criteria) this;
		}

		public Criteria andProportionNotEqualTo(BigDecimal value) {
			addCriterion("PROPORTION <>", value, "proportion");
			return (Criteria) this;
		}

		public Criteria andProportionGreaterThan(BigDecimal value) {
			addCriterion("PROPORTION >", value, "proportion");
			return (Criteria) this;
		}

		public Criteria andProportionGreaterThanOrEqualTo(BigDecimal value) {
			addCriterion("PROPORTION >=", value, "proportion");
			return (Criteria) this;
		}

		public Criteria andProportionLessThan(BigDecimal value) {
			addCriterion("PROPORTION <", value, "proportion");
			return (Criteria) this;
		}

		public Criteria andProportionLessThanOrEqualTo(BigDecimal value) {
			addCriterion("PROPORTION <=", value, "proportion");
			return (Criteria) this;
		}

		public Criteria andProportionIn(List<BigDecimal> values) {
			addCriterion("PROPORTION in", values, "proportion");
			return (Criteria) this;
		}

		public Criteria andProportionNotIn(List<BigDecimal> values) {
			addCriterion("PROPORTION not in", values, "proportion");
			return (Criteria) this;
		}

		public Criteria andProportionBetween(BigDecimal value1,
				BigDecimal value2) {
			addCriterion("PROPORTION between", value1, value2, "proportion");
			return (Criteria) this;
		}

		public Criteria andProportionNotBetween(BigDecimal value1,
				BigDecimal value2) {
			addCriterion("PROPORTION not between", value1, value2, "proportion");
			return (Criteria) this;
		}

		public Criteria andRcStateIsNull() {
			addCriterion("RC_STATE is null");
			return (Criteria) this;
		}

		public Criteria andRcStateIsNotNull() {
			addCriterion("RC_STATE is not null");
			return (Criteria) this;
		}

		public Criteria andRcStateEqualTo(String value) {
			addCriterion("RC_STATE =", value, "rcState");
			return (Criteria) this;
		}

		public Criteria andRcStateNotEqualTo(String value) {
			addCriterion("RC_STATE <>", value, "rcState");
			return (Criteria) this;
		}

		public Criteria andRcStateGreaterThan(String value) {
			addCriterion("RC_STATE >", value, "rcState");
			return (Criteria) this;
		}

		public Criteria andRcStateGreaterThanOrEqualTo(String value) {
			addCriterion("RC_STATE >=", value, "rcState");
			return (Criteria) this;
		}

		public Criteria andRcStateLessThan(String value) {
			addCriterion("RC_STATE <", value, "rcState");
			return (Criteria) this;
		}

		public Criteria andRcStateLessThanOrEqualTo(String value) {
			addCriterion("RC_STATE <=", value, "rcState");
			return (Criteria) this;
		}

		public Criteria andRcStateLike(String value) {
			addCriterion("RC_STATE like", value, "rcState");
			return (Criteria) this;
		}

		public Criteria andRcStateNotLike(String value) {
			addCriterion("RC_STATE not like", value, "rcState");
			return (Criteria) this;
		}

		public Criteria andRcStateIn(List<String> values) {
			addCriterion("RC_STATE in", values, "rcState");
			return (Criteria) this;
		}

		public Criteria andRcStateNotIn(List<String> values) {
			addCriterion("RC_STATE not in", values, "rcState");
			return (Criteria) this;
		}

		public Criteria andRcStateBetween(String value1, String value2) {
			addCriterion("RC_STATE between", value1, value2, "rcState");
			return (Criteria) this;
		}

		public Criteria andRcStateNotBetween(String value1, String value2) {
			addCriterion("RC_STATE not between", value1, value2, "rcState");
			return (Criteria) this;
		}

		public Criteria andCreateDateIsNull() {
			addCriterion("CREATE_DATE is null");
			return (Criteria) this;
		}

		public Criteria andCreateDateIsNotNull() {
			addCriterion("CREATE_DATE is not null");
			return (Criteria) this;
		}

		public Criteria andCreateDateEqualTo(Date value) {
			addCriterion("CREATE_DATE =", value, "createDate");
			return (Criteria) this;
		}

		public Criteria andCreateDateNotEqualTo(Date value) {
			addCriterion("CREATE_DATE <>", value, "createDate");
			return (Criteria) this;
		}

		public Criteria andCreateDateGreaterThan(Date value) {
			addCriterion("CREATE_DATE >", value, "createDate");
			return (Criteria) this;
		}

		public Criteria andCreateDateGreaterThanOrEqualTo(Date value) {
			addCriterion("CREATE_DATE >=", value, "createDate");
			return (Criteria) this;
		}

		public Criteria andCreateDateLessThan(Date value) {
			addCriterion("CREATE_DATE <", value, "createDate");
			return (Criteria) this;
		}

		public Criteria andCreateDateLessThanOrEqualTo(Date value) {
			addCriterion("CREATE_DATE <=", value, "createDate");
			return (Criteria) this;
		}

		public Criteria andCreateDateIn(List<Date> values) {
			addCriterion("CREATE_DATE in", values, "createDate");
			return (Criteria) this;
		}

		public Criteria andCreateDateNotIn(List<Date> values) {
			addCriterion("CREATE_DATE not in", values, "createDate");
			return (Criteria) this;
		}

		public Criteria andCreateDateBetween(Date value1, Date value2) {
			addCriterion("CREATE_DATE between", value1, value2, "createDate");
			return (Criteria) this;
		}

		public Criteria andCreateDateNotBetween(Date value1, Date value2) {
			addCriterion("CREATE_DATE not between", value1, value2,
					"createDate");
			return (Criteria) this;
		}

		public Criteria andCreatorIdIsNull() {
			addCriterion("CREATOR_ID is null");
			return (Criteria) this;
		}

		public Criteria andCreatorIdIsNotNull() {
			addCriterion("CREATOR_ID is not null");
			return (Criteria) this;
		}

		public Criteria andCreatorIdEqualTo(Integer value) {
			addCriterion("CREATOR_ID =", value, "creatorId");
			return (Criteria) this;
		}

		public Criteria andCreatorIdNotEqualTo(Integer value) {
			addCriterion("CREATOR_ID <>", value, "creatorId");
			return (Criteria) this;
		}

		public Criteria andCreatorIdGreaterThan(Integer value) {
			addCriterion("CREATOR_ID >", value, "creatorId");
			return (Criteria) this;
		}

		public Criteria andCreatorIdGreaterThanOrEqualTo(Integer value) {
			addCriterion("CREATOR_ID >=", value, "creatorId");
			return (Criteria) this;
		}

		public Criteria andCreatorIdLessThan(Integer value) {
			addCriterion("CREATOR_ID <", value, "creatorId");
			return (Criteria) this;
		}

		public Criteria andCreatorIdLessThanOrEqualTo(Integer value) {
			addCriterion("CREATOR_ID <=", value, "creatorId");
			return (Criteria) this;
		}

		public Criteria andCreatorIdIn(List<Integer> values) {
			addCriterion("CREATOR_ID in", values, "creatorId");
			return (Criteria) this;
		}

		public Criteria andCreatorIdNotIn(List<Integer> values) {
			addCriterion("CREATOR_ID not in", values, "creatorId");
			return (Criteria) this;
		}

		public Criteria andCreatorIdBetween(Integer value1, Integer value2) {
			addCriterion("CREATOR_ID between", value1, value2, "creatorId");
			return (Criteria) this;
		}

		public Criteria andCreatorIdNotBetween(Integer value1, Integer value2) {
			addCriterion("CREATOR_ID not between", value1, value2, "creatorId");
			return (Criteria) this;
		}

		public Criteria andModifyDateIsNull() {
			addCriterion("MODIFY_DATE is null");
			return (Criteria) this;
		}

		public Criteria andModifyDateIsNotNull() {
			addCriterion("MODIFY_DATE is not null");
			return (Criteria) this;
		}

		public Criteria andModifyDateEqualTo(Date value) {
			addCriterion("MODIFY_DATE =", value, "modifyDate");
			return (Criteria) this;
		}

		public Criteria andModifyDateNotEqualTo(Date value) {
			addCriterion("MODIFY_DATE <>", value, "modifyDate");
			return (Criteria) this;
		}

		public Criteria andModifyDateGreaterThan(Date value) {
			addCriterion("MODIFY_DATE >", value, "modifyDate");
			return (Criteria) this;
		}

		public Criteria andModifyDateGreaterThanOrEqualTo(Date value) {
			addCriterion("MODIFY_DATE >=", value, "modifyDate");
			return (Criteria) this;
		}

		public Criteria andModifyDateLessThan(Date value) {
			addCriterion("MODIFY_DATE <", value, "modifyDate");
			return (Criteria) this;
		}

		public Criteria andModifyDateLessThanOrEqualTo(Date value) {
			addCriterion("MODIFY_DATE <=", value, "modifyDate");
			return (Criteria) this;
		}

		public Criteria andModifyDateIn(List<Date> values) {
			addCriterion("MODIFY_DATE in", values, "modifyDate");
			return (Criteria) this;
		}

		public Criteria andModifyDateNotIn(List<Date> values) {
			addCriterion("MODIFY_DATE not in", values, "modifyDate");
			return (Criteria) this;
		}

		public Criteria andModifyDateBetween(Date value1, Date value2) {
			addCriterion("MODIFY_DATE between", value1, value2, "modifyDate");
			return (Criteria) this;
		}

		public Criteria andModifyDateNotBetween(Date value1, Date value2) {
			addCriterion("MODIFY_DATE not between", value1, value2,
					"modifyDate");
			return (Criteria) this;
		}

		public Criteria andModifierIdIsNull() {
			addCriterion("MODIFIER_ID is null");
			return (Criteria) this;
		}

		public Criteria andModifierIdIsNotNull() {
			addCriterion("MODIFIER_ID is not null");
			return (Criteria) this;
		}

		public Criteria andModifierIdEqualTo(Integer value) {
			addCriterion("MODIFIER_ID =", value, "modifierId");
			return (Criteria) this;
		}

		public Criteria andModifierIdNotEqualTo(Integer value) {
			addCriterion("MODIFIER_ID <>", value, "modifierId");
			return (Criteria) this;
		}

		public Criteria andModifierIdGreaterThan(Integer value) {
			addCriterion("MODIFIER_ID >", value, "modifierId");
			return (Criteria) this;
		}

		public Criteria andModifierIdGreaterThanOrEqualTo(Integer value) {
			addCriterion("MODIFIER_ID >=", value, "modifierId");
			return (Criteria) this;
		}

		public Criteria andModifierIdLessThan(Integer value) {
			addCriterion("MODIFIER_ID <", value, "modifierId");
			return (Criteria) this;
		}

		public Criteria andModifierIdLessThanOrEqualTo(Integer value) {
			addCriterion("MODIFIER_ID <=", value, "modifierId");
			return (Criteria) this;
		}

		public Criteria andModifierIdIn(List<Integer> values) {
			addCriterion("MODIFIER_ID in", values, "modifierId");
			return (Criteria) this;
		}

		public Criteria andModifierIdNotIn(List<Integer> values) {
			addCriterion("MODIFIER_ID not in", values, "modifierId");
			return (Criteria) this;
		}

		public Criteria andModifierIdBetween(Integer value1, Integer value2) {
			addCriterion("MODIFIER_ID between", value1, value2, "modifierId");
			return (Criteria) this;
		}

		public Criteria andModifierIdNotBetween(Integer value1, Integer value2) {
			addCriterion("MODIFIER_ID not between", value1, value2,
					"modifierId");
			return (Criteria) this;
		}
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the database table TAX_CONT_BENEFITOR
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public static class Criterion {
		private String condition;
		private Object value;
		private Object secondValue;
		private boolean noValue;
		private boolean singleValue;
		private boolean betweenValue;
		private boolean listValue;
		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			} else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue,
				String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}
	}

	/**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table TAX_CONT_BENEFITOR
     *
     * @mbggenerated do_not_delete_during_merge Tue Mar 01 17:03:53 CST 2016
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }
}