package com.sinosoft.taxbenefit.core.policyTransfer.dto;

import java.util.List;

import com.sinosoft.taxbenefit.api.base.EBResultCodeDTO;

public class ResEPolicyTransOutRegisterQueryDTO {
	// 转出信息
	private List<EPolicyTransferOutDTO> transferOutList;
	// 返回结果
	private EBResultCodeDTO result;
	public List<EPolicyTransferOutDTO> getTransferOutList() {
		return transferOutList;
	}
	public void setTransferOutList(List<EPolicyTransferOutDTO> transferOutList) {
		this.transferOutList = transferOutList;
	}
	public EBResultCodeDTO getResult() {
		return result;
	}
	public void setResult(EBResultCodeDTO result) {
		this.result = result;
	}
	
}
