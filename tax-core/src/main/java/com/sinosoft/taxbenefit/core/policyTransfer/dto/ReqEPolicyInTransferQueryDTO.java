package com.sinosoft.taxbenefit.core.policyTransfer.dto;

/**
 * 请求（中保信）保单转入余额信息查询对象
 * @author SheChunMing
 */
public class ReqEPolicyInTransferQueryDTO {
	// 查询起期
	private String queryStartDate;
	// 查询止期
	private String queryEndDate;

	public String getQueryStartDate() {
		return queryStartDate;
	}

	public void setQueryStartDate(String queryStartDate) {
		this.queryStartDate = queryStartDate;
	}

	public String getQueryEndDate() {
		return queryEndDate;
	}

	public void setQueryEndDate(String queryEndDate) {
		this.queryEndDate = queryEndDate;
	}
	
}
