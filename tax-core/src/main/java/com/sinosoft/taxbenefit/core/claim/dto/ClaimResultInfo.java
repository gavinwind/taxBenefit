package com.sinosoft.taxbenefit.core.claim.dto;

import com.sinosoft.taxbenefit.api.base.EBResultCodeDTO;

/**
 * 中保信返回的数据
 * @author zhangke
 */
public class ClaimResultInfo {
	/** 理赔赔案号 */
	private String claimNo;
	/** 理赔编码 */
	private String claimCodeP;
	/** 返回结果信息 */
	private EBResultCodeDTO result;

	public String getClaimNo() {
		return claimNo;
	}

	public void setClaimNo(String claimNo) {
		this.claimNo = claimNo;
	}

	public String getClaimCodeP() {
		return claimCodeP;
	}

	public void setClaimCodeP(String claimCodeP) {
		this.claimCodeP = claimCodeP;
	}

	public EBResultCodeDTO getResult() {
		return result;
	}

	public void setResult(EBResultCodeDTO result) {
		this.result = result;
	}
}
