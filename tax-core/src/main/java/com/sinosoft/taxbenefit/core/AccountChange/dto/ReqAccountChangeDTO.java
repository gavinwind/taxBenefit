package com.sinosoft.taxbenefit.core.AccountChange.dto;

import java.util.List;
/**
 * 账户变更上传请求DTO（中保信）
 * @author zhangyu
 * @date 2016年3月16日 下午5:41:04
 */
public class ReqAccountChangeDTO {
	
	private List<AccountChangeDTO> savingAccountFeeList;
	
	public List<AccountChangeDTO> getSavingAccountFeeList() {
		return savingAccountFeeList;
	}
	public void setSavingAccountFeeList(List<AccountChangeDTO> savingAccountFeeList) {
		this.savingAccountFeeList = savingAccountFeeList;
	}
}