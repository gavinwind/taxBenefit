package com.sinosoft.taxbenefit.core.claim.dto;
/**
 * 理赔撤销请求中保信DTO
 * @author zhangke
 *
 */
public class ClaimReversalDTO {
	/*赔案信息*/
	private ClaimReversalInfoDTO claim;

	public ClaimReversalInfoDTO getClaim() {
		return claim;
	}

	public void setClaim(ClaimReversalInfoDTO claim) {
		this.claim = claim;
	}
}
