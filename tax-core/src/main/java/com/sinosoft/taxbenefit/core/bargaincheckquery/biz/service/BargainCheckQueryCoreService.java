package com.sinosoft.taxbenefit.core.bargaincheckquery.biz.service;

import com.sinosoft.taxbenefit.api.dto.request.bargaincheckquery.ReqBargainCheckQueryDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;

public interface BargainCheckQueryCoreService {
	/**
	 * 交易核对信息查询
	 * @param reqBargainCheckQueryDTO
	 * @param paramHead
	 * @return
	 */
	ThirdSendResponseDTO bargainCheckQuery(ReqBargainCheckQueryDTO reqBargainCheckQueryDTO,ParamHeadDTO paramHead);
}
