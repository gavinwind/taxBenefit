package com.sinosoft.taxbenefit.core.AccountChange.biz.service;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.accountchange.ReqAccountInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.core.generated.model.TaxFaultTask;

public interface AccountChangeService {
    /**
     * 添加账户变更信息(单笔)
     * @param reqAccountInfoDTO
     * @param paramHeadDTO
     * @return
     */
	ThirdSendResponseDTO addAccountChange(ReqAccountInfoDTO reqAccountInfoDTO,ParamHeadDTO paramHeadDTO);
	/**
	 * 添加账户变更信息(多笔)
	 * @param reqAccountInfoDTO
	 * @param paramHeadDTO
	 * @return
	 */
	ThirdSendResponseDTO addAccountListChange(List<ReqAccountInfoDTO> reqAccountInfoDTO,ParamHeadDTO paramHeadDTO);
	/**
	 * 账户变更异常任务服务方法
	 * @param reqJSON	核心请求JSON
	 * @param portCode	异常任务接口编码
	 * @param faultTask 任务记录
	 * @return
	 */
	TaxFaultTask accountChangeExceptionJob(String reqJSON, String portCode, Integer faultTaskId);
}