package com.sinosoft.taxbenefit.core.securityguard.config;

public enum ENUM_POLICY_TYPE {

	SINGLE_POLICY_HOLDER("01", "个人投保单"), GROUP_POLICY_HOLDER("02", "团体投保单");

	private final String code;
	private final String desc;

	ENUM_POLICY_TYPE(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public String code() {
		return code;
	}

	public String desc() {
		return desc;
	}

	public static ENUM_POLICY_TYPE getEnumByKey(String key) {
		for (ENUM_POLICY_TYPE enumItem : ENUM_POLICY_TYPE.values()) {
			if (key.equals(enumItem.code())) {
				return enumItem;
			}
		}
		return null;
	}
}
