package com.sinosoft.taxbenefit.core.ebaowebservice;

/**
 * 中保信返回报文
 * @author SheChunMing
 * 主要为存储发送日志表中的接受报文
 */
public class ResponseJSON {
	// 接收报文头
	private ResponseHeader responseHeader;
	// 接收报文体
	private ResponseBody responseBody;
	public ResponseHeader getResponseHeader() {
		return responseHeader;
	}
	public void setResponseHeader(ResponseHeader responseHeader) {
		this.responseHeader = responseHeader;
	}
	public ResponseBody getResponseBody() {
		return responseBody;
	}
	public void setResponseBody(ResponseBody responseBody) {
		this.responseBody = responseBody;
	}
}
