package com.sinosoft.taxbenefit.core.policyTransfer.dto;

import java.util.List;

import com.sinosoft.taxbenefit.api.base.EBResultCodeDTO;

/**
 * （中保信）保单转出信息查询结果 返回解析DTO
 * @author SheChunMing
 */
public class ResEPolicyTransOutQueryDTO {
	// 保单转移-转入信息
	private List<ReqEPolicyTransferDTO> transferInList;
	// 返回结果
	private EBResultCodeDTO result;

	public List<ReqEPolicyTransferDTO> getTransferInList() {
		return transferInList;
	}

	public void setTransferInList(List<ReqEPolicyTransferDTO> transferInList) {
		this.transferInList = transferInList;
	}

	public EBResultCodeDTO getResult() {
		return result;
	}

	public void setResult(EBResultCodeDTO result) {
		this.result = result;
	}
}
