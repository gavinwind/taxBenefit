package com.sinosoft.taxbenefit.core.common.config;

/**
 * 接口状态枚举类
 * @author SheChunMing
 */
public enum ENUM_PORT_STATE {
	EMPLOY("01", "启用"), CEASE("02", "停用");
	/** 枚举code */
	private String code;
	/** 枚举value或者code说明 */
	private String desc;

	ENUM_PORT_STATE(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public String code() {
		return code;
	}

	public String desc() {
		return desc;
	}

}
