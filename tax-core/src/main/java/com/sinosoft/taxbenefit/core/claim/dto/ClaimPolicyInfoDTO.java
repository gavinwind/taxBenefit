package com.sinosoft.taxbenefit.core.claim.dto;

import java.util.List;

/**
 * 理赔保单信息
 * 
 * @author zhangke
 *
 */
public class ClaimPolicyInfoDTO  {
	/** 保单号 **/
	private String policyNo;
	/** 分单号 **/
	private String sequenceNo;
	/** 出险人客户编码 **/
	private String customerNo;
	/** 当前保单状态 **/
	private String policyStatus;
	/** 保单赔付金额 **/
	private String claimAmount;
	/** 险种赔付信息 */
	private List<ClaimCoveragePaymentInfoDTO> coveragePaymentList;

	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public String getSequenceNo() {
		return sequenceNo;
	}

	public void setSequenceNo(String sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getPolicyStatus() {
		return policyStatus;
	}

	public void setPolicyStatus(String policyStatus) {
		this.policyStatus = policyStatus;
	}

	public String getClaimAmount() {
		return claimAmount;
	}

	public void setClaimAmount(String claimAmount) {
		this.claimAmount = claimAmount;
	}

	public List<ClaimCoveragePaymentInfoDTO> getCoveragePaymentList() {
		return coveragePaymentList;
	}

	public void setCoveragePaymentList(
			List<ClaimCoveragePaymentInfoDTO> coveragePaymentList) {
		this.coveragePaymentList = coveragePaymentList;
	}

}
