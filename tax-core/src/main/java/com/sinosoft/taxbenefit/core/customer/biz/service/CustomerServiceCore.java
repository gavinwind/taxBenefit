package com.sinosoft.taxbenefit.core.customer.biz.service;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqCustomerInfoDTO;
import com.sinosoft.taxbenefit.core.generated.model.TaxCustomer;
import com.sinosoft.taxbenefit.core.generated.model.TaxFaultTask;

public interface CustomerServiceCore {
	/**
	 * 客户验证(单笔)
	 * @param customerDTO
	 * @param paramHeadDTO
	 * @return
	 */
	ThirdSendResponseDTO  addCustomer(ReqCustomerInfoDTO customerDTO,ParamHeadDTO paramHeadDTO);	
    /**
     *  客户验证(多笔)
     * @param reqcustomersDTO
     * @param paramHeadDTO
     * @return
     */
	ThirdSendResponseDTO addCustomers(List<ReqCustomerInfoDTO> reqcustomersDTO, ParamHeadDTO paramHeadDTO);
	/**
	 * 客户验证异常任务服务方法
	 * @param reqJSON	核心请求JSON
	 * @param portCode	异常任务接口编码
	 * @param faultTask 任务记录
	 * @return
	 */
	TaxFaultTask customerExceptionJob(String reqJSON, String portCode, Integer faultTaskId);
	
	/**
	 * 添加新的客户信息
	 * @param taxCustomer 
	 * @return 客户存在时返回原有的客户信息 不存在则返回新增的客户信息
	 */
	TaxCustomer addCustomer(TaxCustomer taxCustomer);
	
	/**
	 * 添加或修改客户信息
	 * @param taxCustomer
	 */
	void addOrUpdateCustomer(TaxCustomer taxCustomer);
}