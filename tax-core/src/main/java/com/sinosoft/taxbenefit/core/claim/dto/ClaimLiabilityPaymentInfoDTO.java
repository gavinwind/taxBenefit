package com.sinosoft.taxbenefit.core.claim.dto;

import java.util.List;

/**
 * 理赔责任赔付信息
 * 
 * @author zhangke
 *
 */
public class ClaimLiabilityPaymentInfoDTO {
	/** 平台责任分类 **/
	private String liabilityType;
	/** 公司责任名称 **/
	private String liabilityName;
	/** 理赔意见 **/
	private String claimOpinion;
	/** 理赔结论代码 **/
	private String claimConclusionCode;
	/** 非正常赔付原因 **/
	private String conclusionReason;
	/** 责任赔付金额 **/
	private String paymentAmount;
	/** 是否手术 **/
	private String operation;
	/** 西医疾病 **/
	private List<ClaimWesternMedInfoDTO> westernMediList;
	/** 手术 **/
	private List<ClaimOperationInfoDTO> operationList;

	public String getLiabilityType() {
		return liabilityType;
	}

	public void setLiabilityType(String liabilityType) {
		this.liabilityType = liabilityType;
	}

	public String getLiabilityName() {
		return liabilityName;
	}

	public void setLiabilityName(String liabilityName) {
		this.liabilityName = liabilityName;
	}

	public String getClaimOpinion() {
		return claimOpinion;
	}

	public void setClaimOpinion(String claimOpinion) {
		this.claimOpinion = claimOpinion;
	}

	public String getClaimConclusionCode() {
		return claimConclusionCode;
	}

	public void setClaimConclusionCode(String claimConclusionCode) {
		this.claimConclusionCode = claimConclusionCode;
	}

	public String getConclusionReason() {
		return conclusionReason;
	}

	public void setConclusionReason(String conclusionReason) {
		this.conclusionReason = conclusionReason;
	}

	public String getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public List<ClaimWesternMedInfoDTO> getWesternMediList() {
		return westernMediList;
	}

	public void setWesternMediList(List<ClaimWesternMedInfoDTO> westernMediList) {
		this.westernMediList = westernMediList;
	}

	public List<ClaimOperationInfoDTO> getOperationList() {
		return operationList;
	}

	public void setOperationList(List<ClaimOperationInfoDTO> operationList) {
		this.operationList = operationList;
	}

}
