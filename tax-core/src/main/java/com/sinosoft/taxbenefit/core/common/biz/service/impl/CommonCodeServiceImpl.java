package com.sinosoft.taxbenefit.core.common.biz.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.platform.common.config.SystemConstants;
import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.platform.common.util.DateUtil;
import com.sinosoft.platform.common.util.SecurityUtil;
import com.sinosoft.taxbenefit.api.config.ENUM_BZ_RESPONSE_RESULT;
import com.sinosoft.taxbenefit.api.config.ENUM_SENDSERVICE_CODE;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.core.common.biz.mapper.CommonMapper;
import com.sinosoft.taxbenefit.core.common.biz.service.CommonCodeService;
import com.sinosoft.taxbenefit.core.common.config.ENUM_COMMONCODE;
import com.sinosoft.taxbenefit.core.common.config.ENUM_PORT_STATE;
import com.sinosoft.taxbenefit.core.common.config.ENUM_RC_STATE;
import com.sinosoft.taxbenefit.core.common.dto.EbaoServerPortDTO;
import com.sinosoft.taxbenefit.core.common.dto.ReqBusinessDTO;
import com.sinosoft.taxbenefit.core.common.dto.ReqBusinessDetailDTO;
import com.sinosoft.taxbenefit.core.common.dto.SendLogDTO;
import com.sinosoft.taxbenefit.core.common.dto.ThirdSendDTO;
import com.sinosoft.taxbenefit.core.ebaowebservice.RequestBody;
import com.sinosoft.taxbenefit.core.ebaowebservice.RequestHeader;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxCodeMappingMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxCommonCodeMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxRequestBusinessCollateMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxRequestBusinessDetailMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxRequestEbaoInfoMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxRequestSendLogMapper;
import com.sinosoft.taxbenefit.core.generated.model.TaxCommonCode;
import com.sinosoft.taxbenefit.core.generated.model.TaxCommonCodeExample;
import com.sinosoft.taxbenefit.core.generated.model.TaxRequestBusinessCollate;
import com.sinosoft.taxbenefit.core.generated.model.TaxRequestBusinessDetail;
import com.sinosoft.taxbenefit.core.generated.model.TaxRequestEbaoInfo;
import com.sinosoft.taxbenefit.core.generated.model.TaxRequestEbaoInfoExample;
import com.sinosoft.taxbenefit.core.generated.model.TaxRequestSendLogWithBLOBs;


@Service
public class CommonCodeServiceImpl implements CommonCodeService {
	@Autowired
	TaxCommonCodeMapper taxCommonCodeMapper;
	@Autowired
	TaxCodeMappingMapper taxCodeMappingMapper;
	@Autowired
	TaxRequestSendLogMapper taxRequestSendLogMapper;
	@Autowired
	TaxRequestEbaoInfoMapper taxRequestEbaoInfoMapper;
	@Autowired
	CommonMapper commonMapper;
	@Autowired
	TaxRequestBusinessCollateMapper taxRequestBusinessCollateMapper;
	@Autowired
	TaxRequestBusinessDetailMapper taxRequestBusinessDetailMapper;
	
	@Override
	public String queryCommonCodeInfo(String sysCode, String codeType) {
		TaxCommonCodeExample example = new TaxCommonCodeExample();
		example.createCriteria().andCodeTypeEqualTo(codeType)
				.andSysCodeEqualTo(sysCode);
		List<TaxCommonCode> list = taxCommonCodeMapper.selectByExample(example);
		String codeInfo = null;
		if (list!=null&&list.size() > 0) {
			codeInfo = list.get(0).getContent();
		}
		return codeInfo;
	}

	@Override
	public ThirdSendDTO createThirdSend(String transCode, ParamHeadDTO paramHead, Object body) {
		ThirdSendDTO thirdSend = new ThirdSendDTO();
		RequestHeader requestHeader = new RequestHeader();
		String username = this.queryCommonCodeInfo(ENUM_COMMONCODE.USERNAME.code(),ENUM_COMMONCODE.USERNAME.type());
		String password = this.queryCommonCodeInfo(ENUM_COMMONCODE.PASSWORD.code(),ENUM_COMMONCODE.PASSWORD.type());
		String version = this.queryCommonCodeInfo(ENUM_COMMONCODE.VERSION.code(), ENUM_COMMONCODE.VERSION.type());
		String bodyJson = JSONObject.toJSONString(body);
		String generateKey = SecurityUtil.generateMD5(bodyJson);
		
		requestHeader.setUserName(username);
		requestHeader.setPassword(password);
		requestHeader.setVersion(version);
		requestHeader.setTransType(transCode);
		requestHeader.setTransDate(DateUtil.getDateStr(DateUtil.getCurrentDate(), "yyyy-MM-dd hh:mm:ss"));
		requestHeader.setTransNo(paramHead.getSerialNo());
		requestHeader.setAreaCode(paramHead.getAreaCode());
		requestHeader.setRecordNum(Integer.parseInt(paramHead.getRecordNum()));
		requestHeader.setSignature(generateKey);
		RequestBody requestBody = new RequestBody();
		requestBody.setJsonString(bodyJson);
		
		thirdSend.setBody(requestBody);
		thirdSend.setHead(requestHeader);
		
		return thirdSend;
	}
	
	@Override
	public void addLogSend(SendLogDTO sendLog) {
		TaxRequestSendLogWithBLOBs taxRequestSendLog=new TaxRequestSendLogWithBLOBs();
		taxRequestSendLog.setCreateDate(DateUtil.getCurrentDate());
		taxRequestSendLog.setDescription(sendLog.getDescription());
		taxRequestSendLog.setPortCode(sendLog.getPortCode());
		taxRequestSendLog.setResponseContent(sendLog.getResponseContent());
		taxRequestSendLog.setSendContent(sendLog.getRequestContent());
		taxRequestSendLog.setSendTime(DateUtil.getCurrentDate());
		taxRequestSendLog.setSerialNo(sendLog.getSerialNo());
		taxRequestSendLog.setState(sendLog.getState());
		taxRequestSendLog.setTargetCode(ENUM_SENDSERVICE_CODE.EBAO.code());
		taxRequestSendLogMapper.insert(taxRequestSendLog);
//		SendLogThread sendLogThread=new SendLogThread(requestSendLogMapper, sendLogDTO);
//		threadPoolTaskExecutor.execute(sendLogThread);
	}

	@Override
	public EbaoServerPortDTO queryEBaoServerInfoByPortCode(String portCode) {
		EbaoServerPortDTO ebaoServerPort = new EbaoServerPortDTO();
		TaxRequestEbaoInfoExample example = new TaxRequestEbaoInfoExample();
		example.createCriteria().andPortCodeEqualTo(portCode);
		List<TaxRequestEbaoInfo> taxRequestEbaoList = taxRequestEbaoInfoMapper.selectByExample(example);
		if (taxRequestEbaoList.size() > 0) {
			TaxRequestEbaoInfo taxRequestEbaoInfo = taxRequestEbaoList.get(0);
			ebaoServerPort.setPortCode(taxRequestEbaoInfo.getPortCode());
			ebaoServerPort.setPortName(taxRequestEbaoInfo.getPortName());
			ebaoServerPort.setPortUrl(taxRequestEbaoInfo.getPortUrl());
			return ebaoServerPort;
		} else {
			throw new BusinessDataErrException("中保信接口请求路径信息不存在");
		}
	}

	@Override
	public List<EbaoServerPortDTO> queryEBaoServerInfoAll() {
		List<EbaoServerPortDTO> ebaoServerPortList = new ArrayList<EbaoServerPortDTO>();
		TaxRequestEbaoInfoExample example = new TaxRequestEbaoInfoExample();
		example.createCriteria().andPortStateEqualTo(ENUM_PORT_STATE.EMPLOY.code());
		List<TaxRequestEbaoInfo> taxRequestEbaoList = taxRequestEbaoInfoMapper.selectByExample(example);
		for (TaxRequestEbaoInfo taxRequestEbaoInfo : taxRequestEbaoList) {
			EbaoServerPortDTO ebaoServerPort = new EbaoServerPortDTO();
			ebaoServerPort.setPortCode(taxRequestEbaoInfo.getPortCode());
			ebaoServerPort.setPortName(taxRequestEbaoInfo.getPortName());
			ebaoServerPort.setPortUrl(taxRequestEbaoInfo.getPortUrl());
			ebaoServerPortList.add(ebaoServerPort);
		}
		return ebaoServerPortList;
	}
	
	@Override
	public void addReqBusinessCollate(ReqBusinessDTO reqBusiness) {
		Integer num = commonMapper.selectBusinessNum();
		if(num > 0){
			// 增加交易请求中记录数
			TaxRequestBusinessCollate businessCollate = new TaxRequestBusinessCollate();
			ReqBusinessDTO businessDTO = commonMapper.selectBusinessCollate();
			businessCollate.setSid(businessDTO.getBusid());
			businessCollate.setRequetNum(businessDTO.getRequetNum()+reqBusiness.getRequetNum());
			businessCollate.setReqSuccessNum(businessDTO.getReqSuccessNum()+reqBusiness.getReqSuccessNum());
			businessCollate.setReqFailNum(businessDTO.getReqFailNum()+reqBusiness.getReqFailNum());
			businessCollate.setUpdateTime(DateUtil.getCurrentDate());
			businessCollate.setUpdateId(SystemConstants.SYS_OPERATOR);
			taxRequestBusinessCollateMapper.updateByPrimaryKeySelective(businessCollate);
		}else{
			// 当前请求为当天的第一笔交易请求
			TaxRequestBusinessCollate businessCollate = new TaxRequestBusinessCollate();
			businessCollate.setBusinessDate(DateUtil.parseDate(DateUtil.getCurrentDate("yyyy-MM-dd"), "yyyy-MM-dd"));
			businessCollate.setRequetNum(reqBusiness.getRequetNum());
			businessCollate.setReqSuccessNum(reqBusiness.getReqSuccessNum());
			businessCollate.setReqFailNum(reqBusiness.getReqFailNum());
			businessCollate.setSuccessFlag(ENUM_RC_STATE.UNTREATED.getCode());
			businessCollate.setCreateTime(DateUtil.getCurrentDate());
			businessCollate.setCreateId(SystemConstants.SYS_OPERATOR);
			businessCollate.setUpdateTime(DateUtil.getCurrentDate());
			businessCollate.setUpdateId(SystemConstants.SYS_OPERATOR);
			taxRequestBusinessCollateMapper.insertSelective(businessCollate);
		}
	}

	@Override
	public ReqBusinessDTO initReqBusinessDTO() {
		ReqBusinessDTO businessDTO = new ReqBusinessDTO();
		businessDTO.setRequetNum(0);
		businessDTO.setReqSuccessNum(0);
		businessDTO.setReqFailNum(0);
		return businessDTO;
	}

	@Override
	public ReqBusinessDetailDTO initReqBusinessDetailDTO(ParamHeadDTO paramHead) {
		ReqBusinessDetailDTO businessDetailDTO = new ReqBusinessDetailDTO();
		businessDetailDTO.setPortCode(paramHead.getPortCode());
		businessDetailDTO.setSerialNo(paramHead.getSerialNo());
		return businessDetailDTO;
	}
	
	@Override
	public void addReqBusinessDetail(List<ReqBusinessDetailDTO> reqBusinessDetail) {
		for (ReqBusinessDetailDTO reqBusinessDetailDTO : reqBusinessDetail) {
			TaxRequestBusinessDetail businessDetail = new TaxRequestBusinessDetail();
			businessDetail.setSerialNo(reqBusinessDetailDTO.getSerialNo());
			businessDetail.setPortCode(reqBusinessDetailDTO.getPortCode());
			businessDetail.setResultCode(reqBusinessDetailDTO.getResultCode());
			businessDetail.setResultMessage(reqBusinessDetailDTO.getResultMessage());
			businessDetail.setBusinessDate(DateUtil.parseDate(DateUtil.getCurrentDate("yyyy-MM-dd"), "yyyy-MM-dd"));
			taxRequestBusinessDetailMapper.insertSelective(businessDetail);
		}
	}

	@Override
	public ThirdSendResponseDTO initResponseJSONLoading() {
		ThirdSendResponseDTO thirdSendResponse = new ThirdSendResponseDTO();
		thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
		thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
		thirdSendResponse.setResJson(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
		return thirdSendResponse;
	}
}
