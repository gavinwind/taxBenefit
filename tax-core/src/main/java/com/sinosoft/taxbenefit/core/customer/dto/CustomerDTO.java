package com.sinosoft.taxbenefit.core.customer.dto;

import com.sinosoft.taxbenefit.api.base.EBResultCodeDTO;
/**
 * 客户验证上传返回对象信息
 * @date 2016年3月6日 下午1:17:08
 */
public class CustomerDTO {
	private String busiNo;
	//平台客户编码
	private String customerNo;
	//税优保单存在标志
	private String taxDiscountedExistIndi;
	
	private EBResultCodeDTO result;
	
	public EBResultCodeDTO getResult() {
		return result;
	}
	public void setResult(EBResultCodeDTO result) {
		this.result = result;
	}
	public String getBusiNo() {
		return busiNo;
	}
	public void setBusiNo(String busiNo) {
		this.busiNo = busiNo;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public String getTaxDiscountedExistIndi() {
		return taxDiscountedExistIndi;
	}
	public void setTaxDiscountedExistIndi(String taxDiscountedExistIndi) {
		this.taxDiscountedExistIndi = taxDiscountedExistIndi;
	}
}