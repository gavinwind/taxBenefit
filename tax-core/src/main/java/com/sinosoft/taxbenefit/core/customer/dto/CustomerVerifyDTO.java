package com.sinosoft.taxbenefit.core.customer.dto;
/**
 *
 * title： 客户验证请求信息
 * @author zhangyu
 * @date 2016年3月6日 上午11:59:26
 */
public class CustomerVerifyDTO {
	private String busiNo;
	private String name;
	private String gender;
	private String birthday;
	private String certiType;
	private String certiNo;
	public String getBusiNo() {
		return busiNo;
	}
	public void setBusiNo(String busiNo) {
		this.busiNo = busiNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getCertiType() {
		return certiType;
	}
	public void setCertiType(String certiType) {
		this.certiType = certiType;
	}
	public String getCertiNo() {
		return certiNo;
	}
	public void setCertiNo(String certiNo) {
		this.certiNo = certiNo;
	}
}