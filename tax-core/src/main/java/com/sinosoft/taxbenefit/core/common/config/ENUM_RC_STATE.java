package com.sinosoft.taxbenefit.core.common.config;

public enum ENUM_RC_STATE {
	EFFECTIVE("E", "有效"), INVALID("D", "无效"), UNTREATED("N", "未处理"), SUCCESS("Y","处理成功"), FAIL("F","处理失败");
	/** 枚举code */
	private String code;
	/** 枚举value或者code说明 */
	private String value;

	ENUM_RC_STATE(String code, String value) {
		this.code = code;
		this.value = value;
	}

	public String getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}

	/**
	 * 根据key获得value
	 * 
	 * @param key
	 * @return
	 */
	public static String getEnumValueByKey(String key) {
		for (ENUM_RC_STATE enumItem : ENUM_RC_STATE.values()) {
			if (key.equals(enumItem.getCode())) {
				return enumItem.getValue();
			}
		}
		return "";
	}
}
