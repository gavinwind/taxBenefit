package com.sinosoft.taxbenefit.core.securityguard.biz.service;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.securityguard.ReqKeepBodyDTO;
import com.sinosoft.taxbenefit.core.generated.model.TaxFaultTask;

public interface PreservationOfChangeService {

	/***
	 * 保全接口
	 */
	public ThirdSendResponseDTO modifySecurityGuard(ReqKeepBodyDTO body,ParamHeadDTO paramHeadDTO);

	/**
	 * 保全异常任务批处理接口
	 * @param reqJSON
	 * @param portCode
	 * @return
	 */
	public ThirdSendResponseDTO modifySecurityExceptionJob(String reqJSON, String portCode, TaxFaultTask faultTask);
}
