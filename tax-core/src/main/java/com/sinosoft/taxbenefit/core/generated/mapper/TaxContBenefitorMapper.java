package com.sinosoft.taxbenefit.core.generated.mapper;

import com.sinosoft.taxbenefit.core.generated.model.TaxContBenefitor;
import com.sinosoft.taxbenefit.core.generated.model.TaxContBenefitorExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TaxContBenefitorMapper {

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_BENEFITOR
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	int countByExample(TaxContBenefitorExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_BENEFITOR
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	int deleteByExample(TaxContBenefitorExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_BENEFITOR
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	int deleteByPrimaryKey(Integer sid);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_BENEFITOR
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	int insert(TaxContBenefitor record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_BENEFITOR
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	int insertSelective(TaxContBenefitor record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_BENEFITOR
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	List<TaxContBenefitor> selectByExample(TaxContBenefitorExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_BENEFITOR
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	TaxContBenefitor selectByPrimaryKey(Integer sid);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_BENEFITOR
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	int updateByExampleSelective(@Param("record") TaxContBenefitor record,
			@Param("example") TaxContBenefitorExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_BENEFITOR
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	int updateByExample(@Param("record") TaxContBenefitor record,
			@Param("example") TaxContBenefitorExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_BENEFITOR
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	int updateByPrimaryKeySelective(TaxContBenefitor record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_BENEFITOR
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	int updateByPrimaryKey(TaxContBenefitor record);
}