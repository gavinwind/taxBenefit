package com.sinosoft.taxbenefit.core.generated.model;

import java.util.Date;

public class TaxContTrack {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TAX_CONT_TRACK.SID
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    private Integer sid;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TAX_CONT_TRACK.CONT_ID
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    private Integer contId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TAX_CONT_TRACK.CONT_NO
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    private String contNo;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TAX_CONT_TRACK.TRACK_TYPE
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    private String trackType;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TAX_CONT_TRACK.RISK_CODE
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    private String riskCode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TAX_CONT_TRACK.TRACK_ID
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    private Integer trackId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TAX_CONT_TRACK.TRACK_NAME
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    private String trackName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TAX_CONT_TRACK.TRACK_CODE
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    private String trackCode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TAX_CONT_TRACK.TRACK_BEFORE_VALUE
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    private String trackBeforeValue;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TAX_CONT_TRACK.TRACK_AFTER_VALUE
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    private String trackAfterValue;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TAX_CONT_TRACK.RC_STATE
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    private String rcState;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TAX_CONT_TRACK.CREATE_DATE
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    private Date createDate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TAX_CONT_TRACK.CREATOR_ID
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    private Integer creatorId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TAX_CONT_TRACK.MODIFY_DATE
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    private Date modifyDate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TAX_CONT_TRACK.MODIFIER_ID
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    private Integer modifierId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TAX_CONT_TRACK.REMARK
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    private String remark;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TAX_CONT_TRACK.SID
     *
     * @return the value of TAX_CONT_TRACK.SID
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public Integer getSid() {
        return sid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TAX_CONT_TRACK.SID
     *
     * @param sid the value for TAX_CONT_TRACK.SID
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public void setSid(Integer sid) {
        this.sid = sid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TAX_CONT_TRACK.CONT_ID
     *
     * @return the value of TAX_CONT_TRACK.CONT_ID
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public Integer getContId() {
        return contId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TAX_CONT_TRACK.CONT_ID
     *
     * @param contId the value for TAX_CONT_TRACK.CONT_ID
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public void setContId(Integer contId) {
        this.contId = contId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TAX_CONT_TRACK.CONT_NO
     *
     * @return the value of TAX_CONT_TRACK.CONT_NO
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public String getContNo() {
        return contNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TAX_CONT_TRACK.CONT_NO
     *
     * @param contNo the value for TAX_CONT_TRACK.CONT_NO
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TAX_CONT_TRACK.TRACK_TYPE
     *
     * @return the value of TAX_CONT_TRACK.TRACK_TYPE
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public String getTrackType() {
        return trackType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TAX_CONT_TRACK.TRACK_TYPE
     *
     * @param trackType the value for TAX_CONT_TRACK.TRACK_TYPE
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public void setTrackType(String trackType) {
        this.trackType = trackType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TAX_CONT_TRACK.RISK_CODE
     *
     * @return the value of TAX_CONT_TRACK.RISK_CODE
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public String getRiskCode() {
        return riskCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TAX_CONT_TRACK.RISK_CODE
     *
     * @param riskCode the value for TAX_CONT_TRACK.RISK_CODE
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public void setRiskCode(String riskCode) {
        this.riskCode = riskCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TAX_CONT_TRACK.TRACK_ID
     *
     * @return the value of TAX_CONT_TRACK.TRACK_ID
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public Integer getTrackId() {
        return trackId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TAX_CONT_TRACK.TRACK_ID
     *
     * @param trackId the value for TAX_CONT_TRACK.TRACK_ID
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public void setTrackId(Integer trackId) {
        this.trackId = trackId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TAX_CONT_TRACK.TRACK_NAME
     *
     * @return the value of TAX_CONT_TRACK.TRACK_NAME
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public String getTrackName() {
        return trackName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TAX_CONT_TRACK.TRACK_NAME
     *
     * @param trackName the value for TAX_CONT_TRACK.TRACK_NAME
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TAX_CONT_TRACK.TRACK_CODE
     *
     * @return the value of TAX_CONT_TRACK.TRACK_CODE
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public String getTrackCode() {
        return trackCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TAX_CONT_TRACK.TRACK_CODE
     *
     * @param trackCode the value for TAX_CONT_TRACK.TRACK_CODE
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public void setTrackCode(String trackCode) {
        this.trackCode = trackCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TAX_CONT_TRACK.TRACK_BEFORE_VALUE
     *
     * @return the value of TAX_CONT_TRACK.TRACK_BEFORE_VALUE
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public String getTrackBeforeValue() {
        return trackBeforeValue;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TAX_CONT_TRACK.TRACK_BEFORE_VALUE
     *
     * @param trackBeforeValue the value for TAX_CONT_TRACK.TRACK_BEFORE_VALUE
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public void setTrackBeforeValue(String trackBeforeValue) {
        this.trackBeforeValue = trackBeforeValue;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TAX_CONT_TRACK.TRACK_AFTER_VALUE
     *
     * @return the value of TAX_CONT_TRACK.TRACK_AFTER_VALUE
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public String getTrackAfterValue() {
        return trackAfterValue;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TAX_CONT_TRACK.TRACK_AFTER_VALUE
     *
     * @param trackAfterValue the value for TAX_CONT_TRACK.TRACK_AFTER_VALUE
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public void setTrackAfterValue(String trackAfterValue) {
        this.trackAfterValue = trackAfterValue;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TAX_CONT_TRACK.RC_STATE
     *
     * @return the value of TAX_CONT_TRACK.RC_STATE
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public String getRcState() {
        return rcState;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TAX_CONT_TRACK.RC_STATE
     *
     * @param rcState the value for TAX_CONT_TRACK.RC_STATE
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public void setRcState(String rcState) {
        this.rcState = rcState;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TAX_CONT_TRACK.CREATE_DATE
     *
     * @return the value of TAX_CONT_TRACK.CREATE_DATE
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TAX_CONT_TRACK.CREATE_DATE
     *
     * @param createDate the value for TAX_CONT_TRACK.CREATE_DATE
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TAX_CONT_TRACK.CREATOR_ID
     *
     * @return the value of TAX_CONT_TRACK.CREATOR_ID
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public Integer getCreatorId() {
        return creatorId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TAX_CONT_TRACK.CREATOR_ID
     *
     * @param creatorId the value for TAX_CONT_TRACK.CREATOR_ID
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public void setCreatorId(Integer creatorId) {
        this.creatorId = creatorId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TAX_CONT_TRACK.MODIFY_DATE
     *
     * @return the value of TAX_CONT_TRACK.MODIFY_DATE
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public Date getModifyDate() {
        return modifyDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TAX_CONT_TRACK.MODIFY_DATE
     *
     * @param modifyDate the value for TAX_CONT_TRACK.MODIFY_DATE
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TAX_CONT_TRACK.MODIFIER_ID
     *
     * @return the value of TAX_CONT_TRACK.MODIFIER_ID
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public Integer getModifierId() {
        return modifierId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TAX_CONT_TRACK.MODIFIER_ID
     *
     * @param modifierId the value for TAX_CONT_TRACK.MODIFIER_ID
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public void setModifierId(Integer modifierId) {
        this.modifierId = modifierId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TAX_CONT_TRACK.REMARK
     *
     * @return the value of TAX_CONT_TRACK.REMARK
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public String getRemark() {
        return remark;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TAX_CONT_TRACK.REMARK
     *
     * @param remark the value for TAX_CONT_TRACK.REMARK
     *
     * @mbggenerated Tue Mar 22 10:43:44 CST 2016
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }
}