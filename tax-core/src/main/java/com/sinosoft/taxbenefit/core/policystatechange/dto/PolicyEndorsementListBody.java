package com.sinosoft.taxbenefit.core.policystatechange.dto;

import java.util.List;

public class PolicyEndorsementListBody {

	private List<PolicyEndorsement> endorsementList;

	public List<PolicyEndorsement> getEndorsementList() {
		return endorsementList;
	}
	public void setEndorsementList(List<PolicyEndorsement> endorsementList) {
		this.endorsementList = endorsementList;
	}

	
}
