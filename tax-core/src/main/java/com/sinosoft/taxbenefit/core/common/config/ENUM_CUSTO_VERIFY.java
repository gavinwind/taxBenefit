package com.sinosoft.taxbenefit.core.common.config;

public enum ENUM_CUSTO_VERIFY {
	
	ENUM_CUST_VERIFY("PTY001","客户身份验证上传 ");
	
	private final String code;
	private final String desc;

	ENUM_CUSTO_VERIFY(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public String code() {
		return code;
	}

	public String desc() {
		return desc;
	}
}
