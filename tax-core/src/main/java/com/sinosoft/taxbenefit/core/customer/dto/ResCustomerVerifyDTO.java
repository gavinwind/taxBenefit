package com.sinosoft.taxbenefit.core.customer.dto;

import java.util.List;
/**
 * 中保信返回客户验证对象
 * @author zhangyu
 * @date 2016年3月10日 上午9:41:59
 */
public class ResCustomerVerifyDTO {
	
	private List<CustomerDTO> clientList;
	
	public List<CustomerDTO> getClientList() {
		return clientList;
	}
	public void setClientList(List<CustomerDTO> clientList) {
		this.clientList = clientList;
	}
}