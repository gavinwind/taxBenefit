package com.sinosoft.taxbenefit.core.securityguard.dto;

/**
 * 中报信DtoBody
 * @author gyas-itkezh
 *
 */

public class EndorsementBody {

	
	private EndorsementInfoDTO endorsement;

	public EndorsementInfoDTO getEndorsement() {
		return endorsement;
	}

	public void setEndorsement(EndorsementInfoDTO endorsement) {
		this.endorsement = endorsement;
	}
	
}
