package com.sinosoft.taxbenefit.core.claim.dto;



/**
 * 西医疾病
 * 
 * @author zhangke
 *
 */
public class ClaimWesternMedInfoDTO {
	/** 医疗机构代码 **/
	private String hospitalCode;
	/** 医疗机构名称 **/
	private String hospitalName;
	/** 西医疾病代码 **/
	private String westernMediCode;

	public String getHospitalCode() {
		return hospitalCode;
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

	public String getHospitalName() {
		return hospitalName;
	}

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}

	public String getWesternMediCode() {
		return westernMediCode;
	}

	public void setWesternMediCode(String westernMediCode) {
		this.westernMediCode = westernMediCode;
	}
	
}
