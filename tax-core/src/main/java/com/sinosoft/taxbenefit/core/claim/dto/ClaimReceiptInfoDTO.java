package com.sinosoft.taxbenefit.core.claim.dto;

import java.util.List;

/**
 * 收据信息
 * 
 * @author zhangke
 *
 */
public class ClaimReceiptInfoDTO  {
	/** 收据编号 **/
	private String receiptNo;
	/** 收据类型 **/
	private String receiptType;
	/** 医疗机构代码 **/
	private String hospitalCode;
	/** 医疗机构名称 **/
	private String hospitalName;
	/** 发生日期 **/
	private String date;
	/** 发生金额 **/
	private String amount;
	/** 入院日期 **/
	private String hospitalDate;
	/** 出院日期 **/
	private String dischargeDate;
	/** 住院天数 **/
	private String hospitalStay;
	/** 住院科室 **/
	private String hospitalDept;
	/** 分割单出具单位 **/
	private String issueUnit;
	/** 关联赔案号 **/
	private String relatedClaimNo;
	/** 收据分项费用 **/
	private List<ClaimReceiptFeeInfoDTO> receiptFeeList;
	/**医疗费用**/
	private List<ClaimItemInfoDTO>itemList;
	
	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public String getReceiptType() {
		return receiptType;
	}

	public void setReceiptType(String receiptType) {
		this.receiptType = receiptType;
	}

	public String getHospitalCode() {
		return hospitalCode;
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

	public String getHospitalName() {
		return hospitalName;
	}

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String  date) {
		this.date = date;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getHospitalDate() {
		return hospitalDate;
	}

	public void setHospitalDate(String  hospitalDate) {
		this.hospitalDate = hospitalDate;
	}

	public String getDischargeDate() {
		return dischargeDate;
	}

	public void setDischargeDate(String  dischargeDate) {
		this.dischargeDate = dischargeDate;
	}

	public String getHospitalStay() {
		return hospitalStay;
	}

	public void setHospitalStay(String hospitalStay) {
		this.hospitalStay = hospitalStay;
	}

	public String getHospitalDept() {
		return hospitalDept;
	}

	public void setHospitalDept(String hospitalDept) {
		this.hospitalDept = hospitalDept;
	}

	public String getIssueUnit() {
		return issueUnit;
	}

	public void setIssueUnit(String issueUnit) {
		this.issueUnit = issueUnit;
	}

	public String getRelatedClaimNo() {
		return relatedClaimNo;
	}

	public void setRelatedClaimNo(String relatedClaimNo) {
		this.relatedClaimNo = relatedClaimNo;
	}

	public List<ClaimReceiptFeeInfoDTO> getReceiptFeeList() {
		return receiptFeeList;
	}

	public void setReceiptFeeList(List<ClaimReceiptFeeInfoDTO> receiptFeeList) {
		this.receiptFeeList = receiptFeeList;
	}

	public List<ClaimItemInfoDTO> getItemList() {
		return itemList;
	}

	public void setItemList(List<ClaimItemInfoDTO> itemList) {
		this.itemList = itemList;
	}

}
