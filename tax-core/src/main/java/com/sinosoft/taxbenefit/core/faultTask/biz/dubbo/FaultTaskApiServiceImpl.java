package com.sinosoft.taxbenefit.core.faultTask.biz.dubbo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.taxbenefit.api.dto.common.FaultTaskDTO;
import com.sinosoft.taxbenefit.api.dto.common.TaxFaultTaskInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.TaxFaulTaskDTO;
import com.sinosoft.taxbenefit.api.inter.TaxFaultTaskApiService;
import com.sinosoft.taxbenefit.core.faultTask.biz.service.FaultTaskService;

/**
 * 任务日志Api实现类
 * @author zhangke
 *
 */
@Service("faultTaskApiServiceImpl")
public class FaultTaskApiServiceImpl extends TaxBaseService implements
		TaxFaultTaskApiService {
	@Autowired
	FaultTaskService faultTaskService;

	@Override
	public void addTaxFaultTask(TaxFaulTaskDTO taxFaulTaskDTO) {
		faultTaskService.addTaxFaultTask(taxFaulTaskDTO);
	}


	@Override
	public FaultTaskDTO queryTaxFaultTaskBySerialNo(String serialNo) {
		return faultTaskService.queryTaxFaultTaskBySerialNo(serialNo);
	}



}
