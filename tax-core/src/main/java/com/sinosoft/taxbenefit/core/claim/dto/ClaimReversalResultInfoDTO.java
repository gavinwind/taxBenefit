package com.sinosoft.taxbenefit.core.claim.dto;

import com.sinosoft.taxbenefit.api.base.EBResultCodeDTO;
/**
 * 理赔撤销中保信返回信息DTO
 * @author zhangke
 *
 */
public class ClaimReversalResultInfoDTO {
	/** 理赔赔案号 */
	private String claimNo;
	/** 返回结果信息 */
	private EBResultCodeDTO result;
	
	public String getClaimNo() {
		return claimNo;
	}
	public void setClaimNo(String claimNo) {
		this.claimNo = claimNo;
	}
	public EBResultCodeDTO getResult() {
		return result;
	}
	public void setResult(EBResultCodeDTO result) {
		this.result = result;
	}
}
