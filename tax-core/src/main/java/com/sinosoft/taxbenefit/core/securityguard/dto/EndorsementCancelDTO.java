package com.sinosoft.taxbenefit.core.securityguard.dto;

/**
 * 请求中报信保全撤销DTO
 * 
 * @author zhangke
 *
 */
public class EndorsementCancelDTO {
	private EndorsementCancelInfoDTO endorsementCancel;

	public EndorsementCancelInfoDTO getEndorsementCancel() {
		return endorsementCancel;
	}

	public void setEndorsementCancel(EndorsementCancelInfoDTO endorsementCancel) {
		this.endorsementCancel = endorsementCancel;
	}
}
