package com.sinosoft.taxbenefit.core.taxbenvrify.biz.dubbo;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqTaxBeneVerifyInfoDTO;
import com.sinosoft.taxbenefit.api.inter.TaxBeneVerifyApiService;
import com.sinosoft.taxbenefit.core.taxbenvrify.biz.service.TaxBeneVerifyService;
/**
 * 税优验证Dubbo服务
 * @author zhangyu
 * @date 2016年3月10日 上午9:47:08
 */
@Service("taxBeneVerifyApiServiceImpl")
public class TaxBeneVerifyApiServiceImpl extends TaxBaseService implements TaxBeneVerifyApiService{
    @Autowired
	TaxBeneVerifyService taxBeneVerifyService;
	@Override
	public ThirdSendResponseDTO taxBenefitService(
			ReqTaxBeneVerifyInfoDTO reqTaxBenefitInfoDTO,ParamHeadDTO paramHeadDTO) {
				return taxBeneVerifyService.addtaxBeneVerify(reqTaxBenefitInfoDTO,paramHeadDTO);
	}
}