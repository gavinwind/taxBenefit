package com.sinosoft.taxbenefit.core.claim.dto;
/**
 * 理赔撤销 请求中保信赔案信息DTO
 * @author zhangke
 *
 */
public class ClaimReversalInfoDTO {
	/** 理赔赔案号 **/
	private String claimNo;
	/** 撤销日期 */
	private String cancelDate;
	/** 撤销原因 */
	private String cancellationReason;

	public String getClaimNo() {
		return claimNo;
	}

	public void setClaimNo(String claimNo) {
		this.claimNo = claimNo;
	}

	public String getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(String cancelDate) {
		this.cancelDate = cancelDate;
	}

	public String getCancellationReason() {
		return cancellationReason;
	}

	public void setCancellationReason(String cancellationReason) {
		this.cancellationReason = cancellationReason;
	}

}
