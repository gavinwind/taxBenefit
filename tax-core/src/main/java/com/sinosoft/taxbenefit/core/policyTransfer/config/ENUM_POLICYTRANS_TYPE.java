/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.ewealth.product.config<br/>
 * @FileName: ENUM_PRODUCT_SUBTYPE.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.taxbenefit.core.policyTransfer.config;

/**
 * 保单转移(转出/转入)类型
 * @author SheChunMing
 */
public enum ENUM_POLICYTRANS_TYPE {
	POLICYTRANS_OUT("01","转出"), POLICYTRANS_IN("02","转入");
	
	private final String code;
	private final String desc;

	ENUM_POLICYTRANS_TYPE(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}
	public String code() {
		return code;
	}
	public String desc() {
		return desc;
	}
}


