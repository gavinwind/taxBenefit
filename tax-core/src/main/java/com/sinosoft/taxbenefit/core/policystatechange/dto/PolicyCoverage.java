package com.sinosoft.taxbenefit.core.policystatechange.dto;

import java.util.List;

public class PolicyCoverage {

	/** 产品组代码 **/
	private String coveragePackageCode;
	/** 险种代码 **/
	private String comCoverageCode;
	/** 险种名称 **/
	private String comCoverageName;
	/** 主附险标志 **/
	private String coverageType;
	/** 险种终止日期 **/
	private String terminationDate;
	/** 险种中止日期 **/
	private String suspendDate;
	/** 险种状态 **/
	private String coverageStatus;
	/** 险种终止原因 **/
	private String terminationReason;
	
	private List<PolicyLiability>  liabilityList;
	
	
	
	
	
	public List<PolicyLiability> getLiabilityList() {
		return liabilityList;
	}
	public void setLiabilityList(List<PolicyLiability> liabilityList) {
		this.liabilityList = liabilityList;
	}
	public String getCoveragePackageCode() {
		return coveragePackageCode;
	}
	public void setCoveragePackageCode(String coveragePackageCode) {
		this.coveragePackageCode = coveragePackageCode;
	}
	public String getComCoverageCode() {
		return comCoverageCode;
	}
	public void setComCoverageCode(String comCoverageCode) {
		this.comCoverageCode = comCoverageCode;
	}
	public String getComCoverageName() {
		return comCoverageName;
	}
	public void setComCoverageName(String comCoverageName) {
		this.comCoverageName = comCoverageName;
	}


public String getCoverageType() {
		return coverageType;
	}
	public void setCoverageType(String coverageType) {
		this.coverageType = coverageType;
	}
public String getTerminationDate() {
		return terminationDate;
	}
	public void setTerminationDate(String terminationDate) {
		this.terminationDate = terminationDate;
	}
	public String getSuspendDate() {
		return suspendDate;
	}
	public void setSuspendDate(String suspendDate) {
		this.suspendDate = suspendDate;
	}
	public String getCoverageStatus() {
		return coverageStatus;
	}
	public void setCoverageStatus(String coverageStatus) {
		this.coverageStatus = coverageStatus;
	}
	public String getTerminationReason() {
		return terminationReason;
	}
	public void setTerminationReason(String terminationReason) {
		this.terminationReason = terminationReason;
	}

}
