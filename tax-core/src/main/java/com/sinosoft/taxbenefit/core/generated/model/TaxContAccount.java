package com.sinosoft.taxbenefit.core.generated.model;

import java.math.BigDecimal;
import java.util.Date;

public class TaxContAccount {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_CONT_ACCOUNT.SID
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	private Integer sid;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_CONT_ACCOUNT.CONT_ID
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	private Integer contId;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_CONT_ACCOUNT.CONT_NO
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	private String contNo;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_CONT_ACCOUNT.SEQUENCE_NO
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	private String sequenceNo;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_CONT_ACCOUNT.RISK_CODE
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	private String riskCode;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_CONT_ACCOUNT.RISK_NAME
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	private String riskName;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_CONT_ACCOUNT.COVERAGE_PACKAGE_CODE
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	private String coveragePackageCode;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_CONT_ACCOUNT.SUM_AMOUNT
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	private BigDecimal sumAmount;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_CONT_ACCOUNT.SUM_INTEREST
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	private BigDecimal sumInterest;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_CONT_ACCOUNT.RC_STATE
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	private String rcState;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_CONT_ACCOUNT.CREATE_DATE
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	private Date createDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_CONT_ACCOUNT.CREATOR_ID
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	private Integer creatorId;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_CONT_ACCOUNT.MODIFY_DATE
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	private Date modifyDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column TAX_CONT_ACCOUNT.MODIFIER_ID
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	private Integer modifierId;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_CONT_ACCOUNT.SID
	 * @return  the value of TAX_CONT_ACCOUNT.SID
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public Integer getSid() {
		return sid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_CONT_ACCOUNT.SID
	 * @param sid  the value for TAX_CONT_ACCOUNT.SID
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public void setSid(Integer sid) {
		this.sid = sid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_CONT_ACCOUNT.CONT_ID
	 * @return  the value of TAX_CONT_ACCOUNT.CONT_ID
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public Integer getContId() {
		return contId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_CONT_ACCOUNT.CONT_ID
	 * @param contId  the value for TAX_CONT_ACCOUNT.CONT_ID
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public void setContId(Integer contId) {
		this.contId = contId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_CONT_ACCOUNT.CONT_NO
	 * @return  the value of TAX_CONT_ACCOUNT.CONT_NO
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public String getContNo() {
		return contNo;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_CONT_ACCOUNT.CONT_NO
	 * @param contNo  the value for TAX_CONT_ACCOUNT.CONT_NO
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_CONT_ACCOUNT.SEQUENCE_NO
	 * @return  the value of TAX_CONT_ACCOUNT.SEQUENCE_NO
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public String getSequenceNo() {
		return sequenceNo;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_CONT_ACCOUNT.SEQUENCE_NO
	 * @param sequenceNo  the value for TAX_CONT_ACCOUNT.SEQUENCE_NO
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public void setSequenceNo(String sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_CONT_ACCOUNT.RISK_CODE
	 * @return  the value of TAX_CONT_ACCOUNT.RISK_CODE
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public String getRiskCode() {
		return riskCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_CONT_ACCOUNT.RISK_CODE
	 * @param riskCode  the value for TAX_CONT_ACCOUNT.RISK_CODE
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_CONT_ACCOUNT.RISK_NAME
	 * @return  the value of TAX_CONT_ACCOUNT.RISK_NAME
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public String getRiskName() {
		return riskName;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_CONT_ACCOUNT.RISK_NAME
	 * @param riskName  the value for TAX_CONT_ACCOUNT.RISK_NAME
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public void setRiskName(String riskName) {
		this.riskName = riskName;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_CONT_ACCOUNT.COVERAGE_PACKAGE_CODE
	 * @return  the value of TAX_CONT_ACCOUNT.COVERAGE_PACKAGE_CODE
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public String getCoveragePackageCode() {
		return coveragePackageCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_CONT_ACCOUNT.COVERAGE_PACKAGE_CODE
	 * @param coveragePackageCode  the value for TAX_CONT_ACCOUNT.COVERAGE_PACKAGE_CODE
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public void setCoveragePackageCode(String coveragePackageCode) {
		this.coveragePackageCode = coveragePackageCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_CONT_ACCOUNT.SUM_AMOUNT
	 * @return  the value of TAX_CONT_ACCOUNT.SUM_AMOUNT
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public BigDecimal getSumAmount() {
		return sumAmount;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_CONT_ACCOUNT.SUM_AMOUNT
	 * @param sumAmount  the value for TAX_CONT_ACCOUNT.SUM_AMOUNT
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public void setSumAmount(BigDecimal sumAmount) {
		this.sumAmount = sumAmount;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_CONT_ACCOUNT.SUM_INTEREST
	 * @return  the value of TAX_CONT_ACCOUNT.SUM_INTEREST
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public BigDecimal getSumInterest() {
		return sumInterest;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_CONT_ACCOUNT.SUM_INTEREST
	 * @param sumInterest  the value for TAX_CONT_ACCOUNT.SUM_INTEREST
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public void setSumInterest(BigDecimal sumInterest) {
		this.sumInterest = sumInterest;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_CONT_ACCOUNT.RC_STATE
	 * @return  the value of TAX_CONT_ACCOUNT.RC_STATE
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public String getRcState() {
		return rcState;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_CONT_ACCOUNT.RC_STATE
	 * @param rcState  the value for TAX_CONT_ACCOUNT.RC_STATE
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public void setRcState(String rcState) {
		this.rcState = rcState;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_CONT_ACCOUNT.CREATE_DATE
	 * @return  the value of TAX_CONT_ACCOUNT.CREATE_DATE
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_CONT_ACCOUNT.CREATE_DATE
	 * @param createDate  the value for TAX_CONT_ACCOUNT.CREATE_DATE
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_CONT_ACCOUNT.CREATOR_ID
	 * @return  the value of TAX_CONT_ACCOUNT.CREATOR_ID
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public Integer getCreatorId() {
		return creatorId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_CONT_ACCOUNT.CREATOR_ID
	 * @param creatorId  the value for TAX_CONT_ACCOUNT.CREATOR_ID
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public void setCreatorId(Integer creatorId) {
		this.creatorId = creatorId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_CONT_ACCOUNT.MODIFY_DATE
	 * @return  the value of TAX_CONT_ACCOUNT.MODIFY_DATE
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public Date getModifyDate() {
		return modifyDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_CONT_ACCOUNT.MODIFY_DATE
	 * @param modifyDate  the value for TAX_CONT_ACCOUNT.MODIFY_DATE
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column TAX_CONT_ACCOUNT.MODIFIER_ID
	 * @return  the value of TAX_CONT_ACCOUNT.MODIFIER_ID
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public Integer getModifierId() {
		return modifierId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column TAX_CONT_ACCOUNT.MODIFIER_ID
	 * @param modifierId  the value for TAX_CONT_ACCOUNT.MODIFIER_ID
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public void setModifierId(Integer modifierId) {
		this.modifierId = modifierId;
	}
}