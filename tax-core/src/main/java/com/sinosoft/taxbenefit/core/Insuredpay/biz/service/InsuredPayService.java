package com.sinosoft.taxbenefit.core.Insuredpay.biz.service;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqInsuredPayInfoDTO;
/**
 * 被保人既往理賠額查詢
 * @author zhangyu
 * @date 2016年3月4日 上午10:05:15
 */
public interface InsuredPayService {
	/**
	 * 添加被保人
	 * @param reqInsuredPayInfoDTO
	 */
	ThirdSendResponseDTO addInsuredPayInfo(ReqInsuredPayInfoDTO reqInsuredPayInfoDTO,ParamHeadDTO paramHeadDTO);
}