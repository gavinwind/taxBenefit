package com.sinosoft.taxbenefit.core.applicant.dto;

import com.sinosoft.taxbenefit.api.base.EBResultCodeDTO;
/**
 * 承保撤销上传中保信返回信息
 * @author zhangke
 *
 */
public class PolicyReversalResultInfoDTO {
	/** 保单号 */
	private String policyNo;
	/** 返回结果信息 */
	private EBResultCodeDTO result;

	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public EBResultCodeDTO getResult() {
		return result;
	}

	public void setResult(EBResultCodeDTO result) {
		this.result = result;
	}
}
