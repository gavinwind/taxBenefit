package com.sinosoft.taxbenefit.core.securityguard.biz.dubbo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.securityguard.ReqEndorsementCancelInfoDTO;
import com.sinosoft.taxbenefit.api.inter.PreservationOfCancelApiService;
import com.sinosoft.taxbenefit.core.securityguard.biz.service.PreservationOfCancelService;

@Service("preservationOfCancelApiServiceImpl")
public class PreservationOfCancelApiServiceImpl extends TaxBaseService implements PreservationOfCancelApiService {

	@Autowired
	private PreservationOfCancelService preservationOfCancelService;
	
	@Override
	public ThirdSendResponseDTO PreservationOfChange(
			ReqEndorsementCancelInfoDTO body, ParamHeadDTO paramHeadDTO) {
		logger.info("保全撤销转入后置处理");
		return preservationOfCancelService.modifyPreservationCancel(body, paramHeadDTO);
	}

}
