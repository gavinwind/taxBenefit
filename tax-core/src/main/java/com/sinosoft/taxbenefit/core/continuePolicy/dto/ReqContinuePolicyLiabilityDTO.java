package com.sinosoft.taxbenefit.core.continuePolicy.dto;

import java.math.BigDecimal;

/**
 * Title:中保信-责任信息
 * @author yangdongkai@outlook.com
 * @date 2016年3月2日 上午10:10:14
 */

public class ReqContinuePolicyLiabilityDTO {
	// 公司责任代码
	private String liabilityCode;
	// 责任保额
	private BigDecimal liabilitySa;
	// 责任状态
	private String liabilityStatus;
	/**
	 * @return the liabilityCode
	 */
	public String getLiabilityCode() {
		return liabilityCode;
	}
	/**
	 * @param liabilityCode the liabilityCode to set
	 */
	public void setLiabilityCode(String liabilityCode) {
		this.liabilityCode = liabilityCode;
	}
	/**
	 * @return the liabilitySa
	 */
	public BigDecimal getLiabilitySa() {
		return liabilitySa;
	}
	/**
	 * @param liabilitySa the liabilitySa to set
	 */
	public void setLiabilitySa(BigDecimal liabilitySa) {
		this.liabilitySa = liabilitySa;
	}
	/**
	 * @return the liabilityStatus
	 */
	public String getLiabilityStatus() {
		return liabilityStatus;
	}
	/**
	 * @param liabilityStatus the liabilityStatus to set
	 */
	public void setLiabilityStatus(String liabilityStatus) {
		this.liabilityStatus = liabilityStatus;
	}
}
