package com.sinosoft.taxbenefit.core.common.dto;

/**
 * 交易结果明细
 * @author SheChunMing
 */
public class ReqBusinessDetailDTO {
	// 请求流水号
	private String serialNo;
	// 接口编码
	private String portCode;
	// 结果编码
	private String resultCode;
	// 结果说明
	private String resultMessage;
	// 交易时间
	private String disposeDete;

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getPortCode() {
		return portCode;
	}

	public void setPortCode(String portCode) {
		this.portCode = portCode;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getResultMessage() {
		return resultMessage;
	}

	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}

	public String getDisposeDete() {
		return disposeDete;
	}

	public void setDisposeDete(String disposeDete) {
		this.disposeDete = disposeDete;
	}
	
}
