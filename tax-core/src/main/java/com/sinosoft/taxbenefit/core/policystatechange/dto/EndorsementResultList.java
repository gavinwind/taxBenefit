package com.sinosoft.taxbenefit.core.policystatechange.dto;

import java.util.List;
/**
 * 中保信返回保单状态修改Dto
 * @author zhanghao
 *
 */
public class EndorsementResultList {

	private List<EndorsementResult> endorsementResultList;

	public List<EndorsementResult> getEndorsementResultList() {
		return endorsementResultList;
	}

	public void setEndorsementResultList(
			List<EndorsementResult> endorsementResultList) {
		this.endorsementResultList = endorsementResultList;
	}

}
