package com.sinosoft.taxbenefit.core.applicant.dto;

import java.math.BigDecimal;



/**
 * 责任信息
 * 
 * @author zhangke
 *
 */
public class AppLiabilityDTO  {
	
	/** 公司责任代码 **/
	private String liabilityCode;
	/** 责任保额 **/
	private BigDecimal liabilitySa;
	/** 责任状态 **/
	private String liabilityStatus;

	public String getLiabilityCode() {
		return liabilityCode;
	}

	public void setLiabilityCode(String liabilityCode) {
		this.liabilityCode = liabilityCode;
	}

	public BigDecimal getLiabilitySa() {
		return liabilitySa;
	}

	public void setLiabilitySa(BigDecimal liabilitySa) {
		this.liabilitySa = liabilitySa;
	}

	public String getLiabilityStatus() {
		return liabilityStatus;
	}

	public void setLiabilityStatus(String liabilityStatus) {
		this.liabilityStatus = liabilityStatus;
	}


}
