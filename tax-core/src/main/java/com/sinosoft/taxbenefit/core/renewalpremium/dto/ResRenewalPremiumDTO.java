package com.sinosoft.taxbenefit.core.renewalpremium.dto;

import java.util.List;

/**
 * 中保信-续期信息返回DTO解析
 * Title:ResRenewalPremiumDTO
 * @author yangdongkai@outlook.com
 * @date 2016年3月8日 下午3:34:41
 */
public class ResRenewalPremiumDTO {
	//续期保费结果
	private List<ResPremiumResultDTO> premiumResultList;
	/**
	 * @return the premiumResultList
	 */
	public List<ResPremiumResultDTO> getPremiumResultList() {
		return premiumResultList;
	}
	/**
	 * @param premiumResultList the premiumResultList to set
	 */
	public void setPremiumResultList(List<ResPremiumResultDTO> premiumResultList) {
		this.premiumResultList = premiumResultList;
	}
	
}
