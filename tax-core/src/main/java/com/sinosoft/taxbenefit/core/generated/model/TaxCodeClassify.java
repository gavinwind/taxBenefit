package com.sinosoft.taxbenefit.core.generated.model;

public class TaxCodeClassify {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TAX_CODE_CLASSIFY.SID
     *
     * @mbggenerated Sat Mar 12 10:44:35 CST 2016
     */
    private Integer sid;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TAX_CODE_CLASSIFY.CODE_TYPE
     *
     * @mbggenerated Sat Mar 12 10:44:35 CST 2016
     */
    private String codeType;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TAX_CODE_CLASSIFY.TRANSCODE_FIELD
     *
     * @mbggenerated Sat Mar 12 10:44:35 CST 2016
     */
    private String transcodeField;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TAX_CODE_CLASSIFY.TRANSCODE_SOURCE
     *
     * @mbggenerated Sat Mar 12 10:44:35 CST 2016
     */
    private String transcodeSource;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TAX_CODE_CLASSIFY.REMARK
     *
     * @mbggenerated Sat Mar 12 10:44:35 CST 2016
     */
    private String remark;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TAX_CODE_CLASSIFY.SID
     *
     * @return the value of TAX_CODE_CLASSIFY.SID
     *
     * @mbggenerated Sat Mar 12 10:44:35 CST 2016
     */
    public Integer getSid() {
        return sid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TAX_CODE_CLASSIFY.SID
     *
     * @param sid the value for TAX_CODE_CLASSIFY.SID
     *
     * @mbggenerated Sat Mar 12 10:44:35 CST 2016
     */
    public void setSid(Integer sid) {
        this.sid = sid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TAX_CODE_CLASSIFY.CODE_TYPE
     *
     * @return the value of TAX_CODE_CLASSIFY.CODE_TYPE
     *
     * @mbggenerated Sat Mar 12 10:44:35 CST 2016
     */
    public String getCodeType() {
        return codeType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TAX_CODE_CLASSIFY.CODE_TYPE
     *
     * @param codeType the value for TAX_CODE_CLASSIFY.CODE_TYPE
     *
     * @mbggenerated Sat Mar 12 10:44:35 CST 2016
     */
    public void setCodeType(String codeType) {
        this.codeType = codeType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TAX_CODE_CLASSIFY.TRANSCODE_FIELD
     *
     * @return the value of TAX_CODE_CLASSIFY.TRANSCODE_FIELD
     *
     * @mbggenerated Sat Mar 12 10:44:35 CST 2016
     */
    public String getTranscodeField() {
        return transcodeField;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TAX_CODE_CLASSIFY.TRANSCODE_FIELD
     *
     * @param transcodeField the value for TAX_CODE_CLASSIFY.TRANSCODE_FIELD
     *
     * @mbggenerated Sat Mar 12 10:44:35 CST 2016
     */
    public void setTranscodeField(String transcodeField) {
        this.transcodeField = transcodeField;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TAX_CODE_CLASSIFY.TRANSCODE_SOURCE
     *
     * @return the value of TAX_CODE_CLASSIFY.TRANSCODE_SOURCE
     *
     * @mbggenerated Sat Mar 12 10:44:35 CST 2016
     */
    public String getTranscodeSource() {
        return transcodeSource;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TAX_CODE_CLASSIFY.TRANSCODE_SOURCE
     *
     * @param transcodeSource the value for TAX_CODE_CLASSIFY.TRANSCODE_SOURCE
     *
     * @mbggenerated Sat Mar 12 10:44:35 CST 2016
     */
    public void setTranscodeSource(String transcodeSource) {
        this.transcodeSource = transcodeSource;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TAX_CODE_CLASSIFY.REMARK
     *
     * @return the value of TAX_CODE_CLASSIFY.REMARK
     *
     * @mbggenerated Sat Mar 12 10:44:35 CST 2016
     */
    public String getRemark() {
        return remark;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TAX_CODE_CLASSIFY.REMARK
     *
     * @param remark the value for TAX_CODE_CLASSIFY.REMARK
     *
     * @mbggenerated Sat Mar 12 10:44:35 CST 2016
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }
}