/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.ewealth.general.config<br/>
 * @FileName: ENUM_REPORTLOG_BIZTYPE.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.taxbenefit.core.common.config;

/**
 * 中保信请求返回结果类型枚举类
 * @author SheChunMing
 * @date 2015年9月15日 下午11:21:14 
 * @version V1.0  
 */
public enum ENUM_RESULT_CODE {
	INPUT_ERROR("0", "输入数据有误，无法返回正常值"), SUCCESS("1","输入数据正确，返回正常值"),
	INPUT_LOCALITY_SUCCESS("2", "输入数据部分成功"), EXIST("3", "输入数据对象已存在"),
	REQ_AWAIT("4", "任务接收正常，已经排入队列"), EXECUTE_ING("5", "任务执行中"),
	SYSTEM_ERROR("E", "系统未知错误"), HEADER_ERROR("H", "报文头输入数据有误，无法进行交易处理");
	
	private final String code;
	private final String desc;

	ENUM_RESULT_CODE(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public String code() {
		return code;
	}

	public String desc() {
		return desc;
	}
}


