package com.sinosoft.taxbenefit.core.continuePolicy.dto;

import com.sinosoft.taxbenefit.api.base.EBResultCodeDTO;

/**
 * 续保信息上传请求中保信返回
 * Title:ResContinuePolicyListDTO
 * @author yangdongkai@outlook.com
 * @date 2016年3月12日 下午5:26:59
 */
public class ResContinuePolicyListDTO {
	//续保批单号
	private String renewalEndorsementNo;
	//续保确认编码
	private String renewalSequenceNo;
	//返回结果
	private EBResultCodeDTO result;
	/**
	 * @return the renewalEndorsementNo
	 */
	public String getRenewalEndorsementNo() {
		return renewalEndorsementNo;
	}
	/**
	 * @param renewalEndorsementNo the renewalEndorsementNo to set
	 */
	public void setRenewalEndorsementNo(String renewalEndorsementNo) {
		this.renewalEndorsementNo = renewalEndorsementNo;
	}
	/**
	 * @return the renewalSequenceNo
	 */
	public String getRenewalSequenceNo() {
		return renewalSequenceNo;
	}
	/**
	 * @param renewalSequenceNo the renewalSequenceNo to set
	 */
	public void setRenewalSequenceNo(String renewalSequenceNo) {
		this.renewalSequenceNo = renewalSequenceNo;
	}
	/**
	 * @return the result
	 */
	public EBResultCodeDTO getResult() {
		return result;
	}
	/**
	 * @param result the result to set
	 */
	public void setResult(EBResultCodeDTO result) {
		this.result = result;
	}
	
	
}
