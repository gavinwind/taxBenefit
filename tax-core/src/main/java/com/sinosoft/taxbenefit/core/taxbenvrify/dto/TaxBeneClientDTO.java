package com.sinosoft.taxbenefit.core.taxbenvrify.dto;
/**
 * 中保信税优验证请求对象
 * @author zhangyu
 * @date 2016年3月10日 上午9:43:19
 */
public class TaxBeneClientDTO {
	//姓名
    private String name;
    //性别
	private String gender;
	//生日
	private String  birthday;
	//证件类型
	private String certiType;
	//证件号码
	private String certiNo;
	//国籍
	private String nationality;
	//投保单号
	private String proposalNo;
	//保单投保来源
	private String policySource;
	//险种生效日期
	private String coverageEffectiveDate;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getCoverageEffectiveDate() {
		return coverageEffectiveDate;
	}
	public void setCoverageEffectiveDate(String coverageEffectiveDate) {
		this.coverageEffectiveDate = coverageEffectiveDate;
	}
	public String getCertiType() {
		return certiType;
	}
	public void setCertiType(String certiType) {
		this.certiType = certiType;
	}
	public String getCertiNo() {
		return certiNo;
	}
	public void setCertiNo(String certiNo) {
		this.certiNo = certiNo;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getProposalNo() {
		return proposalNo;
	}
	public void setProposalNo(String proposalNo) {
		this.proposalNo = proposalNo;
	}
	public String getPolicySource() {
		return policySource;
	}
	public void setPolicySource(String policySource) {
		this.policySource = policySource;
	}
}