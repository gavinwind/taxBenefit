package com.sinosoft.taxbenefit.core.continuePolicy.dto;

import java.util.List;
/**
 * 中保信-续保信息上传DTO
 * Title:ReqContinuePolicyListDTO
 * @author yangdongkai@outlook.com
 * @date 2016年3月10日 下午2:18:36
 */
public class ReqContinuePolicyListDTO{
	//保单信息
	private List<ReqContinuePolicyDTO> policyList;
	public List<ReqContinuePolicyDTO> getPolicyList() {
		return policyList;
	}
	public void setPolicyList(List<ReqContinuePolicyDTO> policyList) {
		this.policyList = policyList;
	}

}
