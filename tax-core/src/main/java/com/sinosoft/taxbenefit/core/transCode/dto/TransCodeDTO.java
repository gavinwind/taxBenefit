package com.sinosoft.taxbenefit.core.transCode.dto;

/**
 * 转码DTO
 * 
 * @author zhangke
 *
 */
public class TransCodeDTO {
	private String transcodeField;// 字段名
	private String originalCode;// 原始码
	private String manageCom;//归属机构

	public String getTranscodeField() {
		return transcodeField;
	}

	public void setTranscodeField(String transcodeField) {
		this.transcodeField = transcodeField;
	}

	public String getOriginalCode() {
		return originalCode;
	}

	public void setOriginalCode(String originalCode) {
		this.originalCode = originalCode;
	}

	public String getManageCom() {
		return manageCom;
	}

	public void setManageCom(String manageCom) {
		this.manageCom = manageCom;
	}

}
