package com.sinosoft.taxbenefit.core.common.config;
/**
 * 转码
 * @author zhangke
 *
 */
public enum ENUUM_CODE_MAPPING {
	MAINFLAG("mainFlag","主附险标记"),
	STATE("state","保单/险种状态"),
	LIABILITYSTATUS("liabilityStatus","责任状态"),
	TERMINATIONREASON("terminationReason","保单险种终止原因"),
	PAYMENTFREQUENCY("paymentFrequency","缴费频率"),
	GENDER("gender","性别"),
	CARDTYPE("cardType","身份证件类型代码"),
	CLAIMCONCLUSIONCODE("claimConclusionCode","理赔结论代码"),
	ACCIDENTREASON("accidentReason","人身险出险原因代码"),
	ITEMCATEGORY("itemCategory","人身保险理赔费用代码"),
	ENDORSEMENTTYPE("endorsementType","保全类型代码"),
	FLOWSTATE("flowState","业务流程状态"),
	RISKTYPE("riskType","险类代码"),
	LIABILITYTYPE("liabilityType","人身责任类代码"),
	EVENTTYPE("eventType","医疗事件类型代码"),
	JOBCODE("jobCode","职业代码"),
	ENDORSEMENTSTATUS("endorsementStatus","批单状态");
	/** 枚举code */
	private String code;
	/** 枚举value或者code说明 */
	private String value;
	ENUUM_CODE_MAPPING(String code, String value) {
		this.code = code;
		this.value = value;
	}

	/**
	 * 获得枚举code值
	 * 
	 * @Title: code
	 * @Description: TODO
	 * @return
	 */
	public String code() {
		return code;
	}

	/**
	 * 获得枚举value值
	 * 
	 * @Title: decription
	 * @Description: TODO
	 * @return
	 */
	public String description() {
		return value;
	}
}
