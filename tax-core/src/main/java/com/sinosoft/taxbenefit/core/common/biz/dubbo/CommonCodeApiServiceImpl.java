package com.sinosoft.taxbenefit.core.common.biz.dubbo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.taxbenefit.api.inter.CommonCodeApiService;
import com.sinosoft.taxbenefit.core.common.biz.service.CommonCodeService;

/**
 * 公用接口实现类
 * 
 * @author zhangke
 *
 */
@Service("commonCodeApiServiceImpl")
public class CommonCodeApiServiceImpl extends TaxBaseService implements
		CommonCodeApiService {
	@Autowired
	CommonCodeService commonCodeService;

	@Override
	public String queryCommonCodeInfo(String sysCode, String codeType) {
		return commonCodeService.queryCommonCodeInfo(sysCode, codeType);
	}

}
