package com.sinosoft.taxbenefit.core.continuePolicy.biz.service.impl;

import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.platform.common.config.SystemConstants;
import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.platform.common.util.DateUtil;
import com.sinosoft.platform.common.util.StringUtil;
import com.sinosoft.taxbenefit.api.base.EBResultCodeDTO;
import com.sinosoft.taxbenefit.api.base.HXResultCodeDTO;
import com.sinosoft.taxbenefit.api.config.ENUM_BZ_RESPONSE_RESULT;
import com.sinosoft.taxbenefit.api.config.ENUM_DISPOSE_TYPE;
import com.sinosoft.taxbenefit.api.config.ENUM_SENDSERVICE_CODE;
import com.sinosoft.taxbenefit.api.dto.request.TaxFaulTaskDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.CoreRequestHead;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyCoverageInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyCustomerInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyInfoCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyLiabilityInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqMContinuePolicyInfoCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqMContinuePolicyInfoDTO;
import com.sinosoft.taxbenefit.api.dto.response.continuepolicy.ResultContinuePolicyDTO;
import com.sinosoft.taxbenefit.core.common.biz.service.CommonCodeService;
import com.sinosoft.taxbenefit.core.common.config.ENUM_BUSINESS_RESULR;
import com.sinosoft.taxbenefit.core.common.config.ENUM_EBAO_SERVICE_PORT_CODE;
import com.sinosoft.taxbenefit.core.common.config.ENUM_HX_SERVICE_PORT_CODE;
import com.sinosoft.taxbenefit.core.common.config.ENUM_RC_STATE;
import com.sinosoft.taxbenefit.core.common.config.ENUM_RENEWAL_TYPE;
import com.sinosoft.taxbenefit.core.common.config.ENUM_RESULT_CODE;
import com.sinosoft.taxbenefit.core.common.config.ENUUM_CODE_MAPPING;
import com.sinosoft.taxbenefit.core.common.dto.BookSequenceDTO;
import com.sinosoft.taxbenefit.core.common.dto.EbaoServerPortDTO;
import com.sinosoft.taxbenefit.core.common.dto.ReqBusinessDTO;
import com.sinosoft.taxbenefit.core.common.dto.ReqBusinessDetailDTO;
import com.sinosoft.taxbenefit.core.common.dto.SendLogDTO;
import com.sinosoft.taxbenefit.core.common.dto.ThirdSendDTO;
import com.sinosoft.taxbenefit.core.continuePolicy.biz.service.ContinuePolicyCoreService;
import com.sinosoft.taxbenefit.core.continuePolicy.dto.ReqContinuePolicyCoverageDTO;
import com.sinosoft.taxbenefit.core.continuePolicy.dto.ReqContinuePolicyCustomerDTO;
import com.sinosoft.taxbenefit.core.continuePolicy.dto.ReqContinuePolicyDTO;
import com.sinosoft.taxbenefit.core.continuePolicy.dto.ReqContinuePolicyLiabilityDTO;
import com.sinosoft.taxbenefit.core.continuePolicy.dto.ReqContinuePolicyListDTO;
import com.sinosoft.taxbenefit.core.continuePolicy.dto.ReqContinuePolicyUwinfoDTO;
import com.sinosoft.taxbenefit.core.continuePolicy.dto.ResContinuePolicyDTO;
import com.sinosoft.taxbenefit.core.continuePolicy.dto.ResContinuePolicyListDTO;
import com.sinosoft.taxbenefit.core.ebaowebservice.AsynResultQueryService;
import com.sinosoft.taxbenefit.core.ebaowebservice.AsynResultQueryService_Service;
import com.sinosoft.taxbenefit.core.ebaowebservice.PolicyPremiumService;
import com.sinosoft.taxbenefit.core.ebaowebservice.PolicyPremiumService_Service;
import com.sinosoft.taxbenefit.core.ebaowebservice.PolicyRenewalService;
import com.sinosoft.taxbenefit.core.ebaowebservice.PolicyRenewalService_Service;
import com.sinosoft.taxbenefit.core.ebaowebservice.ResponseBody;
import com.sinosoft.taxbenefit.core.ebaowebservice.ResponseHeader;
import com.sinosoft.taxbenefit.core.ebaowebservice.ResponseJSON;
import com.sinosoft.taxbenefit.core.faultTask.biz.service.FaultTaskService;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxContRenewMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxContRenewRiskMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxFaultTaskMapper;
import com.sinosoft.taxbenefit.core.generated.model.TaxCont;
import com.sinosoft.taxbenefit.core.generated.model.TaxContRenew;
import com.sinosoft.taxbenefit.core.generated.model.TaxContRenewRisk;
import com.sinosoft.taxbenefit.core.generated.model.TaxFaultTask;
import com.sinosoft.taxbenefit.core.generated.model.TaxPolicyRisk;
import com.sinosoft.taxbenefit.core.policy.biz.service.PolicyService;
import com.sinosoft.taxbenefit.core.transCode.biz.service.TransCodeService;

@Service
public class ContinuePolicyCoreServiceImpl extends TaxBaseService implements ContinuePolicyCoreService {
	@Autowired
	TaxContRenewMapper taxContRenewMapper;
	@Autowired
	TaxContRenewRiskMapper taxContRenewRiskMapper;
	@Autowired
	TransCodeService transCodeService;
	@Autowired
	CommonCodeService commonCodeService;
	@Autowired
	FaultTaskService faultTaskService;
	@Autowired
	TaxFaultTaskMapper taxFaultTaskMapper;//异常任务
	@Autowired
	PolicyService policyService;
	@Override
	public ThirdSendResponseDTO continuePolicySettlement(ReqContinuePolicyInfoDTO reqContinuePolicyInfoDTO,ParamHeadDTO paramHead) {
		ThirdSendResponseDTO thirdSendResponse = new ThirdSendResponseDTO();
		List<ReqBusinessDetailDTO> reqBusinessDetailList = new ArrayList<ReqBusinessDetailDTO>();
		try {
			ReqBusinessDTO businessDTO = commonCodeService.initReqBusinessDTO();
			logger.info("单笔续保信息上传（核心请求）JSON 体数据内容："+JSONObject.toJSONString(reqContinuePolicyInfoDTO));
			// 1.将续保上传请求dto转换为中保信续保上传请求dto
			ReqContinuePolicyListDTO reqContinuePolicyListDTO = this.convertCarrier(reqContinuePolicyInfoDTO);
			// 2.调用中保信接口服务,获得返回结果 存储调用中保信日志表
			ThirdSendDTO thirdSendDTO = commonCodeService.createThirdSend(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_RENEWAL_UPLOAD.code(), paramHead,reqContinuePolicyListDTO);
			logger.info("单笔续保信息上传（请求中保信）JSON 数据内容："+JSONObject.toJSONString(thirdSendDTO));
			Holder<ResponseHeader> responseHeaderHolder = new Holder<ResponseHeader>();
			Holder<ResponseBody> responseBodyHolder = new Holder<ResponseBody>();
			EbaoServerPortDTO ebaoServerPort = commonCodeService.queryEBaoServerInfoByPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_RENEWAL_UPLOAD.code());
			String portUrl = ebaoServerPort.getPortUrl();
			URL wsdlLocation = new URL(portUrl);
			PolicyRenewalService_Service service = new PolicyRenewalService_Service(wsdlLocation);
			PolicyRenewalService servicePort = service.getPolicyRenewalServicePort();
			BindingProvider bp = (BindingProvider) servicePort;
			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, portUrl);
			servicePort.renewal(thirdSendDTO.getHead(), thirdSendDTO.getBody(),responseHeaderHolder, responseBodyHolder);
			ResponseHeader responseHeader = responseHeaderHolder.value;
			ResponseBody responseBody = responseBodyHolder.value;
			logger.info("单笔续保信息上传（中保信响应）JSON 数据内容："+responseBody.getJsonString());
			//封装中保信返回报文
			ResponseJSON responseJSON = new ResponseJSON();
			responseJSON.setResponseBody(responseBody);
			responseJSON.setResponseHeader(responseHeader);
			//封装发送报文日志对象
			SendLogDTO sendLog = new SendLogDTO();
			sendLog.setSerialNo(paramHead.getSerialNo());
			sendLog.setPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_RENEWAL_UPLOAD.code());
			sendLog.setRequestContent(JSONObject.toJSONString(thirdSendDTO));
			sendLog.setResponseContent(JSONObject.toJSONString(responseJSON));
			sendLog.setTargetCode(ENUM_SENDSERVICE_CODE.EBAO.code());
			sendLog.setState(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
			sendLog.setDescription(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
			commonCodeService.addLogSend(sendLog);
			// 3.封装返回核心dto;
			if (ENUM_RESULT_CODE.SUCCESS.code().equals(responseHeader.getResultCode())
					|| ENUM_RESULT_CODE.INPUT_LOCALITY_SUCCESS.code().equals(responseHeader.getResultCode())) {
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
				thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
				this.addContinuePolicy(reqContinuePolicyInfoDTO);
			}else{
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
				List list = responseHeader.getResultMessage().getMessage();
				if(list != null && list.size() > 0){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : responseHeader.getResultMessage().getMessage()){
						sbStr.append(mess).append("|");
					}
					String message = sbStr.substring(0, sbStr.length()-1).toString();
					thirdSendResponse.setResultDesc(message);
				}else{
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
				}
			}
			boolean isRegisterFaulTask = false;
			if(!StringUtil.isEmpty(responseBody.getJsonString())){
				ResContinuePolicyDTO resContinuePolicyDTO = JSONObject.parseObject(responseBody.getJsonString(), ResContinuePolicyDTO.class);
				if (null != resContinuePolicyDTO.getRenewalResultList()) {
					// 获取业务数据
					ResultContinuePolicyDTO resultContinuePolicyDTO = new ResultContinuePolicyDTO();
					resultContinuePolicyDTO.setBizNo(reqContinuePolicyInfoDTO.getBizNo());
					if(null != (resContinuePolicyDTO.getRenewalResultList())){
						String renewalSequenceNo = resContinuePolicyDTO.getRenewalResultList().get(0).getRenewalSequenceNo();
						resultContinuePolicyDTO.setRenewalSequenceNo(renewalSequenceNo);
					}
					EBResultCodeDTO eBResultCode = resContinuePolicyDTO.getRenewalResultList().get(0).getResult();
					HXResultCodeDTO hxResultCode = new HXResultCodeDTO();
					hxResultCode.setResultCode(eBResultCode.getResultCode());
					String message = "";
					if(null != eBResultCode.getResultMessage()){
						StringBuffer sbStr = new StringBuffer(); 
						for(String mess : eBResultCode.getResultMessage()){
							sbStr.append(mess).append("|");
						}
						message = sbStr.substring(0, sbStr.length()-1).toString();
					}
					hxResultCode.setResultMessage(message);
					resultContinuePolicyDTO.setResult(hxResultCode);
					thirdSendResponse.setResJson(resultContinuePolicyDTO);
					if(ENUM_RESULT_CODE.SUCCESS.code().equals(eBResultCode.getResultCode())){
						businessDTO.setReqSuccessNum(businessDTO.getReqSuccessNum() + 1);
					}else{
						businessDTO.setReqFailNum(businessDTO.getReqFailNum() + 1);
						ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
						reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
						reqBusinessDetailDTO.setResultMessage(message);
						reqBusinessDetailList.add(reqBusinessDetailDTO);
					}
					isRegisterFaulTask = true;
				}else{
					BookSequenceDTO bookSequence = JSONObject.parseObject(responseBody.getJsonString(), BookSequenceDTO.class);
					TaxFaulTaskDTO taxFaultTask = new TaxFaulTaskDTO();
					taxFaultTask.setSerialNo(paramHead.getSerialNo());
					taxFaultTask.setServerPortCode(paramHead.getPortCode());
					taxFaultTask.setServerPortName(paramHead.getPortName());
					taxFaultTask.setAreaCode(paramHead.getAreaCode());
					taxFaultTask.setDisposeType(ENUM_DISPOSE_TYPE.APPOINTMENT.code());
					taxFaultTask.setBookSequenceNo(bookSequence.getBookingSequenceNo());
					faultTaskService.addTaxFaultTask(taxFaultTask);
					thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
				}
			}else{
				businessDTO.setReqFailNum(responseHeader.getRecordNum());
				ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
				reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
				reqBusinessDetailDTO.setResultMessage(thirdSendResponse.getResultDesc());
				reqBusinessDetailList.add(reqBusinessDetailDTO);
			}
			if (isRegisterFaulTask) {
				commonCodeService.addReqBusinessDetail(reqBusinessDetailList);
				businessDTO.setRequetNum(responseHeader.getRecordNum());
				commonCodeService.addReqBusinessCollate(businessDTO);
			}
		} catch (Exception e) {
			logger.error("单笔续保信息上传后置处理发生异常!");
			e.printStackTrace();
			thirdSendResponse.setResJson(e.getMessage());
			thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
			thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
			// 存储任务表
			TaxFaulTaskDTO taxFaultTask = new TaxFaulTaskDTO();
			taxFaultTask.setSerialNo(paramHead.getSerialNo());
			taxFaultTask.setServerPortCode(paramHead.getPortCode());
			taxFaultTask.setServerPortName(paramHead.getPortName());
			taxFaultTask.setAreaCode(paramHead.getAreaCode());
			taxFaultTask.setDisposeType(ENUM_DISPOSE_TYPE.ERROR.code());
			faultTaskService.addTaxFaultTask(taxFaultTask);
		}
		logger.error("单笔续保信息上传后置处理结束!");
		return thirdSendResponse;
	}

	/**
	 * 核心DTO转中保信DTO(单笔)
	 * @param reqContinuePolicyInfoDTO
	 * @return
	 */		
	public ReqContinuePolicyListDTO convertCarrier(ReqContinuePolicyInfoDTO reqContinuePolicyInfoDTO) {
		// 续保信息 -- 保单信息
		ReqContinuePolicyListDTO reqContinuePolicyListDTO = new ReqContinuePolicyListDTO();
		ReqContinuePolicyDTO reqContinuePolicyDTO=new ReqContinuePolicyDTO();
		List<ReqContinuePolicyDTO> policyList=new ArrayList<ReqContinuePolicyDTO>();
		reqContinuePolicyDTO.setEffectiveDate(reqContinuePolicyInfoDTO.getEffectiveDate());
		reqContinuePolicyDTO.setExpireDate(reqContinuePolicyInfoDTO.getExpireDate());
		reqContinuePolicyDTO.setPolicyNo(reqContinuePolicyInfoDTO.getPolicyNo());
		reqContinuePolicyDTO.setRenewalEndorsementNo(reqContinuePolicyInfoDTO.getRenewalEndorsementNo());
		List<ReqContinuePolicyCustomerDTO> customerList = new ArrayList<ReqContinuePolicyCustomerDTO>();
		if(reqContinuePolicyInfoDTO.getCustomerList() != null){
			// 保单信息 -- 客户信息
			for (ReqContinuePolicyCustomerInfoDTO reqContinuePolicyCustomerInfoDTO : reqContinuePolicyInfoDTO.getCustomerList()) {
				ReqContinuePolicyCustomerDTO reqContinuePolicyCustomerDTO = new ReqContinuePolicyCustomerDTO();
				reqContinuePolicyCustomerDTO.setCustomerNo(reqContinuePolicyCustomerInfoDTO.getCustomerNo());
				reqContinuePolicyCustomerDTO.setSequenceNo(reqContinuePolicyCustomerInfoDTO.getSequenceNo());
				List<ReqContinuePolicyCoverageDTO> coverageInfoDTO = new ArrayList<ReqContinuePolicyCoverageDTO>();
				if(reqContinuePolicyCustomerInfoDTO.getCoverageList() != null ){
					// 客户信息 -- 险种信息
					for (ReqContinuePolicyCoverageInfoDTO reqContinuePolicyCoverageInfoDTO : reqContinuePolicyCustomerInfoDTO.getCoverageList()) {
						ReqContinuePolicyCoverageDTO reqContinuePolicyCoverageDTO = new ReqContinuePolicyCoverageDTO();
						reqContinuePolicyCoverageDTO.setComCoverageCode(reqContinuePolicyCoverageInfoDTO.getComCoverageCode());
						reqContinuePolicyCoverageDTO.setCoverageEffectiveDate(reqContinuePolicyCoverageInfoDTO.getCoverageEffectiveDate());
						reqContinuePolicyCoverageDTO.setCoverageExpireDate(reqContinuePolicyCoverageInfoDTO.getCoverageExpireDate());
						reqContinuePolicyCoverageDTO.setCoveragePackageCode(reqContinuePolicyCoverageInfoDTO.getCoveragePackageCode());
						reqContinuePolicyCoverageDTO.setPremium(reqContinuePolicyCoverageInfoDTO.getPremium());
						reqContinuePolicyCoverageDTO.setSa(reqContinuePolicyCoverageInfoDTO.getInsuranceCoverage());
						List<ReqContinuePolicyLiabilityDTO> liabilityList = new ArrayList<ReqContinuePolicyLiabilityDTO>();
						if(reqContinuePolicyCoverageInfoDTO.getLiabilityList() != null){
							// 险种信息 -- 责任信息
							for (ReqContinuePolicyLiabilityInfoDTO reqContinuePolicyLiabilityInfoDTO : reqContinuePolicyCoverageInfoDTO.getLiabilityList()) {
								ReqContinuePolicyLiabilityDTO reqContinuePolicyLiabilityDTO = new ReqContinuePolicyLiabilityDTO();
								reqContinuePolicyLiabilityDTO.setLiabilityCode(reqContinuePolicyLiabilityInfoDTO.getLiabilityCode());
								reqContinuePolicyLiabilityDTO.setLiabilitySa(reqContinuePolicyLiabilityInfoDTO.getLiabilitySa());
								reqContinuePolicyLiabilityDTO.setLiabilityStatus(transCodeService.transCode(ENUUM_CODE_MAPPING.STATE.code(),reqContinuePolicyLiabilityInfoDTO.getLiabilityStatus()));
								liabilityList.add(reqContinuePolicyLiabilityDTO);
							}
						}
						// 险种信息 -- 核保信息
						if (reqContinuePolicyCoverageInfoDTO.getUnderwriting() != null) {
							ReqContinuePolicyUwinfoDTO reqContinuePolicyUwinfoDTO = new ReqContinuePolicyUwinfoDTO();
							reqContinuePolicyUwinfoDTO.setUnderwritingDate(reqContinuePolicyCoverageInfoDTO.getUnderwriting().getUnderwritingDate());
							reqContinuePolicyUwinfoDTO.setUnderwritingDecision(reqContinuePolicyCoverageInfoDTO.getUnderwriting().getUnderwritingDecision());
							reqContinuePolicyUwinfoDTO.setUnderwritingDes(reqContinuePolicyCoverageInfoDTO.getUnderwriting().getUnderwritingDes());
							reqContinuePolicyCoverageDTO.setUnderwriting(reqContinuePolicyUwinfoDTO);
						}
						reqContinuePolicyCoverageDTO.setLiabilityList(liabilityList);
						coverageInfoDTO.add(reqContinuePolicyCoverageDTO);
					}
					reqContinuePolicyCustomerDTO.setCoverageList(coverageInfoDTO);
					customerList.add(reqContinuePolicyCustomerDTO);
				}
			}
		}
		reqContinuePolicyDTO.setCustomerList(customerList);
		policyList.add(reqContinuePolicyDTO);
		reqContinuePolicyListDTO.setPolicyList(policyList);
		return reqContinuePolicyListDTO;
	}
	
	@Override
	public ThirdSendResponseDTO continueMPolicySettlement(ReqMContinuePolicyInfoDTO reqMContinuePolicyInfoDTO,ParamHeadDTO paramHead) {
		ThirdSendResponseDTO thirdSendResponse = new ThirdSendResponseDTO();
		List<ReqBusinessDetailDTO> reqBusinessDetailList = new ArrayList<ReqBusinessDetailDTO>();
		try {
			ReqBusinessDTO businessDTO = commonCodeService.initReqBusinessDTO();
			logger.info("多笔续保信息上传（核心请求）JSON 体数据内容："+JSONObject.toJSONString(reqMContinuePolicyInfoDTO));
			// 1.将续保上传请求dto转换为中保信续保上传请求dto
			ReqContinuePolicyListDTO reqContinuePolicyListDTO = this.convertCarrier(reqMContinuePolicyInfoDTO);
			// 2.调用中保信接口服务,获得返回结果 存储调用中保信日志表
			ThirdSendDTO thirdSendDTO = commonCodeService.createThirdSend(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_RENEWAL_UPLOAD.code(), paramHead,reqContinuePolicyListDTO);
			logger.info("多笔续保信息上传（请求中保信）JSON 数据内容："+JSONObject.toJSONString(thirdSendDTO));
			Holder<ResponseHeader> responseHeaderHolder = new Holder<ResponseHeader>();
			Holder<ResponseBody> responseBodyHolder = new Holder<ResponseBody>();
			EbaoServerPortDTO ebaoServerPort = commonCodeService.queryEBaoServerInfoByPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_RENEWAL_UPLOAD.code());
			String portUrl = ebaoServerPort.getPortUrl();
			URL wsdlLocation = new URL(portUrl);
			PolicyRenewalService_Service service = new PolicyRenewalService_Service(wsdlLocation);
			PolicyRenewalService servicePort = service.getPolicyRenewalServicePort();
			BindingProvider bp = (BindingProvider) servicePort;
			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, portUrl);
			servicePort.renewal(thirdSendDTO.getHead(), thirdSendDTO.getBody(),responseHeaderHolder, responseBodyHolder);
			ResponseHeader responseHeader = responseHeaderHolder.value;
			ResponseBody responseBody = responseBodyHolder.value;
			logger.info("多笔续保信息上传（中保信响应）JSON 数据内容："+responseBody.getJsonString());
			//封装中保信返回报文
			ResponseJSON responseJSON = new ResponseJSON();
			responseJSON.setResponseBody(responseBody);
			responseJSON.setResponseHeader(responseHeader);
			//封装发送报文日志对象
			SendLogDTO sendLog = new SendLogDTO();
			sendLog.setSerialNo(paramHead.getSerialNo());
			sendLog.setPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_RENEWAL_UPLOAD.code());
			sendLog.setRequestContent(JSONObject.toJSONString(thirdSendDTO));
			sendLog.setResponseContent(JSONObject.toJSONString(responseJSON));
			sendLog.setTargetCode(ENUM_SENDSERVICE_CODE.EBAO.code());
			sendLog.setState(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
			sendLog.setDescription(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
			commonCodeService.addLogSend(sendLog);
			// 3.封装返回核心dto;
			if (ENUM_RESULT_CODE.SUCCESS.code().equals(responseHeader.getResultCode())
					|| ENUM_RESULT_CODE.INPUT_LOCALITY_SUCCESS.code().equals(responseHeader.getResultCode())) {
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
				thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
				ResContinuePolicyDTO resContinuePolicyDTO = JSONObject.parseObject(responseBody.getJsonString(), ResContinuePolicyDTO.class);
				List<ReqContinuePolicyInfoDTO> list = reqMContinuePolicyInfoDTO.getPolicyList() ;
				for (ResContinuePolicyListDTO resDto : resContinuePolicyDTO.getRenewalResultList()) {
					if(resDto.getResult().getResultCode().equals(ENUM_RESULT_CODE.SUCCESS.code())){
						for (ReqContinuePolicyInfoDTO reqContinuePolicyInfoDTO : list) {
							if(reqContinuePolicyInfoDTO.getRenewalEndorsementNo().equals(resDto.getRenewalEndorsementNo())){
								this.addContinuePolicy(reqContinuePolicyInfoDTO);
							}
						}
					}
					
				}
			}else{
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
				List list = responseHeader.getResultMessage().getMessage();
				if(list != null && list.size() > 0){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : responseHeader.getResultMessage().getMessage()){
						sbStr.append(mess).append("|");
					}
					String message = sbStr.substring(0, sbStr.length()-1).toString();
					thirdSendResponse.setResultDesc(message);
				}else{
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
				}
			}
			boolean isRegisterFaulTask = false;
			if(!StringUtil.isEmpty(responseBody.getJsonString())){
				ResContinuePolicyDTO resContinuePolicyDTO = JSONObject.parseObject(responseBody.getJsonString(), ResContinuePolicyDTO.class);
				if (null != resContinuePolicyDTO.getRenewalResultList()) {
					List<ResultContinuePolicyDTO> resultContinuePolicyList = new ArrayList<ResultContinuePolicyDTO>();
					// 成功记录数
					Integer successNum = 0;
					// 失败记录数
					Integer failNum = 0;
					// 获取业务数据
					if (reqMContinuePolicyInfoDTO.getPolicyList() != null) {
						for(ReqContinuePolicyInfoDTO reqContinuePolicyInfoDTO : reqMContinuePolicyInfoDTO.getPolicyList()){
							if (resContinuePolicyDTO.getRenewalResultList() != null) {
								for(ResContinuePolicyListDTO ResContinuePolicyListDTO : resContinuePolicyDTO.getRenewalResultList()){
									if (ResContinuePolicyListDTO.getRenewalEndorsementNo().equals(reqContinuePolicyInfoDTO.getRenewalEndorsementNo())) {
										ResultContinuePolicyDTO resultContinuePolicyDTO = new ResultContinuePolicyDTO();
										resultContinuePolicyDTO.setBizNo(reqContinuePolicyInfoDTO.getBizNo());
										resultContinuePolicyDTO.setRenewalSequenceNo(ResContinuePolicyListDTO.getRenewalSequenceNo());
										EBResultCodeDTO eBResultCode = ResContinuePolicyListDTO.getResult();
										HXResultCodeDTO hxResultCode = new HXResultCodeDTO();
										hxResultCode.setResultCode(eBResultCode.getResultCode());
										String message = "";
										if(null != eBResultCode.getResultMessage()){
											StringBuffer sbStr = new StringBuffer(); 
											for(String mess : eBResultCode.getResultMessage()){
												sbStr.append(mess).append("|");
											}
											message = sbStr.substring(0, sbStr.length()-1).toString();
										}
										hxResultCode.setResultMessage(message);
										resultContinuePolicyDTO.setResult(hxResultCode);
										resultContinuePolicyList.add(resultContinuePolicyDTO);
										//记录成功和失败记录数
										if(ENUM_RESULT_CODE.SUCCESS.code().equals(eBResultCode.getResultCode())){
											successNum+=1;
										}else{
											failNum+=1;
											ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
											reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
											reqBusinessDetailDTO.setResultMessage(message);
											reqBusinessDetailList.add(reqBusinessDetailDTO);
										}
									}
								}
							}
						}
						// 登记请求记录
						businessDTO.setReqSuccessNum(successNum);
						businessDTO.setReqFailNum(failNum);
						thirdSendResponse.setResJson(resultContinuePolicyList);
					}
					isRegisterFaulTask = true;
				}else{
					BookSequenceDTO bookSequence = JSONObject.parseObject(responseBody.getJsonString(), BookSequenceDTO.class);
					TaxFaulTaskDTO taxFaultTask = new TaxFaulTaskDTO();
					taxFaultTask.setSerialNo(paramHead.getSerialNo());
					taxFaultTask.setServerPortCode(paramHead.getPortCode());
					taxFaultTask.setServerPortName(paramHead.getPortName());
					taxFaultTask.setAreaCode(paramHead.getAreaCode());
					taxFaultTask.setDisposeType(ENUM_DISPOSE_TYPE.APPOINTMENT.code());
					taxFaultTask.setBookSequenceNo(bookSequence.getBookingSequenceNo());
					faultTaskService.addTaxFaultTask(taxFaultTask);
					thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
				}
			}else{
				businessDTO.setReqFailNum(responseHeader.getRecordNum());
				for (int i = 0; i < responseHeader.getRecordNum(); i++) {
					ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
					reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
					reqBusinessDetailDTO.setResultMessage(thirdSendResponse.getResultDesc());
					reqBusinessDetailList.add(reqBusinessDetailDTO);
				}
			}
			if (isRegisterFaulTask) {
				commonCodeService.addReqBusinessDetail(reqBusinessDetailList);
				businessDTO.setRequetNum(responseHeader.getRecordNum());
				commonCodeService.addReqBusinessCollate(businessDTO);	
			}
		} catch (Exception e) {
			logger.error("多笔续保信息上传后置处理发生异常!");
			e.printStackTrace();
			thirdSendResponse.setResJson(e.getMessage());
			thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
			thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
			// 存储任务表
			TaxFaulTaskDTO taxFaultTask = new TaxFaulTaskDTO();
			taxFaultTask.setSerialNo(paramHead.getSerialNo());
			taxFaultTask.setServerPortCode(paramHead.getPortCode());
			taxFaultTask.setServerPortName(paramHead.getPortName());
			taxFaultTask.setAreaCode(paramHead.getAreaCode());
			taxFaultTask.setDisposeType(ENUM_DISPOSE_TYPE.ERROR.code());
			faultTaskService.addTaxFaultTask(taxFaultTask);
		}
		logger.error("多笔续保信息上传后置处理结束!");
		return thirdSendResponse;
	}

	/**
	 * 续保信息上传数据持久化
	 * @param list
	 */
	private void addContinuePolicy(ReqContinuePolicyInfoDTO reqContinuePolicyInfoDTO) {
		try {
			String policyNo = reqContinuePolicyInfoDTO.getPolicyNo();
			TaxCont taxCont = policyService.queryPolicyByPolicyNo(policyNo);
			TaxContRenew contRenew = new TaxContRenew(); 
			if(taxCont!=null){
				taxCont.setValiDate(DateUtil.parseDate(reqContinuePolicyInfoDTO.getEffectiveDate()));
				taxCont.setExpireDate(DateUtil.parseDate(reqContinuePolicyInfoDTO.getExpireDate()));
				taxCont.setModifierId(SystemConstants.SYS_OPERATOR);
				taxCont.setModifyDate(DateUtil.getCurrentDate());
				policyService.modifyPolicy(taxCont);
				contRenew.setContId(taxCont.getSid());
			}
			// 储存续保信息表
			contRenew.setRenewType(ENUM_RENEWAL_TYPE.POLICY_PREMIUM.getCode());//添加续期续保类型
			contRenew.setBizNo(reqContinuePolicyInfoDTO.getBizNo());
			contRenew.setContNo(reqContinuePolicyInfoDTO.getPolicyNo());
			if (reqContinuePolicyInfoDTO.getCustomerList() != null) {
				contRenew.setSequenceNo(reqContinuePolicyInfoDTO.getCustomerList().get(0).getSequenceNo());
			}
			contRenew.setRenewEndorsementNo(reqContinuePolicyInfoDTO.getRenewalEndorsementNo());
			contRenew.setExpireDate(DateUtil.parseDate(reqContinuePolicyInfoDTO.getExpireDate()));
			// 添加操作人信息
			contRenew.setRcState(ENUM_RC_STATE.EFFECTIVE.getCode());
			contRenew.setCreateDate(DateUtil.getCurrentDate());
			contRenew.setCreatorId(SystemConstants.SYS_OPERATOR);
			contRenew.setModifierId(SystemConstants.SYS_OPERATOR);
			contRenew.setModifyDate(DateUtil.getCurrentDate());
			taxContRenewMapper.insert(contRenew);
			if(reqContinuePolicyInfoDTO.getCustomerList() != null){
				for (ReqContinuePolicyCustomerInfoDTO reqContinuePolicyCustomerInfoDTO : reqContinuePolicyInfoDTO.getCustomerList()) {
					if(reqContinuePolicyCustomerInfoDTO.getCoverageList() != null){
						for (ReqContinuePolicyCoverageInfoDTO reqContinuePolicyCoverageInfoDTO : reqContinuePolicyCustomerInfoDTO.getCoverageList()) {
							TaxPolicyRisk taxPolicyRisk = policyService.queryRiskInfo(reqContinuePolicyCoverageInfoDTO.getComCoverageCode(), taxCont.getSid());
							//更新保单险种表
							taxPolicyRisk.setExpiredDate(DateUtil.parseDate(reqContinuePolicyCoverageInfoDTO.getCoverageExpireDate()));
							taxPolicyRisk.setAnnualAmnt(reqContinuePolicyCoverageInfoDTO.getInsuranceCoverage());
							taxPolicyRisk.setRiskPrem(reqContinuePolicyCoverageInfoDTO.getPremium());
							taxPolicyRisk.setModifierId(SystemConstants.SYS_OPERATOR);
							taxPolicyRisk.setModifyDate(DateUtil.getCurrentDate());
							policyService.modifyCoverage(taxPolicyRisk);
							//储存续保险种表
							TaxContRenewRisk taxContRenewRisk = new TaxContRenewRisk();
							taxContRenewRisk.setRenewId(contRenew.getSid());
							taxContRenewRisk.setContId(contRenew.getContId());
							taxContRenewRisk.setContNo(contRenew.getContNo());
							taxContRenewRisk.setCoverageExpireDate(DateUtil.parseDate(reqContinuePolicyCoverageInfoDTO.getCoverageExpireDate()));
							taxContRenewRisk.setInsuranceCoverage(reqContinuePolicyCoverageInfoDTO.getInsuranceCoverage());
							taxContRenewRisk.setPremium(reqContinuePolicyCoverageInfoDTO.getPremium());
							taxContRenewRisk.setRcState(ENUM_RC_STATE.EFFECTIVE.getCode());
							taxContRenewRisk.setRiskCode(reqContinuePolicyCoverageInfoDTO.getComCoverageCode());
							taxContRenewRisk.setCreatorId(SystemConstants.SYS_OPERATOR);
							taxContRenewRisk.setCreateDate(DateUtil.getCurrentDate());
							taxContRenewRisk.setModifierId(SystemConstants.SYS_OPERATOR);
							taxContRenewRisk.setModifyDate(DateUtil.getCurrentDate());
							taxContRenewRiskMapper.insert(taxContRenewRisk);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("续保信息上传数据持久化失败"+e.getMessage());
		}
	}
	/**
	 * 核心DTO转中保信DTO（多笔）
	 * @param reqMContinuePolicyInfoDTO
	 * @return
	 */
	public ReqContinuePolicyListDTO convertCarrier(ReqMContinuePolicyInfoDTO reqMContinuePolicyInfoDTO){
		ReqContinuePolicyListDTO reqContinuePolicyListDTO = new ReqContinuePolicyListDTO();
		List<ReqContinuePolicyDTO> policyList = new ArrayList<ReqContinuePolicyDTO>();
		if (reqMContinuePolicyInfoDTO.getPolicyList() != null) {
			// 保单信息
			for (ReqContinuePolicyInfoDTO reqContinuePolicyInfoDTO : reqMContinuePolicyInfoDTO.getPolicyList()) {
				ReqContinuePolicyDTO ReqContinuePolicyDTO = new ReqContinuePolicyDTO();
				ReqContinuePolicyDTO.setEffectiveDate(reqContinuePolicyInfoDTO.getEffectiveDate());
				ReqContinuePolicyDTO.setExpireDate(reqContinuePolicyInfoDTO.getExpireDate());
				ReqContinuePolicyDTO.setPolicyNo(reqContinuePolicyInfoDTO.getPolicyNo());
				ReqContinuePolicyDTO.setRenewalEndorsementNo(reqContinuePolicyInfoDTO.getRenewalEndorsementNo());
				List<ReqContinuePolicyCustomerDTO> customerList = new ArrayList<ReqContinuePolicyCustomerDTO>();
				if(reqContinuePolicyInfoDTO.getCustomerList() != null){
					// 保单信息 -- 客户信息
					for (ReqContinuePolicyCustomerInfoDTO reqContinuePolicyCustomerInfoDTO : reqContinuePolicyInfoDTO.getCustomerList()) {
						ReqContinuePolicyCustomerDTO reqContinuePolicyCustomerDTO = new ReqContinuePolicyCustomerDTO();
						reqContinuePolicyCustomerDTO.setCustomerNo(reqContinuePolicyCustomerInfoDTO.getCustomerNo());
						reqContinuePolicyCustomerDTO.setSequenceNo(reqContinuePolicyCustomerInfoDTO.getSequenceNo());
						List<ReqContinuePolicyCoverageDTO> coverageList = new ArrayList<ReqContinuePolicyCoverageDTO>();
						if (reqContinuePolicyCustomerInfoDTO.getCoverageList() !=null) {
							// 客户信息 -- 险种信息
							for (ReqContinuePolicyCoverageInfoDTO reqContinuePolicyCoverageInfoDTO : reqContinuePolicyCustomerInfoDTO.getCoverageList()) {
								ReqContinuePolicyCoverageDTO reqContinuePolicyCoverageDTO = new ReqContinuePolicyCoverageDTO();
								reqContinuePolicyCoverageDTO.setComCoverageCode(reqContinuePolicyCoverageInfoDTO.getComCoverageCode());
								reqContinuePolicyCoverageDTO.setCoverageEffectiveDate(reqContinuePolicyCoverageInfoDTO.getCoverageEffectiveDate());
								reqContinuePolicyCoverageDTO.setCoverageExpireDate(reqContinuePolicyCoverageInfoDTO.getCoverageExpireDate());
								reqContinuePolicyCoverageDTO.setCoveragePackageCode(reqContinuePolicyCoverageInfoDTO.getCoveragePackageCode());
								reqContinuePolicyCoverageDTO.setPremium(reqContinuePolicyCoverageInfoDTO.getPremium());
								reqContinuePolicyCoverageDTO.setSa(reqContinuePolicyCoverageInfoDTO.getInsuranceCoverage());
								List<ReqContinuePolicyLiabilityDTO> liabilityList = new ArrayList<ReqContinuePolicyLiabilityDTO>();
								if (reqContinuePolicyCoverageInfoDTO.getLiabilityList() != null) {
									// 险种信息 -- 责任信息
									for (ReqContinuePolicyLiabilityInfoDTO reqContinuePolicyLiabilityInfoDTO : reqContinuePolicyCoverageInfoDTO.getLiabilityList()) {
										ReqContinuePolicyLiabilityDTO reqContinuePolicyLiabilityDTO = new ReqContinuePolicyLiabilityDTO();
										reqContinuePolicyLiabilityDTO.setLiabilityCode(reqContinuePolicyLiabilityInfoDTO.getLiabilityCode());
										reqContinuePolicyLiabilityDTO.setLiabilitySa(reqContinuePolicyLiabilityInfoDTO.getLiabilitySa());
										reqContinuePolicyLiabilityDTO.setLiabilityStatus(transCodeService.transCode(ENUUM_CODE_MAPPING.STATE.code(),reqContinuePolicyLiabilityInfoDTO.getLiabilityStatus()));
										liabilityList.add(reqContinuePolicyLiabilityDTO);
									}
								}
								ReqContinuePolicyUwinfoDTO ReqContinuePolicyUwinfoDTO = new ReqContinuePolicyUwinfoDTO();
								// 险种信息 -- 核保信息
								if (reqContinuePolicyCoverageInfoDTO.getUnderwriting() != null) {
									ReqContinuePolicyUwinfoDTO.setUnderwritingDate(reqContinuePolicyCoverageInfoDTO.getUnderwriting().getUnderwritingDate());
									ReqContinuePolicyUwinfoDTO.setUnderwritingDecision(reqContinuePolicyCoverageInfoDTO.getUnderwriting().getUnderwritingDecision());
									ReqContinuePolicyUwinfoDTO.setUnderwritingDes(reqContinuePolicyCoverageInfoDTO.getUnderwriting().getUnderwritingDes());
									reqContinuePolicyCoverageDTO.setUnderwriting(ReqContinuePolicyUwinfoDTO);
								}
								reqContinuePolicyCoverageDTO.setLiabilityList(liabilityList);
								coverageList.add(reqContinuePolicyCoverageDTO);
							}
						}
						reqContinuePolicyCustomerDTO.setCoverageList(coverageList);
						customerList.add(reqContinuePolicyCustomerDTO);
					}
				}
				ReqContinuePolicyDTO.setCustomerList(customerList);
				policyList.add(ReqContinuePolicyDTO);
			}
		}
		reqContinuePolicyListDTO.setPolicyList(policyList);
		return reqContinuePolicyListDTO;
	}

	@Override
	public TaxFaultTask continuePolicyExceptionJob(String reqJSON, String portCode, Integer faultTaskId) {
		logger.info("进入续保信息上传异常任务服务");
		ThirdSendResponseDTO thirdSendResponse = new ThirdSendResponseDTO();
		List<ReqBusinessDetailDTO> reqBusinessDetailList = new ArrayList<ReqBusinessDetailDTO>();
		TaxFaultTask faultTask = taxFaultTaskMapper.selectByPrimaryKey(faultTaskId);
		try {
			ReqBusinessDTO businessDTO = commonCodeService.initReqBusinessDTO();
			ParamHeadDTO paramHead = new ParamHeadDTO();
			CoreRequestHead coreRequestHead = new CoreRequestHead();
			List<ReqContinuePolicyInfoDTO> continuePolicyList = new ArrayList<ReqContinuePolicyInfoDTO>();
			//1. 解析请求报文
			//单笔请求
			if (ENUM_HX_SERVICE_PORT_CODE.POLICY_RENEWAL_UPLOAD_ONE.code().equals(portCode)) {
				ReqContinuePolicyInfoCoreDTO continuePolicyInfoCore = JSONObject.parseObject(reqJSON, ReqContinuePolicyInfoCoreDTO.class);
				ReqContinuePolicyInfoDTO continuePolicyInfo = (ReqContinuePolicyInfoDTO)continuePolicyInfoCore.getBody();
				continuePolicyList.add(continuePolicyInfo);
				coreRequestHead = continuePolicyInfoCore.getRequestHead();
			//多笔请求	
			} else if (ENUM_HX_SERVICE_PORT_CODE.POLICY_RENEWAL_UPLOAD_MORE.code().equals(portCode)){
				ReqMContinuePolicyInfoCoreDTO reqMContinuePolicyInfoCore = JSONObject.parseObject(reqJSON, ReqMContinuePolicyInfoCoreDTO.class);
				ReqMContinuePolicyInfoDTO reqMContinuePolicyInfoDTO = (ReqMContinuePolicyInfoDTO)reqMContinuePolicyInfoCore.getBody();
				continuePolicyList = reqMContinuePolicyInfoDTO.getPolicyList();
				coreRequestHead = reqMContinuePolicyInfoCore.getRequestHead();
			} else {
				throw new BusinessDataErrException("请求接口编码有误! 错误接口编码为："+portCode);
			}
			//2. 封装头部信息
			paramHead.setAreaCode(coreRequestHead.getAreaCode());
			paramHead.setRecordNum(coreRequestHead.getRecordNum());
			paramHead.setSerialNo(coreRequestHead.getSerialNo());
			paramHead.setPortCode(portCode);
			ThirdSendDTO thirdSendDTO = new ThirdSendDTO();
			EbaoServerPortDTO ebaoServerPort = null;

			Holder<ResponseHeader> responseHeaderHolder = new Holder<ResponseHeader>();
			Holder<ResponseBody> responseBodyHolder = new Holder<ResponseBody>();
			// 预约码处理方式
			if(ENUM_DISPOSE_TYPE.APPOINTMENT.code().equals(faultTask.getDisposeType())){
				//3. 封装请求中保信数据
				paramHead.setRecordNum("1"); //异步请求接口请求记录数为1
				BookSequenceDTO bookSequence = new BookSequenceDTO();
				bookSequence.setBookingSequenceNo(faultTask.getRemark());
				thirdSendDTO = commonCodeService.createThirdSend(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_RENEWAL_UPLOAD_APPOINTMENT.code(), paramHead, bookSequence);
				ebaoServerPort = commonCodeService.queryEBaoServerInfoByPortCode(ENUM_EBAO_SERVICE_PORT_CODE.ASYN_RESULT_QUERY.code());
				logger.info("预约码 - 多笔续保信息上传（请求中保信）JSON 数据内容："+JSONObject.toJSONString(thirdSendDTO));
				String portUrl = ebaoServerPort.getPortUrl();
				URL wsdlLocation = new URL(portUrl);
				AsynResultQueryService_Service service = new AsynResultQueryService_Service(wsdlLocation);
				AsynResultQueryService servicePort = service.getAsynResultQueryServicePort();
				BindingProvider bp = (BindingProvider) servicePort;
				bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, portUrl);
				servicePort.asynResultQuery(thirdSendDTO.getHead(), thirdSendDTO.getBody(),responseHeaderHolder, responseBodyHolder);
			}else if(ENUM_DISPOSE_TYPE.ASYNC.code().equals(faultTask.getDisposeType()) || ENUM_DISPOSE_TYPE.ERROR.code().equals(faultTask.getDisposeType())){
				// 将续保信息上传请求dto转换为中保信续保信息上传请求dto
				ReqMContinuePolicyInfoDTO reqMContinuePolicyInfoDTO = new ReqMContinuePolicyInfoDTO();
				reqMContinuePolicyInfoDTO.setPolicyList(continuePolicyList);
				ReqContinuePolicyListDTO reqContinuePolicyListDTO = this.convertCarrier(reqMContinuePolicyInfoDTO);
				thirdSendDTO = commonCodeService.createThirdSend(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_RENEWAL_UPLOAD.code(), paramHead, reqContinuePolicyListDTO);
				ebaoServerPort = commonCodeService.queryEBaoServerInfoByPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_RENEWAL_UPLOAD.code());
				logger.info("异常任务 - 续保信息上传（请求中保信）JSON 数据内容(错误或异常类型)："+JSONObject.toJSONString(thirdSendDTO));
				String portUrl = ebaoServerPort.getPortUrl();
				URL wsdlLocation = new URL(portUrl);
				PolicyRenewalService_Service service = new PolicyRenewalService_Service(wsdlLocation);
				PolicyRenewalService servicePort = service.getPolicyRenewalServicePort();
				BindingProvider bp = (BindingProvider) servicePort;
				bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, portUrl);
				servicePort.renewal(thirdSendDTO.getHead(), thirdSendDTO.getBody(),responseHeaderHolder, responseBodyHolder);
			}else{
				throw new BusinessDataErrException("异常类型有误! 错误异常类型为："+ENUM_DISPOSE_TYPE.getEnumValueByKey(faultTask.getDisposeType()));
			}
			ResponseHeader responseHeader = responseHeaderHolder.value;
			ResponseBody responseBody = responseBodyHolder.value;
			logger.info("异常任务 - 续保信息上传（中保信响应）JSON 数据内容："+responseBody.getJsonString());
			ResponseJSON responseJSON = new ResponseJSON();
			responseJSON.setResponseBody(responseBody);
			responseJSON.setResponseHeader(responseHeader);
			// 如果是异步类型需要记录日志
			if(ENUM_DISPOSE_TYPE.ASYNC.code().equals(faultTask.getDisposeType())){
				SendLogDTO sendLog = new SendLogDTO();
				sendLog.setSerialNo(paramHead.getSerialNo());
				sendLog.setPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_PREMIUM_UPLOAD.code());
				sendLog.setRequestContent(JSONObject.toJSONString(thirdSendDTO));
				sendLog.setResponseContent(JSONObject.toJSONString(responseJSON));
				sendLog.setTargetCode(ENUM_SENDSERVICE_CODE.EBAO.code());
				sendLog.setState(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
				sendLog.setDescription(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
				commonCodeService.addLogSend(sendLog);
			}
			// 初始化头部错误信息
			String messageBody = ENUM_BZ_RESPONSE_RESULT.FAIL.description();
			//5. 封装返回核心dto;
			if(ENUM_RESULT_CODE.SUCCESS.code().equals(responseHeader.getResultCode())
					|| ENUM_RESULT_CODE.INPUT_LOCALITY_SUCCESS.code().equals(responseHeader.getResultCode())){
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
				thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
				ResContinuePolicyDTO resContinuePolicyDTO = JSONObject.parseObject(responseBody.getJsonString(), ResContinuePolicyDTO.class);
				for (ResContinuePolicyListDTO resDto : resContinuePolicyDTO.getRenewalResultList()) {
					if(resDto.getResult().getResultCode().equals(ENUM_RESULT_CODE.SUCCESS.code())){
						for (ReqContinuePolicyInfoDTO reqContinuePolicyInfoDTO : continuePolicyList) {
							if(reqContinuePolicyInfoDTO.getRenewalEndorsementNo().equals(resDto.getRenewalEndorsementNo())){
								addContinuePolicy(reqContinuePolicyInfoDTO);
								break;
							}
						}
					}
				}	
			}else if(ENUM_RESULT_CODE.REQ_AWAIT.code().equals(responseHeader.getResultCode())){
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
				thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
			}else{
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
				List list = responseHeader.getResultMessage().getMessage();
				if(list != null && list.size() > 0){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : responseHeader.getResultMessage().getMessage()){
						sbStr.append(mess).append("|");
					}
					String message = messageBody = sbStr.substring(0, sbStr.length()-1).toString();
					thirdSendResponse.setResultDesc(message);
				}else{
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
				}
			}
			boolean isRegisterFaulTask = false;
			// 获取业务数据
			if(!StringUtil.isEmpty(responseBody.getJsonString())){
				ResContinuePolicyDTO resContinuePolicyDTO = JSONObject.parseObject(responseBody.getJsonString(), ResContinuePolicyDTO.class);
				if (null != resContinuePolicyDTO.getRenewalResultList()) {
					List<ResultContinuePolicyDTO> resultContinuePolicyList = new ArrayList<ResultContinuePolicyDTO>();
					// 成功记录数
					Integer successNum = 0;
					// 失败记录数
					Integer failNum = 0;
					// 获取业务数据
					for(ReqContinuePolicyInfoDTO reqContinuePolicyInfoDTO : continuePolicyList){
						if (resContinuePolicyDTO.getRenewalResultList() != null) {
							for(ResContinuePolicyListDTO ResContinuePolicyListDTO : resContinuePolicyDTO.getRenewalResultList()){
								if (ResContinuePolicyListDTO.getRenewalEndorsementNo().equals(reqContinuePolicyInfoDTO.getRenewalEndorsementNo())) {
									ResultContinuePolicyDTO resultContinuePolicyDTO = new ResultContinuePolicyDTO();
									resultContinuePolicyDTO.setBizNo(reqContinuePolicyInfoDTO.getBizNo());
									resultContinuePolicyDTO.setRenewalSequenceNo(ResContinuePolicyListDTO.getRenewalSequenceNo());
									EBResultCodeDTO eBResultCode = ResContinuePolicyListDTO.getResult();
									HXResultCodeDTO hxResultCode = new HXResultCodeDTO();
									hxResultCode.setResultCode(eBResultCode.getResultCode());
									String message = "";
									if(null != eBResultCode.getResultMessage()){
										StringBuffer sbStr = new StringBuffer(); 
										for(String mess : eBResultCode.getResultMessage()){
											sbStr.append(mess).append("|");
										}
										message = sbStr.substring(0, sbStr.length()-1).toString();
									}
									hxResultCode.setResultMessage(message);
									resultContinuePolicyDTO.setResult(hxResultCode);
									resultContinuePolicyList.add(resultContinuePolicyDTO);
									//记录成功和失败记录数
									if(ENUM_RESULT_CODE.SUCCESS.code().equals(eBResultCode.getResultCode())){
										successNum+=1;
									}else{
										failNum+=1;
										ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
										reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
										reqBusinessDetailDTO.setResultMessage(message);
										reqBusinessDetailList.add(reqBusinessDetailDTO);
									}
								}
							}
						}
					}
					// 登记请求记录
					businessDTO.setReqSuccessNum(successNum);
					businessDTO.setReqFailNum(failNum);
					thirdSendResponse.setResJson(resultContinuePolicyList);
					isRegisterFaulTask = true;
				}else{
					BookSequenceDTO bookSequence = JSONObject.parseObject(responseBody.getJsonString(),BookSequenceDTO.class);
					// 插入异常任务表
					faultTask.setRemark(bookSequence.getBookingSequenceNo());
					faultTask.setDisposeType(ENUM_DISPOSE_TYPE.APPOINTMENT.code());
					thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
				}
			}else{
				businessDTO.setReqFailNum(responseHeader.getRecordNum());
				thirdSendResponse.setResJson(messageBody);
				for (int i = 0; i < responseHeader.getRecordNum(); i++) {
					ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
					reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
					reqBusinessDetailDTO.setResultMessage(thirdSendResponse.getResultDesc());
					reqBusinessDetailList.add(reqBusinessDetailDTO);
				}
			}
			if(isRegisterFaulTask){
				commonCodeService.addReqBusinessDetail(reqBusinessDetailList);
				businessDTO.setRequetNum(responseHeader.getRecordNum());
				commonCodeService.addReqBusinessCollate(businessDTO);
				faultTask.setDisposeResult(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
			}else{
				faultTask.setDisposeResult(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("续保信息上传异常任务服务方法处理发生异常! 异常信息为："+e.getMessage());
			thirdSendResponse.setResJson(e.getMessage());
			thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
			thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
			faultTask.setDisposeResult(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
			if(ENUM_DISPOSE_TYPE.APPOINTMENT.code().equals(faultTask.getDisposeType())){
				faultTask.setDisposeType(ENUM_DISPOSE_TYPE.APPOINTMENT.code());
			}else{
				faultTask.setDisposeType(ENUM_DISPOSE_TYPE.ERROR.code());
			}
		} finally{
			faultTask.setSuccessMessage(JSONObject.toJSONString(thirdSendResponse));
			faultTask.setDisposeDate(DateUtil.getCurrentDate());
			taxFaultTaskMapper.updateByPrimaryKeySelective(faultTask);
		}
		return faultTask;
	}
}