package com.sinosoft.taxbenefit.core.transCode.biz.service;


/**
 * 
 * @author SheChunMing
 *
 */
public interface TransCodeService {
	/**
	 * 将核心的代码转为中保信的代码 
	 * @param codeType 转码的类型
	 * @param lisCode 核心编码
	 * @return 中保信的代码DTO
	 */
	String transCode(String codeType,String lisCode);
	/**
	 * 
	 * @param codeType 代码类型
	 * @param zbxCode 中保信编码
	 * @return
	 */
	String unTransCode(String codeType,String zbxCode);
	
	/**
	 * 将核心的代码转为中保信的代码(区分机构编码)
	 * @param codeType 代码字段名
	 * @param lisCode 核心编码
	 * @param manageCom 归属机构
	 * @return
	 */
	String transCodeManage(String codeType,String lisCode, String manageCom);

	
}
