package com.sinosoft.taxbenefit.core.policystatechange.dto;

import java.util.List;

public class PolicyEndorsement {

	private List<PolicyInsured> insuredList;
	
	public List<PolicyInsured> getInsuredList() {
		return insuredList;
	}
	public void setInsuredList(List<PolicyInsured> insuredList) {
		this.insuredList = insuredList;
	}
	/** 保单号 **/
	private String policyNo;
	/** 保单状态修改类型 **/
	private String policyStatusUpdateType;
	/** 保全批单号 **/
	private String endorsementNo;
	/** 保单批单序号 **/
	private String polEndSeq;
	/** 批单完成时间 **/
	private String finishTime;
	/** 保单终止日期 **/
	private String terminationDate;
	/** 保单中止日期 **/
	private String suspendDate;
	/** 保单状态 **/
	private String policyStatus;
	/** 保单终止原因 **/
	private String terminationReason;
	public String getPolicyNo() {
		return policyNo;
	}
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	public String getPolicyStatusUpdateType() {
		return policyStatusUpdateType;
	}
	public void setPolicyStatusUpdateType(String policyStatusUpdateType) {
		this.policyStatusUpdateType = policyStatusUpdateType;
	}
	public String getEndorsementNo() {
		return endorsementNo;
	}
	public void setEndorsementNo(String endorsementNo) {
		this.endorsementNo = endorsementNo;
	}
	public String getPolEndSeq() {
		return polEndSeq;
	}
	public void setPolEndSeq(String polEndSeq) {
		this.polEndSeq = polEndSeq;
	}
	public String getFinishTime() {
		return finishTime;
	}
	public void setFinishTime(String finishTime) {
		this.finishTime = finishTime;
	}
	public String getTerminationDate() {
		return terminationDate;
	}
	public void setTerminationDate(String terminationDate) {
		this.terminationDate = terminationDate;
	}
	public String getSuspendDate() {
		return suspendDate;
	}
	public void setSuspendDate(String suspendDate) {
		this.suspendDate = suspendDate;
	}
	public String getPolicyStatus() {
		return policyStatus;
	}
	public void setPolicyStatus(String policyStatus) {
		this.policyStatus = policyStatus;
	}
	public String getTerminationReason() {
		return terminationReason;
	}
	public void setTerminationReason(String terminationReason) {
		this.terminationReason = terminationReason;
	}
	
	
	

}
