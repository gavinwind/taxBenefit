/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.ewealth.general.config<br/>
 * @FileName: ENUM_REPORTLOG_BIZTYPE.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.taxbenefit.core.common.config;

/**
 * 中保信请求接口类型枚举类
 * @author SheChunMing
 * @date 2015年9月15日 下午11:21:14 
 * @version V1.0  
 */
public enum ENUM_EBAO_SERVICE_PORT_CODE {
	POLICY_IN_TRANSFER_UPLOAD("END005", "保单外部转入申请上传"),
	POLICY_OUT_TRANSFER_SEL("END006", "保单转出信息查询"),
	POLICY_OUT_TRANSFER_RECORD("END007", "保单转出登记"),
	POLICY_OUT_TRANSFER_APPOINTMENT("END008", "保单转出登记异步查询结果"),
	POLICY_OUT_TRANSFER_REGISTER_SEL("END009", "保单转出登记信息查询"),
	POLICY_IN_TRANSFER_BALANCE_SEL("END010", "保单转入余额信息查询"),
	CLAIM_SETTLEMEN("CLM001", "理赔信息上传"),
	CLAIM_SETTLEMEN_APPOINTMENT("CLM002","理赔信息异步处理结果"),
	CLAIM_REVERSAL("CLM003", "理赔案件撤销上传"),
	SERCURITY_CHANGE("END001", "保全变更信息上传"),
	POLICY_PREMIUM_UPLOAD("PRM001", "续期保费信息上传"),
	POLICY_PREMIUM_UPLOAD_APPOINTMENT("PRM002","获取续期保费信息异步处理结果"),
	POLICY_RENEWAL_UPLOAD("RNW001", "续保信息上传"),
	POLICY_RENEWAL_UPLOAD_APPOINTMENT("RNW002", "获取续保信息异步处理结果"),
	POLICY_RENEWAL_CANCEL("RNW003", "续保撤销"),
	TAX_BENE_VERIFY("PTY003", "税优验证上传 "),
	ENUM_CUST_VERIFY("PTY001", "客户身份验证上传 "),
	ENUM_CUST_VERIFY_APPOINTMENT("PTY002", "获取客户身份验证上传异步处理结果"),
	INSURED_PAY_SEL("PTY004", "被保人既往理赔额查询 "),
	BARGAIN_CHECK_QUERY("CHK001", "交易核对信息查询"),
	APPACCEPT_UPLOAD("NBU001", "承保信息上传"),
	GET_APPACCEPT_DISPOSE_RESULT("NBU002","获取承保异步处理结果"),
	POLICY_REVERSAL("NBU003", "承保撤销上传"),
	CUSTOMER_INFO_QUERY("QRY001", "客户概要信息查询"),
	ACCONUT_CHANGE("ACT001", "万能账户变更信息上传"),
	ACCONUT_CHANGE_APPOINTMENT("ACT002","获取账户变更异步处理结果"),
	POLICY_STATE_CHANGE("END003", "保单状态修改"),
	GET_POLICY_STATE_DISPOSE_RESULT("END004","获取保单状态修改异步处理结果"),
	SERCURITY_CANCEL("END002", "保全撤销上传"),
	ASYN_RESULT_QUERY("CHK002", "预约码查询");
	private final String code;
	private final String desc;

	ENUM_EBAO_SERVICE_PORT_CODE(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public String code() {
		return code;
	}

	public String desc() {
		return desc;
	}
}


