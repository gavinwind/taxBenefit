package com.sinosoft.taxbenefit.core.faultTask.dto;

public class AsynInteractionDTO {
	// 主流水号
	private String mainSerialNo;
	// 子流水号
	private String sonSerialNo;
	// 业务关联号
	private String busNo;
	
	public String getMainSerialNo() {
		return mainSerialNo;
	}
	public void setMainSerialNo(String mainSerialNo) {
		this.mainSerialNo = mainSerialNo;
	}
	public String getSonSerialNo() {
		return sonSerialNo;
	}
	public void setSonSerialNo(String sonSerialNo) {
		this.sonSerialNo = sonSerialNo;
	}
	public String getBusNo() {
		return busNo;
	}
	public void setBusNo(String busNo) {
		this.busNo = busNo;
	}
	
}
