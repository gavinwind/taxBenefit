package com.sinosoft.taxbenefit.core.applicant.dto;


/**
 * 核保信息
 * 
 * @author zhangke
 *
 */
public class AppUnderwritingDTO {

	/** 核保结论日期 **/
	private String underwritingDate;
	/** 核保决定 **/
	private String underwritingDecision;
	/** 核保描述 **/
	private String underwritingDes;

	public String getUnderwritingDate() {
		return underwritingDate;
	}

	public void setUnderwritingDate(String underwritingDate) {
		this.underwritingDate = underwritingDate;
	}

	public String getUnderwritingDecision() {
		return underwritingDecision;
	}

	public void setUnderwritingDecision(String underwritingDecision) {
		this.underwritingDecision = underwritingDecision;
	}

	public String getUnderwritingDes() {
		return underwritingDes;
	}

	public void setUnderwritingDes(String underwritingDes) {
		this.underwritingDes = underwritingDes;
	}

}
