package com.sinosoft.taxbenefit.core.SelCustomerInfo.biz.dubbo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqSelCustomerInfoDTO;
import com.sinosoft.taxbenefit.api.inter.CustomerInfoQueryApiService;
import com.sinosoft.taxbenefit.core.SelCustomerInfo.biz.service.SelCustomerInfoService;
/**
 * 客户信息查询DUBBO服务
 * @author zhangyu
 * @date 2016年3月11日 下午3:07:02
 */
@Service("customerInfoQueryApiServiceImpl")
public class CustomerInfoQueryApiServiceImpl implements
		CustomerInfoQueryApiService {
    @Autowired
	SelCustomerInfoService selCustomerInfoService;
	@Override
	public ThirdSendResponseDTO queryCustomerInfo(
			ReqSelCustomerInfoDTO reqSelCustomerInfoDTO,
			ParamHeadDTO paramHeadDTO) {
		return selCustomerInfoService.queryCusInfo(reqSelCustomerInfoDTO, paramHeadDTO);
	}
}