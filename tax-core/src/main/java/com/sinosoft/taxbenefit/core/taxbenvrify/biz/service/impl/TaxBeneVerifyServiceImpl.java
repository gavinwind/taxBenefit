package com.sinosoft.taxbenefit.core.taxbenvrify.biz.service.impl;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.platform.common.config.SystemConstants;
import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.platform.common.util.DateUtil;
import com.sinosoft.platform.common.util.StringUtil;
import com.sinosoft.taxbenefit.api.base.EBResultCodeDTO;
import com.sinosoft.taxbenefit.api.base.HXResultCodeDTO;
import com.sinosoft.taxbenefit.api.config.ENUM_BZ_RESPONSE_RESULT;
import com.sinosoft.taxbenefit.api.config.ENUM_DISPOSE_TYPE;
import com.sinosoft.taxbenefit.api.config.ENUM_SENDSERVICE_CODE;
import com.sinosoft.taxbenefit.api.dto.request.TaxFaulTaskDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.CoreRequestHead;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqTaxBeneVerifyCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqTaxBeneVerifyInfoDTO;
import com.sinosoft.taxbenefit.api.dto.response.customer.TaxBenVerifyResultDTO;
import com.sinosoft.taxbenefit.core.common.biz.service.CommonCodeService;
import com.sinosoft.taxbenefit.core.common.config.ENUM_EBAO_SERVICE_PORT_CODE;
import com.sinosoft.taxbenefit.core.common.config.ENUM_HX_SERVICE_PORT_CODE;
import com.sinosoft.taxbenefit.core.common.config.ENUM_RC_STATE;
import com.sinosoft.taxbenefit.core.common.config.ENUM_RESULT_CODE;
import com.sinosoft.taxbenefit.core.common.config.ENUUM_CODE_MAPPING;
import com.sinosoft.taxbenefit.core.common.dto.EbaoServerPortDTO;
import com.sinosoft.taxbenefit.core.common.dto.SendLogDTO;
import com.sinosoft.taxbenefit.core.common.dto.ThirdSendDTO;
import com.sinosoft.taxbenefit.core.customer.biz.service.CustomerServiceCore;
import com.sinosoft.taxbenefit.core.ebaowebservice.CustomerHistoryClaimQueryService;
import com.sinosoft.taxbenefit.core.ebaowebservice.CustomerHistoryClaimQueryService_Service;
import com.sinosoft.taxbenefit.core.ebaowebservice.ResponseBody;
import com.sinosoft.taxbenefit.core.ebaowebservice.ResponseHeader;
import com.sinosoft.taxbenefit.core.ebaowebservice.ResponseJSON;
import com.sinosoft.taxbenefit.core.faultTask.biz.service.FaultTaskService;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxCustomerMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxCustomerVerifyTraceMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxFaultTaskMapper;
import com.sinosoft.taxbenefit.core.generated.model.TaxCustomer;
import com.sinosoft.taxbenefit.core.generated.model.TaxCustomerVerifyTrace;
import com.sinosoft.taxbenefit.core.generated.model.TaxCustomerVerifyTraceExample;
import com.sinosoft.taxbenefit.core.generated.model.TaxFaultTask;
import com.sinosoft.taxbenefit.core.taxbenvrify.biz.service.TaxBeneVerifyService;
import com.sinosoft.taxbenefit.core.taxbenvrify.dto.ReqTaxBeneVerDTO;
import com.sinosoft.taxbenefit.core.taxbenvrify.dto.ResTaxBeneVerifyDTO;
import com.sinosoft.taxbenefit.core.taxbenvrify.dto.TaxBeneClientDTO;
import com.sinosoft.taxbenefit.core.transCode.biz.service.TransCodeService;

/**
 * 税优验证具体实现
 * @author zhangyu
 * @date 2016年3月10日 上午9:46:22
 */
@Service
public class TaxBeneVerifyServiceImpl extends TaxBaseService implements TaxBeneVerifyService {
	@Autowired
	TaxCustomerMapper taxCustomerMapper;
	@Autowired
	CustomerServiceCore customerServiceCore;
	@Autowired
	TaxCustomerVerifyTraceMapper taxCustomerVerifyTraceMapper;
	@Autowired
	TransCodeService  transCodeService;
	@Autowired
	private FaultTaskService faultTaskService;
	@Autowired
	TaxFaultTaskMapper taxFaultTaskMapper;//异常任务
	@Autowired
	CommonCodeService commonCodeService;
	
	@Override
	public ThirdSendResponseDTO addtaxBeneVerify(ReqTaxBeneVerifyInfoDTO reqTaxBenefitInfoDTO,ParamHeadDTO paramHead) {
		ThirdSendResponseDTO thirdSendResponse = new ThirdSendResponseDTO();
		try {
			logger.info("税优验证上传（核心请求）JSON 体数据内容：" + JSONObject.toJSONString(reqTaxBenefitInfoDTO));
			//1.将税优验证请求dto转换为中保信客户验证请求dto	
			ReqTaxBeneVerDTO resTaxBeneVerDTO = this.tansforDTO(reqTaxBenefitInfoDTO);
			//2.调用中保信接口服务,获得返回结果   存储调用中保信日志表
			ThirdSendDTO thirdSendDTO = commonCodeService.createThirdSend(ENUM_EBAO_SERVICE_PORT_CODE.TAX_BENE_VERIFY.code(), paramHead,resTaxBeneVerDTO);
			logger.info("税优验证（请求中保信）JSON 数据内容：" + JSONObject.toJSONString(thirdSendDTO));
			Holder<ResponseHeader> responseHeaderHolder = new Holder<ResponseHeader>();
			Holder<ResponseBody> responseBodyHolder = new Holder<ResponseBody>();
			EbaoServerPortDTO ebaoServerPort = commonCodeService.queryEBaoServerInfoByPortCode(ENUM_EBAO_SERVICE_PORT_CODE.TAX_BENE_VERIFY.code());
			String portUrl = ebaoServerPort.getPortUrl();
			URL wsdlLocation = new URL(portUrl);
			CustomerHistoryClaimQueryService_Service service = new CustomerHistoryClaimQueryService_Service(wsdlLocation);
			CustomerHistoryClaimQueryService  servicePort = service.getCustomerHistoryClaimQueryServicePort();
			BindingProvider bp = (BindingProvider) servicePort;
			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, portUrl);
			servicePort.historyClaimQuery(thirdSendDTO.getHead(),  thirdSendDTO.getBody(), responseHeaderHolder, responseBodyHolder);
			ResponseHeader responseHeader = responseHeaderHolder.value;
			ResponseBody responseBody = responseBodyHolder.value;
			logger.info("税优验证上传（中保信响应）JSON 数据内容："+responseBody.getJsonString());
			// 封装中保信返回报文
			ResponseJSON responseJSON = new ResponseJSON();
			responseJSON.setResponseBody(responseBody);
			responseJSON.setResponseHeader(responseHeader);
		    // 封装发送报文日志对象
			SendLogDTO sendLog = new SendLogDTO();
			sendLog.setSerialNo(paramHead.getSerialNo());
			sendLog.setPortCode(ENUM_EBAO_SERVICE_PORT_CODE.TAX_BENE_VERIFY.code());
			sendLog.setRequestContent(JSONObject.toJSONString(thirdSendDTO));
			sendLog.setResponseContent(JSONObject.toJSONString(responseJSON));
			sendLog.setTargetCode(ENUM_SENDSERVICE_CODE.EBAO.code());
			sendLog.setState(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
			sendLog.setDescription(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
			commonCodeService.addLogSend(sendLog);
			//3.封装返回核心dto;
			if(ENUM_RESULT_CODE.SUCCESS.code().equals(responseHeader.getResultCode())
					|| ENUM_RESULT_CODE.INPUT_LOCALITY_SUCCESS.code().equals(responseHeader.getResultCode())){
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
				thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
			}else{
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
				List list = responseHeader.getResultMessage().getMessage();
				if(list != null && list.size() > 0){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : responseHeader.getResultMessage().getMessage()){
						sbStr.append(mess).append("|");
					}
					String message = sbStr.substring(0, sbStr.length()-1).toString();
					thirdSendResponse.setResultDesc(message);
				}else{
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
				}
			}
			TaxBenVerifyResultDTO taxBenVerifyResultDTO = new TaxBenVerifyResultDTO();
			// 获取业务数据
			if(!StringUtil.isEmpty(responseBody.getJsonString())){
				ResTaxBeneVerifyDTO resTaxBeneVerifyDTO = JSONObject.parseObject(responseBody.getJsonString(), ResTaxBeneVerifyDTO.class);
				EBResultCodeDTO eBResultCode = resTaxBeneVerifyDTO.getClientList().get(0).getResult();
				HXResultCodeDTO hxResultCode = new HXResultCodeDTO();
				hxResultCode.setResultCode(eBResultCode.getResultCode());
				String message = "";
				if(null != eBResultCode.getResultMessage()){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : eBResultCode.getResultMessage()){
						sbStr.append(mess).append("|");
					}
					message = sbStr.substring(0, sbStr.length() - 1).toString();
				}
				hxResultCode.setResultMessage(message);
				taxBenVerifyResultDTO.setCustomerNo(resTaxBeneVerifyDTO.getClientList().get(0).getCustomerNo());
				taxBenVerifyResultDTO.setTransferTimes(resTaxBeneVerifyDTO.getClientList().get(0).getTransferTimes());
				taxBenVerifyResultDTO.setBizNo(reqTaxBenefitInfoDTO.getBizNo());
				taxBenVerifyResultDTO.setResult(hxResultCode);
				thirdSendResponse.setResJson(taxBenVerifyResultDTO);
				if(ENUM_RESULT_CODE.SUCCESS.code().equals(eBResultCode.getResultCode())){
					addVerifyTrace(reqTaxBenefitInfoDTO, taxBenVerifyResultDTO);
				}		
			}
		} catch (Exception e) {
			logger.error("税优验证上传失败：" + e.getMessage());
			e.printStackTrace();
			thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
			thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
			// 存储任务表
			TaxFaulTaskDTO taxFaultTask = new TaxFaulTaskDTO();
			taxFaultTask.setSerialNo(paramHead.getSerialNo());
			taxFaultTask.setServerPortCode(paramHead.getPortCode());
			taxFaultTask.setServerPortName(paramHead.getPortName());
			taxFaultTask.setAreaCode(paramHead.getAreaCode());
			taxFaultTask.setDisposeType(ENUM_DISPOSE_TYPE.ERROR.code());
			faultTaskService.addTaxFaultTask(taxFaultTask);
		}
		logger.error("税优验证上传后置处理结束!");
		return thirdSendResponse;
	}
	/**
	 * 数据持久化
	 * @param reqTaxBenefitInfoDTO
	 * @param taxBenVerifyResultDTO
	 */
	private void addVerifyTrace(ReqTaxBeneVerifyInfoDTO reqTaxBenefitInfoDTO, TaxBenVerifyResultDTO taxBenVerifyResultDTO) {
		//存业务表
		TaxCustomer taxCustomer = new TaxCustomer();
		TaxCustomerVerifyTrace taxCustomerVerifyTrace = new TaxCustomerVerifyTrace();
		try {	
			//reqTaxBenefitInfoDTO为核心请求的DTO对象
			taxCustomer.setCustomerName(reqTaxBenefitInfoDTO.getName());
			taxCustomer.setGender(reqTaxBenefitInfoDTO.getGender());
			taxCustomer.setBirthday(DateUtil.parseDate(reqTaxBenefitInfoDTO.getBirthday(),"yyyy-MM-dd"));
			taxCustomer.setCardType(reqTaxBenefitInfoDTO.getCertiType());
			taxCustomer.setCardNo(reqTaxBenefitInfoDTO.getCertiNo());
			taxCustomer.setNationality(reqTaxBenefitInfoDTO.getNationality());
			taxCustomer.setRcState(ENUM_RC_STATE.EFFECTIVE.getCode());
			taxCustomer.setModifyDate(DateUtil.getCurrentDate());
			taxCustomer.setCreateDate(DateUtil.getCurrentDate());
			taxCustomer.setModifierId(SystemConstants.SYS_OPERATOR);
			taxCustomer.setCreatorId(SystemConstants.SYS_OPERATOR);	
			//taxBenVerifyResultDTO为返回中保信的DTO对象
			taxCustomer.setCustomerNo(taxBenVerifyResultDTO.getCustomerNo());
			//添加客户
			TaxCustomer temp = customerServiceCore.addCustomer(taxCustomer);
			taxCustomerVerifyTrace.setCustomerId(temp.getSid());
			taxCustomerVerifyTrace.setTransferTimes(taxBenVerifyResultDTO.getTransferTimes());
			taxCustomerVerifyTrace.setProposalNo(reqTaxBenefitInfoDTO.getProposalNo());
			Date coverageEffectiveDate=DateUtil.parseDate(reqTaxBenefitInfoDTO.getCoverageEffectiveDate(),"yyyy-MM-dd");
			taxCustomerVerifyTrace.setPolicyValidDate(coverageEffectiveDate);
			taxCustomerVerifyTrace.setContSource(reqTaxBenefitInfoDTO.getPolicySource());
			taxCustomerVerifyTrace.setRcState(ENUM_RC_STATE.EFFECTIVE.getCode());
			taxCustomerVerifyTrace.setModifyDate(DateUtil.getCurrentDate());
			taxCustomerVerifyTrace.setCreateDate(DateUtil.getCurrentDate());
			taxCustomerVerifyTrace.setModifierId(SystemConstants.SYS_OPERATOR);
			taxCustomerVerifyTrace.setCreatorId(SystemConstants.SYS_OPERATOR);	
			taxCustomerVerifyTrace.setBizNo(reqTaxBenefitInfoDTO.getBizNo());	
			if(this.checkVerifyTrace(taxCustomerVerifyTrace)){
				taxCustomerVerifyTraceMapper.insert(taxCustomerVerifyTrace);
			}
		} catch (Exception e) {
			logger.error("税优验证上传数据持久化失败" + e.getMessage());
			e.printStackTrace();
		}
	}
	/**
	 * 把核心请求的DTO转换为中保信请求的DTO
	 * @param reqTaxBenefitInfoDTO
	 * @return
	 */
	public ReqTaxBeneVerDTO tansforDTO(ReqTaxBeneVerifyInfoDTO reqTaxBenefitInfoDTO) {
		ReqTaxBeneVerDTO resTaxBeneVerDTO = new ReqTaxBeneVerDTO();
		TaxBeneClientDTO taxBeneClientDTO = new TaxBeneClientDTO();
	    List<TaxBeneClientDTO> TaxBeneClientList = new ArrayList<TaxBeneClientDTO>();
	    taxBeneClientDTO.setName(reqTaxBenefitInfoDTO.getName());
	    taxBeneClientDTO.setGender(transCodeService.transCode(ENUUM_CODE_MAPPING.GENDER.code(), reqTaxBenefitInfoDTO.getGender()));
	    taxBeneClientDTO.setBirthday(reqTaxBenefitInfoDTO.getBirthday());
	    taxBeneClientDTO.setCertiNo(reqTaxBenefitInfoDTO.getCertiNo());
	    taxBeneClientDTO.setCertiType(transCodeService.transCode(ENUUM_CODE_MAPPING.CARDTYPE.code(),reqTaxBenefitInfoDTO.getCertiType()));
	    taxBeneClientDTO.setCoverageEffectiveDate(reqTaxBenefitInfoDTO.getCoverageEffectiveDate());
	    taxBeneClientDTO.setProposalNo(reqTaxBenefitInfoDTO.getProposalNo());
	    taxBeneClientDTO.setNationality(reqTaxBenefitInfoDTO.getNationality());
	    taxBeneClientDTO.setPolicySource(reqTaxBenefitInfoDTO.getPolicySource());
	    TaxBeneClientList.add(taxBeneClientDTO);
	    resTaxBeneVerDTO.setClientList(TaxBeneClientList);
		return resTaxBeneVerDTO;
	}
	@Override
	public ThirdSendResponseDTO taxBeneVerifyExceptionJob(String reqJSON, String portCode, TaxFaultTask faultTask) {
		logger.info("进入税优验证异常任务服务");
		ThirdSendResponseDTO thirdSendResponse = new ThirdSendResponseDTO();
		try {
			ParamHeadDTO paramHead = new ParamHeadDTO();
			CoreRequestHead coreRequestHead = new CoreRequestHead();
			ReqTaxBeneVerifyInfoDTO reqTaxBeneVerifyInfoDTO = new ReqTaxBeneVerifyInfoDTO();
			//1. 解析请求报文
			if (ENUM_HX_SERVICE_PORT_CODE.TAX_BENE_VERIFY.code().equals(portCode)) {
				ReqTaxBeneVerifyCoreDTO reqTaxBeneVerifyCoreDTO = JSONObject.parseObject(reqJSON, ReqTaxBeneVerifyCoreDTO.class);
				reqTaxBeneVerifyInfoDTO = (ReqTaxBeneVerifyInfoDTO) reqTaxBeneVerifyCoreDTO.getBody();
				coreRequestHead = reqTaxBeneVerifyCoreDTO.getRequestHead();
			} else {
				throw new BusinessDataErrException("请求接口编码有误! 错误接口编码为："+portCode);
			}
			//2. 封装头部信息
			paramHead.setAreaCode(coreRequestHead.getAreaCode());
			paramHead.setRecordNum(coreRequestHead.getRecordNum());
			paramHead.setSerialNo(coreRequestHead.getSerialNo());
			ThirdSendDTO thirdSendDTO = new ThirdSendDTO();
			EbaoServerPortDTO ebaoServerPort = null;
			// 将税优验证请求dto转换为中保信税优验证请求dto
			ReqTaxBeneVerDTO reqTaxBeneVerDTO = this.tansforDTO(reqTaxBeneVerifyInfoDTO);
			thirdSendDTO = commonCodeService.createThirdSend(ENUM_EBAO_SERVICE_PORT_CODE.TAX_BENE_VERIFY.code(), paramHead, reqTaxBeneVerDTO);
			ebaoServerPort = commonCodeService.queryEBaoServerInfoByPortCode(ENUM_EBAO_SERVICE_PORT_CODE.TAX_BENE_VERIFY.code());
			logger.info("异常任务 - 税优验证（请求中保信）JSON 数据内容(错误或异常类型)：" + JSONObject.toJSONString(thirdSendDTO));
			Holder<ResponseHeader> responseHeaderHolder = new Holder<ResponseHeader>();
			Holder<ResponseBody> responseBodyHolder = new Holder<ResponseBody>();
			//4. 调用统一的预约码查询接口地址
			String portUrl = ebaoServerPort.getPortUrl();
			URL wsdlLocation = new URL(portUrl);
			CustomerHistoryClaimQueryService_Service service = new CustomerHistoryClaimQueryService_Service(wsdlLocation);
			CustomerHistoryClaimQueryService  servicePort = service.getCustomerHistoryClaimQueryServicePort();
			BindingProvider bp = (BindingProvider) servicePort;
			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, portUrl);
			servicePort.historyClaimQuery(thirdSendDTO.getHead(),  thirdSendDTO.getBody(), responseHeaderHolder, responseBodyHolder);
			ResponseHeader responseHeader = responseHeaderHolder.value;
			ResponseBody responseBody = responseBodyHolder.value;
			logger.info("税优验证（中保信响应）JSON 数据内容：" + responseBody.getJsonString());
			// 初始化头部错误信息
			String messageBody = ENUM_BZ_RESPONSE_RESULT.FAIL.description();
			//封装返回核心dto;
			if(ENUM_RESULT_CODE.SUCCESS.code().equals(responseHeader.getResultCode())
					|| ENUM_RESULT_CODE.INPUT_LOCALITY_SUCCESS.code().equals(responseHeader.getResultCode())){
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
				thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
			}else{
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
				List list = responseHeader.getResultMessage().getMessage();
				if(list != null && list.size() > 0){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : responseHeader.getResultMessage().getMessage()){
						sbStr.append(mess).append("|");
					}
					String message = messageBody = sbStr.substring(0, sbStr.length() - 1).toString();
					thirdSendResponse.setResultDesc(message);
				}else{
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
				}
			}
			TaxBenVerifyResultDTO taxBenVerifyResultDTO=new TaxBenVerifyResultDTO();
			// 获取业务数据
			if(!StringUtil.isEmpty(responseBody.getJsonString())){
				ResTaxBeneVerifyDTO resTaxBeneVerifyDTO=JSONObject.parseObject(responseBody.getJsonString(), ResTaxBeneVerifyDTO.class);
				EBResultCodeDTO eBResultCode=resTaxBeneVerifyDTO.getClientList().get(0).getResult();
				HXResultCodeDTO hxResultCode = new HXResultCodeDTO();
				hxResultCode.setResultCode(eBResultCode.getResultCode());
				String message = "";
				if(null != eBResultCode.getResultMessage()){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : eBResultCode.getResultMessage()){
						sbStr.append(mess).append("|");
					}
					message = sbStr.substring(0, sbStr.length() - 1).toString();
				}
				hxResultCode.setResultMessage(message);
				taxBenVerifyResultDTO.setCustomerNo(resTaxBeneVerifyDTO.getClientList().get(0).getCustomerNo());
				taxBenVerifyResultDTO.setTransferTimes(resTaxBeneVerifyDTO.getClientList().get(0).getTransferTimes());
				taxBenVerifyResultDTO.setBizNo(reqTaxBeneVerifyInfoDTO.getBizNo());
				taxBenVerifyResultDTO.setResult(hxResultCode);
				thirdSendResponse.setResJson(taxBenVerifyResultDTO);
				if(ENUM_RESULT_CODE.SUCCESS.code().equals(eBResultCode.getResultCode())){
					addVerifyTrace(reqTaxBeneVerifyInfoDTO, taxBenVerifyResultDTO);
				}
			}else{
				thirdSendResponse.setResJson(messageBody);
			}
			faultTask.setDisposeResult(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
		} catch (Exception e) {	
			thirdSendResponse.setResJson(e.getMessage());
			thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
			thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
			logger.error("税优验证异常任务服务方法处理发生异常! 异常信息为："+e.getMessage());
			faultTask.setDisposeType(ENUM_DISPOSE_TYPE.ERROR.code());
			faultTask.setDisposeResult(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
		} finally{
			faultTask.setSuccessMessage(JSONObject.toJSONString(thirdSendResponse));
			faultTask.setDisposeDate(DateUtil.getCurrentDate());
			taxFaultTaskMapper.updateByPrimaryKeySelective(faultTask);
		}
		return thirdSendResponse;
	}
	
	/**
	 * 查询该税优资格验证记录是否存在
	 * @param model
	 * @return
	 */
	private boolean checkVerifyTrace(TaxCustomerVerifyTrace model){
		TaxCustomerVerifyTraceExample example = new TaxCustomerVerifyTraceExample();
		example.createCriteria().andRcStateEqualTo(ENUM_RC_STATE.EFFECTIVE.getCode()).andProposalNoEqualTo(model.getProposalNo()).andCustomerIdEqualTo(model.getCustomerId());
		List<TaxCustomerVerifyTrace>list = taxCustomerVerifyTraceMapper.selectByExample(example);
		return list==null||list.size()==0;
	}
}