
package com.sinosoft.taxbenefit.core.ebaowebservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cn.com.ciitc.healthy package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Request_QNAME = new QName("http://www.ciitc.com.cn/healthy", "request");
    private final static QName _Response_QNAME = new QName("http://www.ciitc.com.cn/healthy", "response");
    private final static QName _Header_QNAME = new QName("http://www.ciitc.com.cn/healthy", "header");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cn.com.ciitc.healthy
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ResponseHeader }
     * 
     */
    public ResponseHeader createResponseHeader() {
        return new ResponseHeader();
    }

    /**
     * Create an instance of {@link ResponseBody }
     * 
     */
    public ResponseBody createResponseBody() {
        return new ResponseBody();
    }

    /**
     * Create an instance of {@link RequestBody }
     * 
     */
    public RequestBody createRequestBody() {
        return new RequestBody();
    }

    /**
     * Create an instance of {@link RequestHeader }
     * 
     */
    public RequestHeader createRequestHeader() {
        return new RequestHeader();
    }

    /**
     * Create an instance of {@link ResponseHeader.ResultMessage }
     * 
     */
    public ResponseHeader.ResultMessage createResponseHeaderResultMessage() {
        return new ResponseHeader.ResultMessage();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestBody }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ciitc.com.cn/healthy", name = "request")
    public JAXBElement<RequestBody> createRequest(RequestBody value) {
        return new JAXBElement<RequestBody>(_Request_QNAME, RequestBody.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseBody }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ciitc.com.cn/healthy", name = "response")
    public JAXBElement<ResponseBody> createResponse(ResponseBody value) {
        return new JAXBElement<ResponseBody>(_Response_QNAME, ResponseBody.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestHeader }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ciitc.com.cn/healthy", name = "header")
    public JAXBElement<RequestHeader> createHeader(RequestHeader value) {
        return new JAXBElement<RequestHeader>(_Header_QNAME, RequestHeader.class, null, value);
    }

}
