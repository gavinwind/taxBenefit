package com.sinosoft.taxbenefit.core.claim.dto;

import java.util.List;

/**
 * 理赔上传请求中保信dto
 * 
 * @author zhangke
 *
 */
public class ClaimCoreDTO {
	private List<ClaimInfoDTO> claimList;

	public List<ClaimInfoDTO> getClaimList() {
		return claimList;
	}

	public void setClaimList(List<ClaimInfoDTO> claimList) {
		this.claimList = claimList;
	}

}
