package com.sinosoft.taxbenefit.core.AccountChange.dto;

import java.math.BigDecimal;
/**
 * 账户变更信息请求信息
 * @author zhangyu
 * @date 2016年3月6日 上午11:58:57
 */
public class AccountChangeDTO {
	//保单号
	private String policyNo;
	//分单号
	private String sequenceNo;
	//公司产品组代码 
	private String coveragePackageCode;
	//公司险种代码
	private String comCoverageCode;
	//公司险种名称
	private String comCoverageName;
	//公司费用 ID 
	private String comFeeId;
	//费用发生日期
	private String feeDate;
	//费用发生类型 
	private String feeType;
	//费用金额
	private BigDecimal feeAmount;
	//风险保费公司险种
	private String riskComCoverageCode;
	//风险保费保障起期
	private String  riskCoverageStartDate;
	//风险保费保障止期 
	private String riskCoverageEndDate;
	//结息利率
	private BigDecimal interestRate;
	//账户余额 
	private BigDecimal balanceAmount;
	//红冲费用 ID
	private String revComFeeId;
	//账户余额调整原因 
	private String adjustReason;
	public String getPolicyNo() {
		return policyNo;
	}
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	public String getSequenceNo() {
		return sequenceNo;
	}
	public void setSequenceNo(String sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
	public String getCoveragePackageCode() {
		return coveragePackageCode;
	}
	public void setCoveragePackageCode(String coveragePackageCode) {
		this.coveragePackageCode = coveragePackageCode;
	}
	public String getComCoverageCode() {
		return comCoverageCode;
	}
	public void setComCoverageCode(String comCoverageCode) {
		this.comCoverageCode = comCoverageCode;
	}
	public String getComCoverageName() {
		return comCoverageName;
	}
	public void setComCoverageName(String comCoverageName) {
		this.comCoverageName = comCoverageName;
	}
	public String getComFeeId() {
		return comFeeId;
	}
	public void setComFeeId(String comFeeId) {
		this.comFeeId = comFeeId;
	}
	public String getFeeType() {
		return feeType;
	}
	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}
	public BigDecimal getFeeAmount() {
		return feeAmount;
	}
	public void setFeeAmount(BigDecimal feeAmount) {
		this.feeAmount = feeAmount;
	}
	public String getRiskComCoverageCode() {
		return riskComCoverageCode;
	}
	public void setRiskComCoverageCode(String riskComCoverageCode) {
		this.riskComCoverageCode = riskComCoverageCode;
	}
	public String getFeeDate() {
		return feeDate;
	}
	public void setFeeDate(String feeDate) {
		this.feeDate = feeDate;
	}
	public String getRiskCoverageStartDate() {
		return riskCoverageStartDate;
	}
	public void setRiskCoverageStartDate(String riskCoverageStartDate) {
		this.riskCoverageStartDate = riskCoverageStartDate;
	}
	public String getRiskCoverageEndDate() {
		return riskCoverageEndDate;
	}
	public void setRiskCoverageEndDate(String riskCoverageEndDate) {
		this.riskCoverageEndDate = riskCoverageEndDate;
	}
	public BigDecimal getInterestRate() {
		return interestRate;
	}
	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}
	public BigDecimal getBalanceAmount() {
		return balanceAmount;
	}
	public void setBalanceAmount(BigDecimal balanceAmount) {
		this.balanceAmount = balanceAmount;
	}
	public String getRevComFeeId() {
		return revComFeeId;
	}
	public void setRevComFeeId(String revComFeeId) {
		this.revComFeeId = revComFeeId;
	}
	public String getAdjustReason() {
		return adjustReason;
	}
	public void setAdjustReason(String adjustReason) {
		this.adjustReason = adjustReason;
	}
}