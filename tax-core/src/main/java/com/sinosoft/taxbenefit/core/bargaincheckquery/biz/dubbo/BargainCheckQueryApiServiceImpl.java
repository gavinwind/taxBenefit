package com.sinosoft.taxbenefit.core.bargaincheckquery.biz.dubbo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.taxbenefit.api.dto.request.bargaincheckquery.ReqBargainCheckQueryDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.inter.BargainCheckQueryApiService;
import com.sinosoft.taxbenefit.core.bargaincheckquery.biz.service.BargainCheckQueryCoreService;
/**
 * 交易核对信息查询
 * Title:BargainCheckQueryApiServiceImpl
 * @author yangdongkai@outlook.com
 * @date 2016年3月10日 下午8:57:50
 */
@Service("bargainCheckQueryApiServiceImpl")
public class BargainCheckQueryApiServiceImpl extends TaxBaseService implements BargainCheckQueryApiService{
	@Autowired
	BargainCheckQueryCoreService bargainCheckQueryCoreService;
	
	@Override
	public ThirdSendResponseDTO bargainCheck(ReqBargainCheckQueryDTO reqBargainCheckQueryDTO,ParamHeadDTO paramHead) {
		return bargainCheckQueryCoreService.bargainCheckQuery(reqBargainCheckQueryDTO, paramHead);
	}
	
}
