package com.sinosoft.taxbenefit.core.faultTask.biz.service;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.common.FaultTaskDTO;
import com.sinosoft.taxbenefit.api.dto.request.TaxFaulTaskDTO;
import com.sinosoft.taxbenefit.core.faultTask.dto.AsynInteractionDTO;
import com.sinosoft.taxbenefit.core.generated.model.TaxAsynInteraction;
import com.sinosoft.taxbenefit.core.generated.model.TaxFaultTask;
	/**
	 * 任务记录接口
	 * @author zhangke
	 *
	 */
	public interface FaultTaskService {
	
	/**
	 *记录新的任务记录(处理中报文)
	 * @param taxFaulTaskDTO
	 */
	Integer addTaxFaultTask(TaxFaulTaskDTO taxFaulTaskDTO);
	
	/**
	 * 根据流水号查询任务日志记录DTO
	 * @param serialNo
	 * @return 
	 */
	FaultTaskDTO queryTaxFaultTaskBySerialNo(String serialNo);
	/**
	 * 根据状态代码查找异常任务记录信息(异步类型并未处理的)
	 * 
	 * @param stateCode
	 * @return
	 */
	List<TaxFaultTask> queryTaxFaultTaskByStateCode(String disposeType,String stateCode);
	 
	/**
	 * 处理中和待处理的任务记录
	 * @param stateCode
	 * @return
	 */
	List<TaxFaultTask> queryUnfinishedTaskByStateCode(String stateCode);
	
	/**
	 * 更新子任务的返回内容
	 * @param serialNo
	 * @param resmessage
	 */
	void updateFaultTaskResMessageBySerialNo(String serialNo, String resmessage, String resultCode);
	
	/**
	 * 更新子任务的处理状态
	 * @param serialNo
	 */
	void updateFaultTaskDisportStateBySerialNo(String serialNo);
	
	/**
	 * 添加异步数据关联表数据
	 * @param asynInter
	 */
	void insertAsynInteraction(AsynInteractionDTO asynInter);
	
	/**
	 * 根据主流水号,获取所有子流水号的数据
	 * @param serialNo
	 * @return
	 */
	List<TaxAsynInteraction> selectTaxAsynInteractionByMainSreialNo(String serialNo);
}
