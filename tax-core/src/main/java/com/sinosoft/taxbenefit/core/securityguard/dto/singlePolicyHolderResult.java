package com.sinosoft.taxbenefit.core.securityguard.dto;

public class singlePolicyHolderResult {
	/** 原投保人编码 **/
	private String customerNo;
	/** 新投保人编码 **/
	private String customerNoNew;
	/** 税优提示 **/
	private String taxWarning;
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public String getCustomerNoNew() {
		return customerNoNew;
	}
	public void setCustomerNoNew(String customerNoNew) {
		this.customerNoNew = customerNoNew;
	}
	public String getTaxWarning() {
		return taxWarning;
	}
	public void setTaxWarning(String taxWarning) {
		this.taxWarning = taxWarning;
	}
	
	
	
}
