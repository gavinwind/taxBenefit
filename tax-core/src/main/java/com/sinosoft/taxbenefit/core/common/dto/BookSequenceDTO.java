package com.sinosoft.taxbenefit.core.common.dto;

/**
 * 中保信异步返回对象
 * @author SheChunMing
 */
public class BookSequenceDTO {
	// 上传预约码
	private String bookingSequenceNo;

	public String getBookingSequenceNo() {
		return bookingSequenceNo;
	}

	public void setBookingSequenceNo(String bookingSequenceNo) {
		this.bookingSequenceNo = bookingSequenceNo;
	}
	
	
}
