package com.sinosoft.taxbenefit.core.policy.config;

/**
 * 保单状态
 * @author zhangke
 *
 */
public enum ENUM_POLICY_STATE {
	EFFECTIVE("01","有效"),OTHER("02","其它"),SUSPEND("03","中止"),STOP("04","终止"),
	OTHERS("05","其它");
	private final String code;
	private final String desc;
	
	ENUM_POLICY_STATE(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public String code() {
		return code;
	}

	public String desc() {
		return desc;
	}
	public static ENUM_POLICY_STATE getEnumByKey(String key){
		for(ENUM_POLICY_STATE enumItem:ENUM_POLICY_STATE.values()){
			if(key.equals(enumItem.code())){
				return enumItem;
			}
		}
		return null;
	}
}
