package com.sinosoft.taxbenefit.core.transCode.config;

/**
 * 转码定义常量
 * @author SheChunMing
 */
public class TransCodeConstants {
	// 默认机构编码
	public final static String TRANSCODE_MANAGE = "00";
}
