package com.sinosoft.taxbenefit.core.policy.biz.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.taxbenefit.core.common.config.ENUM_RC_STATE;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxContApplyPersonMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxContBenefitorMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxContGroupApplyPersonMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxContInsuredMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxContMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxContPaymentMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxContPolicyDutyMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxContPolicyUwinfoMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxPolicyRiskMapper;
import com.sinosoft.taxbenefit.core.generated.model.TaxCont;
import com.sinosoft.taxbenefit.core.generated.model.TaxContApplyPerson;
import com.sinosoft.taxbenefit.core.generated.model.TaxContApplyPersonExample;
import com.sinosoft.taxbenefit.core.generated.model.TaxContBenefitor;
import com.sinosoft.taxbenefit.core.generated.model.TaxContBenefitorExample;
import com.sinosoft.taxbenefit.core.generated.model.TaxContExample;
import com.sinosoft.taxbenefit.core.generated.model.TaxContGroupApplyPerson;
import com.sinosoft.taxbenefit.core.generated.model.TaxContGroupApplyPersonExample;
import com.sinosoft.taxbenefit.core.generated.model.TaxContInsured;
import com.sinosoft.taxbenefit.core.generated.model.TaxContInsuredExample;
import com.sinosoft.taxbenefit.core.generated.model.TaxContPayment;
import com.sinosoft.taxbenefit.core.generated.model.TaxContPolicyDuty;
import com.sinosoft.taxbenefit.core.generated.model.TaxContPolicyDutyExample;
import com.sinosoft.taxbenefit.core.generated.model.TaxContPolicyUwinfo;
import com.sinosoft.taxbenefit.core.generated.model.TaxContPolicyUwinfoExample;
import com.sinosoft.taxbenefit.core.generated.model.TaxPolicyRisk;
import com.sinosoft.taxbenefit.core.generated.model.TaxPolicyRiskExample;
import com.sinosoft.taxbenefit.core.policy.biz.service.PolicyService;

@Service
public class PolicyServiceImpl implements PolicyService {
	@Autowired
	private TaxContPaymentMapper taxContPaymentMapper;
	@Autowired
	private TaxContPolicyDutyMapper taxContPolicyDutyMapper;// 险种责任
	@Autowired
	private TaxContBenefitorMapper taxContBenefitMapper;// 受益人
	@Autowired
	private TaxContInsuredMapper taxContInsuredMapper;// 被保人
	@Autowired
	private TaxContMapper taxContMapper;// 保单
	@Autowired
	private TaxPolicyRiskMapper taxPolicyRiskMapper;// 险种
	@Autowired
	private TaxContPolicyUwinfoMapper taxContPolicyUwinfoMapper;// 核保
	@Autowired
	private TaxContApplyPersonMapper taxContApplyPersonMapper;// 投保人
	@Autowired
	private TaxContGroupApplyPersonMapper taxContGroupApplyPersonMapper;// 团体投保单位

	@Override
	public TaxCont queryPolicyByPolicyNo(String policyNo) {
		TaxContExample example = new TaxContExample();
		example.createCriteria().andContNoEqualTo(policyNo)
				.andRcStateEqualTo(ENUM_RC_STATE.EFFECTIVE.getCode());
		List<TaxCont> taxcont = taxContMapper.selectByExample(example);
		if (taxcont!=null&&taxcont.size() > 0) {
			return taxcont.get(0);
		}
		return null;
	}

	@Override
	public void modifyPolicy(TaxCont policy) {
		taxContMapper.updateByPrimaryKeySelective(policy);
	}

	@Override
	public void addPolicyInfo(TaxCont policyInfo) {
		taxContMapper.insert(policyInfo);

	}

	@Override
	public void addPolicyInsured(TaxContInsured policyInsured) {
		taxContInsuredMapper.insert(policyInsured);
	}

	@Override
	public void addPolicyHolder(TaxContApplyPerson singlePolicyHolder) {
		taxContApplyPersonMapper.insert(singlePolicyHolder);

	}

	@Override
	public void addBeneficiary(TaxContBenefitor beneficiary) {
		taxContBenefitMapper.insert(beneficiary);

	}

	@Override
	public void addGroupPolicyApply(TaxContGroupApplyPerson groupPolicyApply) {
		taxContGroupApplyPersonMapper.insert(groupPolicyApply);
	}

	@Override
	public void addCoverage(TaxPolicyRisk polciyRisk) {

		taxPolicyRiskMapper.insert(polciyRisk);
	}

	@Override
	public void addUnderwriting(TaxContPolicyUwinfo underwriting) {
		taxContPolicyUwinfoMapper.insert(underwriting);
	}

	@Override
	public void addTaxContPolicyDuty(TaxContPolicyDuty duty) {
		taxContPolicyDutyMapper.insert(duty);

	}

	@Override
	public void addpayment(TaxContPayment payment) {
		taxContPaymentMapper.insert(payment);
	}

	@Override
	public TaxPolicyRisk queryRiskInfo(String riskCode, Integer policyId) {

		TaxPolicyRiskExample example = new TaxPolicyRiskExample();
		example.createCriteria().andRiskCodeEqualTo(riskCode)
				.andContIdEqualTo(policyId)
				.andRcStateEqualTo(ENUM_RC_STATE.EFFECTIVE.getCode());
		List<TaxPolicyRisk> risk = taxPolicyRiskMapper.selectByExample(example);
		if (risk != null&&risk.size()>0) {

			return risk.get(0);
		} 
			return null;
	}

	@Override
	public TaxContInsured queryInsuredInfo(String customerNo, Integer policyId) {
		TaxContInsuredExample example = new TaxContInsuredExample();
		example.createCriteria().andCustomerNoEqualTo(customerNo)
				.andContIdEqualTo(policyId)
				.andRcStateEqualTo(ENUM_RC_STATE.EFFECTIVE.getCode());

		List<TaxContInsured> insured = taxContInsuredMapper.selectByExample(example);
		if(insured!=null&&insured.size()>0){
			return insured.get(0);
		}
		
		return null;
	}

	@Override
	public TaxContApplyPerson queryApplyPersonInfo(String customerNo,
			Integer policyNo) {

		TaxContApplyPersonExample example = new TaxContApplyPersonExample();
		if (customerNo != null && policyNo != null) {
			example.createCriteria().andCustomerCodeEqualTo(customerNo)
					.andContIdEqualTo(policyNo)
					.andRcStateEqualTo(ENUM_RC_STATE.EFFECTIVE.getCode());
		} else if (policyNo != null && customerNo == null) {
			example.createCriteria().andContIdEqualTo(policyNo)
					.andRcStateEqualTo(ENUM_RC_STATE.EFFECTIVE.getCode());
		}
		List<TaxContApplyPerson> apply = taxContApplyPersonMapper
				.selectByExample(example);

		if (apply != null && apply.size() > 0) {
			return apply.get(0);
		}
		return null;
	}

	@Override
	public TaxContGroupApplyPerson queryGroupApply(String customerNo,
			Integer policyNo) {
		TaxContGroupApplyPersonExample groupApply = new TaxContGroupApplyPersonExample();
		if (customerNo != null && policyNo != null) {
			groupApply.createCriteria().andCustomerNoEqualTo(customerNo)
					.andContIdEqualTo(policyNo)
					.andRcStateEqualTo(ENUM_RC_STATE.EFFECTIVE.getCode());
		}
		if (customerNo == null && policyNo != null) {
			groupApply.createCriteria().andContIdEqualTo(policyNo)
					.andRcStateEqualTo(ENUM_RC_STATE.EFFECTIVE.getCode());
		}

		List<TaxContGroupApplyPerson> gApply = taxContGroupApplyPersonMapper
				.selectByExample(groupApply);

		if (gApply != null && gApply.size() > 0) {
			return gApply.get(0);
		}

		return null;
	}

	@Override
	public TaxContBenefitor queryBeneficiary(String cardNO, Integer policyNo) {
		TaxContBenefitorExample example = new TaxContBenefitorExample();
		example.createCriteria().andCardNoEqualTo(cardNO)
				.andContIdEqualTo(policyNo)
				.andRcStateEqualTo(ENUM_RC_STATE.EFFECTIVE.getCode());
		List<TaxContBenefitor> benefitor = taxContBenefitMapper.selectByExample(
				example);
		if(benefitor!=null&&benefitor.size()>0){
			return benefitor.get(0);
		}
		return null;
	}

	@Override
	public TaxContPolicyDuty queryDutyInfo(String dutyCode, Integer riskId) {
		TaxContPolicyDutyExample example = new TaxContPolicyDutyExample();
		example.createCriteria().andDutyCodeEqualTo(dutyCode).andPolIdEqualTo(riskId)
					.andRcStateEqualTo(ENUM_RC_STATE.EFFECTIVE.getCode());
		List<TaxContPolicyDuty>		 dutylist=	taxContPolicyDutyMapper.selectByExample(example);
			if (dutylist!=null&&dutylist.size()>0) {
				return dutylist.get(0);
			}
		return null;
		
	}

	@Override
	public void modifyPolicyInsured(TaxContInsured insured) {
		taxContInsuredMapper.updateByPrimaryKeySelective(insured);
	}

	@Override
	public void modifyPolicyBeneficicary(TaxContBenefitor beneficiary) {
		taxContBenefitMapper.updateByPrimaryKeySelective(beneficiary);
	}

	@Override
	public void modifyPolicyHolder(TaxContApplyPerson singlePolicyHolder) {
		taxContApplyPersonMapper
				.updateByPrimaryKeySelective(singlePolicyHolder);
	}

	@Override
	public void modifyGroupPolicyApply(TaxContGroupApplyPerson groupPolicyHolder) {
		taxContGroupApplyPersonMapper
				.updateByPrimaryKeySelective(groupPolicyHolder);
	}

	@Override
	public void modifyCoverage(TaxPolicyRisk polciyRisk) {
		taxPolicyRiskMapper.updateByPrimaryKeySelective(polciyRisk);

	}

	@Override
	public TaxContPolicyUwinfo queryUnderwriting(Integer policyNo) {
		TaxContPolicyUwinfoExample example = new TaxContPolicyUwinfoExample();
		example.createCriteria().andPolIdEqualTo(policyNo)
				.andRcStateEqualTo(ENUM_RC_STATE.EFFECTIVE.getCode());
			List<TaxContPolicyUwinfo> uwinfolist=taxContPolicyUwinfoMapper.selectByExample(example);
		if(uwinfolist!=null&&uwinfolist.size()>0){
			return 	uwinfolist.get(0);
		}
		
		return null;
	}

	@Override
	public void modifyUnderwriting(TaxContPolicyUwinfo underwriting) {
		taxContPolicyUwinfoMapper.updateByPrimaryKeySelective(underwriting);
	}

	@Override
	public void modifyLiability(TaxContPolicyDuty liability) {

		taxContPolicyDutyMapper.updateByPrimaryKeySelective(liability);
	}

	@Override
	public void addLiability(TaxContPolicyDuty liability) {
		taxContPolicyDutyMapper.insert(liability);
	}

}
