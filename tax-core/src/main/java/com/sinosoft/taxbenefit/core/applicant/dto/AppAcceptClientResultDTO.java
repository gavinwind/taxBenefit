package com.sinosoft.taxbenefit.core.applicant.dto;

import com.sinosoft.taxbenefit.api.base.EBResultCodeDTO;
/**
 * 中保信返回的客户信息
 * @author zhangke
 *
 */
public class AppAcceptClientResultDTO {
	/** 分单号 */
	private String sequenceNo;
	/** 税优识别码 */
	private String taxCode;

	private EBResultCodeDTO result;

	public String getSequenceNo() {
		return sequenceNo;
	}

	public void setSequenceNo(String sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public EBResultCodeDTO getResult() {
		return result;
	}

	public void setResult(EBResultCodeDTO result) {
		this.result = result;
	}
}
