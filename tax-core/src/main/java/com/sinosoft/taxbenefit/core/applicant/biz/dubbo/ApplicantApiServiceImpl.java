package com.sinosoft.taxbenefit.core.applicant.biz.dubbo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.taxbenefit.api.dto.request.applicant.ReqAppPolicyDTO;
import com.sinosoft.taxbenefit.api.dto.request.applicant.ReqPolicyCancelDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.inter.ApplicantApiService;
import com.sinosoft.taxbenefit.core.applicant.biz.service.ApplicantCoreService;

/**
 * 承保上传Api实现类型
 * 
 * @author zhangke
 *
 */
@Service("applicantApiServiceImpl")
public class ApplicantApiServiceImpl extends TaxBaseService implements
		ApplicantApiService {
	@Autowired
	private ApplicantCoreService applicationCoreService;

	@Override
	public ThirdSendResponseDTO policyInforceService(
			List<ReqAppPolicyDTO>  body, ParamHeadDTO paramHeadDTO) {
		logger.info("承保信息上传转入后置处理");
		return applicationCoreService.addApplicant(body, paramHeadDTO);
	}

	@Override
	public ThirdSendResponseDTO policyReversal(ReqPolicyCancelDTO reqPolicyCancelDTO, ParamHeadDTO paramHeadDTO) {
		return applicationCoreService.policyReversal(reqPolicyCancelDTO,paramHeadDTO);
	}

}
