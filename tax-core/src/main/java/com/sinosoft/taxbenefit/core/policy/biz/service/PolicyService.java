package com.sinosoft.taxbenefit.core.policy.biz.service;

import com.sinosoft.taxbenefit.core.generated.model.TaxCont;
import com.sinosoft.taxbenefit.core.generated.model.TaxContApplyPerson;
import com.sinosoft.taxbenefit.core.generated.model.TaxContBenefitor;
import com.sinosoft.taxbenefit.core.generated.model.TaxContGroupApplyPerson;
import com.sinosoft.taxbenefit.core.generated.model.TaxContInsured;
import com.sinosoft.taxbenefit.core.generated.model.TaxContPayment;
import com.sinosoft.taxbenefit.core.generated.model.TaxContPolicyDuty;
import com.sinosoft.taxbenefit.core.generated.model.TaxContPolicyUwinfo;
import com.sinosoft.taxbenefit.core.generated.model.TaxPolicyRisk;

/**
 * 
 * @author zhanghao@sinosoft.com.cn
 *
 */
public interface PolicyService {
	/**
	 * 查询保单
	 * 
	 * @param policyNo
	 * @return TaxCont
	 */

	TaxCont queryPolicyByPolicyNo(String policyNo);

	/**
	 * 更新保单信息
	 * 
	 * @param policy
	 */
	void modifyPolicy(TaxCont policy);

	/**
	 * 添加保单信息
	 * 
	 * @param policyInfo
	 */
	void addPolicyInfo(TaxCont policyInfo);

	/**
	 * 添加保单被保人
	 */
	void addPolicyInsured(TaxContInsured insured);

	/**
	 * 添加保单投保人
	 * 
	 * @param singlePolicyHolder
	 */
	void addPolicyHolder(TaxContApplyPerson singlePolicyHolder);

	/**
	 * 添加保单受益人
	 * 
	 * @param beneficiary
	 */

	void addBeneficiary(TaxContBenefitor beneficiary);

	/**
	 * 添加团单投保信息
	 * 
	 * @param groupPolicyApply
	 */
	void addGroupPolicyApply(TaxContGroupApplyPerson groupPolicyApply);

	/**
	 * add * 保单险种
	 * 
	 * @param policyNO
	 * @return
	 */

	void addCoverage(TaxPolicyRisk PolicyRisk);

	/**
	 * 核保信息
	 * 
	 * @param underwriting
	 */
	void addUnderwriting(TaxContPolicyUwinfo underwriting);

	/**
	 * 添加险种责任
	 * 
	 * @param duty
	 */
	void addTaxContPolicyDuty(TaxContPolicyDuty duty);

	/**
	 * 添加缴费信息
	 * 
	 * @param payment
	 */
	void addpayment(TaxContPayment payment);

	/**
	 * 根据保单Id和险种编码查找保单的险种
	 * 
	 * @param riskCode
	 * @param policyId
	 * @return
	 */
	TaxPolicyRisk queryRiskInfo(String riskCode, Integer policyId);

	/**
	 * 根据保单Id和客户编码查找被保人信息
	 * 
	 * @param customerNo
	 * @param policyNo
	 * @return
	 */
	TaxContInsured queryInsuredInfo(String customerNo, Integer policyId);
	/**
	 * 根据保单Id和客户编码查找投保人信息
	 * @param customerNo
	 * @param policyNo
	 * @return
	 */
	 TaxContApplyPerson queryApplyPersonInfo(String customerNo,
				Integer policyNo) ;
	 /**
	  * 根据保单Id和客户编码查找团体投保信息
	  * @param customerNo
	  * @param policyNo
	  * @return
	  */
	 TaxContGroupApplyPerson queryGroupApply(String customerNo,
				Integer policyNo);
	 /**
	  * 根据保单Id和客户身份证号码查找受益人信息
	  * @param cardNO
	  * @param policyNo
	  * @return
	  */
	 TaxContBenefitor queryBeneficiary(String cardNO, Integer policyNo);
	 
	 /**
	  * 根据责任编码和险种id查找险种责任信息
	  * @param dutyCode
	  * @param riskId
	  * @return
	  */
	 TaxContPolicyDuty queryDutyInfo(String dutyCode, Integer riskId);
	 
	 /**
	  * 更新保单被保人信息 
	  * @param insured
	  */
	 void modifyPolicyInsured(TaxContInsured insured);
	 
	 /**
	  * 更新保单受益人
	  * @param beneficiary
	  */
	 void modifyPolicyBeneficicary(TaxContBenefitor beneficiary);
	 /**
	  * 更新个人投保信息
	  * @param singlePolicyHolder
	  */
	 void modifyPolicyHolder(TaxContApplyPerson singlePolicyHolder);
	 
	 /**
	  * 更新团体投保信息
	  * @param groupPolicyHolder
	  */
	 void modifyGroupPolicyApply(TaxContGroupApplyPerson groupPolicyHolder);
	 
	 /**
	  * 更新保单的险种信息
	  * @param polciyRisk
	  */
	 void modifyCoverage(TaxPolicyRisk polciyRisk);
	 /**
	  * 查找险种核保信息
	  * @param policyNo
	  * @return
	  */
	 TaxContPolicyUwinfo queryUnderwriting(Integer policyNo);
	 
	 /**
	  * 更新险种核保信息
	  * @param underwriting
	  */
	 void	 modifyUnderwriting(TaxContPolicyUwinfo underwriting);
	 
	 /**
	  * 更新保单责任信息
	  */
	 void modifyLiability(TaxContPolicyDuty liability);
	 
	 /**
	  * 添加保单责任信息
	  */
	 void addLiability(TaxContPolicyDuty liability);
}
