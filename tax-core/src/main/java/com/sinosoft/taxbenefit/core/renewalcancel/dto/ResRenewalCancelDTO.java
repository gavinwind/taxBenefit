package com.sinosoft.taxbenefit.core.renewalcancel.dto;

import com.sinosoft.taxbenefit.api.base.EBResultCodeDTO;
/**
 * 中保信-续保撤销上传返回
 * @author yangdongkai@outlook.com
 * @date 2016年3月22日 下午3:18:12
 */
public class ResRenewalCancelDTO {
	//续保批单号
	private String renewalEndorsementNo;
	//续保撤销确认编码
	private String renewalCancelSequenceNo;
	//返回结果
	private EBResultCodeDTO result;
	/**
	 * @return the renewalEndorsementNo
	 */
	public String getRenewalEndorsementNo() {
		return renewalEndorsementNo;
	}
	/**
	 * @param renewalEndorsementNo the renewalEndorsementNo to set
	 */
	public void setRenewalEndorsementNo(String renewalEndorsementNo) {
		this.renewalEndorsementNo = renewalEndorsementNo;
	}
	/**
	 * @return the renewalCancelSequenceNo
	 */
	public String getRenewalCancelSequenceNo() {
		return renewalCancelSequenceNo;
	}
	/**
	 * @param renewalCancelSequenceNo the renewalCancelSequenceNo to set
	 */
	public void setRenewalCancelSequenceNo(String renewalCancelSequenceNo) {
		this.renewalCancelSequenceNo = renewalCancelSequenceNo;
	}
	/**
	 * @return the result
	 */
	public EBResultCodeDTO getResult() {
		return result;
	}
	/**
	 * @param result the result to set
	 */
	public void setResult(EBResultCodeDTO result) {
		this.result = result;
	}
}