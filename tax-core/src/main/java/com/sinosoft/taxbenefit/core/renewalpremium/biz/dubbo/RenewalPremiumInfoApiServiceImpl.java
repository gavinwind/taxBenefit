package com.sinosoft.taxbenefit.core.renewalpremium.biz.dubbo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqMPremiumInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqSPremiumInfoDTO;
import com.sinosoft.taxbenefit.api.inter.RenewalPremiumInfoApiService;
import com.sinosoft.taxbenefit.core.renewalpremium.biz.service.PremiumCoreService;

/**
 * Title:续期保费上传服务接口实现
 * @author yangdongkai@outlook.com
 * @date 2016年3月2日 下午1:20:08
 */
@Service("renewalPremiumInfoApiServiceImpl")
public class RenewalPremiumInfoApiServiceImpl extends TaxBaseService implements RenewalPremiumInfoApiService{
	@Autowired
	private PremiumCoreService premiumService;

	@Override
	public ThirdSendResponseDTO renewalPremium(ReqSPremiumInfoDTO premiumList,ParamHeadDTO paramHead) {
		return premiumService.premiumSettlement(premiumList,paramHead);

	}

	@Override
	public ThirdSendResponseDTO renewalMPremium(ReqMPremiumInfoDTO premiumList,ParamHeadDTO paramHead) {
		return premiumService.premiumMSettlement(premiumList, paramHead);
	}
}