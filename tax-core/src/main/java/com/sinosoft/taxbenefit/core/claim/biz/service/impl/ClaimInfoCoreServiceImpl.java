package com.sinosoft.taxbenefit.core.claim.biz.service.impl;

import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.platform.common.config.SystemConstants;
import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.platform.common.util.DateUtil;
import com.sinosoft.platform.common.util.ReflectionUtil;
import com.sinosoft.platform.common.util.StringUtil;
import com.sinosoft.taxbenefit.api.base.EBResultCodeDTO;
import com.sinosoft.taxbenefit.api.base.HXResultCodeDTO;
import com.sinosoft.taxbenefit.api.config.ENUM_BZ_RESPONSE_RESULT;
import com.sinosoft.taxbenefit.api.config.ENUM_DISPOSE_TYPE;
import com.sinosoft.taxbenefit.api.config.ENUM_SENDSERVICE_CODE;
import com.sinosoft.taxbenefit.api.dto.request.TaxFaulTaskDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.CoreRequestHead;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimCoreMDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimCoveragePaymentInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimDetailInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimEventInfoLDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimItemInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimLiabilityPaymentInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimOperationInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimPolicyInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimReceiptFeeInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimReceiptInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimReversalDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimReversalInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimWesternMedInfoDTO;
import com.sinosoft.taxbenefit.api.dto.response.claim.ResultClaimReversalDTO;
import com.sinosoft.taxbenefit.api.dto.response.claim.ResultInfoDTO;
import com.sinosoft.taxbenefit.core.claim.biz.service.ClaimInfoCoreService;
import com.sinosoft.taxbenefit.core.claim.config.ENUM_CLAIM_STATE;
import com.sinosoft.taxbenefit.core.claim.dto.ClaimCoreDTO;
import com.sinosoft.taxbenefit.core.claim.dto.ClaimCoveragePaymentInfoDTO;
import com.sinosoft.taxbenefit.core.claim.dto.ClaimDetailInfoDTO;
import com.sinosoft.taxbenefit.core.claim.dto.ClaimEventInfoLDTO;
import com.sinosoft.taxbenefit.core.claim.dto.ClaimInfoDTO;
import com.sinosoft.taxbenefit.core.claim.dto.ClaimItemInfoDTO;
import com.sinosoft.taxbenefit.core.claim.dto.ClaimLiabilityPaymentInfoDTO;
import com.sinosoft.taxbenefit.core.claim.dto.ClaimOperationInfoDTO;
import com.sinosoft.taxbenefit.core.claim.dto.ClaimPolicyInfoDTO;
import com.sinosoft.taxbenefit.core.claim.dto.ClaimReceiptFeeInfoDTO;
import com.sinosoft.taxbenefit.core.claim.dto.ClaimReceiptInfoDTO;
import com.sinosoft.taxbenefit.core.claim.dto.ClaimResultDTO;
import com.sinosoft.taxbenefit.core.claim.dto.ClaimResultInfo;
import com.sinosoft.taxbenefit.core.claim.dto.ClaimReversalDTO;
import com.sinosoft.taxbenefit.core.claim.dto.ClaimReversalInfoDTO;
import com.sinosoft.taxbenefit.core.claim.dto.ClaimReversalResultInfoDTO;
import com.sinosoft.taxbenefit.core.claim.dto.ClaimWesternMedInfoDTO;
import com.sinosoft.taxbenefit.core.common.biz.service.CommonCodeService;
import com.sinosoft.taxbenefit.core.common.config.ENUM_BUSINESS_RESULR;
import com.sinosoft.taxbenefit.core.common.config.ENUM_EBAO_SERVICE_PORT_CODE;
import com.sinosoft.taxbenefit.core.common.config.ENUM_HX_SERVICE_PORT_CODE;
import com.sinosoft.taxbenefit.core.common.config.ENUM_RC_STATE;
import com.sinosoft.taxbenefit.core.common.config.ENUM_RESULT_CODE;
import com.sinosoft.taxbenefit.core.common.config.ENUUM_CODE_MAPPING;
import com.sinosoft.taxbenefit.core.common.dto.BookSequenceDTO;
import com.sinosoft.taxbenefit.core.common.dto.EbaoServerPortDTO;
import com.sinosoft.taxbenefit.core.common.dto.ReqBusinessDTO;
import com.sinosoft.taxbenefit.core.common.dto.ReqBusinessDetailDTO;
import com.sinosoft.taxbenefit.core.common.dto.SendLogDTO;
import com.sinosoft.taxbenefit.core.common.dto.ThirdSendDTO;
import com.sinosoft.taxbenefit.core.ebaowebservice.AsynResultQueryService;
import com.sinosoft.taxbenefit.core.ebaowebservice.AsynResultQueryService_Service;
import com.sinosoft.taxbenefit.core.ebaowebservice.ClaimReversalService;
import com.sinosoft.taxbenefit.core.ebaowebservice.ClaimReversalService_Service;
import com.sinosoft.taxbenefit.core.ebaowebservice.ClaimSettlementService;
import com.sinosoft.taxbenefit.core.ebaowebservice.ClaimSettlementService_Service;
import com.sinosoft.taxbenefit.core.ebaowebservice.ResponseBody;
import com.sinosoft.taxbenefit.core.ebaowebservice.ResponseHeader;
import com.sinosoft.taxbenefit.core.ebaowebservice.ResponseJSON;
import com.sinosoft.taxbenefit.core.faultTask.biz.service.FaultTaskService;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxClaimContDutyPolicyMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxClaimContMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxClaimContPolicyMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxContClaimMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxFaultTaskMapper;
import com.sinosoft.taxbenefit.core.generated.model.TaxClaimCont;
import com.sinosoft.taxbenefit.core.generated.model.TaxClaimContDutyPolicy;
import com.sinosoft.taxbenefit.core.generated.model.TaxClaimContDutyPolicyExample;
import com.sinosoft.taxbenefit.core.generated.model.TaxClaimContExample;
import com.sinosoft.taxbenefit.core.generated.model.TaxClaimContPolicy;
import com.sinosoft.taxbenefit.core.generated.model.TaxClaimContPolicyExample;
import com.sinosoft.taxbenefit.core.generated.model.TaxCont;
import com.sinosoft.taxbenefit.core.generated.model.TaxContClaim;
import com.sinosoft.taxbenefit.core.generated.model.TaxContClaimExample;
import com.sinosoft.taxbenefit.core.generated.model.TaxFaultTask;
import com.sinosoft.taxbenefit.core.policy.biz.service.PolicyService;
import com.sinosoft.taxbenefit.core.transCode.biz.service.TransCodeService;

/**
 * 理赔接口(后置)实现类
 * @author zhangke
 */
@Service
public class ClaimInfoCoreServiceImpl extends TaxBaseService implements ClaimInfoCoreService {
	@Autowired
	TaxContClaimMapper taxContClaimMapper;// 理赔赔案Mapper
	@Autowired
	TaxClaimContMapper taxClaimContMapper;// 理赔保单Mapper
	@Autowired
	TaxClaimContPolicyMapper taxClaimContPolicyMapper;// 理赔保单险种Mapper
	@Autowired
	TaxClaimContDutyPolicyMapper taxClaimContDutyPolicyMapper;// 理赔保单险种责任Mapper
	@Autowired
	FaultTaskService faultTaskService;// 任务日志Service
	@Autowired
	TaxFaultTaskMapper  taxFaultTaskMapper;//异常任务
	@Autowired
	CommonCodeService commonCodeService;// 公用Service
	@Autowired
	PolicyService policyService;// 保单Service
	@Autowired
	TransCodeService transCodeService;//转码Service
	@Override
	public ThirdSendResponseDTO addClaimSettlement(List<ReqClaimInfoDTO> reqClaimInfoDTOs, ParamHeadDTO paramHeadDTO) {	
		ThirdSendResponseDTO thirdSendResponse = new ThirdSendResponseDTO();// 响应核心的报文内容	
		List<ReqBusinessDetailDTO> reqBusinessDetailList = new ArrayList<ReqBusinessDetailDTO>();
		String flag="";
		try {
			ReqBusinessDTO businessDTO = commonCodeService.initReqBusinessDTO();
			flag = paramHeadDTO.getPortCode().equals(ENUM_HX_SERVICE_PORT_CODE.CLAIM_SETTLEMEN_ONE.code()) ? ENUM_HX_SERVICE_PORT_CODE.CLAIM_SETTLEMEN_ONE.desc() : ENUM_HX_SERVICE_PORT_CODE.CLAIM_SETTLEMEN_MORE.desc();
			logger.info(flag + "理赔上传（核心请求）JSON 体数据内容："+ JSONObject.toJSONString(reqClaimInfoDTOs));
			// 1.将核心请求理赔dto转换为请求中保信理赔dto
			List<ClaimInfoDTO> claimInfoDTOList = this.createClaimInfoDTOList(reqClaimInfoDTOs);
			ClaimCoreDTO claimCoreDTO=new ClaimCoreDTO();
			claimCoreDTO.setClaimList(claimInfoDTOList);
			String portCode = ENUM_EBAO_SERVICE_PORT_CODE.CLAIM_SETTLEMEN.code();
			//创建请求中保信DTO对象
			ThirdSendDTO thirdSendDTO = commonCodeService.createThirdSend(portCode, paramHeadDTO, claimCoreDTO);
			logger.info(flag+"理赔上传（请求中保信）JSON 数据内容："+JSONObject.toJSONString(thirdSendDTO));
			Holder<ResponseHeader> responseHeaderHolder = new Holder<ResponseHeader>();
			Holder<ResponseBody> responseBodyHolder = new Holder<ResponseBody>();
			// 2.调用中保信接口服务,获得返回结果 存储调用中保信日志表
			EbaoServerPortDTO ebaoServerPort = commonCodeService.queryEBaoServerInfoByPortCode(ENUM_EBAO_SERVICE_PORT_CODE.CLAIM_SETTLEMEN.code());
			String portUrl = ebaoServerPort.getPortUrl();
			URL wsdlLocation = new URL(portUrl);
			ClaimSettlementService_Service service = new ClaimSettlementService_Service(wsdlLocation);
			ClaimSettlementService servicePort = service.getClaimSettlementServicePort();
			BindingProvider bp = (BindingProvider) servicePort;
			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, portUrl);
			servicePort.settlement(thirdSendDTO.getHead(),thirdSendDTO.getBody(), responseHeaderHolder,responseBodyHolder);
			ResponseHeader responseHeader = responseHeaderHolder.value;
			ResponseBody responseBody = responseBodyHolder.value;
			logger.info(flag+"理赔申请上传（中保信响应）JSON 数据内容："+responseBody.getJsonString());
			// 封装中保信返回报文
			ResponseJSON responseJSON = new ResponseJSON();
			responseJSON.setResponseBody(responseBody);
			responseJSON.setResponseHeader(responseHeader);
			// 封装发送报文日志对象
			SendLogDTO sendLog = new SendLogDTO();
			sendLog.setSerialNo(paramHeadDTO.getSerialNo());
			sendLog.setPortCode(portCode);
			sendLog.setRequestContent(JSONObject.toJSONString(thirdSendDTO));
			sendLog.setResponseContent(JSONObject.toJSONString(responseJSON));
			sendLog.setTargetCode(ENUM_SENDSERVICE_CODE.EBAO.code());
			sendLog.setState(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
			sendLog.setDescription(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
			commonCodeService.addLogSend(sendLog);		
			// 3.封装返回核心dto;
			if (ENUM_RESULT_CODE.SUCCESS.code().equals(responseHeader.getResultCode())
					|| ENUM_RESULT_CODE.INPUT_LOCALITY_SUCCESS.code().equals(responseHeader.getResultCode())) {
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
				thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
				//获取中保信返回的理赔结果
				ClaimResultDTO claimResultDTO = JSONObject.parseObject(responseBody.getJsonString(), ClaimResultDTO.class);
				List<ClaimResultInfo> claimResultInfos = claimResultDTO.getClaimResultList();				
				// 4.存储业务表数据
				addOrModifyClaim(reqClaimInfoDTOs,claimResultInfos);
			}else {
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
				if(this.checkList(responseHeader.getResultMessage().getMessage())){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : responseHeader.getResultMessage().getMessage()){
						sbStr.append(mess).append("|");
					}
					String message = sbStr.substring(0, sbStr.length()-1).toString();
					thirdSendResponse.setResultDesc(message);
				}else{
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
				}
			}
			boolean isRegisterFaulTask = false;
			if(!StringUtil.isEmpty(responseBody.getJsonString())){
				ClaimResultDTO claimResultDTO = JSONObject.parseObject(responseBody.getJsonString(), ClaimResultDTO.class);
				if(null != claimResultDTO.getClaimResultList()){					
					List<ClaimResultInfo> claimResultInfos = claimResultDTO.getClaimResultList();
					List<ResultInfoDTO> resultInfoDTOs = new ArrayList<ResultInfoDTO>();
					ResultInfoDTO resultInfoDTO = null;
					// 成功记录数
					Integer successNum = 0;
					// 失败记录数
					Integer failNum = 0;
					//循环中保信返回信息DTO
					for (ClaimResultInfo resultInfo : claimResultInfos) {
						//循环理赔信息上传请求DTO对象
						for (ReqClaimInfoDTO reqClaimInfoDTO : reqClaimInfoDTOs){		
							if(resultInfo.getClaimNo().equals(reqClaimInfoDTO.getClaimNo())){
								String message = "";
								resultInfoDTO=new ResultInfoDTO();
								EBResultCodeDTO eBResultCode = resultInfo.getResult();
								HXResultCodeDTO hxResultCode = new HXResultCodeDTO();
								hxResultCode.setResultCode(eBResultCode.getResultCode());
								if(null != eBResultCode.getResultMessage()){
									StringBuffer sbStr = new StringBuffer(); 
									for(String mess : eBResultCode.getResultMessage()){
										sbStr.append(mess).append("|");
									}
									message = sbStr.substring(0, sbStr.length()-1).toString();
								}
								resultInfoDTO.setBizNo(reqClaimInfoDTO.getBizNo());
								resultInfoDTO.setClaimCodeP(resultInfo.getClaimCodeP());
								hxResultCode.setResultMessage(message);
								resultInfoDTO.setResult(hxResultCode);
								resultInfoDTOs.add(resultInfoDTO);
								//记录成功和失败记录数
								if(ENUM_RESULT_CODE.SUCCESS.code().equals(eBResultCode.getResultCode())){
									successNum+=1;
								}else{
									failNum+=1;
									ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHeadDTO);
									reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
									reqBusinessDetailDTO.setResultMessage(message);
									reqBusinessDetailList.add(reqBusinessDetailDTO);
								}
							}	
						}			
					}	
					// 登记请求记录
					businessDTO.setReqSuccessNum(successNum);
					businessDTO.setReqFailNum(failNum);		
					//多笔上传返回List
					if(ENUM_HX_SERVICE_PORT_CODE.CLAIM_SETTLEMEN_ONE.desc().equals(paramHeadDTO.getPortCode())){
						thirdSendResponse.setResJson(resultInfoDTOs);
					}else if(ENUM_HX_SERVICE_PORT_CODE.CLAIM_SETTLEMEN_MORE.code().equals(paramHeadDTO.getPortCode())){
						thirdSendResponse.setResJson(resultInfoDTOs.get(0));
					}
					isRegisterFaulTask = true;
				}else{
					BookSequenceDTO bookSequence = JSONObject.parseObject(responseBody.getJsonString(), BookSequenceDTO.class);
					TaxFaulTaskDTO taxFaultTask = new TaxFaulTaskDTO();
					taxFaultTask.setSerialNo(paramHeadDTO.getSerialNo());
					taxFaultTask.setServerPortCode(paramHeadDTO.getPortCode());
					taxFaultTask.setServerPortName(paramHeadDTO.getPortName());
					taxFaultTask.setAreaCode(paramHeadDTO.getAreaCode());
					taxFaultTask.setDisposeType(ENUM_DISPOSE_TYPE.APPOINTMENT.code());
					taxFaultTask.setBookSequenceNo(bookSequence.getBookingSequenceNo());
					faultTaskService.addTaxFaultTask(taxFaultTask);
					thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
				}
			}else{
				businessDTO.setReqFailNum(responseHeader.getRecordNum());
				for (int i = 0; i < responseHeader.getRecordNum(); i++) {
					ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHeadDTO);
					reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
					reqBusinessDetailDTO.setResultMessage(thirdSendResponse.getResultDesc());
					reqBusinessDetailList.add(reqBusinessDetailDTO);
				}
			}
			if(isRegisterFaulTask){
				commonCodeService.addReqBusinessDetail(reqBusinessDetailList);
				businessDTO.setRequetNum(responseHeader.getRecordNum());
				commonCodeService.addReqBusinessCollate(businessDTO);
			}
		} catch (Exception e) {
			logger.error(flag+"理赔申请上传后置处理发生异常!异常信息为"+e.getMessage());
			thirdSendResponse.setResJson(e.getMessage());
			thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
			thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
			// 存储任务表
			TaxFaulTaskDTO taxFaultTask = new TaxFaulTaskDTO();
			taxFaultTask.setServerPortCode(paramHeadDTO.getPortCode());
			taxFaultTask.setServerPortName(paramHeadDTO.getPortName());
			taxFaultTask.setAreaCode(paramHeadDTO.getAreaCode());
			taxFaultTask.setSerialNo(paramHeadDTO.getSerialNo());
			taxFaultTask.setDisposeType(ENUM_DISPOSE_TYPE.ERROR.code());
			faultTaskService.addTaxFaultTask(taxFaultTask);
			e.printStackTrace();
		}
		logger.info(flag+"理赔申请上传后置处理结束!");
		return thirdSendResponse;
	}
	
	/**
	 * 添加或修改理赔赔案信息
	 * @param reqClaimInfoDTOs
	 * @param claimResultInfo 中保信返回的理赔结果
	 */
	private void addOrModifyClaim(List<ReqClaimInfoDTO> reqClaimInfoDTOs,List<ClaimResultInfo> claimResultInfos) {
		TaxContClaim contClaim = null;// 理赔赔案信息
		TaxClaimCont claimCont = null;// 理赔保单信息
 		TaxClaimContPolicy claimContPolicy = null;// 理赔保单险种信息
		TaxClaimContDutyPolicy taxClaimContDutyPolicy = null;// 理赔保单险种责任信息
		Integer claimId = null;// 理赔赔案sId
		Integer policyId = null;// 保单sId
		Integer claimContId = null;// 理赔保单sid
		Integer claimPolicyid = null;// 理赔保单险种sid	
		try {
			//循环理赔结果
			for (ClaimResultInfo claimResultInfo : claimResultInfos) {	
				String resultCode=claimResultInfo.getResult().getResultCode();	
				//判断中保信理赔赔案返回结果
				if(ENUM_RESULT_CODE.SUCCESS.code().equals(resultCode)) {	
					// 循环(核心)理赔案件DTO	
					for (ReqClaimInfoDTO reqClaimInfoDTO : reqClaimInfoDTOs) {
						//判断理赔案件理赔号
						if(claimResultInfo.getClaimNo().equals(reqClaimInfoDTO.getClaimNo())){				
							contClaim = initContClaim(reqClaimInfoDTO);
							TaxContClaim claim = this.queryClaimInfoByClaimNo(contClaim.getClaimNo());
							if (claim == null) {
								contClaim.setCreateDate(DateUtil.getCurrentDate());
								contClaim.setCreatorId(SystemConstants.SYS_OPERATOR);
								contClaim.setModifierId(SystemConstants.SYS_OPERATOR);
								contClaim.setCreateDate(DateUtil.getCurrentDate());
								contClaim.setRcState(ENUM_RC_STATE.EFFECTIVE.getCode());
								this.addNewClaimInfo(contClaim);
								claimId = contClaim.getSid();
							} else {					
								contClaim.setModifyDate(DateUtil.getCurrentDate());
								contClaim.setModifierId(SystemConstants.SYS_OPERATOR);
								contClaim.setRcState(ENUM_RC_STATE.EFFECTIVE.getCode());
								claimId = claim.getSid();
								contClaim.setSid(claimId);
								this.modifyClaimInfo(contClaim);
								//将理赔案件下的所有理赔保单的状态设置都先设置为无效
								this.changTaxClaimCont(claimId);
							}
							// 循环(核心)理赔赔案DTO下的理赔保单DTO集合
							for (ReqClaimPolicyInfoDTO reqClaimPolicyInfoDTO : reqClaimInfoDTO.getPolicyList()) {
								TaxCont taxCont=policyService.queryPolicyByPolicyNo(reqClaimPolicyInfoDTO.getPolicyNo());
								if(taxCont==null){
									policyId = null;
								}else{
									policyId=taxCont.getSid();
								}			
								claimCont = this.initClaimCont(reqClaimPolicyInfoDTO,claimId, policyId);
								TaxClaimCont policy = this.queryClaimContByNo(claimCont.getContNo(), claimId);
								if (null == policy) {
									claimCont.setCreateDate(DateUtil.getCurrentDate());
									claimCont.setCreatorId(SystemConstants.SYS_OPERATOR);
									claimCont.setModifyDate(DateUtil.getCurrentDate());
									claimCont.setModifierId(SystemConstants.SYS_OPERATOR);
									claimCont.setRcState(ENUM_RC_STATE.EFFECTIVE.getCode());
									this.addClaimCont(claimCont);
									claimContId = claimCont.getSid();
								} else {
									claimCont.setModifierId(SystemConstants.SYS_OPERATOR);
									claimCont.setModifyDate(DateUtil.getCurrentDate());
									claimContId = policy.getSid();
									claimCont.setSid(claimContId);
									claimCont.setRcState(ENUM_RC_STATE.EFFECTIVE.getCode());
									this.modifyClaimCont(claimCont);
									//理赔保单下的所有险种的状态都先设置为无效
									this.changTaxClaimContPolicy(claimContId);
								}
								// 循环(核心)理赔保单DTO下的理赔保单险种信息DTO集合
								for (ReqClaimCoveragePaymentInfoDTO claimCoveragePaymentInfoDTO : reqClaimPolicyInfoDTO.getCoveragePaymentList()) {
									claimContPolicy = this.initClaimContPolicy(claimCoveragePaymentInfoDTO, claimContId);
									TaxClaimContPolicy taxClaimContPolicy = this.queryTaxClaimContPolicy(claimContId,claimCoveragePaymentInfoDTO.getComCoverageCode());
									if (null == taxClaimContPolicy) {
										claimContPolicy.setCreateDate(DateUtil.getCurrentDate());
										claimContPolicy.setCreatorId(SystemConstants.SYS_OPERATOR);
										claimContPolicy.setModifierId(SystemConstants.SYS_OPERATOR);
										claimContPolicy.setModifyDate(DateUtil.getCurrentDate());
										claimContPolicy.setRcState(ENUM_RC_STATE.EFFECTIVE.getCode());
										this.addClaimContPolicy(claimContPolicy);
										claimPolicyid = claimContPolicy.getSid();
									} else {			
										claimContPolicy.setModifierId(SystemConstants.SYS_OPERATOR);
										claimContPolicy.setModifyDate(DateUtil.getCurrentDate());
										claimPolicyid = taxClaimContPolicy.getSid();
										claimContPolicy.setSid(claimPolicyid);
										claimContPolicy.setRcState(ENUM_RC_STATE.EFFECTIVE.getCode());
										this.modifyClaimContPolicy(claimContPolicy);
										//理赔险种下的所有责任的状态都先设置为无效
										this.changTaxClaimContDutyPolicy(claimPolicyid);
									}
									// 循环(核心)理赔保单险种信息DTO下的理赔保单险种责任DTO集合
									for (ReqClaimLiabilityPaymentInfoDTO claimLiabilityPaymentInfoDTO : claimCoveragePaymentInfoDTO.getLiabilityPaymentList()) {
										taxClaimContDutyPolicy = this.initClaimContDutyPolicy(claimLiabilityPaymentInfoDTO,claimPolicyid);
										TaxClaimContDutyPolicy claimContDutyPolicy = this.queryTaxClaimContDutyPolicy(claimPolicyid,claimLiabilityPaymentInfoDTO.getLiabilityCode());
										if (claimContDutyPolicy == null) {
											taxClaimContDutyPolicy.setCreateDate(DateUtil.getCurrentDate());
											taxClaimContDutyPolicy.setCreatorId(SystemConstants.SYS_OPERATOR);
											taxClaimContDutyPolicy.setModifierId(SystemConstants.SYS_OPERATOR);
											taxClaimContDutyPolicy.setModifyDate(DateUtil.getCurrentDate());
											taxClaimContDutyPolicy.setRcState(ENUM_RC_STATE.EFFECTIVE.getCode());
											this.addClaimContDutyPolicy(taxClaimContDutyPolicy);
										} else {
											taxClaimContDutyPolicy.setModifierId(SystemConstants.SYS_OPERATOR);
											taxClaimContDutyPolicy.setModifyDate(DateUtil.getCurrentDate());
											taxClaimContDutyPolicy.setRcState(ENUM_RC_STATE.EFFECTIVE.getCode());
											taxClaimContDutyPolicy.setSid(claimContDutyPolicy.getSid());
											this.modifyClaimContDutyPolicy(taxClaimContDutyPolicy);
										}
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("理赔上传数据持久化失败"+e.getMessage());
		}	
	}

	/**
	 * 修改理赔险种下的所有责任的记录状态为无效
	 * @param claimPolicyid 理赔险种id
	 */
	private void changTaxClaimContDutyPolicy(Integer claimPolicyid) {
		TaxClaimContDutyPolicy taxClaimContDutyPolicy=new TaxClaimContDutyPolicy();
		taxClaimContDutyPolicy.setRcState(ENUM_RC_STATE.INVALID.getCode());
		TaxClaimContDutyPolicyExample example=new TaxClaimContDutyPolicyExample();
		example.createCriteria().andClaimPolicyIdEqualTo(claimPolicyid);
		taxClaimContDutyPolicyMapper.updateByExampleSelective(taxClaimContDutyPolicy, example);
	}

	/**
	 * 修改理赔保单下的所有险种记录状态为无效
	 * @param claimContId 理赔保单id
	 */
	private void changTaxClaimContPolicy(Integer claimContId) {
		TaxClaimContPolicy taxClaimContPolicy=new TaxClaimContPolicy();
		taxClaimContPolicy.setRcState(ENUM_RC_STATE.INVALID.getCode());
		TaxClaimContPolicyExample example=new TaxClaimContPolicyExample();
		example.createCriteria().andClaimContIdEqualTo(claimContId);
		taxClaimContPolicyMapper.updateByExampleSelective(taxClaimContPolicy, example);	
	}

	/**
	 * 根据赔案id修改该赔案下的保单记录状态为无效
	 * @param claimId 赔案id
	 */
	private void changTaxClaimCont(Integer claimId) {
		TaxClaimCont policy =new TaxClaimCont();
		policy.setRcState(ENUM_RC_STATE.INVALID.getCode());
		TaxClaimContExample example=new TaxClaimContExample();
		example.createCriteria().andClaimIdEqualTo(claimId);
		taxClaimContMapper.updateByExampleSelective(policy, example);	
	}

	@Override
	public ThirdSendResponseDTO modifyClaimState(ReqClaimReversalInfoDTO bodyDTO, ParamHeadDTO paramHeadDTO) {
		ThirdSendResponseDTO thirdSendResponse = new ThirdSendResponseDTO();// 响应核心的报文内容
		List<ReqBusinessDetailDTO> reqBusinessDetailList = new ArrayList<ReqBusinessDetailDTO>();
		try {
			ReqBusinessDTO businessDTO = commonCodeService.initReqBusinessDTO();	
			logger.info("理赔案件撤销（核心请求）JSON 体数据内容："+ JSONObject.toJSONString(bodyDTO));
			ClaimReversalInfoDTO claimReversal=this.createClaimReversalDTO(bodyDTO);
			ClaimReversalDTO claimReversalDTO=new ClaimReversalDTO();
			claimReversalDTO.setClaim(claimReversal);
			String portCode =ENUM_EBAO_SERVICE_PORT_CODE.CLAIM_REVERSAL.code();
			//创建请求中保信DTO对象
			ThirdSendDTO thirdSendDTO = commonCodeService.createThirdSend(portCode, paramHeadDTO, claimReversalDTO);
			logger.info("理赔案件撤销（请求中保信）JSON 数据内容："+JSONObject.toJSONString(thirdSendDTO));
			Holder<ResponseHeader> responseHeaderHolder = new Holder<ResponseHeader>();
			Holder<ResponseBody> responseBodyHolder = new Holder<ResponseBody>();
			// 2.调用中保信接口服务,获得返回结果 存储调用中保信日志表
			EbaoServerPortDTO ebaoServerPort = commonCodeService.queryEBaoServerInfoByPortCode(ENUM_EBAO_SERVICE_PORT_CODE.CLAIM_REVERSAL.code());
			String portUrl = ebaoServerPort.getPortUrl();
			URL wsdlLocation = new URL(portUrl);
			ClaimReversalService_Service service = new ClaimReversalService_Service(wsdlLocation);
			ClaimReversalService servicePort = service.getClaimReversalServicePort();
			BindingProvider bp = (BindingProvider) servicePort;
			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, portUrl);
			servicePort.reversal(thirdSendDTO.getHead(),thirdSendDTO.getBody(), responseHeaderHolder,responseBodyHolder);
			ResponseHeader responseHeader = responseHeaderHolder.value;
			ResponseBody responseBody = responseBodyHolder.value;
			logger.info("理赔案件撤销上传（中保信响应）JSON 数据内容："+responseBody.getJsonString());
			// 封装中保信返回报文
			ResponseJSON responseJSON = new ResponseJSON();
			responseJSON.setResponseBody(responseBody);
			responseJSON.setResponseHeader(responseHeader);
			// 封装发送报文日志对象
			SendLogDTO sendLog = new SendLogDTO();
			sendLog.setSerialNo(paramHeadDTO.getSerialNo());
			sendLog.setPortCode(portCode);
			sendLog.setRequestContent(JSONObject.toJSONString(thirdSendDTO));
			sendLog.setResponseContent(JSONObject.toJSONString(responseJSON));
			sendLog.setTargetCode(ENUM_SENDSERVICE_CODE.EBAO.code());
			sendLog.setState(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
			sendLog.setDescription(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
			commonCodeService.addLogSend(sendLog);
			thirdSendResponse.setResJson(null);
			if (ENUM_RESULT_CODE.SUCCESS.code().equals(responseHeader.getResultCode())
					|| ENUM_RESULT_CODE.INPUT_LOCALITY_SUCCESS.code().equals(responseHeader.getResultCode())) {
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
				thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
				//修改理赔案件状态
				this.modifyClaimState(claimReversal);		
			}else{
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
				List list = responseHeader.getResultMessage().getMessage();
				if(list != null && list.size() > 0){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : responseHeader.getResultMessage().getMessage()){
						sbStr.append(mess).append("|");
					}
					String message = sbStr.substring(0, sbStr.length()-1).toString();
					thirdSendResponse.setResultDesc(message);
				}else{
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
				}
			}
			if(!StringUtil.isEmpty(responseBody.getJsonString())){
				ClaimReversalResultInfoDTO reversalResultDTO=JSONObject.parseObject(responseBody.getJsonString(), ClaimReversalResultInfoDTO.class);
				EBResultCodeDTO eBResultCode=reversalResultDTO.getResult();
				String message = "";
				ResultClaimReversalDTO reversalDTO=new ResultClaimReversalDTO();
				HXResultCodeDTO resultDTO=new HXResultCodeDTO();
				if(null != eBResultCode.getResultMessage()){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : eBResultCode.getResultMessage()){
						sbStr.append(mess).append("|");
					}
					message = sbStr.substring(0, sbStr.length()-1).toString();
				}
				resultDTO.setResultMessage(message);
				resultDTO.setResultCode(eBResultCode.getResultCode());
				reversalDTO.setClaimResult(resultDTO);
				thirdSendResponse.setResJson(reversalDTO);
				if(ENUM_RESULT_CODE.SUCCESS.code().equals(eBResultCode.getResultCode())){
					businessDTO.setReqSuccessNum(businessDTO.getReqSuccessNum() + 1);
				}else{
					ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHeadDTO);
					reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
					reqBusinessDetailDTO.setResultMessage(message);
					reqBusinessDetailList.add(reqBusinessDetailDTO);
					businessDTO.setReqFailNum(businessDTO.getReqFailNum() + 1);
				}		
			}else{
				businessDTO.setReqFailNum(responseHeader.getRecordNum());
				ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHeadDTO);
				reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
				reqBusinessDetailDTO.setResultMessage(thirdSendResponse.getResultDesc());
				reqBusinessDetailList.add(reqBusinessDetailDTO);
			}
			// 添加失败交易记录
			commonCodeService.addReqBusinessDetail(reqBusinessDetailList);
			businessDTO.setRequetNum(responseHeader.getRecordNum());
			// 添加请求交易记录数
			commonCodeService.addReqBusinessCollate(businessDTO);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("理赔案件撤销上传后置处理发生异常!异常信息为"+e.getMessage());
			thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
			thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
			// 存储任务表
			TaxFaulTaskDTO taxFaultTask = new TaxFaulTaskDTO();
			taxFaultTask.setServerPortCode(paramHeadDTO.getPortCode());
			taxFaultTask.setServerPortName(paramHeadDTO.getPortName());
			taxFaultTask.setAreaCode(paramHeadDTO.getAreaCode());
			// taxFaultTask.setBatchBizNo("");//批次号
			taxFaultTask.setSerialNo(paramHeadDTO.getSerialNo());
			taxFaultTask.setDisposeType(ENUM_DISPOSE_TYPE.ERROR.code());
			faultTaskService.addTaxFaultTask(taxFaultTask);
		}
		logger.info("理赔案件撤销上传后置处理结束!");
		return thirdSendResponse;
	}
	
	/**
	 * 修改赔案案件状态
	 * @param claimReversal
	 */
	private void modifyClaimState(ClaimReversalInfoDTO claimReversal) {
		try {
			TaxContClaimExample example=new TaxContClaimExample();
			example.createCriteria().andClaimNoEqualTo(claimReversal.getClaimNo());
			List<TaxContClaim>list=taxContClaimMapper.selectByExample(example);
			if(this.checkList(list)){
				TaxContClaim taxContClaim=new TaxContClaim();
				taxContClaim.setSid(list.get(0).getSid());
				taxContClaim.setCaseState(ENUM_CLAIM_STATE.REPEAL.code());
				taxContClaim.setModifierId(SystemConstants.SYS_OPERATOR);
				taxContClaim.setModifyDate(DateUtil.getCurrentDate());
				taxContClaimMapper.updateByPrimaryKeySelective(taxContClaim);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("理赔撤销数据持久化失败"+e.getMessage());
		}
	}

	/**
	 * 将核心理赔撤销DTO转为中保信DTO
	 * @param reqClaimReversalDTO
	 * @return
	 */
	private ClaimReversalInfoDTO createClaimReversalDTO(ReqClaimReversalInfoDTO reqClaimReversalDTO) {
		ClaimReversalInfoDTO claimReversalInfoDTO=new ClaimReversalInfoDTO();
		ReflectionUtil.copyProperties(reqClaimReversalDTO,claimReversalInfoDTO);
		return claimReversalInfoDTO;
	}

	/**
	 * 新增理赔赔案信息
	 * @param taxContClaim
	 * @return
	 */
	private int addNewClaimInfo(TaxContClaim taxContClaim) {
		return taxContClaimMapper.insert(taxContClaim);
	}

	/**
	 * 修改理赔赔案信息
	 * @param reqClaimInfoDTO
	 */
	private void modifyClaimInfo(TaxContClaim taxContClaim) {
		taxContClaimMapper.updateByPrimaryKeySelective(taxContClaim);
	}

	/**
	 * 根据理赔号查询理赔信息
	 * 
	 * @param ClaimNO
	 * @return
	 */
	private TaxContClaim queryClaimInfoByClaimNo(String ClaimNo) {
		TaxContClaimExample example = new TaxContClaimExample();
		example.createCriteria().andClaimNoEqualTo(ClaimNo);
		List<TaxContClaim> list = taxContClaimMapper.selectByExample(example);
		if (list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	/**
	 * 新增理赔保单信息
	 * 
	 * @param claimCont
	 * @return 受影响行数
	 */
	private int addClaimCont(TaxClaimCont claimCont) {
		return taxClaimContMapper.insert(claimCont);
	}

	/**
	 * 更新理赔保单信息
	 * 
	 * @param taxContClaim
	 */
	private void modifyClaimCont(TaxClaimCont taxClaimCont) {
		taxClaimContMapper.updateByPrimaryKeySelective(taxClaimCont);
	}

	/**
	 * 根据保单号和理赔赔案Id查询理赔保单信息
	 * 
	 * @param conto 保单号
	 * @param claimId 理赔赔案Id
	 * @return
	 */
	private TaxClaimCont queryClaimContByNo(String contNo, Integer claimId) {
		TaxClaimContExample example = new TaxClaimContExample();
		example.createCriteria().andContNoEqualTo(contNo).andClaimIdEqualTo(claimId);
		List<TaxClaimCont> list = taxClaimContMapper.selectByExample(example);
		if (list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	/**
	 * 添加新的理赔保单险种信息
	 * @param claimContPolicy
	 * @return 受影响行数
	 */
	private int addClaimContPolicy(TaxClaimContPolicy claimContPolicy) {
		return taxClaimContPolicyMapper.insert(claimContPolicy);
	}
	
	/**
	 * 更新理赔保单险种信息
	 * @param claimContPolicy
	 */
	private void modifyClaimContPolicy(TaxClaimContPolicy claimContPolicy) {
		taxClaimContPolicyMapper.updateByPrimaryKeySelective(claimContPolicy);
	}
	/**
	 * 根据理赔保单id和险种编号查询理赔保单险种信息
	 * @param claimContId 理赔保单id
	 * @param riskCode 险种编号
	 * @return 理赔保单险种信息
	 */
	private TaxClaimContPolicy queryTaxClaimContPolicy(Integer claimContId,String riskCode) {
		TaxClaimContPolicyExample example = new TaxClaimContPolicyExample();
		example.createCriteria().andClaimContIdEqualTo(claimContId).andRiskCodeEqualTo(riskCode);
		List<TaxClaimContPolicy> list = taxClaimContPolicyMapper.selectByExample(example);
		if (list.size() > 0) {
			return list.get(0);
		}
		return null;
	}
	
	/**
	 * 添加新的理赔保单险种责任信息
	 * @param claimContDutyPolicy
	 * @return 受影响行数
	 */
	private int addClaimContDutyPolicy(TaxClaimContDutyPolicy claimContDutyPolicy) {
		return taxClaimContDutyPolicyMapper.insert(claimContDutyPolicy);
	}
	
	/**
	 * 更新理赔保单险种责任信息
	 * @param claimContDutyPolicy
	 */
	private void modifyClaimContDutyPolicy(
			TaxClaimContDutyPolicy claimContDutyPolicy) {
		taxClaimContDutyPolicyMapper.updateByPrimaryKeySelective(claimContDutyPolicy);
	}
	
	/**
	 * 根据理赔保单险种id和责任编码查询理赔保单险种责任信息
	 * 
	 * @param claimPolicyId  理赔保单险种id
	 * @param dutyCode 责任编码
	 * @return
	 */
	private TaxClaimContDutyPolicy queryTaxClaimContDutyPolicy(Integer claimPolicyId, String dutyCode) {
		TaxClaimContDutyPolicyExample example = new TaxClaimContDutyPolicyExample();
		example.createCriteria().andClaimPolicyIdEqualTo(claimPolicyId).andDutyCodeEqualTo(dutyCode);
		List<TaxClaimContDutyPolicy> list = taxClaimContDutyPolicyMapper.selectByExample(example);
		if (list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	/**
	 * 将核心请求的理赔信息DTO转为中保信DTO集合
	 * @param reqClaimInfoDTOs 核心请求的理赔信息DTO集合
	 * @return
	 */
	private List<ClaimInfoDTO> createClaimInfoDTOList(List<ReqClaimInfoDTO> reqClaimInfoDTOs) {
		List<ClaimInfoDTO> claimInfoDTOList = new ArrayList<ClaimInfoDTO>();
		for (ReqClaimInfoDTO reqClaimInfoDTO : reqClaimInfoDTOs) {
			ClaimInfoDTO claimInfoDTO = this.getClaimInfoDTO(reqClaimInfoDTO);
			claimInfoDTOList.add(claimInfoDTO);
		}
		return claimInfoDTOList;
	}

	/**
	 * 将核心DTO转为中保信DTO
	 * @param ReqClaimInfoDTO 核心信息DTO  1
	 * @return
	 */
	private ClaimInfoDTO getClaimInfoDTO(ReqClaimInfoDTO reqClaimInfoDTO) {
		ClaimInfoDTO claimInfoDTO = new ClaimInfoDTO();	
		//1将核心DTO转为中保信DTO
		ReflectionUtil.copyProperties(reqClaimInfoDTO, claimInfoDTO);
		// 转码操作
		claimInfoDTO.setAccidentReason(transCodeService.transCode(ENUUM_CODE_MAPPING.ACCIDENTREASON.code(), claimInfoDTO.getAccidentReason()));
		claimInfoDTO.setClaimConclusionCode(transCodeService.transCode(ENUUM_CODE_MAPPING.CLAIMCONCLUSIONCODE.code(), claimInfoDTO.getClaimConclusionCode()));
		claimInfoDTO.setStatus(transCodeService.transCode(ENUUM_CODE_MAPPING.FLOWSTATE.code(), claimInfoDTO.getStatus()));
		claimInfoDTO.setClaimantGender(transCodeService.transCode(ENUUM_CODE_MAPPING.GENDER.code(),claimInfoDTO.getClaimantGender()));
		claimInfoDTO.setClaimantCertiType(transCodeService.transCode(ENUUM_CODE_MAPPING.CARDTYPE.code(),claimInfoDTO.getClaimantCertiType()));
		//医疗事件集合
		List<ClaimEventInfoLDTO> eventList=new ArrayList<ClaimEventInfoLDTO>();
		//循环医疗事件
		if(this.checkList(reqClaimInfoDTO.getEventList())){
			for (ReqClaimEventInfoLDTO reqEvent : reqClaimInfoDTO.getEventList()) {		
				ClaimEventInfoLDTO event = new ClaimEventInfoLDTO();
				ReflectionUtil.copyProperties(reqEvent,event);
				event.setDate(reqEvent.getEventDate());//字段名不一样需要手动转
				// 医疗事件类型代码 转码操作
				event.setEventType(transCodeService.transCode(ENUUM_CODE_MAPPING.EVENTTYPE.code(), reqEvent.getEventType()));
				//手术集合
				List<ClaimOperationInfoDTO>operationList=new ArrayList<ClaimOperationInfoDTO>();
				if(this.checkList(reqEvent.getOperationList())){
					//循环手术
					for (ReqClaimOperationInfoDTO reqopClaimOperation : reqEvent.getOperationList()) {
						ClaimOperationInfoDTO operation	=new ClaimOperationInfoDTO();
						ReflectionUtil.copyProperties(reqopClaimOperation,operation);
						operationList.add(operation);
					}
				}
				//西医疾病集合
				List<ClaimWesternMedInfoDTO> westernMedInfoList=new ArrayList<ClaimWesternMedInfoDTO>();
				if(this.checkList(reqEvent.getWesternMediList())){
					//循环西医
					for (ReqClaimWesternMedInfoDTO reqwClaimWestern : reqEvent.getWesternMediList()) {
						ClaimWesternMedInfoDTO westernMedInfo=new ClaimWesternMedInfoDTO();
						ReflectionUtil.copyProperties(reqwClaimWestern, westernMedInfo);
						westernMedInfoList.add(westernMedInfo);
					}
				}
				//收据 集合
				List<ClaimReceiptInfoDTO> receiptInfoList = new ArrayList<ClaimReceiptInfoDTO>();
				if(this.checkList( reqEvent.getReceiptList())){
					//循环收据
					for (ReqClaimReceiptInfoDTO reqReceiptInfo: reqEvent.getReceiptList()) {
						ClaimReceiptInfoDTO receiptInfoDTO = new ClaimReceiptInfoDTO();
						ReflectionUtil.copyProperties(reqReceiptInfo, receiptInfoDTO);
						receiptInfoDTO.setDate(reqReceiptInfo.getOccurDate());//字段名不一样需要手动转
						//收据下的医疗费用集合
						List<ClaimItemInfoDTO>itemInfoList = new ArrayList<ClaimItemInfoDTO>();
						//循环医疗费用
						if(this.checkList(reqReceiptInfo.getItemList())){
							for (ReqClaimItemInfoDTO reqItem : reqReceiptInfo.getItemList()) {
								ClaimItemInfoDTO itemInfoDTO = new ClaimItemInfoDTO();
								ReflectionUtil.copyProperties(reqItem,itemInfoDTO);
								//医疗费用下的医疗费用明细
								List<ClaimDetailInfoDTO>detailInfoDTOList = new ArrayList<ClaimDetailInfoDTO>();	
								if(this.checkList(reqItem.getDetailList())){
									//循环医疗费用明细
									for (ReqClaimDetailInfoDTO reqDetail: reqItem.getDetailList()) {
										ClaimDetailInfoDTO detailInfoDTO = new ClaimDetailInfoDTO();
										ReflectionUtil.copyProperties(reqDetail,detailInfoDTO);
										detailInfoDTOList.add(detailInfoDTO);
									}					
								}
								itemInfoDTO.setDetailList(detailInfoDTOList);
								itemInfoDTO.setItemCategory(transCodeService.transCode(ENUUM_CODE_MAPPING.ITEMCATEGORY.code(),reqItem.getItemCategory()));
								itemInfoList.add(itemInfoDTO);
							}
						}
						//收据下收据分项费用集合
						List<ClaimReceiptFeeInfoDTO>receiptFeeInfoList=new ArrayList<ClaimReceiptFeeInfoDTO>();
						if(this.checkList(reqReceiptInfo.getReceiptFeeList())){
							//循环收据分项费用
							for (ReqClaimReceiptFeeInfoDTO feeInfoDTO : reqReceiptInfo.getReceiptFeeList()) {
								ClaimReceiptFeeInfoDTO receiptFeeInfoDTO = new ClaimReceiptFeeInfoDTO();
								ReflectionUtil.copyProperties(feeInfoDTO,receiptFeeInfoDTO);
								receiptFeeInfoDTO.setValue(feeInfoDTO.getAmount());//字段名不一样需要手动转
								receiptFeeInfoList.add(receiptFeeInfoDTO);
							}
						}		
						receiptInfoDTO.setReceiptFeeList(receiptFeeInfoList);
						receiptInfoDTO.setItemList(itemInfoList);
						receiptInfoList.add(receiptInfoDTO);		
					}
				}
				event.setReceiptList(receiptInfoList);
				event.setWesternMediList(westernMedInfoList);
				event.setOperationList(operationList);
				eventList.add(event);
			}
		}	
		//理赔保单集合
		List<ClaimPolicyInfoDTO>policyInfoList=new ArrayList<ClaimPolicyInfoDTO>();
		//循环理赔保单
		if(this.checkList(reqClaimInfoDTO.getPolicyList())){
			for (ReqClaimPolicyInfoDTO reqPolicyInfo : reqClaimInfoDTO.getPolicyList()) {
				ClaimPolicyInfoDTO policyInfo = new ClaimPolicyInfoDTO();
				ReflectionUtil.copyProperties(reqPolicyInfo,policyInfo);
				policyInfo.setPolicyStatus(transCodeService.transCode(ENUUM_CODE_MAPPING.STATE.code(), reqPolicyInfo.getPolicyStatus()));
				//险种赔付信息
				List<ClaimCoveragePaymentInfoDTO> paymentInfoDTOList = new ArrayList<ClaimCoveragePaymentInfoDTO>();
				if(this.checkList(reqPolicyInfo.getCoveragePaymentList())){
					//循环险种赔付信息
					for (ReqClaimCoveragePaymentInfoDTO reqPayment : reqPolicyInfo.getCoveragePaymentList()) {
						ClaimCoveragePaymentInfoDTO paymentInfoDTO = new ClaimCoveragePaymentInfoDTO();
						ReflectionUtil.copyProperties(reqPayment,paymentInfoDTO);
						paymentInfoDTO.setCoverageType(transCodeService.transCode(ENUUM_CODE_MAPPING.RISKTYPE.code(),reqPayment.getCoverageType()));
						paymentInfoDTO.setClaimConclusionCode(transCodeService.transCode(ENUUM_CODE_MAPPING.CLAIMCONCLUSIONCODE.code(), reqPayment.getClaimConclusionCode()));
						//责任赔付信息
						List<ClaimLiabilityPaymentInfoDTO> liabilityPaymentInfoList=new ArrayList<ClaimLiabilityPaymentInfoDTO>();
						if(this.checkList(reqPayment.getLiabilityPaymentList())){
							//循环责任赔付信息
							for (ReqClaimLiabilityPaymentInfoDTO reqliaClaimLiability : reqPayment.getLiabilityPaymentList()) {
								ClaimLiabilityPaymentInfoDTO liabilityPaymentInfoDTO = new ClaimLiabilityPaymentInfoDTO();
								ReflectionUtil.copyProperties(reqliaClaimLiability,liabilityPaymentInfoDTO);
								liabilityPaymentInfoDTO.setLiabilityType(transCodeService.transCode(ENUUM_CODE_MAPPING.LIABILITYTYPE.code(), reqliaClaimLiability.getLiabilityType()));
								liabilityPaymentInfoDTO.setClaimConclusionCode(transCodeService.transCode(ENUUM_CODE_MAPPING.CLAIMCONCLUSIONCODE.code(), reqliaClaimLiability.getClaimConclusionCode()));//责任赔付信息下的西医疾病
								List<ClaimWesternMedInfoDTO>claimWesternMedInfoDTOList=new ArrayList<ClaimWesternMedInfoDTO>();
								if(checkList(reqliaClaimLiability.getWesternMediList())){
									//循环西医
									for (ReqClaimWesternMedInfoDTO reqwestern : reqliaClaimLiability.getWesternMediList()) {
										ClaimWesternMedInfoDTO claimWesternMedInfoDTO = new ClaimWesternMedInfoDTO();
										ReflectionUtil.copyProperties(reqwestern,claimWesternMedInfoDTO);
										claimWesternMedInfoDTOList.add(claimWesternMedInfoDTO);
									}
								}
								//责任赔付信息下的手术
								List<ClaimOperationInfoDTO>claimOperationInfoDTOList=new ArrayList<ClaimOperationInfoDTO>();
								if(checkList(reqliaClaimLiability.getOperationList())){
									//循环手术
									for (ReqClaimOperationInfoDTO reqOperation : reqliaClaimLiability.getOperationList()) {
										ClaimOperationInfoDTO claimOperationInfoDTO = new ClaimOperationInfoDTO();
										ReflectionUtil.copyProperties(reqOperation,claimOperationInfoDTO);
										claimOperationInfoDTOList.add(claimOperationInfoDTO);
									}
								}
								liabilityPaymentInfoDTO.setOperationList(claimOperationInfoDTOList);
								liabilityPaymentInfoDTO.setWesternMediList(claimWesternMedInfoDTOList);
								liabilityPaymentInfoList.add(liabilityPaymentInfoDTO);
							}
						}
						paymentInfoDTO.setLiabilityPaymentList(liabilityPaymentInfoList);
						paymentInfoDTOList.add(paymentInfoDTO);
					}
				}
				policyInfo.setCoveragePaymentList(paymentInfoDTOList);
				policyInfoList.add(policyInfo);
			}
		}	
		claimInfoDTO.setPolicyList(policyInfoList);
		claimInfoDTO.setEventList(eventList);
		return claimInfoDTO;
	}

	/**
	 * 封装理赔赔案信息对象,核心请求DTO转为本系统持久化对象
	 * @param reqClaimInfoDTO 请求的理赔赔案信息DTO对象
	 * @return 
	 */
	private TaxContClaim initContClaim(ReqClaimInfoDTO reqClaimInfoDTO) {
		TaxContClaim contClaim = new TaxContClaim();
		contClaim.setAccidentDate(DateUtil.parseDate(reqClaimInfoDTO.getAccidentDate()));
		contClaim.setAccidentPlace(reqClaimInfoDTO.getAccidentPlace());
		contClaim.setAccidentReason(reqClaimInfoDTO.getAccidentReason());
		if(!StringUtil.isBlank(reqClaimInfoDTO.getClaimAmount())){
			contClaim.setActualPayAmount(new BigDecimal(reqClaimInfoDTO.getClaimAmount()));
		}
		if(!StringUtil.isBlank(reqClaimInfoDTO.getApplyAmount())){
			contClaim.setApplyAmount(new BigDecimal(reqClaimInfoDTO.getApplyAmount()));
		}
		contClaim.setApplyDate(DateUtil.parseDate(reqClaimInfoDTO.getApplyDate()));
		contClaim.setBizNo(reqClaimInfoDTO.getBizNo());
		contClaim.setCaseState(reqClaimInfoDTO.getStatus());
		contClaim.setClaimantBirthday(DateUtil.parseDate(reqClaimInfoDTO.getClaimantBirthday()));
		contClaim.setClaimantCardno(reqClaimInfoDTO.getClaimantCertiCode());
		contClaim.setClaimantCardtype(reqClaimInfoDTO.getClaimantCertiType());
		contClaim.setClaimantGender(reqClaimInfoDTO.getClaimantGender());
		contClaim.setClaimantName(reqClaimInfoDTO.getClaimantName());
		contClaim.setClaimConclusionCode(reqClaimInfoDTO.getClaimConclusionCode());
		contClaim.setClaimNo(reqClaimInfoDTO.getClaimNo());
		contClaim.setConclusionReason(reqClaimInfoDTO.getConclusionReason());
		if(!StringUtil.isEmpty(reqClaimInfoDTO.getDeathDate())){
			contClaim.setDeadDate(DateUtil.parseDate(reqClaimInfoDTO.getDeathDate()));
		}
		contClaim.setDeadFlag(reqClaimInfoDTO.getDeathIndi());
		contClaim.setEndCaseDate(DateUtil.parseDate(reqClaimInfoDTO.getEndCaseDate()));
		contClaim.setIsInvestigationFlag(reqClaimInfoDTO.getClaimInvestigation());
		contClaim.setIsNote(reqClaimInfoDTO.getPendingIndi());
		if(!StringUtil.isEmpty(reqClaimInfoDTO.getPendingFinishDate())){
			contClaim.setNoteFinishDate(DateUtil.parseDate(reqClaimInfoDTO.getPendingFinishDate()));
		}
		contClaim.setRcState(ENUM_RC_STATE.EFFECTIVE.getCode());
		contClaim.setRegistrationDate(DateUtil.parseDate(reqClaimInfoDTO.getRegistrationDate()));
		if(!StringUtil.isEmpty(reqClaimInfoDTO.getReportDate())){
			contClaim.setReportDate(DateUtil.parseDate(reqClaimInfoDTO.getReportDate()));
		}
		if(!StringUtil.isBlank(reqClaimInfoDTO.getSocialInsuPayment())){
			contClaim.setSocialInsuAmount(new BigDecimal(reqClaimInfoDTO.getSocialInsuPayment()));
		}
		contClaim.setWarnFlag(reqClaimInfoDTO.getWarningIndi());
		contClaim.setWarnReason(reqClaimInfoDTO.getWarningDesc());
		if(!StringUtil.isEmpty(reqClaimInfoDTO.getLastPaymentTime())){
			contClaim.setLastpaymenttime(DateUtil.parseDate(reqClaimInfoDTO.getLastPaymentTime()));
		}
		if(!StringUtil.isEmpty(reqClaimInfoDTO.getEstimatePayAmount())){
			contClaim.setEstimatepayamount(new BigDecimal(reqClaimInfoDTO.getEstimatePayAmount()));
		}
		return contClaim;
	}

	/**
	 * 封装理赔保单 核心请求DTO转为本系统持久化对象
	 * 
	 * @param reqClaimPolicyInfoDTO 理赔保单信息DTO对象
	 * @param claimId 理赔赔案id
	 * @param policyId 保单id
	 * @return 
	 */
	private TaxClaimCont initClaimCont(ReqClaimPolicyInfoDTO reqClaimPolicyInfoDTO, Integer claimId,Integer policyId) {
		TaxClaimCont taxClaimCont = new TaxClaimCont();
		taxClaimCont.setClaimantCustomerNo(reqClaimPolicyInfoDTO.getCustomerNo());
		taxClaimCont.setClaimId(claimId);
		if(!StringUtil.isBlank(reqClaimPolicyInfoDTO.getClaimAmount())){
			taxClaimCont.setContClaimAmount(new BigDecimal(reqClaimPolicyInfoDTO.getClaimAmount()));
		}
		taxClaimCont.setContNo(reqClaimPolicyInfoDTO.getPolicyNo());
		taxClaimCont.setContId(policyId);
		taxClaimCont.setContState(reqClaimPolicyInfoDTO.getPolicyStatus());
		taxClaimCont.setRcState(ENUM_RC_STATE.EFFECTIVE.getCode());
		taxClaimCont.setSequenceNo(reqClaimPolicyInfoDTO.getSequenceNo());
		return taxClaimCont;
	}

	/**
	 * 封装理赔保单险种 核心请求DTO转为本系统持久化对象
	 * @param claimCoveragePaymentInfoDTO 理赔保单险种DTO
	 * @param claimContId 理赔保单Id
	 * @return 
	 */
	private TaxClaimContPolicy initClaimContPolicy(ReqClaimCoveragePaymentInfoDTO claimCoveragePaymentInfoDTO,Integer claimContId) {
		TaxClaimContPolicy claimContPolicy = new TaxClaimContPolicy();
		if(!StringUtil.isBlank(claimCoveragePaymentInfoDTO.getClaimAmount())){
			claimContPolicy.setClaimAmount(new BigDecimal(claimCoveragePaymentInfoDTO.getClaimAmount()));
		}
		claimContPolicy.setClaimConclusionCode(claimCoveragePaymentInfoDTO.getClaimConclusionCode());
		claimContPolicy.setClaimContId(claimContId);
		claimContPolicy.setClaimResult(claimCoveragePaymentInfoDTO.getClaimOpinion());
		claimContPolicy.setConclusionReason(claimCoveragePaymentInfoDTO.getConclusionReason());
		claimContPolicy.setCoveragePackageCode(claimCoveragePaymentInfoDTO.getCoveragePackageCode());
		claimContPolicy.setCoverageType(claimCoveragePaymentInfoDTO.getCoverageType());
		claimContPolicy.setRiskCode(claimCoveragePaymentInfoDTO.getComCoverageCode());
		claimContPolicy.setRiskName(claimCoveragePaymentInfoDTO.getComCoverageName());
		claimContPolicy.setRcState(ENUM_RC_STATE.EFFECTIVE.getCode());
		return claimContPolicy;
	}

	/**
	 * 封装理赔保单险种责任信息 核心请求DTO转为本系统持久化对象
	 * @param claimLiabilityPaymentInfoDTO 理赔保单险种责任的DTO
	 * @param claimPolicyId 理赔保单险种id
	 * @return
	 */
	private TaxClaimContDutyPolicy initClaimContDutyPolicy(ReqClaimLiabilityPaymentInfoDTO claimLiabilityPaymentInfoDTO,Integer claimPolicyId) {
		TaxClaimContDutyPolicy taxClaimContDutyPolicy = new TaxClaimContDutyPolicy();
		if(!StringUtil.isBlank(claimLiabilityPaymentInfoDTO.getPaymentAmount())){
			taxClaimContDutyPolicy.setClaimAmount(new BigDecimal(claimLiabilityPaymentInfoDTO.getPaymentAmount()));
		}
		taxClaimContDutyPolicy.setClaimConclusionCode(claimLiabilityPaymentInfoDTO.getClaimConclusionCode());
		taxClaimContDutyPolicy.setClaimPolicyId(claimPolicyId);
		taxClaimContDutyPolicy.setClaimResult(claimLiabilityPaymentInfoDTO.getClaimOpinion());
		taxClaimContDutyPolicy.setConclusionReason(claimLiabilityPaymentInfoDTO.getConclusionReason());
		taxClaimContDutyPolicy.setDutyCode(claimLiabilityPaymentInfoDTO.getLiabilityCode());
		taxClaimContDutyPolicy.setDutyName(claimLiabilityPaymentInfoDTO.getLiabilityName());
		taxClaimContDutyPolicy.setIsOperation(claimLiabilityPaymentInfoDTO.getOperation());
		taxClaimContDutyPolicy.setPlatformDutyType(claimLiabilityPaymentInfoDTO.getLiabilityType());
		taxClaimContDutyPolicy.setRcState(ENUM_RC_STATE.EFFECTIVE.getCode());
		return taxClaimContDutyPolicy;
	}
	
	/**
	 * 判断list
	 * @param list
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private boolean checkList(List list){
		return list != null && list.size() > 0;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public TaxFaultTask addClaimSettlementExceptionJob(String reqJSON, String portCode, Integer faultTaskId) {
		logger.info("进入理赔异常任务服务");
		ThirdSendResponseDTO thirdSendResponse = new ThirdSendResponseDTO();
		List<ReqBusinessDetailDTO> reqBusinessDetailList = new ArrayList<ReqBusinessDetailDTO>();
		TaxFaultTask faultTask = taxFaultTaskMapper.selectByPrimaryKey(faultTaskId);
		String flag="";
		try {
			ReqBusinessDTO businessDTO = commonCodeService.initReqBusinessDTO();
			ParamHeadDTO paramHead = new ParamHeadDTO();
			CoreRequestHead coreRequestHead = new CoreRequestHead();
			List<ReqClaimInfoDTO>claimList = new ArrayList<ReqClaimInfoDTO>();			
			//1. 解析请求报文
			if(ENUM_HX_SERVICE_PORT_CODE.CLAIM_SETTLEMEN_ONE.code().equals(portCode)){
				ReqClaimCoreDTO claimCoreDTO=JSONObject.parseObject(reqJSON,ReqClaimCoreDTO.class);
				ReqClaimInfoDTO claim=(ReqClaimInfoDTO)claimCoreDTO.getBody();
				claimList.add(claim);
				coreRequestHead = claimCoreDTO.getRequestHead();
				flag = ENUM_HX_SERVICE_PORT_CODE.CLAIM_SETTLEMEN_ONE.desc();
			}else if(ENUM_HX_SERVICE_PORT_CODE.CLAIM_SETTLEMEN_MORE.code().equals(portCode)){
				ReqClaimCoreMDTO reqClaimCoreMDTO = JSONObject.parseObject(reqJSON,ReqClaimCoreMDTO.class);
				claimList = (List<ReqClaimInfoDTO>)reqClaimCoreMDTO.getBody();
				coreRequestHead = reqClaimCoreMDTO.getRequestHead();
				flag = ENUM_HX_SERVICE_PORT_CODE.CLAIM_SETTLEMEN_MORE.desc();
			}else{
				throw new BusinessDataErrException("请求接口编码有误! 错误接口编码为："+portCode);
			}	
			//2. 封装头部信息
			paramHead.setAreaCode(coreRequestHead.getAreaCode());
			paramHead.setRecordNum(coreRequestHead.getRecordNum());
			paramHead.setSerialNo(coreRequestHead.getSerialNo());
			paramHead.setPortCode(portCode);
			ThirdSendDTO thirdSendDTO =null;
			EbaoServerPortDTO ebaoServerPort=null;
			Holder<ResponseHeader> responseHeaderHolder = new Holder<ResponseHeader>();
			Holder<ResponseBody> responseBodyHolder = new Holder<ResponseBody>();
			//3. 封装请求中保信数据
			if(ENUM_DISPOSE_TYPE.APPOINTMENT.code().equals(faultTask.getDisposeType())){
				// 预约码处理方式 
				paramHead.setRecordNum("1"); //异步请求接口请求记录数为1
				BookSequenceDTO bookSequence = new BookSequenceDTO();
				bookSequence.setBookingSequenceNo(faultTask.getRemark());
				thirdSendDTO = commonCodeService.createThirdSend(ENUM_EBAO_SERVICE_PORT_CODE.CLAIM_SETTLEMEN_APPOINTMENT.code(), paramHead, bookSequence);
				//调用统一的预约码查询接口地址
				ebaoServerPort = commonCodeService.queryEBaoServerInfoByPortCode(ENUM_EBAO_SERVICE_PORT_CODE.ASYN_RESULT_QUERY.code());
				logger.info("预约码 - 理赔信息上传（请求中保信）JSON 数据内容(预约码类型)："+JSONObject.toJSONString(thirdSendDTO));
				String portUrl = ebaoServerPort.getPortUrl();
				URL wsdlLocation = new URL(portUrl);
				AsynResultQueryService_Service service = new AsynResultQueryService_Service(wsdlLocation);
				AsynResultQueryService servicePort = service.getAsynResultQueryServicePort();
				BindingProvider bp = (BindingProvider) servicePort;
				bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, portUrl);
				servicePort.asynResultQuery(thirdSendDTO.getHead(), thirdSendDTO.getBody(),responseHeaderHolder, responseBodyHolder);
				
			}else if(ENUM_DISPOSE_TYPE.ASYNC.code().equals(faultTask.getDisposeType()) || ENUM_DISPOSE_TYPE.ERROR.code().equals(faultTask.getDisposeType())){
				// 错误或异常处理方式
				List<ClaimInfoDTO> claimInfoDTOList = this.createClaimInfoDTOList(claimList);
				ClaimCoreDTO claimCoreDTO=new ClaimCoreDTO();
				claimCoreDTO.setClaimList(claimInfoDTOList);	
				//调用理赔上传请求接口地址
				ebaoServerPort = commonCodeService.queryEBaoServerInfoByPortCode(ENUM_EBAO_SERVICE_PORT_CODE.CLAIM_SETTLEMEN.code());
				thirdSendDTO = commonCodeService.createThirdSend(ENUM_EBAO_SERVICE_PORT_CODE.CLAIM_SETTLEMEN.code(), paramHead, claimCoreDTO);
				logger.info("异常任务 - 理赔信息上传（请求中保信）JSON 数据内容(错误或异常类型)："+JSONObject.toJSONString(thirdSendDTO));
				String portUrl = ebaoServerPort.getPortUrl();
				URL wsdlLocation = new URL(portUrl);
				ClaimSettlementService_Service service = new ClaimSettlementService_Service(wsdlLocation);
				ClaimSettlementService servicePort = service.getClaimSettlementServicePort();
				BindingProvider bp = (BindingProvider) servicePort;
				bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, portUrl);
				servicePort.settlement(thirdSendDTO.getHead(),thirdSendDTO.getBody(), responseHeaderHolder,responseBodyHolder);
			}else{
				throw new BusinessDataErrException("异常类型有误! 错误异常类型为："+ENUM_DISPOSE_TYPE.getEnumValueByKey(faultTask.getDisposeType()));
			}
			ResponseHeader responseHeader = responseHeaderHolder.value;
			ResponseBody responseBody = responseBodyHolder.value;
			logger.info("异常任务 - 理赔上传异常任务服务方法（中保信响应）JSON 数据内容："+responseBody.getJsonString());
			// 封装中保信返回报文
			ResponseJSON responseJSON = new ResponseJSON();
			responseJSON.setResponseBody(responseBody);
			responseJSON.setResponseHeader(responseHeader);
			//如果异步请求 记录发送日志	
			if(ENUM_DISPOSE_TYPE.ASYNC.code().equals(faultTask.getDisposeType())){
				// 封装发送报文日志对象
				SendLogDTO sendLog = new SendLogDTO();
				sendLog.setSerialNo(coreRequestHead.getSerialNo());
				sendLog.setPortCode(ENUM_EBAO_SERVICE_PORT_CODE.CLAIM_SETTLEMEN.code());
				sendLog.setRequestContent(JSONObject.toJSONString(thirdSendDTO));
				sendLog.setResponseContent(JSONObject.toJSONString(responseJSON));
				sendLog.setTargetCode(ENUM_SENDSERVICE_CODE.EBAO.code());
				sendLog.setState(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
				sendLog.setDescription(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
				commonCodeService.addLogSend(sendLog);
			}			
			// 初始化头部错误信息
			String messageBody = ENUM_BZ_RESPONSE_RESULT.FAIL.description();
			// 5.封装返回核心dto;
			if (ENUM_RESULT_CODE.SUCCESS.code().equals(responseHeader.getResultCode())
					|| ENUM_RESULT_CODE.INPUT_LOCALITY_SUCCESS.code().equals(responseHeader.getResultCode())) {
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
				thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
				//获取中保信返回的理赔结果
				ClaimResultDTO claimResultDTO = JSONObject.parseObject(responseBody.getJsonString(), ClaimResultDTO.class);
				List<ClaimResultInfo> claimResultInfos = claimResultDTO.getClaimResultList();				
				// 6.存储业务表数据
				addOrModifyClaim(claimList,claimResultInfos);
			}else if(ENUM_RESULT_CODE.REQ_AWAIT.code().equals(responseHeader.getResultCode())){
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
				thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
			}else{
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
				List list = responseHeader.getResultMessage().getMessage();
				if(list != null && list.size() > 0){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : responseHeader.getResultMessage().getMessage()){
						sbStr.append(mess).append("|");
					}
					String message = messageBody = sbStr.substring(0, sbStr.length()-1).toString();
					thirdSendResponse.setResultDesc(message);
				}else{
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
				}
			}
			boolean isRegisterFaulTask = false;
			if(!StringUtil.isEmpty(responseBody.getJsonString())){
				ClaimResultDTO claimResultDTO = JSONObject.parseObject(responseBody.getJsonString(), ClaimResultDTO.class);
				if(null != claimResultDTO.getClaimResultList()){				
					List<ClaimResultInfo> claimResultInfos = claimResultDTO.getClaimResultList();
					List<ResultInfoDTO> resultInfoDTOs = new ArrayList<ResultInfoDTO>();
					// 成功记录数
					Integer successNum = 0;
					// 失败记录数
					Integer failNum = 0;
					//循环中保信返回信息DTO
					for (ClaimResultInfo resultInfo : claimResultInfos) {
						//循环理赔信息上传请求DTO对象
						for (ReqClaimInfoDTO reqClaimInfoDTO : claimList){		
							if(resultInfo.getClaimNo().equals(reqClaimInfoDTO.getClaimNo())){
								String message = "";
								ResultInfoDTO resultInfoDTO = new ResultInfoDTO();
								EBResultCodeDTO eBResultCode = resultInfo.getResult();
								HXResultCodeDTO hxResultCode = new HXResultCodeDTO();
								hxResultCode.setResultCode(eBResultCode.getResultCode());
								if(null != eBResultCode.getResultMessage()){
									StringBuffer sbStr = new StringBuffer(); 
									for(String mess : eBResultCode.getResultMessage()){
										sbStr.append(mess).append("|");
									}
									message = sbStr.substring(0, sbStr.length()-1).toString();
								}
								resultInfoDTO.setBizNo(reqClaimInfoDTO.getBizNo());
								resultInfoDTO.setClaimCodeP(resultInfo.getClaimCodeP());
								hxResultCode.setResultMessage(message);
								resultInfoDTO.setResult(hxResultCode);
								resultInfoDTOs.add(resultInfoDTO);
								if(ENUM_RESULT_CODE.SUCCESS.code().equals(eBResultCode.getResultCode())){
									successNum+=1;
								}else{
									failNum+=1;
									ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
									reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
									reqBusinessDetailDTO.setResultMessage(message);
									reqBusinessDetailList.add(reqBusinessDetailDTO);
								}
							}	
						}	
					}
					// 登记请求记录
					businessDTO.setReqSuccessNum(successNum);
					businessDTO.setReqFailNum(failNum);
					//多笔上传返回List
					if(flag.equals(ENUM_HX_SERVICE_PORT_CODE.CLAIM_SETTLEMEN_MORE.desc())){
						thirdSendResponse.setResJson(resultInfoDTOs);
					}else{
						thirdSendResponse.setResJson(resultInfoDTOs.get(0));
					}
					isRegisterFaulTask = true;
				}else{
					BookSequenceDTO bookSequence = JSONObject.parseObject(responseBody.getJsonString(),BookSequenceDTO.class);
					// 插入异常任务表
					faultTask.setRemark(bookSequence.getBookingSequenceNo());
					faultTask.setDisposeType(ENUM_DISPOSE_TYPE.APPOINTMENT.code());
					thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
				}
			}else{
				businessDTO.setReqFailNum(responseHeader.getRecordNum());
				thirdSendResponse.setResJson(messageBody);
				for (int i = 0; i < responseHeader.getRecordNum(); i++) {
					ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
					reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
					reqBusinessDetailDTO.setResultMessage(thirdSendResponse.getResultDesc());
					reqBusinessDetailList.add(reqBusinessDetailDTO);
				}
			}
			if(isRegisterFaulTask){
				// 添加失败交易记录
				commonCodeService.addReqBusinessDetail(reqBusinessDetailList);
				businessDTO.setRequetNum(responseHeader.getRecordNum());
				// 添加请求交易记录数
				commonCodeService.addReqBusinessCollate(businessDTO);
				faultTask.setDisposeResult(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
			}else{
				faultTask.setDisposeResult(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("理赔上传异常任务服务方法处理发生异常! 异常信息为："+e.getMessage());
			thirdSendResponse.setResJson(e.getMessage());
			thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
			thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
			faultTask.setDisposeResult(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
			if(ENUM_DISPOSE_TYPE.APPOINTMENT.code().equals(faultTask.getDisposeType())){
				faultTask.setDisposeType(ENUM_DISPOSE_TYPE.APPOINTMENT.code());
			}else{
				faultTask.setDisposeType(ENUM_DISPOSE_TYPE.ERROR.code());
			}
		} finally{
			faultTask.setSuccessMessage(JSONObject.toJSONString(thirdSendResponse));
			faultTask.setDisposeDate(DateUtil.getCurrentDate());
			taxFaultTaskMapper.updateByPrimaryKeySelective(faultTask);
		}
		return faultTask;
	}
		
	@Override
	public ThirdSendResponseDTO modifyClaimStateExceptionJob(String reqJSON, String portCode, TaxFaultTask faultTask) {
		logger.info("进入理赔撤销申请上传异常任务服务");
		ThirdSendResponseDTO thirdSendResponse = new ThirdSendResponseDTO();
		List<ReqBusinessDetailDTO> reqBusinessDetailList = new ArrayList<ReqBusinessDetailDTO>();
		try {
			ReqBusinessDTO businessDTO = commonCodeService.initReqBusinessDTO();
			ParamHeadDTO paramHeadDTO = new ParamHeadDTO();
			CoreRequestHead coreRequestHead = new CoreRequestHead();
			//1. 解析请求报文
			ReqClaimReversalDTO reqClaimReversal = JSONObject.parseObject(reqJSON,ReqClaimReversalDTO.class);
			// 将请求dto转换为中保信理赔请求dto
			ClaimReversalInfoDTO claimReversal=this.createClaimReversalDTO((ReqClaimReversalInfoDTO)reqClaimReversal.getBody());
			ClaimReversalDTO claimReversalDTO=new ClaimReversalDTO();
			claimReversalDTO.setClaim(claimReversal);
			coreRequestHead=reqClaimReversal.getRequestHead();
			//2. 封装头部信息
			paramHeadDTO.setAreaCode(coreRequestHead.getAreaCode());
			paramHeadDTO.setRecordNum(coreRequestHead.getRecordNum());
			paramHeadDTO.setSerialNo(coreRequestHead.getSerialNo());
			paramHeadDTO.setPortCode(portCode);
			ThirdSendDTO thirdSendDTO = commonCodeService.createThirdSend(ENUM_EBAO_SERVICE_PORT_CODE.CLAIM_REVERSAL.code(), paramHeadDTO, claimReversalDTO);
			logger.info("异常任务 -理赔案件撤销（请求中保信）JSON 数据内容："+JSONObject.toJSONString(thirdSendDTO));
			Holder<ResponseHeader> responseHeaderHolder = new Holder<ResponseHeader>();
			Holder<ResponseBody> responseBodyHolder = new Holder<ResponseBody>();
			// 3.调用中保信接口服务,获得返回结果 存储调用中保信日志表
			EbaoServerPortDTO ebaoServerPort = commonCodeService.queryEBaoServerInfoByPortCode(ENUM_EBAO_SERVICE_PORT_CODE.CLAIM_REVERSAL.code());
			String portUrl = ebaoServerPort.getPortUrl();
			URL wsdlLocation = new URL(portUrl);
			ClaimReversalService_Service service = new ClaimReversalService_Service(wsdlLocation);
			ClaimReversalService servicePort = service.getClaimReversalServicePort();
			BindingProvider bp = (BindingProvider) servicePort;
			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, portUrl);
			servicePort.reversal(thirdSendDTO.getHead(),thirdSendDTO.getBody(), responseHeaderHolder,responseBodyHolder);
			ResponseHeader responseHeader = responseHeaderHolder.value;
			ResponseBody responseBody = responseBodyHolder.value;
			logger.info("异常任务 -理赔案件撤销上传（中保信响应）JSON 数据内容："+responseBody.getJsonString());
			// 初始化头部错误信息
			String messageBody = ENUM_BZ_RESPONSE_RESULT.FAIL.description();
			if (ENUM_RESULT_CODE.SUCCESS.code().equals(responseHeader.getResultCode())
					|| ENUM_RESULT_CODE.INPUT_LOCALITY_SUCCESS.code().equals(responseHeader.getResultCode())) {
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
				thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
				//修改理赔案件状态
				this.modifyClaimState(claimReversal);	
			}else{
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
				List list = responseHeader.getResultMessage().getMessage();
				if(list != null && list.size() > 0){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : responseHeader.getResultMessage().getMessage()){
						sbStr.append(mess).append("|");
					}
					String message = messageBody = sbStr.substring(0, sbStr.length()-1).toString();
					thirdSendResponse.setResultDesc(message);
				}else{
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
				}
				faultTask.setDisposeResult(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
			}
			if(!StringUtil.isEmpty(responseBody.getJsonString())){
				ClaimReversalResultInfoDTO reversalResultDTO=JSONObject.parseObject(responseBody.getJsonString(), ClaimReversalResultInfoDTO.class);
				EBResultCodeDTO eBResultCode=reversalResultDTO.getResult();
				String message = "";
				ResultClaimReversalDTO reversalDTO=new ResultClaimReversalDTO();
				HXResultCodeDTO resultDTO=new HXResultCodeDTO();
				if(null != eBResultCode.getResultMessage()){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : eBResultCode.getResultMessage()){
						sbStr.append(mess).append("|");
					}
					message = sbStr.substring(0, sbStr.length()-1).toString();
				}
				resultDTO.setResultMessage(message);
				resultDTO.setResultCode(eBResultCode.getResultCode());
				reversalDTO.setClaimResult(resultDTO);
				thirdSendResponse.setResJson(reversalDTO);
				if(ENUM_RESULT_CODE.SUCCESS.code().equals(eBResultCode.getResultCode())){
					businessDTO.setReqSuccessNum(businessDTO.getReqSuccessNum() + 1);
				}else{
					businessDTO.setReqFailNum(businessDTO.getReqFailNum() + 1);
					ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHeadDTO);
					reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
					reqBusinessDetailDTO.setResultMessage(message);
					reqBusinessDetailList.add(reqBusinessDetailDTO);
				}	
			}else{
				businessDTO.setReqFailNum(responseHeader.getRecordNum());
				thirdSendResponse.setResJson(messageBody);
				ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHeadDTO);
				reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
				reqBusinessDetailDTO.setResultMessage(thirdSendResponse.getResultDesc());
				reqBusinessDetailList.add(reqBusinessDetailDTO);
			}
			// 添加失败交易记录
			commonCodeService.addReqBusinessDetail(reqBusinessDetailList);
			businessDTO.setRequetNum(responseHeader.getRecordNum());
			// 添加请求交易记录数
			commonCodeService.addReqBusinessCollate(businessDTO);
			faultTask.setDisposeResult(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());	
		}catch(Exception e) {
			e.printStackTrace();
			thirdSendResponse.setResJson(e.getMessage());
			thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
			thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
			logger.error("理赔撤销异常任务服务方法处理发生异常! 异常信息为："+e.getMessage());
			faultTask.setDisposeType(ENUM_DISPOSE_TYPE.ERROR.code());
			faultTask.setDisposeResult(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
		} finally{
			faultTask.setSuccessMessage(JSONObject.toJSONString(thirdSendResponse));
			faultTask.setDisposeDate(DateUtil.getCurrentDate());
			taxFaultTaskMapper.updateByPrimaryKeySelective(faultTask);
		}
		logger.info("理赔撤销异常任务服务方法处理结束!");
		return thirdSendResponse;
	}	
}
