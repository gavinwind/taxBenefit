package com.sinosoft.taxbenefit.core.securityguard.dto;

import com.sinosoft.taxbenefit.api.base.EBResultCodeDTO;

/**
 * 保全撤销 中保信返回信息DTO
 * 
 * @author zhangke
 *
 */
public class EndorsementCancelResultBodyDTO {
	/** 保单号 */
	private String policyNo;

	private String endorsementNo;
	private EBResultCodeDTO result;

	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public EBResultCodeDTO getResult() {
		return result;
	}

	public void setResult(EBResultCodeDTO result) {
		this.result = result;
	}

	public String getEndorsementNo() {
		return endorsementNo;
	}

	public void setEndorsementNo(String endorsementNo) {
		this.endorsementNo = endorsementNo;
	}

}
