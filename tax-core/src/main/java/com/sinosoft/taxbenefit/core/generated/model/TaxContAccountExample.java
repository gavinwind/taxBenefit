package com.sinosoft.taxbenefit.core.generated.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TaxContAccountExample {
    /**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table TAX_CONT_ACCOUNT
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	protected String orderByClause;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table TAX_CONT_ACCOUNT
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	protected boolean distinct;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table TAX_CONT_ACCOUNT
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	protected List<Criteria> oredCriteria;

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_ACCOUNT
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public TaxContAccountExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_ACCOUNT
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_ACCOUNT
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public String getOrderByClause() {
		return orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_ACCOUNT
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_ACCOUNT
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public boolean isDistinct() {
		return distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_ACCOUNT
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_ACCOUNT
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_ACCOUNT
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_ACCOUNT
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_ACCOUNT
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table TAX_CONT_ACCOUNT
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the database table TAX_CONT_ACCOUNT
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	protected abstract static class GeneratedCriteria {
		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value,
				String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property
						+ " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1,
				Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property
						+ " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andSidIsNull() {
			addCriterion("SID is null");
			return (Criteria) this;
		}

		public Criteria andSidIsNotNull() {
			addCriterion("SID is not null");
			return (Criteria) this;
		}

		public Criteria andSidEqualTo(Integer value) {
			addCriterion("SID =", value, "sid");
			return (Criteria) this;
		}

		public Criteria andSidNotEqualTo(Integer value) {
			addCriterion("SID <>", value, "sid");
			return (Criteria) this;
		}

		public Criteria andSidGreaterThan(Integer value) {
			addCriterion("SID >", value, "sid");
			return (Criteria) this;
		}

		public Criteria andSidGreaterThanOrEqualTo(Integer value) {
			addCriterion("SID >=", value, "sid");
			return (Criteria) this;
		}

		public Criteria andSidLessThan(Integer value) {
			addCriterion("SID <", value, "sid");
			return (Criteria) this;
		}

		public Criteria andSidLessThanOrEqualTo(Integer value) {
			addCriterion("SID <=", value, "sid");
			return (Criteria) this;
		}

		public Criteria andSidIn(List<Integer> values) {
			addCriterion("SID in", values, "sid");
			return (Criteria) this;
		}

		public Criteria andSidNotIn(List<Integer> values) {
			addCriterion("SID not in", values, "sid");
			return (Criteria) this;
		}

		public Criteria andSidBetween(Integer value1, Integer value2) {
			addCriterion("SID between", value1, value2, "sid");
			return (Criteria) this;
		}

		public Criteria andSidNotBetween(Integer value1, Integer value2) {
			addCriterion("SID not between", value1, value2, "sid");
			return (Criteria) this;
		}

		public Criteria andContIdIsNull() {
			addCriterion("CONT_ID is null");
			return (Criteria) this;
		}

		public Criteria andContIdIsNotNull() {
			addCriterion("CONT_ID is not null");
			return (Criteria) this;
		}

		public Criteria andContIdEqualTo(Integer value) {
			addCriterion("CONT_ID =", value, "contId");
			return (Criteria) this;
		}

		public Criteria andContIdNotEqualTo(Integer value) {
			addCriterion("CONT_ID <>", value, "contId");
			return (Criteria) this;
		}

		public Criteria andContIdGreaterThan(Integer value) {
			addCriterion("CONT_ID >", value, "contId");
			return (Criteria) this;
		}

		public Criteria andContIdGreaterThanOrEqualTo(Integer value) {
			addCriterion("CONT_ID >=", value, "contId");
			return (Criteria) this;
		}

		public Criteria andContIdLessThan(Integer value) {
			addCriterion("CONT_ID <", value, "contId");
			return (Criteria) this;
		}

		public Criteria andContIdLessThanOrEqualTo(Integer value) {
			addCriterion("CONT_ID <=", value, "contId");
			return (Criteria) this;
		}

		public Criteria andContIdIn(List<Integer> values) {
			addCriterion("CONT_ID in", values, "contId");
			return (Criteria) this;
		}

		public Criteria andContIdNotIn(List<Integer> values) {
			addCriterion("CONT_ID not in", values, "contId");
			return (Criteria) this;
		}

		public Criteria andContIdBetween(Integer value1, Integer value2) {
			addCriterion("CONT_ID between", value1, value2, "contId");
			return (Criteria) this;
		}

		public Criteria andContIdNotBetween(Integer value1, Integer value2) {
			addCriterion("CONT_ID not between", value1, value2, "contId");
			return (Criteria) this;
		}

		public Criteria andContNoIsNull() {
			addCriterion("CONT_NO is null");
			return (Criteria) this;
		}

		public Criteria andContNoIsNotNull() {
			addCriterion("CONT_NO is not null");
			return (Criteria) this;
		}

		public Criteria andContNoEqualTo(String value) {
			addCriterion("CONT_NO =", value, "contNo");
			return (Criteria) this;
		}

		public Criteria andContNoNotEqualTo(String value) {
			addCriterion("CONT_NO <>", value, "contNo");
			return (Criteria) this;
		}

		public Criteria andContNoGreaterThan(String value) {
			addCriterion("CONT_NO >", value, "contNo");
			return (Criteria) this;
		}

		public Criteria andContNoGreaterThanOrEqualTo(String value) {
			addCriterion("CONT_NO >=", value, "contNo");
			return (Criteria) this;
		}

		public Criteria andContNoLessThan(String value) {
			addCriterion("CONT_NO <", value, "contNo");
			return (Criteria) this;
		}

		public Criteria andContNoLessThanOrEqualTo(String value) {
			addCriterion("CONT_NO <=", value, "contNo");
			return (Criteria) this;
		}

		public Criteria andContNoLike(String value) {
			addCriterion("CONT_NO like", value, "contNo");
			return (Criteria) this;
		}

		public Criteria andContNoNotLike(String value) {
			addCriterion("CONT_NO not like", value, "contNo");
			return (Criteria) this;
		}

		public Criteria andContNoIn(List<String> values) {
			addCriterion("CONT_NO in", values, "contNo");
			return (Criteria) this;
		}

		public Criteria andContNoNotIn(List<String> values) {
			addCriterion("CONT_NO not in", values, "contNo");
			return (Criteria) this;
		}

		public Criteria andContNoBetween(String value1, String value2) {
			addCriterion("CONT_NO between", value1, value2, "contNo");
			return (Criteria) this;
		}

		public Criteria andContNoNotBetween(String value1, String value2) {
			addCriterion("CONT_NO not between", value1, value2, "contNo");
			return (Criteria) this;
		}

		public Criteria andSequenceNoIsNull() {
			addCriterion("SEQUENCE_NO is null");
			return (Criteria) this;
		}

		public Criteria andSequenceNoIsNotNull() {
			addCriterion("SEQUENCE_NO is not null");
			return (Criteria) this;
		}

		public Criteria andSequenceNoEqualTo(String value) {
			addCriterion("SEQUENCE_NO =", value, "sequenceNo");
			return (Criteria) this;
		}

		public Criteria andSequenceNoNotEqualTo(String value) {
			addCriterion("SEQUENCE_NO <>", value, "sequenceNo");
			return (Criteria) this;
		}

		public Criteria andSequenceNoGreaterThan(String value) {
			addCriterion("SEQUENCE_NO >", value, "sequenceNo");
			return (Criteria) this;
		}

		public Criteria andSequenceNoGreaterThanOrEqualTo(String value) {
			addCriterion("SEQUENCE_NO >=", value, "sequenceNo");
			return (Criteria) this;
		}

		public Criteria andSequenceNoLessThan(String value) {
			addCriterion("SEQUENCE_NO <", value, "sequenceNo");
			return (Criteria) this;
		}

		public Criteria andSequenceNoLessThanOrEqualTo(String value) {
			addCriterion("SEQUENCE_NO <=", value, "sequenceNo");
			return (Criteria) this;
		}

		public Criteria andSequenceNoLike(String value) {
			addCriterion("SEQUENCE_NO like", value, "sequenceNo");
			return (Criteria) this;
		}

		public Criteria andSequenceNoNotLike(String value) {
			addCriterion("SEQUENCE_NO not like", value, "sequenceNo");
			return (Criteria) this;
		}

		public Criteria andSequenceNoIn(List<String> values) {
			addCriterion("SEQUENCE_NO in", values, "sequenceNo");
			return (Criteria) this;
		}

		public Criteria andSequenceNoNotIn(List<String> values) {
			addCriterion("SEQUENCE_NO not in", values, "sequenceNo");
			return (Criteria) this;
		}

		public Criteria andSequenceNoBetween(String value1, String value2) {
			addCriterion("SEQUENCE_NO between", value1, value2, "sequenceNo");
			return (Criteria) this;
		}

		public Criteria andSequenceNoNotBetween(String value1, String value2) {
			addCriterion("SEQUENCE_NO not between", value1, value2,
					"sequenceNo");
			return (Criteria) this;
		}

		public Criteria andRiskCodeIsNull() {
			addCriterion("RISK_CODE is null");
			return (Criteria) this;
		}

		public Criteria andRiskCodeIsNotNull() {
			addCriterion("RISK_CODE is not null");
			return (Criteria) this;
		}

		public Criteria andRiskCodeEqualTo(String value) {
			addCriterion("RISK_CODE =", value, "riskCode");
			return (Criteria) this;
		}

		public Criteria andRiskCodeNotEqualTo(String value) {
			addCriterion("RISK_CODE <>", value, "riskCode");
			return (Criteria) this;
		}

		public Criteria andRiskCodeGreaterThan(String value) {
			addCriterion("RISK_CODE >", value, "riskCode");
			return (Criteria) this;
		}

		public Criteria andRiskCodeGreaterThanOrEqualTo(String value) {
			addCriterion("RISK_CODE >=", value, "riskCode");
			return (Criteria) this;
		}

		public Criteria andRiskCodeLessThan(String value) {
			addCriterion("RISK_CODE <", value, "riskCode");
			return (Criteria) this;
		}

		public Criteria andRiskCodeLessThanOrEqualTo(String value) {
			addCriterion("RISK_CODE <=", value, "riskCode");
			return (Criteria) this;
		}

		public Criteria andRiskCodeLike(String value) {
			addCriterion("RISK_CODE like", value, "riskCode");
			return (Criteria) this;
		}

		public Criteria andRiskCodeNotLike(String value) {
			addCriterion("RISK_CODE not like", value, "riskCode");
			return (Criteria) this;
		}

		public Criteria andRiskCodeIn(List<String> values) {
			addCriterion("RISK_CODE in", values, "riskCode");
			return (Criteria) this;
		}

		public Criteria andRiskCodeNotIn(List<String> values) {
			addCriterion("RISK_CODE not in", values, "riskCode");
			return (Criteria) this;
		}

		public Criteria andRiskCodeBetween(String value1, String value2) {
			addCriterion("RISK_CODE between", value1, value2, "riskCode");
			return (Criteria) this;
		}

		public Criteria andRiskCodeNotBetween(String value1, String value2) {
			addCriterion("RISK_CODE not between", value1, value2, "riskCode");
			return (Criteria) this;
		}

		public Criteria andRiskNameIsNull() {
			addCriterion("RISK_NAME is null");
			return (Criteria) this;
		}

		public Criteria andRiskNameIsNotNull() {
			addCriterion("RISK_NAME is not null");
			return (Criteria) this;
		}

		public Criteria andRiskNameEqualTo(String value) {
			addCriterion("RISK_NAME =", value, "riskName");
			return (Criteria) this;
		}

		public Criteria andRiskNameNotEqualTo(String value) {
			addCriterion("RISK_NAME <>", value, "riskName");
			return (Criteria) this;
		}

		public Criteria andRiskNameGreaterThan(String value) {
			addCriterion("RISK_NAME >", value, "riskName");
			return (Criteria) this;
		}

		public Criteria andRiskNameGreaterThanOrEqualTo(String value) {
			addCriterion("RISK_NAME >=", value, "riskName");
			return (Criteria) this;
		}

		public Criteria andRiskNameLessThan(String value) {
			addCriterion("RISK_NAME <", value, "riskName");
			return (Criteria) this;
		}

		public Criteria andRiskNameLessThanOrEqualTo(String value) {
			addCriterion("RISK_NAME <=", value, "riskName");
			return (Criteria) this;
		}

		public Criteria andRiskNameLike(String value) {
			addCriterion("RISK_NAME like", value, "riskName");
			return (Criteria) this;
		}

		public Criteria andRiskNameNotLike(String value) {
			addCriterion("RISK_NAME not like", value, "riskName");
			return (Criteria) this;
		}

		public Criteria andRiskNameIn(List<String> values) {
			addCriterion("RISK_NAME in", values, "riskName");
			return (Criteria) this;
		}

		public Criteria andRiskNameNotIn(List<String> values) {
			addCriterion("RISK_NAME not in", values, "riskName");
			return (Criteria) this;
		}

		public Criteria andRiskNameBetween(String value1, String value2) {
			addCriterion("RISK_NAME between", value1, value2, "riskName");
			return (Criteria) this;
		}

		public Criteria andRiskNameNotBetween(String value1, String value2) {
			addCriterion("RISK_NAME not between", value1, value2, "riskName");
			return (Criteria) this;
		}

		public Criteria andCoveragePackageCodeIsNull() {
			addCriterion("COVERAGE_PACKAGE_CODE is null");
			return (Criteria) this;
		}

		public Criteria andCoveragePackageCodeIsNotNull() {
			addCriterion("COVERAGE_PACKAGE_CODE is not null");
			return (Criteria) this;
		}

		public Criteria andCoveragePackageCodeEqualTo(String value) {
			addCriterion("COVERAGE_PACKAGE_CODE =", value,
					"coveragePackageCode");
			return (Criteria) this;
		}

		public Criteria andCoveragePackageCodeNotEqualTo(String value) {
			addCriterion("COVERAGE_PACKAGE_CODE <>", value,
					"coveragePackageCode");
			return (Criteria) this;
		}

		public Criteria andCoveragePackageCodeGreaterThan(String value) {
			addCriterion("COVERAGE_PACKAGE_CODE >", value,
					"coveragePackageCode");
			return (Criteria) this;
		}

		public Criteria andCoveragePackageCodeGreaterThanOrEqualTo(String value) {
			addCriterion("COVERAGE_PACKAGE_CODE >=", value,
					"coveragePackageCode");
			return (Criteria) this;
		}

		public Criteria andCoveragePackageCodeLessThan(String value) {
			addCriterion("COVERAGE_PACKAGE_CODE <", value,
					"coveragePackageCode");
			return (Criteria) this;
		}

		public Criteria andCoveragePackageCodeLessThanOrEqualTo(String value) {
			addCriterion("COVERAGE_PACKAGE_CODE <=", value,
					"coveragePackageCode");
			return (Criteria) this;
		}

		public Criteria andCoveragePackageCodeLike(String value) {
			addCriterion("COVERAGE_PACKAGE_CODE like", value,
					"coveragePackageCode");
			return (Criteria) this;
		}

		public Criteria andCoveragePackageCodeNotLike(String value) {
			addCriterion("COVERAGE_PACKAGE_CODE not like", value,
					"coveragePackageCode");
			return (Criteria) this;
		}

		public Criteria andCoveragePackageCodeIn(List<String> values) {
			addCriterion("COVERAGE_PACKAGE_CODE in", values,
					"coveragePackageCode");
			return (Criteria) this;
		}

		public Criteria andCoveragePackageCodeNotIn(List<String> values) {
			addCriterion("COVERAGE_PACKAGE_CODE not in", values,
					"coveragePackageCode");
			return (Criteria) this;
		}

		public Criteria andCoveragePackageCodeBetween(String value1,
				String value2) {
			addCriterion("COVERAGE_PACKAGE_CODE between", value1, value2,
					"coveragePackageCode");
			return (Criteria) this;
		}

		public Criteria andCoveragePackageCodeNotBetween(String value1,
				String value2) {
			addCriterion("COVERAGE_PACKAGE_CODE not between", value1, value2,
					"coveragePackageCode");
			return (Criteria) this;
		}

		public Criteria andSumAmountIsNull() {
			addCriterion("SUM_AMOUNT is null");
			return (Criteria) this;
		}

		public Criteria andSumAmountIsNotNull() {
			addCriterion("SUM_AMOUNT is not null");
			return (Criteria) this;
		}

		public Criteria andSumAmountEqualTo(BigDecimal value) {
			addCriterion("SUM_AMOUNT =", value, "sumAmount");
			return (Criteria) this;
		}

		public Criteria andSumAmountNotEqualTo(BigDecimal value) {
			addCriterion("SUM_AMOUNT <>", value, "sumAmount");
			return (Criteria) this;
		}

		public Criteria andSumAmountGreaterThan(BigDecimal value) {
			addCriterion("SUM_AMOUNT >", value, "sumAmount");
			return (Criteria) this;
		}

		public Criteria andSumAmountGreaterThanOrEqualTo(BigDecimal value) {
			addCriterion("SUM_AMOUNT >=", value, "sumAmount");
			return (Criteria) this;
		}

		public Criteria andSumAmountLessThan(BigDecimal value) {
			addCriterion("SUM_AMOUNT <", value, "sumAmount");
			return (Criteria) this;
		}

		public Criteria andSumAmountLessThanOrEqualTo(BigDecimal value) {
			addCriterion("SUM_AMOUNT <=", value, "sumAmount");
			return (Criteria) this;
		}

		public Criteria andSumAmountIn(List<BigDecimal> values) {
			addCriterion("SUM_AMOUNT in", values, "sumAmount");
			return (Criteria) this;
		}

		public Criteria andSumAmountNotIn(List<BigDecimal> values) {
			addCriterion("SUM_AMOUNT not in", values, "sumAmount");
			return (Criteria) this;
		}

		public Criteria andSumAmountBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("SUM_AMOUNT between", value1, value2, "sumAmount");
			return (Criteria) this;
		}

		public Criteria andSumAmountNotBetween(BigDecimal value1,
				BigDecimal value2) {
			addCriterion("SUM_AMOUNT not between", value1, value2, "sumAmount");
			return (Criteria) this;
		}

		public Criteria andSumInterestIsNull() {
			addCriterion("SUM_INTEREST is null");
			return (Criteria) this;
		}

		public Criteria andSumInterestIsNotNull() {
			addCriterion("SUM_INTEREST is not null");
			return (Criteria) this;
		}

		public Criteria andSumInterestEqualTo(BigDecimal value) {
			addCriterion("SUM_INTEREST =", value, "sumInterest");
			return (Criteria) this;
		}

		public Criteria andSumInterestNotEqualTo(BigDecimal value) {
			addCriterion("SUM_INTEREST <>", value, "sumInterest");
			return (Criteria) this;
		}

		public Criteria andSumInterestGreaterThan(BigDecimal value) {
			addCriterion("SUM_INTEREST >", value, "sumInterest");
			return (Criteria) this;
		}

		public Criteria andSumInterestGreaterThanOrEqualTo(BigDecimal value) {
			addCriterion("SUM_INTEREST >=", value, "sumInterest");
			return (Criteria) this;
		}

		public Criteria andSumInterestLessThan(BigDecimal value) {
			addCriterion("SUM_INTEREST <", value, "sumInterest");
			return (Criteria) this;
		}

		public Criteria andSumInterestLessThanOrEqualTo(BigDecimal value) {
			addCriterion("SUM_INTEREST <=", value, "sumInterest");
			return (Criteria) this;
		}

		public Criteria andSumInterestIn(List<BigDecimal> values) {
			addCriterion("SUM_INTEREST in", values, "sumInterest");
			return (Criteria) this;
		}

		public Criteria andSumInterestNotIn(List<BigDecimal> values) {
			addCriterion("SUM_INTEREST not in", values, "sumInterest");
			return (Criteria) this;
		}

		public Criteria andSumInterestBetween(BigDecimal value1,
				BigDecimal value2) {
			addCriterion("SUM_INTEREST between", value1, value2, "sumInterest");
			return (Criteria) this;
		}

		public Criteria andSumInterestNotBetween(BigDecimal value1,
				BigDecimal value2) {
			addCriterion("SUM_INTEREST not between", value1, value2,
					"sumInterest");
			return (Criteria) this;
		}

		public Criteria andRcStateIsNull() {
			addCriterion("RC_STATE is null");
			return (Criteria) this;
		}

		public Criteria andRcStateIsNotNull() {
			addCriterion("RC_STATE is not null");
			return (Criteria) this;
		}

		public Criteria andRcStateEqualTo(String value) {
			addCriterion("RC_STATE =", value, "rcState");
			return (Criteria) this;
		}

		public Criteria andRcStateNotEqualTo(String value) {
			addCriterion("RC_STATE <>", value, "rcState");
			return (Criteria) this;
		}

		public Criteria andRcStateGreaterThan(String value) {
			addCriterion("RC_STATE >", value, "rcState");
			return (Criteria) this;
		}

		public Criteria andRcStateGreaterThanOrEqualTo(String value) {
			addCriterion("RC_STATE >=", value, "rcState");
			return (Criteria) this;
		}

		public Criteria andRcStateLessThan(String value) {
			addCriterion("RC_STATE <", value, "rcState");
			return (Criteria) this;
		}

		public Criteria andRcStateLessThanOrEqualTo(String value) {
			addCriterion("RC_STATE <=", value, "rcState");
			return (Criteria) this;
		}

		public Criteria andRcStateLike(String value) {
			addCriterion("RC_STATE like", value, "rcState");
			return (Criteria) this;
		}

		public Criteria andRcStateNotLike(String value) {
			addCriterion("RC_STATE not like", value, "rcState");
			return (Criteria) this;
		}

		public Criteria andRcStateIn(List<String> values) {
			addCriterion("RC_STATE in", values, "rcState");
			return (Criteria) this;
		}

		public Criteria andRcStateNotIn(List<String> values) {
			addCriterion("RC_STATE not in", values, "rcState");
			return (Criteria) this;
		}

		public Criteria andRcStateBetween(String value1, String value2) {
			addCriterion("RC_STATE between", value1, value2, "rcState");
			return (Criteria) this;
		}

		public Criteria andRcStateNotBetween(String value1, String value2) {
			addCriterion("RC_STATE not between", value1, value2, "rcState");
			return (Criteria) this;
		}

		public Criteria andCreateDateIsNull() {
			addCriterion("CREATE_DATE is null");
			return (Criteria) this;
		}

		public Criteria andCreateDateIsNotNull() {
			addCriterion("CREATE_DATE is not null");
			return (Criteria) this;
		}

		public Criteria andCreateDateEqualTo(Date value) {
			addCriterion("CREATE_DATE =", value, "createDate");
			return (Criteria) this;
		}

		public Criteria andCreateDateNotEqualTo(Date value) {
			addCriterion("CREATE_DATE <>", value, "createDate");
			return (Criteria) this;
		}

		public Criteria andCreateDateGreaterThan(Date value) {
			addCriterion("CREATE_DATE >", value, "createDate");
			return (Criteria) this;
		}

		public Criteria andCreateDateGreaterThanOrEqualTo(Date value) {
			addCriterion("CREATE_DATE >=", value, "createDate");
			return (Criteria) this;
		}

		public Criteria andCreateDateLessThan(Date value) {
			addCriterion("CREATE_DATE <", value, "createDate");
			return (Criteria) this;
		}

		public Criteria andCreateDateLessThanOrEqualTo(Date value) {
			addCriterion("CREATE_DATE <=", value, "createDate");
			return (Criteria) this;
		}

		public Criteria andCreateDateIn(List<Date> values) {
			addCriterion("CREATE_DATE in", values, "createDate");
			return (Criteria) this;
		}

		public Criteria andCreateDateNotIn(List<Date> values) {
			addCriterion("CREATE_DATE not in", values, "createDate");
			return (Criteria) this;
		}

		public Criteria andCreateDateBetween(Date value1, Date value2) {
			addCriterion("CREATE_DATE between", value1, value2, "createDate");
			return (Criteria) this;
		}

		public Criteria andCreateDateNotBetween(Date value1, Date value2) {
			addCriterion("CREATE_DATE not between", value1, value2,
					"createDate");
			return (Criteria) this;
		}

		public Criteria andCreatorIdIsNull() {
			addCriterion("CREATOR_ID is null");
			return (Criteria) this;
		}

		public Criteria andCreatorIdIsNotNull() {
			addCriterion("CREATOR_ID is not null");
			return (Criteria) this;
		}

		public Criteria andCreatorIdEqualTo(Integer value) {
			addCriterion("CREATOR_ID =", value, "creatorId");
			return (Criteria) this;
		}

		public Criteria andCreatorIdNotEqualTo(Integer value) {
			addCriterion("CREATOR_ID <>", value, "creatorId");
			return (Criteria) this;
		}

		public Criteria andCreatorIdGreaterThan(Integer value) {
			addCriterion("CREATOR_ID >", value, "creatorId");
			return (Criteria) this;
		}

		public Criteria andCreatorIdGreaterThanOrEqualTo(Integer value) {
			addCriterion("CREATOR_ID >=", value, "creatorId");
			return (Criteria) this;
		}

		public Criteria andCreatorIdLessThan(Integer value) {
			addCriterion("CREATOR_ID <", value, "creatorId");
			return (Criteria) this;
		}

		public Criteria andCreatorIdLessThanOrEqualTo(Integer value) {
			addCriterion("CREATOR_ID <=", value, "creatorId");
			return (Criteria) this;
		}

		public Criteria andCreatorIdIn(List<Integer> values) {
			addCriterion("CREATOR_ID in", values, "creatorId");
			return (Criteria) this;
		}

		public Criteria andCreatorIdNotIn(List<Integer> values) {
			addCriterion("CREATOR_ID not in", values, "creatorId");
			return (Criteria) this;
		}

		public Criteria andCreatorIdBetween(Integer value1, Integer value2) {
			addCriterion("CREATOR_ID between", value1, value2, "creatorId");
			return (Criteria) this;
		}

		public Criteria andCreatorIdNotBetween(Integer value1, Integer value2) {
			addCriterion("CREATOR_ID not between", value1, value2, "creatorId");
			return (Criteria) this;
		}

		public Criteria andModifyDateIsNull() {
			addCriterion("MODIFY_DATE is null");
			return (Criteria) this;
		}

		public Criteria andModifyDateIsNotNull() {
			addCriterion("MODIFY_DATE is not null");
			return (Criteria) this;
		}

		public Criteria andModifyDateEqualTo(Date value) {
			addCriterion("MODIFY_DATE =", value, "modifyDate");
			return (Criteria) this;
		}

		public Criteria andModifyDateNotEqualTo(Date value) {
			addCriterion("MODIFY_DATE <>", value, "modifyDate");
			return (Criteria) this;
		}

		public Criteria andModifyDateGreaterThan(Date value) {
			addCriterion("MODIFY_DATE >", value, "modifyDate");
			return (Criteria) this;
		}

		public Criteria andModifyDateGreaterThanOrEqualTo(Date value) {
			addCriterion("MODIFY_DATE >=", value, "modifyDate");
			return (Criteria) this;
		}

		public Criteria andModifyDateLessThan(Date value) {
			addCriterion("MODIFY_DATE <", value, "modifyDate");
			return (Criteria) this;
		}

		public Criteria andModifyDateLessThanOrEqualTo(Date value) {
			addCriterion("MODIFY_DATE <=", value, "modifyDate");
			return (Criteria) this;
		}

		public Criteria andModifyDateIn(List<Date> values) {
			addCriterion("MODIFY_DATE in", values, "modifyDate");
			return (Criteria) this;
		}

		public Criteria andModifyDateNotIn(List<Date> values) {
			addCriterion("MODIFY_DATE not in", values, "modifyDate");
			return (Criteria) this;
		}

		public Criteria andModifyDateBetween(Date value1, Date value2) {
			addCriterion("MODIFY_DATE between", value1, value2, "modifyDate");
			return (Criteria) this;
		}

		public Criteria andModifyDateNotBetween(Date value1, Date value2) {
			addCriterion("MODIFY_DATE not between", value1, value2,
					"modifyDate");
			return (Criteria) this;
		}

		public Criteria andModifierIdIsNull() {
			addCriterion("MODIFIER_ID is null");
			return (Criteria) this;
		}

		public Criteria andModifierIdIsNotNull() {
			addCriterion("MODIFIER_ID is not null");
			return (Criteria) this;
		}

		public Criteria andModifierIdEqualTo(Integer value) {
			addCriterion("MODIFIER_ID =", value, "modifierId");
			return (Criteria) this;
		}

		public Criteria andModifierIdNotEqualTo(Integer value) {
			addCriterion("MODIFIER_ID <>", value, "modifierId");
			return (Criteria) this;
		}

		public Criteria andModifierIdGreaterThan(Integer value) {
			addCriterion("MODIFIER_ID >", value, "modifierId");
			return (Criteria) this;
		}

		public Criteria andModifierIdGreaterThanOrEqualTo(Integer value) {
			addCriterion("MODIFIER_ID >=", value, "modifierId");
			return (Criteria) this;
		}

		public Criteria andModifierIdLessThan(Integer value) {
			addCriterion("MODIFIER_ID <", value, "modifierId");
			return (Criteria) this;
		}

		public Criteria andModifierIdLessThanOrEqualTo(Integer value) {
			addCriterion("MODIFIER_ID <=", value, "modifierId");
			return (Criteria) this;
		}

		public Criteria andModifierIdIn(List<Integer> values) {
			addCriterion("MODIFIER_ID in", values, "modifierId");
			return (Criteria) this;
		}

		public Criteria andModifierIdNotIn(List<Integer> values) {
			addCriterion("MODIFIER_ID not in", values, "modifierId");
			return (Criteria) this;
		}

		public Criteria andModifierIdBetween(Integer value1, Integer value2) {
			addCriterion("MODIFIER_ID between", value1, value2, "modifierId");
			return (Criteria) this;
		}

		public Criteria andModifierIdNotBetween(Integer value1, Integer value2) {
			addCriterion("MODIFIER_ID not between", value1, value2,
					"modifierId");
			return (Criteria) this;
		}
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the database table TAX_CONT_ACCOUNT
	 * @mbggenerated  Wed Mar 02 16:32:03 CST 2016
	 */
	public static class Criterion {
		private String condition;
		private Object value;
		private Object secondValue;
		private boolean noValue;
		private boolean singleValue;
		private boolean betweenValue;
		private boolean listValue;
		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			} else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue,
				String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}
	}

	/**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table TAX_CONT_ACCOUNT
     *
     * @mbggenerated do_not_delete_during_merge Tue Mar 01 17:03:53 CST 2016
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }
}