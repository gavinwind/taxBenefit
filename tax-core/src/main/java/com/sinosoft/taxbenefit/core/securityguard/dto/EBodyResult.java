package com.sinosoft.taxbenefit.core.securityguard.dto;
/**
 * 中报信返回报文对象
 * @author gyas-itkezh
 *
 */
public class EBodyResult {

	private EndorsementResult  endorsementResult;

	public EndorsementResult getEndorsementResult() {
		return endorsementResult;
	}

	public void setEndorsementResult(EndorsementResult endorsementResult) {
		this.endorsementResult = endorsementResult;
	}
	
}
