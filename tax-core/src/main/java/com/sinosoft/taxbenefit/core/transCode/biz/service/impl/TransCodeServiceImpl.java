package com.sinosoft.taxbenefit.core.transCode.biz.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxCodeMappingMapper;
import com.sinosoft.taxbenefit.core.generated.model.TaxCodeMapping;
import com.sinosoft.taxbenefit.core.generated.model.TaxCodeMappingExample;
import com.sinosoft.taxbenefit.core.transCode.biz.service.TransCodeService;
import com.sinosoft.taxbenefit.core.transCode.config.TransCodeConstants;
/**
 * 
 * @author 村长
 *
 */
@Service
public class TransCodeServiceImpl extends TaxBaseService implements TransCodeService{
	@Autowired
	private TaxCodeMappingMapper taxCodeMappingMapper;

	@Override
	public String transCode(String codeType, String lisCode) {
		return transCodeManage(codeType, lisCode, TransCodeConstants.TRANSCODE_MANAGE);
	}

	@Override
	public String unTransCode(String codeType, String zbxCode) {
		TaxCodeMappingExample example=new TaxCodeMappingExample();
		example.createCriteria().andCodeTypeEqualTo(codeType).andTargetCodeEqualTo(zbxCode);
		List<TaxCodeMapping> list = taxCodeMappingMapper.selectByExample(example);
		
		if(null!=list&&list.size()>1){
			return list.get(0).getOriginalCode();
		}		
		return zbxCode;
	}

	@Override
	public String transCodeManage(String codeType, String lisCode, String manageCom) {
		String code="";
		if(null!=lisCode){
			TaxCodeMappingExample example=new TaxCodeMappingExample();
			example.createCriteria().andCodeTypeEqualTo(codeType).andOriginalCodeEqualTo(lisCode).andManageComEqualTo(manageCom);
			List<TaxCodeMapping> list = taxCodeMappingMapper.selectByExample(example);
			if(null!=list&&list.size()>0){
				code= list.get(0).getTargetCode();
			}else{	
				code=lisCode;
			}
		}
		return code;	
	}
}
