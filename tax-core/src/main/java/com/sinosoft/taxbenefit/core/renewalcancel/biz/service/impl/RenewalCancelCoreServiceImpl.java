package com.sinosoft.taxbenefit.core.renewalcancel.biz.service.impl;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.platform.common.util.DateUtil;
import com.sinosoft.platform.common.util.StringUtil;
import com.sinosoft.taxbenefit.api.base.EBResultCodeDTO;
import com.sinosoft.taxbenefit.api.base.HXResultCodeDTO;
import com.sinosoft.taxbenefit.api.config.ENUM_BZ_RESPONSE_RESULT;
import com.sinosoft.taxbenefit.api.config.ENUM_DISPOSE_TYPE;
import com.sinosoft.taxbenefit.api.config.ENUM_SENDSERVICE_CODE;
import com.sinosoft.taxbenefit.api.dto.request.TaxFaulTaskDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.CoreRequestHead;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalcancel.RequestRenewalCancelCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalcancel.RequestRenewalCancelDTO;
import com.sinosoft.taxbenefit.api.dto.response.renewalcancel.ResultRenewalCancelDTO;
import com.sinosoft.taxbenefit.core.common.biz.service.CommonCodeService;
import com.sinosoft.taxbenefit.core.common.config.ENUM_BUSINESS_RESULR;
import com.sinosoft.taxbenefit.core.common.config.ENUM_EBAO_SERVICE_PORT_CODE;
import com.sinosoft.taxbenefit.core.common.config.ENUM_HX_SERVICE_PORT_CODE;
import com.sinosoft.taxbenefit.core.common.config.ENUM_RC_STATE;
import com.sinosoft.taxbenefit.core.common.config.ENUM_RESULT_CODE;
import com.sinosoft.taxbenefit.core.common.dto.EbaoServerPortDTO;
import com.sinosoft.taxbenefit.core.common.dto.ReqBusinessDTO;
import com.sinosoft.taxbenefit.core.common.dto.ReqBusinessDetailDTO;
import com.sinosoft.taxbenefit.core.common.dto.SendLogDTO;
import com.sinosoft.taxbenefit.core.common.dto.ThirdSendDTO;
import com.sinosoft.taxbenefit.core.ebaowebservice.PolicyRenewalCancelService;
import com.sinosoft.taxbenefit.core.ebaowebservice.PolicyRenewalCancelService_Service;
import com.sinosoft.taxbenefit.core.ebaowebservice.ResponseBody;
import com.sinosoft.taxbenefit.core.ebaowebservice.ResponseHeader;
import com.sinosoft.taxbenefit.core.ebaowebservice.ResponseJSON;
import com.sinosoft.taxbenefit.core.faultTask.biz.service.FaultTaskService;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxContMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxContRenewMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxFaultTaskMapper;
import com.sinosoft.taxbenefit.core.generated.model.TaxCont;
import com.sinosoft.taxbenefit.core.generated.model.TaxContRenew;
import com.sinosoft.taxbenefit.core.generated.model.TaxContRenewExample;
import com.sinosoft.taxbenefit.core.generated.model.TaxFaultTask;
import com.sinosoft.taxbenefit.core.policy.biz.service.PolicyService;
import com.sinosoft.taxbenefit.core.renewalcancel.biz.service.RenewalCancelCoreService;
import com.sinosoft.taxbenefit.core.renewalcancel.dto.ReqRenewalCancelDTO;
import com.sinosoft.taxbenefit.core.renewalcancel.dto.ReqRenewalCancelPolicyDTO;
import com.sinosoft.taxbenefit.core.renewalcancel.dto.ResRenewalCancelDTO;

@Service
public class RenewalCancelCoreServiceImpl extends TaxBaseService implements RenewalCancelCoreService {
	@Autowired
	CommonCodeService commonCodeService;
	@Autowired
	FaultTaskService faultTaskService;
	@Autowired
	TaxFaultTaskMapper taxFaultTaskMapper;//异常任务
	@Autowired
	TaxContRenewMapper taxContRenewMapper;
	@Autowired
	PolicyService policyService;
	@Autowired
	TaxContMapper taxContMapper;
	
	@Override
	public ThirdSendResponseDTO renewlCancel(RequestRenewalCancelDTO requestRenewalCancelDTO,ParamHeadDTO paramHead) {
		ThirdSendResponseDTO thirdSendResponse = new ThirdSendResponseDTO();
		List<ReqBusinessDetailDTO> reqBusinessDetailList = new ArrayList<ReqBusinessDetailDTO>();   
		try {
			ReqBusinessDTO businessDTO = commonCodeService.initReqBusinessDTO();
			logger.info("续保撤销上传（核心请求）JSON 体数据内容：" + JSONObject.toJSONString(requestRenewalCancelDTO));
			//1.将续期保费上传请求dto转换为中保信续期保费上传请求
			ReqRenewalCancelDTO reqRenewalCancelDTO = this.convertCarrier(requestRenewalCancelDTO);
			//2.调用中保信接口服务,获得返回结果   存储调用中保信日志表
			ThirdSendDTO thirdSendDTO = commonCodeService.createThirdSend(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_RENEWAL_CANCEL.code(), paramHead, reqRenewalCancelDTO);
			logger.info("续保撤销上传（请求中保信）JSON 数据内容："+JSONObject.toJSONString(thirdSendDTO));
			Holder<ResponseHeader> responseHeaderHolder = new Holder<ResponseHeader>();
			Holder<ResponseBody> responseBodyHolder = new Holder<ResponseBody>();
			EbaoServerPortDTO ebaoServerPort = commonCodeService.queryEBaoServerInfoByPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_RENEWAL_CANCEL.code());
			String portUrl = ebaoServerPort.getPortUrl();
			URL wsdlLocation = new URL(portUrl);
			PolicyRenewalCancelService_Service service = new PolicyRenewalCancelService_Service(wsdlLocation);
			PolicyRenewalCancelService servicePort = service.getPolicyRenewalCancelServicePort();
			BindingProvider bp = (BindingProvider) servicePort;
			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, portUrl);
			servicePort.renewalCancel(thirdSendDTO.getHead(), thirdSendDTO.getBody(), responseHeaderHolder, responseBodyHolder);
			ResponseHeader responseHeader = responseHeaderHolder.value;
			ResponseBody responseBody = responseBodyHolder.value;
			logger.info("续保撤销上传（中保信响应）JSON 数据内容："+responseBody.getJsonString());
			//封装中保信返回报文
			ResponseJSON responseJSON = new ResponseJSON();
			responseJSON.setResponseBody(responseBody);
			responseJSON.setResponseHeader(responseHeader);
			//封装发送报文日志对象
			SendLogDTO sendLog = new SendLogDTO();
			sendLog.setSerialNo(paramHead.getSerialNo());
			sendLog.setPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_RENEWAL_CANCEL.code());
			sendLog.setRequestContent(JSONObject.toJSONString(thirdSendDTO));
			sendLog.setResponseContent(JSONObject.toJSONString(responseJSON));
			sendLog.setTargetCode(ENUM_SENDSERVICE_CODE.EBAO.code());
			sendLog.setState(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
			sendLog.setDescription(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
			commonCodeService.addLogSend(sendLog);
			//封装返回核心dto;
			if(ENUM_RESULT_CODE.SUCCESS.code().equals(responseHeader.getResultCode())
					|| ENUM_RESULT_CODE.INPUT_LOCALITY_SUCCESS.code().equals(responseHeader.getResultCode())){
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
				thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
				updateContRenew(requestRenewalCancelDTO);
			}else{
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
				List list = responseHeader.getResultMessage().getMessage();
				if(list != null && list.size() > 0){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : responseHeader.getResultMessage().getMessage()){
						sbStr.append(mess).append("|");
					}
					String message = sbStr.substring(0, sbStr.length()-1).toString();
					thirdSendResponse.setResultDesc(message);
				}else{
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
				}
			}
			if(!StringUtil.isEmpty(responseBody.getJsonString())){
				ResRenewalCancelDTO resRenewalCancelDTO = JSONObject.parseObject(responseBody.getJsonString(), ResRenewalCancelDTO.class);
				//获取业务数据
				ResultRenewalCancelDTO resultRenewalCancelDTO = new ResultRenewalCancelDTO();
				resultRenewalCancelDTO.setBizNo(requestRenewalCancelDTO.getBizNo());
				resultRenewalCancelDTO.setRenewalCancelSequenceNo(resRenewalCancelDTO.getRenewalCancelSequenceNo());
				resultRenewalCancelDTO.setRenewalEndorsementNo(resRenewalCancelDTO.getRenewalEndorsementNo());
				EBResultCodeDTO eBResultCode = resRenewalCancelDTO.getResult();
				HXResultCodeDTO hxResultCode = new HXResultCodeDTO();
				hxResultCode.setResultCode(eBResultCode.getResultCode());
				String message = "";
				if(null != eBResultCode.getResultMessage()){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : eBResultCode.getResultMessage()){
						sbStr.append(mess).append("|");
					}
					message = sbStr.substring(0, sbStr.length()-1).toString();
				}
				hxResultCode.setResultMessage(message);
				resultRenewalCancelDTO.setResult(hxResultCode);
				thirdSendResponse.setResJson(resultRenewalCancelDTO);
				if(ENUM_RESULT_CODE.SUCCESS.code().equals(eBResultCode.getResultCode())){
					businessDTO.setReqSuccessNum(businessDTO.getReqSuccessNum() + 1);
				}else{
					businessDTO.setReqFailNum(businessDTO.getReqFailNum() + 1);
					ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
					reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
					reqBusinessDetailDTO.setResultMessage(message);
					reqBusinessDetailList.add(reqBusinessDetailDTO);
				}
			}else{
				businessDTO.setReqFailNum(responseHeader.getRecordNum());
				ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
				reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
				reqBusinessDetailDTO.setResultMessage(thirdSendResponse.getResultDesc());
				reqBusinessDetailList.add(reqBusinessDetailDTO);
			}
			commonCodeService.addReqBusinessDetail(reqBusinessDetailList);
			businessDTO.setRequetNum(responseHeader.getRecordNum());
			commonCodeService.addReqBusinessCollate(businessDTO);
		} catch (Exception e) {
			logger.error("续保撤销后置处理发生异常!");
			e.printStackTrace();
			thirdSendResponse.setResJson(e.getMessage());
			thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
			thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
			// 存储任务表
			TaxFaulTaskDTO taxFaultTask = new TaxFaulTaskDTO();
			taxFaultTask.setSerialNo(paramHead.getSerialNo());
			taxFaultTask.setServerPortCode(paramHead.getPortCode());
			taxFaultTask.setServerPortName(paramHead.getPortName());
			taxFaultTask.setAreaCode(paramHead.getAreaCode());
			taxFaultTask.setDisposeType(ENUM_DISPOSE_TYPE.ERROR.code());
			faultTaskService.addTaxFaultTask(taxFaultTask);
		}
		logger.error("续保撤销后置处理结束!");
		return thirdSendResponse;
	}

	/**
	 * 承保撤销数据持久化
	 * @param requestRenewalCancelDTO
	 */
	private void updateContRenew(RequestRenewalCancelDTO requestRenewalCancelDTO) {
		try {
			//4.存储业务表数据
			String policyNo = requestRenewalCancelDTO.getPolicyNo();
			TaxContRenewExample example = new TaxContRenewExample();
			example.createCriteria().andContNoEqualTo(policyNo).andRcStateEqualTo(ENUM_RC_STATE.EFFECTIVE.getCode());
			TaxContRenew taxContRenew = taxContRenewMapper.selectByExample(example).get(0);
			taxContRenew.setRcState(ENUM_RC_STATE.INVALID.getCode());
			taxContRenewMapper.updateByPrimaryKeySelective(taxContRenew);
			TaxCont taxCont = policyService.queryPolicyByPolicyNo(policyNo);
			if(taxCont!=null){
				taxCont.setExpireDate(taxCont.getValiDate());
				taxContMapper.updateByPrimaryKeySelective(taxCont);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("续保撤销上传数据持久化失败"+e.getMessage());
		}
	}
	/**
	 * 核心DTO转中保信DTO
	 * @param requestRenewalCancelDTO
	 * @return
	 */
	private ReqRenewalCancelDTO convertCarrier(RequestRenewalCancelDTO requestRenewalCancelDTO){
		ReqRenewalCancelDTO reqRenewalCancelDTO = new ReqRenewalCancelDTO();
		ReqRenewalCancelPolicyDTO reqRenewalCancelPolicyDTO = new ReqRenewalCancelPolicyDTO();
		reqRenewalCancelPolicyDTO.setPolicyNo(requestRenewalCancelDTO.getPolicyNo());
		reqRenewalCancelPolicyDTO.setRenewalEndorsementNo(requestRenewalCancelDTO.getRenewalEndorsementNo());
		reqRenewalCancelPolicyDTO.setCancelDate(requestRenewalCancelDTO.getCancelDate());
		reqRenewalCancelPolicyDTO.setCancelReason(requestRenewalCancelDTO.getCancelReason());
		reqRenewalCancelDTO.setPolicy(reqRenewalCancelPolicyDTO);
		return reqRenewalCancelDTO;
	}

	@Override
	public ThirdSendResponseDTO renewalCancelExceptionJob(String reqJSON, String portCode, TaxFaultTask faultTask) {
		logger.info("进入续保撤销异常任务服务");
		ThirdSendResponseDTO thirdSendResponse = new ThirdSendResponseDTO();
		List<ReqBusinessDetailDTO> reqBusinessDetailList = new ArrayList<ReqBusinessDetailDTO>();   
		try {
			ReqBusinessDTO businessDTO = commonCodeService.initReqBusinessDTO();
			ParamHeadDTO paramHead = new ParamHeadDTO();
			CoreRequestHead coreRequestHead = new CoreRequestHead();
			RequestRenewalCancelDTO requestRenewalCancelDTO = new RequestRenewalCancelDTO();
			//1. 解析请求报文
			if (ENUM_HX_SERVICE_PORT_CODE.POLICY_RENEWAL_CANCEL.code().equals(portCode)) {
				RequestRenewalCancelCoreDTO requestRenewalCancelCore = JSONObject.parseObject(reqJSON, RequestRenewalCancelCoreDTO.class);
				requestRenewalCancelDTO = (RequestRenewalCancelDTO) requestRenewalCancelCore.getBody();
				coreRequestHead = requestRenewalCancelCore.getRequestHead();
			} else {
				throw new BusinessDataErrException("请求接口编码有误! 错误接口编码为："+portCode);
			}
			//2. 封装头部信息
			paramHead.setAreaCode(coreRequestHead.getAreaCode());
			paramHead.setRecordNum(coreRequestHead.getRecordNum());
			paramHead.setSerialNo(coreRequestHead.getSerialNo());
			paramHead.setPortCode(portCode);
			ThirdSendDTO thirdSendDTO = new ThirdSendDTO();
			EbaoServerPortDTO ebaoServerPort = null;
			// 将续保撤销请求dto转换为中保信续保撤销请求dto
			ReqRenewalCancelDTO reqRenewalCancelDTO = this.convertCarrier(requestRenewalCancelDTO);
			thirdSendDTO = commonCodeService.createThirdSend(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_RENEWAL_CANCEL.code(), paramHead, reqRenewalCancelDTO);
			ebaoServerPort = commonCodeService.queryEBaoServerInfoByPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_RENEWAL_CANCEL.code());
			logger.info("异常任务 - 续保撤销（请求中保信）JSON 数据内容(错误或异常类型)："+JSONObject.toJSONString(thirdSendDTO));
			Holder<ResponseHeader> responseHeaderHolder = new Holder<ResponseHeader>();
			Holder<ResponseBody> responseBodyHolder = new Holder<ResponseBody>();
			//4. 调用统一的预约码查询接口地址
			String portUrl = ebaoServerPort.getPortUrl();
			URL wsdlLocation = new URL(portUrl);
			PolicyRenewalCancelService_Service service = new PolicyRenewalCancelService_Service(wsdlLocation);
			PolicyRenewalCancelService servicePort = service.getPolicyRenewalCancelServicePort();
			BindingProvider bp = (BindingProvider) servicePort;
			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, portUrl);
			servicePort.renewalCancel(thirdSendDTO.getHead(), thirdSendDTO.getBody(), responseHeaderHolder, responseBodyHolder);
			ResponseHeader responseHeader = responseHeaderHolder.value;
			ResponseBody responseBody = responseBodyHolder.value;
			logger.info("续保撤销（中保信响应）JSON 数据内容：" + responseBody.getJsonString());
			// 初始化头部错误信息
			String messageBody = ENUM_BZ_RESPONSE_RESULT.FAIL.description();
			//封装返回核心dto;
			if(ENUM_RESULT_CODE.SUCCESS.code().equals(responseHeader.getResultCode())
					|| ENUM_RESULT_CODE.INPUT_LOCALITY_SUCCESS.code().equals(responseHeader.getResultCode())){
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
				thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
				this.updateContRenew(requestRenewalCancelDTO);
			}else{
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
				List list = responseHeader.getResultMessage().getMessage();
				if(list != null && list.size() > 0){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : responseHeader.getResultMessage().getMessage()){
						sbStr.append(mess).append("|");
					}
					String message = sbStr.substring(0, sbStr.length()-1).toString();
					thirdSendResponse.setResultDesc(message);
				}else{
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
				}
				faultTask.setDisposeResult(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
			}
			//获取业务数据
			if(!StringUtil.isEmpty(responseBody.getJsonString())){
				ResRenewalCancelDTO resRenewalCancelDTO = JSONObject.parseObject(responseBody.getJsonString(), ResRenewalCancelDTO.class);
				ResultRenewalCancelDTO resultRenewalCancelDTO = new ResultRenewalCancelDTO();
				resultRenewalCancelDTO.setBizNo(requestRenewalCancelDTO.getBizNo());
				resultRenewalCancelDTO.setRenewalCancelSequenceNo(resRenewalCancelDTO.getRenewalCancelSequenceNo());
				resultRenewalCancelDTO.setRenewalEndorsementNo(resRenewalCancelDTO.getRenewalEndorsementNo());
				EBResultCodeDTO eBResultCode = resRenewalCancelDTO.getResult();
				HXResultCodeDTO hxResultCode = new HXResultCodeDTO();
				hxResultCode.setResultCode(eBResultCode.getResultCode());
				String message = "";
				if(null != eBResultCode.getResultMessage()){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : eBResultCode.getResultMessage()){
						sbStr.append(mess).append("|");
					}
					message = sbStr.substring(0, sbStr.length()-1).toString();
				}
				hxResultCode.setResultMessage(message);
				resultRenewalCancelDTO.setResult(hxResultCode);
				thirdSendResponse.setResJson(resultRenewalCancelDTO);
				if(ENUM_RESULT_CODE.SUCCESS.code().equals(eBResultCode.getResultCode())){
					businessDTO.setReqSuccessNum(businessDTO.getReqSuccessNum() + 1);
				}else{
					businessDTO.setReqFailNum(businessDTO.getReqFailNum() + 1);
					ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
					reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
					reqBusinessDetailDTO.setResultMessage(message);
					reqBusinessDetailList.add(reqBusinessDetailDTO);
				}
			}else{
				businessDTO.setReqFailNum(responseHeader.getRecordNum());
				thirdSendResponse.setResJson(messageBody);
				ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
				reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
				reqBusinessDetailDTO.setResultMessage(thirdSendResponse.getResultDesc());
				reqBusinessDetailList.add(reqBusinessDetailDTO);
			}
			commonCodeService.addReqBusinessDetail(reqBusinessDetailList);
			businessDTO.setRequetNum(responseHeader.getRecordNum());
			commonCodeService.addReqBusinessCollate(businessDTO);
			faultTask.setDisposeResult(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("续保撤销异常任务服务方法处理发生异常! 异常信息为："+e.getMessage());
			thirdSendResponse.setResJson(e.getMessage());
			thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
			thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
			faultTask.setDisposeType(ENUM_DISPOSE_TYPE.ERROR.code());
			faultTask.setDisposeResult(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
		} finally{
			faultTask.setSuccessMessage(JSONObject.toJSONString(thirdSendResponse));
			faultTask.setDisposeDate(DateUtil.getCurrentDate());
			taxFaultTaskMapper.updateByPrimaryKeySelective(faultTask);
		}
		return thirdSendResponse;
	}
}