package com.sinosoft.taxbenefit.core.common.dto;

/**
 * 中保信接口请求路径
 * @author SheChunMing
 */
public class EbaoServerPortDTO {
	// 接口编码
	private String portCode;
	// 接口名称
	private String portName;
	// 接口请求路径
	private String portUrl;

	public String getPortCode() {
		return portCode;
	}

	public void setPortCode(String portCode) {
		this.portCode = portCode;
	}

	public String getPortName() {
		return portName;
	}

	public void setPortName(String portName) {
		this.portName = portName;
	}

	public String getPortUrl() {
		return portUrl;
	}

	public void setPortUrl(String portUrl) {
		this.portUrl = portUrl;
	}
	
}
