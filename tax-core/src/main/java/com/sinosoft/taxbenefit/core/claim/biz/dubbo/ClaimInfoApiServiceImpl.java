package com.sinosoft.taxbenefit.core.claim.biz.dubbo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimReversalInfoDTO;
import com.sinosoft.taxbenefit.api.inter.ClaimInfoApiService;
import com.sinosoft.taxbenefit.core.claim.biz.service.ClaimInfoCoreService;

/**
 * 理赔服务接口实现
 * 
 * @author zhangke
 *
 */
@Service("claimInfoApiServiceImpl")
public class ClaimInfoApiServiceImpl extends TaxBaseService implements ClaimInfoApiService {
	@Autowired
	ClaimInfoCoreService claimInfoCoreService;

	@Override
	public ThirdSendResponseDTO claimSettlement(ReqClaimInfoDTO reqClaimInfoDTO,ParamHeadDTO paramHeadDTO) {
		List<ReqClaimInfoDTO> reqClaimInfoDTOList=new ArrayList<ReqClaimInfoDTO>();
		reqClaimInfoDTOList.add(reqClaimInfoDTO);
		return claimInfoCoreService.addClaimSettlement(reqClaimInfoDTOList,paramHeadDTO);
	}

	@Override
	public ThirdSendResponseDTO claimSettlementM(List<ReqClaimInfoDTO> reqClaimInfoDTOs, ParamHeadDTO paramHeadDTO) {
		 return claimInfoCoreService.addClaimSettlement(reqClaimInfoDTOs,paramHeadDTO);
	}

	@Override
	public ThirdSendResponseDTO claimReversal(ReqClaimReversalInfoDTO infoDTO, ParamHeadDTO paramHeadDTO) {	
		return claimInfoCoreService.modifyClaimState(infoDTO,paramHeadDTO);
	}
}
