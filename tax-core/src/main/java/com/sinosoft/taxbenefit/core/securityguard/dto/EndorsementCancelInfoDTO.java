package com.sinosoft.taxbenefit.core.securityguard.dto;

/**
 * 保全撤销请求中保信信息DTO
 * 
 * @author zhangke
 *
 */
public class EndorsementCancelInfoDTO {
	/** 保单号 */
	private String policyNo;
	/** 保全批单号 */
	private String endorsementNo;

	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public String getEndorsementNo() {
		return endorsementNo;
	}

	public void setEndorsementNo(String endorsementNo) {
		this.endorsementNo = endorsementNo;
	}
}
