package com.sinosoft.taxbenefit.core.securityguard.dto;

import java.math.BigDecimal;
import java.util.List;

/**
 * 险种信息
 * 
 * @author zhanghaosh@sinosoft.com.cn
 *
 */
public class CoverageInfoDTO {
	// 核保信息
	private UnderwritingInfoDTO underwriting;
	// 责任信息
	private List<LiabilityInfoDTO> liabilityList;

	

	public UnderwritingInfoDTO getUnderwriting() {
		return underwriting;
	}

	public void setUnderwriting(UnderwritingInfoDTO underwriting) {
		this.underwriting = underwriting;
	}

	public List<LiabilityInfoDTO> getLiabilityList() {
		return liabilityList;
	}

	public void setLiabilityList(List<LiabilityInfoDTO> liabilityList) {
		this.liabilityList = liabilityList;
	}

	/** 产品组代码 Y **/
	private String coveragePackageCode;
	/** 险种代码 Y **/
	private String comCoverageCode;
	/** 险种名称 Y **/
	private String comCoverageName;
	/** 险种保费 Y **/
	private String coveragePremium;
	/** 险种当期风险保费 Y **/
	private BigDecimal riskPremium;
	/** 投保年龄 Y **/
	private Integer ageOfInception;
	/** 险种生效日期 Y **/
	private String coverageEffectiveDate;
	/** 险种满期日期 Y **/
	private String coverageExpirationDate;
	/** 险种终止日期 S **/
	private String terminationDate;
	/** 险种中止日期 S **/
	private String suspendDate;
	/** 险种效力恢复日期 S **/
	private String recoverDate;
	/** 主附险标志 Y **/
	private String coverageType;
	/** 险种保额 Y **/
	private BigDecimal sa;
	/** 下期应交日 Y **/
	private String payDueDay;
	/** 是否允许自动续保 Y **/
	private String renewIndi;
	/** 险种状态 Y **/
	private String coverageStatus;
	/** 险种终止原因 S **/
	private String terminationReason;
	/** 险种退保金 S **/
	private BigDecimal surrenderAmount;

	public String getCoveragePackageCode() {
		return coveragePackageCode;
	}

	public void setCoveragePackageCode(String coveragePackageCode) {
		this.coveragePackageCode = coveragePackageCode;
	}

	public String getComCoverageCode() {
		return comCoverageCode;
	}

	public void setComCoverageCode(String comCoverageCode) {
		this.comCoverageCode = comCoverageCode;
	}

	public String getComCoverageName() {
		return comCoverageName;
	}

	public void setComCoverageName(String comCoverageName) {
		this.comCoverageName = comCoverageName;
	}

	public String getCoveragePremium() {
		return coveragePremium;
	}

	public void setCoveragePremium(String coveragePremium) {
		this.coveragePremium = coveragePremium;
	}

	public BigDecimal getRiskPremium() {
		return riskPremium;
	}

	public void setRiskPremium(BigDecimal riskPremium) {
		this.riskPremium = riskPremium;
	}

	


	public Integer getAgeOfInception() {
		return ageOfInception;
	}

	public void setAgeOfInception(Integer ageOfInception) {
		this.ageOfInception = ageOfInception;
	}

	public String getCoverageEffectiveDate() {
		return coverageEffectiveDate;
	}

	public void setCoverageEffectiveDate(String coverageEffectiveDate) {
		this.coverageEffectiveDate = coverageEffectiveDate;
	}

	public String getCoverageExpirationDate() {
		return coverageExpirationDate;
	}

	public void setCoverageExpirationDate(String coverageExpirationDate) {
		this.coverageExpirationDate = coverageExpirationDate;
	}

	public String getTerminationDate() {
		return terminationDate;
	}

	public void setTerminationDate(String terminationDate) {
		this.terminationDate = terminationDate;
	}

	public String getSuspendDate() {
		return suspendDate;
	}

	public void setSuspendDate(String suspendDate) {
		this.suspendDate = suspendDate;
	}

	public String getRecoverDate() {
		return recoverDate;
	}

	public void setRecoverDate(String recoverDate) {
		this.recoverDate = recoverDate;
	}

	public String getCoverageType() {
		return coverageType;
	}

	public void setCoverageType(String coverageType) {
		this.coverageType = coverageType;
	}

	public BigDecimal getSa() {
		return sa;
	}

	public void setSa(BigDecimal sa) {
		this.sa = sa;
	}

	public String getPayDueDay() {
		return payDueDay;
	}

	public void setPayDueDay(String payDueDay) {
		this.payDueDay = payDueDay;
	}

	public String getRenewIndi() {
		return renewIndi;
	}

	public void setRenewIndi(String renewIndi) {
		this.renewIndi = renewIndi;
	}

	public String getCoverageStatus() {
		return coverageStatus;
	}

	public void setCoverageStatus(String coverageStatus) {
		this.coverageStatus = coverageStatus;
	}

	public String getTerminationReason() {
		return terminationReason;
	}

	public void setTerminationReason(String terminationReason) {
		this.terminationReason = terminationReason;
	}

	public BigDecimal getSurrenderAmount() {
		return surrenderAmount;
	}

	public void setSurrenderAmount(BigDecimal surrenderAmount) {
		this.surrenderAmount = surrenderAmount;
	}

}
