package com.sinosoft.taxbenefit.core.faultTask.biz.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.platform.common.config.SystemConstants;
import com.sinosoft.platform.common.util.DateUtil;
import com.sinosoft.platform.common.util.StringUtil;
import com.sinosoft.taxbenefit.api.config.ENUM_BZ_RESPONSE_RESULT;
import com.sinosoft.taxbenefit.api.config.ENUM_DISPOSE_STATE;
import com.sinosoft.taxbenefit.api.dto.common.FaultTaskDTO;
import com.sinosoft.taxbenefit.api.dto.request.TaxFaulTaskDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.core.common.biz.service.CommonCodeService;
import com.sinosoft.taxbenefit.core.common.config.ENUM_RC_STATE;
import com.sinosoft.taxbenefit.core.faultTask.biz.service.FaultTaskService;
import com.sinosoft.taxbenefit.core.faultTask.dto.AsynInteractionDTO;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxAsynInteractionMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxFaultTaskMapper;
import com.sinosoft.taxbenefit.core.generated.model.TaxAsynInteraction;
import com.sinosoft.taxbenefit.core.generated.model.TaxAsynInteractionExample;
import com.sinosoft.taxbenefit.core.generated.model.TaxFaultTask;
import com.sinosoft.taxbenefit.core.generated.model.TaxFaultTaskExample;

@Service
public class FaultTaskServiceImpl implements FaultTaskService {
	@Autowired
	TaxFaultTaskMapper faultTaskMapper;
	@Autowired
	CommonCodeService commonCodeService;
	@Autowired
	TaxAsynInteractionMapper taxAsynInteractionMapper;
	@Override
	public Integer addTaxFaultTask(TaxFaulTaskDTO taxFaultTaskDTO) {
		TaxFaultTask faultTask = new TaxFaultTask();
		faultTask.setSerialNo(taxFaultTaskDTO.getSerialNo());
		faultTask.setAreaCode(taxFaultTaskDTO.getAreaCode());
		faultTask.setCreateDate(DateUtil.getCurrentDate());
		faultTask.setCreateId(SystemConstants.SYS_OPERATOR);
		faultTask.setServerPortCode(taxFaultTaskDTO.getServerPortCode());
		faultTask.setServerPortName(taxFaultTaskDTO.getServerPortName());
		faultTask.setDisposeType(taxFaultTaskDTO.getDisposeType());
		if(!StringUtil.isBlank(taxFaultTaskDTO.getBookSequenceNo())){
			faultTask.setRemark(taxFaultTaskDTO.getBookSequenceNo());
		}
		faultTask.setAsynContent(taxFaultTaskDTO.getAsynContent());
		faultTask.setSuccessMessage(JSONObject.toJSONString(commonCodeService.initResponseJSONLoading()));
		faultTask.setDisposeResult(ENUM_DISPOSE_STATE.WAIT.code());
		faultTask.setRcState(ENUM_RC_STATE.EFFECTIVE.getCode());
		faultTaskMapper.insert(faultTask);
		return faultTask.getSid();
	}

	@Override
	public FaultTaskDTO queryTaxFaultTaskBySerialNo(String serialNo) {
		TaxFaultTaskExample example = new TaxFaultTaskExample();
		example.createCriteria().andSerialNoEqualTo(serialNo);
		List<TaxFaultTask> list = faultTaskMapper.selectByExample(example);
		FaultTaskDTO taxFaultTaskDTO = new FaultTaskDTO();
		if (list.size() > 0) {
			TaxFaultTask faultTask = list.get(0);
			taxFaultTaskDTO.setMessage(faultTask.getSuccessMessage());
			taxFaultTaskDTO.setState(faultTask.getDisposeResult());
			return taxFaultTaskDTO;
		}
		return null;
	}
	
	
	@Override
	public List<TaxFaultTask> queryTaxFaultTaskByStateCode(String disposeType,String stateCode) {
		TaxFaultTaskExample example = new TaxFaultTaskExample();
		example.createCriteria().andDisposeTypeEqualTo(disposeType).andDisposeResultEqualTo(stateCode).andRcStateEqualTo(ENUM_RC_STATE.EFFECTIVE.getCode());
		List<TaxFaultTask> list = faultTaskMapper.selectByExample(example);
		return list;
	}

	@Override
	public List<TaxFaultTask> queryUnfinishedTaskByStateCode(String stateCode) {
		TaxFaultTaskExample example = new TaxFaultTaskExample();
		example.createCriteria().andDisposeResultEqualTo(stateCode).andRcStateEqualTo(ENUM_RC_STATE.EFFECTIVE.getCode());
		return faultTaskMapper.selectByExample(example);
	}
	
	@Override
	public void updateFaultTaskResMessageBySerialNo(String serialNo, String resmessage, String resultCode) {
		TaxFaultTaskExample example = new TaxFaultTaskExample();
		example.createCriteria().andSerialNoEqualTo(serialNo);
		List<TaxFaultTask> faultTaskList = faultTaskMapper.selectByExample(example);
		if(null != faultTaskList && faultTaskList.size() == 1){
			TaxFaultTask faultTask = faultTaskList.get(0);
			faultTask.setSuccessMessage(resmessage);
			faultTask.setDisposeResult(resultCode);
			faultTask.setDisposeDate(DateUtil.getCurrentDate());
			faultTaskMapper.updateByPrimaryKeySelective(faultTask);
		}
	}
	
	@Override
	public void updateFaultTaskDisportStateBySerialNo(String serialNo) {
		TaxFaultTaskExample example = new TaxFaultTaskExample();
		example.createCriteria().andSerialNoEqualTo(serialNo);
		List<TaxFaultTask> faultTaskList = faultTaskMapper.selectByExample(example);
		if(null != faultTaskList && faultTaskList.size() == 1){
			TaxFaultTask faultTask = faultTaskList.get(0);
			faultTask.setDisposeResult(ENUM_DISPOSE_STATE.SUCCES.code());
			ThirdSendResponseDTO thirdSendResponse = new ThirdSendResponseDTO();
			thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
			thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
			thirdSendResponse.setResJson(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
			faultTask.setSuccessMessage(JSONObject.toJSONString(thirdSendResponse));
			faultTaskMapper.updateByPrimaryKeySelective(faultTask);
		}
	}

	@Override
	public void insertAsynInteraction(AsynInteractionDTO asynInter) {
		TaxAsynInteraction tTaxAsynInteraction = new TaxAsynInteraction();
		tTaxAsynInteraction.setMainSerialNo(asynInter.getMainSerialNo());
		tTaxAsynInteraction.setSonSerialNo(asynInter.getSonSerialNo());
		tTaxAsynInteraction.setBusinessNo(asynInter.getBusNo());
		tTaxAsynInteraction.setCreateId(SystemConstants.SYS_OPERATOR);
		tTaxAsynInteraction.setCreateDate(DateUtil.getCurrentDate());
		tTaxAsynInteraction.setRcState(ENUM_RC_STATE.EFFECTIVE.getCode());
		taxAsynInteractionMapper.insert(tTaxAsynInteraction);
		this.updateFaultTaskDisportStateBySerialNo(asynInter.getSonSerialNo());
	}

	@Override
	public List<TaxAsynInteraction> selectTaxAsynInteractionByMainSreialNo(String serialNo) {
		TaxAsynInteractionExample example = new TaxAsynInteractionExample();
		example.createCriteria().andMainSerialNoEqualTo(serialNo).andRcStateEqualTo(ENUM_RC_STATE.EFFECTIVE.getCode());
		return taxAsynInteractionMapper.selectByExample(example);
	}

}
