package com.sinosoft.taxbenefit.core.taxbenvrify.dto;

import java.util.List;
/**
 * 中保信税优验证返回对象
 * @author zhangyu
 * @date 2016年3月10日 上午9:44:20
 */
public class ResTaxBeneVerifyDTO {
	
	private List<TaxBeneVerifyDTO> clientList;
	
	public List<TaxBeneVerifyDTO> getClientList() {
		return clientList;
	}
	public void setClientList(List<TaxBeneVerifyDTO> clientList) {
		this.clientList = clientList;
	}
}