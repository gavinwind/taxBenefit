package com.sinosoft.taxbenefit.core.common.config;

/**
 * 服务接口编码（上传和撤销）
 * @author zhanghao
 *
 */

public enum ENUM_HX_SERVICE_PORT_CODE {
	APPLICANT_ONE("NBU001","单个承保信息上传"),
	APPLICANT_MORE("NBU002","批量承保信息上传"),
	CUSTOMER_VERIFY_ONE("PTY001","单个客户验证上传"),
	CUSTOMER_VERIFY_MORE("PTY002","批量客户验证上传"),
	TAX_BENE_VERIFY("PTY003","税优验证"),
	INSURED_PAY_SEL("PTY004","被保人既往理赔额查询"),
	POLICY_REVERSAL("NBU003","承保撤销上传"),
	SERCURITY_CHANGE("END001","保全信息上传请求"),
	SERCURITY_CANCEL("END002","保全撤销上传请求"),
	POLICY_STATE_CHANGE_ONE("END003","单个保单状态修改上传"),
	POLICY_STATE_CHANGE_MORE("END011","批量保单状态修改上传"),
	POLICY_IN_TRANSFER_UPLOAD_ONE("END004","单个保单外部转入申请上传"),
	POLICY_IN_TRANSFER_UPLOAD_MORE("END005","批量保单外部转入申请上传"),
	POLICY_OUT_TRANSFER_RECORD_ONE("END007","单个保单转出登记请求"),	
	POLICY_OUT_TRANSFER_RECORD_MORE("END008","批量保单转出登记请求"),
	POLICY_PREMIUM_UPLOAD_ONE("PRM001", "单个续期保费信息上传"),
	POLICY_PREMIUM_UPLOAD_MORE("PRM002", "批量续期保费信息上传"),
	POLICY_RENEWAL_UPLOAD_ONE("RNW001","单个续保信息上传"),
	POLICY_RENEWAL_UPLOAD_MORE("RNW002","批量续保信息上传"),
	POLICY_RENEWAL_CANCEL("RNW003","续保撤销上传"),
	CLAIM_SETTLEMEN_ONE("CLM001","单个理赔信息上传"),
	CLAIM_SETTLEMEN_MORE("CLM002","批量理赔信息上传"),
	CLAIM_REVERSAL("CLM003","理赔撤销上传"),
	ACCONUT_CHANGE_ONE("ACT001","单个账户变更上传"),
	ACCONUT_CHANGE_MORE("ACT002","批量账户变更上传");
	
	private final String code;
	private final String desc;
	
	ENUM_HX_SERVICE_PORT_CODE(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public String code() {
		return code;
	}

	public String desc() {
		return desc;
	}
	public static ENUM_HX_SERVICE_PORT_CODE getEnumByKey(String key){
		for(ENUM_HX_SERVICE_PORT_CODE enumItem:ENUM_HX_SERVICE_PORT_CODE.values()){
			if(key.equals(enumItem.code())){
				return enumItem;
			}
		}
		return null;
	}
	
	
}
