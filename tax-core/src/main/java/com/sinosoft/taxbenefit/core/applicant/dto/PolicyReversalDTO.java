package com.sinosoft.taxbenefit.core.applicant.dto;

/**
 * 请求中报信承保撤销报文DTO
 * 
 * @author zhangke
 *
 */
public class PolicyReversalDTO {
	/** 保单撤销信息 */
	private PolicyReversalInfoDTO policyCancel;

	public PolicyReversalInfoDTO getPolicyCancel() {
		return policyCancel;
	}

	public void setPolicyCancel(PolicyReversalInfoDTO policyCancel) {
		this.policyCancel = policyCancel;
	}
}
