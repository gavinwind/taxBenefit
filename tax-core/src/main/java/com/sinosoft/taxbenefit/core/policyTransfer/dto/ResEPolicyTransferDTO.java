package com.sinosoft.taxbenefit.core.policyTransfer.dto;

import java.util.List;

/**
 * 中保信响应 - 保单转移返回解析DTO
 * @author SheChunMing
 */
public class ResEPolicyTransferDTO {
	// 转出信息
	private List<EPolicyTransferOutDTO> transferList;

	public List<EPolicyTransferOutDTO> getTransferList() {
		return transferList;
	}

	public void setTransferList(List<EPolicyTransferOutDTO> transferList) {
		this.transferList = transferList;
	}
}
