package com.sinosoft.taxbenefit.core.securityguard.biz.dubbo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.securityguard.ReqKeepBodyDTO;
import com.sinosoft.taxbenefit.api.inter.PreservationOfChangeApiService;
import com.sinosoft.taxbenefit.core.securityguard.biz.service.PreservationOfChangeService;

@Service("preservationOfChangeApiServiceImpl")
public class PreservationOfChangeApiServiceImpl extends TaxBaseService implements PreservationOfChangeApiService {

	@Autowired
	private PreservationOfChangeService preservationOfChangeService;

	public ThirdSendResponseDTO PreservationOfChange(ReqKeepBodyDTO body, ParamHeadDTO paramHeadDTO) {
		logger.info("保全信息上传转入后置处理");
		return preservationOfChangeService.modifySecurityGuard(body, paramHeadDTO);
	}
}
