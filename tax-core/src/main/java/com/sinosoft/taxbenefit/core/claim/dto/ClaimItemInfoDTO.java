package com.sinosoft.taxbenefit.core.claim.dto;

import java.util.List;

/**
 * 医疗费用
 * 
 * @author zhangke
 *
 */
public class ClaimItemInfoDTO {
	/** 医疗费用类型 **/
	private String itemCategory;
	/** 金额 **/
	private String amount;
	/** 费用扣减金额 **/
	private String deductibleAmount;
	/** 医疗费用明细 **/
	private List<ClaimDetailInfoDTO> detailList;

	public String getItemCategory() {
		return itemCategory;
	}

	public void setItemCategory(String itemCategory) {
		this.itemCategory = itemCategory;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getDeductibleAmount() {
		return deductibleAmount;
	}

	public void setDeductibleAmount(String deductibleAmount) {
		this.deductibleAmount = deductibleAmount;
	}

	public List<ClaimDetailInfoDTO> getDetailList() {
		return detailList;
	}

	public void setDetailList(List<ClaimDetailInfoDTO> detailList) {
		this.detailList = detailList;
	}

}
