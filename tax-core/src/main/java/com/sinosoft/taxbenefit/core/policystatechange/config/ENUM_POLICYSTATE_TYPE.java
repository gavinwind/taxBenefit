package com.sinosoft.taxbenefit.core.policystatechange.config;
/**
 * 保全变更类型
 * @author zhanghaosh@sinosoft.com.cn
 *
 */
public enum ENUM_POLICYSTATE_TYPE {
	POLICYSTATE_CHANGE("01","保单状态变更"),POLICY_RISK_STATE_CHANGE("02","保单险种状态变更");
	
	private final String code;
	private final String desc;
	
	ENUM_POLICYSTATE_TYPE(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public String code() {
		return code;
	}

	public String desc() {
		return desc;
	}
	public static ENUM_POLICYSTATE_TYPE getEnumByKey(String key){
		for(ENUM_POLICYSTATE_TYPE enumItem:ENUM_POLICYSTATE_TYPE.values()){
			if(key.equals(enumItem.code())){
				return enumItem;
			}
		}
		return null;
	}
	
	
}
