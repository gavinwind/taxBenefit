package com.sinosoft.taxbenefit.core.common.dto;

import java.util.Date;

/**
 * 交易核对DTO
 * @author SheChunMing
 */
public class ReqBusinessDTO {
	// 交易编号
	private Integer busid;
	// 交易日
	private Date businessDate;
	// 请求记录总数
	private Integer requetNum;
	// 请求记录成功数
	private Integer reqSuccessNum;
	// 请求记录失败数
	private Integer reqFailNum;
	// 核对记录总数
	private Integer collateNum;
	// 核对记录成功数
	private Integer colSuccessNum;
	// 核对记录失败数
	private Integer colFailNum;
	// 处理标记
	private String successFlag;
	
	public Integer getBusid() {
		return busid;
	}
	public void setBusid(Integer busid) {
		this.busid = busid;
	}
	public Date getBusinessDate() {
		return businessDate;
	}
	public void setBusinessDate(Date businessDate) {
		this.businessDate = businessDate;
	}
	public Integer getRequetNum() {
		return requetNum;
	}
	public void setRequetNum(Integer requetNum) {
		this.requetNum = requetNum;
	}
	public Integer getReqSuccessNum() {
		return reqSuccessNum;
	}
	public void setReqSuccessNum(Integer reqSuccessNum) {
		this.reqSuccessNum = reqSuccessNum;
	}
	public Integer getReqFailNum() {
		return reqFailNum;
	}
	public void setReqFailNum(Integer reqFailNum) {
		this.reqFailNum = reqFailNum;
	}
	public Integer getCollateNum() {
		return collateNum;
	}
	public void setCollateNum(Integer collateNum) {
		this.collateNum = collateNum;
	}
	public Integer getColSuccessNum() {
		return colSuccessNum;
	}
	public void setColSuccessNum(Integer colSuccessNum) {
		this.colSuccessNum = colSuccessNum;
	}
	public Integer getColFailNum() {
		return colFailNum;
	}
	public void setColFailNum(Integer colFailNum) {
		this.colFailNum = colFailNum;
	}
	public String getSuccessFlag() {
		return successFlag;
	}
	public void setSuccessFlag(String successFlag) {
		this.successFlag = successFlag;
	}
}
