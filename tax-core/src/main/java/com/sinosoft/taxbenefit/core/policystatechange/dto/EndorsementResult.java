package com.sinosoft.taxbenefit.core.policystatechange.dto;

import com.sinosoft.taxbenefit.api.base.EBResultCodeDTO;
/**
 * 中保信保单状态修改返回Dto
 * @author zhanghao
 *
 */
public class EndorsementResult {

	/** 保单号 **/
	private String policyNo;
	/** 保全批单号 **/
	private String endorsementNo;
	/** 保单状态修改类型 **/
	private String policyStatusUpdateType;
	/** 保全编码 **/
	private String endorsementSequenceNo;
				   
	private EBResultCodeDTO result;

	public EBResultCodeDTO getResult() {
		return result;
	}

	public void setResult(EBResultCodeDTO result) {
		this.result = result;
	}

	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public String getEndorsementNo() {
		return endorsementNo;
	}

	public void setEndorsementNo(String endorsementNo) {
		this.endorsementNo = endorsementNo;
	}

	public String getPolicyStatusUpdateType() {
		return policyStatusUpdateType;
	}

	public void setPolicyStatusUpdateType(String policyStatusUpdateType) {
		this.policyStatusUpdateType = policyStatusUpdateType;
	}

	public String getEndorsementSequenceNo() {
		return endorsementSequenceNo;
	}

	public void setEndorsementSequenceNo(String endorsementSequenceNo) {
		this.endorsementSequenceNo = endorsementSequenceNo;
	}


}
