package com.sinosoft.taxbenefit.core.renewalpremium.dto;

import com.sinosoft.taxbenefit.api.base.EBResultCodeDTO;

/**
 * 续期中保信返回DTO
 * Title:ResPremiumResultDTO
 * @author yangdongkai@outlook.com
 * @date 2016年3月12日 下午2:57:35
 */
public class ResPremiumResultDTO {
	//保单号
	private String policyNo;
	//分单号
	private String sequenceNo;
	//费用编码
	private String feeId;
	//续期费用确认编码
	private String feeSequenceNo;
	//返回结果
	private EBResultCodeDTO result;
	
	/**
	 * @return the result
	 */
	public EBResultCodeDTO getResult() {
		return result;
	}
	/**
	 * @param result the result to set
	 */
	public void setResult(EBResultCodeDTO result) {
		this.result = result;
	}
	/**
	 * @return the policyNo
	 */
	public String getPolicyNo() {
		return policyNo;
	}
	/**
	 * @param policyNo the policyNo to set
	 */
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	/**
	 * @return the sequenceNo
	 */
	public String getSequenceNo() {
		return sequenceNo;
	}
	/**
	 * @param sequenceNo the sequenceNo to set
	 */
	public void setSequenceNo(String sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
	/**
	 * @return the feeId
	 */
	public String getFeeId() {
		return feeId;
	}
	/**
	 * @param feeId the feeId to set
	 */
	public void setFeeId(String feeId) {
		this.feeId = feeId;
	}
	/**
	 * @return the feeSequenceNo
	 */
	public String getFeeSequenceNo() {
		return feeSequenceNo;
	}
	/**
	 * @param feeSequenceNo the feeSequenceNo to set
	 */
	public void setFeeSequenceNo(String feeSequenceNo) {
		this.feeSequenceNo = feeSequenceNo;
	}
	
}
