package com.sinosoft.taxbenefit.core.common.config;

/**
 * 交易详情结果枚举
 * @author SheChunMing
 */
public enum ENUM_BUSINESS_RESULR {
	SUSSESS("01", "成功"), FAIL("02", "失败");
	private final String code;
	private final String desc;

	ENUM_BUSINESS_RESULR(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public String code() {
		return code;
	}

	public String desc() {
		return desc;
	}
}
