package com.sinosoft.taxbenefit.core.renewalcancel.biz.dubbo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalcancel.RequestRenewalCancelDTO;
import com.sinosoft.taxbenefit.api.inter.RenewalCancelApiService;
import com.sinosoft.taxbenefit.core.renewalcancel.biz.service.RenewalCancelCoreService;
/**
 * 续保撤销服务接口
 * Title:RenewalCancelApiServiceImpl
 * @author yangdongkai@outlook.com
 * @date 2016年3月22日 下午3:27:45
 */
@Service("renewalCancelApiServiceImpl")
public class RenewalCancelApiServiceImpl implements RenewalCancelApiService{
	@Autowired
	RenewalCancelCoreService renewalCancelCoreService;
	
	@Override
	public ThirdSendResponseDTO renewalCancel(RequestRenewalCancelDTO requestRenewalCancelDTO,ParamHeadDTO paramHead) {
		return renewalCancelCoreService.renewlCancel(requestRenewalCancelDTO, paramHead);
	}
}