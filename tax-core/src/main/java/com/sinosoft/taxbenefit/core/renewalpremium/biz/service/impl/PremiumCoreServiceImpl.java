package com.sinosoft.taxbenefit.core.renewalpremium.biz.service.impl;

import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.platform.common.config.SystemConstants;
import com.sinosoft.platform.common.exception.BusinessDataErrException;
import com.sinosoft.platform.common.util.DateUtil;
import com.sinosoft.platform.common.util.StringUtil;
import com.sinosoft.taxbenefit.api.base.EBResultCodeDTO;
import com.sinosoft.taxbenefit.api.base.HXResultCodeDTO;
import com.sinosoft.taxbenefit.api.config.ENUM_BZ_RESPONSE_RESULT;
import com.sinosoft.taxbenefit.api.config.ENUM_DISPOSE_TYPE;
import com.sinosoft.taxbenefit.api.config.ENUM_SENDSERVICE_CODE;
import com.sinosoft.taxbenefit.api.dto.request.TaxFaulTaskDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.CoreRequestHead;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqMPremiumInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqMRenewalPremiumCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqRenewalPremiumCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqRenewalPremiumCoverageInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqRenewalPremiumInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqSPremiumInfoDTO;
import com.sinosoft.taxbenefit.api.dto.response.renewalpremium.ResultRenewalPremiumDTO;
import com.sinosoft.taxbenefit.core.common.biz.service.CommonCodeService;
import com.sinosoft.taxbenefit.core.common.config.ENUM_BUSINESS_RESULR;
import com.sinosoft.taxbenefit.core.common.config.ENUM_EBAO_SERVICE_PORT_CODE;
import com.sinosoft.taxbenefit.core.common.config.ENUM_HX_SERVICE_PORT_CODE;
import com.sinosoft.taxbenefit.core.common.config.ENUM_RC_STATE;
import com.sinosoft.taxbenefit.core.common.config.ENUM_RENEWAL_TYPE;
import com.sinosoft.taxbenefit.core.common.config.ENUM_RESULT_CODE;
import com.sinosoft.taxbenefit.core.common.config.ENUUM_CODE_MAPPING;
import com.sinosoft.taxbenefit.core.common.dto.BookSequenceDTO;
import com.sinosoft.taxbenefit.core.common.dto.EbaoServerPortDTO;
import com.sinosoft.taxbenefit.core.common.dto.ReqBusinessDTO;
import com.sinosoft.taxbenefit.core.common.dto.ReqBusinessDetailDTO;
import com.sinosoft.taxbenefit.core.common.dto.SendLogDTO;
import com.sinosoft.taxbenefit.core.common.dto.ThirdSendDTO;
import com.sinosoft.taxbenefit.core.ebaowebservice.AsynResultQueryService;
import com.sinosoft.taxbenefit.core.ebaowebservice.AsynResultQueryService_Service;
import com.sinosoft.taxbenefit.core.ebaowebservice.PolicyPremiumService;
import com.sinosoft.taxbenefit.core.ebaowebservice.PolicyPremiumService_Service;
import com.sinosoft.taxbenefit.core.ebaowebservice.ResponseBody;
import com.sinosoft.taxbenefit.core.ebaowebservice.ResponseHeader;
import com.sinosoft.taxbenefit.core.ebaowebservice.ResponseJSON;
import com.sinosoft.taxbenefit.core.faultTask.biz.service.FaultTaskService;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxContMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxContRenewMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxFaultTaskMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxPolicyRiskMapper;
import com.sinosoft.taxbenefit.core.generated.model.TaxCont;
import com.sinosoft.taxbenefit.core.generated.model.TaxContRenew;
import com.sinosoft.taxbenefit.core.generated.model.TaxFaultTask;
import com.sinosoft.taxbenefit.core.policy.biz.service.PolicyService;
import com.sinosoft.taxbenefit.core.renewalpremium.biz.service.PremiumCoreService;
import com.sinosoft.taxbenefit.core.renewalpremium.dto.ReqPremiumDTO;
import com.sinosoft.taxbenefit.core.renewalpremium.dto.ReqRenewalPremiumCoverageDTO;
import com.sinosoft.taxbenefit.core.renewalpremium.dto.ReqRenewalPremiumDTO;
import com.sinosoft.taxbenefit.core.renewalpremium.dto.ResPremiumResultDTO;
import com.sinosoft.taxbenefit.core.renewalpremium.dto.ResRenewalPremiumDTO;
import com.sinosoft.taxbenefit.core.transCode.biz.service.TransCodeService;

@Service
public class PremiumCoreServiceImpl extends TaxBaseService implements PremiumCoreService {
	@Autowired
	TaxContRenewMapper taxContRenewMapper;
	@Autowired
	TaxPolicyRiskMapper taxPolicyRiskMapper;
	@Autowired
	TaxContMapper taxContMapper;
	@Autowired
	TransCodeService transCodeService;
	@Autowired
	CommonCodeService commonCodeService;
	@Autowired
	FaultTaskService faultTaskService;
	@Autowired
	TaxFaultTaskMapper taxFaultTaskMapper;//异常任务
	@Autowired
	PolicyService policyService;
	
	@Override
	public ThirdSendResponseDTO premiumSettlement(ReqSPremiumInfoDTO reqSPremiumInfoDTO,ParamHeadDTO paramHead) {
		ThirdSendResponseDTO thirdSendResponse = new ThirdSendResponseDTO();
		List<ReqBusinessDetailDTO> reqBusinessDetailList = new ArrayList<ReqBusinessDetailDTO>();
		try {
			ReqBusinessDTO businessDTO = commonCodeService.initReqBusinessDTO();
			logger.info("单笔续期信息上传（核心请求）JSON 体数据内容：" + JSONObject.toJSONString(reqSPremiumInfoDTO));
			//1.将续期保费上传请求dto转换为中保信续期保费上传请求dto
			ReqRenewalPremiumDTO reqRenewalPremium = this.convertCarrier(reqSPremiumInfoDTO);
			//2.调用中保信接口服务,获得返回结果   存储调用中保信日志表
			ThirdSendDTO thirdSendDTO = commonCodeService.createThirdSend(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_PREMIUM_UPLOAD.code(), paramHead, reqRenewalPremium);
			logger.info("单笔续期信息上传（请求中保信）JSON 数据内容："+JSONObject.toJSONString(thirdSendDTO));
			Holder<ResponseHeader> responseHeaderHolder = new Holder<ResponseHeader>();
			Holder<ResponseBody> responseBodyHolder = new Holder<ResponseBody>();
			EbaoServerPortDTO ebaoServerPort = commonCodeService.queryEBaoServerInfoByPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_PREMIUM_UPLOAD.code());
			String portUrl = ebaoServerPort.getPortUrl();
			URL wsdlLocation = new URL(portUrl);
			PolicyPremiumService_Service service = new PolicyPremiumService_Service(wsdlLocation);
			PolicyPremiumService servicePort = service.getPolicyPremiumServicePort();
			BindingProvider bp = (BindingProvider) servicePort;
			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, portUrl);
			servicePort.premium(thirdSendDTO.getHead(), thirdSendDTO.getBody(), responseHeaderHolder, responseBodyHolder);
			ResponseHeader responseHeader = responseHeaderHolder.value;
			ResponseBody responseBody = responseBodyHolder.value;
			logger.info("单笔续期信息上传（中保信响应）JSON 数据内容："+responseBody.getJsonString());
			//封装中保信返回报文
			ResponseJSON responseJSON = new ResponseJSON();
			responseJSON.setResponseBody(responseBody);
			responseJSON.setResponseHeader(responseHeader);
			//封装发送报文日志对象
			SendLogDTO sendLog = new SendLogDTO();
			sendLog.setSerialNo(paramHead.getSerialNo());
			sendLog.setPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_PREMIUM_UPLOAD.code());
			sendLog.setRequestContent(JSONObject.toJSONString(thirdSendDTO));
			sendLog.setResponseContent(JSONObject.toJSONString(responseJSON));
			sendLog.setTargetCode(ENUM_SENDSERVICE_CODE.EBAO.code());
			sendLog.setState(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
			sendLog.setDescription(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
			commonCodeService.addLogSend(sendLog);
			//封装返回核心dto;
			if(ENUM_RESULT_CODE.SUCCESS.code().equals(responseHeader.getResultCode())
					|| ENUM_RESULT_CODE.INPUT_LOCALITY_SUCCESS.code().equals(responseHeader.getResultCode())){
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
				thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
				//4.存储业务表数据
				if (reqSPremiumInfoDTO.getPremiumList() != null) {
					addContRenew(reqSPremiumInfoDTO.getPremiumList().get(0));
				}
			}else{
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
				List list = responseHeader.getResultMessage().getMessage();
				if(list != null && list.size() > 0){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : responseHeader.getResultMessage().getMessage()){
						sbStr.append(mess).append("|");
					}
					String message = sbStr.substring(0, sbStr.length()-1).toString();
					thirdSendResponse.setResultDesc(message);
				}else{
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
				}
			}
			boolean isRegisterFaulTask = false;
			if(!StringUtil.isEmpty(responseBody.getJsonString())){
				ResRenewalPremiumDTO resRenewalPremium = JSONObject.parseObject(responseBody.getJsonString(), ResRenewalPremiumDTO.class);
				if (null != resRenewalPremium.getPremiumResultList()) {
					//获取业务数据
					ResultRenewalPremiumDTO resultRenewalPremiumDTO = new ResultRenewalPremiumDTO();
					String bizNo = reqSPremiumInfoDTO.getPremiumList().get(0).getBizNo();
					resultRenewalPremiumDTO.setBizNo(bizNo);
					if(resRenewalPremium.getPremiumResultList() != null){
						String feeSequenceNo = resRenewalPremium.getPremiumResultList().get(0).getFeeSequenceNo();
						resultRenewalPremiumDTO.setFeeSequenceNo(feeSequenceNo);
					}
					EBResultCodeDTO eBResultCode = resRenewalPremium.getPremiumResultList().get(0).getResult();
					HXResultCodeDTO hxResultCode = new HXResultCodeDTO();
					hxResultCode.setResultCode(eBResultCode.getResultCode());
					String message = "";
					if(null != eBResultCode.getResultMessage()){
						StringBuffer sbStr = new StringBuffer(); 
						for(String mess : eBResultCode.getResultMessage()){
							sbStr.append(mess).append("|");
						}
						message = sbStr.substring(0, sbStr.length()-1).toString();
					}
					hxResultCode.setResultMessage(message);
					resultRenewalPremiumDTO.setResult(hxResultCode);
					thirdSendResponse.setResJson(resultRenewalPremiumDTO);
					if(ENUM_RESULT_CODE.SUCCESS.code().equals(eBResultCode.getResultCode())){
						businessDTO.setReqSuccessNum(businessDTO.getReqSuccessNum() + 1);
					}else{
						businessDTO.setReqFailNum(businessDTO.getReqFailNum() + 1);
						ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
						reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
						reqBusinessDetailDTO.setResultMessage(message);
						reqBusinessDetailList.add(reqBusinessDetailDTO);
					}
					isRegisterFaulTask = true;
				}else{
					BookSequenceDTO bookSequence = JSONObject.parseObject(responseBody.getJsonString(), BookSequenceDTO.class);
					TaxFaulTaskDTO taxFaultTask = new TaxFaulTaskDTO();
					taxFaultTask.setSerialNo(paramHead.getSerialNo());
					taxFaultTask.setServerPortCode(paramHead.getPortCode());
					taxFaultTask.setServerPortName(paramHead.getPortName());
					taxFaultTask.setAreaCode(paramHead.getAreaCode());
					taxFaultTask.setDisposeType(ENUM_DISPOSE_TYPE.APPOINTMENT.code());
					taxFaultTask.setBookSequenceNo(bookSequence.getBookingSequenceNo());
					faultTaskService.addTaxFaultTask(taxFaultTask);
					thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
				}
			}else{
				businessDTO.setReqFailNum(responseHeader.getRecordNum());
				ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
				reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
				reqBusinessDetailDTO.setResultMessage(thirdSendResponse.getResultDesc());
				reqBusinessDetailList.add(reqBusinessDetailDTO);
			}
			if (isRegisterFaulTask) {
				commonCodeService.addReqBusinessDetail(reqBusinessDetailList);
				businessDTO.setRequetNum(responseHeader.getRecordNum());
				commonCodeService.addReqBusinessCollate(businessDTO);
			}
		}catch (Exception e) {
			logger.error("单笔续期信息上传后置处理发生异常!");
			e.printStackTrace();
			thirdSendResponse.setResJson(e.getMessage());
			thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
			thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
			// 存储任务表
			TaxFaulTaskDTO taxFaultTask = new TaxFaulTaskDTO();
			taxFaultTask.setSerialNo(paramHead.getSerialNo());
			taxFaultTask.setServerPortCode(paramHead.getPortCode());
			taxFaultTask.setServerPortName(paramHead.getPortName());
			taxFaultTask.setAreaCode(paramHead.getAreaCode());
			taxFaultTask.setDisposeType(ENUM_DISPOSE_TYPE.ERROR.code());
			faultTaskService.addTaxFaultTask(taxFaultTask);
		}
		logger.error("单笔续期信息上传后置处理结束!");
		return thirdSendResponse;
	}
	
	/**
	 * 核心对象 - 封装中保信对象(单个)
	 * @param reqPremiumInfoDTOs
	 * @return
	 */
	public ReqRenewalPremiumDTO convertCarrier(ReqSPremiumInfoDTO reqSPremiumInfoDTO){
		ReqRenewalPremiumDTO reqRenewalPremiumDTO = new ReqRenewalPremiumDTO();		
		List<ReqPremiumDTO> premiumList = new ArrayList<ReqPremiumDTO>();
		if(null != reqSPremiumInfoDTO.getPremiumList()){
			for(ReqRenewalPremiumInfoDTO reqRenewalPremiumInfoDTO : reqSPremiumInfoDTO.getPremiumList()){
				ReqPremiumDTO reqPremiumDTO = new ReqPremiumDTO();
				reqPremiumDTO.setConfirmDate(reqRenewalPremiumInfoDTO.getConfirmDate());
				reqPremiumDTO.setFeeId(reqRenewalPremiumInfoDTO.getFeeId());
				reqPremiumDTO.setFeeStatus(reqRenewalPremiumInfoDTO.getFeeStatus());
				reqPremiumDTO.setPayDueDate(reqRenewalPremiumInfoDTO.getPayDueDate());
				reqPremiumDTO.setPayFromDate(reqRenewalPremiumInfoDTO.getPayFromDate());
				reqPremiumDTO.setPaymentFrequency(transCodeService.transCode(ENUUM_CODE_MAPPING.PAYMENTFREQUENCY.code(),reqRenewalPremiumInfoDTO.getPaymentFrequency()));
				reqPremiumDTO.setPayToDate(reqRenewalPremiumInfoDTO.getPayToDate());
				reqPremiumDTO.setPolicyNo(reqRenewalPremiumInfoDTO.getPolicyNo());
				reqPremiumDTO.setPremiumAmount(reqRenewalPremiumInfoDTO.getPremiumAmount());
				reqPremiumDTO.setSequenceNo(reqRenewalPremiumInfoDTO.getSequenceNo());
				List<ReqRenewalPremiumCoverageDTO> coverageList = new ArrayList<ReqRenewalPremiumCoverageDTO>();
				// 险种信息
				if(reqRenewalPremiumInfoDTO.getCoverageList() != null){
					for (ReqRenewalPremiumCoverageInfoDTO reqRenewalPremiumCoverageInfoDTO : reqRenewalPremiumInfoDTO.getCoverageList()) {
						ReqRenewalPremiumCoverageDTO reqRenewalPremiumCoverageDTO=new ReqRenewalPremiumCoverageDTO();
						reqRenewalPremiumCoverageDTO.setComCoverageCode(reqRenewalPremiumCoverageInfoDTO.getComCoverageCode());
						reqRenewalPremiumCoverageDTO.setCoveragePackageCode(reqRenewalPremiumCoverageInfoDTO.getCoveragePackageCode());
						reqRenewalPremiumCoverageDTO.setCoveragePremium(reqRenewalPremiumCoverageInfoDTO.getCoveragePremium());
						reqRenewalPremiumCoverageDTO.setRiskPremium(reqRenewalPremiumCoverageInfoDTO.getRiskPremium());
						coverageList.add(reqRenewalPremiumCoverageDTO);
					}
				} 
				reqPremiumDTO.setCoverageList(coverageList);
				premiumList.add(reqPremiumDTO);
			}
		}
		reqRenewalPremiumDTO.setPremiumList(premiumList);
		return reqRenewalPremiumDTO;
	}
	
	@Override
	public ThirdSendResponseDTO premiumMSettlement(ReqMPremiumInfoDTO reqMPremiumInfoDTO, ParamHeadDTO paramHead) {
		ThirdSendResponseDTO thirdSendResponse = new ThirdSendResponseDTO();
		List<ReqBusinessDetailDTO> reqBusinessDetailList = new ArrayList<ReqBusinessDetailDTO>();
		try {
			ReqBusinessDTO businessDTO = commonCodeService.initReqBusinessDTO();
			logger.info("多笔续期信息上传（核心请求）JSON 体数据内容："+JSONObject.toJSONString(reqMPremiumInfoDTO));
			//1.将续期保费上传请求dto转换为中保信续期保费上传请求dto
			ReqRenewalPremiumDTO reqRenewalPremium = this.convertCarrier(reqMPremiumInfoDTO);
			ThirdSendDTO thirdSendDTO = commonCodeService.createThirdSend(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_PREMIUM_UPLOAD.code(), paramHead, reqRenewalPremium);
			logger.info("多笔续期信息上传（请求中保信）JSON 数据内容："+JSONObject.toJSONString(thirdSendDTO));
			Holder<ResponseHeader> responseHeaderHolder = new Holder<ResponseHeader>();
			Holder<ResponseBody> responseBodyHolder = new Holder<ResponseBody>();
			EbaoServerPortDTO ebaoServerPort = commonCodeService.queryEBaoServerInfoByPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_PREMIUM_UPLOAD.code());
			String portUrl = ebaoServerPort.getPortUrl();
			URL wsdlLocation = new URL(portUrl);
			PolicyPremiumService_Service service = new PolicyPremiumService_Service(wsdlLocation);
			PolicyPremiumService servicePort = service.getPolicyPremiumServicePort();
			BindingProvider bp = (BindingProvider) servicePort;
			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, portUrl);
			servicePort.premium(thirdSendDTO.getHead(), thirdSendDTO.getBody(), responseHeaderHolder, responseBodyHolder);
			ResponseHeader responseHeader = responseHeaderHolder.value;
			ResponseBody responseBody = responseBodyHolder.value;
			logger.info("多笔续期信息上传（中保信响应）JSON 数据内容："+responseBody.getJsonString());
			//封装中保信返回报文
			ResponseJSON responseJSON = new ResponseJSON();
			responseJSON.setResponseBody(responseBody);
			responseJSON.setResponseHeader(responseHeader);
			//封装发送报文日志对象
			SendLogDTO sendLog = new SendLogDTO();
			sendLog.setSerialNo(paramHead.getSerialNo());
			sendLog.setPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_PREMIUM_UPLOAD.code());
			sendLog.setRequestContent(JSONObject.toJSONString(thirdSendDTO));
			sendLog.setResponseContent(JSONObject.toJSONString(responseJSON));
			sendLog.setTargetCode(ENUM_SENDSERVICE_CODE.EBAO.code());
			sendLog.setState(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
			sendLog.setDescription(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
			commonCodeService.addLogSend(sendLog);
			//封装返回核心dto;
			if(ENUM_RESULT_CODE.SUCCESS.code().equals(responseHeader.getResultCode())){
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
				thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
				//4.存储业务表数据
				if (reqMPremiumInfoDTO.getPremiumList() != null) {
					ResRenewalPremiumDTO resRenewalPremium = JSONObject.parseObject(responseBody.getJsonString(), ResRenewalPremiumDTO.class);
					for (ResPremiumResultDTO resPremiumResultDTO : resRenewalPremium.getPremiumResultList()) {
						for (ReqRenewalPremiumInfoDTO reqRenewalPremiumInfoDTO : reqMPremiumInfoDTO.getPremiumList()) {
							if (reqRenewalPremiumInfoDTO.getPolicyNo().equals(resPremiumResultDTO.getPolicyNo())) {
								if (resPremiumResultDTO.getResult().getResultCode().equals(ENUM_RESULT_CODE.SUCCESS.code())) {
									addContRenew(reqRenewalPremiumInfoDTO);
								}
							}
						}
					}
				}
			}else{
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
				List list = responseHeader.getResultMessage().getMessage();
				if(list != null && list.size() > 0){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : responseHeader.getResultMessage().getMessage()){
						sbStr.append(mess).append("|");
					}
					String message = sbStr.substring(0, sbStr.length()-1).toString();
					thirdSendResponse.setResultDesc(message);
				}else{
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
				}
			}
			boolean isRegisterFaulTask = false;
			if(!StringUtil.isEmpty(responseBody.getJsonString())){
				ResRenewalPremiumDTO resRenewalPremium = JSONObject.parseObject(responseBody.getJsonString(), ResRenewalPremiumDTO.class);
				if (null != resRenewalPremium.getPremiumResultList()) {
					List<ResultRenewalPremiumDTO> resultRenewalPremiumList = new ArrayList<ResultRenewalPremiumDTO>();
					// 成功记录数
					Integer successNum = 0;
					// 失败记录数
					Integer failNum = 0;
					//获取业务数据
					if (reqMPremiumInfoDTO.getPremiumList() != null) {
						for (ReqRenewalPremiumInfoDTO reqRenewalPremiumInfoDTO : reqMPremiumInfoDTO.getPremiumList()) {
							if (resRenewalPremium.getPremiumResultList() != null) {
								for (ResPremiumResultDTO resPremiumResultDTO : resRenewalPremium.getPremiumResultList()) {
									if (reqRenewalPremiumInfoDTO.getFeeId().equals(resPremiumResultDTO.getFeeId())) {
										ResultRenewalPremiumDTO resultRenewalPremiumDTO = new ResultRenewalPremiumDTO();
										resultRenewalPremiumDTO.setBizNo(reqRenewalPremiumInfoDTO.getBizNo());
										EBResultCodeDTO eBResultCode = resPremiumResultDTO.getResult();
										HXResultCodeDTO hxResultCode = new HXResultCodeDTO();
										hxResultCode.setResultCode(eBResultCode.getResultCode());
										String message = "";
										if(null != eBResultCode.getResultMessage()){
											StringBuffer sbStr = new StringBuffer(); 
											for(String mess : eBResultCode.getResultMessage()){
												sbStr.append(mess).append("|");
											}
											message = sbStr.substring(0, sbStr.length()-1).toString();
										}
										hxResultCode.setResultMessage(message);
										resultRenewalPremiumDTO.setFeeSequenceNo(resPremiumResultDTO.getFeeSequenceNo());
										resultRenewalPremiumDTO.setResult(hxResultCode);
										resultRenewalPremiumList.add(resultRenewalPremiumDTO);
										//记录成功和失败记录数
										if(ENUM_RESULT_CODE.SUCCESS.code().equals(eBResultCode.getResultCode())){
											successNum+=1;
										}else{
											failNum+=1;
											ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
											reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
											reqBusinessDetailDTO.setResultMessage(message);
											reqBusinessDetailList.add(reqBusinessDetailDTO);
										}
									}
								}
							}
						}
						// 登记请求记录
						businessDTO.setReqSuccessNum(successNum);
						businessDTO.setReqFailNum(failNum);
						thirdSendResponse.setResJson(resultRenewalPremiumList);
					}
					isRegisterFaulTask = true;
				}else{
					BookSequenceDTO bookSequence = JSONObject.parseObject(responseBody.getJsonString(), BookSequenceDTO.class);
					TaxFaulTaskDTO taxFaultTask = new TaxFaulTaskDTO();
					taxFaultTask.setSerialNo(paramHead.getSerialNo());
					taxFaultTask.setServerPortCode(paramHead.getPortCode());
					taxFaultTask.setServerPortName(paramHead.getPortName());
					taxFaultTask.setAreaCode(paramHead.getAreaCode());
					taxFaultTask.setDisposeType(ENUM_DISPOSE_TYPE.APPOINTMENT.code());
					taxFaultTask.setBookSequenceNo(bookSequence.getBookingSequenceNo());
					faultTaskService.addTaxFaultTask(taxFaultTask);
					thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
				}
			}else{
				businessDTO.setReqFailNum(responseHeader.getRecordNum());
				for (int i = 0; i < responseHeader.getRecordNum(); i++) {
					ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
					reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
					reqBusinessDetailDTO.setResultMessage(thirdSendResponse.getResultDesc());
					reqBusinessDetailList.add(reqBusinessDetailDTO);
				}
			}
			if (isRegisterFaulTask) {
				commonCodeService.addReqBusinessDetail(reqBusinessDetailList);
				businessDTO.setRequetNum(responseHeader.getRecordNum());
				commonCodeService.addReqBusinessCollate(businessDTO);
			}
		} catch (Exception e) {
			logger.error("多笔续期信息上传后置处理发生异常!");
			e.printStackTrace();
			thirdSendResponse.setResJson(e.getMessage());
			thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
			thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
			// 存储任务表
			TaxFaulTaskDTO taxFaultTask = new TaxFaulTaskDTO();
			taxFaultTask.setSerialNo(paramHead.getSerialNo());
			taxFaultTask.setServerPortCode(paramHead.getPortCode());
			taxFaultTask.setServerPortName(paramHead.getPortName());
			taxFaultTask.setAreaCode(paramHead.getAreaCode());
			taxFaultTask.setDisposeType(ENUM_DISPOSE_TYPE.ERROR.code());
			faultTaskService.addTaxFaultTask(taxFaultTask);
		}
		logger.error("多笔续期信息上传后置处理结束!");
		return thirdSendResponse;
	}

	private void addContRenew(ReqRenewalPremiumInfoDTO reqRenewalPremiumInfoDTO) {
		TaxContRenew contRenew = new TaxContRenew(); // 续期信息
		try {
			contRenew.setBizNo(reqRenewalPremiumInfoDTO.getBizNo());
			contRenew.setContNo(reqRenewalPremiumInfoDTO.getPolicyNo());
			contRenew.setApayDate(DateUtil.parseDate(reqRenewalPremiumInfoDTO.getConfirmDate()));
			contRenew.setRenewFeeNo(reqRenewalPremiumInfoDTO.getFeeId());
			contRenew.setFeeStatus(reqRenewalPremiumInfoDTO.getFeeStatus());
			contRenew.setSpayDate(DateUtil.parseDate(reqRenewalPremiumInfoDTO.getPayDueDate()));
			contRenew.setStartDate(DateUtil.parseDate(reqRenewalPremiumInfoDTO.getPayFromDate()));
			contRenew.setPaySequence(reqRenewalPremiumInfoDTO.getPaymentFrequency());
			contRenew.setEndDate(DateUtil.parseDate(reqRenewalPremiumInfoDTO.getPayToDate()));
			contRenew.setActualPrem(reqRenewalPremiumInfoDTO.getPremiumAmount());
			contRenew.setSequenceNo(reqRenewalPremiumInfoDTO.getSequenceNo());
			contRenew.setRenewType(ENUM_RENEWAL_TYPE.POLICY_RENEWAL.getCode());//添加续期续保类型
			String policyNo = reqRenewalPremiumInfoDTO.getPolicyNo();
			TaxCont taxCont = policyService.queryPolicyByPolicyNo(policyNo);
			contRenew.setContId(taxCont==null?null:taxCont.getSid());
			contRenew.setRcState(ENUM_RC_STATE.EFFECTIVE.getCode());
			contRenew.setCreateDate(DateUtil.getCurrentDate());
			contRenew.setCreatorId(SystemConstants.SYS_OPERATOR);
			contRenew.setModifierId(SystemConstants.SYS_OPERATOR);
			contRenew.setModifyDate(DateUtil.getCurrentDate());
			taxContRenewMapper.insert(contRenew);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("续期信息上传数据持久化失败"+e.getMessage());
		}
	}
	
	/**
	 * 核心对象 - 封装中保信对象(多个）
	 * @param reqPremiumInfoDTOs
	 * @return
	 */
	public ReqRenewalPremiumDTO convertCarrier(ReqMPremiumInfoDTO reqMPremiumInfoDTO){
		ReqRenewalPremiumDTO reqRenewalPremiumDTO=new ReqRenewalPremiumDTO();		
		List<ReqPremiumDTO> premiumList =new ArrayList<ReqPremiumDTO>();
		if (reqMPremiumInfoDTO.getPremiumList() != null) {
			for(ReqRenewalPremiumInfoDTO reqRenewalPremiumInfoDTO :reqMPremiumInfoDTO.getPremiumList()){
				ReqPremiumDTO reqPremiumDTO = new ReqPremiumDTO();
				reqPremiumDTO.setConfirmDate(reqRenewalPremiumInfoDTO.getConfirmDate());
				reqPremiumDTO.setFeeId(reqRenewalPremiumInfoDTO.getFeeId());
				reqPremiumDTO.setPayDueDate(reqRenewalPremiumInfoDTO.getPayDueDate());
				reqPremiumDTO.setPayFromDate(reqRenewalPremiumInfoDTO.getPayFromDate());
				reqPremiumDTO.setPaymentFrequency(transCodeService.transCode(ENUUM_CODE_MAPPING.PAYMENTFREQUENCY.code(),reqRenewalPremiumInfoDTO.getPaymentFrequency()));
				reqPremiumDTO.setPayToDate(reqRenewalPremiumInfoDTO.getPayToDate());
				reqPremiumDTO.setPolicyNo(reqRenewalPremiumInfoDTO.getPolicyNo());
				reqPremiumDTO.setFeeStatus(reqRenewalPremiumInfoDTO.getFeeStatus());
				reqPremiumDTO.setPremiumAmount(reqRenewalPremiumInfoDTO.getPremiumAmount());
				reqPremiumDTO.setSequenceNo(reqRenewalPremiumInfoDTO.getSequenceNo());
				List<ReqRenewalPremiumCoverageDTO> coverageList = new ArrayList<ReqRenewalPremiumCoverageDTO>();
				if (reqRenewalPremiumInfoDTO.getCoverageList() != null) {
					for (ReqRenewalPremiumCoverageInfoDTO reqRenewalPremiumCoverageInfoDTO : reqRenewalPremiumInfoDTO.getCoverageList()) {
						ReqRenewalPremiumCoverageDTO reqRenewalPremiumCoverageDTO=new ReqRenewalPremiumCoverageDTO();
						reqRenewalPremiumCoverageDTO.setComCoverageCode(reqRenewalPremiumCoverageInfoDTO.getComCoverageCode());
						reqRenewalPremiumCoverageDTO.setCoveragePackageCode(reqRenewalPremiumCoverageInfoDTO.getCoveragePackageCode());
						reqRenewalPremiumCoverageDTO.setCoveragePremium(reqRenewalPremiumCoverageInfoDTO.getCoveragePremium());
						reqRenewalPremiumCoverageDTO.setRiskPremium(reqRenewalPremiumCoverageInfoDTO.getRiskPremium());
						coverageList.add(reqRenewalPremiumCoverageDTO);
					}
				}
				reqPremiumDTO.setCoverageList(coverageList);
				premiumList.add(reqPremiumDTO);
			}
		}
		reqRenewalPremiumDTO.setPremiumList(premiumList);
		return reqRenewalPremiumDTO;
	}
	@Override
	public TaxFaultTask premiumExceptionJob(String reqJSON, String portCode, Integer faultTaskId) {
		logger.info("进入续期保费信息上传异常任务服务");
		ThirdSendResponseDTO thirdSendResponse = new ThirdSendResponseDTO();
		List<ReqBusinessDetailDTO> reqBusinessDetailList = new ArrayList<ReqBusinessDetailDTO>();
		TaxFaultTask faultTask = taxFaultTaskMapper.selectByPrimaryKey(faultTaskId);
		try {
			ReqBusinessDTO businessDTO = commonCodeService.initReqBusinessDTO();
			ParamHeadDTO paramHead = new ParamHeadDTO();
			CoreRequestHead coreRequestHead = new CoreRequestHead();
			ReqMPremiumInfoDTO reqMPremiumInfoDTO = new ReqMPremiumInfoDTO();
			//1. 解析请求报文
			//单笔请求
			if (ENUM_HX_SERVICE_PORT_CODE.POLICY_PREMIUM_UPLOAD_ONE.code().equals(portCode)) {
				ReqRenewalPremiumCoreDTO reqRenewalPremiumCore = JSONObject.parseObject(reqJSON, ReqRenewalPremiumCoreDTO.class);
				ReqSPremiumInfoDTO reqSPremiumInfoDTO = (ReqSPremiumInfoDTO) reqRenewalPremiumCore.getBody();
				List<ReqRenewalPremiumInfoDTO> reqRenewalPremiumInfoDTOList = new ArrayList<ReqRenewalPremiumInfoDTO>();
				reqRenewalPremiumInfoDTOList.add(reqSPremiumInfoDTO.getPremiumList().get(0));
				reqMPremiumInfoDTO.setPremiumList(reqRenewalPremiumInfoDTOList);
				coreRequestHead = reqRenewalPremiumCore.getRequestHead();
			//多笔请求	
			} else if (ENUM_HX_SERVICE_PORT_CODE.POLICY_PREMIUM_UPLOAD_MORE.code().equals(portCode)){
				ReqMRenewalPremiumCoreDTO reqMRenewalPremiumCore = JSONObject.parseObject(reqJSON, ReqMRenewalPremiumCoreDTO.class);
				reqMPremiumInfoDTO = (ReqMPremiumInfoDTO)reqMRenewalPremiumCore.getBody();
				coreRequestHead = reqMRenewalPremiumCore.getRequestHead();
			} else {
				throw new BusinessDataErrException("请求接口编码有误! 错误接口编码为："+portCode);
			}
			//2. 封装头部信息
			paramHead.setAreaCode(coreRequestHead.getAreaCode());
			paramHead.setRecordNum(coreRequestHead.getRecordNum());
			paramHead.setSerialNo(coreRequestHead.getSerialNo());
			paramHead.setPortCode(portCode);
			ThirdSendDTO thirdSendDTO = new ThirdSendDTO();
			EbaoServerPortDTO ebaoServerPort = null;
			Holder<ResponseHeader> responseHeaderHolder = new Holder<ResponseHeader>();
			Holder<ResponseBody> responseBodyHolder = new Holder<ResponseBody>();
			// 预约码处理方式
			if(ENUM_DISPOSE_TYPE.APPOINTMENT.code().equals(faultTask.getDisposeType())){
				//3. 封装请求中保信数据
				paramHead.setRecordNum("1"); //异步请求接口请求记录数为1
				BookSequenceDTO bookSequence = new BookSequenceDTO();
				bookSequence.setBookingSequenceNo(faultTask.getRemark());
				thirdSendDTO = commonCodeService.createThirdSend(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_PREMIUM_UPLOAD_APPOINTMENT.code(), paramHead, bookSequence);
				ebaoServerPort = commonCodeService.queryEBaoServerInfoByPortCode(ENUM_EBAO_SERVICE_PORT_CODE.ASYN_RESULT_QUERY.code());
				logger.info("预约码-续期信息上传（请求中保信）JSON 数据内容："+JSONObject.toJSONString(thirdSendDTO));
				String portUrl = ebaoServerPort.getPortUrl();
				URL wsdlLocation = new URL(portUrl);
				AsynResultQueryService_Service service = new AsynResultQueryService_Service(wsdlLocation);
				AsynResultQueryService servicePort = service.getAsynResultQueryServicePort();
				BindingProvider bp = (BindingProvider) servicePort;
				bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, portUrl);
				servicePort.asynResultQuery(thirdSendDTO.getHead(), thirdSendDTO.getBody(),responseHeaderHolder, responseBodyHolder);
			} else if(ENUM_DISPOSE_TYPE.ASYNC.code().equals(faultTask.getDisposeType()) || ENUM_DISPOSE_TYPE.ERROR.code().equals(faultTask.getDisposeType())){
				// 将续期保费信息上传请求dto转换为中保信续期保费信息上传请求dto
				ReqRenewalPremiumDTO reqRenewalPremium = this.convertCarrier(reqMPremiumInfoDTO);
				thirdSendDTO = commonCodeService.createThirdSend(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_PREMIUM_UPLOAD.code(), paramHead, reqRenewalPremium);
				ebaoServerPort = commonCodeService.queryEBaoServerInfoByPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_PREMIUM_UPLOAD.code());
				logger.info("异常任务 - 续期信息上传（请求中保信）JSON 数据内容(错误或异常类型)："+JSONObject.toJSONString(thirdSendDTO));
				String portUrl = ebaoServerPort.getPortUrl();
				URL wsdlLocation = new URL(portUrl);
				PolicyPremiumService_Service service = new PolicyPremiumService_Service(wsdlLocation);
				PolicyPremiumService servicePort = service.getPolicyPremiumServicePort();
				BindingProvider bp = (BindingProvider) servicePort;
				bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, portUrl);
				servicePort.premium(thirdSendDTO.getHead(), thirdSendDTO.getBody(), responseHeaderHolder, responseBodyHolder);
			} else{
				throw new BusinessDataErrException("异常类型有误! 错误异常类型为："+ENUM_DISPOSE_TYPE.getEnumValueByKey(faultTask.getDisposeType()));
			}
			ResponseHeader responseHeader = responseHeaderHolder.value;
			ResponseBody responseBody = responseBodyHolder.value;
			logger.info("预约码/异常任务-续期信息上传（中保信响应）JSON 数据内容："+responseBody.getJsonString());
			ResponseJSON responseJSON = new ResponseJSON();
			responseJSON.setResponseBody(responseBody);
			responseJSON.setResponseHeader(responseHeader);
			// 如果是异步类型需要记录日志
			if(ENUM_DISPOSE_TYPE.ASYNC.code().equals(faultTask.getDisposeType())){
				SendLogDTO sendLog = new SendLogDTO();
				sendLog.setSerialNo(paramHead.getSerialNo());
				sendLog.setPortCode(ENUM_EBAO_SERVICE_PORT_CODE.POLICY_PREMIUM_UPLOAD.code());
				sendLog.setRequestContent(JSONObject.toJSONString(thirdSendDTO));
				sendLog.setResponseContent(JSONObject.toJSONString(responseJSON));
				sendLog.setTargetCode(ENUM_SENDSERVICE_CODE.EBAO.code());
				sendLog.setState(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
				sendLog.setDescription(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
				commonCodeService.addLogSend(sendLog);
			}
			// 初始化头部错误信息
			String messageBody = ENUM_BZ_RESPONSE_RESULT.FAIL.description();
			//5. 封装返回核心dto;
			if(ENUM_RESULT_CODE.SUCCESS.code().equals(responseHeader.getResultCode())
					|| ENUM_RESULT_CODE.INPUT_LOCALITY_SUCCESS.code().equals(responseHeader.getResultCode())){
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
				thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.SUCCESS.description());
				//储存业务数据
				if (null != reqMPremiumInfoDTO.getPremiumList()) {
					ResRenewalPremiumDTO resRenewalPremium = JSONObject.parseObject(responseBody.getJsonString(), ResRenewalPremiumDTO.class);
					for (ResPremiumResultDTO resPremiumResultDTO : resRenewalPremium.getPremiumResultList()) {
						for (ReqRenewalPremiumInfoDTO reqRenewalPremiumInfoDTO : reqMPremiumInfoDTO.getPremiumList()) {
							if (reqRenewalPremiumInfoDTO.getPolicyNo().equals(resPremiumResultDTO.getPolicyNo())) {
								if (resPremiumResultDTO.getResult().getResultCode().equals(ENUM_RESULT_CODE.SUCCESS.code())) {
									this.addContRenew(reqRenewalPremiumInfoDTO);
								}
							}
						}
					}
				}
			}else if(ENUM_RESULT_CODE.REQ_AWAIT.code().equals(responseHeader.getResultCode())){
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
				thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
			}else{
				thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
				List list = responseHeader.getResultMessage().getMessage();
				if(list != null && list.size() > 0){
					StringBuffer sbStr = new StringBuffer(); 
					for(String mess : responseHeader.getResultMessage().getMessage()){
						sbStr.append(mess).append("|");
					}
					String message = messageBody = sbStr.substring(0, sbStr.length()-1).toString();
					thirdSendResponse.setResultDesc(message);
				}else{
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
				}
			}
			boolean isRegisterFaulTask = false;
			// 获取业务数据
			if(!StringUtil.isEmpty(responseBody.getJsonString())){
				ResRenewalPremiumDTO resRenewalPremium = JSONObject.parseObject(responseBody.getJsonString(), ResRenewalPremiumDTO.class);
				if (null != resRenewalPremium.getPremiumResultList()) {
					List<ResultRenewalPremiumDTO> resultRenewalPremiumList = new ArrayList<ResultRenewalPremiumDTO>();
					// 成功记录数
					Integer successNum = 0;
					// 失败记录数
					Integer failNum = 0;
					//获取业务数据
					if (reqMPremiumInfoDTO.getPremiumList() != null) {
						for (ReqRenewalPremiumInfoDTO reqRenewalPremiumInfoDTO : reqMPremiumInfoDTO.getPremiumList()) {
							if (resRenewalPremium.getPremiumResultList() != null) {
								for (ResPremiumResultDTO resPremiumResultDTO : resRenewalPremium.getPremiumResultList()) {
									if (reqRenewalPremiumInfoDTO.getFeeId().equals(resPremiumResultDTO.getFeeId())) {
										ResultRenewalPremiumDTO resultRenewalPremiumDTO = new ResultRenewalPremiumDTO();
										resultRenewalPremiumDTO.setBizNo(reqRenewalPremiumInfoDTO.getBizNo());
										EBResultCodeDTO eBResultCode = resPremiumResultDTO.getResult();
										HXResultCodeDTO hxResultCode = new HXResultCodeDTO();
										hxResultCode.setResultCode(eBResultCode.getResultCode());
										String message = "";
										if(null != eBResultCode.getResultMessage()){
											StringBuffer sbStr = new StringBuffer(); 
											for(String mess : eBResultCode.getResultMessage()){
												sbStr.append(mess).append("|");
											}
											message = sbStr.substring(0, sbStr.length()-1).toString();
										}
										hxResultCode.setResultMessage(message);
										resultRenewalPremiumDTO.setFeeSequenceNo(resPremiumResultDTO.getFeeSequenceNo());
										resultRenewalPremiumDTO.setResult(hxResultCode);
										resultRenewalPremiumList.add(resultRenewalPremiumDTO);
										//记录成功和失败记录数
										if(ENUM_RESULT_CODE.SUCCESS.code().equals(eBResultCode.getResultCode())){
											successNum+=1;
										}else{
											failNum+=1;
											ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
											reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
											reqBusinessDetailDTO.setResultMessage(message);
											reqBusinessDetailList.add(reqBusinessDetailDTO);
										}
									}
								}
							}
						}
						// 登记请求记录
						businessDTO.setReqSuccessNum(successNum);
						businessDTO.setReqFailNum(failNum);
						thirdSendResponse.setResJson(resultRenewalPremiumList);
					}
					isRegisterFaulTask = true;
				}else{
					BookSequenceDTO bookSequence = JSONObject.parseObject(responseBody.getJsonString(),BookSequenceDTO.class);
					// 插入异常任务表
					faultTask.setRemark(bookSequence.getBookingSequenceNo());
					faultTask.setDisposeType(ENUM_DISPOSE_TYPE.APPOINTMENT.code());
					thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.code());
					thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.EXCEPTION.description());
				}
			}else{
				businessDTO.setReqFailNum(responseHeader.getRecordNum());
				thirdSendResponse.setResJson(messageBody);
				for (int i = 0; i < responseHeader.getRecordNum(); i++) {
					ReqBusinessDetailDTO reqBusinessDetailDTO = commonCodeService.initReqBusinessDetailDTO(paramHead);
					reqBusinessDetailDTO.setResultCode(ENUM_BUSINESS_RESULR.FAIL.code());
					reqBusinessDetailDTO.setResultMessage(thirdSendResponse.getResultDesc());
					reqBusinessDetailList.add(reqBusinessDetailDTO);
				}
			}
			if (isRegisterFaulTask) {
				commonCodeService.addReqBusinessDetail(reqBusinessDetailList);
				businessDTO.setRequetNum(responseHeader.getRecordNum());
				commonCodeService.addReqBusinessCollate(businessDTO);
				faultTask.setDisposeResult(ENUM_BZ_RESPONSE_RESULT.SUCCESS.code());
			}else{
				faultTask.setDisposeResult(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
			}
		} catch (Exception e) {
			logger.error("续期保费信息上传异常任务服务方法处理发生异常! 异常信息为："+e.getMessage());
			e.printStackTrace();
			thirdSendResponse.setResJson(e.getMessage());
			thirdSendResponse.setResultCode(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
			thirdSendResponse.setResultDesc(ENUM_BZ_RESPONSE_RESULT.FAIL.description());
			faultTask.setDisposeResult(ENUM_BZ_RESPONSE_RESULT.FAIL.code());
			if(ENUM_DISPOSE_TYPE.APPOINTMENT.code().equals(faultTask.getDisposeType())){
				faultTask.setDisposeType(ENUM_DISPOSE_TYPE.APPOINTMENT.code());
			}else{
				faultTask.setDisposeType(ENUM_DISPOSE_TYPE.ERROR.code());
			}
		} finally{
			faultTask.setSuccessMessage(JSONObject.toJSONString(thirdSendResponse));
			faultTask.setDisposeDate(DateUtil.getCurrentDate());
			taxFaultTaskMapper.updateByPrimaryKeySelective(faultTask);
		}
		return faultTask;
	}
}