package com.sinosoft.taxbenefit.core.applicant.dto;

import java.math.BigDecimal;



/**
 * 交费信息
 * 
 * @author zhangke
 *
 */
public class AppPremiumDTO  {
	/** 费用编码 **/
	private String feeId;
	/** 交费频率 **/
	private String paymentFrequency;
	/** 保费应收日期 **/
	private String payDueDate;
	/** 实收保费日期 **/
	private String confirmDate;
	/** 实收保费金额 **/
	private BigDecimal premiumAmount;;
	/** 交费起期 **/
	private String payFromDate;
	/** 交费止期 **/
	private String payToDate;
	/** 保费状态 **/
	private String feeStatus;

	public String getFeeId() {
		return feeId;
	}

	public void setFeeId(String feeId) {
		this.feeId = feeId;
	}

	public String getPaymentFrequency() {
		return paymentFrequency;
	}

	public void setPaymentFrequency(String paymentFrequency) {
		this.paymentFrequency = paymentFrequency;
	}

	public String getPayDueDate() {
		return payDueDate;
	}

	public void setPayDueDate(String payDueDate) {
		this.payDueDate = payDueDate;
	}

	public String getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(String confirmDate) {
		this.confirmDate = confirmDate;
	}

	public BigDecimal getPremiumAmount() {
		return premiumAmount;
	}

	public void setPremiumAmount(BigDecimal premiumAmount) {
		this.premiumAmount = premiumAmount;
	}

	public String getPayFromDate() {
		return payFromDate;
	}

	public void setPayFromDate(String payFromDate) {
		this.payFromDate = payFromDate;
	}

	public String getPayToDate() {
		return payToDate;
	}

	public void setPayToDate(String payToDate) {
		this.payToDate = payToDate;
	}

	public String getFeeStatus() {
		return feeStatus;
	}

	public void setFeeStatus(String feeStatus) {
		this.feeStatus = feeStatus;
	}

}
