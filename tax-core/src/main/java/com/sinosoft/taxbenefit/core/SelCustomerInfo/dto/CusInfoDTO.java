package com.sinosoft.taxbenefit.core.SelCustomerInfo.dto;

import java.util.List;
/**
 * title：客户概要信息查询返回DTO
 * @author zhangyu
 * @date 2016年3月6日 下午1:37:29
 */
public class CusInfoDTO {
		//平台客户编码
		private String customerNo;
	    //国籍
		private String nationality;
		//保单列表
		private List<PolicyInfo> policyList;
		//赔案列表
		private List<ClaimInfo> claimList;
		
		public List<PolicyInfo> getPolicyList() {
			return policyList;
		}
		public void setPolicyList(List<PolicyInfo> policyList) {
			this.policyList = policyList;
		}
		public List<ClaimInfo> getClaimList() {
			return claimList;
		}
		public void setClaimList(List<ClaimInfo> claimList) {
			this.claimList = claimList;
		}
		public String getCustomerNo() {
			return customerNo;
		}
		public void setCustomerNo(String customerNo) {
			this.customerNo = customerNo;
		}
		public String getNationality() {
			return nationality;
		}
		public void setNationality(String nationality) {
			this.nationality = nationality;
		}
}