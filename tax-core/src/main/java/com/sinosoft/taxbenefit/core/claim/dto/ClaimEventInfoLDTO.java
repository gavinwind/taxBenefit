package com.sinosoft.taxbenefit.core.claim.dto;

import java.util.List;

/**
 * 医疗事件信息
 * 
 * @author zhangke
 *
 */
public class ClaimEventInfoLDTO {
	/** 日期 **/
	private String date;
	/** 医疗事件类型 **/
	private String eventType;
	/** 医疗机构代码 **/
	private String hospitalCode;
	/**机构名称*/
	private String hospitalName;
	/**是否手术*/
	private String operation;
	
	public String getHospitalName() {
		return hospitalName;
	}

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	/** 西医疾病 **/
	private List<ClaimWesternMedInfoDTO> westernMediList;
	/** 手术 */
	private List<ClaimOperationInfoDTO> operationList;
	/** 收据 **/
	private List<ClaimReceiptInfoDTO> receiptList;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getHospitalCode() {
		return hospitalCode;
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}

	public List<ClaimReceiptInfoDTO> getReceiptList() {
		return receiptList;
	}

	public void setReceiptList(List<ClaimReceiptInfoDTO> receiptList) {
		this.receiptList = receiptList;
	}

	public List<ClaimWesternMedInfoDTO> getWesternMediList() {
		return westernMediList;
	}

	public void setWesternMediList(
			List<ClaimWesternMedInfoDTO> westernMediList) {
		this.westernMediList = westernMediList;
	}

	public List<ClaimOperationInfoDTO> getOperationList() {
		return operationList;
	}

	public void setOperationList(List<ClaimOperationInfoDTO> operationList) {
		this.operationList = operationList;
	}

}
