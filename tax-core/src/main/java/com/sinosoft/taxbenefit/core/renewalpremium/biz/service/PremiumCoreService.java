package com.sinosoft.taxbenefit.core.renewalpremium.biz.service;


import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqMPremiumInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqSPremiumInfoDTO;
import com.sinosoft.taxbenefit.core.generated.model.TaxFaultTask;

public interface PremiumCoreService {
	
	/**
	 * 续期保费信息上传（单个）
	 * @param ReqRenewalPremiumInfoDTO
	 */
	ThirdSendResponseDTO premiumSettlement(ReqSPremiumInfoDTO reqSPremiumInfoDTO,ParamHeadDTO paramHead);
	
	/**
	 * 续期保费信息上传(多个)
	 * @param reqSPremiumInfoDTO
	 * @param paramHead
	 * @return
	 */
	ThirdSendResponseDTO premiumMSettlement(ReqMPremiumInfoDTO reqMPremiumInfoDTO,ParamHeadDTO paramHead);
	
	/**
	 * 续期保费信息上传异常任务服务方法
	 * @param reqJSON	核心请求JSON
	 * @param portCode	异常任务接口编码
	 * @param faultTask 任务记录
	 * @return
	 */
	TaxFaultTask premiumExceptionJob(String reqJSON, String portCode, Integer faultTaskId);

}
