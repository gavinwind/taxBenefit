package com.sinosoft.taxbenefit.core.bargaincheckquery.dto;

import java.util.List;

import com.sinosoft.taxbenefit.api.base.EBResultCodeDTO;

/**
 * 交易核对信息查询中保信返回
 * Title:ResBargainCheckDTO
 * @author yangdongkai@outlook.com
 * @date 2016年3月10日 下午9:34:57
 */
public class ResultBargainCheckDTO{
	//汇总信息
	private ResultBargainCheckTotalDTO total;
	//核对明细
	private List<ResultBargainCheckDetailDTO> detail;
	//返回编码
	private EBResultCodeDTO result; 
	
	/**
	 * @return the detail
	 */
	public List<ResultBargainCheckDetailDTO> getDetail() {
		return detail;
	}
	/**
	 * @param detail the detail to set
	 */
	public void setDetail(List<ResultBargainCheckDetailDTO> detail) {
		this.detail = detail;
	}
	public EBResultCodeDTO getResult() {
		return result;
	}
	public void setResult(EBResultCodeDTO result) {
		this.result = result;
	}
	/**
	 * @return the total
	 */
	public ResultBargainCheckTotalDTO getTotal() {
		return total;
	}
	/**
	 * @param total the total to set
	 */
	public void setTotal(ResultBargainCheckTotalDTO total) {
		this.total = total;
	}
}
