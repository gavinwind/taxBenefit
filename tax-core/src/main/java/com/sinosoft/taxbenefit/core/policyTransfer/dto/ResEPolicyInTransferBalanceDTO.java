package com.sinosoft.taxbenefit.core.policyTransfer.dto;

import java.util.List;

import com.sinosoft.taxbenefit.api.base.EBResultCodeDTO;

/**
 * （中保信）保单转入余额信息查询结果 返回解析DTO
 * @author SheChunMing
 */
public class ResEPolicyInTransferBalanceDTO {
	// 转出保单
	private List<EPolicyTransferOutDTO> transferOutList;
	// 查询结果
	private EBResultCodeDTO result;
	
	public List<EPolicyTransferOutDTO> getTransferOutList() {
		return transferOutList;
	}
	public void setTransferOutList(List<EPolicyTransferOutDTO> transferOutList) {
		this.transferOutList = transferOutList;
	}
	public EBResultCodeDTO getResult() {
		return result;
	}
	public void setResult(EBResultCodeDTO result) {
		this.result = result;
	}
}
