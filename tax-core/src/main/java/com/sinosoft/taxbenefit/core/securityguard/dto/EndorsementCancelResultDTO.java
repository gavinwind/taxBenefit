package com.sinosoft.taxbenefit.core.securityguard.dto;
/**
 * 保全撤销 中保信返回DTO
 * @author zhangke
 *
 */
public class EndorsementCancelResultDTO {
	private EndorsementCancelResultBodyDTO endorsementCancelResult;

	public EndorsementCancelResultBodyDTO getEndorsementCancelResult() {
		return endorsementCancelResult;
	}

	public void setEndorsementCancelResult(
			EndorsementCancelResultBodyDTO endorsementCancelResult) {
		this.endorsementCancelResult = endorsementCancelResult;
	}

}
