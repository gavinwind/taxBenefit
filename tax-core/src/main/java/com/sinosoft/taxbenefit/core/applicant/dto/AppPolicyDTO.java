package com.sinosoft.taxbenefit.core.applicant.dto;

import java.util.List;

/**
 * 保单信息
 * 
 * @author zhanghao@sinosoft.com.cn
 *
 */
public class AppPolicyDTO {

	// 个人保单
	private AppSinglePolicyHolderDTO singlePolicyHolder;
	// 团单
	private AppGroupPolicyHolderDTO groupPolicyHolder;
	// 被保人
	private List<AppInsuredDTO> insuredList;

	/** 保单号 **/
	private String policyNo;
	/** 保单类别 **/
	private String policyType;
	/** 保单所属二级机构名称 **/
	private String policyOrg;
	/** 保单所属区域 **/
	private String policyArea;
	/** 销售渠道 **/
	private String salesChannel;
	/** 销售地区 **/
	private String salesArea;
	/** 投保日期 **/
	private String applicationDate;
	/** 承保日期 **/
	private String acceptDate;
	/** 生效日期 **/
	private String effectiveDate;
	/** 满期日期 **/
	private String expireDate;
	/** 保单来源 **/
	private String policySource;
	/** 保单状态 **/
	private String policyStatus;
	/** 转出保险公司代码 **/
	private String originalOrganCode;
	/** 转出保单号 **/
	private String originalPolicyNo;

	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public String getPolicyType() {
		return policyType;
	}

	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}

	public String getPolicyOrg() {
		return policyOrg;
	}

	public void setPolicyOrg(String policyOrg) {
		this.policyOrg = policyOrg;
	}

	public String getPolicyArea() {
		return policyArea;
	}

	public void setPolicyArea(String policyArea) {
		this.policyArea = policyArea;
	}

	public String getSalesChannel() {
		return salesChannel;
	}

	public void setSalesChannel(String salesChannel) {
		this.salesChannel = salesChannel;
	}


	public String getSalesArea() {
		return salesArea;
	}

	public void setSalesArea(String salesArea) {
		this.salesArea = salesArea;
	}

	public String getApplicationDate() {
		return applicationDate;
	}

	public void setApplicationDate(String applicationDate) {
		this.applicationDate = applicationDate;
	}

	public String getAcceptDate() {
		return acceptDate;
	}

	public void setAcceptDate(String acceptDate) {
		this.acceptDate = acceptDate;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}

	public String getPolicySource() {
		return policySource;
	}

	public void setPolicySource(String policySource) {
		this.policySource = policySource;
	}

	public String getPolicyStatus() {
		return policyStatus;
	}

	public void setPolicyStatus(String policyStatus) {
		this.policyStatus = policyStatus;
	}

	public String getOriginalOrganCode() {
		return originalOrganCode;
	}

	public void setOriginalOrganCode(String originalOrganCode) {
		this.originalOrganCode = originalOrganCode;
	}

	public String getOriginalPolicyNo() {
		return originalPolicyNo;
	}

	public void setOriginalPolicyNo(String originalPolicyNo) {
		this.originalPolicyNo = originalPolicyNo;
	}

	public AppSinglePolicyHolderDTO getSinglePolicyHolder() {
		return singlePolicyHolder;
	}

	public void setSinglePolicyHolder(
			AppSinglePolicyHolderDTO singlePolicyHolder) {
		this.singlePolicyHolder = singlePolicyHolder;
	}

	public AppGroupPolicyHolderDTO getGroupPolicyHolder() {
		return groupPolicyHolder;
	}

	public void setGroupPolicyHolder(AppGroupPolicyHolderDTO groupPolicyHolder) {
		this.groupPolicyHolder = groupPolicyHolder;
	}

	public List<AppInsuredDTO> getInsuredList() {
		return insuredList;
	}

	public void setInsuredList(List<AppInsuredDTO> insuredList) {
		this.insuredList = insuredList;
	}

}
