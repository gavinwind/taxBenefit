package com.sinosoft.taxbenefit.core.applicant.config;
/**
 * 保全变更类型
 * @author zhanghaosh@sinosoft.com.cn
 *
 */
public enum ENUM_APPLICANT_PORT_CODE {
	
	APPLICANT_ONE("NBU001","单笔承保信息上传"),APPLICANT_MORE("NBU002","批量承保信息上传");
	
	private final String code;
	private final String desc;
	
	ENUM_APPLICANT_PORT_CODE(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public String code() {
		return code;
	}

	public String desc() {
		return desc;
	}
	public static ENUM_APPLICANT_PORT_CODE getEnumByKey(String key){
		for(ENUM_APPLICANT_PORT_CODE enumItem:ENUM_APPLICANT_PORT_CODE.values()){
			if(key.equals(enumItem.code())){
				return enumItem;
			}
		}
		return null;
	}
	
	
}
