package com.sinosoft.taxbenefit.core.renewalpremium.dto;

import java.util.List;

/**
 * Title:中保信-续期上传信息
 * @author yangdongkai@outlook.com
 * @date 2016年3月5日 下午4:37:39
 */
public class ReqRenewalPremiumDTO {
	//保费信息
	private List<ReqPremiumDTO> premiumList;

	/**
	 * @return the premiumList
	 */
	public List<ReqPremiumDTO> getPremiumList() {
		return premiumList;
	}

	/**
	 * @param premiumList the premiumList to set
	 */
	public void setPremiumList(List<ReqPremiumDTO> premiumList) {
		this.premiumList = premiumList;
	}
}
