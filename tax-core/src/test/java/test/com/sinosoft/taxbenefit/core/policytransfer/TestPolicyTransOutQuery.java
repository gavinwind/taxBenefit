package test.com.sinosoft.taxbenefit.core.policytransfer;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

import com.sinosoft.platform.base.TaxBaseTest;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqPolicyInTransferQueryDTO;
import com.sinosoft.taxbenefit.core.policyTransfer.biz.service.PolicyTransferService;

public class TestPolicyTransOutQuery extends TaxBaseTest{
	
	@Autowired
	PolicyTransferService policyTransferService;

	@Test
	@Rollback(false)
	public void policyTransOutQueryDemo(){
		ReqPolicyInTransferQueryDTO reqPolicyInTransferQuery = new ReqPolicyInTransferQueryDTO();
		reqPolicyInTransferQuery.setQueryStartDate("2016-02-10");
		reqPolicyInTransferQuery.setQueryEndDate("2016-02-15");
		
		ParamHeadDTO paramHead=new ParamHeadDTO();
		paramHead.setAreaCode("shanghai");
		paramHead.setRecordNum("1");
		paramHead.setSerialNo("A1100001");
		paramHead.setPortCode("END004");
		paramHead.setPortName("保单转移");
		
//		policyTransferService.policyTransOutQuerySettlement(reqPolicyInTransferQuery, paramHead);
		
		policyTransferService.policyTransOutRegisterQuerySettlement(reqPolicyInTransferQuery, paramHead);
	}
	
}
