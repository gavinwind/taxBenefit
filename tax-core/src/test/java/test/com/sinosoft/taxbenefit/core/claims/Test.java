package test.com.sinosoft.taxbenefit.core.claims;

import java.util.ArrayList;
import java.util.List;

import com.sinosoft.platform.common.util.ReflectionUtil;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimCoveragePaymentInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimDetailInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimEventInfoLDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimItemInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimLiabilityPaymentInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimOperationInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimPolicyInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimReceiptFeeInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimReceiptInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimWesternMedInfoDTO;
import com.sinosoft.taxbenefit.core.claim.dto.ClaimCoveragePaymentInfoDTO;
import com.sinosoft.taxbenefit.core.claim.dto.ClaimDetailInfoDTO;
import com.sinosoft.taxbenefit.core.claim.dto.ClaimEventInfoLDTO;
import com.sinosoft.taxbenefit.core.claim.dto.ClaimInfoDTO;
import com.sinosoft.taxbenefit.core.claim.dto.ClaimItemInfoDTO;
import com.sinosoft.taxbenefit.core.claim.dto.ClaimLiabilityPaymentInfoDTO;
import com.sinosoft.taxbenefit.core.claim.dto.ClaimOperationInfoDTO;
import com.sinosoft.taxbenefit.core.claim.dto.ClaimPolicyInfoDTO;
import com.sinosoft.taxbenefit.core.claim.dto.ClaimReceiptFeeInfoDTO;
import com.sinosoft.taxbenefit.core.claim.dto.ClaimReceiptInfoDTO;
import com.sinosoft.taxbenefit.core.claim.dto.ClaimWesternMedInfoDTO;

public class Test {

	public static void main(String[] args) {
//		//请求中保信JSON 
//		String str1="{\"claimList\" : [ {\"claimNo\" : \"0008843784738025\",\"reportDate\" : \"2015-07-20\",\"applyDate\" : \"2015-07-20\",\"registrationDate\" : \"2015-07-20\",\"accidentDate\" : \"2015-07-20\",\"accidentReason\" : \"aClaimCase.AccidentReason\",\"accidentPlace\" : \"aClaimCase.AccidentPlace\",\"claimInvestigation\" : \"aClaimCase.ClaimInvestigation\",\"claimConclusionCode\" : \"aClaimCase.ClaimConclusionCode\",\"conclusionReason\" : \"aClaimCase.ConclusionReason\",\"claimAmount\" : 20000,\"endCaseDate\" : \"2015-07-20\",\"status\" : \"status1\",\"claimantName\" : \"aClaimCase.ClaimantName\",\"claimantGender\": \"aClaimCase.ClaimantGender\",\"claimantBirthday\" : \"2015-07-20\",\"claimantCertiType\" : \"aClaimCase.ClaimantCertiType\",\"claimantCertiCode\" : \"aClaimCase.ClaimantCertiCode\",\"deathIndi\" : \"aClaimCase.DeathIndi\",\"deathDate\" : \"2015-07-20\",\"warningIndi\" : \"Y\",\"warningDesc\" : \"aClaimCase.WarningDesc\",\"policyList\" : [ {\"policyNo\" : \"aClaimPolicy.PolicyNo01\",\"sequenceNo\" : \"aClaimPolicy.SequenceNo01\",\"customerNo\" : \"aClaimPolicy.CustomerNo01\",\"policyStatus\" : \"aClaimPolicy.PolicyStatus01\",\"claimAmount\" : 10002,\"coveragePaymentList\" : [ {\"coveragePackageCode\" : \"aClaimProduct.CoveragePackageCode01\",\"coverageType\" : \"aClaimProduct.CoverageType01\",\"comCoverageCode\" : \"aClaimProduct.ComCoverageCode01\",\"comCoverageName\" : \"aClaimProduct.ComCoverageName01\",\"claimOpinion\" : \"aClaimProduct.ClaimOpinion01\",\"claimConclusionCode\" : \"aClaimProduct.ClaimConclusionCode01\",\"conclusionReason\" : \"aClaimProduct.ConclusionReason01\",\"claimAmount\" : 10002,\"liabilityPaymentList\" : [ {\"liabilityType\" : \"aClaimLiabPay.LiabilityType02\",\"liabilityName\" : \"aClaimLiabPay.LiabilityName02\",\"claimOpinion\" : \"aClaimLiabPay.ClaimOpinion02\",\"claimConclusionCode\" : \"aClaimLiabPay.ClaimConclusionCode02\",\"conclusionReason\" : \"aClaimLiabPay.ConclusionReason02\",\"paymentAmount\" : 10002,\"operation\" : \"aClaimLiabPay.Operation02\",\"westernMediList\" :[ {\"westernMediCode\" : \"aClaimLiabWesternMedi.WesternMediCode->>01\"}, {\"hospitalCode\" : \"aClaimLiabWesternMedi.HospitalCode-->>02\",\"westernMediCode\" : \"aClaimLiabWesternMedi.WesternMediCode->>02\"} ],\"operationList\" : [ {\"operationCode\" : \"aliabClaimOperation.OperationCode-->>01\"}, {\"operationCode\" : \"aliabClaimOperation.OperationCode-->>02\"} ]} ]} ]} ],\"eventList\" : [ {\"date\" : \"2015-07-20\",\"eventType\" : \"aClaimEvent.EventType\",\"hospitalCode\" : \"aClaimEvent.HospitalCode\",\"hospitalName\":\"hospitalName\",\"operation\" : \"aClaimEvent.Operation\",\"westernMediCodeList\" : [ {\"westernMediCode\" : \"ClaimEventWesternMedi.WesternMediCode\"} ],\"operationList\" : [ {\"operationCode\" : \"aliabClaimOperation.OperationCode-->>01\"}, {\"operationCode\" : \"aliabClaimOperation.OperationCode-->>02\"} ],\"receiptList\" : [ {\"receiptNo\" : \"aClaimReceipt.receiptNo\",\"receiptType\" : \"aClaimReceipt.ReceiptType\",\"hospitalCode\" : \"aClaimReceipt.HospitalCode\",\"date\" : \"2015-07-20\",\"amount\" : 20,\"hospitalDate\" : \"2015-07-20\",\"dischargeDate\" : \"2015-07-20\",\"hospitalStay\" : 20,\"hospitalDept\" : \"aClaimReceipt.HospitalDept\",\"issueUnit\" : \"aClaimReceipt.unit\",\"relatedClaimNo\" : \"relatedClaimNo\",\"receiptFeeList\" : [ {\"receiptFeeType\" : \"ClaimReceiptFee.ReceiptFeeType\",\"value\" : 20} ],\"itemList\" : [ {\"itemCategory\" : \"aClaimMediFee.ItemCategory\",\"amount\" : 200,\"deductibleAmount\" : 201,\"detailList\" : [ {\"name\" : \"ClaimMediFeeDetail.name\",\"amount\" : 20000,\"deductibleAmount\" : 20000,\"quantity\" : 20,\"unitPrice\" : 20.76,\"unit\" : \"ClaimMediFeeDetail.unit\"} ]} ]} ]} ]} ]}";
//		ClaimCoreDTO claimInfo=new ClaimCoreDTO();
//		claimInfo=JSONObject.parseObject(str1, ClaimCoreDTO.class);
//		System.out.println(claimInfo.getClaimList().get(0).getAccidentDate());
//				
//		//中保信返回JSON
//		String str2="{\"claimResultList\": [{\"claimNo\": \"687368274\",\"claimCodeP\": \"7673437\",\"result\": {\"resultCode\": \"result.ResultCode\",\"resultMessage\": [\"resultMessage1\"]}}]}";
//		ClaimResultDTO claimResultInfo=new ClaimResultDTO();
//		claimResultInfo=JSONObject.parseObject(str2, ClaimResultDTO.class);
//		System.out.println(claimResultInfo.getClaimResultList());
		ReqClaimInfoDTO claimInfoDTO = new ReqClaimInfoDTO();
		claimInfoDTO.setBizNo("业务号");
		claimInfoDTO.setClaimNo("理赔号");
		claimInfoDTO.setReportDate("2000-01-01");
		claimInfoDTO.setApplyDate("2000-01-01");
		claimInfoDTO.setRegistrationDate("2000-01-01");
		claimInfoDTO.setAccidentReason("1");
		claimInfoDTO.setAccidentDate("2000-01-01");
		claimInfoDTO.setAccidentPlace("出险地点");
		claimInfoDTO.setClaimInvestigation("1");
		claimInfoDTO.setPendingIndi("2");
		claimInfoDTO.setPendingFinishDate("2000-01-01");
		claimInfoDTO.setClaimConclusionCode("2");
		claimInfoDTO.setConclusionReason("2 ");
		claimInfoDTO.setApplyAmount("5000");
		claimInfoDTO.setClaimAmount("4000");
		claimInfoDTO.setSocialInsuPayment("500");
		claimInfoDTO.setEndCaseDate("2000-01-01");
		claimInfoDTO.setStatus("1");
		claimInfoDTO.setClaimantName("张三");
		claimInfoDTO.setClaimantGender("1");
		claimInfoDTO.setClaimantBirthday("1990-01-01");
		claimInfoDTO.setClaimantCertiType("1");
		claimInfoDTO.setClaimantCertiCode("421002199004100518");
		claimInfoDTO.setDeathIndi("1");
		claimInfoDTO.setDeathDate("2000-01-01");
		claimInfoDTO.setWarningIndi("1");
		claimInfoDTO.setWarningDesc("2");
		/** 理赔保单信息 **/
		List<ReqClaimPolicyInfoDTO> policyList = new ArrayList<ReqClaimPolicyInfoDTO>();
		ReqClaimPolicyInfoDTO reqClaimPolicyInfoDTO = new ReqClaimPolicyInfoDTO();
		reqClaimPolicyInfoDTO.setPolicyNo("保单号A1");
		reqClaimPolicyInfoDTO.setSequenceNo("分单号1");
		reqClaimPolicyInfoDTO.setCustomerNo("客户编码");
		reqClaimPolicyInfoDTO.setPolicyStatus("1");
		reqClaimPolicyInfoDTO.setClaimAmount("5000");
		/** 险种赔付信息 */
		List<ReqClaimCoveragePaymentInfoDTO> coveragePaymentList = new ArrayList<ReqClaimCoveragePaymentInfoDTO>();
		ReqClaimCoveragePaymentInfoDTO reqClaimCoveragePaymentInfoDTO = new ReqClaimCoveragePaymentInfoDTO();
		reqClaimCoveragePaymentInfoDTO.setCoveragePackageCode("rr");
		reqClaimCoveragePaymentInfoDTO.setCoverageType("r");
		reqClaimCoveragePaymentInfoDTO.setComCoverageCode("rrr");
		reqClaimCoveragePaymentInfoDTO.setComCoverageName("险种名称1");
		reqClaimCoveragePaymentInfoDTO.setClaimOpinion("理赔意见111");
		reqClaimCoveragePaymentInfoDTO.setClaimConclusionCode("1");
		reqClaimCoveragePaymentInfoDTO.setConclusionReason("非正常赔付原因 ");
		reqClaimCoveragePaymentInfoDTO.setClaimAmount("2000");
		/** 责任赔付信息 **/
		List<ReqClaimLiabilityPaymentInfoDTO> liabilityPaymentList = new ArrayList<ReqClaimLiabilityPaymentInfoDTO>();
		ReqClaimLiabilityPaymentInfoDTO reqClaimLiabilityPaymentInfoDTO = new ReqClaimLiabilityPaymentInfoDTO();
		reqClaimLiabilityPaymentInfoDTO.setLiabilityType("332");
		reqClaimLiabilityPaymentInfoDTO.setLiabilityName("公司责任名称A");
		reqClaimLiabilityPaymentInfoDTO.setLiabilityCode("1");
		reqClaimLiabilityPaymentInfoDTO.setClaimOpinion("理赔意见");
		reqClaimLiabilityPaymentInfoDTO.setClaimConclusionCode("2");
		reqClaimLiabilityPaymentInfoDTO.setConclusionReason("非正常赔付原因");
		reqClaimLiabilityPaymentInfoDTO.setPaymentAmount("5000");
		reqClaimLiabilityPaymentInfoDTO.setOperation("1");
		List<ReqClaimWesternMedInfoDTO> westernMediList = new ArrayList<ReqClaimWesternMedInfoDTO>();
		/** 西医疾病 **/
		ReqClaimWesternMedInfoDTO reqClaimWesternMedInfoDTO = new ReqClaimWesternMedInfoDTO();
		reqClaimWesternMedInfoDTO.setHospitalCode("2");
		reqClaimWesternMedInfoDTO.setHospitalName("医疗机构名称");
		reqClaimWesternMedInfoDTO.setWesternMediCode("1");
		westernMediList.add(reqClaimWesternMedInfoDTO);
		reqClaimLiabilityPaymentInfoDTO.setWesternMediList(westernMediList);
		/** 手术 **/
		List<ReqClaimOperationInfoDTO> operationList = new ArrayList<ReqClaimOperationInfoDTO>();
		ReqClaimOperationInfoDTO reqClaimOperationInfoDTO = new ReqClaimOperationInfoDTO();
		reqClaimOperationInfoDTO.setOperationCode("2");
		operationList.add(reqClaimOperationInfoDTO);
		reqClaimLiabilityPaymentInfoDTO.setOperationList(operationList);
		liabilityPaymentList.add(reqClaimLiabilityPaymentInfoDTO);
		reqClaimCoveragePaymentInfoDTO.setLiabilityPaymentList(liabilityPaymentList);
		coveragePaymentList.add(reqClaimCoveragePaymentInfoDTO);
		reqClaimPolicyInfoDTO.setCoveragePaymentList(coveragePaymentList);
		policyList.add(reqClaimPolicyInfoDTO);
		claimInfoDTO.setPolicyList(policyList);
		/** 医疗事件信息 **/
		List<ReqClaimEventInfoLDTO> eventList = new ArrayList<ReqClaimEventInfoLDTO>();
		ReqClaimEventInfoLDTO reqClaimEventInfoLDTO = new ReqClaimEventInfoLDTO();
		reqClaimEventInfoLDTO.setEventDate("2001-01-01");
		reqClaimEventInfoLDTO.setEventType("2");
		reqClaimEventInfoLDTO.setHospitalCode("2 ");
		/** 西医疾病 **/
		List<ReqClaimWesternMedInfoDTO>list=new ArrayList<ReqClaimWesternMedInfoDTO>();
		ReqClaimWesternMedInfoDTO werClaimWesternMedInfoDTO=new ReqClaimWesternMedInfoDTO();
		werClaimWesternMedInfoDTO.setHospitalCode("12");
		werClaimWesternMedInfoDTO.setHospitalName("33");
		werClaimWesternMedInfoDTO.setWesternMediCode("334");
		list.add(werClaimWesternMedInfoDTO);
		reqClaimEventInfoLDTO.setWesternMediList(list);
		/** 手术 **/
		List<ReqClaimOperationInfoDTO>  list2=new ArrayList<ReqClaimOperationInfoDTO>();
		ReqClaimOperationInfoDTO operationInfoDTO=new ReqClaimOperationInfoDTO();
		operationInfoDTO.setOperationCode("23");
		list2.add(operationInfoDTO);
		reqClaimEventInfoLDTO.setOperationList(list2);
		/** 收据 **/
		List<ReqClaimReceiptInfoDTO> receiptList = new ArrayList<ReqClaimReceiptInfoDTO>();
		ReqClaimReceiptInfoDTO reqClaimReceiptInfoDTO = new ReqClaimReceiptInfoDTO();
		reqClaimReceiptInfoDTO.setReceiptNo("收据编号aaa");
		reqClaimReceiptInfoDTO.setReceiptType("2");
		reqClaimReceiptInfoDTO.setHospitalCode("2");
		reqClaimReceiptInfoDTO.setHospitalName("机构名称");
		reqClaimReceiptInfoDTO.setOccurDate("2001-01-01");
		reqClaimReceiptInfoDTO.setAmount("5000");
		reqClaimReceiptInfoDTO.setHospitalDate("2001-01-01");
		reqClaimReceiptInfoDTO.setDischargeDate("2001-01-01");
		reqClaimReceiptInfoDTO.setHospitalStay("1");
		reqClaimReceiptInfoDTO.setHospitalDept("3");
		reqClaimReceiptInfoDTO.setIssueUnit("2");
		reqClaimReceiptInfoDTO.setRelatedClaimNo("3");
		/** 收据分项费用 **/
		List<ReqClaimReceiptFeeInfoDTO> receiptFeeList = new ArrayList<ReqClaimReceiptFeeInfoDTO>();
		ReqClaimReceiptFeeInfoDTO reqClaimReceiptFeeInfoDTO = new ReqClaimReceiptFeeInfoDTO();
		reqClaimReceiptFeeInfoDTO.setReceiptFeeType("3 ");
		reqClaimReceiptFeeInfoDTO.setAmount("3000");
		receiptFeeList.add(reqClaimReceiptFeeInfoDTO);
		reqClaimReceiptInfoDTO.setReceiptFeeList(receiptFeeList);
		/** 医疗费用 **/
		List<ReqClaimItemInfoDTO> itemList = new ArrayList<ReqClaimItemInfoDTO>();
		ReqClaimItemInfoDTO reqClaimItemInfoDTO = new ReqClaimItemInfoDTO();
		reqClaimItemInfoDTO.setItemCategory("3");
		reqClaimItemInfoDTO.setAmount("4000");
		reqClaimItemInfoDTO.setDeductibleAmount("1500");
		/** 医疗费用明细 **/
		List<ReqClaimDetailInfoDTO> detailList = new ArrayList<ReqClaimDetailInfoDTO>();
		ReqClaimDetailInfoDTO reqClaimDetailInfoDTO = new ReqClaimDetailInfoDTO();
		reqClaimDetailInfoDTO.setName("费用名称");
		reqClaimDetailInfoDTO.setAmount("2000");
		reqClaimDetailInfoDTO.setDeductibleAmount("1000");
		reqClaimDetailInfoDTO.setQuantity("5");
		reqClaimDetailInfoDTO.setUnitPrice("200");
		reqClaimDetailInfoDTO.setUnit("2 ");	
		detailList.add(reqClaimDetailInfoDTO);
		reqClaimItemInfoDTO.setDetailList(detailList);
		itemList.add(reqClaimItemInfoDTO);
		reqClaimReceiptInfoDTO.setItemList(itemList);
		receiptList.add(reqClaimReceiptInfoDTO);
		reqClaimEventInfoLDTO.setReceiptList(receiptList);
		eventList.add(reqClaimEventInfoLDTO);
		claimInfoDTO.setEventList(eventList);
//		ClaimInfoDTO claimInfo=	getClaimInfoDTO(claimInfoDTO);
		System.out.println(2);
	
	}
	
	/**
	 * 将核心DTO转为中保信DTO
	 * 
	 * @return
	 */
	public static ClaimInfoDTO getClaimInfoDTO(ReqClaimInfoDTO reqClaimInfoDTO) {
		ClaimInfoDTO claimInfoDTO = new ClaimInfoDTO();	
		//1将核心DTO转为中保信DTO
		ReflectionUtil.copyProperties(reqClaimInfoDTO, claimInfoDTO);
		// 转码操作
		//claimInfoDTO.setAccidentReason(transCodeService.transCode("accidentReson", claimInfoDTO.getAccidentReason()));
	//	claimInfoDTO.setClaimantCertiCode(transCodeService.transCode("claimantCertiCode", claimInfoDTO.getClaimantCertiCode()));
	//	claimInfoDTO.setStatus(transCodeService.transCode("flowState", claimInfoDTO.getStatus()));
	//	claimInfoDTO.setClaimantGender(transCodeService.transCode("gender",claimInfoDTO.getClaimantGender()));
	//	claimInfoDTO.setClaimantCertiCode(transCodeService.transCode("cardType",claimInfoDTO.getClaimantCertiType()));
		//医疗事件集合
		List<ClaimEventInfoLDTO> eventList=new ArrayList<ClaimEventInfoLDTO>();
		ClaimEventInfoLDTO event=null;
		//医疗事件下的手术集合
		List<ClaimOperationInfoDTO>operationList=new ArrayList<ClaimOperationInfoDTO>();
		ClaimOperationInfoDTO operation=null;
		//医疗事件下的西医集合
		List<ClaimWesternMedInfoDTO> westernMedInfoList=new ArrayList<ClaimWesternMedInfoDTO>();
		ClaimWesternMedInfoDTO westernMedInfo=null;
		//医疗事件下的收据 集合
		List<ClaimReceiptInfoDTO> receiptInfoList=new ArrayList<ClaimReceiptInfoDTO>();
		ClaimReceiptInfoDTO receiptInfoDTO=null;
		//收据下的医疗费用集合
		List<ClaimItemInfoDTO>itemInfoList=new ArrayList<ClaimItemInfoDTO>();
		ClaimItemInfoDTO itemInfoDTO=null;
		//收据下收据分项费用集合
		List<ClaimReceiptFeeInfoDTO>receiptFeeInfoList=new ArrayList<ClaimReceiptFeeInfoDTO>();
		ClaimReceiptFeeInfoDTO receiptFeeInfoDTO=null;
		//医疗费用下的医疗费用明细
		List<ClaimDetailInfoDTO>detailInfoDTOList=new ArrayList<ClaimDetailInfoDTO>();	
		ClaimDetailInfoDTO detailInfoDTO=null;
		//循环医疗事件
		for (ReqClaimEventInfoLDTO reqEvent : reqClaimInfoDTO.getEventList()) {		
			event=new ClaimEventInfoLDTO();
			ReflectionUtil.copyProperties(reqEvent,event);
		//	event.setEventType(transCodeService.transCode("eventType", reqEvent.getEventType()));
			//循环手术
			for (ReqClaimOperationInfoDTO reqopClaimOperation : reqEvent.getOperationList()) {
				operation=new ClaimOperationInfoDTO();
				ReflectionUtil.copyProperties(reqopClaimOperation,operation);
				operationList.add(operation);
			}
			//循环西医
			for (ReqClaimWesternMedInfoDTO reqwClaimWestern : reqEvent.getWesternMediList()) {
				westernMedInfo=new ClaimWesternMedInfoDTO();
				ReflectionUtil.copyProperties(reqwClaimWestern, westernMedInfo);
				westernMedInfoList.add(westernMedInfo);
			}
			//循环收据
			for (ReqClaimReceiptInfoDTO reqReceiptInfo: reqEvent.getReceiptList()) {
				receiptInfoDTO=new ClaimReceiptInfoDTO();
				ReflectionUtil.copyProperties(reqReceiptInfo, receiptInfoDTO);
				//循环医疗费用
				for (ReqClaimItemInfoDTO reqItem : reqReceiptInfo.getItemList()) {
					itemInfoDTO=new ClaimItemInfoDTO();
					ReflectionUtil.copyProperties(reqItem,itemInfoDTO);
					//循环医疗费用明细
					for (ReqClaimDetailInfoDTO reqDetail: reqItem.getDetailList()) {
						detailInfoDTO=new ClaimDetailInfoDTO();
						ReflectionUtil.copyProperties(reqDetail,detailInfoDTO);
						detailInfoDTOList.add(detailInfoDTO);
					}
					itemInfoDTO.setDetailList(detailInfoDTOList);
			//		itemInfoDTO.setItemCategory(transCodeService.transCode("itemCategory",reqItem.getItemCategory()));
					itemInfoList.add(itemInfoDTO);
				}
				//循环收据分项费用
				for (ReqClaimReceiptFeeInfoDTO feeInfoDTO : reqReceiptInfo.getReceiptFeeList()) {
					receiptFeeInfoDTO =new ClaimReceiptFeeInfoDTO();
					ReflectionUtil.copyProperties(feeInfoDTO,receiptFeeInfoDTO);
					receiptFeeInfoList.add(receiptFeeInfoDTO);
				}
				receiptInfoDTO.setReceiptFeeList(receiptFeeInfoList);
				receiptInfoDTO.setItemList(itemInfoList);
				receiptInfoList.add(receiptInfoDTO);		
			}
			event.setReceiptList(receiptInfoList);
			event.setWesternMediList(westernMedInfoList);
			event.setOperationList(operationList);
			eventList.add(event);
		}
		//理赔保单集合
		List<ClaimPolicyInfoDTO>policyInfoList=new ArrayList<ClaimPolicyInfoDTO>();
		//险种赔付信息
		List<ClaimCoveragePaymentInfoDTO> paymentInfoDTOList=new ArrayList<ClaimCoveragePaymentInfoDTO>();
		//责任赔付信息
		List<ClaimLiabilityPaymentInfoDTO> liabilityPaymentInfoList=new ArrayList<ClaimLiabilityPaymentInfoDTO>();
		//责任赔付信息下的西医疾病
		List<ClaimWesternMedInfoDTO>claimWesternMedInfoDTOList=new ArrayList<ClaimWesternMedInfoDTO>();
		//责任赔付信息下的手术
		List<ClaimOperationInfoDTO>claimOperationInfoDTOList=new ArrayList<ClaimOperationInfoDTO>();
		ClaimPolicyInfoDTO policyInfo=null;
		ClaimCoveragePaymentInfoDTO paymentInfoDTO=null;
		ClaimLiabilityPaymentInfoDTO liabilityPaymentInfoDTO=null;
		ClaimWesternMedInfoDTO claimWesternMedInfoDTO=null;
		ClaimOperationInfoDTO claimOperationInfoDTO=null;
		//循环理赔保单
		for (ReqClaimPolicyInfoDTO reqPolicyInfo : reqClaimInfoDTO.getPolicyList()) {
			policyInfo=new ClaimPolicyInfoDTO();
			ReflectionUtil.copyProperties(reqPolicyInfo,policyInfo);
			//policyInfo.setPolicyStatus(transCodeService.transCode("state", reqPolicyInfo.getPolicyStatus()));
			//循环险种赔付信息
			for (ReqClaimCoveragePaymentInfoDTO reqPayment : reqPolicyInfo.getCoveragePaymentList()) {
				paymentInfoDTO=new ClaimCoveragePaymentInfoDTO();
				ReflectionUtil.copyProperties(reqPayment,paymentInfoDTO);
			//	paymentInfoDTO.setCoverageType(transCodeService.transCode("riskType",reqPayment.getCoverageType()));
			//	paymentInfoDTO.setClaimConclusionCode(transCodeService.transCode("claimConclusionCode", reqPayment.getClaimConclusionCode()));
				//循环责任赔付信息
				for (ReqClaimLiabilityPaymentInfoDTO reqliaClaimLiability : reqPayment.getLiabilityPaymentList()) {
					liabilityPaymentInfoDTO=new ClaimLiabilityPaymentInfoDTO();
					ReflectionUtil.copyProperties(reqliaClaimLiability,liabilityPaymentInfoDTO);
			//		liabilityPaymentInfoDTO.setLiabilityType(transCodeService.transCode("liabilityType", reqliaClaimLiability.getLiabilityType()));
			//		liabilityPaymentInfoDTO.setClaimConclusionCode(transCodeService.transCode("claimConclusionCode", reqliaClaimLiability.getClaimConclusionCode()));
					//循环西医
					for (ReqClaimWesternMedInfoDTO reqwestern : reqliaClaimLiability.getWesternMediList()) {
						claimWesternMedInfoDTO =new ClaimWesternMedInfoDTO();
						ReflectionUtil.copyProperties(reqwestern,claimWesternMedInfoDTO);
						claimWesternMedInfoDTOList.add(claimWesternMedInfoDTO);
					}
					//循环手术
					for (ReqClaimOperationInfoDTO reqOperation : reqliaClaimLiability.getOperationList()) {
						claimOperationInfoDTO =new ClaimOperationInfoDTO();
						ReflectionUtil.copyProperties(reqOperation,claimOperationInfoDTO);
						claimOperationInfoDTOList.add(claimOperationInfoDTO);
					}
					liabilityPaymentInfoDTO.setOperationList(claimOperationInfoDTOList);
					liabilityPaymentInfoDTO.setWesternMediList(claimWesternMedInfoDTOList);
					liabilityPaymentInfoList.add(liabilityPaymentInfoDTO);
				}
				paymentInfoDTO.setLiabilityPaymentList(liabilityPaymentInfoList);
				paymentInfoDTOList.add(paymentInfoDTO);
			}
			policyInfo.setCoveragePaymentList(paymentInfoDTOList);
			policyInfoList.add(policyInfo);
		}
		claimInfoDTO.setPolicyList(policyInfoList);
		claimInfoDTO.setEventList(eventList);
		return claimInfoDTO;
	}
}
