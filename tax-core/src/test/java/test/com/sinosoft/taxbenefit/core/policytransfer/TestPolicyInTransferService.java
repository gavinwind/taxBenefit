package test.com.sinosoft.taxbenefit.core.policytransfer;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

import com.sinosoft.platform.base.TaxBaseTest;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqMInPolicyTransferDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqPolicyTransClientDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqPolicyTransferOutDTO;
//import com.sinosoft.taxbenefit.api.dto.request.ReqPolicyMoveLocationDTO;
import com.sinosoft.taxbenefit.core.policyTransfer.biz.service.PolicyTransferService;

public class TestPolicyInTransferService extends TaxBaseTest{
	@Autowired
	PolicyTransferService policyTransferService;

	@Test
	@Rollback(false)
	public void policyTransSInDemo(){
		List<ReqPolicyTransClientDTO> reqPolicyTransClientList = new ArrayList<ReqPolicyTransClientDTO>();
		ReqPolicyTransClientDTO reqPolicyTransClientDTO = new ReqPolicyTransClientDTO();
		reqPolicyTransClientDTO.setSequenceNo("A01");
		reqPolicyTransClientList.add(reqPolicyTransClientDTO);
		
		List<ReqPolicyTransferOutDTO> reqPolicyTransferOutList = new ArrayList<ReqPolicyTransferOutDTO>();
		ReqPolicyTransferOutDTO reqPolicyTransferOutDTO = new ReqPolicyTransferOutDTO();
		reqPolicyTransferOutDTO.setBizNo("A001");
		reqPolicyTransferOutDTO.setCompanyCode("Z0001");
		reqPolicyTransferOutDTO.setCompanyName("asawfd");
		reqPolicyTransferOutDTO.setCustomerNo("Q120");
		reqPolicyTransferOutDTO.setPolicyNo("1535");
		reqPolicyTransferOutDTO.setClientList(reqPolicyTransClientList);
		
		ReqPolicyTransferOutDTO reqPolicyTransferOutDTO2 = new ReqPolicyTransferOutDTO();
		reqPolicyTransferOutDTO2.setBizNo("A001");
		reqPolicyTransferOutDTO2.setCompanyCode("Z0001");
		reqPolicyTransferOutDTO2.setCompanyName("asawfd");
		reqPolicyTransferOutDTO2.setCustomerNo("Q120");
		reqPolicyTransferOutDTO2.setPolicyNo("1535");
		reqPolicyTransferOutDTO2.setClientList(reqPolicyTransClientList);
		reqPolicyTransferOutList.add(reqPolicyTransferOutDTO);
		reqPolicyTransferOutList.add(reqPolicyTransferOutDTO2);
		
		ReqMInPolicyTransferDTO reqMPolicyTransferDTO = new ReqMInPolicyTransferDTO();
		reqMPolicyTransferDTO.setBankName("ACBD");
		reqMPolicyTransferDTO.setAccountNo("13545");
		reqMPolicyTransferDTO.setAccountHolder("dafgs");
		reqMPolicyTransferDTO.setContactName("AAA");
		reqMPolicyTransferDTO.setContactEmail("dadw2312@qq.com");
		reqMPolicyTransferDTO.setContactTele("13543");
		reqMPolicyTransferDTO.setTransApplyDate("2015-2-15");
		reqMPolicyTransferDTO.setTransferOutList(reqPolicyTransferOutList);
		
		
		ParamHeadDTO paramHead=new ParamHeadDTO();
		paramHead.setAreaCode("shanghai");
		paramHead.setRecordNum("2");
		paramHead.setSerialNo("A1100001");
		paramHead.setPortCode("END005");
		paramHead.setPortName("保单转移");
		policyTransferService.addPolicyTransMInSettlement(reqMPolicyTransferDTO, paramHead);
	}
	
}
