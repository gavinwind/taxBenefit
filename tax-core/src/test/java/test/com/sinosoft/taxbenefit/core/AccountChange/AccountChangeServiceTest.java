/*package test.com.sinosoft.taxbenefit.core.AccountChange;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

import com.sinosoft.platform.base.TaxBaseTest;
import com.sinosoft.platform.common.util.DateUtil;
import com.sinosoft.taxbenefit.api.dto.request.accountchange.ReqAccountInfoDTO;
import com.sinosoft.taxbenefit.core.AccountChange.biz.service.AccountChangeService;


public class AccountChangeServiceTest extends TaxBaseTest{
	

	@Autowired
	public AccountChangeService accountChangeService;
	
	List<ReqAccountInfoDTO> accountInfoDTOList=new ArrayList<ReqAccountInfoDTO>();
	
	@Before
	public void prepareData(){
		
		ReqAccountInfoDTO accountInfoDTO = new ReqAccountInfoDTO();
		accountInfoDTO.setBizNo("A111001");
		accountInfoDTO.setPolicyNo("10000293");
		accountInfoDTO.setComCoverageName("医疗保险");
		accountInfoDTO.setComCoverageCode("979876");
		accountInfoDTO.setComFeeId("7800876");
		Date FeedDate =DateUtil.getCurrentDate();
		accountInfoDTO.setFeeDate(FeedDate);
		accountInfoDTO.setFeeType("理賠");
		accountInfoDTO.setAccountFeeSequenceNo(null);
		accountInfoDTO.setRiskCoverageEndDate(FeedDate);
		accountInfoDTO.setRiskCoverageEndDate(null);
		accountInfoDTO.setInterestRate(null);
		accountInfoDTO.setRevComFeeId("3425");
		
		accountInfoDTOList.add(accountInfoDTO);
	}
	@Test
	@Rollback(false)
	public void AccountChangeServiceTest(){
		try {
			accountChangeService.addAccountChange(accountInfoDTOList);
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
	}

}
*/