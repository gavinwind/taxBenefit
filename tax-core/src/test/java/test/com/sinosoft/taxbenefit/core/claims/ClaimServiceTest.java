package test.com.sinosoft.taxbenefit.core.claims;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

import com.sinosoft.platform.base.TaxBaseTest;
import com.sinosoft.taxbenefit.core.claim.biz.service.ClaimInfoCoreService;
import com.sinosoft.taxbenefit.core.common.config.ENUM_RC_STATE;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxClaimContMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxContClaimMapper;
import com.sinosoft.taxbenefit.core.generated.model.TaxClaimCont;
import com.sinosoft.taxbenefit.core.generated.model.TaxClaimContExample;
import com.sinosoft.taxbenefit.core.generated.model.TaxContClaim;
import com.sinosoft.taxbenefit.core.generated.model.TaxContClaimExample;

public class ClaimServiceTest extends TaxBaseTest {
	
	@Autowired
	ClaimInfoCoreService ClaimInfoCoreService;
	
	@Autowired
	TaxClaimContMapper taxClaimContMapper;
	@Autowired
	TaxContClaimMapper taxContClaimMapper;
	public void prepareData(){
		
	}
	@Test
	@Rollback(false)
	public void claimService(){
	TaxContClaim record=new TaxContClaim();
		//record.setRcState("01");
		//example.createCriteria().andClaimNoEqualTo("0008843784738025");
//		List<TaxContClaim>list=taxContClaimMapper.selectByExample(example);
//		record.setSid(list.get(0).getSid());
//		record.setCaseState("02");
		//taxContClaimMapper.updateByExampleSelective(record,example);
		TaxClaimCont policy =new TaxClaimCont();
		policy.setRcState(ENUM_RC_STATE.INVALID.getCode());
		TaxClaimContExample example1=new TaxClaimContExample();
		example1.createCriteria().andClaimIdEqualTo(522);
		taxClaimContMapper.updateByExampleSelective(policy, example1);	
//		TaxContClaim record=new TaxContClaim();
//		record.setSid(522);
//		record.setRcState("D");
//		taxContClaimMapper.updateByPrimaryKey(record);
		System.out.println(123);
	}
}
