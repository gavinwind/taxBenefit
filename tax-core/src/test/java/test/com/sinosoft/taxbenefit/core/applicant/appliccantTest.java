package test.com.sinosoft.taxbenefit.core.applicant;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

import com.sinosoft.platform.base.TaxBaseTest;
import com.sinosoft.taxbenefit.api.dto.request.applicant.ReqAppBeneficiaryDTO;
import com.sinosoft.taxbenefit.api.dto.request.applicant.ReqAppCoverageDTO;
import com.sinosoft.taxbenefit.api.dto.request.applicant.ReqAppInsuredDTO;
import com.sinosoft.taxbenefit.api.dto.request.applicant.ReqAppLiabilityDTO;
import com.sinosoft.taxbenefit.api.dto.request.applicant.ReqAppPolicyDTO;
import com.sinosoft.taxbenefit.api.dto.request.applicant.ReqAppPremiumDTO;
import com.sinosoft.taxbenefit.api.dto.request.applicant.ReqAppSinglePolicyHolderDTO;
import com.sinosoft.taxbenefit.api.dto.request.applicant.ReqAppUnderwritingDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.core.applicant.biz.service.ApplicantCoreService;

public class appliccantTest  extends TaxBaseTest{
	@Autowired
	ApplicantCoreService policyTransferService;
	@Test
	@Rollback(false)
	public void abc(){
		// 保单
				ReqAppPolicyDTO policy = new ReqAppPolicyDTO();
				// 个人保单
				ReqAppSinglePolicyHolderDTO singlePolicyHolder = new ReqAppSinglePolicyHolderDTO();
//				// 团单
//				ReqAppGroupPolicyHolderDTO groupPolicyHolder = new ReqAppGroupPolicyHolderDTO();
				// 被保人
				List<ReqAppInsuredDTO> insuredList = new ArrayList<ReqAppInsuredDTO>();

				
				ReqAppInsuredDTO insured=new ReqAppInsuredDTO();
				
				// 受益人list
				
				 List<ReqAppBeneficiaryDTO> beneficiaryList=new ArrayList<ReqAppBeneficiaryDTO>();
				// 受益人
				 ReqAppBeneficiaryDTO beneficiary=new ReqAppBeneficiaryDTO();
				 // 险种list
				 List<ReqAppCoverageDTO> coverageList=new ArrayList<ReqAppCoverageDTO>();
				
				 ReqAppCoverageDTO coverage=new ReqAppCoverageDTO();
					//	核保
					 ReqAppUnderwritingDTO  underwriting=new ReqAppUnderwritingDTO();
//					缴费
					 ReqAppPremiumDTO premium=new ReqAppPremiumDTO() ;
//					责任
					 List<ReqAppLiabilityDTO>  liabilityList=new ArrayList<ReqAppLiabilityDTO>();
				
					 ReqAppLiabilityDTO liability=new ReqAppLiabilityDTO();
					 
					 liability.setLiabilityCode("libaCode");
					 liability.setLiabilityStatus("有效");
					 liability.setLiabilitySa(new BigDecimal("123"));
					 
					 liabilityList.add(liability);
					 
					 premium.setConfirmDate("2010-1-1");
					 premium.setFeeId("feeCode");
					 premium.setFeeStatus("有效");
					 premium.setPayDueDate("2014-1-1");
					 premium.setPayFromDate("2014-1-1");
					 premium.setPaymentFrequency("频率");
					 premium.setPayToDate("2014-1-1");
					 premium.setPremiumAmount(new BigDecimal("123"));
					 
					underwriting.setUnderwritingDate("2014-2-4");
					underwriting.setUnderwritingDecision("核保决定");
					underwriting.setUnderwritingDes("核保描述");
					coverage.setCoverageStatus("0");
					coverage.setCoverageType("1-1");
					coverage.setLiabilityList(liabilityList);
				//	coverage.setPremium(premium);
					coverage.setUnderwriting(underwriting);
//					coverage.setPremium(premium);
//					coverage.setUnderwriting(underwriting);
//					coverage.setLiability(liabilityList);
					coverageList.add(coverage);
					
					beneficiary.setBeneficiaryType("1");
					beneficiary.setBenInsuredRelation("1");
					beneficiary.setBirthday("2012-1-2");
					beneficiary.setCertiNo("423117199012041324");
					beneficiary.setCertiType("00");
					beneficiary.setGender("F");
					beneficiary.setName("lisi");
					beneficiary.setNationality("CHZ");
					beneficiary.setPriority(new BigDecimal("1"));
					beneficiary.setProportion(new BigDecimal("99"));
					beneficiaryList.add(beneficiary);
					
					insured.setSequenceNo("123123");
					insured.setCustomerNo("909");
					insured.setName("zhangsan");
					insured.setGender("F");
					insured.setBirthday("1992-1-2");
					insured.setCertiNo("423117199012041324");
					insured.setCertiType("00");
					insured.setNationality("ChZ");
					insured.setMobileNo("13103781223");
					insured.setResidencePlace("地区代码");
					insured.setHealthFlag("Y");
					insured.setSocialcareNo("社保卡号 ");
					insured.setJobCode("J70");
					insured.setInsuredType("被保险人类型 ");
					insured.setMainInsuredNo("1");
					insured.setPhInsuredRelation("9");
					insured.setPremium(premium);
					
					
					insured.setBeneficiaryList(beneficiaryList);
					insured.setCoverageList(coverageList);
					insuredList.add(insured);
					
					singlePolicyHolder.setCustomerNo("a");
					singlePolicyHolder.setName("zhangsan");
					singlePolicyHolder.setGender("F");
					singlePolicyHolder.setBirthday("2015-5-6");
					singlePolicyHolder.setCertiNo("423117199012041324");
					singlePolicyHolder.setCertiType("00");
					singlePolicyHolder.setNationality("ChZ");
					singlePolicyHolder.setTaxIndi("Y");
					singlePolicyHolder.setTaxPayerType("S");
					singlePolicyHolder.setWorkUnit("gyas");
					singlePolicyHolder.setOrganCode1("税务登记号");
					singlePolicyHolder.setOrganCode2("组织机构代码");
					singlePolicyHolder.setOrganCode3("营业证号");
					singlePolicyHolder.setOrganCode4("社会信用代码");
					singlePolicyHolder.setResidencePlace("联系地址");
					singlePolicyHolder.setMobileNo("13103781223");
					
					
					policy.setPolicyNo("43434");
					policy.setBizNo("00001");
					policy.setPolicyType("01");//01-个单
					policy.setPolicyOrg("二级机构名称");
					policy.setPolicyArea("保单所属区域--地区代码");
					policy.setSalesChannel("销售渠道");			
					policy.setSalesArea("销售地区");
					policy.setExpireDate("2901-1-1");
					policy.setOriginalOrganCode("转出公司code");
					policy.setOriginalPolicyNo("转出保单号");
					policy.setPolicySource("保单来源");
					policy.setPolicyStatus("1");
					policy.setEffectiveDate("2016-3-6");
					
					policy.setInsuredList(insuredList);
					policy.setSinglePolicyHolder(singlePolicyHolder);
		
		List<ReqAppPolicyDTO> policyList=new ArrayList<ReqAppPolicyDTO>();
		
		policyList.add(policy);
		
		
		
		
		ParamHeadDTO paramHead=new ParamHeadDTO();
		paramHead.setAreaCode("shanghai");
		paramHead.setRecordNum("1");
		paramHead.setSerialNo("A1100001");
		paramHead.setPortCode("END004");
		paramHead.setPortName("保单转移");
		
		policyTransferService.addApplicant(policyList, paramHead);
	}
}
