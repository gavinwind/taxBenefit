package test.com.sinosoft.taxbenefit.core.InsuredPay;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

import com.sinosoft.platform.base.TaxBaseTest;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqInsuredPayInfoDTO;
import com.sinosoft.taxbenefit.core.Insuredpay.biz.service.InsuredPayService;

public class InsuredPayServiceTest extends TaxBaseTest{

	@Autowired
	public InsuredPayService insuredPayService;
	
	public ReqInsuredPayInfoDTO insuredPayInfoDTO;
	
	@Before
	public void prepareData(){
		
		insuredPayInfoDTO = new ReqInsuredPayInfoDTO();
		insuredPayInfoDTO.setBizNo("A111001");
		insuredPayInfoDTO.setName("李思思");
		insuredPayInfoDTO.setGender("nv");
		insuredPayInfoDTO.setBirthday(null);
		insuredPayInfoDTO.setCertiType("学生证");
	
		insuredPayInfoDTO.setCertiNo("78967567");
		insuredPayInfoDTO.setProposalNo("7897576");
		insuredPayInfoDTO.setPolicySource("网销");
		
		
		
		
	}
	@Test
	@Rollback(false)
	public void AccountChangeServiceTest(){
		try {
			insuredPayService.addInsuredPayInfo(insuredPayInfoDTO, null);
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
	}

}
