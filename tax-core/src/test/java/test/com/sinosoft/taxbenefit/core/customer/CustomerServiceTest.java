package test.com.sinosoft.taxbenefit.core.customer;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

import com.sinosoft.platform.base.TaxBaseTest;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqCustomerInfoDTO;
import com.sinosoft.taxbenefit.core.customer.biz.service.CustomerServiceCore;
import com.sinosoft.taxbenefit.core.policyTransfer.config.ENUM_POLICYTRANS_TYPE;

public class CustomerServiceTest extends TaxBaseTest{
	
	@Autowired
	public CustomerServiceCore customerService;
	
	@Test
	@Rollback(false)
	public void UpdateCustomerServiceTest() {
		try {
			ReqCustomerInfoDTO customerDTO = new ReqCustomerInfoDTO();
			customerDTO.setBizNo("A111001");
			customerDTO.setName("张三");
			customerDTO.setGender(ENUM_POLICYTRANS_TYPE.POLICYTRANS_OUT.code());
			customerDTO.setBirthday("f");
			customerDTO.setCertiType("身份证");
			customerDTO.setCertiNo("13511112222");
			
			ParamHeadDTO paramHeadDTO=new ParamHeadDTO(); 
			paramHeadDTO.setAreaCode("shanghai");
			paramHeadDTO.setPortCode("PTY001");
			paramHeadDTO.setRecordNum("1");
			paramHeadDTO.setSerialNo("A1100003");
			
			paramHeadDTO.setPortName("客戶驗證上傳");
			customerService.addCustomer(customerDTO, paramHeadDTO);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
