package test.com.sinosoft.taxbenefit.core.continuePolicy;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

import com.sinosoft.platform.base.TaxBaseTest;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyCoverageInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyLiabilityInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyUwinfoInfoDTO;
import com.sinosoft.taxbenefit.core.continuePolicy.biz.service.ContinuePolicyCoreService;

/**
 * Title:续保信息上传测试类
 * @author yangdongkai@outlook.com
 * @date 2016年3月4日 下午2:11:05
 */
public class ContinuePolicyService extends TaxBaseTest{
	@Autowired
	ContinuePolicyCoreService policyServiceCore;
	
	List<ReqContinuePolicyInfoDTO> policyInfoList = new ArrayList<ReqContinuePolicyInfoDTO>();
	List<ReqContinuePolicyCoverageInfoDTO> policyCoverageInfoList = new ArrayList<ReqContinuePolicyCoverageInfoDTO>();
	List<ReqContinuePolicyUwinfoInfoDTO> policyUwinfoInfoList = new ArrayList<ReqContinuePolicyUwinfoInfoDTO>();
	List<ReqContinuePolicyLiabilityInfoDTO> policyLiabilityInfoList = new ArrayList<ReqContinuePolicyLiabilityInfoDTO>();
	
	@Before
	public void prepareData(){
		ReqContinuePolicyInfoDTO reqPolicyInfoDTO =new ReqContinuePolicyInfoDTO();
		ReqContinuePolicyCoverageInfoDTO reqPolicyCoverageInfoDTO = new ReqContinuePolicyCoverageInfoDTO();
		ReqContinuePolicyUwinfoInfoDTO reqPolicyUwinfoInfoDTO = new ReqContinuePolicyUwinfoInfoDTO();
		ReqContinuePolicyLiabilityInfoDTO reqPolicyLiabilityInfoDTO = new ReqContinuePolicyLiabilityInfoDTO();
		
		reqPolicyUwinfoInfoDTO.setUnderwritingDate("2016-01-01");
		reqPolicyUwinfoInfoDTO.setUnderwritingDecision("核保决定");
		reqPolicyUwinfoInfoDTO.setUnderwritingDes("核保描述");
		policyUwinfoInfoList.add(reqPolicyUwinfoInfoDTO);
		
		reqPolicyLiabilityInfoDTO.setLiabilityCode("责任代码");
		reqPolicyLiabilityInfoDTO.setLiabilitySa(new BigDecimal("0.098"));
		reqPolicyLiabilityInfoDTO.setLiabilityStatus("责任状态");
		policyLiabilityInfoList.add(reqPolicyLiabilityInfoDTO);
		
		reqPolicyCoverageInfoDTO.setComCoverageCode("123");
		reqPolicyCoverageInfoDTO.setCoverageEffectiveDate("2016-01-01");
		reqPolicyCoverageInfoDTO.setCoverageExpireDate("2016-01-01");
		reqPolicyCoverageInfoDTO.setCoveragePackageCode("123");
		reqPolicyCoverageInfoDTO.setPremium(new BigDecimal("0.098"));
		reqPolicyCoverageInfoDTO.setInsuranceCoverage(new BigDecimal("0.098"));
		reqPolicyCoverageInfoDTO.setLiabilityList(policyLiabilityInfoList);
//		reqPolicyCoverageInfoDTO.setUwinfoList(policyUwinfoInfoList);
		policyCoverageInfoList.add(reqPolicyCoverageInfoDTO);
		
		reqPolicyInfoDTO.setBizNo("123");
		reqPolicyInfoDTO.setPolicyNo("123");
		policyInfoList.add(reqPolicyInfoDTO);
	}
	
	@Test
	@Rollback(false)
	public void ContinuePolicyServiceTest(){
		try {
		//	policyServiceCore.policySettlement(policyInfoList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
