package test.com.sinosoft.taxbenefit.core.policytransfer;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

import com.sinosoft.platform.base.TaxBaseTest;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqPolicyTransClientDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqSOutPolicyTransferDTO;
//import com.sinosoft.taxbenefit.api.dto.request.ReqPolicyMoveLocationDTO;
import com.sinosoft.taxbenefit.core.policyTransfer.biz.service.PolicyTransferService;

public class TestPolicyOutTransferService extends TaxBaseTest{
	@Autowired
	PolicyTransferService policyTransferService;

	@Test
	@Rollback(false)
	public void policyTransSOutDemo(){
		List<ReqPolicyTransClientDTO> reqPolicyTransClientList = new ArrayList<ReqPolicyTransClientDTO>();
		ReqPolicyTransClientDTO reqPolicyTransClientDTO = new ReqPolicyTransClientDTO();
		reqPolicyTransClientDTO.setSequenceNo("A01");
		reqPolicyTransClientList.add(reqPolicyTransClientDTO);
		
//		List<ReqPolicyTransferOutDTO> reqPolicyTransferOutList = new ArrayList<ReqPolicyTransferOutDTO>();
		ReqSOutPolicyTransferDTO reqSOutPolicyTransfer = new ReqSOutPolicyTransferDTO();
		reqSOutPolicyTransfer.setBizNo("A001");
		reqSOutPolicyTransfer.setPolicyNo("1535");
		reqSOutPolicyTransfer.setClientList(reqPolicyTransClientList);
//		reqPolicyTransferOutList.add(reqPolicyTransferOutDTO);
		
		ParamHeadDTO paramHead=new ParamHeadDTO();
		paramHead.setAreaCode("shanghai");
		paramHead.setRecordNum("1");
		paramHead.setSerialNo("A1100001");
		paramHead.setPortCode("END004");
		paramHead.setPortName("保单转移");
		
		policyTransferService.addPolicyTransSOutSettlement(reqSOutPolicyTransfer, paramHead);
	}
	
}
