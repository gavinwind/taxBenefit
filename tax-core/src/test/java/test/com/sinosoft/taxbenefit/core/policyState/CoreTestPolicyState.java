package test.com.sinosoft.taxbenefit.core.policyState;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.platform.base.TaxBaseTest;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.policystate.PolciyStateCoverage;
import com.sinosoft.taxbenefit.api.dto.request.policystate.PolicyStateEndorsement;
import com.sinosoft.taxbenefit.api.dto.request.policystate.PolicyStateInsured;
import com.sinosoft.taxbenefit.api.dto.request.policystate.PolicyStateLiability;
import com.sinosoft.taxbenefit.core.policystatechange.biz.service.PolicyStateChangeService;
/**
 * 调用中保信
 * @author Administrator
 *
 */

public class CoreTestPolicyState extends TaxBaseTest {
	
	@Autowired
	PolicyStateChangeService  policyStateService;
	
	static String requestjson;
	
	@Test
	@Rollback(false)
	public void testpolicyStateChange(){
	List<PolicyStateEndorsement> endorsement=new ArrayList<PolicyStateEndorsement>();
	
	PolicyStateEndorsement policy=new PolicyStateEndorsement();
	
	PolicyStateInsured insured=new PolicyStateInsured();
	
	List<PolicyStateInsured> insuredList=new ArrayList<PolicyStateInsured>();
	List<PolciyStateCoverage> coverageList=new ArrayList<PolciyStateCoverage>();
	
	List<PolicyStateLiability> liabilityList =new ArrayList<PolicyStateLiability>();
	
	PolicyStateLiability liability=new PolicyStateLiability();
	policy.setBizNo("BUD0001");
	policy.setEndorsementNo("policy123123");
	policy.setFinishTime("2016-1-1");
	policy.setPolEndSeq("asdf");
	policy.setPolicyNo("policy123123");
	policy.setPolicyStatus("有效");
	policy.setPolicyStatusUpdateType("riskUpdate");
	policy.setSuspendDate("2015-1-1");
	policy.setTerminationDate("2016-1-2");
	policy.setTerminationReason("afdfasdf");
	
	insured.setSequenceNo("3453453");
	liability.setLiabilityCode("libaCode");
	liability.setLiabilityStatus("有效");
	liability.setLiabilityTermReason("afd");
	liabilityList.add(liability);
	PolciyStateCoverage coverage=new PolciyStateCoverage();
	
	coverage.setComCoverageCode("asdf");
	coverage.setComCoverageName("riskCode");
	coverage.setCoveragePackageCode("riskCode");
	coverage.setCoverageStatus("riskCode");
	coverage.setCoverageType("riskCode");
	coverage.setLiabilityList(liabilityList);
	coverage.setSuspendDate("2012-2-2");
	coverage.setTerminationDate("2012-2-2");
	coverage.setTerminationReason("asfasf");
	coverageList.add(coverage);
	insured.setCoverageList(coverageList);
	
	insuredList.add(insured);
//	Map<String,Object> map=new HashMap<String,Object>();
	
	
	
	policy.setInsuredList(insuredList);
	endorsement.add(policy);

//	PolicyEndorsementListBody endorsementList=new PolicyEndorsementListBody();
//	endorsementList.setEndorsementList(endorsement);
	
	requestjson=		JSONObject.toJSONString(endorsement);
	System.out.println(requestjson);
	
	
	ParamHeadDTO paramHead=new ParamHeadDTO();
	paramHead.setAreaCode("shanghai");
	paramHead.setRecordNum("1");
	paramHead.setSerialNo("A1100001");
	paramHead.setPortCode("END003");
	paramHead.setPortName("保单状态修改");
	
	policyStateService.modifyPolicyState(endorsement, paramHead);
	
	
	}
}