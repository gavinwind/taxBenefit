package test.com.sinosoft.taxbenefit.core.customer;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.sinosoft.platform.base.TaxBaseTest;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqTaxBeneVerifyInfoDTO;
import com.sinosoft.taxbenefit.core.taxbenvrify.biz.service.TaxBeneVerifyService;

public class testBeneVerify extends TaxBaseTest {
	@Autowired
	TaxBeneVerifyService taxBeneVerifyService;
	@Test
	public void TextTaxBeneverify(){
		
		ReqTaxBeneVerifyInfoDTO reqCustomerInfoDTO = new ReqTaxBeneVerifyInfoDTO();
		reqCustomerInfoDTO.setName("小米");
		reqCustomerInfoDTO.setGender("F");
		reqCustomerInfoDTO.setBirthday("1993-01-02");
		reqCustomerInfoDTO.setCertiNo("2343567658");
		reqCustomerInfoDTO.setCertiType("身份证");
		reqCustomerInfoDTO.setNationality("中国");
		reqCustomerInfoDTO.setProposalNo("6256784678");
		reqCustomerInfoDTO.setPolicySource("网销");
		reqCustomerInfoDTO.setCoverageEffectiveDate("2015-01-01");
		
		ParamHeadDTO paramHead=new ParamHeadDTO();
		paramHead.setAreaCode("shanghai");
		paramHead.setRecordNum("1");
		paramHead.setSerialNo("A1100001");
		paramHead.setPortCode("END004");
		paramHead.setPortName("保单转移");
		
		taxBeneVerifyService.addtaxBeneVerify(reqCustomerInfoDTO, paramHead);
	}
}
