package test.com.sinosoft.taxbenefit.core.transCode;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.sinosoft.platform.base.TaxBaseTest;
import com.sinosoft.taxbenefit.core.common.config.ENUUM_CODE_MAPPING;
import com.sinosoft.taxbenefit.core.transCode.biz.service.TransCodeService;

public class TestTransCode extends TaxBaseTest {
	@Autowired
	TransCodeService transCodeService;

	@Test
	public void testCodeService() {
		String str=transCodeService.transCode(ENUUM_CODE_MAPPING.CLAIMCONCLUSIONCODE.code(), "4");
		System.out.println(str);
	}
}
