package test.com.sinosoft.taxbenefit.core.premium;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import com.sinosoft.platform.base.TaxBaseTest;
import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqRenewalPremiumCoverageInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqRenewalPremiumInfoDTO;
import com.sinosoft.taxbenefit.core.renewalpremium.biz.service.PremiumCoreService;

/**
 * 
 * Title:续期测试类
 * @author yangdongkai@outlook.com
 * @date 2016年3月4日 下午2:10:26
 */
public class PremiumServiceTest extends TaxBaseTest{
	@Autowired
	PremiumCoreService premiumCoreService;
	
	com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqSPremiumInfoDTO ReqSPremiumInfoDTO = new com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqSPremiumInfoDTO();
	ReqRenewalPremiumInfoDTO reqRenewalPremiumInfoDTO = new ReqRenewalPremiumInfoDTO();
	List<ReqRenewalPremiumInfoDTO> reqRenewalPremiumInfoList = new ArrayList<ReqRenewalPremiumInfoDTO>();

	@Before
	public void prepareData(){
		ReqRenewalPremiumCoverageInfoDTO reqPremiumCoverageInfoDTO = new ReqRenewalPremiumCoverageInfoDTO();
		List<ReqRenewalPremiumCoverageInfoDTO> reqPremiumCoverageInfoList = new ArrayList<ReqRenewalPremiumCoverageInfoDTO>();
		
		reqPremiumCoverageInfoDTO.setComCoverageCode("A111");
		reqPremiumCoverageInfoDTO.setCoveragePackageCode("A111-1");
		reqPremiumCoverageInfoDTO.setCoveragePremium(new BigDecimal("0.098"));
		reqPremiumCoverageInfoDTO.setRiskPremium(new BigDecimal("0.098"));
		reqPremiumCoverageInfoList.add(reqPremiumCoverageInfoDTO);
		
		
		reqRenewalPremiumInfoDTO.setBizNo("111");
		reqRenewalPremiumInfoDTO.setPolicyNo("SH004680244");
		reqRenewalPremiumInfoDTO.setSequenceNo("123");
		reqRenewalPremiumInfoDTO.setConfirmDate("2016-01-01");
		reqRenewalPremiumInfoDTO.setFeeId("111");
		reqRenewalPremiumInfoDTO.setFeeStatus("222");
		reqRenewalPremiumInfoDTO.setPayDueDate("2016-01-01");
		reqRenewalPremiumInfoDTO.setPayFromDate("2016-01-01");
		reqRenewalPremiumInfoDTO.setPayToDate("2016-01-01");
		reqRenewalPremiumInfoDTO.setPaymentFrequency("123456");
		reqRenewalPremiumInfoDTO.setPremiumAmount(new BigDecimal("0.098"));
		reqRenewalPremiumInfoDTO.setFeeStatus("123456");
		reqRenewalPremiumInfoDTO.setCoverageList(reqPremiumCoverageInfoList);
		reqRenewalPremiumInfoList.add(reqRenewalPremiumInfoDTO);
		
		
		ReqSPremiumInfoDTO.setPremiumList(reqRenewalPremiumInfoList);
	}
	@Test
	public void PremiumTest(){
		try {
			ParamHeadDTO paramHeadDTO = new ParamHeadDTO();
			paramHeadDTO.setAreaCode("111");
			paramHeadDTO.setPortCode("PRM001");
			paramHeadDTO.setPortName("111");
			paramHeadDTO.setRecordNum("111");
			paramHeadDTO.setSerialNo("111");
			premiumCoreService.premiumSettlement(ReqSPremiumInfoDTO, paramHeadDTO);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
