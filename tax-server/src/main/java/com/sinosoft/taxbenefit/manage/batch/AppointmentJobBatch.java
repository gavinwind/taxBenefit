package com.sinosoft.taxbenefit.manage.batch;

import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSONObject;
import com.lowagie.text.DocumentException;
import com.sinosoft.platform.base.TaxBaseJob;
import com.sinosoft.platform.common.exception.RequestDateException;
import com.sinosoft.platform.common.util.StringUtil;
import com.sinosoft.taxbenefit.api.config.ENUM_BZ_RESPONSE_RESULT;
import com.sinosoft.taxbenefit.api.config.ENUM_DISPOSE_STATE;
import com.sinosoft.taxbenefit.api.config.ENUM_DISPOSE_TYPE;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.response.accountchange.AccountChangeResultDTO;
import com.sinosoft.taxbenefit.api.dto.response.applicant.ResultApplicantDTO;
import com.sinosoft.taxbenefit.api.dto.response.claim.ResultInfoDTO;
import com.sinosoft.taxbenefit.api.dto.response.continuepolicy.ResultContinuePolicyDTO;
import com.sinosoft.taxbenefit.api.dto.response.customer.CustomerVerifyResultDTO;
import com.sinosoft.taxbenefit.api.dto.response.policystate.ResultEndorsement;
import com.sinosoft.taxbenefit.api.dto.response.policytrans.ResHXPolicyTransferResultDTO;
import com.sinosoft.taxbenefit.api.dto.response.renewalpremium.ResultRenewalPremiumDTO;
import com.sinosoft.taxbenefit.core.AccountChange.biz.service.AccountChangeService;
import com.sinosoft.taxbenefit.core.applicant.biz.service.ApplicantCoreService;
import com.sinosoft.taxbenefit.core.claim.biz.service.ClaimInfoCoreService;
import com.sinosoft.taxbenefit.core.continuePolicy.biz.service.ContinuePolicyCoreService;
import com.sinosoft.taxbenefit.core.customer.biz.service.CustomerServiceCore;
import com.sinosoft.taxbenefit.core.faultTask.biz.service.FaultTaskService;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxFaultTaskMapper;
import com.sinosoft.taxbenefit.core.generated.model.TaxAsynInteraction;
import com.sinosoft.taxbenefit.core.generated.model.TaxFaultTask;
import com.sinosoft.taxbenefit.core.generated.model.TaxRequestReceiveLogWithBLOBs;
import com.sinosoft.taxbenefit.core.policyTransfer.biz.service.PolicyTransferService;
import com.sinosoft.taxbenefit.core.policystatechange.biz.service.PolicyStateChangeService;
import com.sinosoft.taxbenefit.core.renewalcancel.biz.service.RenewalCancelCoreService;
import com.sinosoft.taxbenefit.core.renewalpremium.biz.service.PremiumCoreService;
import com.sinosoft.taxbenefit.core.securityguard.biz.service.PreservationOfCancelService;
import com.sinosoft.taxbenefit.core.securityguard.biz.service.PreservationOfChangeService;
import com.sinosoft.taxbenefit.core.taxbenvrify.biz.service.TaxBeneVerifyService;
import com.sinosoft.taxbenefit.manage.exceptionJob.biz.service.ExceptionJobInfoService;
import com.sinosoft.taxbenefit.manage.exceptionJob.config.ENUM_SERVICE_PORT_CODE;

@Repository
public class AppointmentJobBatch extends TaxBaseJob {
	@Autowired
	private ExceptionJobInfoService exceptionJobService;//
	@Autowired
	AccountChangeService accountChangeService;// 账户变更
	@Autowired
	ApplicantCoreService applicantCoreservice;// 承保
	@Autowired
	ClaimInfoCoreService claimInfoCoreService;// 理赔
	@Autowired
	ContinuePolicyCoreService continuePolicyCoreService;// 续保信息信息上传
	@Autowired
	CustomerServiceCore customerServiceCore;// 客户验证
	@Autowired
	PolicyStateChangeService policyStateChangeService;// 保单状态修改
	@Autowired
	PolicyTransferService policyTransferService;// 保单转入转出
	@Autowired
	RenewalCancelCoreService renewalCancelCoreService;//续保撤销
	@Autowired
	PreservationOfChangeService preservationOfChangeService;// 保全
	@Autowired
	PreservationOfCancelService preservationOfCancelService;// 保全撤销
	@Autowired
	PremiumCoreService premiumCoreService;//续期
	@Autowired
	TaxBeneVerifyService taxBeneVerifyService;//税优验证
	@Autowired
	TaxFaultTaskMapper  taxFaultTaskMapper;//异常任务
	@Autowired
	FaultTaskService tFaultTaskService;
	
	public void executeJob() {
		logger.info("=======================预约码任务批处理开始=======================");
		try {
			// 查找所有预约码类型,并且处理结果为处理失败和待处理
			List<TaxFaultTask> taxFaultTaskList = exceptionJobService.queryAppointmentJobList();
			for (TaxFaultTask taxFaultTask : taxFaultTaskList) {
				this.executeAppointmentJob(taxFaultTask);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("预约码任务批处理错误：" + e.getMessage());
		}			
		logger.info("=======================预约码任务补处理结束=======================");
	}

	@SuppressWarnings("rawtypes")
	private void executeAppointmentJob(TaxFaultTask taxFaultTask) {
		String requestJson = "";
		if(ENUM_DISPOSE_TYPE.APPOINTMENT.code().equals(taxFaultTask.getDisposeType())){
			requestJson = taxFaultTask.getAsynContent();
		}else{
			TaxRequestReceiveLogWithBLOBs receiveLog = (TaxRequestReceiveLogWithBLOBs) exceptionJobService.queryRequestContentBySerialNo(taxFaultTask.getSerialNo());
			requestJson = receiveLog.getRequestContent();	
		}
		if(StringUtil.isBlank(requestJson)){
			throw new RequestDateException("预约码任务编号为"+taxFaultTask.getSid()+"未找到请求报文数据,请检查数据!");
		}
			// 保单转出登记预约码任务
		String portCode = taxFaultTask.getServerPortCode();
		if (ENUM_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_RECORD_ONE.code().equals(portCode) 
				|| ENUM_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_RECORD_MORE.code().equals(portCode)) {
			TaxFaultTask faultTask = policyTransferService.policyTransOutRegisterExceptionJob(requestJson, portCode, taxFaultTask.getSid());
			// 如果请求的是多笔,正常请求外，还需要将返回的结果数据进行拆分,拆分到各自的子任务的返回报文中
			if(ENUM_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_RECORD_MORE.code().equals(portCode)){
				String resMainJson = faultTask.getSuccessMessage();
				// 需要更新任务表状态
				String mainSreialNo = faultTask.getSerialNo();
				// 如果中保信调用成功，则进行拆分返回数据
				if((ENUM_BZ_RESPONSE_RESULT.SUCCESS.code().equals(faultTask.getDisposeResult()))){
					ThirdSendResponseDTO thirdSendResponse = JSONObject.parseObject(resMainJson, ThirdSendResponseDTO.class);
					List thirdSendResList = JSONObject.parseObject(thirdSendResponse.getResJson().toString(), List.class);
					for (Object object : thirdSendResList) {
						ResHXPolicyTransferResultDTO resHXPolicyTransferResult = JSONObject.parseObject(object.toString(), ResHXPolicyTransferResultDTO.class);
						List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
						for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
							// 以业务号作为关联数据
							if(taxAsynInteraction.getBusinessNo().equals(resHXPolicyTransferResult.getBizNo())){
								ThirdSendResponseDTO thirdSendResponseDTO = new ThirdSendResponseDTO();
								thirdSendResponseDTO.setResJson(resHXPolicyTransferResult);
								thirdSendResponseDTO.setResultCode(thirdSendResponse.getResultCode());
								thirdSendResponseDTO.setResultDesc("处理成功");
								String resJson = JSONObject.toJSONString(thirdSendResponseDTO);
								tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resJson, ENUM_DISPOSE_STATE.SUCCES.code());
								break;
							}
						}
					}
				}else{
					List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
					for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
						tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resMainJson, ENUM_DISPOSE_STATE.SUCCES.code());
					}
				}
			}
			//保单状态修改上传预约码任务
		}else if(ENUM_SERVICE_PORT_CODE.POLICY_STATE_CHANGE_ONE.code().equals(portCode)
				|| ENUM_SERVICE_PORT_CODE.POLICY_STATE_CHANGE_MORE.code().equals(portCode)){
			TaxFaultTask faultTask = policyStateChangeService.modifyPolicyStateExceptionJob(requestJson, portCode, taxFaultTask.getSid());
			// 如果请求的是多笔,正常请求外，还需要将返回的结果数据进行拆分,拆分到各自的子任务的返回报文中
			if(ENUM_SERVICE_PORT_CODE.POLICY_STATE_CHANGE_MORE.code().equals(portCode)){
				// 获取中保信的返回数据
				String resMainJson = faultTask.getSuccessMessage();
				// 需要更新任务表状态
				String mainSreialNo = faultTask.getSerialNo();
				// 如果中保信调用成功，则进行拆分返回数据
				if((ENUM_BZ_RESPONSE_RESULT.SUCCESS.code().equals(faultTask.getDisposeResult()))){
					ThirdSendResponseDTO thirdSendResponse = JSONObject.parseObject(resMainJson, ThirdSendResponseDTO.class);
					List thirdSendResList = JSONObject.parseObject(thirdSendResponse.getResJson().toString(), List.class);
					for (Object object : thirdSendResList) {
						ResultEndorsement resultEndorsement = JSONObject.parseObject(object.toString(), ResultEndorsement.class);
						List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
						for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
							// 以业务号作为关联数据
							if(taxAsynInteraction.getBusinessNo().equals(resultEndorsement.getBizNo())){
								ThirdSendResponseDTO thirdSendResponseDTO = new ThirdSendResponseDTO();
								thirdSendResponseDTO.setResJson(resultEndorsement);
								thirdSendResponseDTO.setResultCode(thirdSendResponse.getResultCode());
								thirdSendResponseDTO.setResultDesc("处理成功");
								String resJson = JSONObject.toJSONString(thirdSendResponseDTO);
								tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resJson, ENUM_DISPOSE_STATE.SUCCES.code());
								break;
							}
						}
					}
				}else{
					List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
					for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
						tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resMainJson, ENUM_DISPOSE_STATE.SUCCES.code());
					}
				}
			}
			//承保信息上传预约码任务
		}else if(ENUM_SERVICE_PORT_CODE.APPLICANT_ONE.code().equals(portCode) 
				|| 	ENUM_SERVICE_PORT_CODE.APPLICANT_MORE.code().equals(portCode)){
			TaxFaultTask faultTask = applicantCoreservice.applicantExceptionJob(requestJson, portCode, taxFaultTask.getSid());
			if(ENUM_SERVICE_PORT_CODE.APPLICANT_MORE.code().equals(portCode)){
				String resMainJson = faultTask.getSuccessMessage();
				// 需要更新任务表状态
				String mainSreialNo = faultTask.getSerialNo();
				// 如果中保信调用成功，则进行拆分返回数据
				if((ENUM_BZ_RESPONSE_RESULT.SUCCESS.code().equals(faultTask.getDisposeResult()))){
					ThirdSendResponseDTO thirdSendResponse = JSONObject.parseObject(resMainJson, ThirdSendResponseDTO.class);
					List thirdSendResList = JSONObject.parseObject(thirdSendResponse.getResJson().toString(), List.class);
					for (Object object : thirdSendResList) {
						ResultApplicantDTO resultApplicantDTO = JSONObject.parseObject(object.toString(), ResultApplicantDTO.class);
						List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
						for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
							// 以业务号作为关联数据
							if(taxAsynInteraction.getBusinessNo().equals(resultApplicantDTO.getBizNo())){
								ThirdSendResponseDTO thirdSendResponseDTO = new ThirdSendResponseDTO();
								thirdSendResponseDTO.setResJson(resultApplicantDTO);
								thirdSendResponseDTO.setResultCode(thirdSendResponse.getResultCode());
								thirdSendResponseDTO.setResultDesc("处理成功");
								String resJson = JSONObject.toJSONString(thirdSendResponseDTO);
								tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resJson, ENUM_DISPOSE_STATE.SUCCES.code());
								break;
							}
						}
					}
				}else{
					List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
					for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
						tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resMainJson, ENUM_DISPOSE_STATE.SUCCES.code());
					}
				}
			}
			//理赔上传预约码任务
		}else if(ENUM_SERVICE_PORT_CODE.CLAIM_SETTLEMEN_ONE.code().equals(portCode)
				||	ENUM_SERVICE_PORT_CODE.CLAIM_SETTLEMEN_MORE.code().equals(portCode)){
			TaxFaultTask faultTask = claimInfoCoreService.addClaimSettlementExceptionJob(requestJson, portCode, taxFaultTask.getSid());
			if(ENUM_SERVICE_PORT_CODE.CLAIM_SETTLEMEN_MORE.code().equals(portCode)){
				String resMainJson = faultTask.getSuccessMessage();
				// 需要更新任务表状态
				String mainSreialNo = faultTask.getSerialNo();
				// 如果中保信调用成功，则进行拆分返回数据
				if((ENUM_BZ_RESPONSE_RESULT.SUCCESS.code().equals(faultTask.getDisposeResult()))){
					ThirdSendResponseDTO thirdSendResponse = JSONObject.parseObject(resMainJson, ThirdSendResponseDTO.class);
					List thirdSendResList = JSONObject.parseObject(thirdSendResponse.getResJson().toString(), List.class);
					for (Object object : thirdSendResList) {
						ResultInfoDTO resultInfoDTO = JSONObject.parseObject(object.toString(), ResultInfoDTO.class);
						List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
						for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
							// 以业务号作为关联数据
							if(taxAsynInteraction.getBusinessNo().equals(resultInfoDTO.getBizNo())){
								ThirdSendResponseDTO thirdSendResponseDTO = new ThirdSendResponseDTO();
								thirdSendResponseDTO.setResJson(resultInfoDTO);
								thirdSendResponseDTO.setResultCode(thirdSendResponse.getResultCode());
								thirdSendResponseDTO.setResultDesc("处理成功");
								String resJson = JSONObject.toJSONString(thirdSendResponseDTO);
								tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resJson, ENUM_DISPOSE_STATE.SUCCES.code());
								break;
							}
						}
					}
				}else{
					List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
					for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
						tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resMainJson, ENUM_DISPOSE_STATE.SUCCES.code());
					}
				}
			}
			//客户验证上传预约码任务
		}else if(ENUM_SERVICE_PORT_CODE.CUSTOMER_VERIFY_ONE.code().equals(portCode)
			||	ENUM_SERVICE_PORT_CODE.CUSTOMER_VERIFY_MORE.code().equals(portCode)){
			TaxFaultTask faultTask = customerServiceCore.customerExceptionJob(requestJson, portCode, taxFaultTask.getSid());
			if(ENUM_SERVICE_PORT_CODE.CUSTOMER_VERIFY_MORE.code().equals(portCode)){
				String resMainJson = faultTask.getSuccessMessage();
				// 需要更新任务表状态
				String mainSreialNo = faultTask.getSerialNo();
				// 如果中保信调用成功，则进行拆分返回数据
				if((ENUM_BZ_RESPONSE_RESULT.SUCCESS.code().equals(faultTask.getDisposeResult()))){
					ThirdSendResponseDTO thirdSendResponse = JSONObject.parseObject(resMainJson, ThirdSendResponseDTO.class);
					List thirdSendResList = JSONObject.parseObject(thirdSendResponse.getResJson().toString(), List.class);
					for (Object object : thirdSendResList) {
						CustomerVerifyResultDTO customerVerifyResult = JSONObject.parseObject(object.toString(), CustomerVerifyResultDTO.class);
						List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
						for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
							// 以业务号作为关联数据
							if(taxAsynInteraction.getBusinessNo().equals(customerVerifyResult.getBizNo())){
								ThirdSendResponseDTO thirdSendResponseDTO = new ThirdSendResponseDTO();
								thirdSendResponseDTO.setResJson(customerVerifyResult);
								thirdSendResponseDTO.setResultCode(thirdSendResponse.getResultCode());
								thirdSendResponseDTO.setResultDesc("处理成功");
								String resJson = JSONObject.toJSONString(thirdSendResponseDTO);
								tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resJson, ENUM_DISPOSE_STATE.SUCCES.code());
								break;
							}
						}
					}
				}else{
					List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
					for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
						tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resMainJson, ENUM_DISPOSE_STATE.SUCCES.code());
					}
				}
			}
			//续期保费信息上传预约码任务
		}else if(ENUM_SERVICE_PORT_CODE.POLICY_PREMIUM_UPLOAD_ONE.code().equals(portCode)
			|| ENUM_SERVICE_PORT_CODE.POLICY_PREMIUM_UPLOAD_MORE.code().equals(portCode)){
			TaxFaultTask faultTask = premiumCoreService.premiumExceptionJob(requestJson, portCode, taxFaultTask.getSid());
			if(ENUM_SERVICE_PORT_CODE.POLICY_PREMIUM_UPLOAD_MORE.code().equals(portCode)){
				String resMainJson = faultTask.getSuccessMessage();
				// 需要更新任务表状态
				String mainSreialNo = faultTask.getSerialNo();
				// 如果中保信调用成功，则进行拆分返回数据
				if((ENUM_BZ_RESPONSE_RESULT.SUCCESS.code().equals(faultTask.getDisposeResult()))){
					ThirdSendResponseDTO thirdSendResponse = JSONObject.parseObject(resMainJson, ThirdSendResponseDTO.class);
					List thirdSendResList = JSONObject.parseObject(thirdSendResponse.getResJson().toString(), List.class);
					for (Object object : thirdSendResList) {
						ResultRenewalPremiumDTO resultRenewalPremiumDTO = JSONObject.parseObject(object.toString(), ResultRenewalPremiumDTO.class);
						List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
						for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
							// 以业务号作为关联数据
							if(taxAsynInteraction.getBusinessNo().equals(resultRenewalPremiumDTO.getBizNo())){
								ThirdSendResponseDTO thirdSendResponseDTO = new ThirdSendResponseDTO();
								thirdSendResponseDTO.setResJson(resultRenewalPremiumDTO);
								thirdSendResponseDTO.setResultCode(thirdSendResponse.getResultCode());
								thirdSendResponseDTO.setResultDesc("处理成功");
								String resJson = JSONObject.toJSONString(thirdSendResponseDTO);
								tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resJson, ENUM_DISPOSE_STATE.SUCCES.code());
								break;
							}
						}
					}
				}else{
					List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
					for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
						tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resMainJson, ENUM_DISPOSE_STATE.SUCCES.code());
					}
				}
			}
			//续保信息上传预约码任务
		}else if(ENUM_SERVICE_PORT_CODE.POLICY_RENEWAL_UPLOAD_ONE.code().equals(portCode)
			|| 	ENUM_SERVICE_PORT_CODE.POLICY_RENEWAL_UPLOAD_MORE.code().equals(portCode)){
			TaxFaultTask faultTask = continuePolicyCoreService.continuePolicyExceptionJob(requestJson, portCode, taxFaultTask.getSid());
			if(ENUM_SERVICE_PORT_CODE.POLICY_RENEWAL_UPLOAD_MORE.code().equals(portCode)){
				String resMainJson = faultTask.getSuccessMessage();
				// 需要更新任务表状态
				String mainSreialNo = faultTask.getSerialNo();
				// 如果中保信调用成功，则进行拆分返回数据
				if((ENUM_BZ_RESPONSE_RESULT.SUCCESS.code().equals(faultTask.getDisposeResult()))){
					ThirdSendResponseDTO thirdSendResponse = JSONObject.parseObject(resMainJson, ThirdSendResponseDTO.class);
					List thirdSendResList = JSONObject.parseObject(thirdSendResponse.getResJson().toString(), List.class);
					for (Object object : thirdSendResList) {
						ResultContinuePolicyDTO resultContinuePolicy = JSONObject.parseObject(object.toString(), ResultContinuePolicyDTO.class);
						List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
						for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
							// 以业务号作为关联数据
							if(taxAsynInteraction.getBusinessNo().equals(resultContinuePolicy.getBizNo())){
								ThirdSendResponseDTO thirdSendResponseDTO = new ThirdSendResponseDTO();
								thirdSendResponseDTO.setResJson(resultContinuePolicy);
								thirdSendResponseDTO.setResultCode(thirdSendResponse.getResultCode());
								thirdSendResponseDTO.setResultDesc("处理成功");
								String resJson = JSONObject.toJSONString(thirdSendResponseDTO);
								tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resJson, ENUM_DISPOSE_STATE.SUCCES.code());
								break;
							}
						}
					}
				}else{
					List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
					for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
						tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resMainJson, ENUM_DISPOSE_STATE.SUCCES.code());
					}
				}
			}
			//账户变更上传预约码任务
		}else if(ENUM_SERVICE_PORT_CODE.ACCONUT_CHANGE_ONE.code().equals(portCode)
			|| 	ENUM_SERVICE_PORT_CODE.ACCONUT_CHANGE_MORE.code().equals(portCode)){
			TaxFaultTask faultTask = accountChangeService.accountChangeExceptionJob(requestJson, portCode, taxFaultTask.getSid());
			if(ENUM_SERVICE_PORT_CODE.ACCONUT_CHANGE_MORE.code().equals(portCode)){
				String resMainJson = faultTask.getSuccessMessage();
				// 需要更新任务表状态
				String mainSreialNo = faultTask.getSerialNo();
				// 如果中保信调用成功，则进行拆分返回数据
				if((ENUM_BZ_RESPONSE_RESULT.SUCCESS.code().equals(faultTask.getDisposeResult()))){
					ThirdSendResponseDTO thirdSendResponse = JSONObject.parseObject(resMainJson, ThirdSendResponseDTO.class);
					List thirdSendResList = JSONObject.parseObject(thirdSendResponse.getResJson().toString(), List.class);
					for (Object object : thirdSendResList) {
						AccountChangeResultDTO accountChangeResultDTO = JSONObject.parseObject(object.toString(), AccountChangeResultDTO.class);
						List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
						for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
							// 以业务号作为关联数据
							if(taxAsynInteraction.getBusinessNo().equals(accountChangeResultDTO.getBizNo())){
								ThirdSendResponseDTO thirdSendResponseDTO = new ThirdSendResponseDTO();
								thirdSendResponseDTO.setResJson(accountChangeResultDTO);
								thirdSendResponseDTO.setResultCode(thirdSendResponse.getResultCode());
								thirdSendResponseDTO.setResultDesc("处理成功");
								String resJson = JSONObject.toJSONString(thirdSendResponseDTO);
								tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resJson, ENUM_DISPOSE_STATE.SUCCES.code());
								break;
							}
						}
					}
				}else{
					List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
					for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
						tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resMainJson, ENUM_DISPOSE_STATE.SUCCES.code());
					}
				}
			}
			// 保单外部转入申请上传预异常任务
		}else if (ENUM_SERVICE_PORT_CODE.POLICY_IN_TRANSFER_UPLOAD_ONE.code().equals(portCode)) {
			policyTransferService.policyTransInExceptionJob(requestJson, portCode, taxFaultTask);
			//保全信息上传异常任务
		}else if(ENUM_SERVICE_PORT_CODE.SERCURITY_CHANGE.code().equals(portCode)){
			preservationOfChangeService.modifySecurityExceptionJob(requestJson, portCode, taxFaultTask);
			//保全撤销异常任务
		}else if(ENUM_SERVICE_PORT_CODE.SERCURITY_CANCEL.code().equals(portCode)){
			preservationOfCancelService.modifyCancelExceptionJob(requestJson, portCode, taxFaultTask);
			//承保撤销异常任务
		}else if(ENUM_SERVICE_PORT_CODE.POLICY_REVERSAL.code().equals(portCode)){
			applicantCoreservice.policyReversalExceptionJob(requestJson, portCode, taxFaultTask);
			//理赔撤销上传异常任务
		}else if(ENUM_SERVICE_PORT_CODE.CLAIM_REVERSAL.code().equals(portCode)){
			claimInfoCoreService.modifyClaimStateExceptionJob(requestJson, portCode, taxFaultTask);
			//续保撤销异常任务
		}else if(ENUM_SERVICE_PORT_CODE.POLICY_RENEWAL_CANCEL.code().equals(portCode)){
			renewalCancelCoreService.renewalCancelExceptionJob(requestJson, portCode, taxFaultTask);
			//税优验证上传异常任务
		}else if(ENUM_SERVICE_PORT_CODE.TAX_BENE_VERIFY.code().equals(portCode)){
			taxBeneVerifyService.taxBeneVerifyExceptionJob(requestJson, portCode, taxFaultTask);
		}		
	}
}
