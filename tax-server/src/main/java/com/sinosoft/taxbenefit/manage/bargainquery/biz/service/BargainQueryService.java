package com.sinosoft.taxbenefit.manage.bargainquery.biz.service;

import java.util.List;

import com.sinosoft.taxbenefit.api.dto.request.base.ParamHeadDTO;
import com.sinosoft.taxbenefit.core.generated.model.TaxBusinessCollateDetail;
import com.sinosoft.taxbenefit.core.generated.model.TaxRequestBusinessCollate;
import com.sinosoft.taxbenefit.core.generated.model.TaxRequestBusinessDetail;
import com.sinosoft.taxbenefit.manage.bargainquery.dto.BusinessCollateDetailDTO;
import com.sinosoft.taxbenefit.manage.bargainquery.dto.BusinessDetailDTO;
import com.sinosoft.taxbenefit.manage.bargainquery.dto.ReqBargainQueryDTO;
import com.sinosoft.taxbenefit.manage.bargainquery.dto.RequestBusinessCollateDTO;
import com.sinosoft.taxbenefit.manage.bargainquery.dto.BargainQueryDTO;

public interface BargainQueryService {
	/**
	 * 交易核对信息查询
	 * @param reqBargainQueryDTO
	 */
	void BargainQuery(ReqBargainQueryDTO reqBargainQueryDTO, Integer bid);
	/**
	 * 查询指定日期的交易记录数
	 * @param dataStr
	 * @return RequestBusinessCollateDTO
	 */
	RequestBusinessCollateDTO queryRequestBusinessCollateDTOByDay(String dataStr);
	/**
	 * 封装报文头
	 * @return ParamHeadDTO
	 */
	ParamHeadDTO initParamHeadDTO();
	/**
	 * 查询指定日期的交易记录数
	 * @param taxRequestBusinessCollatePage
	 * @return
	 */
	List<TaxRequestBusinessCollate> queryByList(BargainQueryDTO page);
	/**
	 * 
	 * @param businessCollateDetailDTO
	 * @return
	 */
	List<TaxBusinessCollateDetail> queryByList(BusinessCollateDetailDTO businessCollateDetailDTO);
	
	/**
	 * 根据主键查询
	 * @param sid
	 * @return
	 */
	TaxRequestBusinessCollate queryBySid(Integer sid);
	/**
	 * 根据交易时间查询当天请求记录明细
	 * @param date
	 * @return
	 */
	List<TaxRequestBusinessDetail> queryByBusinessDate(BusinessDetailDTO businessDetailDTO);
}
