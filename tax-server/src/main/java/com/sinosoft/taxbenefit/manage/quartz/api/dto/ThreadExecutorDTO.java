package com.sinosoft.taxbenefit.manage.quartz.api.dto;


public class ThreadExecutorDTO {

	/**
	 * 当前行任务
	 */
	private Object task;
	
	/**
	 * 执行任务的bean名称
	 */
	private String beanName;
	
	/**
	 * bean的method
	 */
	private String method;

	public Object getTask() {
		return task;
	}

	public void setTask(Object task) {
		this.task = task;
	}

	public String getBeanName() {
		return beanName;
	}

	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

}
