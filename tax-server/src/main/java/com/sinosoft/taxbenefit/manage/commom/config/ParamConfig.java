package com.sinosoft.taxbenefit.manage.commom.config;

public class ParamConfig {

	/** 整合数据个数*/
	private String callNum;

	public String getCallNum() {
		return callNum;
	}

	public void setCallNum(String callNum) {
		this.callNum = callNum;
	}
	
}
