package com.sinosoft.taxbenefit.manage.bargainquery.mapper;

import java.util.List;

import com.sinosoft.taxbenefit.core.generated.model.TaxRequestBusinessDetail;
import com.sinosoft.taxbenefit.manage.bargainquery.dto.BusinessDetailDTO;

public interface BusinessDetailMapper {
	/**
	 * 根据交易时间获取请求记录明细
	 * @param businessDate
	 * @return
	 */
	List<TaxRequestBusinessDetail> queryByList(BusinessDetailDTO businessDetailDTO);
	/**
	 * 获取总记录数
	 * @param businessDate
	 * @return
	 */
	int queryByCount(String businessDate);
}