/**
 * @Copyright ®2015 sinosoft Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.ewealth.general.web.filter<br/>
 * @FileName: LoginPermissionFilter.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.taxbenefit.manage.main.web.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.filter.OncePerRequestFilter;

import com.sinosoft.taxbenefit.manage.main.config.LoginConstants;
import com.sinosoft.taxbenefit.manage.main.dto.WebLoginDTO;

/**
 * 登陆拦截
 * @author SheChunMing
 */
public class LoginPermissionFilter extends OncePerRequestFilter{
	protected final Logger logger = Logger.getLogger(getClass());
	private String loginView = "/manage/index.action?action=login";

	@Override
	protected void doFilterInternal(HttpServletRequest request,
			HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException {
		String request_url=request.getServletPath();
		if(request_url.indexOf("action=login") >= 0){
	   		chain.doFilter(request, response);
	   		return ;
	   	}
		if(!isLogin(request)){ 
			logger.info("尚未登录，跳转至登录页面！");
			request.getRequestDispatcher(loginView).forward(request, response);
            return;
        } 
        chain.doFilter(request, response);
    }
	

	/** 
	 * 验证是否登录
	 * @Title: isLogin 
	 * @Description: 验证是否登录
	 * @param request
	 * @return
	*/ 
	private boolean isLogin(HttpServletRequest request) {
		WebLoginDTO loginDTO = (WebLoginDTO) request.getSession().getAttribute(LoginConstants.LOGIN_INFO);
		if(loginDTO == null ){
			return false;
		}
		return true;
	}
}


