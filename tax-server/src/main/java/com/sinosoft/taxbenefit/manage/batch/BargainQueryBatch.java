package com.sinosoft.taxbenefit.manage.batch;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Calendar;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.lowagie.text.DocumentException;
import com.sinosoft.platform.base.TaxBaseJob;
import com.sinosoft.platform.common.util.DateUtil;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxRequestBusinessCollateMapper;
import com.sinosoft.taxbenefit.manage.bargainquery.biz.service.BargainQueryService;
import com.sinosoft.taxbenefit.manage.bargainquery.dto.ReqBargainQueryDTO;
import com.sinosoft.taxbenefit.manage.bargainquery.dto.RequestBusinessCollateDTO;
/**
 * 交易核对信息查询批处理
 * @author yangdongkai@outlook.com
 * @date 2016年3月31日 下午4:10:08
 */
@Repository
public class BargainQueryBatch extends TaxBaseJob {
	@Autowired
	BargainQueryService bargainQueryService;
	@Autowired
	TaxRequestBusinessCollateMapper taxRequestBusinessCollateMapper;
	
	@Override
	public void executeJob() throws IOException, DocumentException,MessagingException {
		logger.info("=======================交易核对信息查询批处理开始=======================");
		try {
			//获得当前日期的前一天日期 格式为"yyyy-MM-dd"
			Date nowDate = java.sql.Date.valueOf(DateUtil.getCurrentDate("yyyy-MM-dd"));
			Calendar calendar =  DateUtil.getCalendar(nowDate);    
			calendar.add(Calendar.DATE, -1);
			String dateStr = DateUtil.getDateStr(calendar.getTime(), "yyyy-MM-dd");
			RequestBusinessCollateDTO businessCollateDTO = bargainQueryService.queryRequestBusinessCollateDTOByDay(dateStr);
			//封装请求中保信DTO
			if(null != businessCollateDTO){
				ReqBargainQueryDTO reqBargainQueryDTO = new ReqBargainQueryDTO();
				Integer bid = businessCollateDTO.getBusid();
				reqBargainQueryDTO.setCheckDate(DateUtil.getDateStr(calendar.getTime(), "yyyy-MM-dd"));
				reqBargainQueryDTO.setComFailNum(new BigDecimal(businessCollateDTO.getReqFailNum()));
				reqBargainQueryDTO.setComSuccessNum(new BigDecimal(businessCollateDTO.getReqSuccessNum()));
				reqBargainQueryDTO.setRequestTotalNum(new BigDecimal(businessCollateDTO.getRequetNum()));
				bargainQueryService.BargainQuery(reqBargainQueryDTO, bid);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("交易核对信息查询批处理失败"+e.getMessage());
		}
		logger.info("=======================交易核对信息查询批处理结束=======================");
	}
}