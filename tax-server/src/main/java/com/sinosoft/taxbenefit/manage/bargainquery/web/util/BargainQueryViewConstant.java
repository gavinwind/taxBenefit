package com.sinosoft.taxbenefit.manage.bargainquery.web.util;

public class BargainQueryViewConstant {
	//交易核对查询页面
	public final static String BARGAIN_QUERY_VIEW = "page/bargainquery/taxRequestBusinessCollate";
	//交易核对详情页面
	public final static String DETAIL_QUERY_VIEW = "page/bargainquery/taxRequestCollateDetail";
	//请求记录明细页面
	public final static String BUSINESS_DETAIL_VIEW = "page/bargainquery/taxRequestDetail";

}
