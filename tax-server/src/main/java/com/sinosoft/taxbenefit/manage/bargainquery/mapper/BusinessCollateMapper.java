package com.sinosoft.taxbenefit.manage.bargainquery.mapper;

import java.util.List;

import com.sinosoft.taxbenefit.core.generated.model.TaxRequestBusinessCollate;
import com.sinosoft.taxbenefit.manage.bargainquery.dto.RequestBusinessCollateDTO;
import com.sinosoft.taxbenefit.manage.bargainquery.dto.BargainQueryDTO;

public interface BusinessCollateMapper {
	/**
	 * 查询指定日期的交易记录数
	 * @param dateStr
	 * @return
	 */
	RequestBusinessCollateDTO selectBusinessCollateByDate(String dateStr);
	/**
	 * 分页
	 * @param taxRequestBusinessCollatePage
	 * @return
	 */
	List<TaxRequestBusinessCollate> queryByList(BargainQueryDTO bargainQueryDTO);
	/**
	 * 查询记录数
	 * @param page
	 * @return
	 */
	Integer queryByCount(BargainQueryDTO page);
}