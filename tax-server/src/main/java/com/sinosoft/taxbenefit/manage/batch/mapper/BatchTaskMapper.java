package com.sinosoft.taxbenefit.manage.batch.mapper;

import java.util.List;

import com.sinosoft.taxbenefit.manage.base.page.BasePage;
import com.sinosoft.taxbenefit.manage.batch.dto.BatchTaskDTO;

public interface BatchTaskMapper {
	
	/**
	 * 查询总记录数
	 * @return
	 */
	Integer queryBatchTaskCount();
	
	/**
	 * 查询列表
	 * @return
	 */
	List<BatchTaskDTO> queryBatchTaskList(BasePage page);
}
