package com.sinosoft.taxbenefit.manage.quartz.api.service;

import java.util.List;

import com.sinosoft.taxbenefit.core.generated.model.TaxBatchTask;

/**
 * 批处理调度service
 * @author zhangzh
 *
 */
public interface QuartzService {

	/**
	 * 初始化批处理，从配置中加载批处理项
	 * @param type 标识调度类型,INIT:初始化调度,RE:重新调度
	 * @throws Exception
	 */
	public void initScheduler() throws Exception;
	
	public void reScheduler() throws Exception;
	
	/**
	 * 取批处理任务
	 * @return
	 */
	public List<TaxBatchTask> getBatchInfoList(TaxBatchTask batch) throws Exception;;
	
}
