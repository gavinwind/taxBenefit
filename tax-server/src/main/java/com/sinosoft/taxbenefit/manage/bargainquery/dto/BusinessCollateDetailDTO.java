package com.sinosoft.taxbenefit.manage.bargainquery.dto;

import com.sinosoft.taxbenefit.manage.base.page.BasePage;
/**
 * 交易核对详情查询DTO
 * @author yangdongkai@outlook.com
 * @date 2016年4月8日 下午2:31:54
 */
public class BusinessCollateDetailDTO extends BasePage{
	//交易编号
	private Integer busId;
	/**
	 * @return the busId
	 */
	public Integer getBusId() {
		return busId;
	}
	/**
	 * @param busId the busId to set
	 */
	public void setBusId(Integer busId) {
		this.busId = busId;
	}
}
