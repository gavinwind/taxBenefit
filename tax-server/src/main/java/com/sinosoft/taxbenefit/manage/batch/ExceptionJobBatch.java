package com.sinosoft.taxbenefit.manage.batch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSONObject;
import com.sinosoft.platform.base.TaxBaseJob;
import com.sinosoft.platform.common.exception.RequestDateException;
import com.sinosoft.platform.common.util.StringUtil;
import com.sinosoft.taxbenefit.api.config.ENUM_BZ_RESPONSE_RESULT;
import com.sinosoft.taxbenefit.api.config.ENUM_DISPOSE_STATE;
import com.sinosoft.taxbenefit.api.config.ENUM_DISPOSE_TYPE;
import com.sinosoft.taxbenefit.api.dto.request.TaxFaulTaskDTO;
import com.sinosoft.taxbenefit.api.dto.request.accountchange.ReqAccountCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.accountchange.ReqAccountInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.accountchange.ReqAccountMoreChangeCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.accountchange.ReqAccountMoreChangeDTO;
import com.sinosoft.taxbenefit.api.dto.request.applicant.RepAppAcceptOneBody;
import com.sinosoft.taxbenefit.api.dto.request.applicant.ReqAppAcceptMoreBody;
import com.sinosoft.taxbenefit.api.dto.request.applicant.ReqAppPolicyDTO;
import com.sinosoft.taxbenefit.api.dto.request.base.CoreRequestHead;
import com.sinosoft.taxbenefit.api.dto.request.base.ThirdSendResponseDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimCoreMDTO;
import com.sinosoft.taxbenefit.api.dto.request.claim.ReqClaimInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyInfoCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqContinuePolicyInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqMContinuePolicyInfoCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.continuepolicy.ReqMContinuePolicyInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqCustomerCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqCustomerInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqCustomerMoreCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.customer.ReqCustomerMoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.policystate.PolicyStateEndorsement;
import com.sinosoft.taxbenefit.api.dto.request.policystate.PolicyStateMoreBody;
import com.sinosoft.taxbenefit.api.dto.request.policystate.PolicyStateOneBody;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqMOutPolicyTransBodyDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqMOutPolicyTransCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqSOutPolicyTransCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.policytrans.ReqSOutPolicyTransferDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqMPremiumInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqMRenewalPremiumCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqRenewalPremiumCoreDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqRenewalPremiumInfoDTO;
import com.sinosoft.taxbenefit.api.dto.request.renewalpremium.ReqSPremiumInfoDTO;
import com.sinosoft.taxbenefit.api.dto.response.accountchange.AccountChangeResultDTO;
import com.sinosoft.taxbenefit.api.dto.response.applicant.ResultApplicantDTO;
import com.sinosoft.taxbenefit.api.dto.response.claim.ResultInfoDTO;
import com.sinosoft.taxbenefit.api.dto.response.continuepolicy.ResultContinuePolicyDTO;
import com.sinosoft.taxbenefit.api.dto.response.customer.CustomerVerifyResultDTO;
import com.sinosoft.taxbenefit.api.dto.response.policystate.ResultEndorsement;
import com.sinosoft.taxbenefit.api.dto.response.policytrans.ResHXPolicyTransferResultDTO;
import com.sinosoft.taxbenefit.api.dto.response.renewalpremium.ResultRenewalPremiumDTO;
import com.sinosoft.taxbenefit.core.AccountChange.biz.service.AccountChangeService;
import com.sinosoft.taxbenefit.core.applicant.biz.service.ApplicantCoreService;
import com.sinosoft.taxbenefit.core.claim.biz.service.ClaimInfoCoreService;
import com.sinosoft.taxbenefit.core.common.config.ENUM_HX_SERVICE_PORT_CODE;
import com.sinosoft.taxbenefit.core.continuePolicy.biz.service.ContinuePolicyCoreService;
import com.sinosoft.taxbenefit.core.customer.biz.service.CustomerServiceCore;
import com.sinosoft.taxbenefit.core.faultTask.biz.service.FaultTaskService;
import com.sinosoft.taxbenefit.core.faultTask.dto.AsynInteractionDTO;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxFaultTaskMapper;
import com.sinosoft.taxbenefit.core.generated.model.TaxAsynInteraction;
import com.sinosoft.taxbenefit.core.generated.model.TaxFaultTask;
import com.sinosoft.taxbenefit.core.generated.model.TaxRequestReceiveLogWithBLOBs;
import com.sinosoft.taxbenefit.core.policyTransfer.biz.service.PolicyTransferService;
import com.sinosoft.taxbenefit.core.policystatechange.biz.service.PolicyStateChangeService;
import com.sinosoft.taxbenefit.core.renewalcancel.biz.service.RenewalCancelCoreService;
import com.sinosoft.taxbenefit.core.renewalpremium.biz.service.PremiumCoreService;
import com.sinosoft.taxbenefit.core.securityguard.biz.service.PreservationOfCancelService;
import com.sinosoft.taxbenefit.core.securityguard.biz.service.PreservationOfChangeService;
import com.sinosoft.taxbenefit.core.taxbenvrify.biz.service.TaxBeneVerifyService;
import com.sinosoft.taxbenefit.manage.batch.dto.BargainRequestInfo;
import com.sinosoft.taxbenefit.manage.commom.config.ParamConfig;
import com.sinosoft.taxbenefit.manage.exceptionJob.biz.service.ExceptionJobInfoService;
import com.sinosoft.taxbenefit.manage.exceptionJob.config.ENUM_SERVICE_PORT_CODE;

/**
 * 异常任务批处理
 * @author zhanghao
 */
@Repository
public class ExceptionJobBatch extends TaxBaseJob {
	@Autowired
	ParamConfig paramConfig; // 配置数据对象
	@Autowired
	ExceptionJobInfoService exceptionJobService;//
	@Autowired
	AccountChangeService accountChangeService;// 账户变更
	@Autowired
	ApplicantCoreService applicantCoreservice;// 承保
	@Autowired
	ClaimInfoCoreService claimInfoCoreService;// 理赔
	@Autowired
	ContinuePolicyCoreService continuePolicyCoreService;// 续保信息信息上传
	@Autowired
	CustomerServiceCore customerServiceCore;// 客户验证
	@Autowired
	PolicyStateChangeService policyStateChangeService;// 保单状态修改
	@Autowired
	PolicyTransferService policyTransferService;// 保单转入转出
	@Autowired
	RenewalCancelCoreService renewalCancelCoreService;//续保撤销
	@Autowired
	PreservationOfChangeService preservationOfChangeService;// 保全
	@Autowired
	PreservationOfCancelService preservationOfCancelService;// 保全撤销
	@Autowired
	PremiumCoreService premiumCoreService;//续期
	@Autowired
	TaxBeneVerifyService taxBeneVerifyService;//税优验证
	@Autowired
	TaxFaultTaskMapper taxFaultTaskMapper;//异常任务
	@Autowired
	FaultTaskService tFaultTaskService;
	// 默认调用次数
	Integer num = 7;
	public void executeJob() {
		logger.info("=======================异常和异步任务批处理开始=======================");
		try {
			// 查找所有异步和异常类型,并且处理结果为处理失败和待处理
			List<TaxFaultTask> taxFaultTaskList = exceptionJobService.queryExceptionJobList();
			
			// 保单外部转入 | 税优验证 | 承保撤销上传 | 保全信息上传 | 保全撤销上传 | 续保撤销上传 | 理赔撤销上传 及以下八个的多笔请求,这些需要单次发送
			List<TaxFaultTask> faultTaskList = new ArrayList<TaxFaultTask>();	
			List<TaxFaultTask> policyOutFaultTaskList = new ArrayList<TaxFaultTask>(); // 保单转出登记
			List<TaxFaultTask> applicantFaultTaskList = new ArrayList<TaxFaultTask>();	// 承保信息上传
			List<TaxFaultTask> customerFaultTaskList = new ArrayList<TaxFaultTask>();	// 客户验证上传
			List<TaxFaultTask> policyStateChangeFaultTaskList = new ArrayList<TaxFaultTask>();	// 保单状态变更上传
			List<TaxFaultTask> premiumUploadFaultTaskList = new ArrayList<TaxFaultTask>();	// 续期保费信息上传
			List<TaxFaultTask> renewalUploadFaultTaskList = new ArrayList<TaxFaultTask>();	// 续保信息上传
			List<TaxFaultTask> claimSettlemenFaultTaskList = new ArrayList<TaxFaultTask>();	// 理赔信息上传
			List<TaxFaultTask> accountChangeFaultTaskList = new ArrayList<TaxFaultTask>();	// 账户变更上传
			
			for (TaxFaultTask taxFaultTask : taxFaultTaskList) {
				if(ENUM_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_RECORD_ONE.code().equals(taxFaultTask.getServerPortCode())){
					policyOutFaultTaskList.add(taxFaultTask);
				}else if(ENUM_SERVICE_PORT_CODE.APPLICANT_ONE.code().equals(taxFaultTask.getServerPortCode())){
					applicantFaultTaskList.add(taxFaultTask);
				}else if(ENUM_SERVICE_PORT_CODE.CUSTOMER_VERIFY_ONE.code().equals(taxFaultTask.getServerPortCode())){
					customerFaultTaskList.add(taxFaultTask);
				}else if(ENUM_SERVICE_PORT_CODE.POLICY_STATE_CHANGE_ONE.code().equals(taxFaultTask.getServerPortCode())){
					policyStateChangeFaultTaskList.add(taxFaultTask);
				}else if(ENUM_SERVICE_PORT_CODE.POLICY_PREMIUM_UPLOAD_ONE.code().equals(taxFaultTask.getServerPortCode())){
					premiumUploadFaultTaskList.add(taxFaultTask);
				}else if(ENUM_SERVICE_PORT_CODE.POLICY_RENEWAL_UPLOAD_ONE.code().equals(taxFaultTask.getServerPortCode())){
					renewalUploadFaultTaskList.add(taxFaultTask);
				}else if(ENUM_SERVICE_PORT_CODE.CLAIM_SETTLEMEN_ONE.code().equals(taxFaultTask.getServerPortCode())){
					claimSettlemenFaultTaskList.add(taxFaultTask);
				}else if(ENUM_SERVICE_PORT_CODE.ACCONUT_CHANGE_ONE.code().equals(taxFaultTask.getServerPortCode())){
					accountChangeFaultTaskList.add(taxFaultTask);
				}else{
					faultTaskList.add(taxFaultTask);
				}
			}
			List<Map<String, Object>> mapList = new ArrayList<Map<String,Object>>();
			// 整合客户验证上传
			Map<String, Object> tCmap = new HashMap<String, Object>();
			this.allotMap(tCmap, customerFaultTaskList);
			if(tCmap.size() > 0){
				mapList.add(tCmap);
			}
			// 整合承保信息上传
			Map<String, Object> tAmap = new HashMap<String, Object>();
			this.allotMap(tAmap, applicantFaultTaskList);
			if(tAmap.size() > 0){
				mapList.add(tAmap);
			}
			// 整合保单转出登记
			Map<String, Object> tPOmap = new HashMap<String, Object>();
			this.allotMap(tPOmap, policyOutFaultTaskList);
			if(tPOmap.size() > 0){
				mapList.add(tPOmap);
			}
			// 整合保单状态变更
			Map<String, Object> tPSCmap = new HashMap<String, Object>();
			this.allotMap(tPSCmap, policyStateChangeFaultTaskList);
			if(tPSCmap.size() > 0){
				mapList.add(tPSCmap);
			}
			// 整合保单续期上传
			Map<String, Object> tPUmap = new HashMap<String, Object>();
			this.allotMap(tPUmap, premiumUploadFaultTaskList);
			if(tPUmap.size() > 0){
				mapList.add(tPUmap);
			}
			// 整合保单续保信息上传
			Map<String, Object> tRUmap = new HashMap<String, Object>();
			this.allotMap(tRUmap, renewalUploadFaultTaskList);
			if(tRUmap.size() > 0){
				mapList.add(tRUmap);
			}
			// 整合理赔信息上传
			Map<String, Object> tCSmap = new HashMap<String, Object>();
			this.allotMap(tCSmap, claimSettlemenFaultTaskList);
			if(tCSmap.size() > 0){
				mapList.add(tCSmap);
			}
			// 整合账户变更上传
			Map<String, Object> tACmap = new HashMap<String, Object>();
			this.allotMap(tACmap, accountChangeFaultTaskList);
			if(tACmap.size() > 0){
				mapList.add(tACmap);
			}
			// 整合数据个数
			try {
				num = Integer.parseInt(paramConfig.getCallNum());
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("配置整合数据个数数据错误！使用默认数值！");
			}
			
			// 执行多笔异步请求接口
			for (Map<String, Object> map : mapList) {
				this.groupMapToTaskList(map);
			}
			// 执行多笔任务和其他查询单笔请求
			for (TaxFaultTask taxFaultTask : faultTaskList) {
				this.executeExceptionJob(taxFaultTask);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("异常和异步任务批处理错误：" + e.getMessage());
		}
		logger.info("=======================异常和异步任务批处理结束=======================");
	}
	
	/**
	 * 将集合中相同地区编码的整理到一个Map集合中
	 * @param map
	 * @param list
	 */
	@SuppressWarnings("unchecked")
	private void allotMap(Map<String, Object> map, List<TaxFaultTask> list){
		List<TaxFaultTask> pFaultTask = new ArrayList<TaxFaultTask>();
		for (TaxFaultTask tPItaxFaultTask : list) {
			String areacode = tPItaxFaultTask.getAreaCode();
			if(map.containsKey(areacode)){
				List<TaxFaultTask> piFaultTask = (List<TaxFaultTask>) map.get(areacode);
				piFaultTask.add(tPItaxFaultTask);
			}else{
				if(StringUtil.isBlank(areacode)){
					// 没有填写地区编码的自动为一组
					pFaultTask.add(tPItaxFaultTask);
					map.put("0000", pFaultTask);
				}else{
					List<TaxFaultTask> piFaultTask = new ArrayList<TaxFaultTask>();
					piFaultTask.add(tPItaxFaultTask);
					map.put(areacode, piFaultTask);
				}
			}
		}
	}
	
	/**
	 * 将具有相同地区编码的Map集合按照指定个数分组
	 * @param tMap
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void groupMapToTaskList(Map<String, Object> tMap){
		try {
			Iterator keys = tMap.keySet().iterator();
			while(keys.hasNext()){
				String portCode= "";
				String key = (String) keys.next();
				// 获取某一个地区的任务记录集合
				List<TaxFaultTask> tFaultTaskList = (List<TaxFaultTask>) tMap.get(key);
				int i = 1;
				List<BargainRequestInfo> bargainList = new ArrayList<BargainRequestInfo>();
				Map<String, List<BargainRequestInfo>> bargainMapList = new HashMap<String, List<BargainRequestInfo>>();
				for (TaxFaultTask faultTask : tFaultTaskList) {
					portCode = faultTask.getServerPortCode();
					// 按照之前定义的个数进行分组,超过定义的个数则算一组
					if(i > num){
						bargainMapList.put(faultTask.getSid()+"", bargainList);
						bargainList =  new ArrayList<BargainRequestInfo>();
						i = 0;
					}
					TaxRequestReceiveLogWithBLOBs receiveLog = (TaxRequestReceiveLogWithBLOBs) exceptionJobService.queryRequestContentBySerialNo(faultTask.getSerialNo());
					if(null != receiveLog){
						BargainRequestInfo bargainInfo = new BargainRequestInfo();
						bargainInfo.setFaultTaskId(faultTask.getSid());
						bargainInfo.setSerialNo(faultTask.getSerialNo());
						bargainInfo.setPortCode(receiveLog.getBusinessType());
						bargainInfo.setRequestContent(receiveLog.getRequestContent());
						bargainList.add(bargainInfo);
						i ++;
					}
				}
				// 不足个数，算余下一组
				bargainMapList.put("0000", bargainList);
				this.executeExceptionJob(bargainMapList, portCode);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("执行多笔异步请求接口时出现错误");
		}
	}
	
	/**
	 * 主要是针对不符合中保信多笔异步的其他接口，需要每单次调用，
	 * 方法体中已经对当前的任务进行更新
	 * 其中多笔请求调用失败后，在再次请求之后，需要更新子任务的状态
	 * @param taxFaultTask
	 * @param portCode
	 */
	@SuppressWarnings("rawtypes")
	private void executeExceptionJob(TaxFaultTask taxFaultTask){
		String requestJson = "";
			// 保单转出登记异常任务
		String portCode = taxFaultTask.getServerPortCode();
		if (ENUM_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_RECORD_ONE.code().equals(portCode) || ENUM_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_RECORD_MORE.code().equals(portCode)) {
			if(ENUM_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_RECORD_ONE.code().equals(portCode)){
				TaxRequestReceiveLogWithBLOBs receiveLog = (TaxRequestReceiveLogWithBLOBs) exceptionJobService.queryRequestContentBySerialNo(taxFaultTask.getSerialNo());
				requestJson = receiveLog.getRequestContent();	
			}else if(ENUM_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_RECORD_MORE.code().equals(portCode)){
				requestJson = taxFaultTask.getAsynContent();
			}
			if(StringUtil.isBlank(requestJson)){
				throw new RequestDateException("未找到错误请求报文数据");
			}
			TaxFaultTask faultTask = policyTransferService.policyTransOutRegisterExceptionJob(requestJson, portCode, taxFaultTask.getSid());
			// 如果请求的是多笔,正常请求外，还需要将返回的结果数据进行拆分,拆分到各自的子任务的返回报文中
			if(ENUM_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_RECORD_MORE.code().equals(portCode)){
				String resMainJson = faultTask.getSuccessMessage();
				// 需要更新任务表状态
				String mainSreialNo = faultTask.getSerialNo();
				// 如果中保信调用成功，则进行拆分返回数据
				if((ENUM_BZ_RESPONSE_RESULT.SUCCESS.code().equals(faultTask.getDisposeResult())) && (!ENUM_DISPOSE_TYPE.APPOINTMENT.code().equals(faultTask.getDisposeType()))){
					ThirdSendResponseDTO thirdSendResponse = JSONObject.parseObject(resMainJson, ThirdSendResponseDTO.class);
					List thirdSendResList = JSONObject.parseObject(thirdSendResponse.getResJson().toString(), List.class);
					for (Object object : thirdSendResList) {
						ResHXPolicyTransferResultDTO resHXPolicyTransferResult = JSONObject.parseObject(object.toString(), ResHXPolicyTransferResultDTO.class);
						List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
						for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
							// 以业务号作为关联数据
							if(taxAsynInteraction.getBusinessNo().equals(resHXPolicyTransferResult.getBizNo())){
								ThirdSendResponseDTO thirdSendResponseDTO = new ThirdSendResponseDTO();
								thirdSendResponseDTO.setResJson(resHXPolicyTransferResult);
								thirdSendResponseDTO.setResultCode(thirdSendResponse.getResultCode());
								thirdSendResponseDTO.setResultDesc("处理成功");
								String resJson = JSONObject.toJSONString(thirdSendResponseDTO);
								tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resJson, ENUM_DISPOSE_STATE.SUCCES.code());
								break;
							}
						}
					}
				}else{
					List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
					for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
						tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resMainJson, ENUM_DISPOSE_STATE.SUCCES.code());
					}
				}
			}
			//保单状态修改上传
		}else if(ENUM_SERVICE_PORT_CODE.POLICY_STATE_CHANGE_ONE.code().equals(portCode)	|| ENUM_SERVICE_PORT_CODE.POLICY_STATE_CHANGE_MORE.code().equals(portCode)){
			if(ENUM_SERVICE_PORT_CODE.POLICY_STATE_CHANGE_ONE.code().equals(portCode)){
				TaxRequestReceiveLogWithBLOBs receiveLog = (TaxRequestReceiveLogWithBLOBs) exceptionJobService.queryRequestContentBySerialNo(taxFaultTask.getSerialNo());
				requestJson = receiveLog.getRequestContent();	
			}else if(ENUM_SERVICE_PORT_CODE.POLICY_STATE_CHANGE_MORE.code().equals(portCode)){
				requestJson = taxFaultTask.getAsynContent();
			}
			if(StringUtil.isBlank(requestJson)){
				throw new RequestDateException("未找到错误请求报文数据");
			}
			TaxFaultTask faultTask = policyStateChangeService.modifyPolicyStateExceptionJob(requestJson, portCode, taxFaultTask.getSid());
			// 如果请求的是多笔,正常请求外，还需要将返回的结果数据进行拆分,拆分到各自的子任务的返回报文中
			if(ENUM_SERVICE_PORT_CODE.POLICY_STATE_CHANGE_MORE.code().equals(portCode)){
				// 获取中保信的返回数据
				String resMainJson = faultTask.getSuccessMessage();
				// 需要更新任务表状态
				String mainSreialNo = faultTask.getSerialNo();
				// 如果中保信调用成功，则进行拆分返回数据
				if((ENUM_BZ_RESPONSE_RESULT.SUCCESS.code().equals(faultTask.getDisposeResult())) && (!ENUM_DISPOSE_TYPE.APPOINTMENT.code().equals(faultTask.getDisposeType()))){
					ThirdSendResponseDTO thirdSendResponse = JSONObject.parseObject(resMainJson, ThirdSendResponseDTO.class);
					List thirdSendResList = JSONObject.parseObject(thirdSendResponse.getResJson().toString(), List.class);
					for (Object object : thirdSendResList) {
						ResultEndorsement resultEndorsement = JSONObject.parseObject(object.toString(), ResultEndorsement.class);
						List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
						for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
							// 以业务号作为关联数据
							if(taxAsynInteraction.getBusinessNo().equals(resultEndorsement.getBizNo())){
								ThirdSendResponseDTO thirdSendResponseDTO = new ThirdSendResponseDTO();
								thirdSendResponseDTO.setResJson(resultEndorsement);
								thirdSendResponseDTO.setResultCode(thirdSendResponse.getResultCode());
								thirdSendResponseDTO.setResultDesc("处理成功");
								String resJson = JSONObject.toJSONString(thirdSendResponseDTO);
								tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resJson, ENUM_DISPOSE_STATE.SUCCES.code());
								break;
							}
						}
					}
				}else{
					List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
					for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
						tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resMainJson, ENUM_DISPOSE_STATE.SUCCES.code());
					}
				}
			}
			//承保信息上传	
		}else if(ENUM_SERVICE_PORT_CODE.APPLICANT_ONE.code().equals(portCode) || 	ENUM_SERVICE_PORT_CODE.APPLICANT_MORE.code().equals(portCode)){
			if(ENUM_SERVICE_PORT_CODE.APPLICANT_ONE.code().equals(portCode)){
				TaxRequestReceiveLogWithBLOBs receiveLog = (TaxRequestReceiveLogWithBLOBs) exceptionJobService.queryRequestContentBySerialNo(taxFaultTask.getSerialNo());
				requestJson = receiveLog.getRequestContent();	
			}else if(ENUM_SERVICE_PORT_CODE.APPLICANT_MORE.code().equals(portCode)){
				requestJson = taxFaultTask.getAsynContent();
			}
			if(StringUtil.isBlank(requestJson)){
				throw new RequestDateException("未找到错误请求报文数据");
			}
			TaxFaultTask faultTask = applicantCoreservice.applicantExceptionJob(requestJson, portCode, taxFaultTask.getSid());
			if(ENUM_SERVICE_PORT_CODE.APPLICANT_MORE.code().equals(portCode)){
				String resMainJson = faultTask.getSuccessMessage();
				// 需要更新任务表状态
				String mainSreialNo = faultTask.getSerialNo();
				// 如果中保信调用成功，则进行拆分返回数据
				if((ENUM_BZ_RESPONSE_RESULT.SUCCESS.code().equals(faultTask.getDisposeResult())) && (!ENUM_DISPOSE_TYPE.APPOINTMENT.code().equals(faultTask.getDisposeType()))){
					ThirdSendResponseDTO thirdSendResponse = JSONObject.parseObject(resMainJson, ThirdSendResponseDTO.class);
					List thirdSendResList = JSONObject.parseObject(thirdSendResponse.getResJson().toString(), List.class);
					for (Object object : thirdSendResList) {
						ResultApplicantDTO resultApplicantDTO = JSONObject.parseObject(object.toString(), ResultApplicantDTO.class);
						List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
						for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
							// 以业务号作为关联数据
							if(taxAsynInteraction.getBusinessNo().equals(resultApplicantDTO.getBizNo())){
								ThirdSendResponseDTO thirdSendResponseDTO = new ThirdSendResponseDTO();
								thirdSendResponseDTO.setResJson(resultApplicantDTO);
								thirdSendResponseDTO.setResultCode(thirdSendResponse.getResultCode());
								thirdSendResponseDTO.setResultDesc("处理成功");
								String resJson = JSONObject.toJSONString(thirdSendResponseDTO);
								tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resJson, ENUM_DISPOSE_STATE.SUCCES.code());
								break;
							}
						}
					}
				}else{
					List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
					for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
						tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resMainJson, ENUM_DISPOSE_STATE.SUCCES.code());
					}
				}
			}
			//理赔上传
		}else if(ENUM_SERVICE_PORT_CODE.CLAIM_SETTLEMEN_ONE.code().equals(portCode) ||	ENUM_SERVICE_PORT_CODE.CLAIM_SETTLEMEN_MORE.code().equals(portCode)){
			if(ENUM_SERVICE_PORT_CODE.CLAIM_SETTLEMEN_ONE.code().equals(portCode)){
				TaxRequestReceiveLogWithBLOBs receiveLog = (TaxRequestReceiveLogWithBLOBs) exceptionJobService.queryRequestContentBySerialNo(taxFaultTask.getSerialNo());
				requestJson = receiveLog.getRequestContent();	
			}else if(ENUM_SERVICE_PORT_CODE.CLAIM_SETTLEMEN_MORE.code().equals(portCode)){
				requestJson = taxFaultTask.getAsynContent();
			}
			if(StringUtil.isBlank(requestJson)){
				throw new RequestDateException("未找到错误请求报文数据");
			}
			TaxFaultTask faultTask = claimInfoCoreService.addClaimSettlementExceptionJob(requestJson, portCode, taxFaultTask.getSid());
			if(ENUM_SERVICE_PORT_CODE.CLAIM_SETTLEMEN_MORE.code().equals(portCode)){
				String resMainJson = faultTask.getSuccessMessage();
				// 需要更新任务表状态
				String mainSreialNo = faultTask.getSerialNo();
				// 如果中保信调用成功，则进行拆分返回数据
				if((ENUM_BZ_RESPONSE_RESULT.SUCCESS.code().equals(faultTask.getDisposeResult())) && (!ENUM_DISPOSE_TYPE.APPOINTMENT.code().equals(faultTask.getDisposeType()))){
					ThirdSendResponseDTO thirdSendResponse = JSONObject.parseObject(resMainJson, ThirdSendResponseDTO.class);
					List thirdSendResList = JSONObject.parseObject(thirdSendResponse.getResJson().toString(), List.class);
					for (Object object : thirdSendResList) {
						ResultInfoDTO resultInfoDTO = JSONObject.parseObject(object.toString(), ResultInfoDTO.class);
						List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
						for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
							// 以业务号作为关联数据
							if(taxAsynInteraction.getBusinessNo().equals(resultInfoDTO.getBizNo())){
								ThirdSendResponseDTO thirdSendResponseDTO = new ThirdSendResponseDTO();
								thirdSendResponseDTO.setResJson(resultInfoDTO);
								thirdSendResponseDTO.setResultCode(thirdSendResponse.getResultCode());
								thirdSendResponseDTO.setResultDesc("处理成功");
								String resJson = JSONObject.toJSONString(thirdSendResponseDTO);
								tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resJson, ENUM_DISPOSE_STATE.SUCCES.code());
								break;
							}
						}
					}
				}else{
					List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
					for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
						tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resMainJson, ENUM_DISPOSE_STATE.SUCCES.code());
					}
				}
			}
			//客户验证上传
		}else if(ENUM_SERVICE_PORT_CODE.CUSTOMER_VERIFY_ONE.code().equals(portCode) ||	ENUM_SERVICE_PORT_CODE.CUSTOMER_VERIFY_MORE.code().equals(portCode)){
			if(ENUM_SERVICE_PORT_CODE.CUSTOMER_VERIFY_ONE.code().equals(portCode)){
				TaxRequestReceiveLogWithBLOBs receiveLog = (TaxRequestReceiveLogWithBLOBs) exceptionJobService.queryRequestContentBySerialNo(taxFaultTask.getSerialNo());
				requestJson = receiveLog.getRequestContent();	
			}else if(ENUM_SERVICE_PORT_CODE.CUSTOMER_VERIFY_MORE.code().equals(portCode)){
				requestJson = taxFaultTask.getAsynContent();
			}
			if(StringUtil.isBlank(requestJson)){
				throw new RequestDateException("未找到错误请求报文数据");
			}
			TaxFaultTask faultTask = customerServiceCore.customerExceptionJob(requestJson, portCode, taxFaultTask.getSid());
			if(ENUM_SERVICE_PORT_CODE.CUSTOMER_VERIFY_MORE.code().equals(portCode)){
				String resMainJson = faultTask.getSuccessMessage();
				// 需要更新任务表状态
				String mainSreialNo = faultTask.getSerialNo();
				// 如果中保信调用成功，则进行拆分返回数据
				if((ENUM_BZ_RESPONSE_RESULT.SUCCESS.code().equals(faultTask.getDisposeResult())) && (!ENUM_DISPOSE_TYPE.APPOINTMENT.code().equals(faultTask.getDisposeType()))){
					ThirdSendResponseDTO thirdSendResponse = JSONObject.parseObject(resMainJson, ThirdSendResponseDTO.class);
					List thirdSendResList = JSONObject.parseObject(thirdSendResponse.getResJson().toString(), List.class);
					for (Object object : thirdSendResList) {
						CustomerVerifyResultDTO customerVerifyResult = JSONObject.parseObject(object.toString(), CustomerVerifyResultDTO.class);
						List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
						for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
							// 以业务号作为关联数据
							if(taxAsynInteraction.getBusinessNo().equals(customerVerifyResult.getBizNo())){
								ThirdSendResponseDTO thirdSendResponseDTO = new ThirdSendResponseDTO();
								thirdSendResponseDTO.setResJson(customerVerifyResult);
								thirdSendResponseDTO.setResultCode(thirdSendResponse.getResultCode());
								thirdSendResponseDTO.setResultDesc("处理成功");
								String resJson = JSONObject.toJSONString(thirdSendResponseDTO);
								tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resJson, ENUM_DISPOSE_STATE.SUCCES.code());
								break;
							}
						}
					}
				}else{
					List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
					for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
						tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resMainJson, ENUM_DISPOSE_STATE.SUCCES.code());
					}
				}
			}
			//续期保费信息上传
		}else if(ENUM_SERVICE_PORT_CODE.POLICY_PREMIUM_UPLOAD_ONE.code().equals(portCode) || ENUM_SERVICE_PORT_CODE.POLICY_PREMIUM_UPLOAD_MORE.code().equals(portCode)){
			if(ENUM_SERVICE_PORT_CODE.POLICY_PREMIUM_UPLOAD_ONE.code().equals(portCode)){
				TaxRequestReceiveLogWithBLOBs receiveLog = (TaxRequestReceiveLogWithBLOBs) exceptionJobService.queryRequestContentBySerialNo(taxFaultTask.getSerialNo());
				requestJson = receiveLog.getRequestContent();	
			}else if(ENUM_SERVICE_PORT_CODE.POLICY_PREMIUM_UPLOAD_MORE.code().equals(portCode)){
				requestJson = taxFaultTask.getAsynContent();
			}
			if(StringUtil.isBlank(requestJson)){
				throw new RequestDateException("未找到错误请求报文数据");
			}
			TaxFaultTask faultTask = premiumCoreService.premiumExceptionJob(requestJson, portCode, taxFaultTask.getSid());
			if(ENUM_SERVICE_PORT_CODE.POLICY_PREMIUM_UPLOAD_MORE.code().equals(portCode)){
				String resMainJson = faultTask.getSuccessMessage();
				// 需要更新任务表状态
				String mainSreialNo = faultTask.getSerialNo();
				// 如果中保信调用成功，则进行拆分返回数据
				if((ENUM_BZ_RESPONSE_RESULT.SUCCESS.code().equals(faultTask.getDisposeResult())) && (!ENUM_DISPOSE_TYPE.APPOINTMENT.code().equals(faultTask.getDisposeType()))){
					ThirdSendResponseDTO thirdSendResponse = JSONObject.parseObject(resMainJson, ThirdSendResponseDTO.class);
					List thirdSendResList = JSONObject.parseObject(thirdSendResponse.getResJson().toString(), List.class);
					for (Object object : thirdSendResList) {
						ResultRenewalPremiumDTO resultRenewalPremiumDTO = JSONObject.parseObject(object.toString(), ResultRenewalPremiumDTO.class);
						List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
						for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
							// 以业务号作为关联数据
							if(taxAsynInteraction.getBusinessNo().equals(resultRenewalPremiumDTO.getBizNo())){
								ThirdSendResponseDTO thirdSendResponseDTO = new ThirdSendResponseDTO();
								thirdSendResponseDTO.setResJson(resultRenewalPremiumDTO);
								thirdSendResponseDTO.setResultCode(thirdSendResponse.getResultCode());
								thirdSendResponseDTO.setResultDesc("处理成功");
								String resJson = JSONObject.toJSONString(thirdSendResponseDTO);
								tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resJson, ENUM_DISPOSE_STATE.SUCCES.code());
								break;
							}
						}
					}
				}else{
					List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
					for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
						tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resMainJson, ENUM_DISPOSE_STATE.SUCCES.code());
					}
				}
			}
			//续保信息上传
		}else if(ENUM_SERVICE_PORT_CODE.POLICY_RENEWAL_UPLOAD_ONE.code().equals(portCode) || 	ENUM_SERVICE_PORT_CODE.POLICY_RENEWAL_UPLOAD_MORE.code().equals(portCode)){
			if(ENUM_SERVICE_PORT_CODE.POLICY_RENEWAL_UPLOAD_ONE.code().equals(portCode)){
				TaxRequestReceiveLogWithBLOBs receiveLog = (TaxRequestReceiveLogWithBLOBs) exceptionJobService.queryRequestContentBySerialNo(taxFaultTask.getSerialNo());
				requestJson = receiveLog.getRequestContent();	
			}else if(ENUM_SERVICE_PORT_CODE.POLICY_RENEWAL_UPLOAD_MORE.code().equals(portCode)){
				requestJson = taxFaultTask.getAsynContent();
			}
			if(StringUtil.isBlank(requestJson)){
				throw new RequestDateException("未找到错误请求报文数据");
			}
			TaxFaultTask faultTask = continuePolicyCoreService.continuePolicyExceptionJob(requestJson, portCode, taxFaultTask.getSid());
			if(ENUM_SERVICE_PORT_CODE.POLICY_RENEWAL_UPLOAD_MORE.code().equals(portCode)){
				String resMainJson = faultTask.getSuccessMessage();
				// 需要更新任务表状态
				String mainSreialNo = faultTask.getSerialNo();
				// 如果中保信调用成功，则进行拆分返回数据
				if((ENUM_BZ_RESPONSE_RESULT.SUCCESS.code().equals(faultTask.getDisposeResult())) && (!ENUM_DISPOSE_TYPE.APPOINTMENT.code().equals(faultTask.getDisposeType()))){
					ThirdSendResponseDTO thirdSendResponse = JSONObject.parseObject(resMainJson, ThirdSendResponseDTO.class);
					List thirdSendResList = JSONObject.parseObject(thirdSendResponse.getResJson().toString(), List.class);
					for (Object object : thirdSendResList) {
						ResultContinuePolicyDTO resultContinuePolicy = JSONObject.parseObject(object.toString(), ResultContinuePolicyDTO.class);
						List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
						for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
							// 以业务号作为关联数据
							if(taxAsynInteraction.getBusinessNo().equals(resultContinuePolicy.getBizNo())){
								ThirdSendResponseDTO thirdSendResponseDTO = new ThirdSendResponseDTO();
								thirdSendResponseDTO.setResJson(resultContinuePolicy);
								thirdSendResponseDTO.setResultCode(thirdSendResponse.getResultCode());
								thirdSendResponseDTO.setResultDesc("处理成功");
								String resJson = JSONObject.toJSONString(thirdSendResponseDTO);
								tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resJson, ENUM_DISPOSE_STATE.SUCCES.code());
								break;
							}
						}
					}
				}else{
					List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
					for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
						tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resMainJson, ENUM_DISPOSE_STATE.SUCCES.code());
					}
				}
			}
			//账户变更上传
		}else if(ENUM_SERVICE_PORT_CODE.ACCONUT_CHANGE_ONE.code().equals(portCode) || 	ENUM_SERVICE_PORT_CODE.ACCONUT_CHANGE_MORE.code().equals(portCode)){
			if(ENUM_SERVICE_PORT_CODE.ACCONUT_CHANGE_ONE.code().equals(portCode)){
				TaxRequestReceiveLogWithBLOBs receiveLog = (TaxRequestReceiveLogWithBLOBs) exceptionJobService.queryRequestContentBySerialNo(taxFaultTask.getSerialNo());
				requestJson = receiveLog.getRequestContent();	
			}else if(ENUM_SERVICE_PORT_CODE.ACCONUT_CHANGE_MORE.code().equals(portCode)){
				requestJson = taxFaultTask.getAsynContent();
			}
			if(StringUtil.isBlank(requestJson)){
				throw new RequestDateException("未找到错误请求报文数据");
			}
			TaxFaultTask faultTask = accountChangeService.accountChangeExceptionJob(requestJson, portCode, taxFaultTask.getSid());
			if(ENUM_SERVICE_PORT_CODE.ACCONUT_CHANGE_MORE.code().equals(portCode)){
				String resMainJson = faultTask.getSuccessMessage();
				// 需要更新任务表状态
				String mainSreialNo = faultTask.getSerialNo();
				// 如果中保信调用成功，则进行拆分返回数据
				if((ENUM_BZ_RESPONSE_RESULT.SUCCESS.code().equals(faultTask.getDisposeResult())) && (!ENUM_DISPOSE_TYPE.APPOINTMENT.code().equals(faultTask.getDisposeType()))){
					ThirdSendResponseDTO thirdSendResponse = JSONObject.parseObject(resMainJson, ThirdSendResponseDTO.class);
					List thirdSendResList = JSONObject.parseObject(thirdSendResponse.getResJson().toString(), List.class);
					for (Object object : thirdSendResList) {
						AccountChangeResultDTO accountChangeResultDTO = JSONObject.parseObject(object.toString(), AccountChangeResultDTO.class);
						List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
						for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
							// 以业务号作为关联数据
							if(taxAsynInteraction.getBusinessNo().equals(accountChangeResultDTO.getBizNo())){
								ThirdSendResponseDTO thirdSendResponseDTO = new ThirdSendResponseDTO();
								thirdSendResponseDTO.setResJson(accountChangeResultDTO);
								thirdSendResponseDTO.setResultCode(thirdSendResponse.getResultCode());
								thirdSendResponseDTO.setResultDesc("处理成功");
								String resJson = JSONObject.toJSONString(thirdSendResponseDTO);
								tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resJson, ENUM_DISPOSE_STATE.SUCCES.code());
								break;
							}
						}
					}
				}else{
					List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
					for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
						tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resMainJson, ENUM_DISPOSE_STATE.SUCCES.code());
					}
				}
			}
			// 保单外部转入申请上传
		}else if (ENUM_SERVICE_PORT_CODE.POLICY_IN_TRANSFER_UPLOAD_ONE.code().equals(portCode)) {
			TaxRequestReceiveLogWithBLOBs receiveLog = (TaxRequestReceiveLogWithBLOBs) exceptionJobService.queryRequestContentBySerialNo(taxFaultTask.getSerialNo());
			requestJson = receiveLog.getRequestContent();
			policyTransferService.policyTransInExceptionJob(requestJson, portCode, taxFaultTask);
			//保全信息上传
		}else if(ENUM_SERVICE_PORT_CODE.SERCURITY_CHANGE.code().equals(portCode)){
			TaxRequestReceiveLogWithBLOBs receiveLog = (TaxRequestReceiveLogWithBLOBs) exceptionJobService.queryRequestContentBySerialNo(taxFaultTask.getSerialNo());
			requestJson = receiveLog.getRequestContent();
			preservationOfChangeService.modifySecurityExceptionJob(requestJson, portCode, taxFaultTask);
			//保全撤销
		}else if(ENUM_SERVICE_PORT_CODE.SERCURITY_CANCEL.code().equals(portCode)){
			TaxRequestReceiveLogWithBLOBs receiveLog = (TaxRequestReceiveLogWithBLOBs) exceptionJobService.queryRequestContentBySerialNo(taxFaultTask.getSerialNo());
			requestJson = receiveLog.getRequestContent();
			preservationOfCancelService.modifyCancelExceptionJob(requestJson, portCode, taxFaultTask);
			//承保撤销
		}else if(ENUM_SERVICE_PORT_CODE.POLICY_REVERSAL.code().equals(portCode)){
			TaxRequestReceiveLogWithBLOBs receiveLog = (TaxRequestReceiveLogWithBLOBs) exceptionJobService.queryRequestContentBySerialNo(taxFaultTask.getSerialNo());
			requestJson = receiveLog.getRequestContent();
			applicantCoreservice.policyReversalExceptionJob(requestJson, portCode, taxFaultTask);
			//理赔撤销上传
		}else if(ENUM_SERVICE_PORT_CODE.CLAIM_REVERSAL.code().equals(portCode)){
			TaxRequestReceiveLogWithBLOBs receiveLog = (TaxRequestReceiveLogWithBLOBs) exceptionJobService.queryRequestContentBySerialNo(taxFaultTask.getSerialNo());
			requestJson = receiveLog.getRequestContent();
			claimInfoCoreService.modifyClaimStateExceptionJob(requestJson, portCode, taxFaultTask);
			//续保撤销
		}else if(ENUM_SERVICE_PORT_CODE.POLICY_RENEWAL_CANCEL.code().equals(portCode)){
			TaxRequestReceiveLogWithBLOBs receiveLog = (TaxRequestReceiveLogWithBLOBs) exceptionJobService.queryRequestContentBySerialNo(taxFaultTask.getSerialNo());
			requestJson = receiveLog.getRequestContent();
			renewalCancelCoreService.renewalCancelExceptionJob(requestJson, portCode, taxFaultTask);
			//税优验证上传
		}else if(ENUM_SERVICE_PORT_CODE.TAX_BENE_VERIFY.code().equals(portCode)){
			TaxRequestReceiveLogWithBLOBs receiveLog = (TaxRequestReceiveLogWithBLOBs) exceptionJobService.queryRequestContentBySerialNo(taxFaultTask.getSerialNo());
			requestJson = receiveLog.getRequestContent();
			taxBeneVerifyService.taxBeneVerifyExceptionJob(requestJson, portCode, taxFaultTask);
		}		
	}
	
	/**
	 * 执行多笔中保信异步请求
	 * @param bargainMapList
	 * @param portCode
	 */
	@SuppressWarnings("rawtypes")
	private void executeExceptionJob(Map<String, List<BargainRequestInfo>> bargainMapList, String portCode) {
		try {
			if (ENUM_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_RECORD_ONE.code().equals(portCode)) {	// 保单转出异步调用
				CoreRequestHead coreRequestHead = new CoreRequestHead();
				List<ReqSOutPolicyTransferDTO> transInfoList = new ArrayList<ReqSOutPolicyTransferDTO>();
				// 将同一Key值下的集合的报文整合成多笔请求报文
				Iterator keys = bargainMapList.keySet().iterator();
				while(keys.hasNext()){
					String key = (String) keys.next();
					List<BargainRequestInfo> tBargainRequestList = bargainMapList.get(key);
					List<AsynInteractionDTO> tAsynInteractionList = new ArrayList<AsynInteractionDTO>();
					for (BargainRequestInfo bargainRequestInfo : tBargainRequestList) {
						if(ENUM_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_RECORD_ONE.code().equals(bargainRequestInfo.getPortCode())){
							ReqSOutPolicyTransCoreDTO reqSOutPolicyTransCore = JSONObject.parseObject(bargainRequestInfo.getRequestContent(), ReqSOutPolicyTransCoreDTO.class);
							ReqSOutPolicyTransferDTO transInfo = (ReqSOutPolicyTransferDTO) reqSOutPolicyTransCore.getBody();
							transInfoList.add(transInfo);
							AsynInteractionDTO tAsynInteraction = new AsynInteractionDTO();
							tAsynInteraction.setSonSerialNo(bargainRequestInfo.getSerialNo());
							tAsynInteraction.setBusNo(transInfo.getBizNo());
							tAsynInteractionList.add(tAsynInteraction);
							coreRequestHead = reqSOutPolicyTransCore.getRequestHead();
//				目前核心只存在单笔		
//					}else if(ENUM_HX_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_RECORD_MORE.code().equals(portCode)){
//						ReqMOutPolicyTransCoreDTO reqMOutPolicyTransCore = JSONObject.parseObject(bargainRequestInfo.getRequestContent(), ReqMOutPolicyTransCoreDTO.class);
//						ReqMOutPolicyTransBodyDTO transInfoBody = (ReqMOutPolicyTransBodyDTO) reqMOutPolicyTransCore.getBody();
//						for (ReqSOutPolicyTransferDTO reqSOutPolicyTransfer : transInfoBody.getTransferOutList()) {
//							transInfoList.add(reqSOutPolicyTransfer);
//							AsynInteractionDTO tAsynInteraction = new AsynInteractionDTO();
//							tAsynInteraction.setSonSerialNo(bargainRequestInfo.getSerialNo());
//							tAsynInteraction.setBusNo(reqSOutPolicyTransfer.getBizNo());
//							tAsynInteractionList.add(tAsynInteraction);
//						}
//						coreRequestHead = reqMOutPolicyTransCore.getRequestHead();
						}
					}
					coreRequestHead.setRecordNum(tAsynInteractionList.size()+"");
					//这里需要注意的是：由于是将多条业务数据整合到一起，流水号需要更新
					String uuid = UUID.randomUUID().toString();
					coreRequestHead.setSerialNo(uuid);
					ReqMOutPolicyTransBodyDTO reqMOutPolicyTransBodyDTO = new ReqMOutPolicyTransBodyDTO();
					reqMOutPolicyTransBodyDTO.setTransferOutList(transInfoList);
					ReqMOutPolicyTransCoreDTO reqMOutPolicyTransCoreDTO = new ReqMOutPolicyTransCoreDTO();
					reqMOutPolicyTransCoreDTO.setBody(reqMOutPolicyTransBodyDTO);
					reqMOutPolicyTransCoreDTO.setRequestHead(coreRequestHead);
					String requestJson = JSONObject.toJSONString(reqMOutPolicyTransCoreDTO);
					
					// 添加异步数据关联表数据,并修改子任务状态
					for(AsynInteractionDTO tAsynInteractionInfo : tAsynInteractionList){
						tAsynInteractionInfo.setMainSerialNo(uuid);
						tFaultTaskService.insertAsynInteraction(tAsynInteractionInfo);
					}
					// 为当前多笔请求任务,添加任务记录表数据，用于接收组装多笔的返回结果
					TaxFaulTaskDTO taxFaulTaskDTO = new TaxFaulTaskDTO();
					taxFaulTaskDTO.setDisposeType(ENUM_DISPOSE_TYPE.ASYNC.code());
					taxFaulTaskDTO.setAsynContent(requestJson);
					taxFaulTaskDTO.setServerPortCode(ENUM_HX_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_RECORD_MORE.code());
					taxFaulTaskDTO.setServerPortName(ENUM_HX_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_RECORD_MORE.desc());
					taxFaulTaskDTO.setSerialNo(uuid);
					Integer faultTaskId = tFaultTaskService.addTaxFaultTask(taxFaulTaskDTO);
					
					// 调用各自接口的异常任务方法
					TaxFaultTask faultTask = policyTransferService.policyTransOutRegisterExceptionJob(requestJson, ENUM_HX_SERVICE_PORT_CODE.POLICY_OUT_TRANSFER_RECORD_MORE.code(), faultTaskId);
					// 获取中保信的返回数据
					String resMainJson = faultTask.getSuccessMessage();
					// 需要更新任务表状态
					String mainSreialNo = faultTask.getSerialNo();
					// 如果中保信调用成功，则进行拆分返回数据
					if((ENUM_BZ_RESPONSE_RESULT.SUCCESS.code().equals(faultTask.getDisposeResult())) && (!ENUM_DISPOSE_TYPE.APPOINTMENT.code().equals(faultTask.getDisposeType()))){
						ThirdSendResponseDTO thirdSendResponse = JSONObject.parseObject(resMainJson, ThirdSendResponseDTO.class);
						List thirdSendResList = JSONObject.parseObject(thirdSendResponse.getResJson().toString(), List.class);
						for (Object object : thirdSendResList) {
							ResHXPolicyTransferResultDTO resHXPolicyTransferResult = JSONObject.parseObject(object.toString(), ResHXPolicyTransferResultDTO.class);
							List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
							for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
								// 以业务号作为关联数据
								if(taxAsynInteraction.getBusinessNo().equals(resHXPolicyTransferResult.getBizNo())){
									ThirdSendResponseDTO thirdSendResponseDTO = new ThirdSendResponseDTO();
									thirdSendResponseDTO.setResJson(resHXPolicyTransferResult);
									thirdSendResponseDTO.setResultCode(thirdSendResponse.getResultCode());
									thirdSendResponseDTO.setResultDesc("处理成功");
									String resJson = JSONObject.toJSONString(thirdSendResponseDTO);
									tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resJson, ENUM_DISPOSE_STATE.SUCCES.code());
									break;
								}
							}
						}
					}else{
						List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
						for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
							tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resMainJson, ENUM_DISPOSE_STATE.SUCCES.code());
						}
					}
				}
			}else if (ENUM_SERVICE_PORT_CODE.APPLICANT_ONE.code().equals(portCode)) {	// 承保上传异步调用
				CoreRequestHead coreRequestHead = new CoreRequestHead();
				List<ReqAppPolicyDTO> appPolicyList = new ArrayList<ReqAppPolicyDTO>();
				// 将同一Key值下的集合的报文整合成多笔请求报文
				Iterator keys = bargainMapList.keySet().iterator();
				while(keys.hasNext()){
					String key = (String) keys.next();
					List<BargainRequestInfo> tBargainRequestList = bargainMapList.get(key);
					List<AsynInteractionDTO> tAsynInteractionList = new ArrayList<AsynInteractionDTO>();
					for (BargainRequestInfo bargainRequestInfo : tBargainRequestList) {
						if(ENUM_SERVICE_PORT_CODE.APPLICANT_ONE.code().equals(bargainRequestInfo.getPortCode())){
							RepAppAcceptOneBody repAppAcceptOneBody = JSONObject.parseObject(bargainRequestInfo.getRequestContent(), RepAppAcceptOneBody.class);
							ReqAppPolicyDTO appPolicy = (ReqAppPolicyDTO)repAppAcceptOneBody.getBody();
							appPolicyList.add(appPolicy);
							AsynInteractionDTO tAsynInteraction = new AsynInteractionDTO();
							tAsynInteraction.setSonSerialNo(bargainRequestInfo.getSerialNo());
							tAsynInteraction.setBusNo(appPolicy.getBizNo());
							tAsynInteractionList.add(tAsynInteraction);
							coreRequestHead = repAppAcceptOneBody.getRequestHead();
						}
					}
					coreRequestHead.setRecordNum(tAsynInteractionList.size()+"");
					//这里需要注意的是：由于是将多条业务数据整合到一起，流水号需要更新
					String uuid = UUID.randomUUID().toString();
					coreRequestHead.setSerialNo(uuid);
					ReqAppAcceptMoreBody reqAppAcceptMoreBody = new ReqAppAcceptMoreBody();
					reqAppAcceptMoreBody.setBody(appPolicyList);
					reqAppAcceptMoreBody.setRequestHead(coreRequestHead);
					String requestJson = JSONObject.toJSONString(reqAppAcceptMoreBody);
					
					// 添加异步数据关联表数据,并修改子任务状态
					for(AsynInteractionDTO tAsynInteractionInfo : tAsynInteractionList){
						tAsynInteractionInfo.setMainSerialNo(uuid);
						tFaultTaskService.insertAsynInteraction(tAsynInteractionInfo);
					}
					// 为当前多笔请求任务,添加任务记录表数据，用于接收组装多笔的返回结果
					TaxFaulTaskDTO taxFaulTaskDTO = new TaxFaulTaskDTO();
					taxFaulTaskDTO.setDisposeType(ENUM_DISPOSE_TYPE.ASYNC.code());
					taxFaulTaskDTO.setAsynContent(requestJson);
					taxFaulTaskDTO.setServerPortCode(ENUM_HX_SERVICE_PORT_CODE.APPLICANT_MORE.code());
					taxFaulTaskDTO.setServerPortName(ENUM_HX_SERVICE_PORT_CODE.APPLICANT_MORE.desc());
					taxFaulTaskDTO.setSerialNo(uuid);
					Integer faultTaskId = tFaultTaskService.addTaxFaultTask(taxFaulTaskDTO);
					
					// 调用各自接口的异常任务方法
					TaxFaultTask faultTask = applicantCoreservice.applicantExceptionJob(requestJson, ENUM_HX_SERVICE_PORT_CODE.APPLICANT_MORE.code(), faultTaskId);
					// 获取中保信的返回数据
					String resMainJson = faultTask.getSuccessMessage();
					// 需要更新任务表状态
					String mainSreialNo = faultTask.getSerialNo();
					// 如果中保信调用成功，则进行拆分返回数据
					if((ENUM_BZ_RESPONSE_RESULT.SUCCESS.code().equals(faultTask.getDisposeResult())) && (!ENUM_DISPOSE_TYPE.APPOINTMENT.code().equals(faultTask.getDisposeType()))){
						ThirdSendResponseDTO thirdSendResponse = JSONObject.parseObject(resMainJson, ThirdSendResponseDTO.class);
						List thirdSendResList = JSONObject.parseObject(thirdSendResponse.getResJson().toString(), List.class);
						for (Object object : thirdSendResList) {
							ResultApplicantDTO resultApplicantDTO = JSONObject.parseObject(object.toString(), ResultApplicantDTO.class);
							List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
							for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
								// 以业务号作为关联数据
								if(taxAsynInteraction.getBusinessNo().equals(resultApplicantDTO.getBizNo())){
									ThirdSendResponseDTO thirdSendResponseDTO = new ThirdSendResponseDTO();
									thirdSendResponseDTO.setResJson(resultApplicantDTO);
									thirdSendResponseDTO.setResultCode(thirdSendResponse.getResultCode());
									thirdSendResponseDTO.setResultDesc("处理成功");
									String resJson = JSONObject.toJSONString(thirdSendResponseDTO);
									tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resJson, ENUM_DISPOSE_STATE.SUCCES.code());
									break;
								}
							}
						}
					}else{
						List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
						for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
							tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resMainJson, ENUM_DISPOSE_STATE.SUCCES.code());
						}
					}
				}
			}else if (ENUM_SERVICE_PORT_CODE.CUSTOMER_VERIFY_ONE.code().equals(portCode)) {		// 客户验证异步上传
				CoreRequestHead coreRequestHead = new CoreRequestHead();
				List<ReqCustomerInfoDTO> customerInfoList = new ArrayList<ReqCustomerInfoDTO>();
				// 将同一Key值下的集合的报文整合成多笔请求报文
				Iterator keys = bargainMapList.keySet().iterator();
				while(keys.hasNext()){
					String key = (String) keys.next();
					List<BargainRequestInfo> tBargainRequestList = bargainMapList.get(key);
					List<AsynInteractionDTO> tAsynInteractionList = new ArrayList<AsynInteractionDTO>();
					for (BargainRequestInfo bargainRequestInfo : tBargainRequestList) {
						if(ENUM_SERVICE_PORT_CODE.CUSTOMER_VERIFY_ONE.code().equals(bargainRequestInfo.getPortCode())){
							ReqCustomerCoreDTO reqCustomerCore = JSONObject.parseObject(bargainRequestInfo.getRequestContent(), ReqCustomerCoreDTO.class);
							ReqCustomerInfoDTO customerInfo = (ReqCustomerInfoDTO)reqCustomerCore.getBody();
							customerInfoList.add(customerInfo);
							AsynInteractionDTO tAsynInteraction = new AsynInteractionDTO();
							tAsynInteraction.setSonSerialNo(bargainRequestInfo.getSerialNo());
							tAsynInteraction.setBusNo(customerInfo.getBizNo());
							tAsynInteractionList.add(tAsynInteraction);
							coreRequestHead = reqCustomerCore.getRequestHead();
						}
					}
					coreRequestHead.setRecordNum(tAsynInteractionList.size()+"");
					//这里需要注意的是：由于是将多条业务数据整合到一起，流水号需要更新
					String uuid = UUID.randomUUID().toString();
					coreRequestHead.setSerialNo(uuid);
					ReqCustomerMoreDTO reqCustomerMore = new ReqCustomerMoreDTO();
					reqCustomerMore.setClientList(customerInfoList);
					ReqCustomerMoreCoreDTO reqCustomerMoreCore = new ReqCustomerMoreCoreDTO();
					reqCustomerMoreCore.setBody(reqCustomerMore);
					reqCustomerMoreCore.setRequestHead(coreRequestHead);
					String requestJson = JSONObject.toJSONString(reqCustomerMoreCore);
					
					// 添加异步数据关联表数据,并修改子任务状态
					for(AsynInteractionDTO tAsynInteractionInfo : tAsynInteractionList){
						tAsynInteractionInfo.setMainSerialNo(uuid);
						tFaultTaskService.insertAsynInteraction(tAsynInteractionInfo);
					}
					// 为当前多笔请求任务,添加任务记录表数据，用于接收组装多笔的返回结果
					TaxFaulTaskDTO taxFaulTaskDTO = new TaxFaulTaskDTO();
					taxFaulTaskDTO.setDisposeType(ENUM_DISPOSE_TYPE.ASYNC.code());
					taxFaulTaskDTO.setAsynContent(requestJson);
					taxFaulTaskDTO.setServerPortCode(ENUM_HX_SERVICE_PORT_CODE.CUSTOMER_VERIFY_MORE.code());
					taxFaulTaskDTO.setServerPortName(ENUM_HX_SERVICE_PORT_CODE.CUSTOMER_VERIFY_MORE.desc());
					taxFaulTaskDTO.setSerialNo(uuid);
					Integer faultTaskId = tFaultTaskService.addTaxFaultTask(taxFaulTaskDTO);
					
					// 调用各自接口的异常任务方法
					TaxFaultTask faultTask = customerServiceCore.customerExceptionJob(requestJson, ENUM_HX_SERVICE_PORT_CODE.CUSTOMER_VERIFY_MORE.code(), faultTaskId);
					// 获取中保信的返回数据
					String resMainJson = faultTask.getSuccessMessage();
					// 需要更新任务表状态
					String mainSreialNo = faultTask.getSerialNo();
					// 如果中保信调用成功，则进行拆分返回数据
					if((ENUM_BZ_RESPONSE_RESULT.SUCCESS.code().equals(faultTask.getDisposeResult())) && (!ENUM_DISPOSE_TYPE.APPOINTMENT.code().equals(faultTask.getDisposeType()))){
						ThirdSendResponseDTO thirdSendResponse = JSONObject.parseObject(resMainJson, ThirdSendResponseDTO.class);
						List thirdSendResList = JSONObject.parseObject(thirdSendResponse.getResJson().toString(), List.class);
						for (Object object : thirdSendResList) {
							CustomerVerifyResultDTO customerVerifyResult = JSONObject.parseObject(object.toString(), CustomerVerifyResultDTO.class);
							List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
							for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
								// 以业务号作为关联数据
								if(taxAsynInteraction.getBusinessNo().equals(customerVerifyResult.getBizNo())){
									ThirdSendResponseDTO thirdSendResponseDTO = new ThirdSendResponseDTO();
									thirdSendResponseDTO.setResJson(customerVerifyResult);
									thirdSendResponseDTO.setResultCode(thirdSendResponse.getResultCode());
									thirdSendResponseDTO.setResultDesc("处理成功");
									String resJson = JSONObject.toJSONString(thirdSendResponseDTO);
									tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resJson, ENUM_DISPOSE_STATE.SUCCES.code());
									break;
								}
							}
						}
					}else{
						List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
						for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
							tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resMainJson, ENUM_DISPOSE_STATE.SUCCES.code());
						}
					}
				}
			}else if (ENUM_SERVICE_PORT_CODE.POLICY_STATE_CHANGE_ONE.code().equals(portCode)) {		// 保单状态变更异步上传
				CoreRequestHead coreRequestHead = new CoreRequestHead();
				List<PolicyStateEndorsement> policyStateEndorsementList = new ArrayList<PolicyStateEndorsement>();
				// 将同一Key值下的集合的报文整合成多笔请求报文
				Iterator keys = bargainMapList.keySet().iterator();
				while(keys.hasNext()){
					String key = (String) keys.next();
					List<BargainRequestInfo> tBargainRequestList = bargainMapList.get(key);
					List<AsynInteractionDTO> tAsynInteractionList = new ArrayList<AsynInteractionDTO>();
					for (BargainRequestInfo bargainRequestInfo : tBargainRequestList) {
						if(ENUM_SERVICE_PORT_CODE.POLICY_STATE_CHANGE_ONE.code().equals(bargainRequestInfo.getPortCode())){
							PolicyStateOneBody policyStateOneBody = JSONObject.parseObject(bargainRequestInfo.getRequestContent(), PolicyStateOneBody.class);
							PolicyStateEndorsement policyStateEndorsement = (PolicyStateEndorsement)policyStateOneBody.getBody();
							policyStateEndorsementList.add(policyStateEndorsement);
							AsynInteractionDTO tAsynInteraction = new AsynInteractionDTO();
							tAsynInteraction.setSonSerialNo(bargainRequestInfo.getSerialNo());
							tAsynInteraction.setBusNo(policyStateEndorsement.getBizNo());
							tAsynInteractionList.add(tAsynInteraction);
							coreRequestHead = policyStateOneBody.getRequestHead();
						}
					}
					coreRequestHead.setRecordNum(tAsynInteractionList.size()+"");
					//这里需要注意的是：由于是将多条业务数据整合到一起，流水号需要更新
					String uuid = UUID.randomUUID().toString();
					coreRequestHead.setSerialNo(uuid);
					PolicyStateMoreBody rolicyStateMoreBody = new PolicyStateMoreBody();
					rolicyStateMoreBody.setBody(policyStateEndorsementList);
					rolicyStateMoreBody.setRequestHead(coreRequestHead);
					String requestJson = JSONObject.toJSONString(rolicyStateMoreBody);
					
					// 添加异步数据关联表数据,并修改子任务状态
					for(AsynInteractionDTO tAsynInteractionInfo : tAsynInteractionList){
						tAsynInteractionInfo.setMainSerialNo(uuid);
						tFaultTaskService.insertAsynInteraction(tAsynInteractionInfo);
					}
					// 为当前多笔请求任务,添加任务记录表数据，用于接收组装多笔的返回结果
					TaxFaulTaskDTO taxFaulTaskDTO = new TaxFaulTaskDTO();
					taxFaulTaskDTO.setDisposeType(ENUM_DISPOSE_TYPE.ASYNC.code());
					taxFaulTaskDTO.setAsynContent(requestJson);
					taxFaulTaskDTO.setServerPortCode(ENUM_HX_SERVICE_PORT_CODE.POLICY_STATE_CHANGE_MORE.code());
					taxFaulTaskDTO.setServerPortName(ENUM_HX_SERVICE_PORT_CODE.POLICY_STATE_CHANGE_MORE.desc());
					taxFaulTaskDTO.setSerialNo(uuid);
					Integer faultTaskId = tFaultTaskService.addTaxFaultTask(taxFaulTaskDTO);
					
					// 调用各自接口的异常任务方法
					TaxFaultTask faultTask = policyStateChangeService.modifyPolicyStateExceptionJob(requestJson, ENUM_HX_SERVICE_PORT_CODE.POLICY_STATE_CHANGE_MORE.code(), faultTaskId);
					// 获取中保信的返回数据
					String resMainJson = faultTask.getSuccessMessage();
					// 需要更新任务表状态
					String mainSreialNo = faultTask.getSerialNo();
					// 如果中保信调用成功，则进行拆分返回数据
					if((ENUM_BZ_RESPONSE_RESULT.SUCCESS.code().equals(faultTask.getDisposeResult())) && (!ENUM_DISPOSE_TYPE.APPOINTMENT.code().equals(faultTask.getDisposeType()))){
						ThirdSendResponseDTO thirdSendResponse = JSONObject.parseObject(resMainJson, ThirdSendResponseDTO.class);
						List thirdSendResList = JSONObject.parseObject(thirdSendResponse.getResJson().toString(), List.class);
						for (Object object : thirdSendResList) {
							ResultEndorsement resultEndorsement = JSONObject.parseObject(object.toString(), ResultEndorsement.class);
							List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
							for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
								// 以业务号作为关联数据
								if(taxAsynInteraction.getBusinessNo().equals(resultEndorsement.getBizNo())){
									ThirdSendResponseDTO thirdSendResponseDTO = new ThirdSendResponseDTO();
									thirdSendResponseDTO.setResJson(resultEndorsement);
									thirdSendResponseDTO.setResultCode(thirdSendResponse.getResultCode());
									thirdSendResponseDTO.setResultDesc("处理成功");
									String resJson = JSONObject.toJSONString(thirdSendResponseDTO);
									tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resJson, ENUM_DISPOSE_STATE.SUCCES.code());
									break;
								}
							}
						}
					}else{
						List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
						for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
							tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resMainJson, ENUM_DISPOSE_STATE.SUCCES.code());
						}
					}
				}
			}else if (ENUM_SERVICE_PORT_CODE.POLICY_PREMIUM_UPLOAD_ONE.code().equals(portCode)) {		// 保单续期保费异步上传
				CoreRequestHead coreRequestHead = new CoreRequestHead();
				List<ReqRenewalPremiumInfoDTO> reqRenewalPremiumInfoList = new ArrayList<ReqRenewalPremiumInfoDTO>();
				// 将同一Key值下的集合的报文整合成多笔请求报文
				Iterator keys = bargainMapList.keySet().iterator();
				while(keys.hasNext()){
					String key = (String) keys.next();
					List<BargainRequestInfo> tBargainRequestList = bargainMapList.get(key);
					List<AsynInteractionDTO> tAsynInteractionList = new ArrayList<AsynInteractionDTO>();
					for (BargainRequestInfo bargainRequestInfo : tBargainRequestList) {
						if(ENUM_SERVICE_PORT_CODE.POLICY_PREMIUM_UPLOAD_ONE.code().equals(bargainRequestInfo.getPortCode())){
							ReqRenewalPremiumCoreDTO reqRenewalPremiumCoreDTO = JSONObject.parseObject(bargainRequestInfo.getRequestContent(), ReqRenewalPremiumCoreDTO.class);
							ReqSPremiumInfoDTO reqSPremiumInfoDTO = (ReqSPremiumInfoDTO)reqRenewalPremiumCoreDTO.getBody();
							List<ReqRenewalPremiumInfoDTO> reqSRenewalPremiumInfoList = reqSPremiumInfoDTO.getPremiumList();
							if(null != reqSRenewalPremiumInfoList && reqSRenewalPremiumInfoList.size() > 0){
								ReqRenewalPremiumInfoDTO reqRenewalPremiumInfoDTO = reqSRenewalPremiumInfoList.get(0);
								reqRenewalPremiumInfoList.add(reqRenewalPremiumInfoDTO);
								AsynInteractionDTO tAsynInteraction = new AsynInteractionDTO();
								tAsynInteraction.setSonSerialNo(bargainRequestInfo.getSerialNo());
								tAsynInteraction.setBusNo(reqRenewalPremiumInfoDTO.getBizNo());
								tAsynInteractionList.add(tAsynInteraction);
							}
							coreRequestHead = reqRenewalPremiumCoreDTO.getRequestHead();
						}
					}
					coreRequestHead.setRecordNum(tAsynInteractionList.size()+"");
					//这里需要注意的是：由于是将多条业务数据整合到一起，流水号需要更新
					String uuid = UUID.randomUUID().toString();
					coreRequestHead.setSerialNo(uuid);
					ReqMPremiumInfoDTO reqMPremiumInfoDTO = new ReqMPremiumInfoDTO();
					reqMPremiumInfoDTO.setPremiumList(reqRenewalPremiumInfoList);
					ReqMRenewalPremiumCoreDTO reqMRenewalPremiumCoreDTO = new ReqMRenewalPremiumCoreDTO();
					reqMRenewalPremiumCoreDTO.setBody(reqMPremiumInfoDTO);
					reqMRenewalPremiumCoreDTO.setRequestHead(coreRequestHead);
					String requestJson = JSONObject.toJSONString(reqMRenewalPremiumCoreDTO);
					
					// 添加异步数据关联表数据,并修改子任务状态
					for(AsynInteractionDTO tAsynInteractionInfo : tAsynInteractionList){
						tAsynInteractionInfo.setMainSerialNo(uuid);
						tFaultTaskService.insertAsynInteraction(tAsynInteractionInfo);
					}
					// 为当前多笔请求任务,添加任务记录表数据，用于接收组装多笔的返回结果
					TaxFaulTaskDTO taxFaulTaskDTO = new TaxFaulTaskDTO();
					taxFaulTaskDTO.setDisposeType(ENUM_DISPOSE_TYPE.ASYNC.code());
					taxFaulTaskDTO.setAsynContent(requestJson);
					taxFaulTaskDTO.setServerPortCode(ENUM_HX_SERVICE_PORT_CODE.POLICY_PREMIUM_UPLOAD_MORE.code());
					taxFaulTaskDTO.setServerPortName(ENUM_HX_SERVICE_PORT_CODE.POLICY_PREMIUM_UPLOAD_MORE.desc());
					taxFaulTaskDTO.setSerialNo(uuid);
					Integer faultTaskId = tFaultTaskService.addTaxFaultTask(taxFaulTaskDTO);
					
					// 调用各自接口的异常任务方法
					TaxFaultTask faultTask = premiumCoreService.premiumExceptionJob(requestJson, ENUM_HX_SERVICE_PORT_CODE.POLICY_PREMIUM_UPLOAD_MORE.code(), faultTaskId);
					// 获取中保信的返回数据
					String resMainJson = faultTask.getSuccessMessage();
					// 需要更新任务表状态
					String mainSreialNo = faultTask.getSerialNo();
					// 如果中保信调用成功，则进行拆分返回数据
					if((ENUM_BZ_RESPONSE_RESULT.SUCCESS.code().equals(faultTask.getDisposeResult())) && (!ENUM_DISPOSE_TYPE.APPOINTMENT.code().equals(faultTask.getDisposeType()))){
						ThirdSendResponseDTO thirdSendResponse = JSONObject.parseObject(resMainJson, ThirdSendResponseDTO.class);
						List thirdSendResList = JSONObject.parseObject(thirdSendResponse.getResJson().toString(), List.class);
						for (Object object : thirdSendResList) {
							ResultRenewalPremiumDTO resultRenewalPremiumDTO = JSONObject.parseObject(object.toString(), ResultRenewalPremiumDTO.class);
							List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
							for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
								// 以业务号作为关联数据
								if(taxAsynInteraction.getBusinessNo().equals(resultRenewalPremiumDTO.getBizNo())){
									ThirdSendResponseDTO thirdSendResponseDTO = new ThirdSendResponseDTO();
									thirdSendResponseDTO.setResJson(resultRenewalPremiumDTO);
									thirdSendResponseDTO.setResultCode(thirdSendResponse.getResultCode());
									thirdSendResponseDTO.setResultDesc("处理成功");
									String resJson = JSONObject.toJSONString(thirdSendResponseDTO);
									tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resJson, ENUM_DISPOSE_STATE.SUCCES.code());
									break;
								}
							}
						}
					}else{
						List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
						for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
							tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resMainJson, ENUM_DISPOSE_STATE.SUCCES.code());
						}
					}
				}
			}else if (ENUM_SERVICE_PORT_CODE.POLICY_RENEWAL_UPLOAD_ONE.code().equals(portCode)) {		// 保单续保异步上传
				CoreRequestHead coreRequestHead = new CoreRequestHead();
				List<ReqContinuePolicyInfoDTO> reqContinuePolicyInfoList = new ArrayList<ReqContinuePolicyInfoDTO>();
				// 将同一Key值下的集合的报文整合成多笔请求报文
				Iterator keys = bargainMapList.keySet().iterator();
				while(keys.hasNext()){
					String key = (String) keys.next();
					List<BargainRequestInfo> tBargainRequestList = bargainMapList.get(key);
					List<AsynInteractionDTO> tAsynInteractionList = new ArrayList<AsynInteractionDTO>();
					for (BargainRequestInfo bargainRequestInfo : tBargainRequestList) {
						if(ENUM_SERVICE_PORT_CODE.POLICY_RENEWAL_UPLOAD_ONE.code().equals(bargainRequestInfo.getPortCode())){
							ReqContinuePolicyInfoCoreDTO reqContinuePolicyInfoCore = JSONObject.parseObject(bargainRequestInfo.getRequestContent(), ReqContinuePolicyInfoCoreDTO.class);
							ReqContinuePolicyInfoDTO reqContinuePolicyInfo = (ReqContinuePolicyInfoDTO)reqContinuePolicyInfoCore.getBody();
							reqContinuePolicyInfoList.add(reqContinuePolicyInfo);
							AsynInteractionDTO tAsynInteraction = new AsynInteractionDTO();
							tAsynInteraction.setSonSerialNo(bargainRequestInfo.getSerialNo());
							tAsynInteraction.setBusNo(reqContinuePolicyInfo.getBizNo());
							tAsynInteractionList.add(tAsynInteraction);
							coreRequestHead = reqContinuePolicyInfoCore.getRequestHead();
						}
					}
					coreRequestHead.setRecordNum(tAsynInteractionList.size()+"");
					//这里需要注意的是：由于是将多条业务数据整合到一起，流水号需要更新
					String uuid = UUID.randomUUID().toString();
					coreRequestHead.setSerialNo(uuid);
					ReqMContinuePolicyInfoDTO reqMContinuePolicyInfoDTO = new ReqMContinuePolicyInfoDTO();
					reqMContinuePolicyInfoDTO.setPolicyList(reqContinuePolicyInfoList);
					ReqMContinuePolicyInfoCoreDTO reqMContinuePolicyInfoCoreDTO = new ReqMContinuePolicyInfoCoreDTO();
					reqMContinuePolicyInfoCoreDTO.setBody(reqMContinuePolicyInfoDTO);
					reqMContinuePolicyInfoCoreDTO.setRequestHead(coreRequestHead);
					String requestJson = JSONObject.toJSONString(reqMContinuePolicyInfoCoreDTO);
					
					// 添加异步数据关联表数据,并修改子任务状态
					for(AsynInteractionDTO tAsynInteractionInfo : tAsynInteractionList){
						tAsynInteractionInfo.setMainSerialNo(uuid);
						tFaultTaskService.insertAsynInteraction(tAsynInteractionInfo);
					}
					// 为当前多笔请求任务,添加任务记录表数据，用于接收组装多笔的返回结果
					TaxFaulTaskDTO taxFaulTaskDTO = new TaxFaulTaskDTO();
					taxFaulTaskDTO.setDisposeType(ENUM_DISPOSE_TYPE.ASYNC.code());
					taxFaulTaskDTO.setAsynContent(requestJson);
					taxFaulTaskDTO.setServerPortCode(ENUM_HX_SERVICE_PORT_CODE.POLICY_RENEWAL_UPLOAD_MORE.code());
					taxFaulTaskDTO.setServerPortName(ENUM_HX_SERVICE_PORT_CODE.POLICY_RENEWAL_UPLOAD_MORE.desc());
					taxFaulTaskDTO.setSerialNo(uuid);
					Integer faultTaskId = tFaultTaskService.addTaxFaultTask(taxFaulTaskDTO);
					
					// 调用各自接口的异常任务方法
					TaxFaultTask faultTask = continuePolicyCoreService.continuePolicyExceptionJob(requestJson, ENUM_HX_SERVICE_PORT_CODE.POLICY_RENEWAL_UPLOAD_MORE.code(), faultTaskId);
					// 获取中保信的返回数据
					String resMainJson = faultTask.getSuccessMessage();
					// 需要更新任务表状态
					String mainSreialNo = faultTask.getSerialNo();
					// 如果中保信调用成功，则进行拆分返回数据
					if((ENUM_BZ_RESPONSE_RESULT.SUCCESS.code().equals(faultTask.getDisposeResult())) && (!ENUM_DISPOSE_TYPE.APPOINTMENT.code().equals(faultTask.getDisposeType()))){
						ThirdSendResponseDTO thirdSendResponse = JSONObject.parseObject(resMainJson, ThirdSendResponseDTO.class);
						List thirdSendResList = JSONObject.parseObject(thirdSendResponse.getResJson().toString(), List.class);
						for (Object object : thirdSendResList) {
							ResultContinuePolicyDTO resultContinuePolicy = JSONObject.parseObject(object.toString(), ResultContinuePolicyDTO.class);
							List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
							for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
								// 以业务号作为关联数据
								if(taxAsynInteraction.getBusinessNo().equals(resultContinuePolicy.getBizNo())){
									ThirdSendResponseDTO thirdSendResponseDTO = new ThirdSendResponseDTO();
									thirdSendResponseDTO.setResJson(resultContinuePolicy);
									thirdSendResponseDTO.setResultCode(thirdSendResponse.getResultCode());
									thirdSendResponseDTO.setResultDesc("处理成功");
									String resJson = JSONObject.toJSONString(thirdSendResponseDTO);
									tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resJson, ENUM_DISPOSE_STATE.SUCCES.code());
									break;
								}
							}
						}
					}else{
						List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
						for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
							tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resMainJson, ENUM_DISPOSE_STATE.SUCCES.code());
						}
					}
				}
			}else if (ENUM_SERVICE_PORT_CODE.CLAIM_SETTLEMEN_ONE.code().equals(portCode)) {		// 保单理赔异步上传
				CoreRequestHead coreRequestHead = new CoreRequestHead();
				List<ReqClaimInfoDTO> reqClaimInfoDTOList = new ArrayList<ReqClaimInfoDTO>();
				// 将同一Key值下的集合的报文整合成多笔请求报文
				Iterator keys = bargainMapList.keySet().iterator();
				while(keys.hasNext()){
					String key = (String) keys.next();
					List<BargainRequestInfo> tBargainRequestList = bargainMapList.get(key);
					List<AsynInteractionDTO> tAsynInteractionList = new ArrayList<AsynInteractionDTO>();
					for (BargainRequestInfo bargainRequestInfo : tBargainRequestList) {
						if(ENUM_SERVICE_PORT_CODE.CLAIM_SETTLEMEN_ONE.code().equals(bargainRequestInfo.getPortCode())){
							ReqClaimCoreDTO reqClaimCoreDTO = JSONObject.parseObject(bargainRequestInfo.getRequestContent(), ReqClaimCoreDTO.class);
							ReqClaimInfoDTO reqClaimInfoDTO = (ReqClaimInfoDTO)reqClaimCoreDTO.getBody();
							reqClaimInfoDTOList.add(reqClaimInfoDTO);
							AsynInteractionDTO tAsynInteraction = new AsynInteractionDTO();
							tAsynInteraction.setSonSerialNo(bargainRequestInfo.getSerialNo());
							tAsynInteraction.setBusNo(reqClaimInfoDTO.getBizNo());
							tAsynInteractionList.add(tAsynInteraction);
							coreRequestHead = reqClaimCoreDTO.getRequestHead();
						}
					}
					coreRequestHead.setRecordNum(tAsynInteractionList.size()+"");
					//这里需要注意的是：由于是将多条业务数据整合到一起，流水号需要更新
					String uuid = UUID.randomUUID().toString();
					coreRequestHead.setSerialNo(uuid);
					ReqClaimCoreMDTO reqClaimCoreMDTO = new ReqClaimCoreMDTO();
					reqClaimCoreMDTO.setBody(reqClaimInfoDTOList);
					reqClaimCoreMDTO.setRequestHead(coreRequestHead);
					String requestJson = JSONObject.toJSONString(reqClaimCoreMDTO);
					
					// 添加异步数据关联表数据,并修改子任务状态
					for(AsynInteractionDTO tAsynInteractionInfo : tAsynInteractionList){
						tAsynInteractionInfo.setMainSerialNo(uuid);
						tFaultTaskService.insertAsynInteraction(tAsynInteractionInfo);
					}
					// 为当前多笔请求任务,添加任务记录表数据，用于接收组装多笔的返回结果
					TaxFaulTaskDTO taxFaulTaskDTO = new TaxFaulTaskDTO();
					taxFaulTaskDTO.setDisposeType(ENUM_DISPOSE_TYPE.ASYNC.code());
					taxFaulTaskDTO.setAsynContent(requestJson);
					taxFaulTaskDTO.setServerPortCode(ENUM_HX_SERVICE_PORT_CODE.CLAIM_SETTLEMEN_MORE.code());
					taxFaulTaskDTO.setServerPortName(ENUM_HX_SERVICE_PORT_CODE.CLAIM_SETTLEMEN_MORE.desc());
					taxFaulTaskDTO.setSerialNo(uuid);
					Integer faultTaskId = tFaultTaskService.addTaxFaultTask(taxFaulTaskDTO);
					
					// 调用各自接口的异常任务方法
					TaxFaultTask faultTask = claimInfoCoreService.addClaimSettlementExceptionJob(requestJson, ENUM_HX_SERVICE_PORT_CODE.CLAIM_SETTLEMEN_MORE.code(), faultTaskId);
					// 获取中保信的返回数据
					String resMainJson = faultTask.getSuccessMessage();
					// 需要更新任务表状态
					String mainSreialNo = faultTask.getSerialNo();
					// 如果中保信调用成功，则进行拆分返回数据
					if((ENUM_BZ_RESPONSE_RESULT.SUCCESS.code().equals(faultTask.getDisposeResult())) && (!ENUM_DISPOSE_TYPE.APPOINTMENT.code().equals(faultTask.getDisposeType()))){
						ThirdSendResponseDTO thirdSendResponse = JSONObject.parseObject(resMainJson, ThirdSendResponseDTO.class);
						List thirdSendResList = JSONObject.parseObject(thirdSendResponse.getResJson().toString(), List.class);
						for (Object object : thirdSendResList) {
							ResultInfoDTO resultInfoDTO = JSONObject.parseObject(object.toString(), ResultInfoDTO.class);
							List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
							for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
								// 以业务号作为关联数据
								if(taxAsynInteraction.getBusinessNo().equals(resultInfoDTO.getBizNo())){
									ThirdSendResponseDTO thirdSendResponseDTO = new ThirdSendResponseDTO();
									thirdSendResponseDTO.setResJson(resultInfoDTO);
									thirdSendResponseDTO.setResultCode(thirdSendResponse.getResultCode());
									thirdSendResponseDTO.setResultDesc("处理成功");
									String resJson = JSONObject.toJSONString(thirdSendResponseDTO);
									tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resJson, ENUM_DISPOSE_STATE.SUCCES.code());
									break;
								}
							}
						}
					}else{
						List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
						for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
							tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resMainJson, ENUM_DISPOSE_STATE.SUCCES.code());
						}
					}
				}
			}else if (ENUM_SERVICE_PORT_CODE.ACCONUT_CHANGE_ONE.code().equals(portCode)) {		// 账户变更异步上传
				CoreRequestHead coreRequestHead = new CoreRequestHead();
				List<ReqAccountInfoDTO> reqAccountInfoDTOList = new ArrayList<ReqAccountInfoDTO>();
				// 将同一Key值下的集合的报文整合成多笔请求报文
				Iterator keys = bargainMapList.keySet().iterator();
				while(keys.hasNext()){
					String key = (String) keys.next();
					List<BargainRequestInfo> tBargainRequestList = bargainMapList.get(key);
					List<AsynInteractionDTO> tAsynInteractionList = new ArrayList<AsynInteractionDTO>();
					for (BargainRequestInfo bargainRequestInfo : tBargainRequestList) {
						if(ENUM_SERVICE_PORT_CODE.ACCONUT_CHANGE_ONE.code().equals(bargainRequestInfo.getPortCode())){
							ReqAccountCoreDTO reqAccountCoreDTO = JSONObject.parseObject(bargainRequestInfo.getRequestContent(), ReqAccountCoreDTO.class);
							ReqAccountInfoDTO reqAccountInfoDTO = (ReqAccountInfoDTO)reqAccountCoreDTO.getBody();
							reqAccountInfoDTOList.add(reqAccountInfoDTO);
							AsynInteractionDTO tAsynInteraction = new AsynInteractionDTO();
							tAsynInteraction.setSonSerialNo(bargainRequestInfo.getSerialNo());
							tAsynInteraction.setBusNo(reqAccountInfoDTO.getBizNo());
							tAsynInteractionList.add(tAsynInteraction);
							coreRequestHead = reqAccountCoreDTO.getRequestHead();
						}
					}
					coreRequestHead.setRecordNum(tAsynInteractionList.size()+"");
					//这里需要注意的是：由于是将多条业务数据整合到一起，流水号需要更新
					String uuid = UUID.randomUUID().toString();
					coreRequestHead.setSerialNo(uuid);
					ReqAccountMoreChangeDTO reqAccountMoreChangeDTO = new ReqAccountMoreChangeDTO();
					reqAccountMoreChangeDTO.setSavingAccountFeeList(reqAccountInfoDTOList);
					ReqAccountMoreChangeCoreDTO ReqAccountMoreChangeCoreDTO = new ReqAccountMoreChangeCoreDTO();
					ReqAccountMoreChangeCoreDTO.setBody(reqAccountMoreChangeDTO);
					ReqAccountMoreChangeCoreDTO.setRequestHead(coreRequestHead);
					String requestJson = JSONObject.toJSONString(ReqAccountMoreChangeCoreDTO);
					
					// 添加异步数据关联表数据,并修改子任务状态
					for(AsynInteractionDTO tAsynInteractionInfo : tAsynInteractionList){
						tAsynInteractionInfo.setMainSerialNo(uuid);
						tFaultTaskService.insertAsynInteraction(tAsynInteractionInfo);
					}
					// 为当前多笔请求任务,添加任务记录表数据，用于接收组装多笔的返回结果
					TaxFaulTaskDTO taxFaulTaskDTO = new TaxFaulTaskDTO();
					taxFaulTaskDTO.setDisposeType(ENUM_DISPOSE_TYPE.ASYNC.code());
					taxFaulTaskDTO.setAsynContent(requestJson);
					taxFaulTaskDTO.setServerPortCode(ENUM_HX_SERVICE_PORT_CODE.ACCONUT_CHANGE_MORE.code());
					taxFaulTaskDTO.setServerPortName(ENUM_HX_SERVICE_PORT_CODE.ACCONUT_CHANGE_MORE.desc());
					taxFaulTaskDTO.setSerialNo(uuid);
					Integer faultTaskId = tFaultTaskService.addTaxFaultTask(taxFaulTaskDTO);
					
					// 调用各自接口的异常任务方法
					TaxFaultTask faultTask = accountChangeService.accountChangeExceptionJob(requestJson, ENUM_HX_SERVICE_PORT_CODE.ACCONUT_CHANGE_MORE.code(), faultTaskId);
					// 获取中保信的返回数据
					String resMainJson = faultTask.getSuccessMessage();
					// 需要更新任务表状态
					String mainSreialNo = faultTask.getSerialNo();
					// 如果中保信调用成功，则进行拆分返回数据
					if((ENUM_BZ_RESPONSE_RESULT.SUCCESS.code().equals(faultTask.getDisposeResult())) && (!ENUM_DISPOSE_TYPE.APPOINTMENT.code().equals(faultTask.getDisposeType()))){
						ThirdSendResponseDTO thirdSendResponse = JSONObject.parseObject(resMainJson, ThirdSendResponseDTO.class);
						List thirdSendResList = JSONObject.parseObject(thirdSendResponse.getResJson().toString(), List.class);
						for (Object object : thirdSendResList) {
							AccountChangeResultDTO accountChangeResultDTO = JSONObject.parseObject(object.toString(), AccountChangeResultDTO.class);
							List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
							for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
								// 以业务号作为关联数据
								if(taxAsynInteraction.getBusinessNo().equals(accountChangeResultDTO.getBizNo())){
									ThirdSendResponseDTO thirdSendResponseDTO = new ThirdSendResponseDTO();
									thirdSendResponseDTO.setResJson(accountChangeResultDTO);
									thirdSendResponseDTO.setResultCode(thirdSendResponse.getResultCode());
									thirdSendResponseDTO.setResultDesc("处理成功");
									String resJson = JSONObject.toJSONString(thirdSendResponseDTO);
									tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resJson, ENUM_DISPOSE_STATE.SUCCES.code());
									break;
								}
							}
						}
					}else{
						List<TaxAsynInteraction> tTaxAsynInteractionList = tFaultTaskService.selectTaxAsynInteractionByMainSreialNo(mainSreialNo);
						for (TaxAsynInteraction taxAsynInteraction : tTaxAsynInteractionList) {
							tFaultTaskService.updateFaultTaskResMessageBySerialNo(taxAsynInteraction.getSonSerialNo(), resMainJson, ENUM_DISPOSE_STATE.SUCCES.code());
						}
					}
				}
			}
		} catch (Exception e) {
			logger.info("执行多笔中保信异步请求出错!");
			e.printStackTrace();
		}
	}
}
