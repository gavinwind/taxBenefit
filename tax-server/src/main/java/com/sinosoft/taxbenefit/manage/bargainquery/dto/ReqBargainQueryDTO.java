package com.sinosoft.taxbenefit.manage.bargainquery.dto;

import java.math.BigDecimal;

/**
 * 中保信-交易核对查询请求DTO
 * @author yangdongkai@outlook.com
 * @date 2016年3月28日 下午3:00:04
 */
public class ReqBargainQueryDTO {
	// 指定核对日期
	private String checkDate;
	// 保险公司请求总记录数
	private BigDecimal requestTotalNum;
	// 保险公司成功记录数
	private BigDecimal comSuccessNum;
	// 保险公司失败记录数
	private BigDecimal comFailNum;
	/**
	 * @return the checkDate
	 */
	public String getCheckDate() {
		return checkDate;
	}
	/**
	 * @param checkDate the checkDate to set
	 */
	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}
	/**
	 * @return the requestTotalNum
	 */
	public BigDecimal getRequestTotalNum() {
		return requestTotalNum;
	}
	/**
	 * @param requestTotalNum the requestTotalNum to set
	 */
	public void setRequestTotalNum(BigDecimal requestTotalNum) {
		this.requestTotalNum = requestTotalNum;
	}
	/**
	 * @return the comSuccessNum
	 */
	public BigDecimal getComSuccessNum() {
		return comSuccessNum;
	}
	/**
	 * @param comSuccessNum the comSuccessNum to set
	 */
	public void setComSuccessNum(BigDecimal comSuccessNum) {
		this.comSuccessNum = comSuccessNum;
	}
	/**
	 * @return the comFailNum
	 */
	public BigDecimal getComFailNum() {
		return comFailNum;
	}
	/**
	 * @param comFailNum the comFailNum to set
	 */
	public void setComFailNum(BigDecimal comFailNum) {
		this.comFailNum = comFailNum;
	}
}