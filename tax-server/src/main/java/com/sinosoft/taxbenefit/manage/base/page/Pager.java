package com.sinosoft.taxbenefit.manage.base.page;

/**
 * 分页参数
 * 
 * @version 2.0
 */
// oracle,sqlserver,mysql分页技术
public class Pager {

	private Integer pageId = 1; // 当前页
	private Integer rowCount = 0; // 总行数
	private Integer pageSize = 2; // 页大小
	private Integer pageCount = 0; // 总页数
	private Integer pageOffset = 0;// 当前页起始记录
	private Integer pageTail = 0;// 当前页到达的记录

	private String orderField;
	private boolean orderDirection;

	// 页面显示分页按钮个数
	private Integer length = 6;

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public Pager() {
		this.orderDirection = true;
	}

	public String getOrderCondition() {
		String condition = "";
		if (this.orderField != null && this.orderField.length() != 0) {
			condition = " order by " + orderField
					+ (orderDirection ? " " : " desc ");
		}
		return condition;
	}

	public void setOrderDirection(boolean orderDirection) {
		this.orderDirection = orderDirection;
	}

	public boolean isOrderDirection() {
		return orderDirection;
	}

	public void setOrderField(String orderField) {
		this.orderField = orderField;
	}

	public String getOrderField() {
		return orderField;
	}

	public void setPageCount(Integer pageCount) {
		this.pageCount = pageCount;
	}

	public Integer getPageCount() {
		return pageCount;
	}

	public void setPageId(Integer pageId) {
		this.pageId = pageId;
	}

	public Integer getPageId() {
		return pageId;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setRowCount(Integer rowCount) {
		if (rowCount > 0) {
			this.rowCount = rowCount;
			pageCount = (this.rowCount + pageSize - 1) / pageSize;
		}
	}

	public Integer getRowCount() {
		return rowCount;
	}

	public Integer getPageOffset() {
		return pageOffset;
	}

	public void setPageOffset(Integer pageOffset) {
		this.pageOffset = pageOffset;
	}

	public Integer getPageTail() {
		return pageTail;
	}

	public void setPageTail(Integer pageTail) {
		this.pageTail = pageTail;
	}

}