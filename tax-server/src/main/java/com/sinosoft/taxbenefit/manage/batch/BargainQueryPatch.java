package com.sinosoft.taxbenefit.manage.batch;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.lowagie.text.DocumentException;
import com.sinosoft.platform.base.TaxBaseJob;
import com.sinosoft.platform.common.util.DateUtil;
import com.sinosoft.taxbenefit.core.common.config.ENUM_RC_STATE;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxRequestBusinessCollateMapper;
import com.sinosoft.taxbenefit.core.generated.model.TaxRequestBusinessCollate;
import com.sinosoft.taxbenefit.core.generated.model.TaxRequestBusinessCollateExample;
import com.sinosoft.taxbenefit.manage.bargainquery.biz.service.BargainQueryService;
import com.sinosoft.taxbenefit.manage.bargainquery.dto.ReqBargainQueryDTO;
/**
 * 交易核对信息查询补处理
 * @author yangdongkai@outlook.com
 * @date 2016年3月31日 下午4:09:51
 */
@Repository
public class BargainQueryPatch extends TaxBaseJob {
	@Autowired
	TaxRequestBusinessCollateMapper taxRequestBusinessCollateMapper;
	@Autowired
	BargainQueryService bargainQueryService;
	
	@Override
	public void executeJob() throws IOException, DocumentException, MessagingException {
		logger.info("=======================交易核对信息查询补处理开始！=======================");
		//处理处理失败的交易记录
		TaxRequestBusinessCollateExample example = new TaxRequestBusinessCollateExample();
		example.createCriteria().andSuccessFlagEqualTo(ENUM_RC_STATE.FAIL.getCode());
		List<TaxRequestBusinessCollate> businessCollateList = taxRequestBusinessCollateMapper.selectByExample(example);
		for (TaxRequestBusinessCollate taxRequestBusinessCollate : businessCollateList) {
			try {
				Date date = taxRequestBusinessCollate.getBusinessDate();
				String dateStr = DateUtil.getDateStr(date, "yyyy-MM-dd");
				ReqBargainQueryDTO reqBargainQueryDTO = new ReqBargainQueryDTO();
				Integer bid = taxRequestBusinessCollate.getSid();
				reqBargainQueryDTO.setCheckDate(dateStr);
				reqBargainQueryDTO.setComFailNum(new BigDecimal(taxRequestBusinessCollate.getReqFailNum()));
				reqBargainQueryDTO.setComSuccessNum(new BigDecimal(taxRequestBusinessCollate.getReqSuccessNum()));
				reqBargainQueryDTO.setRequestTotalNum(new BigDecimal(taxRequestBusinessCollate.getRequetNum()));
				bargainQueryService.BargainQuery(reqBargainQueryDTO, bid);
			} catch (Exception e) {
				logger.error("交易核对信息查询补处理失败！"+e.getMessage());
				e.printStackTrace();
			}
		}
		logger.info("=======================交易核对信息查询补处理结束！=======================");
	}
}
