package com.sinosoft.taxbenefit.manage.exceptionJob.mapper;

import java.util.List;

import com.sinosoft.taxbenefit.core.generated.model.TaxFaultTask;

public interface QueryFailJobMapper {

	/**
	 * 异常异步/待处理处理失败的任务记录
	 * @return
	 */
	public List<TaxFaultTask> selectExceptionJobList();
	
	/**
	 * 预约码/待处理处理失败的任务记录
	 * @return
	 */
	public List<TaxFaultTask> selectAppointmentJobList();
	
	public List<TaxFaultTask> queryFailJoblist();
}
