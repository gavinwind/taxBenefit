package com.sinosoft.taxbenefit.manage.bargainquery.dto;
/**
 * 交易核对查询中保信返回核对明细DTO
 * Title:ResBargainQueryDetailDTO
 * @author yangdongkai@outlook.com
 * @date 2016年3月28日 下午3:21:50
 */
public class ResBargainQueryDetailDTO {
	// 核对请求类型
	private String checkTransType;
	// 交易时间
	private String transDate;
	// 保单号码
	private String policyNo;
	// 分单号码
	private String sequenceNo;
	// 保全批单号
	private String endorsementNo;
	// 理赔赔案号
	private String claimNo;
	// 费用编码
	private String feeId;
	// 保费状态
	private String feeStatus;
	// 续保批单号
	private String renewalEndorsementNo;
	// 公司费用 ID
	private String comFeeId;
	// 返回结果
	private String returnCode;
	// 失败原因
	private String errorMessage;
	/**
	 * @return the checkTransType
	 */
	public String getCheckTransType() {
		return checkTransType;
	}
	/**
	 * @param checkTransType the checkTransType to set
	 */
	public void setCheckTransType(String checkTransType) {
		this.checkTransType = checkTransType;
	}
	/**
	 * @return the transDate
	 */
	public String getTransDate() {
		return transDate;
	}
	/**
	 * @param transDate the transDate to set
	 */
	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}
	/**
	 * @return the policyNo
	 */
	public String getPolicyNo() {
		return policyNo;
	}
	/**
	 * @param policyNo the policyNo to set
	 */
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	/**
	 * @return the sequenceNo
	 */
	public String getSequenceNo() {
		return sequenceNo;
	}
	/**
	 * @param sequenceNo the sequenceNo to set
	 */
	public void setSequenceNo(String sequenceNo) {
		this.sequenceNo = sequenceNo;
	}
	/**
	 * @return the endorsementNo
	 */
	public String getEndorsementNo() {
		return endorsementNo;
	}
	/**
	 * @param endorsementNo the endorsementNo to set
	 */
	public void setEndorsementNo(String endorsementNo) {
		this.endorsementNo = endorsementNo;
	}
	/**
	 * @return the claimNo
	 */
	public String getClaimNo() {
		return claimNo;
	}
	/**
	 * @param claimNo the claimNo to set
	 */
	public void setClaimNo(String claimNo) {
		this.claimNo = claimNo;
	}
	/**
	 * @return the feeId
	 */
	public String getFeeId() {
		return feeId;
	}
	/**
	 * @param feeId the feeId to set
	 */
	public void setFeeId(String feeId) {
		this.feeId = feeId;
	}
	/**
	 * @return the feeStatus
	 */
	public String getFeeStatus() {
		return feeStatus;
	}
	/**
	 * @param feeStatus the feeStatus to set
	 */
	public void setFeeStatus(String feeStatus) {
		this.feeStatus = feeStatus;
	}
	/**
	 * @return the renewalEndorsementNo
	 */
	public String getRenewalEndorsementNo() {
		return renewalEndorsementNo;
	}
	/**
	 * @param renewalEndorsementNo the renewalEndorsementNo to set
	 */
	public void setRenewalEndorsementNo(String renewalEndorsementNo) {
		this.renewalEndorsementNo = renewalEndorsementNo;
	}
	/**
	 * @return the comFeeId
	 */
	public String getComFeeId() {
		return comFeeId;
	}
	/**
	 * @param comFeeId the comFeeId to set
	 */
	public void setComFeeId(String comFeeId) {
		this.comFeeId = comFeeId;
	}
	/**
	 * @return the returnCode
	 */
	public String getReturnCode() {
		return returnCode;
	}
	/**
	 * @param returnCode the returnCode to set
	 */
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}
	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
