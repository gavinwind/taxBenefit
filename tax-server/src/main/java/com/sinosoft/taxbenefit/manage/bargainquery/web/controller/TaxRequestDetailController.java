package com.sinosoft.taxbenefit.manage.bargainquery.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.sinosoft.taxbenefit.core.generated.model.TaxBusinessCollateDetail;
import com.sinosoft.taxbenefit.core.generated.model.TaxRequestBusinessDetail;
import com.sinosoft.taxbenefit.manage.bargainquery.biz.service.BargainQueryService;
import com.sinosoft.taxbenefit.manage.bargainquery.dto.BusinessDetailDTO;
import com.sinosoft.taxbenefit.manage.bargainquery.web.util.BargainQueryViewConstant;
import com.sinosoft.taxbenefit.manage.base.util.HtmlUtil;
import com.sinosoft.taxbenefit.manage.base.web.BaseAction;
 
/**
 * 请求记录明细
 * @author zhangke
 *
 */
@Controller
@RequestMapping("/manage/taxRequestDetail") 
public class TaxRequestDetailController extends BaseAction{
	
	// Servrice start
	@Autowired(required=false) //自动注入，不需要生成set方法了，required=false表示没有实现类，也不会报错。
	private BargainQueryService bargainQueryService; 
	/**
	 * 
	 * @param url
	 * @param classifyId
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/list.do") 
	public ModelAndView  list(BusinessDetailDTO page,HttpServletRequest request) throws Exception{
		Map<String,Object>  context = getRootMap();
		logger.info("进入请求记录明细页面！/manage/taxRequestDetail/list.do");
		context.put("businessDate", page.getBusinessDate());
		return forword(BargainQueryViewConstant.BUSINESS_DETAIL_VIEW,context); 
	}
	/**
	 * ilook 首页
	 * @param url
	 * @param classifyId
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/detailList.do") 
	public void  datalist(BusinessDetailDTO page,HttpServletResponse response) throws Exception{
		logger.info("获取请求记录明细列表！/manage/taxRequestDetail/dataList.do");
		List<TaxRequestBusinessDetail> dataList = bargainQueryService.queryByBusinessDate(page);
		//设置页面数据
		Map<String,Object> jsonMap = new HashMap<String,Object>();
		jsonMap.put("total",page.getPager().getRowCount());
		jsonMap.put("rows", dataList);
		HtmlUtil.writerJson(response, jsonMap);
	}
}