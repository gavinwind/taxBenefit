package com.sinosoft.taxbenefit.manage.transCode.dto;

import com.sinosoft.taxbenefit.manage.base.page.BasePage;

/**
 * 页面查询转码DTO
 * 
 * @author zhangke
 *
 */
public class TransCodeModelDTO extends BasePage {
	private Integer sid;// 主键id
	private String codeType;// 转码类型
	private String originalCode;// 核心编码
	private String originalDesc;// 核心编码说明
	private String targetCode;// 中保信编码
	private String targetDesc;// 中保信编码说明
	private String remark;// 转码类型名称

	public String getCodeType() {
		return codeType;
	}

	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}

	public Integer getSid() {
		return sid;
	}

	public void setSid(Integer sid) {
		this.sid = sid;
	}

	public String getOriginalCode() {
		return originalCode;
	}

	public void setOriginalCode(String originalCode) {
		this.originalCode = originalCode;
	}

	public String getOriginalDesc() {
		return originalDesc;
	}

	public void setOriginalDesc(String originalDesc) {
		this.originalDesc = originalDesc;
	}

	public String getTargetCode() {
		return targetCode;
	}

	public void setTargetCode(String targetCode) {
		this.targetCode = targetCode;
	}

	public String getTargetDesc() {
		return targetDesc;
	}

	public void setTargetDesc(String targetDesc) {
		this.targetDesc = targetDesc;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
