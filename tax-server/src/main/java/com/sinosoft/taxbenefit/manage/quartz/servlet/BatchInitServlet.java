package com.sinosoft.taxbenefit.manage.quartz.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.sinosoft.platform.common.context.SpringContext;
import com.sinosoft.taxbenefit.manage.quartz.api.service.QuartzService;
/**
 * 启动应用批处理servlet
 * @author SheChunMing
 */
public class BatchInitServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static Logger logger = Logger.getLogger(BatchInitServlet.class.getName());

	private SpringContext springContext;
	
	public void init() {
		logger.info("批处理加载开始");

		try {
			super.init();
			springContext= (SpringContext) WebApplicationContextUtils
	                .getRequiredWebApplicationContext(getServletContext()).getBean(
	                        "springContext");
			// 取spring容器中的任务调度服务bean
			QuartzService quartzService = (QuartzService) springContext.getBean("quartzServiceImpl");

			// 调用service初始化批处理
			quartzService.initScheduler();
		} catch (Exception e) {
			logger.info("批处理加载失败，请手动加载！\n" + e);
		}

		logger.info("批处理加载结束");
	}

	/**
	 * 重新调度批处理任务
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {

		logger.info("批处理重新调度开始");

		try {
			// 取spring容器中的任务调度服务bean
			QuartzService quartzService = (QuartzService) springContext.getBean("quartzServiceImpl");
			
			// 重新调度任务
			quartzService.reScheduler();
		} catch (Exception e) {
			logger.info("批处理调度失败！\n" , e);
		}

		logger.info("批处理重新调度结束");
	}

	/**
	 * 重新调度批处理任务
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {

		this.doGet(request, response);
	}
}
