package com.sinosoft.taxbenefit.manage.bargainquery.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.sinosoft.taxbenefit.core.generated.model.TaxRequestBusinessCollate;
import com.sinosoft.taxbenefit.manage.bargainquery.biz.service.BargainQueryService;
import com.sinosoft.taxbenefit.manage.bargainquery.dto.BargainQueryDTO;
import com.sinosoft.taxbenefit.manage.bargainquery.web.util.BargainQueryViewConstant;
import com.sinosoft.taxbenefit.manage.base.util.HtmlUtil;
import com.sinosoft.taxbenefit.manage.base.web.BaseAction;
 
/**
 * 
 * <br>
 * <b>功能：</b>TaxRequestBusinessCollateController<br>
 * <b>作者：</b>www.jeecg.org<br>
 * <b>日期：</b> Feb 2, 2013 <br>
 * <b>版权所有：<b>版权所有(C) 2013，www.jeecg.org<br>
 */ 
@Controller
@RequestMapping("/manage/taxRequestBusinessCollate") 
public class TaxRequestBusinessCollateController extends BaseAction{
	
	// Servrice start
	@Autowired(required=false) //自动注入，不需要生成set方法了，required=false表示没有实现类，也不会报错。
	private BargainQueryService bargainQueryService; 
	/**
	 * 
	 * @param url
	 * @param classifyId
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/list.do") 
	public ModelAndView  list(BargainQueryDTO page,HttpServletRequest request) throws Exception{
		Map<String,Object>  context = getRootMap();
		logger.info("进入交易核对查询页面！/manage/taxRequestBusinessCollate/list.do");
		return forword(BargainQueryViewConstant.BARGAIN_QUERY_VIEW,context); 
	}
	/**
	 * ilook 首页
	 * @param url
	 * @param classifyId
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/dataList.do") 
	public void  datalist(BargainQueryDTO page,HttpServletResponse response) throws Exception{
		logger.info("进入交易核对分页查询！/manage/taxRequestBusinessCollate/dataList.do");
		List<TaxRequestBusinessCollate> dataList = bargainQueryService.queryByList(page);
		//设置页面数据
		Map<String,Object> jsonMap = new HashMap<String,Object>();
		jsonMap.put("total",page.getPager().getRowCount());
		jsonMap.put("rows", dataList);
		HtmlUtil.writerJson(response, jsonMap);
	}	
}