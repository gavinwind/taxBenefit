package com.sinosoft.taxbenefit.manage.bargainquery.dto;

import java.util.Date;
/**
 * 查询交易记录返回DTO
 * @author yangdongkai@outlook.com
 * @date 2016年3月30日 下午5:48:01
 */
public class RequestBusinessCollateDTO {
	//SID
	private Integer busid;
	//交易时间
	private Date businessDate;
	//保险公司请求总记录数
	private Integer requetNum;
	//保险公司请求成功记录数
	private Integer reqSuccessNum;
	//保险公司请求失败记录数
	private Integer reqFailNum;
	/**
	 * @return the busid
	 */
	public Integer getBusid() {
		return busid;
	}
	/**
	 * @param busid the busid to set
	 */
	public void setBusid(Integer busid) {
		this.busid = busid;
	}
	/**
	 * @return the businessDate
	 */
	public Date getBusinessDate() {
		return businessDate;
	}
	/**
	 * @param businessDate the businessDate to set
	 */
	public void setBusinessDate(Date businessDate) {
		this.businessDate = businessDate;
	}
	/**
	 * @return the requetNum
	 */
	public Integer getRequetNum() {
		return requetNum;
	}
	/**
	 * @param requetNum the requetNum to set
	 */
	public void setRequetNum(Integer requetNum) {
		this.requetNum = requetNum;
	}
	/**
	 * @return the reqSuccessNum
	 */
	public Integer getReqSuccessNum() {
		return reqSuccessNum;
	}
	/**
	 * @param reqSuccessNum the reqSuccessNum to set
	 */
	public void setReqSuccessNum(Integer reqSuccessNum) {
		this.reqSuccessNum = reqSuccessNum;
	}
	/**
	 * @return the reqFailNum
	 */
	public Integer getReqFailNum() {
		return reqFailNum;
	}
	/**
	 * @param reqFailNum the reqFailNum to set
	 */
	public void setReqFailNum(Integer reqFailNum) {
		this.reqFailNum = reqFailNum;
	}
}
