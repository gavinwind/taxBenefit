package com.sinosoft.taxbenefit.manage.quartz.constant;

public interface QuartzConstant {
	// 系统操作人ID
	public static final Integer SYS_OPERATPOR_ID = 0;
	
	// 记录不删除 
	public static final String SYS_IS_DELETE_N = "N";
	
	// 记录已删除 
	public static final String SYS_IS_DELETE_Y = "Y";
	
	//批处理不启动
	public static String BATCH_NOT_RUN = "02";
	
	// 批处理标识
	public static String BATCH_CODE = "01";
	
	// 批处理启用标识
	public static String BATCH_STATE = "batch_state";

}
