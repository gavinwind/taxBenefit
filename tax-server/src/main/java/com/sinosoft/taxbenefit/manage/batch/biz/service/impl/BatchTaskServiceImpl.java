package com.sinosoft.taxbenefit.manage.batch.biz.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.platform.common.config.SystemConstants;
import com.sinosoft.platform.common.util.DateUtil;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxBatchTaskMapper;
import com.sinosoft.taxbenefit.core.generated.model.TaxBatchTask;
import com.sinosoft.taxbenefit.manage.base.page.BasePage;
import com.sinosoft.taxbenefit.manage.base.page.Pager;
import com.sinosoft.taxbenefit.manage.batch.biz.service.BatchTaskService;
import com.sinosoft.taxbenefit.manage.batch.dto.BatchTaskDTO;
import com.sinosoft.taxbenefit.manage.batch.mapper.BatchTaskMapper;
import com.sinosoft.taxbenefit.manage.quartz.constant.QuartzConstant;

@Service
public class BatchTaskServiceImpl extends TaxBaseService implements
		BatchTaskService {
	
	@Autowired
	TaxBatchTaskMapper taxBatchTaskMapper;
	@Autowired
	BatchTaskMapper batchTaskMapper;
	
	@Override
	public List<BatchTaskDTO> queryBatchTaskList(BasePage page) {
		Integer rowCount = batchTaskMapper.queryBatchTaskCount();
		Pager p = page.getPager();
		p.setRowCount(rowCount);
		p.setPageOffset(page.getPage() * page.getRows());
		p.setPageTail(page.getRows() * (page.getPage() - 1));
		return batchTaskMapper.queryBatchTaskList(page);
	}

	@Override
	public BatchTaskDTO queryBatchTaskById(Integer sid) {
		BatchTaskDTO batchTask = new BatchTaskDTO();
		TaxBatchTask taxBatchTask = taxBatchTaskMapper.selectByPrimaryKey(sid);
		if(null != taxBatchTask){
			batchTask.setSid(taxBatchTask.getSid());
			batchTask.setTaskName(taxBatchTask.getTaskName());
			batchTask.setBatchType(taxBatchTask.getBatchType());
			batchTask.setExecuteTime(taxBatchTask.getExecuteTime());
			batchTask.setDealClassName(taxBatchTask.getDealClassName());
			batchTask.setDealMethodName(taxBatchTask.getDealMethodName());
			batchTask.setDescription(taxBatchTask.getDescription());
		}
		return batchTask;
	}

	@Override
	public void saveNewBatchTask(BatchTaskDTO batchTask) {
		TaxBatchTask taxBatchTask = new TaxBatchTask();
		taxBatchTask.setTaskName(batchTask.getTaskName());
		taxBatchTask.setBatchType(batchTask.getBatchType());
		taxBatchTask.setExecuteTime(batchTask.getExecuteTime());
		taxBatchTask.setDealClassName(batchTask.getDealClassName());
		taxBatchTask.setDealMethodName(batchTask.getDealMethodName());
		taxBatchTask.setDescription(batchTask.getDescription());
		taxBatchTask.setCreateDate(DateUtil.getCurrentDate());
		taxBatchTask.setCreatorId(SystemConstants.SYS_OPERATOR);
		taxBatchTask.setUpdateDate(DateUtil.getCurrentDate());
		taxBatchTask.setUpdaterId(SystemConstants.SYS_OPERATOR);
		taxBatchTask.setIsDelete(QuartzConstant.SYS_IS_DELETE_N);
		taxBatchTaskMapper.insertSelective(taxBatchTask);
	}

	@Override
	public void updateBatchTask(BatchTaskDTO batchTask) {
		TaxBatchTask taxBatchTask = taxBatchTaskMapper.selectByPrimaryKey(batchTask.getSid());
		taxBatchTask.setTaskName(batchTask.getTaskName());
		taxBatchTask.setBatchType(batchTask.getBatchType());
		taxBatchTask.setExecuteTime(batchTask.getExecuteTime());
		taxBatchTask.setDealClassName(batchTask.getDealClassName());
		taxBatchTask.setDealMethodName(batchTask.getDealMethodName());
		taxBatchTask.setDescription(batchTask.getDescription());
		taxBatchTask.setUpdateDate(DateUtil.getCurrentDate());
		taxBatchTask.setUpdaterId(SystemConstants.SYS_OPERATOR);
		taxBatchTaskMapper.updateByPrimaryKeySelective(taxBatchTask);
	}

}
