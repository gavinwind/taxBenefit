package com.sinosoft.taxbenefit.manage.transCode.biz.service;

import java.util.List;

import com.sinosoft.taxbenefit.core.generated.model.TaxCodeMapping;
import com.sinosoft.taxbenefit.manage.transCode.dto.TransCodeModelDTO;

public interface TransCodeManageService {
	/**
	 * 获取转码的所有类型
	 * 
	 * @return
	 */
	List<TaxCodeMapping> queryAllCodeType() throws Exception;

	/**
	 * 分页查询转码
	 * 
	 * @param model
	 * @return
	 */
	List<TaxCodeMapping> queryByList(TransCodeModelDTO model) throws Exception;

	/**
	 * 添加转码
	 * 
	 * @param model
	 */
	void addCodeMapping(TransCodeModelDTO model) throws Exception;

	/**
	 * 修改转码
	 * 
	 * @param model
	 */
	void modifyCodeMapping(TransCodeModelDTO model) throws Exception;

	/**
	 * 删除转码
	 * 
	 * @param sid
	 *            主键id
	 * @return
	 */
	void deleteCodeMapping(Integer[] sid) throws Exception;

	/**
	 * 根据主键查询
	 * 
	 * @param sid
	 * @return
	 */
	TaxCodeMapping queryCodeMappingBySid(Integer sid) throws Exception;
	
	/**
	 * 根据转码类型和核心编码判断转码是否存在
	 * @param model
	 * @return
	 */
	Integer queryByTypeAndCode(TransCodeModelDTO model);
	/**
	 * 根据转码类型查询
	 * @param model
	 * @return
	 */
	String queryByCodeType(String codeType);
}
