package com.sinosoft.taxbenefit.manage.transCode.web.util;

/**
 * 转码页面
 * 
 * @author zhangke
 *
 */
public class TransCodeViewConstant {
	public static final String TRANSCODE_VIEW = "page/transCode/transCode";
}
