package com.sinosoft.taxbenefit.manage.bargainquery.dto;

import java.util.Date;

import com.sinosoft.taxbenefit.manage.base.page.BasePage;

/**
 * 请求记录明细DTO
 * 
 * @author zhangke
 *
 */
public class BusinessDetailDTO extends BasePage {
	/** 交易时间 */
	private String businessDate;

	public String getBusinessDate() {
		return businessDate;
	}

	public void setBusinessDate(String businessDate) {
		this.businessDate = businessDate;
	}
}
