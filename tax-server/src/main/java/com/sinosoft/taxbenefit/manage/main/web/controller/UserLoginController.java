package com.sinosoft.taxbenefit.manage.main.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sinosoft.platform.common.util.ReflectionUtil;
import com.sinosoft.taxbenefit.manage.base.entity.BaseEntity.DELETED;
import com.sinosoft.taxbenefit.manage.base.util.DateUtil;
import com.sinosoft.taxbenefit.manage.base.util.MethodUtil;
import com.sinosoft.taxbenefit.manage.base.util.SessionUtils;
import com.sinosoft.taxbenefit.manage.base.web.BaseAction;
import com.sinosoft.taxbenefit.manage.jeecg.biz.service.impl.SysUserService;
import com.sinosoft.taxbenefit.manage.jeecg.entity.SysUser;
import com.sinosoft.taxbenefit.manage.jeecg.page.SysUserModel;
import com.sinosoft.taxbenefit.manage.main.dto.WebLoginDTO;
import com.sinosoft.taxbenefit.manage.main.web.util.MainViewConstant;

/**
 * 登录
 * @author SheChunMing
 */
@Controller
@RequestMapping("/manage/index.action")
public class UserLoginController extends BaseAction{
	private String message = null;
	// Servrice start
	@Autowired(required=false) 
	private SysUserService<SysUser> sysUserService; 
	/**
	 * 登陆页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(params = "action=login")
	public String initIndex(HttpServletRequest request, HttpServletResponse response, ModelMap model){	
		logger.info("进入登陆页面:/manage/index.action?action=login");
		return MainViewConstant.USER_LOGIN_VIEW;
	}
	
	/**
	 * 检查用户名称
	 * @param user
	 * @param req
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(params = "action=checkuser.do")
	public void checkuser(SysUserModel userModel, HttpServletRequest req,HttpServletResponse response) throws Exception {
		logger.info("检查用户名称:/manage/index.action?action=checkuser.do");
		SysUser user = sysUserService.queryLogin(userModel.getEmail(), MethodUtil.MD5(userModel.getPwd()));
		if (user != null) {
				message = "用户: " + user.getNickName() + "登录成功";
				//-------------------------------------------------------
				//登录次数加1 修改登录时间
				int loginCount = 0;
				if(user.getLoginCount() != null){
					loginCount = user.getLoginCount();
				}
				user.setLoginCount(loginCount+1);
				user.setLoginTime(DateUtil.getDateByString(""));
				sysUserService.updateBySelective(user);
				//设置User到Session
				WebLoginDTO loginDTO=new WebLoginDTO();
				ReflectionUtil.copyProperties(user,loginDTO);
				SessionUtils.setUser(req,loginDTO);
				//记录成功登录日志
				logger.info(message);
				sendSuccessMessage(response,message);
				//-------------------------------------------------------
		} else {
			sendFailureMessage(response, "用户名或密码错误!");
		}
	}
	
	/**
	 * 退出登录
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(params ="action=logout.do")
	public String  logout(HttpServletRequest request,HttpServletResponse response) throws Exception{
		SessionUtils.removeUser(request);
		return MainViewConstant.USER_LOGIN_VIEW;
	}
	
	/**
	 * 修改密码
	 * @param url
	 * @param classifyId
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(params ="action=modifyPwd.do")
	public void modifyPwd(String oldPwd,String newPwd,HttpServletRequest request,HttpServletResponse response) throws Exception{
		logger.info("进入检查用户名称:/manage/index.action?action=modifyPwd.do");
		WebLoginDTO user = SessionUtils.getUser(request);
		if(user == null){
			sendFailureMessage(response, "对不起,登录超时.");
			return;
		}
		SysUser bean  = sysUserService.queryById(user.getSid());
		if(bean.getSid() == null || DELETED.YES.key == bean.getDeleted()){
			sendFailureMessage(response, "对不起,用户不存在.");
			return;
		}
		if(StringUtils.isBlank(newPwd)){
			sendFailureMessage(response, "密码不能为空.");
			return;
		}
		//不是超级管理员，匹配旧密码
		if(!MethodUtil.ecompareMD5(oldPwd,bean.getPwd())){
			sendFailureMessage(response, "旧密码输入不匹配.");
			return;
		}
		bean.setPwd(MethodUtil.MD5(newPwd));
		sysUserService.updateBySelective(bean);
		sendSuccessMessage(response, "Save success.");
	}
}
