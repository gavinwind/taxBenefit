package com.sinosoft.taxbenefit.manage.quartz.api.quartzdb;

import java.util.List;

import com.sinosoft.taxbenefit.core.generated.model.TaxBatchTask;
import com.sinosoft.taxbenefit.core.generated.model.TaxCommonCode;

public interface QuartzBo {

	/**
	 * 从配置中读取批处理定义信息
	 * @param type
	 * @return 批处理定义信息列表
	 * @throws Exception
	 */
	public List<TaxBatchTask> queryBatchFromConfig(String type) throws Exception;
	
	/**
	 * 获取数据字典表数据
	 * @return
	 */
	public TaxCommonCode getCommonCode(String sysCode, String codeType);
}
