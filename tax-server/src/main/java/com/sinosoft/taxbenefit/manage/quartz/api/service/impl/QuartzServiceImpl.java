package com.sinosoft.taxbenefit.manage.quartz.api.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.taxbenefit.core.generated.model.TaxBatchTask;
import com.sinosoft.taxbenefit.core.generated.model.TaxCommonCode;
import com.sinosoft.taxbenefit.manage.quartz.api.dto.QuartzJobDTO;
import com.sinosoft.taxbenefit.manage.quartz.api.quartzdb.QuartzBo;
import com.sinosoft.taxbenefit.manage.quartz.api.service.QuartzService;
import com.sinosoft.taxbenefit.manage.quartz.api.util.QuartzJob;
import com.sinosoft.taxbenefit.manage.quartz.api.util.QuartzUtil;
import com.sinosoft.taxbenefit.manage.quartz.constant.QuartzConstant;

@Service
public class QuartzServiceImpl extends TaxBaseService implements QuartzService {
	@Autowired
	private QuartzBo quartzBo;
	@Autowired
	private QuartzUtil quartzUtil;

	public void initScheduler() throws Exception {
		List<TaxBatchTask> batchList = null;
		TaxCommonCode taxCommonCode = quartzBo.getCommonCode(QuartzConstant.BATCH_CODE, QuartzConstant.BATCH_STATE);
		if (null != taxCommonCode) {
			String runFlag = taxCommonCode.getContent();
			if (!QuartzConstant.BATCH_NOT_RUN.equals(runFlag)) {
				batchList = quartzBo.queryBatchFromConfig(runFlag);
			}
		}
		// 逐笔设置任务信息到quartz,并调度任务
		if (null != batchList && batchList.size() > 0) {
			for (TaxBatchTask batchInfo : batchList) {
				QuartzJobDTO job = new QuartzJobDTO();
				job.getJobDataMap().put(QuartzJob.OBJECT_NAME, batchInfo.getDealClassName());
				job.getJobDataMap().put(QuartzJob.OBJECT_METHOD, batchInfo.getDealMethodName());

				// 判断是否以多线程的方式执行任务
//				if (StringUtils.isNotBlank(batchInfo.getExecuteParameter())) {
//					// 多线程的方式执行任务
//					logger.info("多线程执行任务" + batchInfo.getDealClassName() + "."
//							+ batchInfo.getDealMethodName());
//					String counts = batchInfo.getExecuteParameter();
//
//					int cs = 0;
//					try {
//						cs = Integer.parseInt(counts);
//					} catch (NumberFormatException ex) {
//						logger.warn("多线程配置错误：" + batchInfo.getSid());
//					}
//
//					job.getJobDataMap().put(
//							QuartzPoolJob.OBJECT_OBJECT_THREADPOOL,
//							ExecutorUtil.createPoolTaskExecutor(cs));
//					job.setJobClass(QuartzPoolJob.class);
//				} else {
					// 单线程方式执行任务
					job.setJobClass(QuartzJob.class);
//				}

				job.setCronExpression(batchInfo.getExecuteTime());
				String jobName = batchInfo.getTaskName() + "_"
						+ batchInfo.getSid();
				job.setJobName(jobName);
				logger.info("----开始部署任务:" + jobName);
				quartzUtil.scheduleCronJob(job);
				logger.info("----成功部署任务:" + jobName);
			}
			logger.info("批处理提取并调度完成");
		}
	}

	public void reScheduler() throws Exception {
		// 取消现有的任务
		String[] jobNames = quartzUtil.getJobNames();
		if (null != jobNames && jobNames.length > 0) {
			for (String jobName : jobNames) {
				logger.info("----开始移除任务:" + jobName);
				quartzUtil.cancelJob(jobName);
				logger.info("----成功移除任务:" + jobName);
			}
		}
		logger.info("现有任务已全部取消");

		this.initScheduler();
	}

	/**
	 * 取批处理任务
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<TaxBatchTask> getBatchInfoList(TaxBatchTask batchTask)
			throws Exception {
		if (null != batchTask) {
			return quartzBo.queryBatchFromConfig(batchTask.getBatchType());
		}
		return null;
	}

}
