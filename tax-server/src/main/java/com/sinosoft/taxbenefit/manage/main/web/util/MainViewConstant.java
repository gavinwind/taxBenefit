package com.sinosoft.taxbenefit.manage.main.web.util;

/**
 * 后台首页面路径
 * 
 * @author SheChunMing
 */
public class MainViewConstant {

	/** 后台登陆页面 */
	public final static String USER_LOGIN_VIEW = "page/main/login";
	/** 后台主页面 */
	public final static String MANAGE_MAIN_VIEW = "view/main/main";
	/** 菜单管理页面 */
	public final static String MAIN_MENU_VIEW = "view/sys/sysMenu";
	/** 角色管理页面 */
	public final static String MAIN_ROLE_VIEW = "view/sys/sysRole";
	/** 系统用户管理页面 */
	public final static String MAIN_USER_VIEW = "view/sys/sysUser";
	/** 系统用户授权页面 */
	public final static String MAIN_USER_ROLE_VIEW = "view/sys/sysUserRole";
}
