package com.sinosoft.taxbenefit.manage.batch.dto;

/**
 * 整合请求单条报文
 * @author gyas-itchunmingsh
 *
 */
public class BargainRequestInfo {
	// 任务编号
	private Integer faultTaskId;
	// 子任务流水号
	private String serialNo;
	// 请求报文
	private String requestContent;
	// 对应的请求编码
	private String portCode;
	
	public Integer getFaultTaskId() {
		return faultTaskId;
	}
	public void setFaultTaskId(Integer faultTaskId) {
		this.faultTaskId = faultTaskId;
	}
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public String getRequestContent() {
		return requestContent;
	}
	public void setRequestContent(String requestContent) {
		this.requestContent = requestContent;
	}
	public String getPortCode() {
		return portCode;
	}
	public void setPortCode(String portCode) {
		this.portCode = portCode;
	}
	
}
