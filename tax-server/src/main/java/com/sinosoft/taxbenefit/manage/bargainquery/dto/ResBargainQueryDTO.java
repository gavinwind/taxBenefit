package com.sinosoft.taxbenefit.manage.bargainquery.dto;

import java.util.List;
import com.sinosoft.taxbenefit.api.base.EBResultCodeDTO;
/**
 * 交易核对查询中保信返回DTO
 * @author yangdongkai@outlook.com
 * @date 2016年3月28日 下午3:23:11
 */
public class ResBargainQueryDTO {
	//汇总信息
	private ResBargainQueryTotalDTO total;
	//核对明细
	private List<ResBargainQueryDetailDTO> detail;
	//返回编码
	private EBResultCodeDTO result;
	
	/**
	 * @return the detail
	 */
	public List<ResBargainQueryDetailDTO> getDetail() {
		return detail;
	}
	/**
	 * @param detail the detail to set
	 */
	public void setDetail(List<ResBargainQueryDetailDTO> detail) {
		this.detail = detail;
	}
	/**
	 * @return the total
	 */
	public ResBargainQueryTotalDTO getTotal() {
		return total;
	}
	/**
	 * @param total the total to set
	 */
	public void setTotal(ResBargainQueryTotalDTO total) {
		this.total = total;
	}
	/**
	 * @return the result
	 */
	public EBResultCodeDTO getResult() {
		return result;
	}
	/**
	 * @param result the result to set
	 */
	public void setResult(EBResultCodeDTO result) {
		this.result = result;
	} 
	
}
