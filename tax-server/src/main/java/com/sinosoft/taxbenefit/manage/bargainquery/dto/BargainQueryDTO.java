package com.sinosoft.taxbenefit.manage.bargainquery.dto;

import java.util.Date;
import com.sinosoft.taxbenefit.manage.base.page.BasePage;
/**
 * 交易核对查询页面DTO
 * @author yangdongkai@outlook.com
 * @date 2016年4月8日 下午2:31:29
 */
public class BargainQueryDTO extends BasePage {
	//SID   
	private Integer sid;
	//开始时间
	private Date startDate;
	//结束时间
	private Date endDate;
	/**
	 * @return the sid
	 */
	public Integer getSid() {
		return sid;
	}
	/**
	 * @param sid the sid to set
	 */
	public void setSid(Integer sid) {
		this.sid = sid;
	}
	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}
	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}
	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}