package com.sinosoft.taxbenefit.manage.batch.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sinosoft.platform.common.util.StringUtil;
import com.sinosoft.taxbenefit.core.generated.model.TaxCodeMapping;
import com.sinosoft.taxbenefit.manage.base.page.BasePage;
import com.sinosoft.taxbenefit.manage.base.util.HtmlUtil;
import com.sinosoft.taxbenefit.manage.base.web.BaseAction;
import com.sinosoft.taxbenefit.manage.batch.biz.service.BatchTaskService;
import com.sinosoft.taxbenefit.manage.batch.dto.BatchTaskDTO;
import com.sinosoft.taxbenefit.manage.batch.web.util.BatchTaskViewConstant;

/**
 * 后台批处理管理
 * @author SheChunMing
 */
@Controller
@RequestMapping("/manage/batchTaskController") 
public class BatchTaskController extends BaseAction{
	@Autowired
	BatchTaskService batchTaskService;
	
	@RequestMapping("/batchTask.do")
	public String initBatchTask(HttpServletRequest request, HttpServletResponse response) throws Exception{
		logger.info("进入批处理管理页面：/manage/batchTaskController/batchTask.do");
		return BatchTaskViewConstant.BATCH_TASK_VIEW;
	}
	
	@RequestMapping("/dataList.do")
	public void queryBatchTaskList( HttpServletRequest request, HttpServletResponse response) throws Exception{
		logger.info("获取批处理列表：/manage/batchTaskController/dataList.do");
		BasePage page = new BasePage();
		List<BatchTaskDTO> batchTaskList = batchTaskService.queryBatchTaskList(page);
		Map<String,Object> jsonMap = new HashMap<String,Object>();
		jsonMap.put("total",page.getPager().getRowCount());
		jsonMap.put("rows", batchTaskList);
		HtmlUtil.writerJson(response, jsonMap);
	}
	
	
	/**
	 * 添加或修改数据
	 * @param url
	 * @param classifyId
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/save")
	public void save(BatchTaskDTO batchTask,Integer[] typeIds,HttpServletResponse response) throws Exception{
		logger.info("添加或修改批处理：/manage/batchTaskController/save.do");
		try {
			if (null == batchTask.getSid()) {
				// 添加
				batchTaskService.saveNewBatchTask(batchTask);
			}else{
				// 更新
				batchTaskService.updateBatchTask(batchTask);
			}
		} catch (Exception e) {
			e.printStackTrace();
			sendFailureMessage(response, "操作失败~");
		}
		sendSuccessMessage(response, "操作成功~");
	}
	
	/**
	 * 获取指定批处理对象
	 * @param sid
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/getId")
	public void getId(String sid,HttpServletResponse response) throws Exception{
		logger.info("获取批处理信息：/manage/batchTaskController/getId.do");
		Map<String, Object> context = new HashMap();
		try {
			BatchTaskDTO batchTask = batchTaskService.queryBatchTaskById(Integer.parseInt(sid));
			if (batchTask == null) {
				sendFailureMessage(response, "没有找到对应的记录!");
				return;
			}
			context.put(SUCCESS, true);
			context.put("data", batchTask);
		} catch (Exception e) {
			e.printStackTrace();
			context.put(SUCCESS, false);
		}finally{
			HtmlUtil.writerJson(response, context);
		}
		
	}
}
