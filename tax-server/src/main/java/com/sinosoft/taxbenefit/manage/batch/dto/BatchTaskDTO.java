package com.sinosoft.taxbenefit.manage.batch.dto;

/**
 * 批处理对象
 * @author SheChunMing
 */
public class BatchTaskDTO {
	private Integer sid;
	// 批处理名称
	private String taskName;
	// 启用状态
	private String batchType;
	// 触发时机
	private String executeTime;
	// 调用类名
	private String dealClassName;
	// 调用方法名
	private String dealMethodName;
	private String description;
	
	public Integer getSid() {
		return sid;
	}
	public void setSid(Integer sid) {
		this.sid = sid;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public String getBatchType() {
		return batchType;
	}
	public void setBatchType(String batchType) {
		this.batchType = batchType;
	}
	public String getExecuteTime() {
		return executeTime;
	}
	public void setExecuteTime(String executeTime) {
		this.executeTime = executeTime;
	}
	public String getDealClassName() {
		return dealClassName;
	}
	public void setDealClassName(String dealClassName) {
		this.dealClassName = dealClassName;
	}
	public String getDealMethodName() {
		return dealMethodName;
	}
	public void setDealMethodName(String dealMethodName) {
		this.dealMethodName = dealMethodName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
