/**
 * @Copyright ®2012 Focoon Co. Ltd. All rights reserved.<br/>
 * @Package:  com.sinosoft.business.user.config<br/>
 * @FileName: LoginConstants.java<br/>
 * @Description: xxxxxxxxx<br/>
 */
package com.sinosoft.taxbenefit.manage.main.config;

/**  
 * 用户登录session存放code
 * session存放code
 * @author wangyibing@focoom.com.cn
 * @date 2015-8-18 下午3:59:38 
 * @version V1.0  
 */
public class LoginConstants {
	/** 用户登录session存放用户表DTO */
	public static String LOGIN_INFO = "loginInfo";
}


