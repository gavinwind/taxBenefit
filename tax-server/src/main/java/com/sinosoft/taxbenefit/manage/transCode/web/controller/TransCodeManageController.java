package com.sinosoft.taxbenefit.manage.transCode.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.sinosoft.platform.common.util.StringUtil;
import com.sinosoft.taxbenefit.core.generated.model.TaxCodeMapping;
import com.sinosoft.taxbenefit.manage.base.util.HtmlUtil;
import com.sinosoft.taxbenefit.manage.base.web.BaseAction;
import com.sinosoft.taxbenefit.manage.transCode.biz.service.TransCodeManageService;
import com.sinosoft.taxbenefit.manage.transCode.dto.TransCodeModelDTO;
import com.sinosoft.taxbenefit.manage.transCode.web.util.TransCodeViewConstant;

@Controller
@RequestMapping("/manage/transCode")
public class TransCodeManageController extends BaseAction {
	@Autowired
	private TransCodeManageService transCodeManageService;// 转码页面的

	/**
	 * 
	 * @param url
	 * @param classifyId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/list.do")
	public ModelAndView list(TransCodeModelDTO page, HttpServletRequest request)
			throws Exception {
		logger.info("进入平台转码页面！/manage/transCode/list.do");
		Map<String, Object> context = getRootMap();
		return forword(TransCodeViewConstant.TRANSCODE_VIEW, context);
	}

	@RequestMapping("/getCodeTypeList.do")
	public void getCodeTypeList( HttpServletResponse response)throws Exception {
		logger.info("获取所有转码类型列表！/manage/transCode/getCodeTypeList.do");
		List<TaxCodeMapping> list = transCodeManageService.queryAllCodeType();
		HtmlUtil.writerJson(response, list);
	}
	
	/**
	 * 
	 * 
	 * @param url
	 * @param classifyId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/dataList.do")
	public void datalist(TransCodeModelDTO page, HttpServletResponse response)
			throws Exception {
		logger.info("进入平台转码获取转码列表！/manage/transCode/dataList.do");
		List<TaxCodeMapping> dataList = transCodeManageService.queryByList(page);
		// 设置页面数据
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		jsonMap.put("total", page.getPager().getRowCount());
		jsonMap.put("rows", dataList);
		HtmlUtil.writerJson(response, jsonMap);
	}

	/**
	 * 添加或修改数据
	 * 
	 * @param url
	 * @param classifyId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/save.do")
	public void save(TransCodeModelDTO page, Integer[] typeIds,
			HttpServletResponse response) throws Exception {
		logger.info("进入平台转码编辑！/manage/transCode/save.do");	
		if(StringUtil.isEmpty(page.getCodeType())){
			sendFailureMessage(response, "请选择转码类型");
			return ;
		}
		String codeTypeName=transCodeManageService.queryByCodeType(page.getCodeType());
		if(codeTypeName !=null && !codeTypeName.equals(page.getRemark())){
			sendFailureMessage(response, "输入转码的编码和原有转码的名称不匹配");
			return ;
		}
		if (page.getSid() == null || StringUtil.isEmpty(page.getSid().toString())) {		
			if(transCodeManageService.queryByTypeAndCode(page) > 0){
				sendFailureMessage(response, "输入的转码已存在");
				return ;
			}		
			transCodeManageService.addCodeMapping(page);		
		} else {
			transCodeManageService.modifyCodeMapping(page);
		}
		sendSuccessMessage(response, "保存成功~");
	}

	@RequestMapping("/getId.do")
	public void getId(Integer sid, HttpServletRequest request,HttpServletResponse response)
			throws Exception {
		logger.info("进入平台转码获取转码信息！/manage/transCode/getId.do");
		Map<String, Object> context = new HashMap<String, Object>();
		TaxCodeMapping entity = transCodeManageService.queryCodeMappingBySid(sid);
		if (entity == null) {
			sendFailureMessage(response, "没有找到对应的记录!");
			return;
		}
		context.put(SUCCESS, true);
		context.put("data", entity);
		HtmlUtil.writerJson(response, context);
	}

	@RequestMapping("/delete.do")
	public void delete(Integer[] sid, HttpServletResponse response)
			throws Exception {
		logger.info("进入平台转码删除转码信息！/manage/transCode/delete.do");
		transCodeManageService.deleteCodeMapping(sid);
		sendSuccessMessage(response, "删除成功"); 
	}
}
