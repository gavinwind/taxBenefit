package com.sinosoft.taxbenefit.manage.exceptionJob.biz.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.base.TaxBaseService;
import com.sinosoft.taxbenefit.core.faultTask.biz.service.FaultTaskService;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxRequestReceiveLogMapper;
import com.sinosoft.taxbenefit.core.generated.model.TaxFaultTask;
import com.sinosoft.taxbenefit.core.generated.model.TaxRequestReceiveLogExample;
import com.sinosoft.taxbenefit.core.generated.model.TaxRequestReceiveLogWithBLOBs;
import com.sinosoft.taxbenefit.manage.exceptionJob.biz.service.ExceptionJobInfoService;
import com.sinosoft.taxbenefit.manage.exceptionJob.mapper.QueryFailJobMapper;
@Service
public class ExceptionJobBatchServiceImpl extends TaxBaseService implements ExceptionJobInfoService {

	@Autowired
	private FaultTaskService faultTaskService; 
	@Autowired
	private TaxRequestReceiveLogMapper receiveLogMapper;
	@Autowired
	QueryFailJobMapper queryFailJobMapper;
	@Override
	public List<TaxFaultTask> queryExceptionJobList() {
		return queryFailJobMapper.selectExceptionJobList();
	}

	@Override
	public List<TaxFaultTask> queryAppointmentJobList() {
		return queryFailJobMapper.selectAppointmentJobList();
	}
	
	@Override
	public TaxRequestReceiveLogWithBLOBs  queryRequestContentBySerialNo(String SerialNo) {
		TaxRequestReceiveLogWithBLOBs taxRequestReceiveLog = new TaxRequestReceiveLogWithBLOBs();
		TaxRequestReceiveLogExample example=new TaxRequestReceiveLogExample();
		example.createCriteria().andSerialNoEqualTo(SerialNo);
		List<TaxRequestReceiveLogWithBLOBs> taxRequestReceiveLogList = receiveLogMapper.selectByExampleWithBLOBs(example);
		if(null != taxRequestReceiveLogList && taxRequestReceiveLogList.size() == 1){
			taxRequestReceiveLog = taxRequestReceiveLogList.get(0);
		}
		return taxRequestReceiveLog;
	}

	public List<TaxFaultTask> queryFailJoblist(){
		return queryFailJobMapper.queryFailJoblist();
	}

}
