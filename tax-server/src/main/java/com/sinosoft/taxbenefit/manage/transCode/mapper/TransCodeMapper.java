package com.sinosoft.taxbenefit.manage.transCode.mapper;

import java.util.List;

import com.sinosoft.taxbenefit.core.generated.model.TaxCodeMapping;
import com.sinosoft.taxbenefit.manage.transCode.dto.TransCodeModelDTO;

/**
 * 页面转码的Mapper
 * @author zhangke
 *
 */
public interface TransCodeMapper {
	/**
	 * 获取转码的所有类型
	 * @return
	 */
	List<TaxCodeMapping> queryAllCodeType();
	/**
	 * 分页查询转码
	 * @param model
	 * @return
	 */
	List<TaxCodeMapping> queryByList(TransCodeModelDTO model);
	/**
	 * 查询转码的总记录数
	 * @param model
	 * @return
	 */
	Integer queryByCount(TransCodeModelDTO model);
	
	/**
	 * 根据转码类型和核心编码查询
	 * @param model
	 * @return
	 */
	Integer queryByTypeAndCode(TransCodeModelDTO model);
	
	/**
	 * 根据转码类型查询转码类型名称
	 * @param model
	 * @return
	 */
	String queryByCodeType(String codeType);
}
