package com.sinosoft.taxbenefit.manage.exceptionJob.biz.service;

import java.util.List;

import com.sinosoft.taxbenefit.core.generated.model.TaxFaultTask;
import com.sinosoft.taxbenefit.core.generated.model.TaxRequestReceiveLogWithBLOBs;

public interface ExceptionJobInfoService {
	/**
	 * 异常异步/待处理处理失败的任务记录
	 * @param stateCode
	 * @return
	 */
	public List<TaxFaultTask> queryExceptionJobList();
	
	/**
	 * 预约码/待处理处理失败的任务记录
	 * @param stateCode
	 * @return
	 */
	public List<TaxFaultTask> queryAppointmentJobList();
	
	/**
	 * 根据交易流水号查找报文内容
	 * @param SerialNo
	 * @return
	 */
	TaxRequestReceiveLogWithBLOBs queryRequestContentBySerialNo(String SerialNo);
	
	/**
	 * 查找处理失败，异常类型，预约码类型的任务。
	 * @param disposeTypelist
	 * @param stateCode
	 * @return
	 */
//	public List<TaxFaultTask> queryFailJoblist(List<String> disposeTypelist,String stateCode);

	
	public List<TaxFaultTask> queryFailJoblist();
}
