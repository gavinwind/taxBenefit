package com.sinosoft.taxbenefit.manage.base.service;

import java.util.List;
import java.util.UUID;

import com.sinosoft.taxbenefit.manage.base.dao.BaseDao;
import com.sinosoft.taxbenefit.manage.base.page.BasePage;
import com.sinosoft.taxbenefit.manage.base.page.Pager;
import com.sinosoft.taxbenefit.manage.base.util.ClassReflectUtil;

public abstract class BaseService<T> {

	public abstract BaseDao<T> getDao();

	public void add(T t) throws Exception {
		// 设置主键.字符类型采用UUID,数字类型采用自增
		//ClassReflectUtil.setIdKeyValue(t, "sid", UUID.randomUUID().toString());
		getDao().add(t);
	}

	public void update(T t) throws Exception {
		getDao().update(t);
	}

	public void updateBySelective(T t) {
		getDao().updateBySelective(t);
	}

	public void delete(Object... ids) throws Exception {
		if (ids == null || ids.length < 1) {
			return;
		}
		for (Object id : ids) {
			getDao().delete(id);
		}
	}

	public int queryByCount(BasePage page) throws Exception {
		return getDao().queryByCount(page);
	}

	public List<T> queryByList(BasePage page) throws Exception {
		Integer rowCount = queryByCount(page);
		Pager p = page.getPager();
		p.setRowCount(rowCount);
		p.setPageOffset(page.getPage() * page.getRows());
		p.setPageTail(page.getRows() * (page.getPage() - 1));
		return getDao().queryByList(page);
	}

	public T queryById(Object id) throws Exception {
		return getDao().queryById(id);
	}
}
