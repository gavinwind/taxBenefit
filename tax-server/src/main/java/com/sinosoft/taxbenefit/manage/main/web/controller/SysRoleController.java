package com.sinosoft.taxbenefit.manage.main.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.sinosoft.taxbenefit.manage.base.util.HtmlUtil;
import com.sinosoft.taxbenefit.manage.base.web.BaseAction;
import com.sinosoft.taxbenefit.manage.jeecg.biz.service.impl.SysMenuService;
import com.sinosoft.taxbenefit.manage.jeecg.biz.service.impl.SysRoleRelService;
import com.sinosoft.taxbenefit.manage.jeecg.biz.service.impl.SysRoleService;
import com.sinosoft.taxbenefit.manage.jeecg.entity.SysMenu;
import com.sinosoft.taxbenefit.manage.jeecg.entity.SysRole;
import com.sinosoft.taxbenefit.manage.jeecg.entity.SysRoleRel;
import com.sinosoft.taxbenefit.manage.jeecg.entity.SysRoleRel.RelType;
import com.sinosoft.taxbenefit.manage.jeecg.page.SysRoleModel;
import com.sinosoft.taxbenefit.manage.main.web.util.MainViewConstant;
 /**
  * 角色管理
  * @author zhangke
  *
  */
@Controller
@RequestMapping("/sysRole") 
public class SysRoleController extends BaseAction{
	
	// Servrice start
	@Autowired(required=false) 
	private SysRoleService<SysRole> sysRoleService; 
	
	// Servrice start
	@Autowired(required=false) 
	private SysMenuService<SysMenu> sysMenuService; 
	@Autowired(required=false) 
	private SysRoleRelService<SysRoleRel> sysRoleRelService;
	/**
	 * 
	 * @param url
	 * @param classifyId
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/role.do")
	public ModelAndView  list(SysRoleModel model,HttpServletRequest request) throws Exception{
		Map<String,Object>  context = getRootMap();
		return forword(MainViewConstant.MAIN_ROLE_VIEW,context); 
	}
	
	
	/**
	 * ilook 首页
	 * @param url
	 * @param classifyId
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/dataList.do") 
	public void  datalist(SysRoleModel model,HttpServletResponse response) throws Exception{
		List<SysRole> dataList = sysRoleService.queryByList(model);
		//设置页面数据
		Map<String,Object> jsonMap = new HashMap<String,Object>();
		jsonMap.put("total",model.getPager().getRowCount());
		jsonMap.put("rows", dataList);
		HtmlUtil.writerJson(response, jsonMap);
	}
	
	/**
	 * 添加或修改数据
	 * @param url
	 * @param classifyId
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/save.do")
	public void save(SysRole bean,Integer[] menuIds,Integer[] btnIds,HttpServletResponse response) throws Exception{
		if(bean.getSid() == null){
			sysRoleService.add(bean,menuIds,btnIds);
		}else{
			sysRoleService.update(bean,menuIds,btnIds);
		}
		sendSuccessMessage(response, "保存成功~");
	}
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping("/getId.do")
	public void getId(Integer sid,HttpServletResponse response) throws Exception{
		Map<String,Object>  context = new HashMap<String,Object> ();
		SysRole bean  = sysRoleService.queryById(sid);
		if(bean  == null){
			sendFailureMessage(response, "没有找到对应的记录!");
			return;
		}
		//获取权限关联的菜单
		Integer[] menuIds = null;
		List<SysMenu> menuList =  sysMenuService.getMenuByRoleId(sid);
		if(menuList != null){
			menuIds = new Integer[menuList.size()];
			int i = 0;
			for(SysMenu item : menuList){
				menuIds[i] = item.getSid();
				i++;
			}
		}
		//获取权限下关联的按钮
		Integer[] btnIds = null;
		List<SysRoleRel>  btnList =sysRoleRelService.queryByRoleId(sid, RelType.BTN.key);
		if(btnList != null){
			btnIds = new Integer[btnList.size()];
			int i = 0;
			for(SysRoleRel item : btnList){
				btnIds[i] = item.getObjId();
				i++;
			}
		}
		//将对象转成Map
		Map<String,Object> data = BeanUtils.describe(bean);
		data.put("menuIds", menuIds);
		data.put("btnIds", btnIds);
		context.put(SUCCESS, true);
		context.put("data", data);
		HtmlUtil.writerJson(response, context);
	}
		
	@RequestMapping("/delete.do")
	public void delete(Integer[] sid,HttpServletResponse response) throws Exception{
		sysRoleService.delete(sid);
		sendSuccessMessage(response, "删除成功");
	}
		
	@RequestMapping("/loadRoleList.do")
	public void loadRoleList(HttpServletResponse response) throws Exception{
		List<SysRole>  roloList = sysRoleService.queryAllList();
		HtmlUtil.writerJson(response, roloList);
	}
}
