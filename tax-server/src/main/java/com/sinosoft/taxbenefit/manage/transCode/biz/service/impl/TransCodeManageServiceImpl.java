package com.sinosoft.taxbenefit.manage.transCode.biz.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sinosoft.platform.common.util.ReflectionUtil;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxCodeMappingMapper;
import com.sinosoft.taxbenefit.core.generated.model.TaxCodeMapping;
import com.sinosoft.taxbenefit.manage.base.page.Pager;
import com.sinosoft.taxbenefit.manage.transCode.biz.service.TransCodeManageService;
import com.sinosoft.taxbenefit.manage.transCode.dto.TransCodeModelDTO;
import com.sinosoft.taxbenefit.manage.transCode.mapper.TransCodeMapper;

@Service
public class TransCodeManageServiceImpl implements TransCodeManageService {
	@Autowired
	TransCodeMapper transCodeMapper;
	@Autowired
	TaxCodeMappingMapper taxCodeMappingMapper;

	@Override
	public List<TaxCodeMapping> queryAllCodeType() throws Exception {
		return transCodeMapper.queryAllCodeType();
	}

	@Override
	public List<TaxCodeMapping> queryByList(TransCodeModelDTO model)
			throws Exception {
		Integer rowCount = transCodeMapper.queryByCount(model);
		Pager p = model.getPager();
		p.setRowCount(rowCount);
		p.setPageOffset(model.getPage() * model.getRows());
		p.setPageTail(model.getRows() * (model.getPage() - 1));
		return transCodeMapper.queryByList(model);
	}

	@Override
	public void addCodeMapping(TransCodeModelDTO model) throws Exception {
		TaxCodeMapping taxCodeMapping = new TaxCodeMapping();
		ReflectionUtil.copyProperties(model, taxCodeMapping);
		taxCodeMapping.setManageCom("00");
		taxCodeMappingMapper.insert(taxCodeMapping);
	}

	@Override
	public void modifyCodeMapping(TransCodeModelDTO model) {
		TaxCodeMapping taxCodeMapping = new TaxCodeMapping();
		ReflectionUtil.copyProperties(model, taxCodeMapping);
		taxCodeMapping.setManageCom("00");
		taxCodeMappingMapper.updateByPrimaryKeySelective(taxCodeMapping);
	}

	@Override
	public void deleteCodeMapping(Integer[] sid) throws Exception {
		for (Integer id : sid) {
			taxCodeMappingMapper.deleteByPrimaryKey(id);
		}
	}

	@Override
	public TaxCodeMapping queryCodeMappingBySid(Integer sid) throws Exception {
		return taxCodeMappingMapper.selectByPrimaryKey(sid);
	}

	@Override
	public Integer queryByTypeAndCode(TransCodeModelDTO model) {
		return transCodeMapper.queryByTypeAndCode(model);
	}

	@Override
	public String queryByCodeType(String codeType) {
		return transCodeMapper.queryByCodeType(codeType);
	}
}
