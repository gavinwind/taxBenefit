package com.sinosoft.taxbenefit.manage.batch.web.util;

/**
 * 批处理管理页面配置
 * @author SheChunMing
 */
public class BatchTaskViewConstant {

	//批处理管理页面
	public final static String BATCH_TASK_VIEW = "page/batchtask/batch_task_manage";
}
