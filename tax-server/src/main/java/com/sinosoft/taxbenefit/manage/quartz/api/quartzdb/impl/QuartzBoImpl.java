package com.sinosoft.taxbenefit.manage.quartz.api.quartzdb.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinosoft.taxbenefit.core.generated.mapper.TaxBatchTaskMapper;
import com.sinosoft.taxbenefit.core.generated.mapper.TaxCommonCodeMapper;
import com.sinosoft.taxbenefit.core.generated.model.TaxBatchTask;
import com.sinosoft.taxbenefit.core.generated.model.TaxBatchTaskExample;
import com.sinosoft.taxbenefit.core.generated.model.TaxCommonCode;
import com.sinosoft.taxbenefit.core.generated.model.TaxCommonCodeExample;
import com.sinosoft.taxbenefit.manage.quartz.api.quartzdb.QuartzBo;
import com.sinosoft.taxbenefit.manage.quartz.constant.QuartzConstant;

@Component
public class QuartzBoImpl implements QuartzBo {
	@Autowired
	private TaxBatchTaskMapper taxBatchTaskMapper;
	
	@Autowired
	private TaxCommonCodeMapper taxCommonCodeMapper;
	
	
	public List<TaxBatchTask> queryBatchFromConfig(String type)throws Exception {
		TaxBatchTaskExample example = new TaxBatchTaskExample();
		example.createCriteria().andBatchTypeEqualTo(type).andIsDeleteEqualTo(QuartzConstant.SYS_IS_DELETE_N);
		return taxBatchTaskMapper.selectByExample(example);
	}

	public TaxCommonCode getCommonCode(String sysCode, String codeType) {
		TaxCommonCodeExample example = new TaxCommonCodeExample();
		example.createCriteria().andSysCodeEqualTo(sysCode).andCodeTypeEqualTo(codeType);
		return taxCommonCodeMapper.selectByExample(example).get(0);
	}
}
