package com.sinosoft.taxbenefit.manage.jeecg.mapper;

import java.util.List;

import com.sinosoft.taxbenefit.manage.base.dao.BaseDao;
import com.sinosoft.taxbenefit.manage.jeecg.entity.SysRoleRel;

/**
 * SysRoleRel Mapper
 * @author Administrator
 *
 */
public interface SysRoleRelDao<T> extends BaseDao<T> {
	
	public void deleteByRoleId(java.util.Map<String, Object> param);
	
	public void deleteByObjId(java.util.Map<String, Object> param);
	
	
	public List<SysRoleRel> queryByRoleId(java.util.Map<String, Object> param);
	
	
	public List<SysRoleRel> queryByObjId(java.util.Map<String, Object> param);
	
	
}
