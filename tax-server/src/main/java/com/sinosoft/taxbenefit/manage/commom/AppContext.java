package com.sinosoft.taxbenefit.manage.commom;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.springframework.web.context.support.WebApplicationContextUtils;

import com.sinosoft.platform.common.context.SpringContext;

public class AppContext extends HttpServlet{

	private SpringContext springContext;
	
	@Override
	public void init(ServletConfig servletConfig) throws ServletException {
		super.init(servletConfig);
	    springContext= (SpringContext) WebApplicationContextUtils
                .getRequiredWebApplicationContext(getServletContext()).getBean(
                        "springContext");
	}

	public SpringContext getSpringContext() {
		return springContext;
	}

	public void setSpringContext(SpringContext springContext) {
		this.springContext = springContext;
	}

	
}
