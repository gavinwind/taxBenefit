package com.sinosoft.taxbenefit.manage.batch.biz.service;

import java.util.List;

import com.sinosoft.taxbenefit.manage.base.page.BasePage;
import com.sinosoft.taxbenefit.manage.batch.dto.BatchTaskDTO;

public interface BatchTaskService {

	/**
	 * 获取所有系统批处理的配置信息
	 * @return
	 */
	List<BatchTaskDTO> queryBatchTaskList(BasePage page);
	
	/**
	 * 获取指定批处理信息
	 * @param sid
	 * @return
	 */
	BatchTaskDTO queryBatchTaskById(Integer sid);
	
	/**
	 * 保存批处理信息
	 * @param batchTask
	 */
	void saveNewBatchTask(BatchTaskDTO batchTask);
	
	/**
	 * 更新批处理信息
	 * @param batchTask
	 */
	void updateBatchTask(BatchTaskDTO batchTask);
}
