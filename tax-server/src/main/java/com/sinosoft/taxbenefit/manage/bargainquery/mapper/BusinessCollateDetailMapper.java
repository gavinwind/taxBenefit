package com.sinosoft.taxbenefit.manage.bargainquery.mapper;

import java.util.List;

import com.sinosoft.taxbenefit.core.generated.model.TaxBusinessCollateDetail;
import com.sinosoft.taxbenefit.manage.bargainquery.dto.BusinessCollateDetailDTO;

public interface BusinessCollateDetailMapper {
	/**
	 * @param businessCollateDetailDTO
	 * @return
	 */
	List<TaxBusinessCollateDetail> queryByList(BusinessCollateDetailDTO businessCollateDetailDTO);
	/**
	 * @param businessCollateDetailDTO
	 * @return
	 */
	Integer queryByCount(BusinessCollateDetailDTO businessCollateDetailDTO);
}
