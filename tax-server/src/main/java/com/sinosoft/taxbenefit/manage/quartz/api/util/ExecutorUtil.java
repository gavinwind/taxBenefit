package com.sinosoft.taxbenefit.manage.quartz.api.util;

import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

public class ExecutorUtil {

	
	/**
	 * 最大任务队列长度
	 */
	public final static int  DEFAULT_QUEUE_CAPACITY = 500;
	
	/**
	 * 线程数
	 */
	public final static int  DEFAULT_CORE_POOLSIZE = 1;
	
	/**
	 * 线程最大空闲时间
	 */
	public final static int  DEFAULT_KEEP_ALIVE_SECONDS = 300;
	
	/**
	 * 生成线程池对象
	 * @param counts
	 * @return
	 */
	public static ThreadPoolTaskExecutor createPoolTaskExecutor(int maxcount) {
		ThreadPoolTaskExecutor poolTaskExecutor = new ThreadPoolTaskExecutor();
		// 线程池所使用的缓冲队列
		poolTaskExecutor.setQueueCapacity(200);
		
		//线程池维护线程的最大数量,如果线程数量小于1则设置其为1
		if(maxcount < 1 ){
			maxcount = DEFAULT_CORE_POOLSIZE;
		}
		poolTaskExecutor.setMaxPoolSize(maxcount);
		
		// 线程池维护线程的数量
		poolTaskExecutor.setCorePoolSize(maxcount);
		
		/*
		 *  线程池维护线程所允许的空闲时间,,当线程池中的线程数量大于 corePoolSize时，
		 * 如果某线程空闲时间超过keepAliveTime，线程将被终止。这样，线程池可以动态的调整池中的线程数
		 */
		poolTaskExecutor.setKeepAliveSeconds(DEFAULT_KEEP_ALIVE_SECONDS);
		
		poolTaskExecutor.initialize();
		return poolTaskExecutor;
	}
}
