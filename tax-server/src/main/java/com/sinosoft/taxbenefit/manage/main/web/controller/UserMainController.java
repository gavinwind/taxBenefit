package com.sinosoft.taxbenefit.manage.main.web.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.sinosoft.taxbenefit.manage.base.entity.TreeNode;
import com.sinosoft.taxbenefit.manage.base.util.Constant.SuperAdmin;
import com.sinosoft.taxbenefit.manage.base.util.HtmlUtil;
import com.sinosoft.taxbenefit.manage.base.util.SessionUtils;
import com.sinosoft.taxbenefit.manage.base.util.TreeUtil;
import com.sinosoft.taxbenefit.manage.base.util.URLUtils;
import com.sinosoft.taxbenefit.manage.base.web.BaseAction;
import com.sinosoft.taxbenefit.manage.jeecg.biz.service.impl.SysMenuBtnService;
import com.sinosoft.taxbenefit.manage.jeecg.biz.service.impl.SysMenuService;
import com.sinosoft.taxbenefit.manage.jeecg.entity.SysMenu;
import com.sinosoft.taxbenefit.manage.jeecg.entity.SysMenuBtn;
import com.sinosoft.taxbenefit.manage.main.dto.WebLoginDTO;
import com.sinosoft.taxbenefit.manage.main.web.util.MainViewConstant;

/**
 * 主页面
 * @author SheChunMing
 */
@Controller
@RequestMapping("/manage/main.do")
public class UserMainController extends BaseAction {
	@Autowired(required=false) 
	private SysMenuService<SysMenu> sysMenuService; 
	@Autowired(required=false) 
	private SysMenuBtnService<SysMenuBtn> sysMenuBtnService;
	
	/**
	 * 进入首页
	 * @param url
	 * @param classifyId
	 * @return
	 */
	@RequestMapping(params = "action=toMain.do") 
	public ModelAndView  main(HttpServletRequest request){
		logger.info("进入后台系统页面 :/manage/main.do?action=toMain.do");
		Map<String,Object>  context = getRootMap();
		WebLoginDTO user = SessionUtils.getUser(request);
		List<SysMenu> rootMenus = null;
		List<SysMenu> childMenus = null;
		List<SysMenuBtn> childBtns = null;
		//超级管理员
		if(user != null && SuperAdmin.YES.key ==  user.getSuperAdmin()){
			rootMenus = sysMenuService.getRootMenu(null);// 查询所有根节点
			childMenus = sysMenuService.getChildMenu();//查询所有子节点
		}else{
			rootMenus = sysMenuService.getRootMenuByUser(user.getSid() );//根节点
			childMenus = sysMenuService.getChildMenuByUser(user.getSid());//子节点
			childBtns = sysMenuBtnService.getMenuBtnByUser(user.getSid());//按钮操作
			buildData(childMenus,childBtns,request); //构建必要的数据
		}
		context.put("user", user);
		context.put("menuList", treeMenu(rootMenus,childMenus));
		return forword(MainViewConstant.MANAGE_MAIN_VIEW,context); 
	}
	
	/**
	 * 构建树形数据
	 * @return
	 */
	private List<TreeNode> treeMenu(List<SysMenu> rootMenus,List<SysMenu> childMenus){
		TreeUtil util = new TreeUtil(rootMenus,childMenus);
		List<TreeNode> list=util.getTreeNode();
		TreeNode treeNode=list.get(0);
		list.remove(0);
		list.add(treeNode);
		return list;
	}
	
	
	/**
	 * 构建树形数据
	 * @return
	 */
	private void buildData(List<SysMenu> childMenus,List<SysMenuBtn> childBtns,HttpServletRequest request){
		//能够访问的url列表
		List<String> accessUrls  = new ArrayList<String>();
		//菜单对应的按钮
		Map<String,List> menuBtnMap = new HashMap<String,List>(); 
		for(SysMenu menu: childMenus){
			//判断URL是否为空
			if(StringUtils.isNotBlank(menu.getUrl())){
				List<String> btnTypes = new ArrayList<String>();
				for(SysMenuBtn btn  : childBtns){
					if(menu.getSid().equals(btn.getMenuid())){
						btnTypes.add(btn.getBtnType());
						URLUtils.getBtnAccessUrls(menu.getUrl(), btn.getActionUrls(),accessUrls);
					}
				}
				menuBtnMap.put(menu.getUrl(), btnTypes);
				URLUtils.getBtnAccessUrls(menu.getUrl(), menu.getActions(),accessUrls);
				accessUrls.add(menu.getUrl());
			}
		}
		SessionUtils.setAccessUrl(request, accessUrls);//设置可访问的URL
		SessionUtils.setMemuBtnMap(request, menuBtnMap); //设置可用的按钮
	}
	
	/**
	 * 获取Action下的按钮
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(params ="action=getActionBtn.do")
	public void  getActionBtn(String url,HttpServletRequest request,HttpServletResponse response) throws Exception{
		Map<String, Object> result = new HashMap<String, Object>();
		List<String> actionTypes = new ArrayList<String>();
		//判断是否超级管理员
		if(SessionUtils.isAdmin(request)){
			result.put("allType", true);
		}else{
			String menuUrl = URLUtils.getReqUri(url);
			menuUrl = StringUtils.remove(menuUrl,request.getContextPath());
			//获取权限按钮
			actionTypes = SessionUtils.getMemuBtnListVal(request, StringUtils.trim(menuUrl));
			result.put("allType", false);
			result.put("types", actionTypes);
		}
		result.put(SUCCESS, true);
		HtmlUtil.writerJson(response, result);
	}
}
