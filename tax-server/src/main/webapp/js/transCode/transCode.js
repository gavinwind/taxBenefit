$package('taxCodeMapping');
taxCodeMapping = function(){
	var _box = null;
	var _this = {
		config:{
			event:{
				add:function(){
					$('#typeIds_combobox').combobox('reload');
					_box.handler.add();
				},
				edit:function(){
					$('#typeIds_combobox').combobox('reload');
					_box.handler.edit();
				}
			},
  			dataGrid:{
  				title:'转码查询',
	   			url:'dataList.do',
	   			columns:[[
					{field:'sid',checkbox:true},
					{field:'codeType',title:'转码类型',align:'center',sortable:true,width:100,
							formatter:function(value,row,index){
								return row.codeType;
							}
						},
					{field:'remark',title:'转码类型名称',align:'center',sortable:true,width:100,
							formatter:function(value,row,index){
								return row.remark;
							}
						},
					{field:'originalCode',title:'核心编码',align:'center',sortable:true,width:100,
							formatter:function(value,row,index){
								return row.originalCode;
							}
						},
					{field:'originalDesc',title:'核心编码说明',align:'center',sortable:true,width:100,
							formatter:function(value,row,index){
								return row.originalDesc;
							}
						},
					{field:'targetCode',title:'中保信编码',align:'center',sortable:true,width:100,
							formatter:function(value,row,index){
								return row.targetCode;
							}
						},
					{field:'targetDesc',title:'中保信编码说明',align:'center',sortable:true,width:100,
							formatter:function(value,row,index){
								return row.targetDesc;
							}
						},
					]]
			}
		},
		init:function(){
			_box = new YDataGrid(_this.config); 
			_box.init();
		}
	}
	return _this;
}();

$(function(){
	taxCodeMapping.init();
	//datagrid加载数据之前
	$("#data-list").datagrid({
		onBeforeLoad:function (){
			//刷新转码类型的select框
			$.post(urls['msUrl']+"manage/transCode/getCodeTypeList.do","",function(data){	
				var text = "<option value=''>请选择</option>"+
						"<option value='new'>添加新的转码类型</option>";
				var cont =  "<option value=''>全部</option>";
				var html="";
				for (var i = 0; i < data.length; i++) {
					html = html + "<option value='" + data[i].codeType + "'>"
							+ data[i].remark + "</option>";
				}
				text=text+html;
				cont=cont+html;
				$("[name='codeType']").empty();
				$("[name='codeType']").eq(1).append(text);
				$("[name='codeType']").eq(0).append(cont);
			});
		}	
	});
	//dialog窗口加载数据完成后
	$("#edit-win").dialog({
		"onOpen":function(){
			//修改页面
			if($("#SID").val() != ""){
				$("#codeType").prop("disabled", true);
				$("#newCodeType").hide();
				$("#remark").hide();
			}else{
				//添加页面
				$("#codeType").removeAttr("name");
				$("#codeType").prop("disabled", false);
				$("#newCodeType").show();
				$("#remark").show();
			}
		},
		"onClose":function(){	//dialog窗口关闭后
			$("#SID").val("");
			$("#codeType").attr("name","codeType");
			$("#newCodeType").find("input").attr("name","codeType");
		}
	});
});
//选择select框事件
function changeCodeType(obj){
	if($(obj).val()=="new" || $(obj).val()==""){
		$("#newCodeType").show();
		$("#remark").show();
		$("#codeType").removeAttr("name");
		$("#newCodeType").find("input").attr("name","codeType");
	}else{
		$("#newCodeType").hide();
		$("#remark").hide();
		$("#codeType").attr("name","codeType");
		$("#newCodeType").find("input").removeAttr("name");
		$("#newCodeType").find("input").val($(obj).val());
		$("#remark").find("input").val($(obj).find("option:selected").text());
	}
}