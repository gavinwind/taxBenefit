$package('jeecg.batchTaskController');
jeecg.batchTaskController = function(){
	var _box = null;
	var _this = {
		config:{
			event:{
				add:function(){
					$('#typeIds_combobox').combobox('reload');
					_box.handler.add();
				},
				edit:function(){
					$('#typeIds_combobox').combobox('reload');
					_box.handler.edit();
				}
			},
  			dataGrid:{
  				title:'批处理配置信息',
	   			url:'dataList.do',
	   			columns:[[
					{field:'sid',checkbox:true},
					{field:'taskName',title:'批处理名称',align:'center',sortable:true,width:100,
							formatter:function(value,row,index){
								return row.taskName;
							}
						},
					{field:'executeTime',title:'触发时间',align:'center',sortable:true,width:100,
							formatter:function(value,row,index){
								return row.executeTime;
							}
						},
					{field:'dealClassName',title:'批处理调用类名',align:'center',sortable:true,width:100,
							formatter:function(value,row,index){
								return row.dealClassName;
							}
						},
					{field:'dealMethodName',title:'批处理调用方法名',align:'center',sortable:true,width:100,
							formatter:function(value,row,index){
								return row.dealMethodName;
							}
						},
					{field:'batchType',title:'调用状态',align:'center',sortable:true,width:100,
							formatter:function(value,row,index){
								if('01' == row.batchType){
									return '启用';
								}else if('02' == row.batchType){
									return '停用';
								}
								return '未知';
							}
						},
					{field:'description',title:'批处理调用说明',align:'center',sortable:true,width:100,
							formatter:function(value,row,index){
								return row.description;
							}
						} 		
					]]
			}
		},
		init:function(){
			_box = new YDataGrid(_this.config); 
			_box.init();
		}
	}
	return _this;
}();

$(function(){
	jeecg.batchTaskController.init();
});