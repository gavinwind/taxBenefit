$package('jeecg.taxRequestCollateDetail');
jeecg.taxRequestCollateDetail = function(){
	var busId=$("#busId").val();
	var _box = null;
	var _this = {
		config:{
			event:{
				add:function(){
					$('#typeIds_combobox').combobox('reload');
					_box.handler.add();
				},
				edit:function(){
					$('#typeIds_combobox').combobox('reload');
					_box.handler.edit();
				}
			},
			
  			dataGrid:{
  				title:'交易核对查询详情',
	   			url:'detailList.do?busId='+busId,
	   			columns:[[
					{field:'sid',hidden:true},
					{field:'policyNo',title:'保单号码',align:'center',sortable:true,width:100,
							formatter:function(value,row,index){
								return row.policyNo;
							}
						},
					{field:'sequenceNo',title:'分单号吗',align:'center',sortable:true,width:100,
							formatter:function(value,row,index){
								return row.sequenceNo;
							}
						},
					{field:'edorNo',title:'保全批单号',align:'center',sortable:true,width:100,
							formatter:function(value,row,index){
								return row.edorNo;
							}
						},
					{field:'claimNo',title:'理赔赔案号',align:'center',sortable:true,width:100,
							formatter:function(value,row,index){
								return row.claimNo;
							}
						},
					{field:'renewFeeNo',title:'费用编码',align:'center',sortable:true,width:100,
							formatter:function(value,row,index){
								return row.renewFeeNo;
							}
						},
					{field:'feeStatus',title:'保费状态',align:'center',sortable:true,width:100,
							formatter:function(value,row,index){
								return row.feeStatus;
							}
						},
					{field:'renewEndorsementNo',title:'续保批单号',align:'center',sortable:true,width:100,
							formatter:function(value,row,index){
								return row.renewEndorsementNo;
							}
						},
					{field : 'feeId',title : '公司费用编号',align:'center',sortable:true,width:100,
							formatter:function(value,row,index){
								return row.feeId;
							}
						} 		
					]]
			}
		},
		init:function(){
			_box = new YDataGrid(_this.config); 
			_box.init();
		}
	}
	return _this;
}();

$(function(){
	jeecg.taxRequestCollateDetail.init();
});