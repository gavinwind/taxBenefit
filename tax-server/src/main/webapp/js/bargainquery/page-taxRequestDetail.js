$package('jeecg.taxRequestDetail');
jeecg.taxRequestDetail = function(){
	var businessDate=$("#businessDate").val();
	var _box = null;
	var _this = {
		config:{
			event:{
				add:function(){
					$('#typeIds_combobox').combobox('reload');
					_box.handler.add();
				},
				edit:function(){
					$('#typeIds_combobox').combobox('reload');
					_box.handler.edit();
				}
			},
			
  			dataGrid:{
  				title:'请求记录明细',
	   			url:'detailList.do?businessDate='+businessDate,
	   			columns:[[
					{field:'sid',hidden:true},
					{field:'serialNo',title:'交易流水号',align:'center',sortable:true,width:150,
							formatter:function(value,row,index){
								return row.serialNo;
							}
						},
					{field:'portCode',title:'接口编码',align:'center',sortable:true,width:80,
							formatter:function(value,row,index){
								return row.portCode;
							}
						},
					{field:'resultCode',title:'处理结果',align:'center',sortable:true,width:50,
							formatter:function(value,row,index){
								if(row.resultCode =='01'){
									return '成功';
								}
								else if(row.resultCode =='02'){
									return '失败';
								}
								return '未知';
							}
						},
					{field:'resultMessage',title:'结果描述',align:'left',sortable:true,width:250,
							formatter:function(value,row,index){
								return row.resultMessage;
							}
						}		
					]]
			}
		},
		init:function(){
			_box = new YDataGrid(_this.config); 
			_box.init();
		}
	}
	return _this;
}();

$(function(){
	jeecg.taxRequestDetail.init();
});