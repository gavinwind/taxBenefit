$package('jeecg.taxRequestBusinessCollate');
jeecg.taxRequestBusinessCollate = function(){
	var _box = null;
	var _this = {
		config:{
			event:{
				add:function(){
					$('#typeIds_combobox').combobox('reload');
					_box.handler.add();
				},
				edit:function(){
					$('#typeIds_combobox').combobox('reload');
					_box.handler.edit();
				}
			},
  			dataGrid:{
  				title:'交易查询',
	   			url:'dataList.do',
	   			columns:[[
					{field:'sid',hidden:true},
					{field:'businessDate',title:'交易日时间',align:'center',sortable:true,width:100,
							formatter:function(value,row,index){
								return row.businessDate;
							}
						},
					{field:'requetNum',title:'请求记录数',align:'center',sortable:true,width:100,
							formatter:function(value,row,index){
								return row.requetNum;
							}
						},
					{field:'reqSuccessNum',title:'请求成功数',align:'center',sortable:true,width:100,
							formatter:function(value,row,index){
								return row.reqSuccessNum;
							}
						},
					{field:'reqFailNum',title:'请求失败数',align:'center',sortable:true,width:100,
							formatter:function(value,row,index){
								return row.reqFailNum;
							}
						},
					{field:'collateNum',title:'核对记录数',align:'center',sortable:true,width:100,
							formatter:function(value,row,index){
								return row.collateNum;
							}
						},
					{field:'colSuccessNum',title:'核对成功数',align:'center',sortable:true,width:100,
							formatter:function(value,row,index){
								return row.colSuccessNum;
							}
						},
					{field:'colFailNum',title:'核对失败数',align:'center',sortable:true,width:100,
							formatter:function(value,row,index){
								return row.colFailNum;
							}
						},
					{field : 'opr',title : '操作',align:'center',sortable:false,width:150,
							formatter : function(value, row, index) {	
								return "<a href='#' onclick=toList("+ row.sid + ",'" + row.businessDate + "')>核对详情</a>"+
									" | <a href='#' onclick=toDetailList('"+row.businessDate+"')>记录明细</a>";
							}
						} 		
					]]
			}
		},
		init:function(){
			_box = new YDataGrid(_this.config); 
			_box.init();
		}
	}
	return _this;
}();

function toDetailList(businessDate){
	var title=businessDate+"请求记录明细";
	var url=urls['msUrl']+'manage/taxRequestDetail/list.do?businessDate='+businessDate ;
	addTab(url,title);
}

function addTab(url,title){
	var tt =window.parent.$("#tab-box");
	if(tt.tabs('exists',title) ){
		var tab = tt.tabs('getTab',title);
		var index = tt.tabs('getTabIndex',tab);
		tt.tabs('select',index);
		if(tab && tab.find('iframe').length > 0){  
			 var _refresh_ifram = tab.find('iframe')[0];  
			 _refresh_ifram.contentWindow.location.href=url;  
		} 
	}else{		
		var _content ="<iframe scrolling='auto' frameborder='0' src='"+url+"' style='width:100%; height:100%'></iframe>";
		tt.tabs('add',{
			title:title,
			content:_content,
			closable:true});	
	}
}

function toList(sid,businessDate){
	var title=businessDate+"交易核对详情";
//	var tt =window.parent.$("#tab-box");
	var url=urls['msUrl']+'manage/taxRequestCollateDetail/list.do?busId='+sid ;
	addTab(url,title);
//	if(tt.tabs('exists',title) ){
//		var tab = tt.tabs('getTab',title);
//		var index = tt.tabs('getTabIndex',tab);
//		tt.tabs('select',index);
//		if(tab && tab.find('iframe').length > 0){  
//			 var _refresh_ifram = tab.find('iframe')[0];  
//			 _refresh_ifram.contentWindow.location.href=url;  
//		} 
//	}else{		
//		var _content ="<iframe scrolling='auto' frameborder='0' src='"+url+"' style='width:100%; height:100%'></iframe>";
//		tt.tabs('add',{
//			title:title,
//			content:_content,
//			closable:true});	
//	}
}
$(function(){
	jeecg.taxRequestBusinessCollate.init();
});