<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:te="http://www.seasar.org/teeda/extension" xml:lang="ja"
	lang="ja">
<head>
<title>系统错误</title>
<%@ include file="/resource.jsp" %>
<link href="${basePath }/css/error/error_404.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body>
	<div class="errorbigbox">
		<div class="bg_img">
			<img class="bg_img01" src="${basePath }/images/error/bg_img03.png" /> <img
				class="bg_img02" src="${basePath }/images/error/bg_img02.png" />
		</div>
		<div class="right_text">
			<p class="text_01">抱歉，找不到您要的页面......</p>
			<p class="text_02">Sorry,the page you are looking for is not
				existing.</p>
			<p class="text_03">
				似乎你所寻找的网页已移动或丢失了。<br /> 或者也许你只是键入了错误的一些信息。<br /> 请不要担心，这没事。<br />
				火星不太安全，我可以面给送你回地球
			</p>
			<div class="text_04"><a href="${basePath }/manage/main.do?action=toMain.do">返回首页</a></div>
		</div>
	</div>
</body>
</html>
