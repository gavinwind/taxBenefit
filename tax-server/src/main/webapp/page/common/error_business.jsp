<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:te="http://www.seasar.org/teeda/extension" xml:lang="ja"
	lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/resource.jsp" %>
<title>系统错误信息</title>
<link href="${basePath }/css/error/error_normal.css" rel="stylesheet" type="text/css" />

 <script language="javascript">

        $(document).ready(function() { 
            function jump(count) { 
                window.setTimeout(function(){ 
                    count--; 
                    if(count > 0) { 
                        $('#num').html(count); 
                        jump(count); 
                    } else { 
                        location.href=$("#basePath").val()+"/manage/main.do?action=toMain.do"; 
                    } 
                }, 1000); 
            } 
            jump(5); 
        }); 

</script> 
</head>

<body>
<div class="errorbigbox">
	<div class="errorbig">
        <div class="bg_img">
            <img class="bg_img01" src="${basePath }/img/website/others/error/bg_img06.png"/>
        </div>
        <div class="right_text">
            <p class="text_01">系统业务异常.....</p>
            <p class="text_02"><span id="num">5</span>秒后自动跳转</p>
            <input type="hidden" id="basePath" value="${basePath }">
			<a href="${basePath }/manage/main.do?action=toMain.do" class="text_03">返回首页</a>
        </div>
    </div>
</div>
</body>
</html>
