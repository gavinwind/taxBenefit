<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="f"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:te="http://www.seasar.org/teeda/extension" xml:lang="ja"
	lang="ja">
<head>
<%@ include file="/resource.jsp" %>
<link href="${basePath }/css/error/error_500.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>系统错误</title>
</head>

<body>
<div class="errorbigbox">
	<div class="errorbig">
        <div class="bg_img">
            <img class="bg_img01" src="${basePath }/images/error/bg_img05.png" />
            <img class="bg_img02" src="${basePath }/images/error/bg_img04.png" /> 
        </div>
        <div class="right_text">
            <p class="text_01">呜呜......被浏览器无情滴拒绝访问了</p>
            <p class="text_02">（错误信息500：服务器拒绝访问）</p>
            <div class="btnbox">
            	<a href="${basePath }/manage/main.do?action=toMain.do">返回首页</a>
            </div>
        </div>
    </div>
</div>
</body>
</html>
