<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path;
	String paths = request.getServletPath();
	request.setAttribute("imgBase", request.getAttribute("imgUrl"));
	request.setAttribute("paths", paths);
	request.setAttribute("base", basePath);
%>
<link rel="shortcut icon" type="image/ico" 
	href="${base}/img/back/lvcheng.ico?v=2">
<%-- <script type="text/javascript" src="${base}/lib/website/commonjquery.watermark.min.js"></script> --%>
<script src="${base}/lib/back/jquery-1.8.3.js" type="text/javascript"></script>
