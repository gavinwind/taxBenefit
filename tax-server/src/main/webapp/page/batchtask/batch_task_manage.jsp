<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
   <%@include file="/resource.jsp" %>
  </head>
  <body class="easyui-layout">
 	 <!-- Search panel start
 	 <div class="ui-search-panel" region="north" style="height: 80px;" title="过滤条件" data-options="striped: true,collapsible:false,iconCls:'icon-search',border:false" >  
 	 <form id="searchForm">
        <p class="ui-fields">
			<label class="ui-label">开始时间:</label>
			<input name="startDate" id="startDate" type="text" class="Wdate"
							onclick="WdatePicker({maxDate:'#F{$dp.$D(\'endDate\')}',readOnly:true})" style="width:150px;"/>
			<label class="ui-label">结束时间:</label>
			<input name="endDate" id="endDate" type="text" class="Wdate"
							onclick="WdatePicker({minDate:'#F{$dp.$D(\'startDate\')}',readOnly:true})" style="width:150px;"/>
	    </p>
	    <a href="#" id="btn-search" class="easyui-linkbutton" iconCls="icon-search">查询</a>
      </form>  
     </div> 
      -->
     <!-- Data List -->
     <div region="center" border="false" >
     <table id="data-list"></table>
	 </div>
	 <!-- Edit Win&Form -->
     <div id="edit-win" class="easyui-dialog" title="批处理编辑" data-options="closed:true,iconCls:'icon-save',modal:true" style="width:400px;overflow: hidden;">  
     	<form id="editForm" class="ui-form" method="post">  
     		 <input class="hidden" name="sid">
     		 <div class="ui-edit">
		     	   <div class="ftitle">批处理</div>
					<div class="fitem" style="margin-left: 20px;">
						<label>批处理名称</label>
						<input name="taskName" type="text" maxlength="100" class="easyui-validatebox" data-options="required:true" missingMessage="请填写批处理名称">
					</div>
					<div class="fitem" style="margin-left: 20px;">
						<label>调用状态</label>
						<select  name="batchType" style="width: 157px;">
							<option value="02">停用</option>
							<option value="01">启用</option>
						</select>
					</div>
					<div class="fitem" style="margin-left: 20px;">
						<label>触发时间</label>
						<input name="executeTime" type="text" maxlength="100" class="easyui-validatebox" data-options="required:true" missingMessage="请填写触发时间">
					</div>
					<div class="fitem" style="margin-left: 20px;">
						<label>批处理调用类名</label>
						<input name="dealClassName" type="text" maxlength="100" class="easyui-validatebox" data-options="required:true" missingMessage="请填写批处理调用类名">
					</div>
					<div class="fitem" style="margin-left: 20px;">
						<label>批处理调用方法名</label>
						<input name="dealMethodName" type="text" maxlength="100" class="easyui-validatebox" data-options="required:true" missingMessage="请填写批处理调用方法名">
					</div>
					<div class="fitem" style="margin-left: 20px;">
						<label>批处理调用说明</label>
						<input name="description" type="text" maxlength="200" class="easyui-validatebox" data-options="" missingMessage="请填写批处理调用说明">
					</div>
  			</div>
     	</form>
  	 </div>
	 
  	 <script type="text/javascript" src="<%=basePath%>/js/batchtask/batch_task_manage.js"></script>
  </body>
</html>
