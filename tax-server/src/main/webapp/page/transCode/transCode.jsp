<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@include file="/resource.jsp"%>
</head>
<body class="easyui-layout" >
	<!-- Search panel start -->
	<div class="ui-search-panel" region="north" style="height: 80px;"
		title="查询条件"
		data-options="striped: true,collapsible:false,iconCls:'icon-search',border:false">
		<form id="searchForm">
			<p class="ui-fields">
				<label class="ui-label">转码类型:</label> <select
					name="codeType" class="easyui-box ui-text" style="width: 150px;">
				
				</select>
				<label class="ui-label">核心编码</label>
				<input name="originalCode" type="text" class="easyui-box ui-text" style="width:150px;"/>
				<label class="ui-label">中保信编码</label>
				<input name="targetCode" type="text" class="easyui-box ui-text" style="width:150px;"/>
			</p>
			<a href="#" id="btn-search" class="easyui-linkbutton" iconCls="icon-search">查询</a>
		</form>
	</div>
	<!--  Search panel end -->

	<!-- Data List -->
	<div region="center" border="false">
		<table id="data-list"></table>
	</div>

	<!-- Edit Win&Form -->
	<div id="edit-win" class="easyui-dialog" title="平台转码" 
		data-options="closed:true,iconCls:'icon-save',modal:true,"
		style="width: 400px;overflow: hidden;" >
		<form id="editForm" class="ui-form" method="post">
			<input type="hidden" name="sid" id="SID">
			<div class="ui-edit">
				<div class="ftitle">平台转码信息</div>
				<div class="fitem" style="margin-left: 20px;">
					<label>转码类型：</label> 
					<select id="codeType" name="codeType" onchange="changeCodeType(this)"  
						class="easyui-box ui-text" style="width: 157px;" >
					</select>
				</div>
				<div class="fitem" id="newCodeType" style="margin-left: 20px;">
					<label>转码类型编码：</label> <input name="codeType" type="text" 
						maxlength="200" data-options="required:true" class="easyui-validatebox"  missingMessage="请填写类型编码">
				</div>
				<div class="fitem" style="margin-left: 20px;" id="remark">
					<label>转码类型名称：</label> <input name="remark" type="text" 
						maxlength="200" class="easyui-validatebox"
						data-options="required:true" missingMessage="请填写类型名称">
				</div>
				<div class="fitem" style="margin-left: 20px;">
					<label>核心编码：</label> <input name="originalCode" type="text"
						maxlength="30" class="easyui-validatebox"
						data-options="required:true" missingMessage="请填写核心编码">
				</div>
				<div class="fitem" style="margin-left: 20px;">
					<label>核心编码说明：</label> <input name="originalDesc" type="text"
						maxlength="200" class="easyui-validatebox"
						data-options="required:true" missingMessage="请填写核心编码说明">
				</div>
				<div class="fitem" style="margin-left: 20px;">
					<label>中保信编码：</label> <input name="targetCode" type="text"
						maxlength="30" class="easyui-validatebox" data-options="required:true"
						missingMessage="请填写中保信编码">
				</div>
				<div class="fitem" style="margin-left: 20px;">
					<label>中保信编码说明：</label> <input name="targetDesc" type="text"
						maxlength="200" class="easyui-validatebox" data-options="required:true"
						missingMessage="请填写中保信编码说明">
				</div>
			</div>
		</form>
	</div>
<script type="text/javascript"
		src="<%=basePath%>/js/transCode/transCode.js">
</script>
</body>
</html>
