<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@include file="/resource.jsp"%>

</head>
<body class="easyui-layout">
	<input type="hidden" value="${businessDate}" id="businessDate" />
	<!-- Data List -->
	<div region="center" border="false">
		<table id="data-list"></table>
	</div>
	<script type="text/javascript"
		src="<%=basePath%>/js/bargainquery/page-taxRequestDetail.js"></script>
</body>
</html>
