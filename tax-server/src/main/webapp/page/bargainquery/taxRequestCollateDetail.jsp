<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@include file="/resource.jsp"%>
<style type="text/css">
form table tr{height: 24px;line-height: 24px;}
form table td span{color: red;margin-right: 10px;}
</style>
</head>
<body class="easyui-layout">
	<!-- Search panel start -->
	<input type="hidden" id="busId" value="${busId}" />
	<!--  Search panel end -->
	<div class="ui-search-panel" region="north" style="height: 80px;"
		data-options="striped: true,collapsible:false,iconCls:'icon-search',border:false">
		<form id="searchForm">
			<table border="0px" width="400px;" style="margin-top: 5px;">
				<tr>
					<td>请求记录数：</td>
					<td><span>${collate.requetNum}条</span></span></td>
					<td>请求成功数：</td>
					<td><span>${collate.reqSuccessNum}条</span></td>
					<td>请求失败数：</td>
					<td><span>${collate.reqFailNum}条</span></td>
				</tr>
				<tr>
					<td>核对记录数：</td>
					<td><span>
							<c:if test="${collate.collateNum eq null}">暂无</c:if>
							<c:if test="${collate.collateNum ne null}">${collate.collateNum}条</c:if>
						</span></td>
					<td>核对成功数：</td>
					<td><span>
							<c:if test="${collate.colSuccessNum eq null}">暂无</c:if>
							<c:if test="${collate.colSuccessNum ne null}">${collate.colSuccessNum}条</c:if>
						</span></td>
					<td>核对失败数：</td>
					<td><span>
							<c:if test="${collate.colFailNum eq null}">暂无</c:if>
							<c:if test="${collate.colFailNum ne null}"><span>${collate.colFailNum}条</c:if>
						</span></td>
				</tr>
			</table>
		</form>
	</div>
	<!-- Data List -->
	<div region="center" border="false">
		<table id="data-list"></table>
	</div>
	<script type="text/javascript"
		src="<%=basePath%>/js/bargainquery/page-taxRequestCollateDetail.js"></script>
</body>
</html>
